Filter package
The list of filters that do not work:
1. HalftoneFilter

TODO:
Add thinning algorithm implementation (Stentiford, Iterative Thinning, Zhang-Suen, Neusius-Olszewski)
Add unit test to ImageMath
Add unit test to
NEW: Add Wiener filtering
Kalman filter is done by Apache Commons math3
normalized cuts
mean shift

Bayesian view
Region-based merge
split algorithms
Graph theoretic formulation of grouping
G = (V, E) - undirected, edges formed between every pair of nodes
w(i, j) - is a weight function of similarity between nodes i & j

http://www4.comp.polyu.edu.hk/~cslzhang/MSRM/PR_MSRM_website.htm

FIX: RaysFilter (OutOfBoundException :-( ) - fixed
FIX: ShineFilter (OutOfBoundException :-( )



Useful filters:
1. MedianFilter - for noise removal.  
2. ReduceNoiseFilter as a name suggests removes some noise (1) is better
3. SaturationFilter - improves an image quality
4. ScaleFilter - changes the image size, default is 32x32.
5. SharpenFilter - sharpens the image, useful for image enhancement  
6. SolarizeFilter - also sharpens the image.
7. SwimFilter - can be useful for making text as waves.
 

Image segmentation - Java implementations
https://github.com/search?q=Java+image+segmentation&ref=cmdform

Some popular image segmentation techniques:
active contours
level sets
graph-based merging 
mean shift
texture and intervening contour-based normalized cuts
binary MRF solved using graph cuts

Useful maven plug-ins:
1. Code coverage - cobertura:cobertura
2. Finds TODOs   - taglist:taglist
3. Checks versions of the dependencies - mvn versions:display-dependency-updates

Process flow
Image Segmentation ---> Feature Extraction ---> Classification
http://algoval.essex.ac.uk/viadocs/







