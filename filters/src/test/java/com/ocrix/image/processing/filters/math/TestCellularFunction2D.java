/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link CellularFunction2D} functionality.
 */
public class TestCellularFunction2D {
	private CellularFunction2D cf2d;
	
	@Before
	public void setUp() throws Exception {
		cf2d = new CellularFunction2D();
	}

	@Test
	public void testCellularFunction2D() {
		assertNotNull(cf2d);
	}

	@Test
	public void testSetCoefficient() {
		int c = TestUtils.genInteger(4);
		float v = TestUtils.genFloat();
		cf2d.setCoefficient(c, v);
		float result = cf2d.getCoefficient(c);
		assertEquals(v, result, EPSILON);
	}
	
	@Test
	public void testSetCoefficientEsleCase() {
		int c = QUAD;
		float v = TestUtils.genFloat();
		cf2d.setCoefficient(c, v);
		assertTrue(true);
	}

	@Test
	public void testGetCoefficient() {
		testSetCoefficient();
	}

	@Test
	public void testEvaluate() {
		float x = TestUtils.genFloat();
		float y = TestUtils.genFloat();
		float result = cf2d.evaluate(x, y);
		assertTrue(result != 0.0f);
	}
	
	@Test
	public void testToString(){
		assertTrue(cf2d.toString().contains(CellularFunction2D.class.getName()));
	}
}
