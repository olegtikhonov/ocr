/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.CDLVI;
import static com.ocrix.image.processing.filters.composite.TestParams.FRENCH_DEPARTMENT_VAR;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.Validator;


/**
 * Tests {@link Histogram} functionality.
 */
public class HistogramTest {

	private static Histogram histogram = null;
	private static BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		histogram = new Histogram();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(histogram);
		
		/* Another citor */
		int[] pixels = TestUtils.fillArray(FRENCH_DEPARTMENT_VAR); 
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), src.getMinX(), QUAD);
		assertNotNull(histogram);
	}

	
	@Test
	public void testIsGray(){
		int intVal = (histogram.isGray()) ? 1 : 0;
		Validator.validateIntInRange(intVal, 0, 1);
	}
	
	
	@Test
	public void testGetNumSamples(){
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		assertTrue(histogram.getNumSamples() > 0);
	}
	
	
	@Test
	public void testGetFrequency(){
		assertEquals(-1, histogram.getFrequency(-QUAD));
		assertEquals(-1, histogram.getFrequency(QUAD, FRENCH_DEPARTMENT_VAR));
		
		//numSamples > 0 && isGray && value >= 0 && value <= 255
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		int result = histogram.getFrequency(0);
		assertEquals(CDLVI, result);
	}

	
	@Test
	public void testGetMinFrequency(){
		assertEquals(-1, histogram.getMinFrequency());
		assertEquals(-1, histogram.getMinFrequency(~QUAD));
		
		
		//numSamples > 0 && isGray
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		
		assertEquals(histogram.getMinFrequency(), 0);
	}
	
	@Test
	public void testGetMaxFrequency(){
		assertEquals(-1, histogram.getMaxFrequency());
		assertEquals(-1, histogram.getMaxFrequency(~QUAD));
		
		//numSamples > 0 && isGray
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		assertTrue(histogram.getMaxFrequency() > 0);
	}
	
	@Test
	public void testGetMinValue(){
		assertEquals(-1, histogram.getMinValue());
		assertEquals(-1, histogram.getMinValue(~QUAD));
		
		// numSamples > 0 && isGray
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		
		assertEquals(histogram.getMinValue(), 0);
	}
	
	
	@Test
	public void testGetMaxValue(){
		assertEquals(-1, histogram.getMaxValue());
		assertEquals(-1, histogram.getMaxValue(~QUAD));
		
		//numSamples > 0 && isGray
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		
		assertEquals(QUAD, histogram.getMaxValue());
	}
	
	
	@Test
	public void testGetMeanValue(){
		assertTrue(Float.compare(-1.0f, histogram.getMeanValue())<= 0);
		assertTrue(Float.compare(-1.0f, histogram.getMeanValue(~QUAD)) == 0);
		
		//numSamples > 0 && isGray
		int[] pixels = src.getRaster().getPixel(0, 0, new int[src.getWidth()]);
		histogram = new Histogram(pixels, src.getWidth(), src.getHeight(), 0, 100);
		histogram.setisGary(true);
		assertNotNull(histogram.getMeanValue() != 0);
	}
}
