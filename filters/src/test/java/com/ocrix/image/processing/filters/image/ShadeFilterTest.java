/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.SHADE;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.DIME;
import static org.junit.Assert.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.math.Function2D;
import com.ocrix.image.processing.filters.math.RidgedFBM;
import com.ocrix.image.processing.filters.vecmath.Color4f;
import com.ocrix.image.processing.filters.vecmath.Vector3f;


/**
 * Tests {@link ShadeFilter} functionality.
 */
public class ShadeFilterTest {

	private static final int RESULT_ZERO = 0;
	private ShadeFilter shadeFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		shadeFilter = new ShadeFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}

	@Test
	public void testFilterPixels() {
		int width = src.getWidth();
		int height = src.getHeight();
		int[] inPixels = src.getRGB(0, 0, width, height, null, 0, width);
		int[] result = shadeFilter.filterPixels(width, height, inPixels, null);
		assertNotNull(result);
	}

	@Test
	public void testShadeFilter() throws IOException {
		shadeFilter.setEnvironmentMap(ImageIO.read(new File(PREFIXPATH + OLD_TEXT)));
		BufferedImage result = shadeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHADE.to(), "gif");
	}

	@Test
	public void testSetBumpFunction() {
		Function2D bumpFunction = new RidgedFBM();
		shadeFilter.setBumpFunction(bumpFunction);
		assertEquals(bumpFunction, shadeFilter.getBumpFunction());
	}

	@Test
	public void testGetBumpFunction() {
		testSetBumpFunction();
	}

	@Test
	public void testSetBumpHeight() {
		float bumpHeight = TestUtils.genFloat();
		shadeFilter.setBumpHeight(bumpHeight);
		assertEquals(bumpHeight, shadeFilter.getBumpHeight(), EPSILON);
	}

	@Test
	public void testGetBumpHeight() {
		testSetBumpHeight();
	}

	@Test
	public void testSetBumpSoftness() {
		float bumpSoftness = TestUtils.genFloat();
		shadeFilter.setBumpSoftness(bumpSoftness);
		assertEquals(bumpSoftness, shadeFilter.getBumpSoftness(), EPSILON);
	}

	@Test
	public void testGetBumpSoftness() {
		testSetBumpSoftness();
	}

	@Test
	public void testSetEnvironmentMap() {
		shadeFilter.setEnvironmentMap(src);
		assertEquals(src, shadeFilter.getEnvironmentMap());
		
		// Case 2: environmentMap = null
		shadeFilter.setEnvironmentMap(null);
		assertNull(shadeFilter.getEnvironmentMap());
	}

	@Test
	public void testGetEnvironmentMap() {
		testSetEnvironmentMap();
	}

	@Test
	public void testSetBumpSource() {
		// Case 1: BUMPS_FROM_IMAGE
		shadeFilter.setBumpSource(ShadeFilter.BUMPS.BUMPS_FROM_BEVEL);
		assertEquals(ShadeFilter.BUMPS.BUMPS_FROM_BEVEL, shadeFilter.getBumpSource());

		// Case 2: BUMPS_FROM_IMAGE_ALPHA
		shadeFilter.setBumpSource(ShadeFilter.BUMPS.BUMPS_FROM_IMAGE_ALPHA);
		assertEquals(ShadeFilter.BUMPS.BUMPS_FROM_IMAGE_ALPHA, shadeFilter.getBumpSource());
		
		// Case 3: BUMPS_FROM_MAP
		shadeFilter.setBumpSource(ShadeFilter.BUMPS.BUMPS_FROM_MAP);
		assertEquals(ShadeFilter.BUMPS.BUMPS_FROM_MAP, shadeFilter.getBumpSource());
		
		// Case 4: BUMPS_FROM_BEVEL
		shadeFilter.setBumpSource(ShadeFilter.BUMPS.BUMPS_FROM_BEVEL);
		assertEquals(ShadeFilter.BUMPS.BUMPS_FROM_BEVEL, shadeFilter.getBumpSource());
	}

	@Test
	public void testGetBumpSource() {
		testSetBumpSource();
	}

	@Test
	public void testSetFromRGB() {
		Color4f shadeColor = new Color4f(Color.BLACK);
		shadeFilter.setFromRGB(shadeColor, DIME);
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(shadeFilter.toString().contains(ShadeFilter.class.getSimpleName()));
	}

	@Test
	public void testGetShadedColor() {
		Color4f color4f = new Color4f();
		try {
			Method setShadedColor = shadeFilter.getClass().getDeclaredMethod("setShadedColor", Color4f.class);
			setShadedColor.setAccessible(true);
			setShadedColor.invoke(shadeFilter, color4f);
		} catch(Exception e) {
			assertFalse(true);
		}
		
		
	    Color4f result = shadeFilter.getShadedColor();
	    assertEquals(color4f, result);
	}

	@Test
	public void testGetDiffuseColor() {
		Color4f color4f = new Color4f();
		try {
			Method setShadedColor = shadeFilter.getClass().getDeclaredMethod("setDiffuseColor", Color4f.class);
			setShadedColor.setAccessible(true);
			setShadedColor.invoke(shadeFilter, color4f);
		} catch(Exception e) {
			assertFalse(true);
		}
		
		
	    Color4f result = shadeFilter.getDiffuseColor();
	    assertEquals(color4f, result);
	}

	@Test
	public void testGetSpecularColor() {
		Color4f color4f = new Color4f();
		try {
			Method setShadedColor = shadeFilter.getClass().getDeclaredMethod("setSpecularColor", Color4f.class);
			setShadedColor.setAccessible(true);
			setShadedColor.invoke(shadeFilter, color4f);
		} catch (Exception e) {
			assertFalse(true);
		}

		Color4f result = shadeFilter.getSpecularColor();
		assertEquals(color4f, result);
	}

	@Test
	public void testGetL() {
		Vector3f vector3f = new Vector3f();
		try {
			Method setL = shadeFilter.getClass().getDeclaredMethod("setL", Vector3f.class);
			setL.setAccessible(true);
			setL.invoke(shadeFilter, vector3f);
		} catch (Exception e) {
			assertFalse(true);
		}

		Vector3f result = shadeFilter.getL();
		assertEquals(vector3f, result);
	}

	@Test
	public void testGetN() {
		Vector3f vector3f = new Vector3f();
		try {
			Method setN = shadeFilter.getClass().getDeclaredMethod("setN", Vector3f.class);
			setN.setAccessible(true);
			setN.invoke(shadeFilter, vector3f);
		} catch (Exception e) {
			assertFalse(true);
		}

		Vector3f result = shadeFilter.getN();
		assertEquals(vector3f, result);
	}
	
	@Test
	public void testGetEnvironmentMapP() {
		try {
			Method setN = shadeFilter.getClass().getDeclaredMethod("getEnvironmentMapP", Vector3f.class, int[].class, Integer.TYPE, Integer.TYPE);
			setN.setAccessible(true);
			shadeFilter.setEnvironmentMap(null);
			int result = (Integer) setN.invoke(shadeFilter, new Vector3f(), src.getRaster().getPixel(0, 0, new int[7]), src.getWidth(), src.getHeight());
			assertEquals(RESULT_ZERO, result);
		} catch (Exception e) {
			assertFalse(true);
		}
	}
}
