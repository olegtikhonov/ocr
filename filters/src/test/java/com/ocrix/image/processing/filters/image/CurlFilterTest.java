/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CURL;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.image.CurlFilter.Sampler;

/**
 * Tests {@link CurlFilter} functionality.
 */
public class CurlFilterTest {

	private static CurlFilter curlFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		curlFilter = new CurlFilter();
		curlFilter.setAngle(30.0f);
		curlFilter.setTransition(0.367f);
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}

	@Test
	public void testCurlFilter() {
		assertNotNull(curlFilter);
	}

	@Test
	public void testFilter() {
		BufferedImage result = curlFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterDstIsNotNullNearestNeighbour() throws IOException {
		curlFilter.interpolation = TransformFilter.NEAREST_NEIGHBOUR;
		BufferedImage result = curlFilter.filter(src, ImageIO.read(new File(PREFIXPATH + COW)));
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CURL.to(), "png");
		assertNotNull(result);
	}

	@Test
	public void testGetTransition() {
		float transition = TestUtils.genFloat(QUAD);
		curlFilter.setTransition(transition);
		assertEquals(transition, curlFilter.getTransition(), EPSILON);
	}

	@Test
	public void testSetTransition() {
		testGetTransition();
	}

	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat(QUAD);
		curlFilter.setAngle(angle);
		assertEquals(angle, curlFilter.getAngle(), EPSILON);
	}

	@Test
	public void testGetAngle() {
		testSetAngle();
	}

	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat(QUAD);
		curlFilter.setRadius(radius);
		assertEquals(radius, curlFilter.getRadius(), EPSILON);
	}

	@Test
	public void testGetRadius() {
		testSetRadius();
	}
	
	@Test
	public void testInnerSample() {
		Sampler sample = new Sampler(src);
		assertNotNull(sample);
	}
	
	@Test
	public void testInnerSampleSample() {
		int result = new Sampler(src).sample(TestUtils.genFloat(), TestUtils.genFloat());
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(curlFilter.toString().contains(CurlFilter.class.getName()));
	}
	
	@Test
	public void testTransformInverseCaseNegativeSin(){
		curlFilter.setAngle(QUAD);
		curlFilter.transformInverse(TestUtils.genInteger(), TestUtils.genInteger(), new float[5]);
	}
}
