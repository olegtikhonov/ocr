/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.MARBLE_TEX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 *  Tests {@link MarbleTexFilter} functionality.
 */
public class MarbleTexFilterTest {

	private MarbleTexFilter marbleTexFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		marbleTexFilter = new MarbleTexFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(marbleTexFilter);
	}
	
	
	@Test
	public void testMarbleTexFilter(){
		marbleTexFilter.setScale(marbleTexFilter.getScale() * 3);
		marbleTexFilter.setColormap(new LinearColormap(Color.WHITE.getRGB(), Color.GRAY.getRGB()));
		BufferedImage result = marbleTexFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MARBLE_TEX.to(), "png");
	}
	
	@Test
	public void testSetStretch() {
		float stretch = TestUtils.genFloat();
		marbleTexFilter.setStretch(stretch);
		assertEquals(stretch, marbleTexFilter.getStretch(), EPSILON);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		marbleTexFilter.setAngle(angle);
		assertEquals(angle, marbleTexFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloat();
		marbleTexFilter.setTurbulence(turbulence);
		assertEquals(turbulence, marbleTexFilter.getTurbulence(), EPSILON);
	}
	
	@Test
	public void testSetTurbulenceFactor() {
		float turbulenceFactor = TestUtils.genFloat();
		marbleTexFilter.setTurbulenceFactor(turbulenceFactor);
		assertEquals(turbulenceFactor, marbleTexFilter.getTurbulenceFactor(), EPSILON);
	}
	
	@Test
	public void testSetColormap() {
		Colormap colormap = new LinearColormap(Color.YELLOW.getRGB(), Color.BLUE.getRGB());
		marbleTexFilter.setColormap(colormap);
		assertEquals(colormap, marbleTexFilter.getColormap());
	}
	
	@Test
	public void testFilterRGB() {
		int result = marbleTexFilter.filterRGB(0, 0, Color.RED.getRGB());
		assertTrue(result < 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(marbleTexFilter.toString().contains(MarbleTexFilter.class.getName()));
	}
}
