/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.HSB_ADJUST;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link HSBAdjustFilter} functionality.
 */
public class HSBAdjustFilterTest {

	private HSBAdjustFilter hsbAdjustFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		hsbAdjustFilter = new HSBAdjustFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	
	@Test
	public void testCitor(){
		assertNotNull(hsbAdjustFilter);
	}
	
	@Test
	public void testGetHFactor() {
		float hFactor = TestUtils.genFloat();
		hsbAdjustFilter.setHFactor(hFactor);
		assertEquals(hFactor, hsbAdjustFilter.getHFactor(), EPSILON);
	}
	
	@Test
	public void testGetSFactor() {
		float sFactor = TestUtils.genFloat();
		hsbAdjustFilter.setSFactor(sFactor);
		assertEquals(sFactor, hsbAdjustFilter.getSFactor(), EPSILON);
	}
	
	@Test
	public void testGetBFactor() {
		float bFactor = TestUtils.genFloat();
		hsbAdjustFilter.setBFactor(bFactor);
		assertEquals(bFactor, hsbAdjustFilter.getBFactor(), EPSILON);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(RONNA); 
		int y = TestUtils.genInteger(RONNA); 
		int rgb = src.getRGB(x, y);
		int result = hsbAdjustFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(hsbAdjustFilter.toString().contains(HSBAdjustFilter.class.getName()));
	}
	
	@Test
	public void testHsbAdjustFilter(){
		hsbAdjustFilter.setBFactor(0.15f);
		hsbAdjustFilter.setHFactor(0.15f);
		hsbAdjustFilter.setSFactor(0.15f);
		BufferedImage result = hsbAdjustFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + HSB_ADJUST.to(), "jpeg");
	}
}
