/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.Filters.POLAR;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PolarFilter} functionality.
 */
public class PolarFilterTest {

	private PolarFilter polarFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		polarFilter = new PolarFilter();
		src = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(polarFilter);
	}
	
	@Test
	public void testPolarFilter(){
		BufferedImage result = polarFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + POLAR.to(), "png");
	}
	
	@Test
	public void testSetType() {
		// Case 1: PolarFilter.RECT_TO_POLAR
		polarFilter.setType(PolarFilter.RECT_TO_POLAR);
		assertEquals(PolarFilter.RECT_TO_POLAR, polarFilter.getType());
		// Case 2: PolarFilter.POLAR_TO_RECT
		polarFilter.setType(PolarFilter.POLAR_TO_RECT);
		assertEquals(PolarFilter.POLAR_TO_RECT, polarFilter.getType());
		// Case 3: PolarFilter.INVERT_IN_CIRCLE
		polarFilter.setType(PolarFilter.INVERT_IN_CIRCLE);
		assertEquals(PolarFilter.INVERT_IN_CIRCLE, polarFilter.getType());
	}
	
	@Test
	public void testToString() {
		assertTrue(polarFilter.toString().contains(PolarFilter.class.getSimpleName()));
	}
	
	
	@Test
	public void testTransformInverse() {
		int x = TestUtils.genInteger();
		int y = TestUtils.genInteger();
		float[] out = TestUtils.generateFloatArray(256);

		// Insures that width is set.
		polarFilter.filter(src, null);
		
		// Case 1: RECT_TO_POLAR
		polarFilter.setType(PolarFilter.RECT_TO_POLAR);
		polarFilter.transformInverse(x, y, out);
		assertTrue(true);
		
		// Case 2: POLAR_TO_RECT
		polarFilter.setType(PolarFilter.POLAR_TO_RECT);
		polarFilter.transformInverse(x, y, out);
		assertTrue(true);
		// Case 2.1 - theta >= ImageMath.PI
		// theta = x / width * ImageMath.TWO_PI;
		polarFilter.transformInverse(src.getWidth(), y, out);
		assertTrue(true);
		// Case 2.2 - theta >= ImageMath.PI
	    // theta >= ImageMath.PI
		polarFilter.transformInverse(src.getWidth() / 2, y, out);
		assertTrue(true);
		// Case 2.3 - theta >= ImageMath.PI
	    // theta >= 0.5f * ImageMath.PI
		polarFilter.transformInverse(src.getWidth() / 3, y, out);
		assertTrue(true);
		
		// Case 3: INVERT_IN_CIRCLE
		polarFilter.setType(PolarFilter.INVERT_IN_CIRCLE);
		polarFilter.transformInverse(x, y, out);
		assertTrue(true);
	}
}
