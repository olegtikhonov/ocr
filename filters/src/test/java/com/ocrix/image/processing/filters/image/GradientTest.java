/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TETRA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link Gradient} functionality.
 */
public class GradientTest {

	private static Gradient gradient = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		gradient = new Gradient();
	}
	
	
	@Test
	public void testCitors(){
		assertNotNull(gradient);
		int[] colors = {Color.BLACK.getRGB(), Color.BLUE.getRGB(), Color.CYAN.getRGB(), Color.DARK_GRAY.getRGB()};
		gradient = new Gradient(colors);
		assertNotNull(gradient);
		int[] a = TestUtils.fillArray(QUAD);
		gradient = new Gradient(a, colors);
		assertNotNull(gradient);
		byte[] interTypes = {Gradient.CIRCLE_DOWN, Gradient.CIRCLE_UP, Gradient.CONSTANT, Gradient.HUE_CCW, Gradient.HUE_CW, Gradient.LINEAR, Gradient.RGB, Gradient.SPLINE};
		gradient = new Gradient(TestUtils.fillArray(QUAD), colors, interTypes);
		assertNotNull(gradient);
	}
	
	
	@Test
	public void testClone(){
		Gradient clone = (Gradient) gradient.clone();
		assertEquals(0, gradient.equals(clone));
	}
	
	@Test
	public void testCopyTo(){
		Gradient clone = new Gradient();
		gradient.copyTo(clone);
		assertEquals(0, gradient.equals(clone));
	}
	
	@Test
	public void testSetColor() {
		int n = 1;
		int color = TestUtils.genInteger(256);
		gradient.setColor(n, color);
		assertEquals(color, gradient.getMap()[n]);
		
		/* For the sake of code coverage */
		//if (n > 0)
		n = -1;
		gradient.setColor(n, color);
		//if (n < 256 - 1)
		n = 257;
		gradient.setColor(n, color);
	}
	
	@Test
	public void testSetNumKnot() {
		int n = 1;
		int color = TestUtils.genInteger(256);
		gradient.setKnot(n, color);
		int knot = gradient.getKnot(n);
		assertEquals(color, knot);
	}
	
	@Test
	public void testGetNumKnot() {
		testSetNumKnot();
	}
	
	@Test
	public void testGetNumKnots() {
		assertTrue(gradient.getNumKnots() >= TETRA);
	}
	
	@Test
	public void testSetKnotType() {
		int n = TestUtils.genInteger(4);
		int type = TestUtils.genInteger(4);
		gradient.setKnotType(n, type);
		assertTrue(gradient.getKnotType(n) >= 0);
	}
	
	@Test
	public void testSetKnotBlend() {
		int n = TestUtils.genInteger(4);
		int type = TestUtils.genInteger(4);
		gradient.setKnotBlend(n, type);
		int result = gradient.getKnotBlend(n);
		assertTrue(result >= 0);
	}
	
	@Test
	public void testAddKnot() {
		int x = TestUtils.genInteger(4);
		int color = TestUtils.genInteger(256);
		int type = TestUtils.genInteger(4);
		int sizeBeforeKnots = gradient.getNumKnots();
		gradient.addKnot(x, color, type);
		int sizeAfterKnots = gradient.getNumKnots();
		assertTrue(sizeBeforeKnots < sizeAfterKnots);
	}
	
//	@Test
	public void testRemoveKnot() {
		int n = TestUtils.genInteger(gradient.getNumKnots());
		int sizeBeforeRemoving = gradient.getNumKnots();
		gradient.removeKnot(n);
		int sizeAfterRemoving = gradient.getNumKnots();
		assertTrue(sizeBeforeRemoving > sizeAfterRemoving);
	}
	
	@Test
	public void testToString() {
		assertTrue(gradient.toString().contains(Gradient.class.getName()));
	}
	
	@Test
	public void testSetKnots() {
		int[] x = TestUtils.fillArray(TETRA); 
		int[] y = TestUtils.fillArray(TETRA); 
		byte[] types = { Gradient.RGB | Gradient.SPLINE, Gradient.RGB | Gradient.SPLINE, Gradient.RGB | Gradient.SPLINE, Gradient.RGB | Gradient.SPLINE };
		int offset = 0;//TestUtils.genInteger(TETRA);
		int count = 1;//TestUtils.genIntegerGreaterThanZero(TETRA);

		gradient.setKnots(x, y, types, offset, count);
		assertEquals(1, gradient.getNumKnots());
	}
	
	@Test
	public void testSplitSpan() {
		int n = TestUtils.genInteger(gradient.getNumKnots());
		gradient.splitSpan(n);
		assertTrue(true);
	}
	
//	@Test
	public void testSetKnotPosition() {
		int n = 0;
		int x = 0;
		
		gradient.setKnotPosition(n, x);
		assertTrue(gradient.getKnotPosition(n) > 0);
		
		n = 2;
		gradient.setKnotPosition(n, x);
		assertFalse(false);
	}
	
	@Test
	public void testKnotAt() {
		int n = 0;
		int x = 0;
		gradient.setKnotPosition(n, x);
		assertTrue(gradient.knotAt(x) > 0);
	}
	
	@Test
	public void testRebuild() {
		gradient.rebuild();
		assertTrue(true);
	}
	
	@Test
	public void testRandomize() {
		gradient.randomize();
		assertTrue(true);
	}
	
	@Test
	public void testMutate() {
		float amount = TestUtils.genFloat();
		gradient.mutate(amount);
		assertTrue(true);
	}
	
	@Test
	public void testRandomGradient() {
		Gradient grad = Gradient.randomGradient();
		assertNotNull(grad);
	}
	
	@Test
	public void testEquals() {
		Gradient dest = Gradient.randomGradient();
		assertEquals(0, dest.equals(dest));
	}
}
