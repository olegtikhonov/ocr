/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.edgedetecting.GaussianNoise;
import com.ocrix.image.processing.filters.edgedetecting.Laplacian;

/**
 * Tests MarrHildreth.
 */
public class TestMarrHildreth {
	private static int noise = 5;
	private static Laplacian loG = null;
	private static GaussianNoise gNoise = null;
	private Logger LOG = Logger.getLogger(TestMarrHildreth.class.getName());

	
	@BeforeClass
	public static void setUp() throws Exception {
		loG = new Laplacian();
		gNoise = new GaussianNoise();
	}

	@Test
	public void testMarrHildrethED() throws IOException {
		BufferedImage src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		assertNotNull(src);

		int width = src.getWidth();// current
		int height = src.getHeight();// current

		int[] orig = new int[width * height];

		/* Creates a pixel grabber */
		PixelGrabber grabber = new PixelGrabber(src, 0, 0, width, height, orig,
				0, width);
		try {
			grabber.grabPixels();
		} catch (InterruptedException e) {
			LOG.severe("error: " + CommonUtils.stackToString(e, 2));
		}

		gNoise.init(orig, width, height, (float) noise);
		orig = gNoise.process();

		loG.init(orig, width, height);// , (int) sigmavalue / 10
		loG.process();
		int[] res = loG.getMH();
		Image mis = Toolkit.getDefaultToolkit().createImage(
				new MemoryImageSource(width, height, res, 0, width));
		BufferedImage output = CommonUtils.toBufferedImage(mis);
		assertNotNull(output);
		CommonUtils.saveBufferedImage(output, TO_BE_SAVED + "TestMarrHildreth",
				"jpeg");
	}
}
