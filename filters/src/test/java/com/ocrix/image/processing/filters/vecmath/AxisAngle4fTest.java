/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.vecmath;

import static org.junit.Assert.*;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.TETRA;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link AxisAngle4f} functionality.
 */
public class AxisAngle4fTest {

	private AxisAngle4f angle4f;
	enum INDICES {X, Y, Z, ANGLE};
	
	@Before
	public void setUp(){
		angle4f = new AxisAngle4f();
	}
	
	@Test
	public void testAxisAngle4f() {
		assertNotNull(angle4f);
	}

	@Test
	public void testAxisAngle4fFloatArray() {
		float[] input = TestUtils.generateFloatArray(HEPTAGON);
		angle4f = new AxisAngle4f(input);
		assertNotNull(angle4f);
	}

	@Test
	public void testAxisAngle4fFloatFloatFloatFloat() {
		float[] input = TestUtils.generateFloatArray(TETRA);
		angle4f = new AxisAngle4f(input[INDICES.X.ordinal()], input[INDICES.Y.ordinal()], input[INDICES.Z.ordinal()], input[INDICES.ANGLE.ordinal()]);
		assertNotNull(angle4f);
	}

	@Test
	public void testAxisAngle4fAxisAngle4f() {
		AxisAngle4f original = new AxisAngle4f(TestUtils.generateFloatArray(TETRA));
		angle4f = new AxisAngle4f(original);
		assertNotNull(angle4f);
	}

	@Test
	public void testAxisAngle4fVector3fFloat() {
		float[] input = TestUtils.generateFloatArray(TETRA);
		Vector3f v3f = new Vector3f(TestUtils.generateFloatArray(HEPTAGON));
		angle4f = new AxisAngle4f(v3f, input[INDICES.ANGLE.ordinal()]);
		assertNotNull(angle4f);
	}

	@Test
	public void testSetFloatFloatFloatFloat() {
		float[] input = TestUtils.generateFloatArray(TETRA);
		angle4f.set(input[INDICES.X.ordinal()], input[INDICES.Y.ordinal()], input[INDICES.Z.ordinal()], input[INDICES.ANGLE.ordinal()]);
		float[] result = new float[TETRA]; 
		angle4f.get(result);
		assertTrue(Arrays.equals(input, result));
	}

	@Test
	public void testSetAxisAngle4f() {
		float[] input = TestUtils.generateFloatArray(TETRA);
		Vector3f v3f = new Vector3f(input);
		AxisAngle4f original = new AxisAngle4f(v3f, input[INDICES.ANGLE.ordinal()]);
		angle4f.set(original);
		AxisAngle4f result = new AxisAngle4f();
		angle4f.get(result);
		assertNotNull(result);
	}

	@Test
	public void testGetAxisAngle4f() {
		testSetAxisAngle4f();
	}

	@Test
	public void testGetFloatArray() {
		testSetFloatFloatFloatFloat();
	}

	@Test
	public void testToString() {
		assertTrue(angle4f.toString().contains(AxisAngle4f.class.getName()));
	}

}
