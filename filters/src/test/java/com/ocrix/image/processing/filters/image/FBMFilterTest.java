/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FBM;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.math.Function2D;
import com.ocrix.image.processing.filters.math.RidgedFBM;


/**
 * Tests {@link FBMFilter} functionality.
 */
public class FBMFilterTest {

	private static FBMFilter fbmFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		fbmFilter = new FBMFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(fbmFilter);
	}
	
	
	@Test
	public void testFBMFilter() throws IOException {
		BufferedImage result = fbmFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FBM.to(), "jpeg");
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		fbmFilter.setAmount(amount);
		assertEquals(amount, fbmFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testSetOperation() {
		int operation = TestUtils.genInteger();
		fbmFilter.setOperation(operation);
		assertEquals(operation, fbmFilter.getOperation());
	}
	
	@Test
	public void testSetScale(){ 
		float scale = TestUtils.genFloat() + 1;
		fbmFilter.setScale(scale);
		assertEquals(scale, fbmFilter.getScale(), EPSILON);
	}
	
	@Test
	public void testSetStretch() {
		float stretch = TestUtils.genFloat() + 1;
		fbmFilter.setStretch(stretch);
		assertEquals(stretch, fbmFilter.getStretch(), EPSILON);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		fbmFilter.setAngle(angle);
		assertEquals(angle, fbmFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetOctaves() {
		float octaves = TestUtils.genFloat();
		fbmFilter.setOctaves(octaves);
		assertEquals(octaves, fbmFilter.getOctaves(), EPSILON);
	}
	
	@Test
	public void testSetH() {
		float H = TestUtils.genFloat();
		fbmFilter.setH(H);
		assertEquals(H, fbmFilter.getH(), EPSILON);
	}
	
	@Test
	public void testSetLacunarity() {
		float lacunarity = TestUtils.genFloat();
		fbmFilter.setLacunarity(lacunarity);
		assertEquals(lacunarity, fbmFilter.getLacunarity(), EPSILON);
	}
	
	@Test
	public void testSetGain() {
		float gain = TestUtils.genFloat();
		fbmFilter.setGain(gain);
		assertEquals(gain, fbmFilter.getGain(), EPSILON);
	}
	
	@Test
	public void testSetBias() {
		float bias = TestUtils.genFloat();
		fbmFilter.setBias(bias);
		assertEquals(bias, fbmFilter.getBias(), EPSILON);
	}
	
	@Test
	public void testSetColormaps() {
		Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.PINK.getRGB());
		fbmFilter.setColormap(colormap);
		assertEquals(colormap, fbmFilter.getColormap());
	}
	
	@Test
	public void testSetBasisType() {
		// CELLULAR
		fbmFilter.setBasisType(FBMFilter.BASIS_TYPE.CELLULAR);
		assertNotNull(fbmFilter.getBasis());
		
		// NOISE
		fbmFilter.setBasisType(FBMFilter.BASIS_TYPE.NOISE);
		assertNotNull(fbmFilter.getBasis());

		// RIDGED
		fbmFilter.setBasisType(FBMFilter.BASIS_TYPE.RIDGED);
		assertNotNull(fbmFilter.getBasis());

		// SCNOISE
		fbmFilter.setBasisType(FBMFilter.BASIS_TYPE.SCNOISE);
		assertNotNull(fbmFilter.getBasis());

		// VLNOISE
		fbmFilter.setBasisType(FBMFilter.BASIS_TYPE.VLNOISE);
		assertNotNull(fbmFilter.getBasis());
	}
	
	@Test
	public void testFilterRGB() {
		int x = src.getHeight() / 3; 
		int y = src.getWidth() / 3;
		int rgb = src.getRGB(x, y);
		fbmFilter.filterRGB(x, y, rgb);
		assertTrue(true);
	}
	
	@Test
	public void testFilterRGBColorMapIsNull() {
		fbmFilter.setColormap(null);
		int x = src.getHeight() / 3; 
		int y = src.getWidth() / 3;
		int rgb = src.getRGB(x, y);
		int result = fbmFilter.filterRGB(x, y, rgb);
		System.out.println(result);
		assertTrue(result != 0);
	}
	
	@Test
	public void testSetBasis() {
		Function2D basis = new RidgedFBM();
		fbmFilter.setBasis(basis);
		assertEquals(basis, fbmFilter.getBasis());
	}
	
	@Test
	public void testToString() {
		assertTrue(fbmFilter.toString().contains(FBMFilter.class.getName()));
	}
}
