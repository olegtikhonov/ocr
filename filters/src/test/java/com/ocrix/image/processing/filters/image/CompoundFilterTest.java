/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.COMPOUND;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link CompoundFilter} functionality.
 */
public class CompoundFilterTest {

	private static CompoundFilter compoundFilter = null;
	private static BufferedImage src = null;
	private static BufferedImageOp sharp = null;
	private static BufferedImageOp edge = null;

	@BeforeClass
	public static void setUp() throws Exception {
		float[] edgeKernel = { 0.0f, -1.0f, 0.0f, -1.0f, 4.0f, -1.0f, 0.0f,
				-1.0f, 0.0f };
		float[] sharpKernel = { 0.0f, -1.0f, 0.0f, -1.0f, 5.0f, -1.0f, 0.0f,
				-1.0f, 0.0f };
		edge = new ConvolveOp(new Kernel(3, 3, edgeKernel));
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		sharp = new ConvolveOp(new Kernel(3, 3, sharpKernel));
		compoundFilter = new CompoundFilter(edge, sharp);
	}

	@Test
	public void testCitor() {
		assertNotNull(compoundFilter);
	}

	@Test
	public void testFilter() throws URISyntaxException, IOException {
		BufferedImage result = compoundFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + COMPOUND.to(), "png");
	}
	
	@Test
	public void testToString() {
		assertTrue(compoundFilter.toString().contains(CompoundFilter.class.getName()));
	}
}