/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import static com.ocrix.image.processing.filters.composite.TestParams.FIBONACCI_PRIME;

import java.awt.Graphics;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.util.Random;

import com.ocrix.image.processing.filters.image.FieldWarpFilter.Line;

public class TestUtils {

	public static BufferedImage convertToGrayScale(BufferedImage src) {
		if (src != null) {
			BufferedImage image = new BufferedImage(src.getWidth(),
					src.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
			Graphics g = image.getGraphics();
			g.drawImage(src, 0, 0, null);
			g.dispose();
			return image;
		} else {
			/* Instead of returning null */
			return new BufferedImage(0, 0, 1);
		}
	}

	public static int[] fillArray(int size) {
		Random randGen = new Random();
		int[] x = new int[size];
		for (int i = 0; i < size; i++) {
			x[i] = randGen.nextInt(FIBONACCI_PRIME);
		}
		return x;
	}
	
	public static int[] genIntegerArray(int size, int seed) {
		int[] x = new int[size];
		for (int i = 0; i < size; i++) {
			x[i] = genInteger(seed);
		}
		return x;
	}
	
	/**
	 * Generates array of float numbers.
	 * 
	 * @param size of the array.
	 * 
	 * @return generated array.
	 */
	public static float[] generateFloatArray(int size) {
		Random randGen = new Random();
		float[] x = new float[size];
		for (int i = 0; i < size; i++) {
			x[i] = randGen.nextFloat();
		}
		return x;
	}
	
	/**
	 * Generates a {@link Double}.
	 * 
	 * @return generated double.
	 */
    public static double genDouble() {
        Random generator = new Random();
        return generator.nextDouble();
    }
    
    /**
     * Generated a double array of size N.
     * 
     * @param arraySize a size of the desired generated array.
     * 
     * @return double generated array.
     */
    public static double[] getDoubleArray(int arraySize){
    	Random randGen = new Random();
		double[] x = new double[arraySize];
		for (int i = 0; i < arraySize; i++) {
			x[i] = randGen.nextDouble();
		}
		return x;
    }
    
    
    /**
     * Generates a single Integer.
     * 
     * @return generated integer.
     */
    public static Integer genInteger() {
        Random generator = new Random();
        return Math.abs(generator.nextInt(100) + 1);
    }
    
    public static Integer genInteger(int seed) {
        Random generator = new Random();
        return Math.abs(generator.nextInt(seed));
    }
    
    public static Integer genIntegerGreaterThanZero(int seed) {
        Random generator = new Random();
        
        int result = -1;
        
        while (result < 0) {
			result = Math.abs(generator.nextInt(seed));
			System.out.println(result);
		}
        
        return Math.abs(generator.nextInt(seed));
    }
    
    /**
     * Generates a float number.
     * 
     * @return generated float number.
     */
    public static float genFloat(){
    	Random generator = new Random();
    	return Math.abs(generator.nextFloat());
    }
    
    /**
     * Generates a float number.
     * 
     * @return generated float number.
     */
    public static float genFloat(long seed){
    	Random generator = new Random(seed);
    	return Math.abs(generator.nextFloat() + seed);
    }
    
    /**
     * Generates a float number in range.
     * 
     * @param leftLimit
     * @param rightLimit
     * 
     * @return
     */
    public static float genFloatInRange(int leftLimit, int rightLimit) {
        return leftLimit + (new Random().nextFloat() * (rightLimit - leftLimit));
    }
    
    /**
     * Generates array of {@link Line}s.
     * 
     * @param numOfLines the amount of generated lines.
     * 
     * @return new generated array of lines.
     */
    public static Line[] genLines(int numOfLines, int seed) {
    	Line[] result = new Line[numOfLines];
    	for(int i = 0; i < numOfLines; i++) {
    		int x1 = genInteger(seed);
    		int y1 = genInteger(seed);
    		int x2 = genInteger(seed); 
    		int y2 = genInteger(seed);
    		Line l = new Line(x1, y1, x2, y2);
    		
    		result[i] = l; 
    	}
    	return result;
    }
    
	
	/**
	 * Converts {@link BufferedImage} to 2D array of pixels.
	 * 
	 * @param image to be converted to 2D array.
	 * 
	 * @return 2D array of pixels.
	 */
	public static int[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {

		final byte[] pixels = ((DataBufferByte) image.getRaster()
				.getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		int[][] result = new int[height][width];
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) pixels[pixel] & 0xff) << 24);          // alpha
				argb += ((int) pixels[pixel + 1] & 0xff);              // blue
				argb += (((int) pixels[pixel + 2] & 0xff) << 8);       // green
				argb += (((int) pixels[pixel + 3] & 0xff) << 16);      // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216;                                // 255 alpha
				argb += ((int) pixels[pixel] & 0xff);             // blue
				argb += (((int) pixels[pixel + 1] & 0xff) << 8);  // green
				argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		}

		return result;
	}
	
	/**
	 * Converts {@link BufferedImage} to 2D array of pixels.
	 * 
	 * @param image to be converted to 2D array.
	 * 
	 * @return 2D array of pixels.
	 */
	public static int[][] convertTo2DUsingGetRGB(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		int[][] result = new int[height][width];

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				result[row][col] = image.getRGB(col, row);
			}
		}

		return result;
	}
	

	/**
	 * Converts the source image to 8-bit colour using the default 256-colour
	 * palette. No transparency.
	 * The code <a href="http://www.java2s.com/Code/Java/2D-Graphics-GUI/Providesusefulmethodsforconvertingimagesfromonecolourdepthtoanother.htm">taken from the following.</a>
	 * 
	 * @param src - the source image to convert
	 * @return a copy of the source image with an 8-bit colour depth
	 */
	public static BufferedImage convert8(BufferedImage src) {
		BufferedImage dest = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		ColorConvertOp cco = new ColorConvertOp(src.getColorModel().getColorSpace(), dest.getColorModel().getColorSpace(), null);
		cco.filter(src, dest);
		return dest;
	}
	
	/**
	 * Converts the source image to 4-bit colour using the given colour map. No
	 * transparency.
	 * 
	 * The code taken from:
	 * http://www.java2s.com/Code/Java/2D-Graphics-GUI/Providesusefulmethodsforconvertingimagesfromonecolourdepthtoanother.htm
	 * 
	 * @param src
	 *            the source image to convert
	 * @param cmap
	 *            the colour map, which should contain no more than 16 entries
	 *            The entries are in the form RRGGBB (hex).
	 * @return a copy of the source image with a 4-bit colour depth, with the
	 *         custom colour pallette
	 */
	public static BufferedImage convert4(BufferedImage src, int[] cmap) {
		IndexColorModel icm = new IndexColorModel(4, cmap.length, cmap, 0,
				false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
		BufferedImage dest = new BufferedImage(src.getWidth(), src.getHeight(),
				BufferedImage.TYPE_BYTE_BINARY, icm);
		ColorConvertOp cco = new ColorConvertOp(src.getColorModel()
				.getColorSpace(), dest.getColorModel().getColorSpace(), null);
		cco.filter(src, dest);

		return dest;
	}
	
	/**
	 * Converts the source image to 4-bit colour using the default 16-colour
	 * palette:
	 * <ul>
	 * <li>black</li>
	 * <li>dark red</li>
	 * <li>dark green</li>
	 * <li>dark yellow</li>
	 * <li>dark blue</li>
	 * <li>dark magenta</li>
	 * <li>dark cyan</li>
	 * <li>dark grey</li>
	 * <li>light grey</li>
	 * <li>red</li>
	 * <li>green</li>
	 * <li>yellow</li>
	 * <li>blue</li>
	 * <li>magenta</li>
	 * <li>cyan</li>
	 * <li>white</li>
	 * </ul>
	 * No transparency.
	 * 
	 * @param src
	 *            the source image to convert
	 * @return a copy of the source image with a 4-bit colour depth, with the
	 *         default colour pallette
	 */
	public static BufferedImage convert4(BufferedImage src) {
		int[] cmap = new int[] { 0x000000, 0x800000, 0x008000, 0x808000,
				0x000080, 0x800080, 0x008080, 0x808080, 0xC0C0C0, 0xFF0000,
				0x00FF00, 0xFFFF00, 0x0000FF, 0xFF00FF, 0x00FFFF, 0xFFFFFF };
		return convert4(src, cmap);
	}
	
	/**
	 * Converts 2D array to the 1D.
	 * 
	 * @param toBeConverted
	 * 
	 * @return
	 */
	public static int[] convert2Dto1D(int[][] toBeConverted) {
		if(toBeConverted == null){
			return null;
		}
		
		int[] result = new int[toBeConverted.length * toBeConverted[0].length];
		
		for(int i = 0; i < toBeConverted.length; i++){
			for(int j = 0; j < toBeConverted[0].length; j++) {
				result[i + j] = toBeConverted[i][j];
			}
		}
		
		return result;
	}
	
	/**
	 * Gets a diff counter of two int arrays.
	 * Checks if graphical operation is done.
	 * 
	 * @param src
	 * @param dst
	 * @return
	 */
	public static int diffCounter(int[] src, int[] dst) {
		int resultCounter = 0;
		for(int i = 0; i < src.length; i++) {
			if(dst.length > i) {
				if(src[i] != dst[i]) {
					resultCounter++;
				}
			}
		}
		return resultCounter;
	}
}
