/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;

public class SpectrumColormapTest {
	private SpectrumColormap spectrumColormap;

	@Before
	public void setUp() throws Exception {
		spectrumColormap = new SpectrumColormap();
	}

	@Test
	public void testSpectrumColormap() {
		assertNotNull(spectrumColormap);
	}

	@Test
	public void testGetColor() {
		float specCol = TestUtils.genFloat();
		int result = spectrumColormap.getColor(specCol);
		assertTrue(result != 0);
	}
}
