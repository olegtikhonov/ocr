/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OCTAHEDRON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.ExclusionComposite;
import com.ocrix.image.processing.filters.composite.ExclusionComposite.Context;

/**
 * Tests {@link ExclusionComposite} functionality.
 */
public class ExclusionCompositeTest {
	/* Test's member */
	private static ExclusionComposite exclusionComposite;

	@BeforeClass
	public static void setUp() throws Exception {
		exclusionComposite = new ExclusionComposite(ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(exclusionComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		CompositeContext comp = exclusionComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(
						RenderingHints.KEY_TEXT_LCD_CONTRAST,
						RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VRGB));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		Context context = new Context(ALPHA, srcColorModel, dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(OCTAHEDRON);
		int[] dst = TestUtils.fillArray(OCTAHEDRON);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
		assertTrue(context.toString().contains("Context"));
	}

	@Test
	public void testToString() {
		assertTrue(exclusionComposite.toString().contains(
				ExclusionComposite.class.getName()));
	}

	@Test
	public void testComposeRGBNegative() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		Context context = new Context(ALPHA, srcColorModel, dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(2);
		int[] dst = TestUtils.fillArray(1);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
