/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.*;
import java.awt.Color;
import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests {@link SplineColormap} functionality.
 */
public class SplineColormapTest {
    private static final int ZERO = 0;
	private SplineColormap sc;
	
	@Before
	public void setUp() throws Exception {
		sc = new SplineColormap();
	}

	@Test
	public void testSplineColormap() {
		assertNotNull(sc);
	}

	@Test
	public void testSplineColormapIntArrayIntArray() {
		//int[] xKnots, int[] yKnots
		int[] xKnots = TestUtils.fillArray(TestParams.DIME);
		int[] yKnots = TestUtils.fillArray(TestParams.DIME);
		sc = new SplineColormap(xKnots, yKnots);
		assertNotNull(sc);
	}

	@Test
	public void testSetKnot() {
		sc.setKnot(ZERO, Color.BLACK.getRGB());
		assertEquals(Color.BLACK.getRGB(), sc.getKnot(ZERO));
	}

	@Test
	public void testGetKnot() {
		testSetKnot();
	}

	@Test
	public void testAddKnot() {
		sc.addKnot(ZERO, Color.YELLOW.getRGB());
		assertTrue(true);
	}

	@Test
	public void testRemoveKnot() {
		sc.removeKnot(ZERO);
		assertTrue(true);
	}

	@Test
	public void testSetKnotPosition() {
		sc.setKnotPosition(ZERO, ZERO + 1);
		assertTrue(true);
	}
	
	@Test
	public void testNumKnotsGreaterThanFour() {
		try {
			Field numKnots = sc.getClass().getDeclaredField("numKnots");
			numKnots.setAccessible(true);
			numKnots.set(sc, TestParams.DIME / 2);
			
			sc.removeKnot(TestParams.TETRA);
			assertTrue(true);
			
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
