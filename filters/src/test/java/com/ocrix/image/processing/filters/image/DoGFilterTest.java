/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.DOG;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link DoGFilter} functionality.
 */
public class DoGFilterTest {
	/* Test members */
	private static DoGFilter doGFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		doGFilter = new DoGFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(doGFilter);
	}
	
	@Test
	public void testDoGFilter()throws Exception {
		doGFilter.setRadius1(1.0f);
		doGFilter.setRadius2(4.8908765f);
		doGFilter.setNormalize(true);
		doGFilter.setInvert(true);
		BufferedImage result = doGFilter.filter(src, ImageIO.read(new File(PREFIXPATH + COW)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DOG.to(), "jpg");
		
		doGFilter.setInvert(false);
	}
	
	@Test
	public void testDoGFilterDontInvert() throws Exception{
		doGFilter.setNormalize(true);
		doGFilter.setInvert(false);
		BufferedImage result = doGFilter.filter(src, ImageIO.read(new File(PREFIXPATH + COW)));
		assertNotNull(result);
	}
	
	@Test
	public void testGetRadius1(){
		float radius1 = TestUtils.genFloat();
		doGFilter.setRadius1(radius1);
		assertEquals(radius1, doGFilter.getRadius1(), EPSILON);
	}
	
	@Test
	public void testSetRadius1() {
		testGetRadius1();
	}
	
	@Test
	public void testGetRadius2(){
		float radius2 = TestUtils.genFloat();
		doGFilter.setRadius2(radius2);
		assertEquals(radius2, doGFilter.getRadius2(), EPSILON);
	}
	
	@Test
	public void testSetRadius2() {
		testGetRadius2();
	}
	
	@Test
	public void testGetNormalize(){
		doGFilter.setNormalize(true);
		assertTrue(doGFilter.getNormalize());
		
		doGFilter.setNormalize(false);
		assertFalse(doGFilter.getNormalize());
	}
	
	@Test
	public void testGetInvert() {
		doGFilter.setInvert(true);
		assertTrue(doGFilter.getInvert());
		
		doGFilter.setInvert(false);
		assertFalse(doGFilter.getInvert());
	}
	
	@Test
	public void testToString() {
		assertTrue(doGFilter.toString().contains(DoGFilter.class.getName()));
	}
}
