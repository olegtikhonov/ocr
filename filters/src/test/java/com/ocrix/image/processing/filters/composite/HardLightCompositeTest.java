/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OCTAHEDRON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.HardLightComposite;
import com.ocrix.image.processing.filters.composite.HardLightComposite.Context;

/**
 * Tests an {@link HardLightComposite} functionality.
 */
public class HardLightCompositeTest {
	/* Test's member */
	private static HardLightComposite hardLightComposite;

	@BeforeClass
	public static void setUp() throws Exception {
		hardLightComposite = new HardLightComposite(ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(hardLightComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		CompositeContext comp = hardLightComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(
						RenderingHints.KEY_ALPHA_INTERPOLATION,
						RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		Context context = new Context(ALPHA, srcColorModel, dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(OCTAHEDRON);
		int[] dst = TestUtils.fillArray(OCTAHEDRON);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);

		assertTrue(context.toString().contains("Context"));
	}

	@Test
	public void testComposeRGBElseCases() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		Context context = new Context(ALPHA, srcColorModel, dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(OCTAHEDRON);
		/* Case 1: sr = src[i] && sr < 127 */
		src[0] = 126;
		/* Case 2: sg < 127 && src[i + 1] */
		src[1] = 126;
		/* Case 3: sb < 127 && sb = src[i + 2] */
		src[2] = 126;

		int[] dst = TestUtils.fillArray(OCTAHEDRON);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(hardLightComposite.toString().contains(
				HardLightComposite.class.getName()));
	}

	@Test
	public void testComposeRGBNegative() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		Context context = new Context(ALPHA, srcColorModel, dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(2);
		int[] dst = TestUtils.fillArray(1);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
