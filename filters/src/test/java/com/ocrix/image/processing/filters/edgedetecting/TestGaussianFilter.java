/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.edgedetecting;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests {@link GaussianFilter} functionality.
 */
public class TestGaussianFilter {

	private GaussianFilter gaussianFilter;
	
	@Before
	public void setUp(){
		gaussianFilter = new GaussianFilter();
	}
	
	@Test
	public void testGaussianFilter() {
		assertNotNull(gaussianFilter);
	}

	@Test
	public void testInitIntArrayIntIntIntInt() {
		int original[] = TestUtils.fillArray(TestParams.TETRA);
		int sigmaIn = TestUtils.genInteger();
		int tempSize = TestUtils.genInteger();
		int widthIn = TestUtils.genInteger();
		int heightIn = TestUtils.genInteger();
		gaussianFilter.init(original, Math.abs(sigmaIn), Math.abs(tempSize), Math.abs(widthIn), Math.abs(heightIn));
		assertTrue(true);
	}

	@Test
	public void testInitIntArrayDoubleIntIntInt() {
		int original[] = TestUtils.fillArray(TestParams.TETRA);
		double sigmaIn = TestUtils.genDouble();
		int tempSize = TestUtils.genInteger();
		int widthIn = TestUtils.genInteger();
		int heightIn = TestUtils.genInteger();
		gaussianFilter.init(original, Math.abs(sigmaIn), Math.abs(tempSize), Math.abs(widthIn), Math.abs(heightIn));
		assertTrue(true);
	}

	@Test
	public void testGenerateTemplate() {
		testInitIntArrayDoubleIntIntInt();
		gaussianFilter.generateTemplate();
		assertTrue(true);
	}

	@Test
	public void testProcess() {
		testInitIntArrayDoubleIntIntInt();
		int[] result = gaussianFilter.process();
		assertNotNull(result);
	}

	@Test
	public void testGetProgress() {
		testProcess();
		assertTrue(gaussianFilter.getProgress() >= 0);
	}
}
