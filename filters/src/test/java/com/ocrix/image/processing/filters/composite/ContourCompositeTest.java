/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.composite.ContourComposite;
import com.ocrix.image.processing.filters.composite.ContourCompositeContext;

/**
 * Tests a {@link ContourComposite} functionality.
 */
public class ContourCompositeTest {

	private static ContourComposite contourComposite;

	@BeforeClass
	public static void setUp() throws Exception {
		contourComposite = new ContourComposite(BufferedImage.TYPE_INT_RGB);
	}

	@Test
	public void testCitor() {
		assertNotNull(contourComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		CompositeContext comp = contourComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(
						RenderingHints.KEY_FRACTIONALMETRICS,
						RenderingHints.VALUE_FRACTIONALMETRICS_ON));
		assertNotNull(comp);
	}

	@Test
	public void testCompose() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ContourCompositeContext context = new ContourCompositeContext(
				TestParams.UNITY, srcColorModel, dstColorModel);
		assertNotNull(context);

		BufferedImage bi = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		byte[] pixels = CommonUtils
				.generatePixels(bi.getWidth(), bi.getHeight(),
						new Rectangle2D.Float(-2.0f, -1.2f, 3.2f, 2.4f));
		DataBufferByte dbuf = new DataBufferByte(pixels, bi.getWidth()
				* bi.getHeight(), 0);

		int bitMasks[] = new int[] { (byte) 0xf };

		SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(
				DataBuffer.TYPE_BYTE, bi.getWidth(), bi.getHeight(), bitMasks);

		Raster srcRast = Raster.createRaster(sm, dbuf, new Point(0, 0));

		Raster dstIn = Raster.createRaster(sm, dbuf, new Point(0, 0));

		WritableRaster dstOut = Raster
				.createWritableRaster(sm, new Point(0, 0));

		context.compose(srcRast, dstIn, dstOut);

		assertTrue(context.toString().contains(TestParams.VP_COUNTOR));
		context.dispose();
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertNotNull(contourComposite.toString());
	}

	@Test
	public void testHashCode() {
		int hashCode = HashCodeBuilder.reflectionHashCode(contourComposite,
				false);
		assertEquals(hashCode, contourComposite.hashCode());
	}

	@Test
	public void testEquals() {
		boolean result = contourComposite.equals(contourComposite);
		assertTrue(result);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
