/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Composite;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.Operations;
import com.ocrix.image.processing.filters.composite.ExclusionComposite;
import com.ocrix.image.processing.filters.composite.MiscComposite;
import com.ocrix.image.processing.filters.composite.NegationComposite;

/**
 * Tests a {@link MiscComposite} functionality.
 */
public class MiscCompositeTest {

	private static Composite composite;
	private static MiscComposite comp = null;

	@BeforeClass
	public static void setUp() throws Exception {
		composite = MiscComposite.getInstance(Operations.DODGE.ordinal(),
				TestParams.ALPHA);

		Constructor<MiscComposite> instOMisc = MiscComposite.class
				.getDeclaredConstructor(Integer.TYPE, Float.TYPE);

		instOMisc.setAccessible(true);

		comp = instOMisc.newInstance(9, 0.9f);
	}

	@Test
	public void testPrivateConstructorInt() {
		try {
			Constructor<MiscComposite> instOMisc = MiscComposite.class
					.getDeclaredConstructor(Integer.TYPE);
			instOMisc.setAccessible(true);

			MiscComposite comp = instOMisc.newInstance(9);
			assertNotNull(comp);
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
	}

	@Test
	public void testPrivateConstructorIntFloat() {

		try {
			Constructor<MiscComposite> instOMisc = MiscComposite.class
					.getDeclaredConstructor(Integer.TYPE, Float.TYPE);
			instOMisc.setAccessible(true);

			MiscComposite comp = instOMisc.newInstance(9, 0.9f);
			assertNotNull(comp);
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
	}

	@Test
	public void testCitor() {
		assertNotNull(composite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));

		comp.createContext(srcColorModel, srcColorModel, new RenderingHints(
				RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BICUBIC));
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceBlend() {
		composite = MiscComposite.getInstance(MiscComposite.BLEND,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceAdd() {
		composite = MiscComposite.getInstance(MiscComposite.ADD,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceSubtract() {
		composite = MiscComposite.getInstance(MiscComposite.SUBTRACT,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceDifference() {
		composite = MiscComposite.getInstance(MiscComposite.DIFFERENCE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceMultiply() {
		composite = MiscComposite.getInstance(MiscComposite.MULTIPLY,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceDarken() {
		composite = MiscComposite.getInstance(MiscComposite.DARKEN,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceBurn() {
		composite = MiscComposite.getInstance(MiscComposite.BURN,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceColorburn() {
		composite = MiscComposite.getInstance(MiscComposite.COLOR_BURN,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceScreen() {
		composite = MiscComposite.getInstance(MiscComposite.SCREEN,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceLighten() {
		composite = MiscComposite.getInstance(MiscComposite.LIGHTEN,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceDodge() {
		composite = MiscComposite.getInstance(MiscComposite.DODGE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceColordodge() {
		composite = MiscComposite.getInstance(MiscComposite.COLOR_DODGE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceHue() {
		composite = MiscComposite.getInstance(MiscComposite.HUE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceSaturation() {
		composite = MiscComposite.getInstance(MiscComposite.SATURATION,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceValue() {
		composite = MiscComposite.getInstance(MiscComposite.VALUE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceColor() {
		composite = MiscComposite.getInstance(MiscComposite.COLOR,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceOverlay() {
		composite = MiscComposite.getInstance(MiscComposite.OVERLAY,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceSoftLight() {
		composite = MiscComposite.getInstance(MiscComposite.SOFT_LIGHT,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceHardLight() {
		composite = MiscComposite.getInstance(MiscComposite.HARD_LIGHT,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstancePinLight() {
		composite = MiscComposite.getInstance(MiscComposite.PIN_LIGHT,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceExclusion() {
		composite = MiscComposite.getInstance(MiscComposite.EXCLUSION,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceNegation() {
		composite = MiscComposite.getInstance(MiscComposite.NEGATION,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceAverage() {
		composite = MiscComposite.getInstance(MiscComposite.AVERAGE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceStencil() {
		composite = MiscComposite.getInstance(MiscComposite.STENCIL,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testGetInstanceSilhouette() {
		composite = MiscComposite.getInstance(MiscComposite.SILHOUETTE,
				TestParams.ALPHA);
		assertNotNull(composite);
	}

	@Test
	public void testToString() {
		ExclusionComposite composite = (ExclusionComposite) MiscComposite
				.getInstance(MiscComposite.EXCLUSION, TestParams.ALPHA);
		assertTrue(composite.toString().contains(
				ExclusionComposite.class.getName()));

		assertTrue(comp.toString().contains(MiscComposite.class.getName()));
	}

	@Test
	public void testGetAlpha() {
		try {

			Method method = MiscComposite.class.getDeclaredMethod("getAlpha");
			method.setAccessible(true);
			float value = (Float) method.invoke(comp);
			assertEquals(0.9, value, 0.0008);

		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
	}

	@Test
	public void testGetRule() {
		try {

			Method method = MiscComposite.class.getDeclaredMethod("getRule");
			method.setAccessible(true);
			float value = (Integer) method.invoke(comp);
			assertEquals(9, value, 0.009);

		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
	}

	@Test
	public void testHashCode() {
		int value = comp.hashCode();
		assertTrue(value != 0);
	}

	@Test
	public void testEquals() {
		assertTrue(comp.equals(comp));

		NegationComposite mc = (NegationComposite) MiscComposite.getInstance(
				21, TestParams.ALPHA);

		assertFalse(mc.equals(comp));
	}

	@Test
	public void testGetInstanceMiscComp() {
		MiscComposite mc = (MiscComposite) MiscComposite.getInstance(25,
				TestParams.ALPHA);
		assertNotNull(mc);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
