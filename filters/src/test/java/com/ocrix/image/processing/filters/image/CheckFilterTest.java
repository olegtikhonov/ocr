/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CHECK;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link CheckFilter} functionality
 */
public class CheckFilterTest {
	/* Test' members */
	private static CheckFilter checkFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		checkFilter = new CheckFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(checkFilter);
	}

	@Test
	public void testFilter() {
		checkFilter.setAngle(ALPHA);
		checkFilter.setBackground(TestUtils.genInteger(RONNA));
		checkFilter.setForeground(TestUtils.genInteger(RONNA));
		checkFilter.setFuzziness(TestUtils.genInteger(RONNA));
		checkFilter.setXScale(TestUtils.genInteger(RONNA));
		checkFilter.setYScale(TestUtils.genInteger(RONNA));
		BufferedImage result = checkFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CHECK.to(),
				"tiff");
	}

	@Test
	public void testGetForeground() {
		int foreground = TestUtils.genInteger();
		checkFilter.setForeground(foreground);
		assertEquals(foreground, checkFilter.getForeground());
	}

	@Test
	public void testSetForeground() {
		testGetForeground();
	}

	@Test
	public void testGetBackground() {
		int background = TestUtils.genInteger();
		checkFilter.setBackground(background);
		assertEquals(background, checkFilter.getBackground());
	}

	@Test
	public void testSetBackground() {
		testGetBackground();
	}

	@Test
	public void testSetXScale() {
		int xScale = TestUtils.genInteger();
		checkFilter.setXScale(xScale);
		assertEquals(xScale, checkFilter.getXScale());
	}

	@Test
	public void testGetXScale() {
		testSetXScale();
	}

	@Test
	public void testSetYScale() {
		int yScale = TestUtils.genInteger();
		checkFilter.setYScale(yScale);
		assertEquals(yScale, checkFilter.getYScale());
	}
	
	@Test
	public void testGetYScale() {
		testSetYScale();
	}
	
	@Test
	public void testGetFuzziness() {
		int fuzziness = TestUtils.genInteger();
		checkFilter.setFuzziness(fuzziness);
		assertEquals(fuzziness, checkFilter.getFuzziness());
	}
	
	@Test
	public void testSetFuzziness() {
		testGetFuzziness();
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat(360);
		checkFilter.setAngle(angle);
		assertEquals(angle, checkFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testGetAngle() {
		testSetAngle();
	}
	
	@Test
	public void testToString() {
		assertTrue(checkFilter.toString().contains(CheckFilter.class.getName()));
	}
}
