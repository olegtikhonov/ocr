/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.Raster;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.Validator;

/**
 * Tests {@link Validator} functionality.
 */
public class TestValidator {

	private static final int DEFAULT_SIZE = 5;
	private static final float FLOAT = 0.05f;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateColorModelColorModel() {
		ColorModel cm = null;
		Validator.validateColorModel(cm);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateColorModelColorModelArray() {
		ColorModel[] cm = new ColorModel[DEFAULT_SIZE];
		Validator.validateColorModel(cm);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateBufferedImageBufferedImage() {
		BufferedImage image = null;
		Validator.validateBufferedImage(image);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateBufferedImageBufferedImageArray() {
		BufferedImage[] images = new BufferedImage[DEFAULT_SIZE];
		Validator.validateBufferedImage(images);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateSize() {
		BufferedImage src = null;
		BufferedImage dest = null;
		Validator.validateSize(src, dest);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidatePoint2DPoint2D() {
		Point2D srcPoint2D = null;
		Validator.validatePoint2D(srcPoint2D);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidatePoint2DPoint2DArray() {
		Point2D[] srcPoint2D = new Point2D[DEFAULT_SIZE];
		Validator.validatePoint2D(srcPoint2D);
	}

	@Test
	public void testValidateAlpha() {
		Validator.validateAlpha(0.03f);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateAlphaOutOfRange() {
		Validator.validateAlpha(-0.03f);
		assertTrue(true);
	}

	@Test
	public void testValidateRule() {
		Validator.validateRule(DEFAULT_SIZE, 1, 10);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateRasterRaster() {
		Raster raster = null;
		Validator.validateRaster(raster);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateRasterRasterArray() {
		Raster[] raster = new Raster[DEFAULT_SIZE];
		Validator.validateRaster(raster);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateIntArrayEmptiness() {
		Validator.validateIntArrayEmptiness(null);
	}

	@Test
	public void testValidateStringArray() {
		String[] array = null;
		Validator.validateStringArray(array);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateIntPositivenessIntNeg() {
		Validator.validateIntPositiveness(~DEFAULT_SIZE);
	}

	@Test
	public void testValidateIntPositivenessInt() {
		Validator.validateIntPositiveness(DEFAULT_SIZE);
		assertTrue(true);
	}

	@Test
	public void testValidateIntInRange() {
		Validator.validateIntInRange(DEFAULT_SIZE, 0, Integer.MAX_VALUE);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateIntInRangeNeg() {
		Validator.validateIntInRange(~DEFAULT_SIZE, 0, Integer.MAX_VALUE);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateIntPositivenessIntArray() {
		int[] values = new int[DEFAULT_SIZE];
		values[0] = ~DEFAULT_SIZE;
		Validator.validateIntPositiveness(values);
	}

	@Test
	public void testValidateFloatPositivenessFloat() {
		Validator.validateFloatPositiveness(FLOAT);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateFloatPositivenessFloatNet() {
		Validator.validateFloatPositiveness(-FLOAT);
	}

	@Test
	public void testValidateFloatInRange() {
		Validator.validateFloatInRange(FLOAT, 0, Float.MAX_VALUE);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testValidateFloatInRangeNeg() {
		Validator.validateFloatInRange(-FLOAT, 0, Float.MAX_VALUE);
	}

	// @Test
	public void testValidateFloatPositivenessFloatArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateIntNotZeroInt() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateIntNotZeroIntArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateRectangle() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateDoubleDouble() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateDoubleDoubleArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateComposite() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateAffineTransform() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateBufferedImageOpBufferedImageOp() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateBufferedImageOpBufferedImageOpArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateCurve() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateByteArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateImageImage() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateImageImageArray() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateImageProducer() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateComponent() {
		fail("Not yet implemented");
	}

	// @Test
	public void testValidateGraphics() {
		fail("Not yet implemented");
	}

}
