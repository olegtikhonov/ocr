/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Canvas;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class ImageUtilsTest {

	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCreateImage(){
		BufferedImage result = ImageUtils.createImage(src.getSource());
		assertNotNull(result);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testCreateImageNullArg(){
		BufferedImage result = ImageUtils.createImage(null);
		assertNull(result);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testConvertImageToARGBNullArg(){
		ImageUtils.convertImageToARGB(null);
	}
	
	
	@Test
	public void testConvertImageToARGB(){
		BufferedImage result = ImageUtils.convertImageToARGB(src);
		assertNotNull(result);
	}
	
	
	@Test
	public void testGetSubimage(){
		BufferedImage result = ImageUtils.getSubimage(src, 0, 0, src.getWidth() / 2, src.getHeight() / 2);
		assertNotNull(result);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testGetSubimageImageNull(){
		BufferedImage result = ImageUtils.getSubimage(null, 0, 0, src.getWidth() / 2, src.getHeight() / 2);
		assertNull(result);
	}
	
	
	@Test
	public void testPaintCheckedBackground(){
		Canvas canvas = new Canvas();
		canvas.setBounds(0, 0, src.getWidth(), src.getHeight());
		ImageUtils.paintCheckedBackground(canvas, src.getGraphics(), 0, 0, src.getWidth(), src.getHeight());
		CommonUtils.saveBufferedImage(src, TO_BE_SAVED + "ImageUtils", "bmp");
	}
	
	
	@Test
	public void testGetSelectedBounds(){
		Rectangle bounds = ImageUtils.getSelectedBounds(src);
		assertTrue(Double.compare(0, bounds.getX()) == 0);
		assertTrue(Double.compare(0, bounds.getY()) == 0);
		assertTrue(Double.compare(src.getWidth(), bounds.getWidth()) == 0);
		assertTrue(Double.compare(src.getHeight(), bounds.getHeight()) == 0);
	}
	
	
	@Test
	public void testComposeThroughMask(){
		BufferedImage dst = new BufferedImage(src.getWidth(), src.getHeight(),src.getType());
		ImageUtils.composeThroughMask(src.getRaster(), dst.getRaster(), src.getRaster());
	}
	
	@Test
	public void testGetRGB(){
		int[] pixels = new int[src.getWidth() * src.getHeight()];
		int[] pixs = ImageUtils.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), pixels);
		assertEquals((src.getWidth() * src.getHeight()), pixs.length);
	}
	
	@Test
	public void testSetRGB(){
		int[] pixels = new int[src.getWidth() * src.getHeight()];//TestUtils.fillArray(QUAD);
		ImageUtils.setRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), pixels);
		int[] pixs = ImageUtils.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), pixels);
		assertEquals((src.getWidth() * src.getHeight()), pixs.length);
	}
	
	@Test
	public void testCloneImage() throws Exception{
		BufferedImage image = ImageIO.read(new File(PREFIXPATH + TUX));
		BufferedImage result = ImageUtils.cloneImage(image);
		assertNotNull(result);
	}
}
