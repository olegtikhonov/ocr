/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.PINCH;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PinchFilter} functionality.
 */
public class PinchFilterTest {
	private PinchFilter pinchFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		pinchFilter = new PinchFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(pinchFilter);
	}
	
	@Test
	public void testPinchFilter(){
		pinchFilter.setRadius(ANGEL_NUMBER);
		BufferedImage result = pinchFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + PINCH.to(), "jpeg");
	}
	
	@Test
	public void testPinchFilterRadiusIsZero(){
		pinchFilter.setRadius(0);
		BufferedImage result = pinchFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testsetAngle() {
		float angle = TestUtils.genFloat();
		pinchFilter.setAngle(angle);
		assertEquals(angle, pinchFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		pinchFilter.setCentreX(centreX);
		assertEquals(centreX, pinchFilter.getCentreX(), EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		pinchFilter.setCentreY(centreY);
		assertEquals(centreY, pinchFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float(src.getHeight() / 2, src.getWidth() / 2);
		pinchFilter.setCentre(centre);
		assertEquals(centre, pinchFilter.getCentre());
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat();
		pinchFilter.setRadius(radius);
		assertEquals(radius, pinchFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		pinchFilter.setAmount(amount);
		assertEquals(amount, pinchFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(pinchFilter.toString().contains(PinchFilter.class.getSimpleName()));
	}
}
