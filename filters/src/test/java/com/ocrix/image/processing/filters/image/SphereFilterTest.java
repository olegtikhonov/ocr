package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


public class SphereFilterTest {
	private SphereFilter sphereFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		sphereFilter = new SphereFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(sphereFilter);
	}
	
	@Test
	public void testShpereFilter(){
		BufferedImage result = sphereFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "SphereFilter", "jpeg");
	}
	
	@Test
	public void testSetRefractionIndex() {
		float refractionIndex = TestUtils.genFloat();
		sphereFilter.setRefractionIndex(refractionIndex);
		assertEquals(refractionIndex, sphereFilter.getRefractionIndex(), TestParams.EPSILON);
	}
	
	@Test
	public void testGetRefractionIndex() {
		testSetRefractionIndex();
	}
	
	
	@Test
	public void testSetRadius() {
		float r = TestUtils.genFloat();
		sphereFilter.setRadius(r);
		assertEquals(r, sphereFilter.getRadius(), TestParams.EPSILON);
	}
	
	@Test
	public void testGetRadius() {
		testSetRadius();
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		sphereFilter.setCentreX(centreX);
		assertEquals(centreX, sphereFilter.getCentreX(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		sphereFilter.setCentreY(centreY);
		assertEquals(centreY, sphereFilter.getCentreY(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Double(TestUtils.genDouble(), TestUtils.genDouble());
		sphereFilter.setCentre(centre);
		assertEquals(centre.getX(), sphereFilter.getCentre().getX(), TestParams.EPSILON);
		assertEquals(centre.getY(), sphereFilter.getCentre().getY(), TestParams.EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(sphereFilter.toString().contains(SphereFilter.class.getSimpleName()));
	}
}
