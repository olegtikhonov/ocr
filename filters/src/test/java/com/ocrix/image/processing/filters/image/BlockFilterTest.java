/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.BLOCK;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests a {@link BlockFilter} functionality.
 */
public class BlockFilterTest {

	private static BlockFilter blockFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dest = null;

	@BeforeClass
	public static void setUp() throws Exception {
		blockFilter = new BlockFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dest = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitorNoParams() {
		assertNotNull(blockFilter);
	}

	@Test
	public void testCitorParam() {
		blockFilter = new BlockFilter(QUAD);
		assertNotNull(blockFilter);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorNegParam() {
		blockFilter = new BlockFilter(~QUAD + 1);
		assertTrue(false);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBlockSizeNegParam() {
		blockFilter.setBlockSize(~QUAD + 1);
		assertTrue(false);
	}

	@Test
	public void testSetBlockSize() {
		blockFilter.setBlockSize(QUAD);
		assertEquals(QUAD, blockFilter.getBlockSize());
	}

	@Test
	public void testFilter() {
		blockFilter.setBlockSize(src.getHeight());
		BufferedImage scaledImage = CommonUtils.resizeImage(src, dest.getWidth(), dest.getHeight(), dest.getType());
		BufferedImage result = blockFilter.filter(dest, scaledImage);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + BLOCK.to(), "tiff");
	}
	
	@Test
	public void testFilterDstIsNull() {
		BufferedImage result = blockFilter.filter(dest, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED +  BLOCK.to(), "jpeg");
	}
	
	@Test
	public void testToString() {
		assertTrue(blockFilter.toString().contains(BlockFilter.class.getName()));
	}
}
