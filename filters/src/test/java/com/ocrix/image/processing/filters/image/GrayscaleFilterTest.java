/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GRAYSCALE;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link GrayscaleFilter} functionality.
 */
public class GrayscaleFilterTest {

	private static GrayscaleFilter grayscaleFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		grayscaleFilter = new GrayscaleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(grayscaleFilter);
	}

	
	@Test
	public void testGrayScaleFilter(){
		BufferedImage result = grayscaleFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GRAYSCALE.to(), "png");
	}
	
	@Test
	public void testToString(){
		assertTrue(grayscaleFilter.toString().contains(GrayscaleFilter.class.getName()));
	}
	
	@Test
	public void testFilterRGB() {
		int x = src.getHeight() / 3; 
		int y = src.getWidth() / 4;
		int rgb = src.getRGB(x, y);
		int result = grayscaleFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
}
