/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.DILATE;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link DilateFilter} functionality.
 */
public class DilateFilterTest {

	private static DilateFilter dilateFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		dilateFilter = new DilateFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(dilateFilter);
	}
	
	@Test
	public void testDilateFilter() throws Exception{
		dilateFilter.setThreshold(1);
		dilateFilter.setIterations(1);
		dilateFilter.setColormap(new LinearColormap(Color.GREEN.getRGB(), Color.PINK.getRGB()));
		BufferedImage result = dilateFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DILATE.to(), "png");
	}
	
	
	@Test
	public void testGetThreshold() {
		int threshold = TestUtils.genInteger();
		dilateFilter.setThreshold(threshold);
		assertEquals(threshold, dilateFilter.getThreshold());
	}
	
	@Test
	public void testToString() {
		assertTrue(dilateFilter.toString().contains(DilateFilter.class.getName()));
	}
}
