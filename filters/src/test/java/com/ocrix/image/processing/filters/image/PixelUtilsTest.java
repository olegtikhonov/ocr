/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import org.junit.Test;

/**
 * Tests {@link PixelUtils} functionality.
 */
public class PixelUtilsTest {

	private static final int UNUNOCTIUM = 118;
	private static final float ONE_HALF = 0.5f;
	private static final int URANIUM_MASS = 234;
	private static final int TWO = 2;
	private static final int FOUR = -4;
	private static final int MORE_THEN_255 = 256;

	@Test
	public void testClamp() {
		// Case 1: c < 0
		int result = PixelUtils.clamp(FOUR);
		assertEquals(0, result);

		// Case 2: c > 255
		result = PixelUtils.clamp(MORE_THEN_255);
		assertEquals(255, result);

		// Case 3: c is between the limits.
		result = PixelUtils.clamp(128);
		assertEquals(128, result);
	}

	@Test
	public void testInterpolate() {
		int v1 = TWO;
		int v2 = URANIUM_MASS;
		float f = ONE_HALF;
		int result = PixelUtils.interpolate(v1, v2, f);
		assertEquals(UNUNOCTIUM, result);
	}

	@Test
	public void testBrightness() {
		int result = PixelUtils.brightness(Color.GREEN.getRGB());
		assertEquals(85, result);
	}

	@Test
	public void testNearColors() {
		int rgb1 = Color.YELLOW.getRGB();
		int rgb2 = Color.GREEN.getRGB();
		int tolerance = 255;
		boolean isNear = PixelUtils.nearColors(rgb1, rgb2, tolerance);
		System.out.println(isNear);
	}

	@Test
	public void testCombinePixelsIntIntInt() {
		int rgb1 = Color.YELLOW.getRGB();
		int rgb2 = Color.GREEN.getRGB();
		int op = PixelUtils.SUBTRACT;
		int result = PixelUtils.combinePixels(rgb1, rgb2, op);
		assertTrue(result != 0);
	}

	@Test
	public void testCombinePixelsIntIntIntIntInt() {
		int rgb1 = Color.YELLOW.getRGB();
		int rgb2 = Color.GREEN.getRGB();
		int op = PixelUtils.EXCHANGE;
		int extraAlpha = 123;
		int result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 2: op = REPLACE
		op = PixelUtils.REPLACE;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 3: op = DIFFERENCE
		op = PixelUtils.DIFFERENCE;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 4: op = MULTIPLY
		op = PixelUtils.MULTIPLY;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 5: op = DISSOLVE
		op = PixelUtils.DISSOLVE;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 6: op = COLOR
		op = PixelUtils.COLOR;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 7: op = HUE
		op = PixelUtils.HUE;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);

		// Case 8: op = SATURATION
		op = PixelUtils.SATURATION;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 9: op = VALUE
		op = PixelUtils.VALUE;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 10: op = SCREEN
		op = PixelUtils.SCREEN;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 11: op = OVERLAY
		op = PixelUtils.OVERLAY;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 12: op = CLEAR
		op = PixelUtils.CLEAR;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 13: op = DST_IN
		op = PixelUtils.DST_IN;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 14: op = ALPHA
		op = PixelUtils.ALPHA;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
		
		// Case 15: op = ALPHA_TO_GRAY
		op = PixelUtils.ALPHA_TO_GRAY;
		result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha);
		assertTrue(result != 0);
	}

	@Test
	public void testCombinePixelsIntIntIntInt() {
		int rgb1 = Color.YELLOW.getRGB();
		int rgb2 = Color.GREEN.getRGB();
		int op = PixelUtils.EXCHANGE;
		int extraAlpha = 123;
		int channelMask = 11;
		int result = PixelUtils.combinePixels(rgb1, rgb2, op, extraAlpha,
				channelMask);
		assertTrue(result != 0);
	}
}
