/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.MAP;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.math.CellularFunction2D;
import com.ocrix.image.processing.filters.math.Function2D;
import com.ocrix.image.processing.filters.math.ImageFunction2D;


/**
 * Tests {@link MapFilter} functionality.
 */
public class MapFilterTest {

	private MapFilter mapFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		mapFilter = new MapFilter();
		mapFilter.setXMapFunction(new CellularFilter());
		mapFilter.setYMapFunction(new CellularFilter());
	}

	@Test
	public void testTransformInverse() {
		mapFilter.transformInverse(0, 0, src.getRaster().getPixel(0, 0, new float[10]));
		assertTrue(true);
	}

	@Test
	public void testMapFilter() {
		assertNotNull(mapFilter);
	}

	@Test
	public void testSetXMapFunction() {
		Function2D xMapFunction = new CellularFunction2D();
		mapFilter.setXMapFunction(xMapFunction);
		assertEquals(xMapFunction, mapFilter.getXMapFunction());
	}

	@Test
	public void testGetXMapFunction() {
		testSetXMapFunction();
	}

	@Test
	public void testSetYMapFunction() {
		Function2D yMapFunction = new ImageFunction2D(src);
		mapFilter.setYMapFunction(yMapFunction);
		assertEquals(yMapFunction, mapFilter.getYMapFunction());
	}

	@Test
	public void testGetYMapFunction() {
		testSetYMapFunction();
	}

	@Test
	public void testToString() {
		assertTrue(mapFilter.toString().contains(MapFilter.class.getName()));
	}
	
	
	@Test
	public void testFiler() {
		BufferedImage result = mapFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MAP.to(), "png");
	}
}
