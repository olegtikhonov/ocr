/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.DISPLACE;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link DisplaceFilter}
 * 
 */
public class DisplaceFilterTest {
	/* Class member declarations */
	private static DisplaceFilter displaceFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		displaceFilter = new DisplaceFilter();
	}

	@Test
	public void testCitor() {
		assertNotNull(displaceFilter);
	}

	@Test
	public void testDisplaceFilter() throws IOException {
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
		displaceFilter.setAmount((float) Math.pow(ALPHA, ALPHA));
		BufferedImage result = displaceFilter.filter(src, null);
		assertNotNull(result);
		/* For visual checking */
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DISPLACE.to(), "tiff");
	}
	
	@Test
	public void testSetDisplacementMap()  throws IOException {
		BufferedImage displacementMap = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		displaceFilter.setDisplacementMap(displacementMap);
		assertEquals(displacementMap, displaceFilter.getDisplacementMap());
	}
	
	@Test
	public void testGetAmount() {
		float amount = TestUtils.genFloat();
		displaceFilter.setAmount(amount);
		assertEquals(amount, displaceFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(displaceFilter.toString().contains(DisplaceFilter.class.getName()));
	}
}
