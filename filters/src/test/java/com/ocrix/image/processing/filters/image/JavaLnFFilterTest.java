/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.JAVA_LN_F;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link JavaLnFFilter} functionality. 
 *
 */
public class JavaLnFFilterTest {

	private JavaLnFFilter javaLnFFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		javaLnFFilter = new JavaLnFFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(javaLnFFilter);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(src.getHeight());
		int y = TestUtils.genInteger(src.getWidth()); 
		int rgb = src.getRGB(x, y);
		int result = javaLnFFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testJavaLnFFilter(){
		BufferedImage result = javaLnFFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + JAVA_LN_F.to(), "png");
	}
	
	@Test
	public void testToString() {
		assertTrue(javaLnFFilter.toString().contains(JavaLnFFilter.class.getName()));
	}
}
