/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.DarkenComposite;
import com.ocrix.image.processing.filters.composite.DarkenComposite.Context;

/**
 * Tests s {@link DarkenComposite} functionality.
 */
public class DarkenCompositeTest {
	/* Test's member */
	private static DarkenComposite darkenComposite;

	@BeforeClass
	public static void setUp() throws Exception {
		darkenComposite = new DarkenComposite(TestParams.ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(darkenComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		CompositeContext comp = darkenComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(
						RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BICUBIC));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.HEPTAGON + 1);
		int[] dst = TestUtils.fillArray(TestParams.HEPTAGON + 1);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(context.toString().contains(TestParams.VP_DARKEN));
	}

	@Test
	public void testToString() {
		assertNotNull(darkenComposite.toString());
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
