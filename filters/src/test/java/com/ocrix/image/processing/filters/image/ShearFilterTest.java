/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SHEAR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link ShearFilter}.
 */
public class ShearFilterTest {

	private ShearFilter shearFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		shearFilter = new ShearFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(shearFilter);
	}
	
	@Test
	public void testShearFilter(){
		shearFilter.setXAngle(0.15f);
		shearFilter.setYAngle(0.45f);
		BufferedImage result= shearFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHEAR.to(), "jpeg");
	}
	
	@Test
	public void testToString() {
		assertTrue(shearFilter.toString().contains(ShearFilter.class.getSimpleName()));
	}
	
	@Test
	public void testSetResize() {
		boolean resize = true;
		shearFilter.setResize(resize);
		assertTrue(shearFilter.isResize());
		
		// Case 2: resize = false;
		resize = false;
		shearFilter.setResize(resize);
		assertFalse(shearFilter.isResize());
	}
	
	@Test
	public void testSetXAngle() {
		float xangle = TestUtils.genFloat();
		shearFilter.setXAngle(xangle);
		assertEquals(xangle, shearFilter.getXAngle(), EPSILON);
	}
	
	@Test
	public void testTransformSpace() {
		Rectangle r = new Rectangle();
		shearFilter.setXAngle(-0.56f);
		shearFilter.setYAngle(-0.56f);
		shearFilter.transformSpace(r);
		assertTrue(true);
		
	}
	
	@Test
	public void testSetYAngle() {
		float yangle = TestUtils.genFloat();
		shearFilter.setYAngle(yangle);
		assertEquals(yangle, shearFilter.getYAngle(), EPSILON);
	}
	
	
}
