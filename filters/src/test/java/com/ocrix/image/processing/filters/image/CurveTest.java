/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link Curve} functionality.
 */
public class CurveTest {

	private static Curve curve = null;

	@BeforeClass
	public static void setUp() throws Exception {
		curve = new Curve();
	}

	@Test
	public void testCitor() {
		assertNotNull(curve);
	}

	@Test
	public void testCurveCurve() {
		curve = new Curve(new Curve());
		assertNotNull(curve);
	}
	
	@Test
	public void testSortKnots(){
		curve.sortKnots();
		assertTrue(true);
	}
	
	@Test
	public void testAddKnot(){
		curve.addKnot(TestUtils.genFloat(), TestUtils.genFloat());
		assertNotNull(curve);
	}
	
	@Test
	public void testRemoveKnots() {
		for(int i = 0; i < 5; i++){
			curve.addKnot(TestUtils.genFloat(), TestUtils.genFloat());
		}
		curve.removeKnot(1);
		assertTrue(true);
	}
	
	@Test
	public void testRemoveKnotsLessEqTwo() {
		int numKnots = curve.x.length;
		
		for(int i = 0; i < numKnots; i++){
			curve.removeKnot(i);
		}
	}
	
	@Test
	public void testToString(){
		assertTrue(curve.toString().contains(Curve.class.getName()));
	}
}
