/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.TEXTURE;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.Operations;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;
import com.ocrix.image.processing.filters.math.CellularFunction2D;
import com.ocrix.image.processing.filters.math.Function2D;

/**
 * Tests {@link TextureFilter} functionality.
 */
public class TextureFilterTest {
	private TextureFilter textureFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		textureFilter = new TextureFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(textureFilter);
	}

	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		textureFilter.setAmount(amount);
		assertEquals(amount, textureFilter.getAmount(), TestParams.EPSILON);
	}

	@Test
	public void testTextureFilter() {
		BufferedImage result = textureFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + TEXTURE.to(), "bmp");
	}

	@Test
	public void testGetAmount() {
		testSetAmount();
	}

	@Test
	public void testSetFunction() {
		Function2D function = new CellularFunction2D();
		textureFilter.setFunction(function);
		assertEquals(function, textureFilter.getFunction());
	}

	@Test
	public void testSetOperation() {
		int operation = Operations.BURN.ordinal();
		textureFilter.setOperation(operation);
		assertEquals(operation, textureFilter.getOperation());
	}

	@Test
	public void testSetScale() {
		float scale = TestUtils.genFloatInRange(1, 300);
		textureFilter.setScale(scale);
		assertEquals(scale, textureFilter.getScale(), TestParams.EPSILON);
	}

	@Test
	public void testSetStretch() {
		float stretch = TestUtils.genFloatInRange(TestParams.UNITY, 50);
		textureFilter.setStretch(stretch);
		assertEquals(stretch, textureFilter.getStretch(), TestParams.EPSILON);
	}

	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		textureFilter.setAngle(angle);
		assertEquals(angle, textureFilter.getAngle(), TestParams.EPSILON);
	}

	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloatInRange(0, TestParams.UNITY);
		textureFilter.setTurbulence(turbulence);
		assertEquals(turbulence, textureFilter.getTurbulence(), TestParams.EPSILON);
	}

	@Test
	public void testSetColormap() {
		Colormap colormap = new ArrayColormap(TestUtils.fillArray(TestParams.DIME));
		textureFilter.setColormap(colormap);
		assertEquals(colormap, textureFilter.getColormap());
	}

	@Test
	public void testFilterRGB() {
		int result = textureFilter.filterRGB(0, 0, Color.BLUE.getRGB());
		assertTrue(result != 0);
	}

	@Test
	public void testToString() {
		assertTrue(textureFilter.toString().contains(TEXTURE.to()));
		
		// case 2:
		textureFilter.setColormap(null);
		textureFilter.setFunction(null);
		assertTrue(textureFilter.toString().contains(TEXTURE.to()));
	}

	@After
	public void tearDown() throws Exception {
	}
}
