/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.AddComposite;
import com.ocrix.image.processing.filters.composite.AddComposite.Context;

/**
 * Tests {@link AddComposite} functionality.
 */
public class AddCompositeTest {
	private static AddComposite addComposite;

	@BeforeClass
	public static void setUp() throws Exception {
		addComposite = new AddComposite(TestParams.ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(addComposite);
	}

	@Test
	public void testComposeRGB() throws IOException {
		// float alpha, ColorModel srcColorModel, ColorModel dstColorModel
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.HEPTAGON);
		int[] dst = TestUtils.fillArray(TestParams.HEPTAGON);
		context.composeRGB(src, dst, TestParams.ALPHA);
		boolean result = Arrays.equals(src, dst);
		assertFalse(result);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testComposeRGBNegative() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.HEPTAGON - 2);
		int[] dst = TestUtils.fillArray(TestParams.HEPTAGON);
		context.composeRGB(src, dst, TestParams.ALPHA);
		boolean result = Arrays.equals(src, dst);
		assertFalse(result);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel colorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		CompositeContext comp = addComposite.createContext(colorModel,
				colorModel, new RenderingHints(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON));
		assertNotNull(comp);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreateContextNullParams() {
		CompositeContext comp = addComposite.createContext(null, null,
				new RenderingHints(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON));
		assertNotNull(comp);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
