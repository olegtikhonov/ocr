/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link FractalSumFunction} functionality.
 */
public class TestFractalSumFunction {

	private FractalSumFunction fsf;

	@Before
	public void setUp() throws Exception {
		fsf = new FractalSumFunction(new Noise());
	}

	@Test
	public void testFractalSumFunction() {
		assertNotNull(fsf);
	}

	@Test
	public void testEvaluate() {
		float x = TestUtils.genFloat();
		float y = TestUtils.genFloat();
		float result = fsf.evaluate(x, y);
		assertTrue(result != 0.0f);
	}
	
	@Test
	public void testToString(){
		assertTrue(fsf.toString().contains(FractalSumFunction.class.getName()));
	}
}
