/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FLIP;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link FlipFilter} functionality.
 */
public class FlipFilterTest {

	private static FlipFilter flipFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		flipFilter = new FlipFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(flipFilter);
	}
	
	
	@Test
	public void testFlipFilter(){
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FLIP.to(), "jpeg");
	}
	
	@Test
	public void testSetOperation() {
		int operation = TestUtils.genInteger();
		flipFilter.setOperation(operation);
		assertEquals(operation, flipFilter.getOperation());
	}
	
	@Test
	public void testToString() {
		assertTrue(flipFilter.toString().contains("Flip"));
	}
	
	@Test
	public void testSetNewWidth() {
		int newWidth = TestUtils.genInteger(); 
		flipFilter.setNewWidth(newWidth);
		assertEquals(newWidth, flipFilter.getNewWidth());
	}
	
	@Test
	public void testSetNewHeight() {
		int newHeight = TestUtils.genInteger();
		flipFilter.setNewHeight(newHeight);
		assertEquals(newHeight, flipFilter.getNewHeight());
	}
	
	@Test
	public void testFilterFLIP_HOp() {
		flipFilter.setOperation(FlipFilter.FLIP_H);
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterFLIP_VOp() {
		flipFilter.setOperation(FlipFilter.FLIP_V);
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterFLIP_90CWOp() {
		flipFilter.setOperation(FlipFilter.FLIP_90CW);
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterFLIP_90CCWOp() {
		flipFilter.setOperation(FlipFilter.FLIP_90CCW);
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterFFLIP_180Op() {
		flipFilter.setOperation(FlipFilter.FLIP_180);
		BufferedImage result = flipFilter.filter(src, null);
		assertNotNull(result);
	}
	

	@Test
	public void testFilterToString() {
		flipFilter.setOperation(FlipFilter.FLIP_180);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
		
		flipFilter.setOperation(FlipFilter.FLIP_90CCW);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
		
		flipFilter.setOperation(FlipFilter.FLIP_90CW);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
		
		flipFilter.setOperation(FlipFilter.FLIP_H);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
		
		flipFilter.setOperation(FlipFilter.FLIP_HV);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
		
		flipFilter.setOperation(FlipFilter.FLIP_V);
		assertTrue(flipFilter.toString().contains(FlipFilter.class.getName()));
	}
}
