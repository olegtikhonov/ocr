/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.GRAY;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link GrayFilter} functionality.
 */
public class GrayFilterTest {
	private GrayFilter grayFilter;
	private BufferedImage image;

	@Before
	public void setUp() throws Exception {
		grayFilter = new GrayFilter();
		image = ImageIO.read(new File(PREFIXPATH + TUX));
	}

	@Test
	public void testFilterRGB() {
		int rgb = image.getRGB(image.getHeight() / 2, image.getWidth() / 2);
		int result = grayFilter.filterRGB(image.getHeight() / 2, image.getWidth() / 2, rgb);
		assertTrue(rgb < result);
	}

	@Test
	public void testGrayFilter() {
		BufferedImage result = grayFilter.filter(image, image);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GRAY.to(), "png");
	}

	@Test
	public void testToString() {
		assertTrue(grayFilter.toString().contains(GrayFilter.class.getName()));
	}
}
