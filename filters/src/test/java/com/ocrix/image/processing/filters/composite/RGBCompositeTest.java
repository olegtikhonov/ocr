/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import java.util.Random;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.composite.RGBComposite;
import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.FRENCH_DEPARTMENT_VAR;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests a {@link RGBComposite} functionality.
 */
public class RGBCompositeTest {

	private static Random randGen = null;

	@BeforeClass
	public static void setUp() throws Exception {
		randGen = new Random();
	}

	@Test
	public void testClamp() {
		assertTrue(RGBComposite.RGBCompositeContext.clamp(Math.abs(randGen
				.nextInt() + 1)) > 0);
	}

	@Test
	public void testNegClamp() {
		assertTrue(RGBComposite.RGBCompositeContext.clamp(-1) == 0);
	}

	@Test
	public void testClampMore255() {
		assertTrue(RGBComposite.RGBCompositeContext.clamp(ANGEL_NUMBER) == QUAD);
	}

	@Test
	public void testClampInRange() {
		assertTrue(RGBComposite.RGBCompositeContext
				.clamp(FRENCH_DEPARTMENT_VAR) == FRENCH_DEPARTMENT_VAR);
	}

	@Test
	public void testMultiply255() {
		assertEquals(RGBComposite.RGBCompositeContext.multiply255(QUAD, QUAD),
				QUAD);
	}
}
