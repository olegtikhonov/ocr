/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests the {@link ApplyMaskFilter} functionality.
 */
public class ApplyMaskFilterTest {

	private ApplyMaskFilter applyMaskFilter = null;
	private BufferedImage src = null;
	private ColorModel dstCM = null;
	private BufferedImage dest = null;

	@Before
	public void setUp() throws Exception {
		applyMaskFilter = new ApplyMaskFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dstCM = CommonUtils.getColorModel(src);
		dest = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(applyMaskFilter);
	}

	@Test
	public void testCreateCompatibleDestImage() throws IOException {
		dest = applyMaskFilter.createCompatibleDestImage(src, dstCM);
		assertNotNull(dest);
	}

	// Null param testing
	@Test(expected = IllegalArgumentException.class)
	public void testCreateCompatibleDestImageNullParams() {
		dest = applyMaskFilter.createCompatibleDestImage(null, null);

	}

	@Test
	public void testGetBounds2D() {
		Rectangle2D rect = applyMaskFilter.getBounds2D(src);
		assertNotNull(rect);
	}

	// Null param testing
	@Test(expected = IllegalArgumentException.class)
	public void testGetBounds2DNullParam() {
		Rectangle2D rect = applyMaskFilter.getBounds2D(null);
		assertNull(rect);
	}

	@Test
	public void testGetPoint2D() {
		Point2D pointA = new Point2D.Float();
		Point2D pointB = new Point2D.Float();
		Point2D pointC = applyMaskFilter.getPoint2D(pointA, pointB);
		assertNotNull(pointC);
	}

	@Test
	public void testGetPoint2DSecParamNull() {
		Point2D pointA = new Point2D.Float();
		Point2D pointC = applyMaskFilter.getPoint2D(pointA, null);
		assertNotNull(pointC);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetPoint2DFirstParamNull() {
		Point2D pointB = new Point2D.Float();
		Point2D pointC = applyMaskFilter.getPoint2D(null, pointB);
		assertNull(pointC);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetPoint2DBothParamsNull() {
		Point2D pointC = applyMaskFilter.getPoint2D(null, null);
		assertNull(pointC);
	}

	@Test
	public void testGetRGB() {
		int[] pixels = applyMaskFilter.getRGB(src, src.getMinX(),
				src.getMinY(), src.getWidth(), src.getHeight(), null);
		assertTrue((pixels != null) && (pixels.length > 0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetRGBFirstParamNull() {
		int[] pixels = applyMaskFilter.getRGB(null, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), new int[QUAD]);
		assertTrue(pixels == null);
	}

	@Test
	public void testSetRGB() {
		int[] pixels = applyMaskFilter.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), null);
		applyMaskFilter.setRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), pixels);
		int[] result = applyMaskFilter.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), new int[pixels.length]);
		assertTrue((result != null) && (result.length > 0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetRGBFirstParamNull() {
		int[] pixels = applyMaskFilter.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), null);
		applyMaskFilter.setRGB(null, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), pixels);
		assertTrue(true);
	}

	@Test
	public void testSetRGBIntParamsNegative() {
		int[] pixels = applyMaskFilter.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), null);
		applyMaskFilter.setRGB(src, -1, -255, -244, -234567, pixels);
		int[] result = applyMaskFilter.getRGB(src, src.getMinX(), src.getMinY(), src.getWidth(), src.getHeight(), new int[pixels.length]);
		assertTrue((result != null) && (result.length > 0));
	}

	@Test
	public void testClone() {
		assertTrue(applyMaskFilter.clone().getClass() == applyMaskFilter
				.getClass());
	}

	@Test
	public void testCloneNotEq() {
		assertFalse(applyMaskFilter.clone().getClass() == TestParams.class.getClass());
	}

	// Time to test a real class
	@Test
	public void testSetDestination() throws IOException {
		applyMaskFilter.setDestination(dest);
		BufferedImage result = applyMaskFilter.getDestination();
		assertEquals(dest, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetDestinationNullParam() throws IOException {
		applyMaskFilter.setDestination(null);
		BufferedImage result = applyMaskFilter.getDestination();
		assertTrue(result == null);
	}

	@Test
	public void testSetMaskImage() throws IOException {
		applyMaskFilter.setMaskImage(dest);
		BufferedImage result = applyMaskFilter.getMaskImage();
		assertEquals(dest, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetMaskImageNullParam() throws IOException {
		applyMaskFilter.setMaskImage(null);
		BufferedImage result = applyMaskFilter.getDestination();
		assertTrue(result == null);
	}

	@Test
	public void testComposeThroughMask() {
		byte[] pixels = CommonUtils.generatePixels(src.getWidth(), src.getHeight(), new Rectangle2D.Float(-2.0f, -1.2f, 3.2f, 2.4f));
		DataBuffer dbuf = new DataBufferByte(pixels, src.getWidth() * src.getHeight(), 0);
		SampleModel sm = new SinglePixelPackedSampleModel(DataBuffer.TYPE_BYTE, src.getWidth(), src.getHeight(), new int[] { (byte) 0xf });
		Raster srcRaster = Raster.createRaster(sm, dbuf, new Point(0, 0));
		WritableRaster dstRaster = Raster.createWritableRaster(sm, new Point(0,0));
		ApplyMaskFilter.composeThroughMask(srcRaster, dstRaster, srcRaster);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testComposeThroughMaskNull() {
		ApplyMaskFilter.composeThroughMask(null, null, null);
		assertTrue(true);
	}

	@Test
	public void testFilter() {
		assertNotNull(applyMaskFilter.filter(src, dest));
	}

	@Test
	public void testFilterDestNull() {
		assertNotNull(applyMaskFilter.filter(src, null));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFilterSrcNull() {
		assertNull(applyMaskFilter.filter(null, null));
	}

	@Test
	public void testSave() {
		ApplyMaskFilter aplMaskFilter = new ApplyMaskFilter(src, dest);
		BufferedImage bi = aplMaskFilter.filter(src, dest);
		CommonUtils.saveBufferedImage(bi, TO_BE_SAVED + "MaskFilter", "jpg");
	}
}
