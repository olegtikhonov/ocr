/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.KALEIDOSCOPE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link KaleidoscopeFilter} functionality.
 */
public class KaleidoscopeFilterTest {

	private KaleidoscopeFilter kaleidoscopeFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		kaleidoscopeFilter = new KaleidoscopeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(kaleidoscopeFilter);
	}
	
	@Test
	public void testSetSides() {
		int sides = TestUtils.genInteger();
		kaleidoscopeFilter.setSides(sides);
		assertEquals(sides, kaleidoscopeFilter.getSides());
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		kaleidoscopeFilter.setAngle(angle);
		assertEquals(angle, kaleidoscopeFilter.getAngle(), EPSILON);
	}

	@Test
	public void testSetAngle2() {
		float angle2 = TestUtils.genFloat();
		kaleidoscopeFilter.setAngle2(angle2);
		assertEquals(angle2, kaleidoscopeFilter.getAngle2(), EPSILON);
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		kaleidoscopeFilter.setCentreX(centreX);
		assertEquals(centreX, kaleidoscopeFilter.getCentreX(), EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		kaleidoscopeFilter.setCentreY(centreY);
		assertEquals(centreY, kaleidoscopeFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float();
		kaleidoscopeFilter.setCentre(centre);
		assertEquals(centre, kaleidoscopeFilter.getCentre());
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat();
		kaleidoscopeFilter.setRadius(radius);
		assertEquals(radius, kaleidoscopeFilter.getRadius(), EPSILON);
	}

	@Test
	public void testToString() {
		assertTrue(kaleidoscopeFilter.toString().contains(KaleidoscopeFilter.class.getName()));
	}
	
	@Test
	public void testKaleidoscopeFilter(){
		BufferedImage result = kaleidoscopeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + KALEIDOSCOPE.to(), "png");
	}
}
