/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests a {@link CompoundFunction2D} functionality.
 */
public class TestCompoundFunction2D {

	private CompoundFunction2D cf2d;
	
	@Before
	public void setUp() throws Exception {
		cf2d = new FractalSumFunction(new Noise());
	}

	@Test
	public void testCompoundFunction2D() {
	    assertNotNull(cf2d);
	}

	@Test
	public void testSetBasis() {
		Function2D basis = new RidgedFBM();
		cf2d.setBasis(basis);
		assertEquals(basis, cf2d.getBasis());
	}

	@Test
	public void testGetBasis() {
		testSetBasis();
	}
}
