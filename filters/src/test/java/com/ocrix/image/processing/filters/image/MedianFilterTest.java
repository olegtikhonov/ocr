/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.MEDIAN;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link MedianFilter} functionality. 
 */
public class MedianFilterTest {

	private static MedianFilter medianFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		medianFilter = new MedianFilter();
		src = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
	}

	@Test
	public void testCitor(){
		assertNotNull(medianFilter);
	}
	
	@Test
	public void testMedianFilter(){
		BufferedImage result = medianFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MEDIAN.to(), "bmp");
	}
	
	@Test
	public void testToString() {
		assertTrue(medianFilter.toString().contains(MedianFilter.class.getSimpleName()));
	}
}
