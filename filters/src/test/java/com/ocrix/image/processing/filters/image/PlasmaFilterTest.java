/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.PLASMA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PlasmaFilter} functionality.
 */
public class PlasmaFilterTest {
	private PlasmaFilter plasmaFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		plasmaFilter = new PlasmaFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(plasmaFilter);
	}
	
	@Test
	public void testPlasmaFilter(){
		BufferedImage result = plasmaFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + PLASMA.to(), "png");
	}
	
	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloat();
		plasmaFilter.setTurbulence(turbulence);
		assertEquals(turbulence, plasmaFilter.getTurbulence(), EPSILON);
	}
	
	@Test
	public void testSetScaling() {
		float scaling = TestUtils.genFloat();
		plasmaFilter.setScaling(scaling);
		assertEquals(scaling, plasmaFilter.getScaling(), EPSILON);
	}
	
	@Test
	public void testSetColormap() {
		Colormap map = new ArrayColormap();
		plasmaFilter.setColormap(map);
		assertEquals(map, plasmaFilter.getColormap());
	}
	
	@Test
	public void testSetUseColormap() {
		// Case 1: true case
		boolean useColormap = true;
		plasmaFilter.setUseColormap(useColormap);
		assertTrue(plasmaFilter.getUseColormap());
		
		// Case 2: false
		useColormap = false;
		plasmaFilter.setUseColormap(useColormap);
		assertFalse(plasmaFilter.getUseColormap());
	}
	
	@Test
	public void testSetUseImageColors() {
		// Case 1: true case
		boolean useImageColors = true;
		plasmaFilter.setUseImageColors(useImageColors);
		assertTrue(plasmaFilter.getUseImageColors());
		
		// Case 2: false
		useImageColors = false;
		plasmaFilter.setUseImageColors(useImageColors);
		assertFalse(plasmaFilter.getUseImageColors());
	}
	
	@Test
	public void testSetSeed() {
		int seed = TestUtils.genInteger();
		plasmaFilter.setSeed(seed);
		assertEquals(seed, plasmaFilter.getSeed());
	}
	
	@Test
	public void testRandomize() {
		plasmaFilter.randomize();
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(plasmaFilter.toString().contains(PlasmaFilter.class.getSimpleName()));
	}
}
