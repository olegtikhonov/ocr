/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.COMPOSITE;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.UNITY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link CompositeFilter} functionality.
 */
public class CompositeFilterTest {
	/* Class' members */
	private static CompositeFilter compositeFilter = null;
	private static BufferedImage src = null;
	private static AffineTransform transformer;

	@BeforeClass
	public static void setUp() throws Exception {
		compositeFilter = new CompositeFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		transformer = new AffineTransform();
	}

	@Test
	public void testCitorNoParams() {
		assertNotNull(compositeFilter);
	}

	@Test
	public void testCitorParam() {
		Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				(float) (Math.random() * 0.1f + ALPHA));
		compositeFilter = new CompositeFilter(c);
		assertNotNull(compositeFilter);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorNullParam() {
		compositeFilter = new CompositeFilter(null);
		assertNull(compositeFilter);
	}

	@Test
	public void testFilter() {
		/* Composite initialization */
		float swt = (float) (Math.random() * ALPHA);
		Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_IN, swt);
		compositeFilter = new CompositeFilter(c);
		transformer.translate(TestUtils.genInteger(HEPTAGON),
				TestUtils.genInteger(HEPTAGON));
		transformer.scale(UNITY, TestUtils.genInteger(HEPTAGON));
		compositeFilter.setTransform(transformer);
		/* Applies filter to the src */
		BufferedImage result = compositeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + COMPOSITE.to(), "jpg");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTransformNull() {
		/* Composite initialization */
		Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				(float) (Math.random() * 0.1f + ALPHA));
		compositeFilter = new CompositeFilter(c);
		compositeFilter.setTransform(null);
	}

	@Test
	public void testSetTransform() {
		/* Composite initialization */
		Composite c = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				(float) (Math.random() * 0.1f + ALPHA));
		compositeFilter = new CompositeFilter(c);
		transformer.translate(TestUtils.genInteger(HEPTAGON),
				TestUtils.genInteger(HEPTAGON));
		transformer.scale(TestUtils.genInteger(HEPTAGON),
				TestUtils.genInteger(HEPTAGON));
		compositeFilter.setTransform(transformer);
		assertEquals(compositeFilter.getTransform(), transformer);
	}

	@Test
	public void testCompositeFilter() {
		Composite composite = AlphaComposite.getInstance(AlphaComposite.SRC_IN,
				TestUtils.genFloat());
		compositeFilter = new CompositeFilter(composite, transformer);
		assertNotNull(compositeFilter);
	}
	
	@Test
	public void testToString() {
		assertTrue(compositeFilter.toString().contains(CompositeFilter.class.getName()));
	}
}
