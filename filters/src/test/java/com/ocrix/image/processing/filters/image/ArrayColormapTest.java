/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests an {@link ArrayColormap} functionality.
 */
public class ArrayColormapTest {

	private static ArrayColormap arrayColormap = new ArrayColormap();
	private static int[] colorMap = { 1, 2 };
	private static Random randGen = null;

	@BeforeClass
	public static void setUp() throws Exception {
		arrayColormap = new ArrayColormap();
		randGen = new Random();
	}

	@Test
	public void testCitor() {
		assertNotNull(arrayColormap);
	}

	@Test
	public void testCitorParam() {
		colorMap = new int[RONNA];
		/* Fills colormap with pseudo random numbers [0...256) */
		for (int i = 0; i < RONNA; i++) {
			colorMap[i] = randGen.nextInt(RONNA);
		}
		ArrayColormap acm = new ArrayColormap(colorMap);
		assertNotNull(acm);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorParamNull() {
		ArrayColormap acm = new ArrayColormap(null);
		assertNull(acm);
	}

	@Test
	public void testClone() {
		assertEquals(arrayColormap.clone().getClass(), arrayColormap.getClass());
	}

	// @Test
	public void testCloneNeg() {
		assertFalse(arrayColormap.clone().getClass() == colorMap.getClass());
	}

	// @Test
	public void testSetMap() {
		ArrayColormap acm = new ArrayColormap();
		acm.setMap(colorMap);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetMapNullParam() {
		ArrayColormap acm = new ArrayColormap();
		acm.setMap(null);
		assertTrue(false);
	}

	// @Test
	public void testGetMap() {
		ArrayColormap acm = new ArrayColormap();
		acm.setMap(colorMap);
		assertNotNull(acm.getMap());
	}

//	@Test
	public void testGetColor() {
		ArrayColormap acm = new ArrayColormap();
		acm.setMap(colorMap);
		assertTrue(acm.getColor(ALPHA) >= 0);
	}

	@Test
	public void testGetColorNeg() {
		ArrayColormap acm = new ArrayColormap();
		acm.setMap(colorMap);
		assertTrue(acm.getColor(-ALPHA) >= 0);
	}

	@Test
	public void testSetColorInterpolated() {
		int lower = randGen.nextInt(RONNA >> 4);
		int index = nextInt(randGen, lower, (colorMap.length - 1));
		// first < index < last
		if ((lower ^ index) >= 0) {
			arrayColormap.setColorInterpolated(index, lower,
					(colorMap.length - 1), randGen.nextInt(RONNA));
			assertTrue(true);
		} else {// ~i + 1
			arrayColormap.setColorInterpolated((index < 0 ? (~index + 1)
					: index), lower, (colorMap.length - 1), randGen
					.nextInt(RONNA));
			assertTrue(true);
		}
	}

	@Test
	public void testSetColorRange() {
		int lower = randGen.nextInt(RONNA >> 4);
		arrayColormap.setColorRange(lower, (colorMap.length - 1),
				randGen.nextInt(RONNA));
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetColorRangeNegativeParams() {
		arrayColormap.setColorRange(((RONNA ^ -1) + 1), ((RONNA ^ -1) + 1),
				((RONNA ^ -1) + 1));
		assertTrue(false);
	}

	@Test
	public void testSetColorRangeFourParams() {
		int lower = randGen.nextInt(RONNA >> 4);
		arrayColormap.setColorRange(lower, (colorMap.length - 1),
				randGen.nextInt(RONNA), randGen.nextInt(RONNA));
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetColorRangeFourNegativeParams() {
		arrayColormap.setColorRange(((RONNA ^ -1) + 1), ((RONNA ^ -1) + 1),
				((RONNA ^ -1) + 1), ((RONNA ^ -1) + 1));
		assertTrue(false);
	}

	@Test
	public void testSetColor() {
		arrayColormap.setColor(nextInt(randGen, 0, colorMap.length - 1),
				randGen.nextInt(RONNA));
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetColorNegPArams() {
		arrayColormap.setColor(((RONNA ^ -1) + 1), ((RONNA ^ -1) + 1));
		assertTrue(false);
	}

	private static int nextInt(Random r, int lower, int higher) {
		int ran = r.nextInt();
		double x = (double) (ran / Integer.MAX_VALUE * higher);
		return (x >= 0) ? ((int) x + lower) : ((int) -x + lower);
	}
}
