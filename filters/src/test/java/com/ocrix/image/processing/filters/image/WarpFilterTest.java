/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.WARP;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link WarpFilter} functionality.
 */
public class WarpFilterTest {

	private static WarpFilter warpFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		warpFilter = new WarpFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(warpFilter);
	}

	@Test
	public void testWarpFilter()  throws Exception{
		BufferedImage dest = CommonUtils.createCompatibleImage(src);

		WarpGrid srcWarpGrid = new WarpGrid(src.getHeight(), src.getWidth(), src.getWidth(), src.getHeight());
		WarpGrid destWarpGrid = new WarpGrid(dest.getHeight(), dest.getWidth(), dest.getWidth() - 90, dest.getHeight() - 90);

		warpFilter = new WarpFilter(srcWarpGrid, destWarpGrid);
		BufferedImage result = warpFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), src); 
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + WARP.to(), "png");
	}
}
