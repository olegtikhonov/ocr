/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.MASK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link MaskFilter} functionality.
 */
public class MaskFilterTest {

	private MaskFilter maskFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		maskFilter = new MaskFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(maskFilter);
	}
	
	
	@Test
	public void testMaskFilter(){
		maskFilter.setMask(Color.YELLOW.getRGB());
		BufferedImage result = maskFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MASK.to(), "tiff");
	}
	
	@Test
	public void testSetMask() {
		int mask = TestUtils.genInteger();
		maskFilter.setMask(mask);
		assertEquals(mask, maskFilter.getMask());
	}
	
	@Test
	public void testFilterRGB() {
		int result = maskFilter.filterRGB(0, 0, Color.YELLOW.getRGB());
		assertTrue(result < 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(maskFilter.toString().contains(MaskFilter.class.getName()));
	}
}
