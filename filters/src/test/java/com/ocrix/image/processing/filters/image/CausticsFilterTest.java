/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.HRADIUS;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.UNITY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link CausticsFilter} functionality.
 */
public class CausticsFilterTest {
	private static CausticsFilter causticsFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		causticsFilter = new CausticsFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(causticsFilter);
	}

	@Test
	public void testSetScale() {
		causticsFilter.setScale(HRADIUS);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegScale() {
		causticsFilter.setScale(-ALPHA);
		assertTrue(false);
	}

	@Test
	public void testSetBrightness() {
		causticsFilter.setBrightness(UNITY);
		assertTrue(true);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBrightnessOutOfRange() {
		causticsFilter.setBrightness(ANGEL_NUMBER);
	}

	@Test
	public void testSetTurbulence() {
		causticsFilter.setTurbulence(ALPHA);
		assertTrue(ALPHA == causticsFilter.getTurbulence());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTurbulenceOutOfRange() {
		causticsFilter.setTurbulence(-ALPHA);
		assertTrue(ALPHA != causticsFilter.getTurbulence());
	}

	@Test
	public void testSetAmount() {
		causticsFilter.setAmount(ALPHA);
		assertTrue(ALPHA == causticsFilter.getAmount());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetAmountOutOfRange() {
		causticsFilter.setAmount(ANGEL_NUMBER);
		assertTrue(ALPHA != causticsFilter.getAmount());
	}

	@Test
	public void testSetDispersion() {
		causticsFilter.setDispersion(ALPHA);
		assertTrue(ALPHA == causticsFilter.getDispersion());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetDispersionOutOfRange() {
		causticsFilter.setDispersion(-ALPHA);
		assertTrue(-ALPHA != causticsFilter.getDispersion());
	}

	@Test
	public void testSetTime() {
		causticsFilter.setTime(ALPHA);
		assertTrue(ALPHA == causticsFilter.getTime());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTimeWrongValue() {
		causticsFilter.setTime(-ALPHA);
		assertTrue(ALPHA != causticsFilter.getTime());
	}

	@Test
	public void testSetSamples() {
		causticsFilter.setSamples(ANGEL_NUMBER);
		assertTrue(ANGEL_NUMBER == causticsFilter.getSamples());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetSamplesNegParam() {
		causticsFilter.setSamples(-ANGEL_NUMBER);
		assertTrue(ANGEL_NUMBER != causticsFilter.getSamples());
	}

	@Test
	public void testFilter() {
		causticsFilter = new CausticsFilter();
		BufferedImage result = causticsFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "CausticsFilter", "jpg");
	}

	@Test
	public void testGetScale() {
		float vp_scale = TestUtils.genFloat(5);
		causticsFilter.setScale(vp_scale);
		assertEquals(vp_scale, causticsFilter.getScale(), EPSILON);
	}

	@Test
	public void testGetBrightness() {
		int vp_brightness = TestUtils.genInteger(1);
		causticsFilter.setBrightness(vp_brightness);
		assertEquals(vp_brightness, causticsFilter.getBrightness());
	}

	@Test
	public void testSetBgColor() {
		int vp_color = TestUtils.genInteger();
		causticsFilter.setBgColor(vp_color);
		assertEquals(vp_color, causticsFilter.getBgColor());
	}

	@Test
	public void testGetBgColor() {
		testSetBgColor();
	}

	@Test
	public void testToString() {
		assertTrue(causticsFilter.toString().contains(
				CausticsFilter.class.getName()));
	}
	
	@Test
	public void testAddTwoParams() {
		int result = causticsFilter.add(TestUtils.genInteger(), TestUtils.genFloat());
		assertTrue(result != 0);
	}

	@Test
	public void testAddTwoParamsOtherCases() {
		int result = causticsFilter.add(TestUtils.genInteger(), ANGEL_NUMBER);
		assertTrue(result == -1);
	}

	
	@Test
	public void testAddThreeParams() {
		int result = causticsFilter.add(TestUtils.genInteger(), TestUtils.genFloat(), TestUtils.genInteger());
		assertTrue(result != 0);
	}

	@Test
	public void testAddThreeParamsOtherCases() {
		/* Case when c = 2 */
		int result = causticsFilter.add(TestUtils.genInteger(), ANGEL_NUMBER, 2);
		assertTrue(result != 0);
		
		/* Case when c = 1 */
		result = causticsFilter.add(TestUtils.genInteger(), ANGEL_NUMBER, 1);
		assertTrue(result != 0);
		
		/* Case when c != 1 & c!= 2 */
		result = causticsFilter.add(TestUtils.genInteger(), ANGEL_NUMBER, 3);
		assertTrue(result != 0);
	}
	
	

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
