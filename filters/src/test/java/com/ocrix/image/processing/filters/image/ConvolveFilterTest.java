/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.NONAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.image.ConvolveFilter.EDGES_ACTION;


/**
 * Tests {@link ConvolveFilter} functionality.
 */
public class ConvolveFilterTest {

	private ConvolveFilter convolveFilter;
	private BufferedImage src = null;
	private Kernel kernel;
	private int[] inPixels;
	private int[] outPixels;
	private int width;
	private int height;
	private boolean alpha;
	
	
	@Before
	public void setUp() throws Exception {
		convolveFilter = new ConvolveFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
		kernel = new Kernel(3, 3, TestUtils.generateFloatArray(NONAGON));
		inPixels = TestUtils.fillArray(NONAGON);
		outPixels = TestUtils.fillArray(NONAGON);
		width = 3;
		height = 3;
		alpha = true;
	}

	@Test
	public void testCreateCompatibleDestImage() {
		BufferedImage result = convolveFilter.createCompatibleDestImage(src, CommonUtils.getColorModel(src));
		assertNotNull(result);
	}

	@Test
	public void testGetBounds2D() {
		Rectangle2D r2d = convolveFilter.getBounds2D(src);
		assertNotNull(r2d);
	}

	@Test
	public void testGetPoint2D() {
		Point2D p2d = convolveFilter.getPoint2D(new Point2D.Float(), new Point2D.Float());
		assertNotNull(p2d);
	}

	@Test
	public void testGetRenderingHints() {
		convolveFilter.setRenderingHints(new RenderingHints(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_DEFAULT));
		assertNotNull(convolveFilter.getRenderingHints());
	}

	@Test
	public void testConvolveFilter() {
		convolveFilter.setKernel(new Kernel(3, 3, new float[]{-1, -1, -1, -1, 9.15f, -1, -1, -1, -1}));
		BufferedImage result = convolveFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "ConvolveFilter", "png");
	}

	@Test
	public void testConvolveFilterFloatArray() {
		float[] convlMatrix = TestUtils.generateFloatArray(NONAGON);
		convolveFilter = new ConvolveFilter(convlMatrix);
		assertNotNull(convolveFilter);
	}

	@Test
	public void testConvolveFilterIntIntFloatArray() {
		float[] matrix = TestUtils.generateFloatArray(NONAGON * NONAGON);
		convolveFilter = new ConvolveFilter(NONAGON, NONAGON, matrix);
		assertEquals(convolveFilter.getKernel().getHeight(), convolveFilter.getKernel().getWidth());
	}

	@Test
	public void testConvolveFilterKernel() {
		convolveFilter = new ConvolveFilter(new Kernel(3, 3, new float[]{-1, -1, -1, -1, 4.678f, -1, -1, -1, -1}));
		assertNotNull(convolveFilter);
	}

	@Test
	public void testSetKernel() {
		Kernel kernal = new Kernel(NONAGON, NONAGON, TestUtils.generateFloatArray((int) Math.pow(NONAGON, 2)));
		convolveFilter.setKernel(kernal);
		assertEquals(kernal, convolveFilter.getKernel());
	}

	@Test
	public void testGetKernel() {
		testSetKernel();
	}

	@Test
	public void testSetEdgeAction() {
		convolveFilter.setEdgeAction(EDGES_ACTION.CLAMP);
		assertEquals(EDGES_ACTION.CLAMP, convolveFilter.getEdgeAction());
	}

	@Test
	public void testGetEdgeAction() {
		testSetEdgeAction();
	}

	@Test
	public void testSetUseAlpha() {
		convolveFilter.setUseAlpha(true);
		assertTrue(convolveFilter.getUseAlpha());
		
		convolveFilter.setUseAlpha(false);
		assertFalse(convolveFilter.getUseAlpha());
	}

	@Test
	public void testGetUseAlpha() {
		testSetUseAlpha();
	}

	@Test
	public void testSetPremultiplyAlpha() {
		convolveFilter.setPremultiplyAlpha(true);
		assertTrue(convolveFilter.getPremultiplyAlpha());
		
		convolveFilter.setPremultiplyAlpha(false);
		assertFalse(convolveFilter.getPremultiplyAlpha());
	}

	@Test
	public void testGetPremultiplyAlpha() {
		testSetPremultiplyAlpha();
	}

	@Test
	public void testFilter() {
		convolveFilter = new ConvolveFilter(new Kernel(3, 3, new float[]{-1, -1, -1, -1, 8.0f, -1, -1, -1, -1}));
		try {
			BufferedImage result = convolveFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), src);
			CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "ConvolveFilter", "tiff");
		} catch (IOException e) {
		}
	}
	
	@Test
	public void testFilterPremultiplyAlpha() throws IOException {
		convolveFilter.setPremultiplyAlpha(false);
		BufferedImage result = convolveFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), src);
		assertNotNull(result);
	}

	@Test
	public void testConvolveKernelIntArrayIntArrayIntIntEDGES() {
		ConvolveFilter.convolve(kernel, inPixels, outPixels, width, height, EDGES_ACTION.WRAP);
		assertTrue(true);
	}

	@Test
	public void testConvolveKernelIntArrayIntArrayIntIntBooleanEDGES() {
		ConvolveFilter.convolve(kernel, inPixels, outPixels, width, height, alpha, EDGES_ACTION.ZERO);
		assertTrue(true);
	}

	@Test
	public void testConvolveHV() {
		ConvolveFilter.convolveHV(kernel, inPixels, outPixels, width, height, alpha, EDGES_ACTION.CLAMP);
		assertTrue(true);
	}

	@Test
	public void testConvolveH() {
		ConvolveFilter.convolveH(kernel, inPixels, outPixels, width, height, alpha, EDGES_ACTION.CLAMP);
		assertTrue(true);
	}

	@Test
	public void testConvolveV() {
		ConvolveFilter.convolveV(kernel, inPixels, outPixels, width, height, alpha, EDGES_ACTION.CLAMP);
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(convolveFilter.toString().contains(ConvolveFilter.class.getName()));
	}
	
	//Negative
	@Test
	public void testGetPoint2DDstPntNull() {
		Point2D point = convolveFilter.getPoint2D(new Point2D.Float(), null);
		assertNotNull(point);
	}
	
	@Test
	public void testIfKernelGetHeightEqOne(){
		kernel = new Kernel(1, 1, TestUtils.generateFloatArray(3));
		ConvolveFilter.convolve(kernel, inPixels, outPixels, width, height, EDGES_ACTION.ZERO);
		assertTrue(true);
	}
	
	@Test
	public void testIfKernelGetWeightEqOne(){
		kernel = new Kernel(1, 2, TestUtils.generateFloatArray(3));
		ConvolveFilter.convolve(kernel, inPixels, outPixels, width, height, EDGES_ACTION.ZERO);
		assertTrue(true);
	}
}
