/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.SHINE;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link ShineFilter} functionality.
 */
public class ShineFilterTest {
	private ShineFilter shineFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		shineFilter = new ShineFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(shineFilter);
	}

	@Test
	public void testShineFilter() throws IOException {
		shineFilter.setShineColor(Color.WHITE.getRGB());
		BufferedImage result = shineFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHINE.to(), "bmp");
	}
	
	@Test
	public void testShineFilterDstNull() throws IOException {
		shineFilter.setShineColor(Color.WHITE.getRGB());
		BufferedImage result = shineFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		shineFilter.setAngle(angle);
		assertEquals(angle, shineFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		shineFilter.setDistance(distance);
		assertEquals(distance, shineFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat();
		shineFilter.setRadius(radius);
		assertEquals(radius, shineFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testSetBevel() {
		float bevel = TestUtils.genFloat();
		shineFilter.setBevel(bevel);
		assertEquals(bevel, shineFilter.getBevel(), EPSILON);
	}
	
	@Test
	public void testSetShineColor() {
		int shineColor = TestUtils.genInteger();
		shineFilter.setShineColor(shineColor);
		assertEquals(shineColor, shineFilter.getShineColor());
	}
	
	@Test
	public void testSetShadowOnly() {
		boolean shadowOnly = true;
		shineFilter.setShadowOnly(shadowOnly);
		assertTrue(shineFilter.getShadowOnly());
		
		// Case 2: false
		shadowOnly = false;
		shineFilter.setShadowOnly(shadowOnly);
		assertFalse(shineFilter.getShadowOnly());
	}
	
	@Test
	public void testSetBrightness() {
		float brightness = TestUtils.genFloat();
		shineFilter.setBrightness(brightness);
		assertEquals(brightness, shineFilter.getBrightness(), EPSILON);
	}
	
	@Test
	public void testSetSoftness() {
		float softness = TestUtils.genFloat();
		shineFilter.setSoftness(softness);
		assertEquals(softness, shineFilter.getSoftness(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(shineFilter.toString().contains(ShineFilter.class.getSimpleName()));
	}
}
