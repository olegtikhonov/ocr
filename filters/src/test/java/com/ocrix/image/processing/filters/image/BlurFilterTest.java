/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.BINARY_STAR;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests a {@link BlurFilter} functionality.
 */
public class BlurFilterTest {
	private static BlurFilter blurFilter = null;
	private static BufferedImage dest = null;

	@BeforeClass
	public static void setUp() throws Exception {
		blurFilter = new BlurFilter();
		dest = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(blurFilter);
	}

	@Test
	public void testFilter() {
		BufferedImage result = null, temp = dest;
		
		for (int i = 0; i < BINARY_STAR; i++) {
			temp = blurFilter.filter(temp, null);
			result = temp;
		}
		
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "BlurFilter",
				"tiff");
	}
	
	@Test
	public void testToString(){
		assertTrue(blurFilter.toString().contains(BlurFilter.class.getSimpleName()));
	}
}