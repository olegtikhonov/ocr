/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

/**
 * Defines a filter names.
 */
public enum Filters {

	CROP("CropFilter"),
	
	CHANNEL_MIX("ChannelMixFilter"),
	
	CHECK("CheckFilter"),
	
	CIRCLE("CircleFilter"),
	
	COLOR_HALFTONE("ColorHalfToneFilter"),
	
	COMPOUND("CompoundFilter"),
	
	COMPOSITE("CompositeFilter"),
	
	CONTOUR("ContourFilter"),
	
	AVERAGE("AverageFilter"),
	
	BICUBIC_SCALE("BicubicScaleFilter"),
	
	BLOCK("BlockFilter"),
	
	BOXBLUR("BoxBlurFilter"),
	
	BRUSHED_METAL("BrushedMetalFilter"),
	
	CELLULAR("CellularFilter"),
	
	CONTRAST("ContrastFilter"),
	
	CRYSTALLIZE("CrystallizeFilter"),
	
	CURL("CurlFilter"),
	
	CURVES("CurvesFilter"),
	
	DEINTERLACE("DeinterlaceFilter"),
	
	DESPECKLE("DespeckleFilter"),
	
	DIFFUSE("DiffuseFilter"),
	
	DIFFUSION("DiffusionFilter"),
	
	ERODE("ErodeFilter"),
	
	WARP("WarpFilter"),
	
	DILATE("DilateFilter"),
	
	DISPLACE("DisplaceFilter"),
	
	DISSOLVE("DissolveFilter"),
	
	DITHER("DitherFilter"),
	
	DOG("DoGFilter"),
	
	EDGE("EdgeFilter"),
	
	EMBOSS("EmbossFilter"),
	
	EQUALIZE("EqualizeFilter"),
	
	ERODEALPHA("ErodeAlphaFilter"),
	
	EXPOSURE("ExposureFilter"),
	
	FBM("FBMFilter"),
	
	FADE("FadeFilter"),
	
	FEEDBACK("FeedbackFilter"),
	
	FIELDWARP("FieldWarpFilter"),
	
	FILL("FillFilter"),
	
	FLARE("FlareFilter"),
	
	FLIP("FlipFilter"),
	
	FLUSH_3D("Flush3DFilter"),
	
	FOUR_COLOR("FourColorFilter"),
	
	GAIN("GainFilter"),
	
	GAMMA("GammaFilter"),
	
	GAUSSIAN("GaussianFilter"),
	
	OTSU("OtsuMethod"),
	
	GLINT("GlintFilter"),
	
	GLOW("GlowFilter"),
	
	GRADIENT("GradientFilter"),
	
	GRADIENT_WIPE("GradientWipeFilter"),
	
	GRAY("GrayFilter"),
	
	GRAYSCALE("GrayscaleFilter"),
	
	HSB_ADJUST("HSBAdjustFilter"),
	
	HALFTONE("HalftoneFilter"),
	
	HIGH_PASS("HighPassFilter"),
	
	IMAGE_COMBINING("ImageCombiningFilter"),
	
	INTERPOLATE("InterpolateFilter"),
	
	INVERT_ALPHA("InvertAlphaFilter"),
	
	SOBEL("SobelEdgeDetector"),
	
	INVERT("InvertFilter"),
	
	ITERATED("IteratedFilter"),
	
	JAVA_LN_F("JavaLnFFilter"),
	
	KALEIDOSCOPE("KaleidoscopeFilter"),
	
	KEY("KeyFilter"),
	
	GABOR("GaborFilter"),
	
	LAPLACIAN("LaplacianFilter"),
	
	LENS_BLUR("LensBlur"),
	
	LEVELS("LevelsFilter"),
	
	LIFE("LifeFilter"),
	
	LIGHT("LightFilter"),
	
	LOOKUP("LookupFilter"),
	
	MAP_COLORS("MapColorsFilter"),
	
	MAP("MapFilter"),
	
	MARBLE("MarbleFilter"),
	
	MARBLE_TEX("MarbleTexFilter"),
	
	MASK("MaskFilter"),
	
	MAXIMUM("MaximumFilter"),
	
	MEDIAN("MedianFilter"),
	
	MINIMUM("MinimumFilter"),
	
	MIRROR("MirrorFilter"),
	
	MOTION_BLUR("MotionBlurFilter"),
	
	MOTION_BLUR_OP("MotionBlurOpFilter"),
	
	NOISE("NoiseFilter"),
	
	OFFSET("OffsetFilter"),
	
	OIL("OilFilter"),
	
	OPACITY("OpacityFilter"),
	
	OUTLINE("OutlineFilter"),
	
	PERSPECTIVE("PerspectiveFilter"),
	
	PINCH("PinchFilter"),
	
	PLASMA("PlasmaFilter"),
	
	POINTILLIZE("PointillizeFilter"),
	
	POLAR("PolarFilter"),
	
	POSTERIZE("PosterizeFilter"),
	
	PREMULTIPLY("PremultiplyFilter"),
	
	QUANTIZE("QuantizeFilter"),
	
	QUILT("QuiltFilter"),
	
	RGB_ADJUST("RGBAdjustFilter"),
	
	RAYS("RaysFilter"),
	
	REDUCE_NOISE("ReduceNoiseFilter"),
	
	RENDER_TEXT("RenderTextFilter"),
	
	RESCALE("RescaleFilter"),
	
	RIPPLE("RippleFilter"),
	
	ROTATE("RotateFilter"),
	
	SATURATION("SaturationFilter"),
	
	SCALE("ScaleFilter"),
	
	SCRATCH("ScratchFilter"),
	
	SHADE("ShadeFilter"),
	
	SHADOW("ShadowFilter"),
	
	SHAPE("ShapeFilter"),
	
	SHARPEN("SharpenFilter"),
	
	SHATTER("ShatterFilter"),
	
	SHEAR("ShearFilter"),
	
	SHINE("ShineFilter"),
	
	SKELETON("SkeletonFilter"),
	
	SKELETON_ZHANG_SUEN_THINNING("SkeletonZhangSuenThinning"),
	
	SKY("SkyFilter"),
	
	SMART_BLUR("SmartBlurFilter"),
	
	SOLARIZE("SolarizeFilter"),
	
	SPARKLE("SparkleFilter"),
	
	SMEAR("SmearFilter"),
	
	SWIM("SwimFilter"),
	
	SWIZZLE("SwizzleFilter"),
	
	TEXTURE("TextureFilter"),
	
	THRESHOLD("ThresholdFilter");
	
	
	private String value;
	
	private Filters(String var){
		this.value = var;
	}
	
	/**
	 * Gets a textual representation of the filter.
	 * 
	 * @return filter description.
	 */
	public String to(){
		return this.value;
	}
}
