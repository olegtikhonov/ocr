/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.DIFFUSE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link DiffuseFilter} functionality.
 */
public class DiffuseFilterTest {

	private static DiffuseFilter diffuseFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		diffuseFilter = new DiffuseFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(diffuseFilter);
	}

	@Test
	public void testFilter() {
		diffuseFilter.setScale(2);
		BufferedImage result = diffuseFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DIFFUSE.to(), "png");
	}
	
	@Test
	public void testGetScale(){
		float scale = TestUtils.genFloat(QUAD);
		diffuseFilter.setScale(scale);
		assertEquals(scale, diffuseFilter.getScale(), EPSILON);
	}
	
	@Test
	public void testToString(){
		assertTrue(diffuseFilter.toString().contains(DiffuseFilter.class.getName()));
	}
}
