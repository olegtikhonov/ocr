/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.POSTERIZE;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link PosterizeFilter} functionality.
 */
public class PosterizeFilterTest {
	private PosterizeFilter posterizeFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		posterizeFilter = new PosterizeFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
	}

	@Test
	public void testCitor() {
		assertNotNull(posterizeFilter);
	}

	@Test
	public void testPosterizeFilter() {
		posterizeFilter.setNumLevels(4);
		BufferedImage result = posterizeFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + POSTERIZE.to(), "jpeg");
	}

	@Test
	public void testSetNumLevels() {
		int numLevels = TestUtils.genInteger();
		posterizeFilter.setNumLevels(numLevels);
		assertEquals(numLevels, posterizeFilter.getNumLevels());
	}

	@Test
	public void testInitialize() {
		posterizeFilter.initialize();
		assertTrue(true);
	}

	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(src.getWidth() / 2);
		int y = TestUtils.genInteger(src.getHeight() / 2);
		int rgb = src.getRGB(x, y);
		int result = posterizeFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(posterizeFilter.toString().contains(PosterizeFilter.class.getSimpleName()));
	}
}
