/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.MIRROR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link MirrorFilter} functionality. 
 */
public class MirrorFilterTest {

	private static MirrorFilter mirrorFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		mirrorFilter = new MirrorFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(mirrorFilter);
	}
	
	@Test
	public void testMirrorFilter(){
		BufferedImage result = mirrorFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MIRROR.to(), "png");
	}
	
	@Test
	public void testMirrorFilterDstNotNull(){
		BufferedImage result = mirrorFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MIRROR.to() + "_notNull", "bmp");
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		mirrorFilter.setAngle(angle);
		assertEquals(angle, mirrorFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		mirrorFilter.setDistance(distance);
		assertEquals(distance, mirrorFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRotation() {
		float rotation = TestUtils.genFloat();
		mirrorFilter.setRotation(rotation);
		assertEquals(rotation, mirrorFilter.getRotation(), EPSILON);
	}
	
	@Test
	public void testSetGap() {
		float gap = TestUtils.genFloat();
		mirrorFilter.setGap(gap);
		assertEquals(gap, mirrorFilter.getGap(), EPSILON);
	}
	
	@Test
	public void testSetOpacity() {
		float opacity = TestUtils.genFloat();
		mirrorFilter.setOpacity(opacity);
		assertEquals(opacity, mirrorFilter.getOpacity(), EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		mirrorFilter.setCentreY(centreY);
		assertEquals(centreY, mirrorFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(mirrorFilter.toString().contains(MirrorFilter.class.getSimpleName()));
	}
}
