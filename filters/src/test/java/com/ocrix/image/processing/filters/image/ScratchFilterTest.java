/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SCRATCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * @Test {@link ScratchFilter} functionality.
 */
public class ScratchFilterTest {
	private ScratchFilter scratchFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		scratchFilter = new ScratchFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(scratchFilter);
	}

	@Test
	public void testScratchFilter() {
		scratchFilter.setDensity(0.025f);
		scratchFilter.setColor(Color.RED.getRGB());
		BufferedImage result = scratchFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SCRATCH.to(), "gif");
	}
	
	// Case 2: dst = null
	@Test
	public void testScratchFilterDstNull() {
		scratchFilter.setDensity(0.025f);
		scratchFilter.setColor(Color.RED.getRGB());
		BufferedImage result = scratchFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testSetAngle() {
		 float angle = TestUtils.genFloat();
		 scratchFilter.setAngle(angle);
		 assertEquals(angle, scratchFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetAngleVariation() {
		float angleVariation = TestUtils.genFloat();
		scratchFilter.setAngleVariation(angleVariation);
		assertEquals(angleVariation, scratchFilter.getAngleVariation(), EPSILON);
	}
	
	@Test
	public void testSetDensity() {
		float density  = TestUtils.genFloat();
		scratchFilter.setDensity(density);
		assertEquals(density, scratchFilter.getDensity(), EPSILON);
	}
	
	@Test
	public void testSetLength() {
		float length  = TestUtils.genFloat();
		scratchFilter.setLength(length);
		assertEquals(length, scratchFilter.getLength(), EPSILON);
	}
	
	@Test
	public void testSetWidth() {
		float width  = TestUtils.genFloat();
		scratchFilter.setWidth(width);
		assertEquals(width, scratchFilter.getWidth(), EPSILON);
	}
	
	@Test
	public void testSetColor() {
		int color = Color.GREEN.getRGB();
		scratchFilter.setColor(color);
		assertEquals(color, scratchFilter.getColor());
	}
	
	@Test
	public void testSetSeed() {
		int seed = Color.GREEN.getRGB();
		scratchFilter.setSeed(seed);
		assertEquals(seed, scratchFilter.getSeed());
	}
	
	@Test
	public void testToString() {
		assertTrue(scratchFilter.toString().contains(ScratchFilter.class.getSimpleName()));
	}
}
