/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.SMEAR;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.SmearShape;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link SmearFilter} functionality. 
 */
public class SmearFilterTest {

	private SmearFilter smearFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		smearFilter = new SmearFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(smearFilter);
	}
	
	
	@Test
	public void testSmearFilter(){
		smearFilter.setShape(SmearShape.SQUARES);
		smearFilter.setDensity(0.015f);
		BufferedImage result = smearFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SMEAR.to(), "png");
	}
	
	@Test
	public void testSetShape() {
		SmearShape shape = SmearShape.LINES;
		smearFilter.setShape(shape);
		assertEquals(shape, smearFilter.getShape());
	}
	
	@Test
	public void testSetDistance() {
        int distance = TestUtils.genInteger();
		smearFilter.setDistance(distance);
		assertEquals(distance, smearFilter.getDistance());
	}
	
	@Test
	public void testSetDensity() {
        float density = TestUtils.genFloat();
		smearFilter.setDensity(density);
		assertEquals(density, smearFilter.getDensity(), EPSILON);
	}
	
	@Test
	public void testSetScatter() {
        float scatter = TestUtils.genFloat();
		smearFilter.setScatter(scatter);
		assertEquals(scatter, smearFilter.getScatter(), EPSILON);
	}
	
	@Test
	public void testSetAngle() {
        float angle = TestUtils.genFloat();
		smearFilter.setAngle(angle);
		assertEquals(angle, smearFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetMix() {
        float mix = TestUtils.genFloat();
		smearFilter.setMix(mix);
		assertEquals(mix, smearFilter.getMix(), EPSILON);
	}
	
	@Test
	public void testSetFadeout() {
        int fadeout = TestUtils.genInteger();
		smearFilter.setFadeout(fadeout);
		assertEquals(fadeout, smearFilter.getFadeout());
	}
	
	@Test
	public void testSetBackground() {
        boolean background = true;
		smearFilter.setBackground(background);
		assertTrue(background);
	}
	
	@Test
	public void testRandomize() {
		smearFilter.randomize();
		assertTrue(true);
	}
	
	@Test
	public void testFilterPixel() {
		//filterPixels
		int[] inPixels = TestUtils.convert2Dto1D(TestUtils.convertTo2DUsingGetRGB(src));
		Rectangle transformedSpace = new Rectangle(0, 0, src.getWidth(), src.getHeight());
		
		// Case 1: CROSSES
		smearFilter.setShape(SmearShape.CROSSES);
		
		int[] outPixels = smearFilter.filterPixels(src.getWidth(), src.getHeight(), inPixels, transformedSpace);
		int resultCounter = TestUtils.diffCounter(inPixels, outPixels);
		assertTrue(resultCounter > 0);
		
		// Case 2: LINES
		smearFilter.setShape(SmearShape.LINES);
		outPixels = smearFilter.filterPixels(src.getWidth(), src.getHeight(), inPixels, transformedSpace);
		resultCounter = TestUtils.diffCounter(inPixels, outPixels);
		assertTrue(resultCounter > 0);
	}
	
	@Test
	public void testGetBackground() {
		// Case: true
		smearFilter.setBackground(true);
		assertTrue(smearFilter.getBackground());
		
		// Case: false
		smearFilter.setBackground(false);
		assertFalse(smearFilter.getBackground());
	}
	
	@Test
	public void testToString() {
		assertTrue(smearFilter.toString().contains(SmearFilter.class.getSimpleName()));
	}
}
