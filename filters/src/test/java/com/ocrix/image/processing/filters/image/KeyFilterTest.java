/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.KEY;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link KeyFilter} functionality.
 */
public class KeyFilterTest {

	private KeyFilter keyFilter = null;
	private BufferedImage src = null;
	private BufferedImage dst = null;
	private BufferedImage cleanImg = null;
	
	@Before
	public void setUp() throws Exception {
		keyFilter = new KeyFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dst = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
		cleanImg = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(keyFilter);
	}
	
	@Test
	public void testKeyFilter(){
		keyFilter.setBTolerance(ALPHA);
		keyFilter.setHTolerance(ALPHA);
		keyFilter.setSTolerance(ALPHA * ALPHA);
		keyFilter.setDestination(dst);
		keyFilter.setCleanImage(cleanImg);
		
		BufferedImage result = keyFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + KEY.to(), "bmp");
	}
	
	@Test
	public void testGetHTolerance() {
		float hTolerance = TestUtils.genFloat();
		keyFilter.setHTolerance(hTolerance);
		assertEquals(hTolerance, keyFilter.getHTolerance(), EPSILON);
	}
	
	@Test
	public void testGetSTolerance() {
		float sTolerance = TestUtils.genFloat();
		keyFilter.setSTolerance(sTolerance);
		assertEquals(sTolerance, keyFilter.getSTolerance(), EPSILON);
	}
	
	@Test
	public void testGetBTolerance() {
		float bTolerance = TestUtils.genFloat();
		keyFilter.setBTolerance(bTolerance);
		assertEquals(bTolerance, keyFilter.getBTolerance(), EPSILON);
	}
	
	@Test
	public void testGetDestination() {
		keyFilter.setDestination(src);
		assertEquals(src, keyFilter.getDestination());
	}
	
	@Test
	public void testGetCleanImage() {
		keyFilter.setCleanImage(src);
		assertEquals(src, keyFilter.getCleanImage());
	}
	
	@Test
	public void testToString() {
		assertTrue(keyFilter.toString().contains(KeyFilter.class.getName()));
	}
}
