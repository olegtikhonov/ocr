/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.DIME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.HENDECAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.NUMBER_OF_KNUTS_IN_ONE_SICKLE;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ImageMath} functionality.
 */
public class ImageMathTest {

	@Test
	public void testBias() {
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		float result = ImageMath.bias(a, b);
		assertTrue(result != 0);
	}

	@Test
	public void testGain() {
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		float result = ImageMath.gain(a, b);
		assertTrue(result != 0);
	}

	@Test
	public void testStep() {
		// Case: random
		float a = TestUtils.genFloat();
		float x = TestUtils.genFloat();
		float result = ImageMath.step(a, x);

		// Case x > a
		x = 1.3f;
		a = 1.2f;
		result = ImageMath.step(a, x);
		assertEquals(1.0f, result, EPSILON);

		// Case: x < a
		x = 1.33f;
		a = 1.42f;
		result = ImageMath.step(a, x);
		assertEquals(0.0f, result, EPSILON);
	}

	@Test
	public void testPulse() {
		// (x < a || x >= b) ? 0.0f : 1.0f;
		// Case: random
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		float x = TestUtils.genFloat();
		float result = ImageMath.pulse(a, b, x);
		assertTrue(true);

		// x < a
		a = 1.2f;
		x = 1.1f;
		result = ImageMath.pulse(a, b, x);
		assertEquals(0.0f, result, EPSILON);

		// x >= b
		b = 1.0f;
		result = ImageMath.pulse(a, b, x);
		assertEquals(0.0f, result, EPSILON);

		// x > a || x < b
		a = 0.9f;
		x = 1.23f;
		b = 1.24f;

		result = ImageMath.pulse(a, b, x);
		assertEquals(1.0f, result, EPSILON);
	}

	@Test
	public void testSmoothPulse() {
		// Random case.
		float a1 = TestUtils.genFloat();
		float a2 = TestUtils.genFloat();
		float b1 = TestUtils.genFloat();
		float b2 = TestUtils.genFloat();
		float x = TestUtils.genFloat();

		ImageMath.smoothPulse(a1, a2, b1, b2, x);
		assertTrue(true);

		// case: x < a1 || x >= b2
		x = 0.2f;
		a1 = 0.3f;
		b2 = 0.12f;
		float result = ImageMath.smoothPulse(a1, a2, b1, b2, x);
		assertEquals(0.0f, result, EPSILON);

		// if (x >= a2) & (x < b1)
		a1 = 0.19f;
		b1 = 0.21f;
		b2 = 0.21f;
		x = 0.2f;
		a2 = 0.16f;
		result = ImageMath.smoothPulse(a1, a2, b1, b2, x);
		assertEquals(1.0f, result, EPSILON);

		// if (x >= a2) & (x > b1)
		x = 0.25f;
		a1 = 0.19f;
		b1 = 0.21f;
		b2 = 0.26f;
		result = ImageMath.smoothPulse(a1, a2, b1, b2, x);
		assertEquals(0.10399985f, result, EPSILON);
	}

	@Test
	public void testSmoothStep() {
		// Random case
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		float x = TestUtils.genFloat();

		float result = ImageMath.smoothStep(a, b, x);
		assertTrue(true);

		// case: if (x < a) return 0;
		x = 1.2f;
		a = 1.23f;
		result = ImageMath.smoothStep(a, b, x);
		assertEquals(0.0f, result, EPSILON);

		// if (x >= b) return 1;
		a = 1.19f;
		b = a;
		result = ImageMath.smoothStep(a, b, x);
		assertEquals(1.0f, result, EPSILON);

		// if (x < b)
		b = 1.25f;
		result = ImageMath.smoothStep(a, b, x);
		assertEquals(0.07407408f, result, EPSILON);
	}

	@Test
	public void testCircleUp() {
		float x = TestUtils.genFloat();
		ImageMath.circleUp(x);
		assertTrue(true);
	}

	@Test
	public void testCircleDown() {
		float x = TestUtils.genFloat();
		ImageMath.circleDown(x);
		assertTrue(true);
	}

	@Test
	public void testClampFloatFloatFloat() {
		float x = TestUtils.genFloat();  
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		
		ImageMath.clamp(x, a, b);
		assertTrue(true);
		//(x < a) ? a : (x > b) ? b : x;
		
		// Case: x < a return a;
		x = 1.2f;
		a = 1.23f;
		float result = ImageMath.clamp(x, a, b);
		assertEquals(a, result, EPSILON);
		
		// Case x > a & x > b --> b
		x = 1.25f;
		a = 1.23f;
		b = a;
		result = ImageMath.clamp(x, a, b);
		assertEquals(b, result, EPSILON);
		
		// Case x > a & x < b --> x
		x = 1.25f;
		a = 1.23f;
		b = 1.26f;
		result = ImageMath.clamp(x, a, b);
		assertEquals(x, result, EPSILON);
	}

	@Test
	public void testClampIntIntInt() {
		int x = TestUtils.genInteger(); 
		int a = TestUtils.genInteger(); 
		int b = TestUtils.genInteger();
		
		int result = ImageMath.clamp(x, a, b);
		assertTrue(true);
		
		//(x < a) ? a : (x > b) ? b : x;
        // Case: x < a return a;
		x = 2;
		a = 3;
		result = ImageMath.clamp(x, a, b);
		assertEquals(a, result);
		
		// Case x > a & x > b --> b
		x = 2;
		a = 1;
		b = a;
		result = ImageMath.clamp(x, a, b);
		assertEquals(b, result);
		
		// Case x > a & x < b --> x
		x = 2;
		a = 1;
		b = 3;
		result = ImageMath.clamp(x, a, b);
		assertEquals(x, result);
	}

	@Test
	public void testModDoubleDouble() {
		// Random case
		double a = TestUtils.genDouble(); 
		double b = TestUtils.genDouble();
		ImageMath.mod(a, b);
		assertTrue(true);
		
		//TODO: add more test cases.
	}

	@Test
	public void testModFloatFloat() {
		float a = TestUtils.genFloat(); 
		float b = TestUtils.genFloat();
		
		ImageMath.mod(a, b);
		assertTrue(true);
		
		// case: a < 0
		a = -4.0f;
		b = 3.5f;
		float result = ImageMath.mod(a, b);
		assertEquals(3, result, EPSILON);
	}

	@Test
	public void testModIntInt() {
		int a = TestUtils.genInteger(); 
		int b = TestUtils.genInteger(); 
		
		ImageMath.mod(a, b);
		assertTrue(true);
		
		// case: a < 0
		a = -4;
		b = 3;
		int result = ImageMath.mod(a, b);
		assertEquals(2, result);
	}

	@Test
	public void testTriangle() {
		// Random case
		float x = TestUtils.genFloat();
		ImageMath.triangle(x);
		assertTrue(true);
		//case r < 0.5
		x = 0.06333822f;
		float result = ImageMath.triangle(x);
		assertEquals(0.12667644f, result, EPSILON);
		
		//case r > 0.5
		x = 0.7172438f;
		result = ImageMath.triangle(x);
		assertEquals(0.5655124f, result, EPSILON);
	}

	@Test
	public void testLerpFloatFloatFloat() {
		float t = TestUtils.genFloat();
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat();
		ImageMath.lerp(t, a, b);
		assertTrue(true);
	}

	@Test
	public void testLerpFloatIntInt() {
		float t = TestUtils.genFloat();
		int a = TestUtils.genInteger();
		int b = TestUtils.genInteger();
		ImageMath.lerp(t, a, b);
		assertTrue(true);
	}

	@Test
	public void testMixColors() {
		float t = TestUtils.genFloat(); 
		int rgb1 = TestUtils.genInteger(QUAD); 
		int rgb2 = TestUtils.genInteger(QUAD); 
		int mixedColor = ImageMath.mixColors(t, rgb1, rgb2);
		assertTrue(mixedColor != 0);
	}

	@Test
	public void testBilinearInterpolate() {
		float x = TestUtils.genFloat();
		float y = TestUtils.genFloat(); 
		int nw = TestUtils.genInteger(QUAD);
		int ne = TestUtils.genInteger(QUAD);
		int sw = TestUtils.genInteger(QUAD); 
		int se = TestUtils.genInteger(QUAD);
		
		int biResult = ImageMath.bilinearInterpolate(x, y, nw, ne, sw, se);
		assertTrue(biResult != 0);
	}

	@Test
	public void testBrightnessNTSC() {
		int rgb = QUAD;
		int bNTSCResult = ImageMath.brightnessNTSC(rgb);
		assertEquals(NUMBER_OF_KNUTS_IN_ONE_SICKLE, bNTSCResult);
	}

	@Test
	public void testSplineFloatIntFloatArray() {
		float x = 23f; 
		int numKnots = DIME; 
		float[] knots = {0.88349366f, 0.5389428f,  0.8357008f,  0.7118448f,  0.653182f, 
				         0.076886356f,0.47518772f, 0.32955527f, 0.44830465f, 0.6558485f };
		//case: numSpans >= 1 where numSpans = numKnots - 3; span = x & span > numKnots - 4
		float result = ImageMath.spline(x, numKnots, knots);
		assertEquals(0.44830465, result, EPSILON);
	}

	@Test
	public void testSplineFloatIntIntArrayIntArray() {
		float x = 17999900f; 
		int numKnots = DIME;
		int[] xknots = { 94474, 41699, 281880, 417710, 293419, 416202, 122833, 452455, 175249, 485136 };
		int[] yknots = { 308220, 362590, 126128, 3518, 488231, 83333, 129090, 91237, 427347, 91082 };
		float result = ImageMath.spline(x, numKnots, xknots, yknots);
		assertEquals(1.35537721344E11, result, EPSILON);
	}

	@Test
	public void testColorSplineFloatIntIntArray() {
		float x = 13; 
		int numKnots = HENDECAGON; 
		int[] knots = { 9521, 38054, 77222, 38554, 204293, 222657, 422665, 201200, 316390, 15970, 395047 };
		int result = ImageMath.colorSpline(x, numKnots, knots);
		assertEquals(15970, result);
	}

	@Test
	public void testColorSplineIntIntIntArrayIntArray() {
		int x = HEPTAGON; 
		int numKnots = DIME; 
		int[] xknots = { 281133, 217864, 315277, 470989, 316457, 42006, 342681 };
        int[] yknots = { 267723, 268067, 397052, 286541, 17943, 479676, 14015 };
        int result = ImageMath.colorSpline(x, numKnots, xknots, yknots);
        assertEquals(268067, result);
	}

	@Test
	public void testResample() {
		int[] source = TestUtils.fillArray(HEPTAGON); 
		int[] dest  = TestUtils.fillArray(HEPTAGON);
		int length = HEPTAGON;
        int offset = 0;
        int stride = TestUtils.genInteger();
        float[] out = TestUtils.generateFloatArray(HEPTAGON);
        
        ImageMath.resample(source, dest, length, offset, stride, out);
        assertTrue(true);
	}

	@Test
	public void testPremultiply() {
		int[] p = { 161, 44, 203, 16, 61, 242, 0, 10, 16, 131 };
		int offset = 0;
		int length = DIME;
		
		ImageMath.premultiply(p, offset, length);
		assertTrue(true);
		
	}

	@Test
	public void testUnpremultiply() {
		int[] p = { 173, 44, 97, 194, 126, 41, 253, 249, 153, 7 };
		int offset = 0;
		int length = DIME;
		ImageMath.unpremultiply(p, offset, length);
	}
}
