/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import java.awt.color.ColorSpace;

/**
 * Defines a color space stab. Uses for unit tests.
 */
public class MockColorSpace extends ColorSpace {

	/* finger print */
	private static final long serialVersionUID = -3683630687261576406L;

	public MockColorSpace(int type, int numcomponents) {
		super(type, numcomponents);
	}

	@Override
	public float[] toRGB(float[] colorvalue) {
		return colorvalue;
	}

	@Override
	public float[] fromRGB(float[] rgbvalue) {
		return rgbvalue;
	}

	@Override
	public float[] toCIEXYZ(float[] colorvalue) {
		return colorvalue;
	}

	@Override
	public float[] fromCIEXYZ(float[] colorvalue) {
		return colorvalue;
	}
}
