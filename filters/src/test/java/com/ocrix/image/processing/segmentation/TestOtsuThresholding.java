/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.segmentation;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.*;
import static com.ocrix.image.processing.filters.Filters.OTSU;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;

/**
 * 
 * Tests Otsu thresholding.
 */
public class TestOtsuThresholding {

	private OtsuThresholding otsuThresholding;
	
	@Before
	public void setUp() throws Exception {
		otsuThresholding = new OtsuThresholding();
	}

	@Test
	public void testGetOtsuThreshold() throws IOException {
		BufferedImage original = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		otsuThresholding.getOtsuThreshold(getGreyArray(original));
		
		assertTrue(true);
		
		int threshold = otsuThresholding.otsuTreshold(original);
		assertEquals(196, threshold);
		BufferedImage result = binarize(original, threshold);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + OTSU.to(), "png");
		
	}

	private int[] getGreyArray(BufferedImage bi) {
		int[] result = new int[bi.getWidth() * bi.getHeight()];
		
		for(int i = 0; i < bi.getWidth(); i++) {
			for(int j = 0; j < bi.getHeight(); j++){
				int rgb = bi.getRGB(i, j);
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb & 0xFF);
				int gray = (r + g + b) / 3;
				result[i+j] = gray;
			}
		}
		
		return result;
	}
	
	private static BufferedImage binarize(BufferedImage original, int threshold) {

		int red;
		int newPixel;
		BufferedImage binarized = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

		for (int i = 0; i < original.getWidth(); i++) {
			for (int j = 0; j < original.getHeight(); j++) {

				// Gets pixels
				red = new Color(original.getRGB(i, j)).getRed();
				int alpha = new Color(original.getRGB(i, j)).getAlpha();
				if (red > threshold) {
					newPixel = 255;
				} else {
					newPixel = 0;
				}
				newPixel = colorToRGB(alpha, newPixel, newPixel, newPixel);
				binarized.setRGB(i, j, newPixel);

			}
		}
		
		return binarized;
	}
	
	
    // Converts R, G, B, Alpha to standard 8 bit
	private static int colorToRGB(int alpha, int red, int green, int blue) {

		int newPixel = 0;
		newPixel += alpha;
		newPixel = newPixel << 8;
		newPixel += red;
		newPixel = newPixel << 8;
		newPixel += green;
		newPixel = newPixel << 8;
		newPixel += blue;

		return newPixel;
	}
}
