/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.INVERT_ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link InvertAlphaFilter} functionality.
 */
public class InvertAlphaFilterTest {

	private InvertAlphaFilter invertAlphaFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		invertAlphaFilter = new InvertAlphaFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(invertAlphaFilter);
	}
	
	
	@Test
	public void testInvertAlphaFilter() throws IOException{
		BufferedImage result = invertAlphaFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + INVERT_ALPHA.to(), "jpeg");
	}
	
	@Test
	public void testFilterRGB() {
		int x = 5; //TestUtils.genInteger(src.getHeight());
		int y = 5; //TestUtils.genInteger(src.getWidth());
		int rgb = src.getRGB(x, y);
		int result = invertAlphaFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(invertAlphaFilter.toString().contains(InvertAlphaFilter.class.getName()));
	}
}
