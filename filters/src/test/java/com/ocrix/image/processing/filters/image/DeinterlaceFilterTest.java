/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.DEINTERLACE;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link DeinterlaceFilter} functionality.
 */
public class DeinterlaceFilterTest {

	private static DeinterlaceFilter deinterlaceFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		deinterlaceFilter = new DeinterlaceFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(deinterlaceFilter);
	}

	@Test
	public void testDeinterlaceFilterEvenMode() {
		deinterlaceFilter.setMode(DeinterlaceFilter.OPS.EVEN.ordinal());
		BufferedImage result = deinterlaceFilter.filter(src, null);
		assertNotNull(result);
	}

	@Test
	public void testDeinterlaceFilterOddMode() {
		deinterlaceFilter.setMode(DeinterlaceFilter.OPS.ODD.ordinal());
		assertEquals(DeinterlaceFilter.OPS.ODD.ordinal(), deinterlaceFilter.getMode());
		BufferedImage result = deinterlaceFilter.filter(src, null);
		assertNotNull(result);
	}

	@Test
	public void testDeinterlaceFilterAverageMode() {
		deinterlaceFilter.setMode(DeinterlaceFilter.OPS.AVERAGE.ordinal());
		assertEquals(DeinterlaceFilter.OPS.AVERAGE.ordinal(), deinterlaceFilter.getMode());
		BufferedImage result = deinterlaceFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DEINTERLACE.to(), "bmp");
	}
	
	@Test
	public void testToString(){
		assertTrue(deinterlaceFilter.toString().contains(DeinterlaceFilter.class.getName()));
	}
}
