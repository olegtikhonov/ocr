/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.LOOKUP;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link LookupFilter} functionality. 
 */
public class LookupFilterTest {

	private static LookupFilter lookupFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		lookupFilter = new LookupFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(lookupFilter);
	}
	
	
	@Test
	public void testLookupFilter(){
		BufferedImage result = lookupFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + LOOKUP.to(), "jpeg");
	}
	
	@Test
	public void testLookupFilterColorMap() {
		Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
		lookupFilter = new LookupFilter(colormap);
		
		BufferedImage result = lookupFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + LOOKUP.to(), "png");
	}
	
	@Test
	public void testSetColormap() {
		Colormap spectrum = new SpectrumColormap();
		lookupFilter.setColormap(spectrum);
		assertEquals(spectrum, lookupFilter.getColormap());
	}
	
	@Test
	public void testToString() {
		assertTrue(lookupFilter.toString().contains(LookupFilter.class.getSimpleName()));
	}
}
