/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GRADIENT;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;

/**
 * Tests {@link GradientFilter} functionality.
 */
public class GradientFilterTest {
	private GradientFilter gradientFilter;
	private BufferedImage image;

	@Before
	public void setUp() throws Exception {
		gradientFilter = new GradientFilter();
		image = ImageIO.read(new File(PREFIXPATH + TUX));
	}

	@Test
	public void testGradientFilter() {
		Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
		gradientFilter.setColormap(colormap);
		gradientFilter.setType(GradientFilter.BICONICAL);
		BufferedImage result = gradientFilter.filter(image, null);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GRADIENT.to(), "png");
	}

	@Test
	public void testGradientFilterPointPointIntIntBooleanIntInt() {
		Point a = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getWidth()));
		Point b = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getHeight()));
		GradientFilter grad = new GradientFilter(a, b, Color.BLACK.getRGB(), Color.GRAY.getRGB(), true, GradientFilter.RADIAL, GradientFilter.INT_SMOOTH);
		assertNotNull(grad);
	}

	@Test
	public void testSetPoint1() {
		Point a = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getWidth()));
		gradientFilter.setPoint1(a);
		assertEquals(a, gradientFilter.getPoint1());
	}

	@Test
	public void testGetPoint1() {
		testSetPoint1();
	}

	@Test
	public void testSetPoint2() {
		Point a = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getWidth()));
		gradientFilter.setPoint2(a);
		assertEquals(a, gradientFilter.getPoint2());
	}

	@Test
	public void testGetPoint2() {
		testSetPoint2();
	}

	@Test
	public void testSetType() {
		/* case: BICONICAL */
		gradientFilter.setType(GradientFilter.BICONICAL);
		assertEquals(GradientFilter.BICONICAL, gradientFilter.getType());
		
		/* case: BILINEAR */
		gradientFilter.setType(GradientFilter.BILINEAR);
		assertEquals(GradientFilter.BILINEAR, gradientFilter.getType());
		
		/* case: LINEAR */
		gradientFilter.setType(GradientFilter.LINEAR);
		assertEquals(GradientFilter.LINEAR, gradientFilter.getType());
		
		/* case: RADIAL */
		gradientFilter.setType(GradientFilter.RADIAL);
		assertEquals(GradientFilter.RADIAL, gradientFilter.getType());
		
		/* case: CONICAL */
		gradientFilter.setType(GradientFilter.CONICAL);
		assertEquals(GradientFilter.CONICAL, gradientFilter.getType());
		
		/* case: SQUARE */
		gradientFilter.setType(GradientFilter.SQUARE);
		assertEquals(GradientFilter.SQUARE, gradientFilter.getType());
	}

	@Test
	public void testGetType() {
		testSetType();
	}

	@Test
	public void testSetInterpolation() {
		/* case: LINEAR interpolation. */
		gradientFilter.setInterpolation(GradientFilter.INT_LINEAR);
		assertEquals(GradientFilter.INT_LINEAR, gradientFilter.getInterpolation());
		
		/* case: CIRCLE_DOWN interpolation. */
		gradientFilter.setInterpolation(GradientFilter.INT_CIRCLE_DOWN);
		assertEquals(GradientFilter.INT_CIRCLE_DOWN, gradientFilter.getInterpolation());
		
		/* case: CIRCLE_UP interpolation. */
		gradientFilter.setInterpolation(GradientFilter.INT_CIRCLE_UP);
		assertEquals(GradientFilter.INT_CIRCLE_UP, gradientFilter.getInterpolation());
		
		/* case: SMOOTH interpolation. */
		gradientFilter.setInterpolation(GradientFilter.INT_SMOOTH);
		assertEquals(GradientFilter.INT_SMOOTH, gradientFilter.getInterpolation());
	}

	@Test
	public void testGetInterpolation() {
		testSetInterpolation();
	}

	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		gradientFilter.setAngle(angle);
		assertEquals(angle, gradientFilter.getAngle(), EPSILON);
	}

	@Test
	public void testGetAngle() {
		testSetAngle();
	}

	@Test
	public void testSetColormap() {
		Colormap colormap = new LinearColormap(Color.GRAY.getRGB(), Color.YELLOW.getRGB());
		gradientFilter.setColormap(colormap);
		assertEquals(colormap, gradientFilter.getColormap());
	}

	@Test
	public void testGetColormap() {
		testSetColormap();
	}

	@Test
	public void testSetPaintMode() {
		gradientFilter.setPaintMode(PixelUtils.HUE);
		assertEquals(PixelUtils.HUE, gradientFilter.getPaintMode());
	}

	@Test
	public void testGetPaintMode() {
		testSetPaintMode();
	}

	@Test
	public void testFilter() {
		gradientFilter.setColormap(new LinearColormap(Color.BLUE.getRGB(), Color.PINK.getRGB()));
		BufferedImage result = gradientFilter.filter(image, image);
		assertNotNull(result);
	}

	@Test
	public void testToString() {
		assertTrue(gradientFilter.toString().contains(GradientFilter.class.getName()));
	}

	@Test
	public void testGetColor1() {
		assertTrue(gradientFilter.getColor1() != 0);
	}

	@Test
	public void testGetColor2() {
		assertTrue(gradientFilter.getColor2() != 0);
	}
	
	@Test
	public void testRepeatGradient() {
		int[] pixels = TestUtils.fillArray(TestParams.HEPTAGON);
		int w = TestUtils.genInteger(); 
		int h = TestUtils.genInteger(); 
		float rowrel = TestUtils.genFloat();
		float dx = TestUtils.genFloat(); 
		float dy = TestUtils.genFloat();
		
		try {
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("repeatGradient", new Class[] {pixels.getClass(), Integer.TYPE, Integer.TYPE, Float.TYPE, Float.TYPE, Float.TYPE});
			repeatGrad.setAccessible(true);
			repeatGrad.invoke(gradientFilter, pixels, w, h, rowrel, dx, dy);
			assertTrue(true);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testRadialGradient() {
		int[] pixels = TestUtils.fillArray(TestParams.HEPTAGON);
		int y = TestUtils.genInteger(); 
		int w = TestUtils.genInteger(); 
		int h = TestUtils.genInteger(); 
		try {
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("radialGradient", new Class[] {pixels.getClass(), Integer.TYPE, Integer.TYPE, Integer.TYPE});
			repeatGrad.setAccessible(true);
			repeatGrad.invoke(gradientFilter, pixels, y, w, h);
			assertTrue(true);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testSquareGradient() {
		int[] pixels = TestUtils.fillArray(TestParams.HEPTAGON);
		int y = TestUtils.genInteger(); 
		int w = TestUtils.genInteger(); 
		int h = TestUtils.genInteger(); 
		try {
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("squareGradient", new Class[] {pixels.getClass(), Integer.TYPE, Integer.TYPE, Integer.TYPE});
			repeatGrad.setAccessible(true);
			repeatGrad.invoke(gradientFilter, pixels, y, w, h);
			assertTrue(true);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testConicalGradient() {
		int[] pixels = TestUtils.fillArray(TestParams.HEPTAGON);
		int y = TestUtils.genInteger(); 
		int w = TestUtils.genInteger(); 
		int h = TestUtils.genInteger(); 
		try {
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("conicalGradient", new Class[] {pixels.getClass(), Integer.TYPE, Integer.TYPE, Integer.TYPE});
			repeatGrad.setAccessible(true);
			repeatGrad.invoke(gradientFilter, pixels, y, w, h);
			assertTrue(true);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testMap() {
		float v = TestUtils.genFloat(); 
		try {
			
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("map", new Class[] {Float.TYPE});
			repeatGrad.setAccessible(true);
			
			// Case1: repeat = true & INT_CIRCLE_UP
			Point a = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getWidth()));
			Point b = new Point(TestUtils.genInteger(image.getHeight()), TestUtils.genInteger(image.getHeight()));
			GradientFilter gradientFilter = new GradientFilter(a, b, Color.BLACK.getRGB(), Color.GRAY.getRGB(), true, GradientFilter.RADIAL, GradientFilter.INT_SMOOTH);
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			gradientFilter.setInterpolation(GradientFilter.INT_CIRCLE_UP);
			repeatGrad.invoke(gradientFilter, v);
			assertTrue(true);
			//Case2: INT_CIRCLE_DOWN
			gradientFilter.setInterpolation(GradientFilter.INT_CIRCLE_DOWN);
			repeatGrad.invoke(gradientFilter, v);
			assertTrue(true);
			
			//Case3: INT_SMOOTH
			gradientFilter.setInterpolation(GradientFilter.INT_SMOOTH);
			repeatGrad.invoke(gradientFilter, v);
			assertTrue(true);
			
			
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testDistance() {
		float a = TestUtils.genFloat();
		float b = TestUtils.genFloat(); 
		try {
			Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.BLUE.getRGB());
			gradientFilter.setColormap(colormap);
			Method repeatGrad = GradientFilter.class.getDeclaredMethod("distance", new Class[] {Float.TYPE, Float.TYPE});
			repeatGrad.setAccessible(true);
			repeatGrad.invoke(gradientFilter, a, b);
			assertTrue(true);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
}
