/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.QUILT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * 
 */
public class QuiltFilterTest {
	private QuiltFilter quiltFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		quiltFilter = new QuiltFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(quiltFilter);
	}
	
	@Test
	public void testQuiltFilter(){
		BufferedImage result = quiltFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + QUILT.to(), "bmp");
	}
	
	@Test
	public void testRandomize() {
		quiltFilter.randomize();
		assertTrue(true);
	}
	
	@Test
	public void testSetA() {
		float a = TestUtils.genFloat();
		quiltFilter.setA(a);
		assertEquals(a, quiltFilter.getA(), EPSILON);
	}
	
	@Test
	public void testSetIterations() {
		int iterations = TestUtils.genInteger();
		quiltFilter.setIterations(iterations);
		assertEquals(iterations, quiltFilter.getIterations());
	}
	
	@Test
	public void testSetB() {
		float b = TestUtils.genFloat();
		quiltFilter.setB(b);
		assertEquals(b, quiltFilter.getB(), EPSILON);
	}
	
	@Test
	public void testSetC() {
		float c = TestUtils.genFloat();
		quiltFilter.setC(c);
		assertEquals(c, quiltFilter.getC(), EPSILON);
	}
	
	@Test
	public void testSetD() {
		float d = TestUtils.genFloat();
		quiltFilter.setD(d);
		assertEquals(d, quiltFilter.getD(), EPSILON);
	}
	
	@Test
	public void testSetK() {
		int k = TestUtils.genInteger();
		quiltFilter.setK(k);
		assertEquals(k, quiltFilter.getK(), EPSILON);
	}
	
	@Test
	public void testSetColormap() {
		Colormap colormap = new LinearColormap();
		quiltFilter.setColormap(colormap);
		assertEquals(colormap, quiltFilter.getColormap());
	}
	
	@Test
	public void testToString() {
		assertTrue(quiltFilter.toString().contains(QuiltFilter.class.getSimpleName()));
	}
}
