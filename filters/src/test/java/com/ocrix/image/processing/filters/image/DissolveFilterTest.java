/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.DISSOLVE;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.CONRTST;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link DissolveFilter} functionality.
 */
public class DissolveFilterTest {
	/* Test member declarations */
	private static DissolveFilter dissolveFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		dissolveFilter = new DissolveFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(dissolveFilter);
	}
	
	@Test
	public void testDissolveFilter() throws Exception {
		dissolveFilter.setDensity(ALPHA);
		dissolveFilter.setSoftness(ALPHA);
		BufferedImage result = dissolveFilter.filter(ImageIO.read(new File(PREFIXPATH + CONRTST)), src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DISSOLVE.to(), "bmp");
	}
	
	@Test
	public void testGetDensity() {
		float density = TestUtils.genFloat();
		dissolveFilter.setDensity(density);
		assertEquals(density, dissolveFilter.getDensity(), EPSILON);
	}
	
	@Test
	public void testgetSoftness() {
		float softness = TestUtils.genFloat();
		dissolveFilter.setSoftness(softness);
		assertEquals(softness, dissolveFilter.getSoftness(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(dissolveFilter.toString().contains(DissolveFilter.class.getName()));
	}
}
