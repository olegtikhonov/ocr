package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.EQUALIZE;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class EqualizeFilterTest {

	private static EqualizeFilter equalizeFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		equalizeFilter = new EqualizeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(equalizeFilter);
	}
	
	@Test
	public void testEqualizeFilter() throws IOException{
		BufferedImage result = equalizeFilter.filter(src, ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + EQUALIZE.to(), "tiff");
	}
	
	@Test
	public void testFilterPixels() {
		int x = src.getMinX();
		int y = src.getMinY();
		int w = src.getWidth();
		int[] inPixels = null;
		inPixels = src.getRaster().getPixels(x, y, w, 1, inPixels);
		equalizeFilter.filterPixels(src.getWidth(), src.getHeight(), inPixels, new Rectangle(src.getWidth(), src.getHeight()));
	}

	@Test 
	public void testToString() {
		assertTrue(equalizeFilter.toString().contains(EqualizeFilter.class.getName()));
	}
}
