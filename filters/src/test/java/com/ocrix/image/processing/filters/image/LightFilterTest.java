/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.LIGHT;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.image.LightFilter.AmbientLight;
import com.ocrix.image.processing.filters.image.LightFilter.DistantLight;
import com.ocrix.image.processing.filters.image.LightFilter.Light;
import com.ocrix.image.processing.filters.image.LightFilter.Material;
import com.ocrix.image.processing.filters.image.LightFilter.PointLight;
import com.ocrix.image.processing.filters.image.LightFilter.SpotLight;
import com.ocrix.image.processing.filters.math.Function2D;
import com.ocrix.image.processing.filters.vecmath.Color4f;
import com.ocrix.image.processing.filters.vecmath.Vector3f;


/**
 *  Tests {@link LightFilter} functionality.
 */
public class LightFilterTest {
	private LightFilter lightFilter = null;
	private BufferedImage src = null;
	private Light light = null;
	private Material material;

	
	@Before
	public void setUp() throws Exception {
		lightFilter = new LightFilter();
		light = new Light();
		material = new Material();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(lightFilter);
	}
	
	
	@Test
	public void testLightFilter(){
		lightFilter.setBumpHeight(HEPTAGON);
		BufferedImage result = lightFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + LIGHT.to(), "tiff");
	}
	
	
	@Test
	public void testSetMaterial() {
		Material material = new Material();
		lightFilter.setMaterial(material);
		assertEquals(material, lightFilter.getMaterial());
	}
	
	
	@Test
	public void testSetBumpFunction() {
		Function2D bumpFunction  = new CellularFilter();
		lightFilter.setBumpFunction(bumpFunction);
		assertEquals(bumpFunction, lightFilter.getBumpFunction());
	}
	
	
	@Test
	public void testSetBumpHeight() {
		float bumpHeight = TestUtils.genFloat();
		lightFilter.setBumpHeight(bumpHeight);
		assertEquals(bumpHeight, lightFilter.getBumpHeight(), EPSILON);
	}
	
	@Test
	public void testSetBumpSoftness() {
		float bumpSoftness = TestUtils.genFloat();
		lightFilter.setBumpSoftness(bumpSoftness);
		assertEquals(bumpSoftness, lightFilter.getBumpSoftness(), EPSILON);
	}
	
	@Test
	public void testSetBumpShape(){
		int bumpShape = TestUtils.genInteger();
		lightFilter.setBumpShape(bumpShape);
		assertEquals(bumpShape, lightFilter.getBumpShape());
	}
	
	@Test
	public void testSetViewDistance() {
		float viewDistance = TestUtils.genFloat();
		lightFilter.setViewDistance(viewDistance);
		assertEquals(viewDistance, lightFilter.getViewDistance(), EPSILON);
	}
	
	@Test
	public void testSetEnvironmentMap() throws IOException {
		BufferedImage environmentMap = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		lightFilter.setEnvironmentMap(environmentMap);
		assertEquals(environmentMap, lightFilter.getEnvironmentMap());
	}
	
	@Test
	public void testSetColorSource() {
		int colorSource =  TestUtils.genInteger();
		lightFilter.setColorSource(colorSource);
		assertEquals(colorSource, lightFilter.getColorSource());
	}
	
	@Test
	public void testSetBumpSource() {
		int bumpSource = TestUtils.genInteger();
		lightFilter.setBumpSource(bumpSource);
		assertEquals(bumpSource, lightFilter.getBumpSource());
	}
	
	@Test
	public void testSetDiffuseColor() {
		int diffuseColor = TestUtils.genInteger();
		lightFilter.setDiffuseColor(diffuseColor);
		assertEquals(diffuseColor, lightFilter.getDiffuseColor());
	}
	
	@Test
	public void testAddLight() {
		Light light = new Light();
		lightFilter.addLight(light);
		int index = lightFilter.getLights().indexOf(light);
		assertEquals(light, lightFilter.getLights().get(index));
	}
	
	@Test
	public void testRemoveLight(){
		Light light = new Light();
		lightFilter.addLight(light);
		lightFilter.addLight(light);
		
		for(Light lig :lightFilter.getLights()){
			lightFilter.removeLight(lig);
		}
		
		assertEquals(0, lightFilter.getLights().size());
		
	}
	
	@Test
	public void testSetEnvironmentMapNullEnvironmentMap() {
		BufferedImage environmentMap = null; 
		lightFilter.setEnvironmentMap(environmentMap);
		assertTrue(true);
	}
	
	@Test
	public void testFilterPixels() {
		int[] pxs = null;
		int[] pxels = src.getRaster().getPixels(0,  0, src.getWidth() , src.getHeight(), pxs);
		lightFilter.setBumpShape(1);
		int[] result = lightFilter.filterPixels(src.getWidth(), src.getHeight(), pxels, new Rectangle(src.getWidth(), src.getHeight()));
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertEquals(lightFilter.toString(), LightFilter.class.getName());
	}
	
	// Testing inner classes - code coverage.
	
	@Test
	public void testAmbientLight() {
		AmbientLight ambientLight = new LightFilter(). new AmbientLight();
		assertNotNull(ambientLight);
		assertEquals(ambientLight.toString(), "AmbientLight");
	}
	
	@Test
	public void testSpotLight() {
		SpotLight spotLight = new LightFilter(). new SpotLight();
		assertNotNull(spotLight);
		assertEquals(spotLight.toString(), "SpotLight");
	}
	
	@Test 
	public void testDistantLight() {
		DistantLight distantLight = new LightFilter(). new DistantLight(); 
		assertNotNull(distantLight);
		assertEquals(distantLight.toString(), "DistantLight");
	}
	
	@Test
	public void testPointLight() {
		PointLight pointLight = new LightFilter(). new PointLight();
		assertNotNull(pointLight);
		assertEquals(pointLight.toString(), "PointLight");
	}
	
	@Test
	public void testLightToString() {
		assertEquals("Light", light.toString());
	}
	
	@Test
	public void testLightClone() {
		Light clone = (Light) light.clone();
		assertNotNull(clone);
	}
	
	@Test
	public void testLightSetCentreY() {
		float y = TestUtils.genFloat();
		light.setCentreY(y);
		assertEquals(y,  light.getCentreY(), EPSILON);
	}
	
	@Test
	public void testLightSetCentreX() {
		float x = TestUtils.genFloat();
		light.setCentreX(x);
		assertEquals(x,  light.getCentreX(), EPSILON);
	}
	
	@Test
	public void testLightSetColor() {
		int color = TestUtils.genInteger();
		light.setColor(color);
		assertEquals(color,  light.getColor());
	}
	
	@Test
	public void testLightSetFocus() {
		float focus = TestUtils.genFloat();
		light.setFocus(focus);
		assertEquals(focus,  light.getFocus(), EPSILON);
	}
	
	@Test
	public void testLightSetConeAngle() {
		float coneAngle = TestUtils.genFloat();
		light.setConeAngle(coneAngle);
		assertEquals(coneAngle,  light.getConeAngle(), EPSILON);
	}
	

	@Test
	public void testLightSetIntensity() {
		float intensity = TestUtils.genInteger();
		light.setIntensity(intensity);
		assertEquals(intensity, light.getIntensity(), EPSILON);
	}
	
	@Test
	public void testLightSetDistance() {
		float distance = TestUtils.genInteger();
		light.setDistance(distance);
		assertEquals(distance, light.getDistance(), EPSILON);
	}
	
	@Test
	public void testLightSetElevation() {
		float elevation = TestUtils.genInteger();
		light.setElevation(elevation);
		assertEquals(elevation, light.getElevation(), EPSILON);
	}
	
	@Test
	public void testLightSetAzimuth() {
		float azimuth = TestUtils.genInteger();
		light.setAzimuth(azimuth);
		assertEquals(azimuth, light.getAzimuth(), EPSILON);
	}
	
	@Test
	public void testMaterialSetDiffuseColor() {
		int diffuseColor = TestUtils.genInteger();
		material.setDiffuseColor(diffuseColor);
		assertEquals(diffuseColor, material.getDiffuseColor());
	}
	
	@Test
	public void testMaterialSetOpacity() {
		float opacity = TestUtils.genFloat();
		material.setOpacity(opacity);
		assertEquals(opacity, material.getOpacity(), EPSILON);
	}
	
	@Test
	public void testGetEnvironmentMapIsNull() {
		try {
			lightFilter.setEnvironmentMap(null);
			int[] pxs = null;
			int[] pxels = src.getRaster().getPixels(0,  0, src.getWidth() , src.getHeight(), pxs);
			
			Method method = lightFilter.getClass().getDeclaredMethod("getEnvironmentMap", Vector3f.class, int[].class, Integer.TYPE, Integer.TYPE);
			method.setAccessible(true);
			int result = (Integer) method.invoke(lightFilter, new Vector3f(), pxels, src.getWidth(), src.getHeight());
			assertEquals(0, result);
			
		} catch (Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testGetEnvironmentMapNotNull() {
		try {
			lightFilter.setEnvironmentMap(src);
			int[] pxs = null;
			int[] pxels = src.getRaster().getPixels(0,  0, src.getWidth() , src.getHeight(), pxs);
			
			Method method = lightFilter.getClass().getDeclaredMethod("getEnvironmentMap", Vector3f.class, int[].class, Integer.TYPE, Integer.TYPE);
			method.setAccessible(true);
			int result = (Integer) method.invoke(lightFilter, new Vector3f(), pxels, src.getWidth(), src.getHeight());
			assertEquals(-1256028, result);
			
		} catch (Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testPhongShade() {
		Vector3f position = new Vector3f();
		Vector3f viewpoint = new Vector3f();
		Vector3f normal = new Vector3f();
		Color4f diffuseColor = new Color4f(Color.CYAN);
		Color4f specularColor = new Color4f(Color.DARK_GRAY);
		Material material = new Material();
		

		
		Light[] lightsArray = new Light[] {
				                            new LightFilter().new DistantLight(new Vector3f(), new Vector3f()),
				                            new LightFilter().new PointLight(new Vector3f(), new Vector3f()),
				                            new LightFilter().new SpotLight(new Vector3f(), TestUtils.genFloat(), TestUtils.genFloat(), TestUtils.genFloat()),
				                            new LightFilter().new DistantLight(new Vector3f(), new Vector3f()) 
				                          };
		
		Color4f result = lightFilter.phongShade(position, viewpoint, normal, diffuseColor, specularColor, material, lightsArray);
		System.out.println(result);
	}
	
	@Test
	public void createAmbientLight() {
		AmbientLight ambientLight = new LightFilter().new AmbientLight(new Vector3f(), new Vector3f());
		assertNotNull(ambientLight);
	}
	
	@Test
	public void createSpotLight() {
		SpotLight spotLight = new LightFilter().new SpotLight(new Vector3f(), new Vector3f());
		assertNotNull(spotLight);
	}
}
