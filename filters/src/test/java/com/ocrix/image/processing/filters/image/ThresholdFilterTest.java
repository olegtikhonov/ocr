/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.THRESHOLD;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ThresholdFilter} functionality.
 */
public class ThresholdFilterTest {
	private ThresholdFilter thresholdFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		thresholdFilter = new ThresholdFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(thresholdFilter);
	}
	
	
	@Test
	public void testThresholdFilter(){
		thresholdFilter.setLowerThreshold(130);
		thresholdFilter.setUpperThreshold(170);
		BufferedImage result = thresholdFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + THRESHOLD.to(), "png");
	}

	@Test
	public void testSetLowerThreshold() {
		int lowerThreshold = TestUtils.genInteger();
		thresholdFilter.setLowerThreshold(lowerThreshold);
		assertEquals(lowerThreshold, thresholdFilter.getLowerThreshold());
	}
	
	@Test
	public void testSetUpperThreshold() {
		int upperThreshold = TestUtils.genInteger();
		thresholdFilter.setUpperThreshold(upperThreshold);
		assertEquals(upperThreshold, thresholdFilter.getUpperThreshold());
	}
	
	@Test
	public void testSetWhite() {
		int white = TestUtils.genInteger();
		thresholdFilter.setWhite(white);
		assertEquals(white, thresholdFilter.getWhite()); 
	}
	
	@Test
	public void testSetBlack() {
		int black = TestUtils.genInteger();
		thresholdFilter.setBlack(black);
		assertEquals(black, thresholdFilter.getBlack()); 
	}
	
	@Test
	public void testFilterRGB() {
		int result = thresholdFilter.filterRGB(0, 0, Color.MAGENTA.getRGB());
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(thresholdFilter.toString().contains(THRESHOLD.to()));
	}
	
	@After
	public void tearDown() throws Exception {
	}
}
