/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FADE;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link FadeFilter} functionality. 
 */
public class FadeFilterTest {

	private static FadeFilter fadeFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		fadeFilter = new FadeFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(fadeFilter);
	}
	
	@Test
	public void testFadeFilter() throws IOException {
		BufferedImage result = fadeFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FADE.to(), "gif");
	}
	
	@Test
	public void testGetAngle() {
		float angle = TestUtils.genFloat();
		fadeFilter.setAngle(angle);
		assertEquals(angle, fadeFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testGetSides() {
		int sides = TestUtils.genInteger();
		fadeFilter.setSides(sides);
		assertEquals(sides, fadeFilter.getSides());
	}
	
	@Test
	public void testGetFadeStart() {
		float fadeStart = TestUtils.genFloat();
		fadeFilter.setFadeStart(fadeStart);
		assertEquals(fadeStart, fadeFilter.getFadeStart(), EPSILON);
	}
	
	@Test
	public void testGetFadeWidth() {
		float fadeWidth = TestUtils.genFloat();
		fadeFilter.setFadeWidth(fadeWidth);
		assertEquals(fadeWidth, fadeFilter.getFadeWidth(), EPSILON);
	}
	
	@Test
	public void testSetInvert() {
		fadeFilter.setInvert(false);
		assertFalse(fadeFilter.getInvert());
		fadeFilter.setInvert(true);
		assertTrue(fadeFilter.getInvert());
	}
	
	
	@Test
	public void testSymmetry() {
		float x = TestUtils.genFloat(); 
		float b = TestUtils.genFloat();
		float result = fadeFilter.symmetry(x, b);
		result = fadeFilter.symmetry((x - 1), b);
		assertTrue(result != 0);
	}
	
	@Test
	public void testFilterRGB() {
		int x = src.getHeight() / 3; 
		int y = src.getWidth() / 3; 
		int rgb = src.getRGB(x, y);
		
	    // sides = 2
		fadeFilter.setSides(2);
		fadeFilter.filterRGB(x, y, rgb);
		assertTrue(true);
		
		// sides = 3
		fadeFilter.setSides(3);
		fadeFilter.filterRGB(x, y, rgb);
		assertTrue(true);
		
		// sides = 4
		fadeFilter.setSides(4);
		fadeFilter.filterRGB(x, y, rgb);
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(fadeFilter.toString().contains(FadeFilter.class.getName()));
	}
}
