/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TIFF;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.VolatileImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.sanselan.ImageWriteException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link CommonUtils} functionality.
 */
public class TestCommonUtils {

	/* Test's constants */
	final static int width = 150;
	final static int height = 250;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetColorModel() {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsConfiguration gc = ge.getDefaultScreenDevice()
				.getDefaultConfiguration();

		VolatileImage vImg = gc.createCompatibleVolatileImage(width, height);
		ColorModel cm = CommonUtils.getColorModel(vImg);
		assertNotNull(cm);
	}

	@Test
	public void testGeneratePixels() {
		byte[] result = CommonUtils.generatePixels(width, height,
				new Rectangle2D.Float());
		assertNotNull(result);
	}

	@Test
	public void testGeneratePixelsNull() {
		byte[] result = CommonUtils.generatePixels(width, height, null);
		assertTrue(result == null);
	}

	// @Test
	// public void testSaveBufferedImage() {
	// CommonUtils
	// .saveBufferedImage(imageToBeSaved, pathPrefix, fileExtension);
	// }

	@Test
	public void testSaveBufferedImageAsTIFF() {
		try {
			String imName = TestCommonUtils.class.getSimpleName();

			if (new File(TO_BE_SAVED + imName + "." + TIFF).exists()) {
				new File(TO_BE_SAVED + imName + "." + TIFF).delete();
			}

			CommonUtils.saveBufferedImageAsTIFF(
					ImageIO.read(new File(PREFIXPATH + IMAGENAME)), TO_BE_SAVED
							+ imName, TIFF);
			assertTrue(new File(TO_BE_SAVED + imName + "." + TIFF).exists());
		} catch (ImageWriteException e) {
		} catch (IOException e) {
		}
	}

	@Test
	public void testSaveBufferedImageDifExtension() {
		try {
			String difExt = "common_utils";
			String imName = TestCommonUtils.class.getSimpleName();

			if (new File(TO_BE_SAVED + imName + "." + difExt).exists()) {
				new File(TO_BE_SAVED + imName + "." + difExt).delete();
			}

			CommonUtils.saveBufferedImage(ImageIO.read(new File(PREFIXPATH + IMAGENAME)), TO_BE_SAVED + imName, difExt);
		} catch (IOException e) {
		}
	}

	@Test
	public void testSaveBufferedImageAsTIFFNullParam() {
		try {
			String imName = TestCommonUtils.class.getSimpleName();

			if (new File(TO_BE_SAVED + imName + "." + TIFF).exists()) {
				new File(TO_BE_SAVED + imName + "." + TIFF).delete();
			}

			CommonUtils.saveBufferedImageAsTIFF(null, TO_BE_SAVED + imName, TIFF);
			assertTrue(true);
		} catch (ImageWriteException e) {
		} catch (IOException e) {
		}
	}

	@Test
	public void testResizeImage() {
		BufferedImage bi;
		int wb, hb, wa, ha;
		try {
			bi = CommonUtils.toBufferedImage(ImageIO.read(new File(PREFIXPATH
					+ IMAGENAME)));
			wb = bi.getWidth();
			hb = bi.getHeight();

			bi = CommonUtils.resizeImage(bi, width, height, Image.SCALE_SMOOTH);

			wa = bi.getWidth();
			ha = bi.getHeight();

			assertFalse(wb == wa);
			assertFalse(hb == ha);

		} catch (IOException e) {
			assertTrue(false);
		}
	}

	@Test
	public void testToBufferedImage() {
		try {
			BufferedImage bi = CommonUtils.toBufferedImage(CommonUtils
					.toImage(ImageIO.read(new File(PREFIXPATH + IMAGENAME))));

			assertNotNull(bi);
		} catch (IOException e) {
		}
	}

	@Test
	public void testToBufferedImageNull() {
		BufferedImage bi = CommonUtils.toBufferedImage(null);
		assertTrue(bi == null);
	}

	@Test
	public void testToImage() {
		try {
			Image image = CommonUtils.toImage(ImageIO.read(new File(PREFIXPATH
					+ IMAGENAME)));
			assertNotNull(image);
		} catch (IOException e) {
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testToImageNull() {
		Image image = CommonUtils.toImage(null);
		assertTrue(image == null);
	}

	@Test
	public void testConvertToGrayScale() {
		try {
			BufferedImage bi = CommonUtils.convertToGrayScale(ImageIO
					.read(new File(PREFIXPATH + IMAGENAME)));

			assertNotNull(bi);
		} catch (IOException e) {
		}
	}

	@Test
	public void testConvertToGrayScaleImageNull() {
		BufferedImage bi = CommonUtils.convertToGrayScale(null);
		assertTrue(bi != null);
	}
}
