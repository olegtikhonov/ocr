/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.POINTILLIZE;
import static org.junit.Assert.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PointillizeFilter} functionality.
 * TODO: find why it's not working.
 */
public class PointillizeFilterTest {

	private PointillizeFilter pointillizeFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		pointillizeFilter = new PointillizeFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}

	@Test
	public void testToString() {
		assertTrue(pointillizeFilter.toString().contains(PointillizeFilter.class.getSimpleName()));
	}

	@Test
	public void testPointillizeFilter() {
		pointillizeFilter.setRandomness(1.0f);
		BufferedImage result = pointillizeFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + POINTILLIZE.to(), "png");
	}

	@Test
	public void testSetEdgeThickness() {
		float edgeThickness = TestUtils.genFloat();
		pointillizeFilter.setEdgeThickness(edgeThickness);
		assertEquals(edgeThickness, pointillizeFilter.getEdgeThickness(), EPSILON);
	}

	@Test
	public void testGetEdgeThickness() {
		testSetEdgeThickness();
	}

	@Test
	public void testSetFadeEdges() {
		// Case 1: fadeEdges is true
		boolean fadeEdges = true;
		pointillizeFilter.setFadeEdges(fadeEdges);
		assertTrue(pointillizeFilter.getFadeEdges());
		
		// Case 2: fadeEdges is false
		fadeEdges = false;
		pointillizeFilter.setFadeEdges(fadeEdges);
		assertFalse(pointillizeFilter.getFadeEdges());
	}

	@Test
	public void testGetFadeEdges() {
		testSetFadeEdges();
	}

	@Test
	public void testSetEdgeColor() {
		int edgeColor = Color.YELLOW.getRGB();
		pointillizeFilter.setEdgeColor(edgeColor);
		assertEquals(edgeColor, pointillizeFilter.getEdgeColor());
	}

	@Test
	public void testGetEdgeColor() {
		testSetEdgeColor();
	}

	@Test
	public void testSetFuzziness() {
		float fuzziness = TestUtils.genFloat();
		pointillizeFilter.setFuzziness(fuzziness);
		assertEquals(fuzziness, pointillizeFilter.getFuzziness(), EPSILON);
	}

	@Test
	public void testGetFuzziness() {
		testSetFuzziness();
	}

	@Test
	public void testGetPixel() {
		int[] arr = null;
		// fadeEdges is false
		pointillizeFilter.setFadeEdges(false);
		int result = pointillizeFilter.getPixel(0, 0, src.getRaster().getPixel(0, 0, arr), src.getWidth(), src.getHeight());
		assertEquals(-16777216, result);
		
		// fadeEdges is true
		pointillizeFilter.setFadeEdges(true);
		result = pointillizeFilter.getPixel(0, 0, src.getRaster().getPixel(0, 0, arr), src.getWidth(), src.getHeight());
		assertEquals(65, result);
		
	}
}
