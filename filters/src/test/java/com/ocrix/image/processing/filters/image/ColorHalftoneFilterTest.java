/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.COLOR_HALFTONE;
import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ColorHalftoneFilter} functionality.
 */
public class ColorHalftoneFilterTest {
	private static ColorHalftoneFilter colorHalftoneFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		colorHalftoneFilter = new ColorHalftoneFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(colorHalftoneFilter);
	}

	@Test
	public void testFilter() {
		BufferedImage result = colorHalftoneFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + COLOR_HALFTONE.to(), "tiff");
	}
	
	@Test
	public void testSetdotRadius() {
		float dotRadius = TestUtils.genFloat(ANGEL_NUMBER);
		colorHalftoneFilter.setdotRadius(dotRadius);
		assertEquals(dotRadius, colorHalftoneFilter.getdotRadius(), EPSILON);
	}
	
	@Test
	public void testGetdotRadius() {
		testSetdotRadius();
	}
	
	@Test
	public void testSetCyanScreenAngle() {
		float cyanScreenAngle = TestUtils.genFloat(ANGEL_NUMBER);
		colorHalftoneFilter.setCyanScreenAngle(cyanScreenAngle);
		assertEquals(cyanScreenAngle, colorHalftoneFilter.getCyanScreenAngle(), EPSILON);
	}
	
	@Test
	public void testGetCyanScreenAngle() {
		testSetCyanScreenAngle();
	}
	
	@Test
	public void testSetMagentaScreenAngle() {
		float magentaScreenAngle = TestUtils.genFloat(ANGEL_NUMBER);
		colorHalftoneFilter.setMagentaScreenAngle(magentaScreenAngle);
		assertEquals(magentaScreenAngle, colorHalftoneFilter.getMagentaScreenAngle(), EPSILON);
	}
	
	@Test
	public void testSetYellowScreenAngle() {
		float yellowScreenAngle = TestUtils.genFloat(ANGEL_NUMBER);
		colorHalftoneFilter.setYellowScreenAngle(yellowScreenAngle);
		assertEquals(yellowScreenAngle, colorHalftoneFilter.getYellowScreenAngle(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(colorHalftoneFilter.toString().contains(ColorHalftoneFilter.class.getName()));
	}
}
