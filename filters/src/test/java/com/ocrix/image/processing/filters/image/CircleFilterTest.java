/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CIRCLE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.HRADIUS;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.VRADIUS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link CircleFilter} functionality.
 */
public class CircleFilterTest {

	private static CircleFilter circleFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		circleFilter = new CircleFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(circleFilter);
	}

	@Test
	public void testFilter() {
		circleFilter.setAngle(VRADIUS);
		assertTrue(VRADIUS == circleFilter.getAngle());
		circleFilter.setEdgeAction(TransformFilter.WRAP);
		assertTrue(TransformFilter.WRAP == circleFilter.getEdgeAction());
		circleFilter.setHeight(HRADIUS);
		assertTrue(HRADIUS == circleFilter.getHeight());
		circleFilter.setRadius(HRADIUS);
		assertTrue(HRADIUS == circleFilter.getRadius());
		BufferedImage result = circleFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CIRCLE.to(), "jpg");
	}

	@Test
	public void testSetSpreadAngle() {
		float spreadAngle = TestUtils.genInteger();
		circleFilter.setSpreadAngle(spreadAngle);
		assertEquals(spreadAngle, circleFilter.getSpreadAngle(), EPSILON);
	}
	
	@Test
	public void testGetSpreadAngle() {
		testSetSpreadAngle();
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genInteger();
		circleFilter.setRadius(radius);
		assertEquals(radius, circleFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testGetRadius() {
		testSetRadius();
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genInteger();
		circleFilter.setCentreX(centreX);
		assertEquals(centreX, circleFilter.getCentreX(), EPSILON);
	}
	
	@Test
	public void testGetCentreX() {
		testSetCentreX();
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genInteger();
		circleFilter.setCentreY(centreY);
		assertEquals(centreY, circleFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testGetCentreY() {
		testSetCentreY();
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float(TestUtils.genInteger(), TestUtils.genInteger()); 
		circleFilter.setCentre(centre);
		assertEquals(centre, circleFilter.getCentre());
	}
	
	@Test
	public void testGetCentre() {
		testSetCentre();
	}
	
	@Test
	public void testFilterSrcDstNotNull() throws IOException {
		BufferedImage dst = ImageIO.read(new File(PREFIXPATH + TUX)); 
		BufferedImage result = circleFilter.filter(dst, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CIRCLE.to(), "tiff");
	}
	
	@Test
	public void testToString(){
		assertTrue(circleFilter.toString().contains(CircleFilter.class.getName()));
	}
}
