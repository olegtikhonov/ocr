/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SpectrumTest {

	private Spectrum spectrum;
	
	@Before
	public void setUp() throws Exception {
		spectrum = new Spectrum();
	}

	@Test
	public void testWavelengthToRGB() {
		// Case 1: w < 380
		float wavelength = 379; 
		int result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-16777216, result);
		
		// Case 2: w < 440
		wavelength = 439;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-16121601, result);
		
		// Case 3: w < 490
		wavelength = 489;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-16712705, result);
		
		// Case 4: w < 510
		wavelength = 509;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-16711913, result);
		
		// Case 5: w < 580
		wavelength = 579;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-196864, result);
		
		// Case 6: w < 645
		wavelength = 644;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-63232, result);
		
		// Case 7: w <= 780
		wavelength = 780;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-10420224, result);
		
		// Case 8: w > 780
		wavelength = 781;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-16777216, result);
		
		// Case 9: 380 <= w && w <= 419
		wavelength = 401;
		result = Spectrum.wavelengthToRGB(wavelength);
		assertEquals(-8191815, result);
	}
	
	@Test
	public void testToString() {
		assertTrue(spectrum.toString().equals(Spectrum.class.getSimpleName()));
	}
}
