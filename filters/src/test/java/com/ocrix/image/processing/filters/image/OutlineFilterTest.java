/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.OUTLINE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link OutlineFilter} functionality.
 */
public class OutlineFilterTest {
	private OutlineFilter outlineFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		outlineFilter = new OutlineFilter();
		src = TestUtils.convert4(ImageIO.read(new File(PREFIXPATH + "E.jpg")));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(outlineFilter);
	}
	
	@Test
	public void testOutlineFilter(){
		BufferedImage result = outlineFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + OUTLINE.to(), "gif");
	}
	
	@Test
	public void testFilterPixels() throws Exception {
		int width = src.getWidth();
		int height = src.getHeight();
		int[] inPixels = src.getRGB(0, 0, width, height, null, 0, width);
		
		Rectangle transformedSpace = new Rectangle(0, 0, width, height);
		int[] filteredPizels = outlineFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertNotNull(filteredPizels);
	}
	
	@Test
	public void testToString() {
		assertTrue(outlineFilter.toString().contains(OutlineFilter.class.getSimpleName()));
	}
}
