/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.SKELETON;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link SkeletonFilter} functionality.
 */
public class SkeletonFilterTest {
	private SkeletonFilter skeletonFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		skeletonFilter = new SkeletonFilter();
		src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH
				+ OLD_TEXT)));
	}

	@Test
	public void testCitor() {
		assertNotNull(skeletonFilter);
	}

	@Test
	public void testSkeletonFilter() {
		BufferedImage result = skeletonFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SKELETON.to(),
				"png");
	}
	
	@Test
	public void testToString() {
		assertTrue(skeletonFilter.toString().contains(SkeletonFilter.class.getSimpleName()));
	}

	@Test
	public void testSkeletonFilter2() throws IOException {
		ZhangSuenThinningFilter sf = new ZhangSuenThinningFilter();
		BufferedImage result = sf.filter(ImageIO.read(new File(PREFIXPATH + "E.jpg")));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SKELETON.to() + "_2", "png");
	}

	@Test
	public void testFilterPixels() throws IOException {
		skeletonFilter.setColormap(new ArrayColormap());
		
		
		BufferedImage src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH + "E.jpg")));
		int width = src.getWidth();
		int height = src.getHeight();
		int[] inPixels = src.getRGB(0, 0, width, height, null, 0, width);
		Rectangle transformedSpace = new Rectangle();
		int[] result = skeletonFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertTrue(compareTwoIntArrays(inPixels, result));
		CommonUtils.saveBufferedImage(convert1DIntArrayToBufferedImage(src, result), TO_BE_SAVED
						+ SKELETON.to() + "_3", "png");
	}

	private BufferedImage convert1DIntArrayToBufferedImage(BufferedImage src, int[] imageData) {
		MemoryImageSource ims = new MemoryImageSource(src.getWidth(), src.getHeight(), imageData, 0, src.getWidth());
		return toBufferedImage(Toolkit.getDefaultToolkit().createImage(ims));
	}

	/**
	 * Converts a given Image into a BufferedImage
	 * 
	 * @param img
	 *            The Image to be converted
	 * @return The converted BufferedImage
	 */
	private BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	private boolean compareTwoIntArrays(int[] src, int[] dst) {
		if (src != null && dst != null) {

			if (src.length == dst.length) {
				for (int i = 0; i < src.length; i++) {
					if (src[i] != dst[i]) {
						return true;
					}
				}
			} else {
				return false;
			}

		} else {
			return false;
		}

		return false;
	}

}
