/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link LinearColormap} functionality.
 */
public class LinearColormapTest {

	private LinearColormap colormap;
	
	@Before
	public void setUp() throws Exception {
		colormap = new LinearColormap();
	}

	@Test
	public void testLinearColormap() {
		assertNotNull(colormap);
	}

	@Test
	public void testLinearColormapIntInt() {
		colormap = new LinearColormap(Color.RED.getRGB(), Color.GREEN.getRGB());
		assertEquals(Color.RED.getRGB(), colormap.getColor1());
		assertEquals(Color.GREEN.getRGB(), colormap.getColor2());
	}

	@Test
	public void testSetColor1() {
		int color = TestUtils.genInteger();
		colormap.setColor1(color);
		assertEquals(color, colormap.getColor1());
	}

	@Test
	public void testGetColor1() {
		testSetColor1();
	}

	@Test
	public void testSetColor2() {
		int color = TestUtils.genInteger();
		colormap.setColor2(color);
		assertEquals(color, colormap.getColor2());
	}

	@Test
	public void testGetColor2() {
		testSetColor2();
	}

	@Test
	public void testGetColor() {
		int red = Color.RED.getRGB();
		int green = Color.GREEN.getRGB();
		colormap.setColor1(red);
		colormap.setColor2(green);
		int color = TestUtils.genInteger();
		int result = colormap.getColor(color);
		assertEquals(255, new Color(result).getGreen());
	}

}
