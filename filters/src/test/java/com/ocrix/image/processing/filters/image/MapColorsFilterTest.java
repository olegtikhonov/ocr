package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static com.ocrix.image.processing.filters.Filters.MAP_COLORS;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link MapColorsFilter} functionality.
 */
public class MapColorsFilterTest {
	private MapColorsFilter mapColorsFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		mapColorsFilter = new MapColorsFilter();
		src = ImageIO.read(new File(PREFIXPATH + COW));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(mapColorsFilter);
	}

	
	@Test
	public void testMapColorFilter(){
		mapColorsFilter = new MapColorsFilter(Color.BLACK.getRGB(), Color.YELLOW.getRGB());
		BufferedImage result = mapColorsFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MAP_COLORS.to(), "gif");
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(256);
		int y = TestUtils.genInteger(256);
		int rgb = Color.PINK.getRGB();
		
		mapColorsFilter = new MapColorsFilter(x, y);
		int result = mapColorsFilter.filterRGB(x, y, rgb);
		assertEquals(rgb, result);
		
		// Case rgb == oldcolor
		result = mapColorsFilter.filterRGB(x, y, x);
		assertEquals(y, result);
	}
	
}
