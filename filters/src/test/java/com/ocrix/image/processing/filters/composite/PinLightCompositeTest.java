/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OCTAHEDRON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static java.awt.RenderingHints.KEY_ALPHA_INTERPOLATION;
import static java.awt.RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.PinLightComposite;
import com.ocrix.image.processing.filters.composite.PinLightComposite.Context;

/**
 * Tests {@link PinLightComposite} functionality.
 */
public class PinLightCompositeTest {

	private static PinLightComposite pinLightComposite = null;

	@BeforeClass
	public static void setUp() throws Exception {
		pinLightComposite = new PinLightComposite(ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(pinLightComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		CompositeContext comp = pinLightComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(KEY_ALPHA_INTERPOLATION,
						VALUE_ALPHA_INTERPOLATION_QUALITY));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(OCTAHEDRON);
		int[] dst = TestUtils.fillArray(OCTAHEDRON);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
	}

	@Test
	public void testComposeRGBElse() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(2);
		int[] dst = TestUtils.fillArray(1);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
	}
}
