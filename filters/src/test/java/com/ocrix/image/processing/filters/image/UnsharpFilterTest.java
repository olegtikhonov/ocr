package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;



public class UnsharpFilterTest {

	private static UnsharpFilter unsharpFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		unsharpFilter = new UnsharpFilter();
		src =ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(unsharpFilter);
	}
	
	@Test
	public void testUnsharpFiler(){
		unsharpFilter.setRadius(90);
		BufferedImage result = unsharpFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "UnsharpFilter", "gif");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
