/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.DIFFUSION;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link DiffusionFilter} functionality.
 */
public class DiffusionFilterTest {

	private static DiffusionFilter diffusionFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		diffusionFilter = new DiffusionFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(diffusionFilter);
	}
	
	@Test
	public void testDiffusionFilter()  throws Exception {
		BufferedImage result = diffusionFilter.filter(src, ImageIO.read(new File(PREFIXPATH + COW)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DIFFUSION.to(), "jpg");
	}

    @Test
    public void testSetSerpentine() {
    	diffusionFilter.setSerpentine(true);
    	assertEquals(true, diffusionFilter.getSerpentine());
    	
    	diffusionFilter.setSerpentine(false);
    	assertEquals(false, diffusionFilter.getSerpentine());
    }
    
    @Test
    public void testSetColorDither() {
    	diffusionFilter.setColorDither(true);
    	assertEquals(true, diffusionFilter.getColorDither());
    	
    	diffusionFilter.setColorDither(false);
    	assertEquals(false, diffusionFilter.getColorDither());
    }
    
    @Test
    public void testGetMatrix() {
    	int[] matrix = TestUtils.fillArray(HEPTAGON);
    	diffusionFilter.setMatrix(matrix);
    	assertEquals(matrix, diffusionFilter.getMatrix());
    }
    
    @Test
    public void testGetLevels(){
    	diffusionFilter.setLevels(HEPTAGON);
    	assertEquals(HEPTAGON, diffusionFilter.getLevels());
    }
    
    @Test
    public void testToString() {
    	assertTrue(diffusionFilter.toString().contains(DiffusionFilter.class.getName()));
    }
}
