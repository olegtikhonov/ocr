/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.BRUSHED_METAL;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.HRADIUS;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.VRADIUS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests a {@link BrushedMetalFilter} functionality.
 */
public class BrushedMetalFilterTest {

	private static BrushedMetalFilter brushedMetalFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		brushedMetalFilter = new BrushedMetalFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitorNoParams() {
		assertNotNull(brushedMetalFilter);
	}

	@Test
	public void testCitorParams() {
		int color = TestUtils.genInteger(RONNA);
		brushedMetalFilter = new BrushedMetalFilter(color, (int) VRADIUS, ALPHA, true, ALPHA);
		assertNotNull(brushedMetalFilter);
		assertEquals(brushedMetalFilter.getColor(), color);
		assertEquals(brushedMetalFilter.getRadius(), (int) VRADIUS);
		assertTrue(brushedMetalFilter.getAmount() == ALPHA);
		assertTrue(brushedMetalFilter.getMonochrome());
		assertTrue(brushedMetalFilter.getShine() == ALPHA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorNegParams() {
		int color = TestUtils.genInteger(RONNA);
		brushedMetalFilter = new BrushedMetalFilter(~color, ~(int) VRADIUS,
				ALPHA, true, HRADIUS);
		assertTrue(false);
	}

	@Test
	public void testSetRadius() {
		int radius = TestUtils.genInteger(RONNA);
		brushedMetalFilter.setRadius(radius);
		assertEquals(brushedMetalFilter.getRadius(), radius);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegRadius() {
		int radius = TestUtils.genInteger(RONNA);
		brushedMetalFilter.setRadius(~radius);
		assertTrue(false);
	}

	@Test
	public void testSetAmount() {
		brushedMetalFilter.setAmount(ALPHA);
		assertTrue(brushedMetalFilter.getAmount() == ALPHA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegAmount() {
		brushedMetalFilter.setAmount(-ALPHA);
		assertTrue(brushedMetalFilter.getAmount() != ALPHA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetOutOfRangeShine() {
		brushedMetalFilter.setShine(-HRADIUS);
		assertTrue(brushedMetalFilter.getShine() != -HRADIUS);
	}

	@Test
	public void testSetShine() {
		brushedMetalFilter.setShine(ALPHA);
		assertTrue(brushedMetalFilter.getShine() == ALPHA);
	}

	@Test
	public void testSetColor() {
		int color = TestUtils.genInteger(RONNA);
		brushedMetalFilter.setColor(color);
		assertEquals(color, brushedMetalFilter.getColor());
	}

	@Test
	public void testSetMonochrome() {
		brushedMetalFilter.setMonochrome(false);
		assertFalse(brushedMetalFilter.getMonochrome());
	}

	@Test
	public void testFilter() {
		brushedMetalFilter.setMonochrome(true);
		BufferedImage result = brushedMetalFilter.filter(src, null);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + BRUSHED_METAL.to(), "jpg");
	}
	
	@Test
	public void testFilterDstNotNull() throws IOException {
		brushedMetalFilter.setMonochrome(true);
		BufferedImage dst = ImageIO.read(new File(PREFIXPATH + TUX));
		BufferedImage result = brushedMetalFilter.filter(src, dst);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + BRUSHED_METAL.to(), "png");
	}

	@Test
	public void testCreateCompatibleDestImage() throws IOException {
		ColorModel colorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		BufferedImage compDestImage = brushedMetalFilter
				.createCompatibleDestImage(src, colorModel);
		assertNotNull(compDestImage);
	}

	@Test
	public void testGetBounds2D() {
		Rectangle2D bounds = brushedMetalFilter.getBounds2D(src);
		assertEquals(src.getHeight(), (int) bounds.getHeight());
		assertEquals(src.getWidth(), (int) bounds.getWidth());
	}

	@Test
	public void testGetPoint2D() {
		Point2D pointA = new Point2D.Float();
		Point2D pointB = new Point2D.Float();
		Point2D point = brushedMetalFilter.getPoint2D(pointA, pointB);
		assertNotNull(point);
	}
	
	@Test
	public void testGetRenderingHints() {
		assertNotNull(brushedMetalFilter.getRenderingHints());
	}
	
	@Test
	public void testGetPoint2DDstPntIsNull(){
		Point2D pointA = new Point2D.Float();
		Point2D pointB = null;
		Point2D point = brushedMetalFilter.getPoint2D(pointA, pointB);
		assertNotNull(point);
	}
	
	@Test
	public void testToString() {
		assertTrue(brushedMetalFilter.toString().contains(BrushedMetalFilter.class.getName()));
	}
}
