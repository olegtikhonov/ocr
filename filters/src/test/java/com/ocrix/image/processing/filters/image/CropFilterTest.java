/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.CROP;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link CropFilter} functionality.
 */
public class CropFilterTest {

	private static CropFilter cropFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		cropFilter = new CropFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}

	@Test
	public void testCitor(){
		assertNotNull(cropFilter);
	}
	
	@Test
	public void testFilter(){
		cropFilter.setHeight(src.getHeight() / 2);
		cropFilter.setWidth(src.getWidth() / 2);
		
		BufferedImage result = cropFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CROP.to(), "jpg");
	}
	
	@Test
	public void testFilterDstNotNull() throws IOException{
		BufferedImage result = cropFilter.filter(ImageIO.read(new File(PREFIXPATH + TUX)), src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CROP.to(), "jpg");
	}
	
	@Test
	public void testSetX() {
		int x = TestUtils.genInteger(QUAD);
		cropFilter.setX(x);
		assertEquals(x, cropFilter.getX());
	}
	
	@Test
	public void testGetX() {
		testSetX();
	}
	
	@Test
	public void testSetY() {
		int y = TestUtils.genInteger(QUAD);
		cropFilter.setY(y);
		assertEquals(y, cropFilter.getY()); 
	}
	
	@Test
	public void testGetY() {
		testSetY();
	}
	
	@Test
	public void testToString() {
		assertTrue(cropFilter.toString().contains(CropFilter.class.getName()));
	}
}
