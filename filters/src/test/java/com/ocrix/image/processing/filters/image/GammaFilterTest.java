/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GAMMA;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GammaFilter} functionality.
 */
public class GammaFilterTest {

	private static GammaFilter gammaFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		gammaFilter = new GammaFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(gammaFilter);
	}
	
	
	@Test
	public void testGammaFilter() throws IOException{
		gammaFilter.setGamma(ALPHA * ALPHA);
		BufferedImage result = gammaFilter.filter(src, ImageIO.read(new File(PREFIXPATH + COW)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GAMMA.to(), "gif");
	}
	
	@Test
	public void testGetGamma() {
		float gamma = TestUtils.genFloat();
		gammaFilter.setGamma(gamma);
		assertEquals(gamma, gammaFilter.getGamma(), EPSILON);
	}
	
	@Test
	public void testInitializeGGammaEqRGamma() {
		float rGamma = TestUtils.genFloat();
		gammaFilter.setGamma(rGamma, rGamma, TestUtils.genFloat());
		gammaFilter.initialize();
		assertTrue(true);
	}
	
	@Test
	public void testInitializeGGammaNotEqRGamma() {
		float rGamma = TestUtils.genFloat();
		float gGamma = TestUtils.genFloat();
		gammaFilter.setGamma(rGamma, gGamma, TestUtils.genFloat());
		gammaFilter.initialize();
		assertTrue(true);
	}
	
	@Test
	public void testInitializeBGammaEqRGamma() {
		float bGamma = TestUtils.genFloat();
		gammaFilter.setGamma(bGamma, TestUtils.genFloat(), bGamma);
		gammaFilter.initialize();
		assertTrue(true);
	}
	
	@Test
	public void testInitializeBGammaEqGGamma() {
		float bGamma = TestUtils.genFloat();
		gammaFilter.setGamma(TestUtils.genFloat(), bGamma, bGamma);
		gammaFilter.initialize();
		assertTrue(true);
	}
	
	@Test
	public void testInitialize() {
		gammaFilter.setGamma(TestUtils.genFloat(), TestUtils.genFloat(), TestUtils.genFloat());
		gammaFilter.initialize();
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(gammaFilter.toString().contains(GammaFilter.class.getName()));
	}
}
