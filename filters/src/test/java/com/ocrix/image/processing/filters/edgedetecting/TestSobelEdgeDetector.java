/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.SOBEL;
import static org.junit.Assert.assertNotNull;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.edgedetecting.Sobel;

/**
 * Tests a {@link Sobel} functionality.
 */
public class TestSobelEdgeDetector {

	private static Sobel edgeDetector = null;
	private static final Logger LOG = Logger.getLogger(TestSobelEdgeDetector.class);

	@BeforeClass
	public static void setUp() throws Exception {
		edgeDetector = new Sobel();
	}

	@Test
	public void testSobelEdgeDetector() throws IOException {
		BufferedImage src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		assertNotNull(src);

		int width = src.getWidth();// current
		int height = src.getHeight();// current

		int[] orig = new int[width * height];// creates orig int array
		/* Creates a pixel grabber */
		PixelGrabber grabber = new PixelGrabber(src, 0, 0, width, height, orig, 0, width);
		
		try {
			grabber.grabPixels();
		} catch (InterruptedException e2) {
			LOG.error(e2);
		}
		/* Inits an edgeDetector */
		edgeDetector.init(orig, width, height);
		/* Does a Sobel edge detection */
		int[] res = edgeDetector.process();

		/* Save a new image */
		Image mis = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(width, height, res, 0, width));
		BufferedImage output = CommonUtils.toBufferedImage(mis);
		assertNotNull(output);
		CommonUtils.saveBufferedImage(output, TO_BE_SAVED + SOBEL.to(), "png");
	}
}
