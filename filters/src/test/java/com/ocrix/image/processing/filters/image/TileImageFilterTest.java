package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;



public class TileImageFilterTest {

	private static TileImageFilter tileImageFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		tileImageFilter = new TileImageFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(tileImageFilter);
	}
	
	@Test
	public void testTileImageFilter(){
		tileImageFilter.setWidth(ANGEL_NUMBER);
		tileImageFilter.setHeight(ANGEL_NUMBER);
		BufferedImage result = tileImageFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "TileImageFilter", "png");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
