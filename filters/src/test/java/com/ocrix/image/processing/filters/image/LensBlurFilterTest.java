/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.LENS_BLUR;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/*
 * Tests {@link LensBlurFilter} functionality. 
 */
public class LensBlurFilterTest {

	private static LensBlurFilter lensBlurFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		lensBlurFilter = new LensBlurFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	
	@Test
	public void testCitor(){
		assertNotNull(lensBlurFilter);
	}
	
	
	@Test
	public void testLensBlurFilter(){
		lensBlurFilter.setBloom(1.1984f);
		lensBlurFilter.setBloomThreshold(170.0f);
		lensBlurFilter.setRadius(2.45678f);
		BufferedImage result = lensBlurFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + LENS_BLUR.to(), "png");
	}
	
	@Test
	public void testGetRadius(){
		float orig = TestUtils.genFloat();
		lensBlurFilter.setRadius(orig);
		float result = lensBlurFilter.getRadius();
		assertEquals(orig, result, EPSILON);
		
	}
	
	@Test
	public void testSetSides(){
		int sidesLen = TestUtils.genInteger();
		lensBlurFilter.setSides(sidesLen);
		int result = lensBlurFilter.getSides();
		assertEquals(sidesLen, result);
	}
	
	@Test
	public void testGetBloom() {
		float bloom = TestUtils.genFloat();
		lensBlurFilter.setBloom(bloom);
		float result = lensBlurFilter.getBloom();
		assertEquals(bloom, result, EPSILON);
	}
	
	@Test
	public void testGetBloomThreshold() {
		float bloomThresh = TestUtils.genFloat();
		lensBlurFilter.setBloomThreshold(bloomThresh);
		float result = lensBlurFilter.getBloomThreshold();
		assertEquals(bloomThresh, result, EPSILON);
	}
	
	@Test
	public void testToString() {
		String result = lensBlurFilter.toString();
		assertEquals(result, LensBlurFilter.class.getName());
	}
}
