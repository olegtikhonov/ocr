/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GLINT;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GlintFilter} functionality.
 */
public class GlintFilterTest {

	private static GlintFilter glintFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		glintFilter = new GlintFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor(){
		assertNotNull(glintFilter);
	}
	
	
	@Test
	public void testGlintFilter(){
		glintFilter.setBlur(ALPHA);
		assertEquals(Float.compare(ALPHA, glintFilter.getBlur()), 0);
		glintFilter.setThreshold(0.85f);
		assertEquals(Float.compare(0.85f, glintFilter.getThreshold()), 0);
		BufferedImage result = glintFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GLINT.to(), "jpeg");
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		glintFilter.setAmount(amount);
		assertEquals(amount, glintFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testSetLength() {
		int length = TestUtils.genInteger();
		glintFilter.setLength(length);
		assertEquals(length, glintFilter.getLength());
	}
	
	@Test
	public void testSetGlintOnly() {
		boolean glintOnly = true;
		glintFilter.setGlintOnly(glintOnly);
		assertTrue(glintFilter.getGlintOnly());
		
		glintOnly = false;
		glintFilter.setGlintOnly(glintOnly);
		assertFalse(glintFilter.getGlintOnly());
	}
	
	@Test
	public void testSetColormap() {
		Colormap colormap = new LinearColormap(Color.GREEN.getRGB(), Color.PINK.getRGB());
		glintFilter.setColormap(colormap);
		assertEquals(colormap, glintFilter.getColormap());
	}
	
	@Test
	public void testToString() {
		assertTrue(glintFilter.toString().contains(GlintFilter.class.getName()));
	}
}
