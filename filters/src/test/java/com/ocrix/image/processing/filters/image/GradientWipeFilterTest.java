/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GRADIENT_WIPE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.WIPE_FILTER_MASK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GradientWipeFilter} functionality.
 */
public class GradientWipeFilterTest {
	/* Test's members */
	private static GradientWipeFilter gradientWipeFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		gradientWipeFilter = new GradientWipeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		gradientWipeFilter.setMask(ImageIO.read(new File(PREFIXPATH + WIPE_FILTER_MASK)));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(gradientWipeFilter);
	}
	
	
	@Test
	public void testGradientWipeFilter() throws IOException{
		BufferedImage result = gradientWipeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GRADIENT_WIPE.to(), "png");
	}
	
	@Test
	public void testGetDensity() {
		float density = TestUtils.genFloat();
		gradientWipeFilter.setDensity(density);
		assertEquals(density, gradientWipeFilter.getDensity(), EPSILON);
	}
	
	@Test
	public void testGetSoftness() {
		float softness = TestUtils.genFloat();
		gradientWipeFilter.setSoftness(softness);
		assertEquals(softness, gradientWipeFilter.getSoftness(), EPSILON);
	}
	
	@Test
	public void testSetMask() throws IOException{
		BufferedImage mask = ImageIO.read(new File(PREFIXPATH + WIPE_FILTER_MASK));
		gradientWipeFilter.setMask(mask);
		assertEquals(mask, gradientWipeFilter.getMask());
	}
	
	@Test
	public void testSetInvert(){
		boolean invert = true;
		gradientWipeFilter.setInvert(invert);
		assertTrue(gradientWipeFilter.getInvert());
		
		//Case2: false
		invert = false;
		gradientWipeFilter.setInvert(invert);
		assertFalse(gradientWipeFilter.getInvert());
	}
	
	@Test
	public void testToString() {
		assertTrue(gradientWipeFilter.toString().contains(GradientWipeFilter.class.getName()));
	}
	
	@Test
	public void testGradientWipeFilterDstNotNullMaskIsNull() throws IOException{
		gradientWipeFilter.setMask(null);
		BufferedImage result = gradientWipeFilter.filter(src, src);
		assertNotNull(result);
	}
}
