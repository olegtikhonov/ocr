/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.OIL;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link OilFilter} functionality. 
 */
public class OilFilterTest {
	private static OilFilter oilFilter = null;
	private static BufferedImage src = null;

	
	@Before
	public void setUp() throws Exception {
		oilFilter = new OilFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
	}

	@Test
	public void testCitor(){
		assertNotNull(oilFilter);
	}
	
	@Test
	public void testOilFilter(){
		BufferedImage result = oilFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + OIL.to(), "jpg");
	}
	
	@Test
	public void testSetRange() {
		int range = TestUtils.genInteger();
		oilFilter.setRange(range);
		assertEquals(range, oilFilter.getRange());
	}
	
	@Test
	public void testSetLevels() {
		int levels = TestUtils.genInteger();
		oilFilter.setLevels(levels);
		assertEquals(levels, oilFilter.getLevels());
	}

	@Test
	public void testToString() {
		assertTrue(oilFilter.toString().contains(OilFilter.class.getSimpleName()));
	}
}
