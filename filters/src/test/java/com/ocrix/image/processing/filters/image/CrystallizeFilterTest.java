/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CRYSTALLIZE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.NONAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link CrystallizeFilter} functionality.
 */
public class CrystallizeFilterTest {

	private CrystallizeFilter crystallizeFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		crystallizeFilter = new CrystallizeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(crystallizeFilter);
	}

	@Test
	public void testSetEdgeThickness() {
		float edgeThickness = TestUtils.genFloat(QUAD);
		crystallizeFilter.setEdgeThickness(edgeThickness);
		assertEquals(edgeThickness, crystallizeFilter.getEdgeThickness(), EPSILON);
	}

	@Test
	public void testGetEdgeThickness() {
		testSetEdgeThickness();
	}

	@Test
	public void testSetFadeEdges() {
		crystallizeFilter.setFadeEdges(true);
		assertEquals(true, crystallizeFilter.getFadeEdges());

		crystallizeFilter.setFadeEdges(false);
		assertEquals(false, crystallizeFilter.getFadeEdges());
	}

	@Test
	public void testGetFadeEdges() {
		testSetFadeEdges();
	}

	@Test
	public void testSetEdgeColor() {
		int edgeColor = TestUtils.genInteger(QUAD);
		crystallizeFilter.setEdgeColor(edgeColor);
		assertEquals(edgeColor, crystallizeFilter.getEdgeColor());
	}

	@Test
	public void testGetEdgeColor() {
		testSetEdgeColor();
	}

	@Test
	public void testGetPixel() {
		int x = TestUtils.genIntegerGreaterThanZero(QUAD);
		int y = TestUtils.genIntegerGreaterThanZero(QUAD);
		int[] inPixels = TestUtils.fillArray(NONAGON);
		int result = crystallizeFilter.getPixel(x, y, inPixels, src.getWidth(), src.getHeight());
		assertTrue(true);
	}

	@Test
	public void testFilter() {
		crystallizeFilter.setAmount(0.5f);
		crystallizeFilter.setAngleCoefficient(0.5f);
		crystallizeFilter.setDistancePower(1);
		BufferedImage result = crystallizeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CRYSTALLIZE.to(), "png");
	}

	@Test
	public void testToString() {
		assertTrue(crystallizeFilter.toString().contains(
				CrystallizeFilter.class.getName()));
	}

	// fadeEdges
	@Test
	public void test() {
		int x = TestUtils.genInteger(QUAD);
		int y = TestUtils.genInteger(QUAD);
		int[] inPixels = TestUtils.fillArray(NONAGON);
		crystallizeFilter.setFadeEdges(true);
		int result = crystallizeFilter.getPixel(x, y, inPixels, src.getWidth(), src.getHeight());
		assertTrue(result >= 0);
	}
}
