/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.RENDER_TEXT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link RenderTextFilter}.
 */
public class RenderTextFilterTest {
	private RenderTextFilter renderTextFilter = null;
	private BufferedImage src = null;
	private BufferedImage dst = null;

	@Before
	public void setUp() throws Exception {
		renderTextFilter = new RenderTextFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
		dst = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(renderTextFilter);
	}

	@Test
	public void testRenderTextFilter() {
		String txt = "٩๏̯͡๏۶(̅_̅_̅_̅(̅_̅_̅_̅_̅_̅_̅_̅_̅̅_̅()ڪے~ ~break";
		Font font = new Font("Verdana", Font.BOLD, 28);
		renderTextFilter = new RenderTextFilter(txt, font, null, null, null);
		BufferedImage result = renderTextFilter.filter(dst, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + RENDER_TEXT.to(),
				"png");
	}

	@Test
	public void testSetText() {
		String text = "Describe what Marsellus Wallace looks like!";
		renderTextFilter.setText(text);
		assertEquals(text, renderTextFilter.getText());
	}

	@Test
	public void testSetComposite() {
		Composite composite = AlphaComposite.getInstance(AlphaComposite.SRC);
		renderTextFilter.setComposite(composite);
		assertEquals(composite, renderTextFilter.getComposite());
	}

	@Test
	public void testSetPaint() {
		Paint paint = ((Graphics2D) src.getGraphics()).getPaint();
		renderTextFilter.setPaint(paint);
		assertEquals(paint, renderTextFilter.getPaint());
	}

	@Test
	public void testSetFont() {
		Font font = new Font("Serif", Font.BOLD, 12);
		renderTextFilter.setFont(font);
		assertEquals(font, renderTextFilter.getFont());
	}

	@Test
	public void testSetTransform() {
		AffineTransform transform = AffineTransform.getRotateInstance(2.4f);
		renderTextFilter.setTransform(transform);
		assertEquals(transform, renderTextFilter.getTransform());
	}

	@Test
	public void testFilterDstNull() {
		renderTextFilter.setTransform(AffineTransform.getRotateInstance(2.4f));
		renderTextFilter.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC));
		renderTextFilter.setPaint(((Graphics2D) src.getGraphics()).getPaint());
		renderTextFilter.setText(null);
		renderTextFilter.setFont(null);

		renderTextFilter.filter(src, null);
	}
}
