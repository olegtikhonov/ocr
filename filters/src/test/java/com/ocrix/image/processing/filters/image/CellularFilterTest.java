/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.CELLULAR;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.Shape;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests a {@link CellularFilter} public functionality
 */
public class CellularFilterTest {
	/* Test members */
	private static CellularFilter cellularFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		cellularFilter = new CellularFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(cellularFilter);
	}

	@Test
	public void test() {
		BufferedImage result = cellularFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CELLULAR.to(), "jpg");
	}

	
	@Test
	public void testSetStretch() {
		float vp_streach = TestUtils.genFloat(1);
		cellularFilter.setStretch(vp_streach);
		assertEquals(vp_streach, cellularFilter.stretch, EPSILON);
	}
	
	@Test
	public void testSetAngle() {
		float vp_angle = TestUtils.genFloat(1);
		cellularFilter.setAngle(vp_angle);
		assertEquals(vp_angle, cellularFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testGetAngle() {
		testSetAngle();
	}
	
	@Test
	public void testSetCoefficient() {
		int vp_index = TestUtils.genInteger(4);
		float vp_float = TestUtils.genFloat();
		cellularFilter.setCoefficient(vp_index, vp_float);
		assertEquals(vp_float, cellularFilter.getCoefficient(vp_index), EPSILON);
	}
	
	@Test
	public void testGetCoefficient() {
		testSetCoefficient();
	}
	
	@Test
	public void testSetAngleCoefficient() {
		float vp_angle_coef = TestUtils.genFloat();
		cellularFilter.setAngleCoefficient(vp_angle_coef);
		assertEquals(vp_angle_coef, cellularFilter.getAngleCoefficient(), EPSILON);
	}
	
	@Test
	public void testGetAngleCoefficient() {
		testSetAngleCoefficient();
	}
	
	@Test
	public void testSetGradientCoefficient() {
		float gradientCoefficient = TestUtils.genFloat();
		cellularFilter.setGradientCoefficient(gradientCoefficient);
		assertEquals(gradientCoefficient, cellularFilter.getGradientCoefficient(), EPSILON);
	}
	
	@Test
	public void testGetGradientCoefficient() {
		testSetGradientCoefficient();
	}
	
	@Test
	public void testSetF1() {
		float v = TestUtils.genFloat();
		cellularFilter.setF1(v);
		assertEquals(v, cellularFilter.getF1(), EPSILON);
	}
	
	@Test
	public void testGetF1() {
		testSetF1();
	}
	
	@Test
	public void testSetF2() {
		float v = TestUtils.genFloat();
		cellularFilter.setF2(v);
		assertEquals(v, cellularFilter.getF2(), EPSILON);
	}
	
	@Test
	public void testGetF2() {
		testSetF2();
	}
	
	@Test
	public void testSetF3() {
		float v = TestUtils.genFloat();
		cellularFilter.setF3(v);
		assertEquals(v, cellularFilter.getF3(), EPSILON);
	}
	
	@Test
	public void testGetF3() {
		testSetF3();
	}
	
	@Test
	public void testSetF4() {
		float v = TestUtils.genFloat();
		cellularFilter.setF4(v);
		assertEquals(v, cellularFilter.getF4(), EPSILON);
	}
	
	@Test
	public void testGetF4() {
		testSetF4();
	}
	
	@Test
	public void testSetColormap() {
		Colormap colorMap = new LinearColormap(Color.GREEN.getRGB(), Color.PINK.getRGB());
		cellularFilter.setColormap(colorMap);
		assertEquals(colorMap, cellularFilter.getColormap());
	}
	
	@Test
	public void testGetColormap() {
		testSetColormap();
	}
	
	@Test
	public void testSetGridType() {
		cellularFilter.setGridType(Shape.TRIANGULAR);
		assertEquals(Shape.TRIANGULAR, cellularFilter.getGridType());
	}
	
	@Test
	public void testGetGridType() {
		testSetGridType();
	}
	
	@Test
	public void testSetDistancePower() {
		float distancePower = TestUtils.genFloat();
		cellularFilter.setDistancePower(distancePower);
		assertEquals(distancePower, cellularFilter.getDistancePower(), EPSILON);
	}
	
	@Test
	public void testGetDistancePower() {
		testSetDistancePower();
	}
	
	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloat();
		cellularFilter.setTurbulence(turbulence);
		assertEquals(turbulence, cellularFilter.getTurbulence(), EPSILON);
	}
	
	@Test
	public void testGetTurbulence() {
		testSetTurbulence();
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		cellularFilter.setAmount(amount);
		assertEquals(amount, cellularFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testGetAmount() {
		testSetAmount();
	}
	
	@Test
	public void testToString() {
		assertTrue(cellularFilter.toString().contains(CellularFilter.class.getName()));
	}
	
	@Test
	public void testClone() {
		assertNotNull(cellularFilter.clone());
	}
}
