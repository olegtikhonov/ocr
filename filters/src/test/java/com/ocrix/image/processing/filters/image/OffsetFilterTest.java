/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.OFFSET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link OffsetFilter} functionality.
 */
public class OffsetFilterTest {

	private OffsetFilter offsetFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		offsetFilter = new OffsetFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(offsetFilter);
	}
	
	
	@Test
	public void testOffsetFilter(){
		offsetFilter.setXOffset(ANGEL_NUMBER);
		offsetFilter.setYOffset(ANGEL_NUMBER);
		BufferedImage result = offsetFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + OFFSET.to(), "jpeg");
	}
	
	@Test
	public void testSetXOffset() {
		int xOffset = TestUtils.genInteger();
		offsetFilter.setXOffset(xOffset);
		assertEquals(xOffset, offsetFilter.getXOffset());
		
	}
	
	@Test
	public void testSetYOffset() {
		int yOffset = TestUtils.genInteger();
		offsetFilter.setYOffset(yOffset);
		assertEquals(yOffset, offsetFilter.getYOffset());
		
	}
	
	@Test
	public void testSetWrap() {
		boolean wrap = true;
		// Case 1: true.
		offsetFilter.setWrap(wrap);
		assertTrue(offsetFilter.getWrap());
		wrap = false;
		offsetFilter.setWrap(wrap);
		assertFalse(offsetFilter.getWrap());
	}
	
	@Test
	public void testTransformInverse() {
		int x = TestUtils.genInteger();
		int y = TestUtils.genInteger();
		float[] out = new float[2];
		offsetFilter.setWidth(src.getWidth());
		offsetFilter.setHeight(src.getHeight());
		offsetFilter.transformInverse(x, y, out);
		assertTrue(true);
	}
	
	@Test
	public void testTransformInverseWrapFalse() {
		int x = TestUtils.genInteger();
		int y = TestUtils.genInteger();
		float[] out = new float[2];
		offsetFilter.setWidth(src.getWidth());
		offsetFilter.setHeight(src.getHeight());
		offsetFilter.setWrap(false);
		offsetFilter.transformInverse(x, y, out);
		assertTrue(true);
	}
	
	@Test
	public void testFilterWrapFalse() {
		offsetFilter.setWrap(false);
		BufferedImage result = offsetFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(offsetFilter.toString().contains(OffsetFilter.class.getSimpleName()));
	}
}
