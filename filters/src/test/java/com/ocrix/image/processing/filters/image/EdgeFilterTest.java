/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.EDGE;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


public class EdgeFilterTest {
	/* Test member declarations */
	private EdgeFilter edgeFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		edgeFilter = new EdgeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(edgeFilter);
	}
	
	
	@Test
	public void testEdgeFilter() throws Exception {
		BufferedImage im = ImageIO.read(new File(PREFIXPATH + TEXT));
		BufferedImage result = edgeFilter.filter(im, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + EDGE.to() + "_2", "bmp");
	}
	
	@Test
	public void testEdgeFilterAllTwoParams() throws Exception {
		BufferedImage im = ImageIO.read(new File(PREFIXPATH + TEXT));
		BufferedImage result = edgeFilter.filter(src, im);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + EDGE.to(), "bmp");
	}
	@Test (expected=IllegalArgumentException.class)
	public void testEdgeFilterNullSource(){
		BufferedImage result = edgeFilter.filter(null, null);
		assertNull(result);
	}

	@Test
	public void testSetVEdgeMatrix() {
		float[] vEdgeMatrix = TestUtils.generateFloatArray(HEPTAGON);
		edgeFilter.setVEdgeMatrix(vEdgeMatrix);
		assertEquals(vEdgeMatrix, edgeFilter.getVEdgeMatrix());
	}
	@Test
	public void testSetHEdgeMatrix() {
		float[] hEdgeMatrix = TestUtils.generateFloatArray(HEPTAGON);
		edgeFilter.setHEdgeMatrix(hEdgeMatrix);
		assertEquals(hEdgeMatrix, edgeFilter.getHEdgeMatrix());
	}
	@Test
	public void testToString() {
		assertTrue(edgeFilter.toString().contains(EdgeFilter.class.getName()));
	}
}
