/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static org.junit.Assert.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import static com.ocrix.image.processing.filters.Filters.MOTION_BLUR_OP;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link MotionBlurOp} functionality.
 */
public class MotionBlurOpTest {

	private MotionBlurOp motionBlurOp;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		motionBlurOp = new MotionBlurOp();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testMotionBlurOp() {
		assertNotNull(motionBlurOp);
	}

	@Test
	public void testMotionBlurOpFloatFloatFloatFloat() {
		float distance = TestUtils.genFloat();
		float angle = TestUtils.genFloat();
		float rotation = TestUtils.genFloat();
		float zoom = TestUtils.genFloat();
		
		motionBlurOp = new MotionBlurOp(distance, angle, rotation, zoom);
		assertNotNull(motionBlurOp);
	}

	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		motionBlurOp.setAngle(angle);
		assertEquals(angle, motionBlurOp.getAngle(), EPSILON);
	}

	@Test
	public void testGetAngle() {
		testSetAngle();
	}

	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		motionBlurOp.setDistance(distance);
		assertEquals(distance, motionBlurOp.getDistance(), EPSILON);
	}

	@Test
	public void testGetDistance() {
		testSetDistance();
	}

	@Test
	public void testSetRotation() {
		float rotation = TestUtils.genFloat();
		motionBlurOp.setRotation(rotation);
		assertEquals(rotation, motionBlurOp.getRotation(), EPSILON);
	}

	@Test
	public void testGetRotation() {
		testSetRotation();
	}

	@Test
	public void testSetZoom() {
		float zoom = TestUtils.genFloat();
		motionBlurOp.setZoom(zoom);
		assertEquals(zoom, motionBlurOp.getZoom(), EPSILON);
	}

	@Test
	public void testGetZoom() {
		testSetZoom();
	}

	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		motionBlurOp.setCentreX(centreX);
		assertEquals(centreX, motionBlurOp.getCentreX(), EPSILON);
		
	}

	@Test
	public void testGetCentreX() {
		testSetCentreX();
	}

	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		motionBlurOp.setCentreY(centreY);
		assertEquals(centreY, motionBlurOp.getCentreY(), EPSILON);
	}

	@Test
	public void testGetCentreY() {
		testSetCentreY();
	}

	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat());
		motionBlurOp.setCentre(centre);
		assertEquals(centre, motionBlurOp.getCentre());
	}

	@Test
	public void testGetCentre() {
		testSetCentre();
	}

	@Test
	public void testFilter() {
		motionBlurOp.setDistance(5.1f);
		BufferedImage result = motionBlurOp.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MOTION_BLUR_OP.to(), "png");
	}
	
	@Test
	public void testFilterDstIsNullRotationNotZero() {
		motionBlurOp.setZoom(TestUtils.genFloat());
		motionBlurOp.setRotation(TestUtils.genFloat());
		BufferedImage result = motionBlurOp.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterStepsIsZero() {
		/*
		 *  float maxDistance = distance + Math.abs(rotation * imageRadius) + zoom * imageRadius;
		 *  int steps = log2((int) maxDistance);
		 */
		motionBlurOp.setDistance(0.0f);
		motionBlurOp.setRotation(0.0f);
		motionBlurOp.setZoom(0.0f);
		BufferedImage result = motionBlurOp.filter(src, src);
		assertNotNull(result);
	}

	@Test
	public void testToString() {
		assertTrue(motionBlurOp.toString().contains(MotionBlurOp.class.getSimpleName()));
	}
}
