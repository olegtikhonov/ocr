/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.ERODE;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ErodeFilter} functionality.
 */
public class ErodeFilterTest {

	private static ErodeFilter erodeFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		erodeFilter = new ErodeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor(){
		assertNotNull(erodeFilter);
	}
	
	@Test
	public void testErodeFilter() throws IOException{
		erodeFilter.setThreshold(0);
		erodeFilter.setIterations(1);
		BufferedImage result = erodeFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), src);
		assertNotNull(erodeFilter);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + ERODE.to(), "jpeg");
	}
	
	@Test
	public void testGetThreshold() {
		int threshold = TestUtils.genInteger();
		erodeFilter.setThreshold(threshold);
		assertEquals(threshold, erodeFilter.getThreshold());
	}
	
	@Test
	public void testToString() {
		assertTrue(erodeFilter.toString().contains(ErodeFilter.class.getName()));
	}
}
