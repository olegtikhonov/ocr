/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.QUANTIZE;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link QuantizeFilter} functionality.
 */
public class QuantizeFilterTest {

	private QuantizeFilter quantizeFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		quantizeFilter = new QuantizeFilter();
		src = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(quantizeFilter);
	}
	
	@Test
	public void testQuantizeFilter(){
		BufferedImage result = quantizeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + QUANTIZE.to(), "tiff");
	}
	
	@Test
	public void testToString() {
		assertEquals(quantizeFilter.toString(),QuantizeFilter.class.getName());
	}
	
	@Test
	public void testSetNumColors() {
		final int numColors = TestUtils.genInteger(RONNA);
		// It a little bit tricky, due to ..
		// Math.min(Math.max(numColors, 8), 256)
		int trickyValue = Math.min(Math.max(numColors, 8), 256);
		quantizeFilter.setNumColors(numColors);
		assertEquals(trickyValue, quantizeFilter.getNumColors());
	}
	
	@Test
	public void testSetDither() {
		// Case 1: dither is true.
		boolean dither = true;
		quantizeFilter.setDither(dither);
		assertTrue(quantizeFilter.getDither());
		
		// Case 2: dither is false.
		dither = false;
		quantizeFilter.setDither(dither);
		assertFalse(quantizeFilter.getDither());
	}
	
	@Test
	public void testSetSerpentine() {
		// Case 1: serpentine is true.
		boolean serpentine = true;
		quantizeFilter.setSerpentine(serpentine);
		assertTrue(quantizeFilter.getSerpentine());
		
		// Case 2: serpentine is false.
		serpentine = false;
		quantizeFilter.setSerpentine(serpentine);
		assertFalse(quantizeFilter.getSerpentine());
	}
	
	@Test
	public void testQuantizeDitherTrue() {
		quantizeFilter.setDither(true);
		
		int[] inPixels = TestUtils.convert2Dto1D(TestUtils.convertTo2DUsingGetRGB(src));
		int[] outPixels = new int[inPixels.length];
		int width = src.getWidth();
		int height = src.getHeight();
		int numColors = TestUtils.genInteger(RONNA);
		boolean dither = true;
		boolean serpentine = true;
		quantizeFilter.quantize(inPixels, outPixels, width, height, numColors, dither, serpentine);
		assertTrue(true);
	}
}
