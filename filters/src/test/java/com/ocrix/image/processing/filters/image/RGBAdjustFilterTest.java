/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.RGB_ADJUST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link RGBAdjustFilter} functionality. 
 */
public class RGBAdjustFilterTest {
	private static RGBAdjustFilter rgbAdjustFilter = null;
	private static BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		rgbAdjustFilter = new RGBAdjustFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(rgbAdjustFilter);
	}

	
	@Test
	public void testRGBAdjustFilter(){
		rgbAdjustFilter.setRFactor(0.3f);
		rgbAdjustFilter.setGFactor(0.4f);
		rgbAdjustFilter.setBFactor(0.2f);
		BufferedImage result = rgbAdjustFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + RGB_ADJUST.to(), "jpeg");
	}
	
	@Test
	public void testSetRFactor() {
		float rFactor = TestUtils.genFloat();
		rgbAdjustFilter.setRFactor(rFactor);
		assertEquals(rFactor, rgbAdjustFilter.getRFactor(), EPSILON);
	}
	
	@Test
	public void testSetGFactor() {
		float gFactor = TestUtils.genFloat();
		rgbAdjustFilter.setGFactor(gFactor);
		assertEquals(gFactor, rgbAdjustFilter.getGFactor(), EPSILON);
	}
	
	@Test
	public void testSetBFactor() {
		float bFactor = TestUtils.genFloat();
		rgbAdjustFilter.setBFactor(bFactor);
		assertEquals(bFactor, rgbAdjustFilter.getBFactor(), EPSILON);
	}
	
	@Test
	public void testGetLUT() {
		int[] lut = rgbAdjustFilter.getLUT();
		assertNotNull(lut);
	}
	
	@Test
	public void testFilterRGB(){
		int result = rgbAdjustFilter.filterRGB(0, 0, Color.RED.getRGB());
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(rgbAdjustFilter.toString().contains(RGBAdjustFilter.class.getSimpleName()));
	}
}
