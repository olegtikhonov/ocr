/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FEEDBACK;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


public class FeedbackFilterTest {

	private static FeedbackFilter feedbackFilter = null; 
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		feedbackFilter = new FeedbackFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(feedbackFilter);
	}
	
	@Test
	public void testFeedBackFilter() throws IOException{
		feedbackFilter.setEndAlpha(ALPHA);
		feedbackFilter.setRotation(-23.567890f);
		feedbackFilter.setIterations(3);
		BufferedImage result = feedbackFilter.filter(src, ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FEEDBACK.to(), "png");
	}
	
	@Test
	public void testFeedbackFilter() {
		float distance = TestUtils.genFloat(); 
		float angle = TestUtils.genFloat();
		float rotation = TestUtils.genFloat(); 
		float zoom = TestUtils.genFloat();
		
		feedbackFilter = new FeedbackFilter(distance, angle, rotation, zoom);
		assertNotNull(feedbackFilter);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		feedbackFilter.setAngle(angle);
		assertEquals(angle, feedbackFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		feedbackFilter.setDistance(distance);
		assertEquals(distance, feedbackFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRotation() {
		float rotation = TestUtils.genFloat();
		feedbackFilter.setRotation(rotation);
		assertEquals(rotation, feedbackFilter.getRotation(), EPSILON);
	}
	
	@Test
	public void testSetZoom() {
		float zoom = TestUtils.genFloat();
		feedbackFilter.setZoom(zoom);
		assertEquals(zoom, feedbackFilter.getZoom(), EPSILON);
	}
	
	@Test
	public void testSetStartAlpha() {
		float startAlpha = TestUtils.genFloat();
		feedbackFilter.setStartAlpha(startAlpha);
		assertEquals(startAlpha, feedbackFilter.getStartAlpha(), EPSILON);
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		feedbackFilter.setCentreX(centreX);
		assertEquals(centreX, feedbackFilter.getCentreX(), EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		feedbackFilter.setCentreY(centreY);
		assertEquals(centreY, feedbackFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float (TestUtils.genFloat(QUAD), TestUtils.genFloat(QUAD));
		feedbackFilter.setCentre(centre);
		assertEquals(centre, feedbackFilter.getCentre());
	}
	
	@Test
	public void testSetIterations() {
		int iterations = TestUtils.genInteger();
		feedbackFilter.setIterations(iterations);
		assertEquals(iterations, feedbackFilter.getIterations());
	}
	
	@Test
	public void testSetEndAlpha() {
		float endAlpha = TestUtils.genFloat();
		feedbackFilter.setEndAlpha(endAlpha);
		assertEquals(endAlpha, feedbackFilter.getEndAlpha(), EPSILON);
	}
	
	
	//-----------------------------
	// Other cases
	//-----------------------------
	@Test (expected=IllegalArgumentException.class)
	public void testFilterDstEqNullIterationBiggerThanZero() {
		feedbackFilter.setIterations(1);
		BufferedImage result = feedbackFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterDstEqNullIterationZero() {
		feedbackFilter.setIterations(0);
		BufferedImage result = feedbackFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(feedbackFilter.toString().contains(FeedbackFilter.class.getName()));
	}

}
