/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;
import static com.ocrix.image.processing.filters.composite.TestParams.HRADIUS;
import static com.ocrix.image.processing.filters.composite.TestParams.BETA;
import static com.ocrix.image.processing.filters.composite.TestParams.ANGEL_NUMBER;

/**
 * Tests {@link SCNoise} functionality.
 */
public class TestSCNoise {

	private SCNoise noise;
	
	@Before
	public void setUp() throws Exception {
		noise = new SCNoise();
	}

	@Test
	public void testEvaluateFloat() {
		float genF = TestUtils.genFloat();
		float result = noise.evaluate(genF);
		assertTrue(result != 0.0f);
	}

	@Test
	public void testEvaluateFloatFloat() {
		float value = TestUtils.genFloat();
		float error = TestUtils.genFloat();
		float result = noise.evaluate(value, error);
		assertTrue(result != 0.0f);
	}

	@Test
	public void testEvaluateFloatFloatFloat() {
		float result = noise.evaluate(TestUtils.genFloat(), TestUtils.genFloat(), TestUtils.genFloat());
		assertTrue(result != 0.0f);
	}

	@Test
	public void testFloor() {
		float value = TestUtils.genFloat() + HRADIUS;
		float result = SCNoise.floor(value);
		assertTrue(result >= (int)HRADIUS);
	}

	@Test
	public void testCatrom2() {
		float value = TestUtils.genFloat() + BETA;
		float result = noise.catrom2(value);
		assertTrue(result != 0.0f);
	}

	@Test
	public void testImpulseTabInit() {
		float[] result = SCNoise.impulseTabInit(ANGEL_NUMBER);
		assertNotNull(result);
	}

}
