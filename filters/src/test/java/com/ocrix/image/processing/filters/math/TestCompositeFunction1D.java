/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.math;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link CompositeFunction1D} functionality.
 */
public class TestCompositeFunction1D {

	private CompositeFunction1D cf1d;
	
	@Before
	public void setUp() throws Exception {
		cf1d = new  CompositeFunction1D(new MathFunction1D(MathFunction1D.ATAN), new MathFunction1D(MathFunction1D.ASIN));
	}

	@Test
	public void testCompositeFunction1D() {
		assertNotNull(cf1d);
	}

	@Test
	public void testEvaluate() {
		float value = TestUtils.genFloat();
		float result = cf1d.evaluate(value);
		assertTrue(result != 0.0f);
	}
	
	@Test
	public void testToString(){
		assertTrue(cf1d.toString().contains(CompositeFunction1D.class.getName()));
	}
}
