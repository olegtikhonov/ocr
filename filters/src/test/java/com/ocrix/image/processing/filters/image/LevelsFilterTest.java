/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.LEVELS;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link LevelsFilter} functionality.
 */
public class LevelsFilterTest {

	private static LevelsFilter levelsFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		levelsFilter = new LevelsFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(levelsFilter);
	}
	
	@Test
	public void testLevelsFilter(){
		levelsFilter.setLowLevel(ALPHA);
		levelsFilter.setHighOutputLevel(ALPHA);
		levelsFilter.setHighLevel(ALPHA + 0.0987f);
		BufferedImage result = levelsFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + LEVELS.to(), "bmp");
	}
	
	@Test
	public void testGetLowLevel() {
		float lowLevel = TestUtils.genFloat();
		levelsFilter.setLowLevel(lowLevel);
		assertEquals(lowLevel, levelsFilter.getLowLevel(), EPSILON);
	}
	
	@Test
	public void testGetHighLevel() {
		float highLevel = TestUtils.genFloat();
		levelsFilter.setHighLevel(highLevel);
		assertEquals(highLevel, levelsFilter.getHighLevel(), EPSILON);
	}
	
	@Test
	public void testGetLowOutputLevel() {
		float lowOutputLevel = TestUtils.genFloat();
		levelsFilter.setLowOutputLevel(lowOutputLevel);
		assertEquals(lowOutputLevel, levelsFilter.getLowOutputLevel(), EPSILON);
	}
	
	@Test
	public void testGetHighOutputLevel() {
		float highOutputLevel = TestUtils.genFloat();
		levelsFilter.setHighOutputLevel(highOutputLevel);
		assertEquals(highOutputLevel, levelsFilter.getHighOutputLevel(), EPSILON);
	}
	
	@Test
	public void testToString(){
		assertEquals(levelsFilter.toString(), LevelsFilter.class.getName());
	}
}
