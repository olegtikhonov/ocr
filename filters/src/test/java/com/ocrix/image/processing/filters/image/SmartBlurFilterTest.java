/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.SMART_BLUR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.awt.image.Kernel;
import java.io.File;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link SmartBlurFilter} functionality.
 */
public class SmartBlurFilterTest {
	private static final int FIVE = 5;
	private SmartBlurFilter smartBlurFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		smartBlurFilter = new SmartBlurFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(smartBlurFilter);
	}
	
	@Test
	public void testSmartBlurFilter(){
		smartBlurFilter.setHRadius(2000);
		smartBlurFilter.setRadius(2000);
		smartBlurFilter.setVRadius(3000);
		smartBlurFilter.setThreshold(10);
		BufferedImage result = smartBlurFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SMART_BLUR.to(), "tiff");
	}
	
	@Test
	public void testSmartBlurFilterDstNotNull(){
		BufferedImage result = smartBlurFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testThresholdBlur() {
		try {
			Kernel kernel = GaussianFilter.makeKernel(TestUtils.genFloat());
			int[] inPixels = TestUtils.fillArray(FIVE);
			int[] outPixels = TestUtils.fillArray(FIVE);
			int width = src.getWidth(); 
			int height = src.getWidth();
			boolean alpha = true;
			
			Method thresholdBllur = smartBlurFilter.getClass().getDeclaredMethod(
					                                                             "thresholdBlur", 
					                                                             Kernel.class,
							                                                     int[].class, 
							                                                     int[].class, 
							                                                     Integer.TYPE,
							                                                     Integer.TYPE, 
							                                                     Boolean.TYPE);
			thresholdBllur.setAccessible(true);
			thresholdBllur.invoke(smartBlurFilter, kernel, inPixels, outPixels, 
					                               width, height, alpha);			
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testSetVRadius() {
		int vRadius = TestUtils.genInteger();
		smartBlurFilter.setVRadius(vRadius);
		assertEquals(vRadius, smartBlurFilter.getVRadius());
	}
	
	@Test
	public void testSetHRadius() {
		int hRadius = TestUtils.genInteger();
		smartBlurFilter.setHRadius(hRadius);
		assertEquals(hRadius, smartBlurFilter.getHRadius());
	}
	
	@Test
	public void testSetRadius() {
		int radius = TestUtils.genInteger();
		smartBlurFilter.setRadius(radius);
		assertEquals(radius, smartBlurFilter.getRadius());
	}
	
	@Test
	public void testThreshold() {
		int threshold = TestUtils.genInteger();
		smartBlurFilter.setThreshold(threshold);
		assertEquals(threshold, smartBlurFilter.getThreshold());
	}
	
	@Test
	public void testToString() {
		assertTrue(smartBlurFilter.toString().contains(SmartBlurFilter.class.getSimpleName()));
	}
	
}
