/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.PREMULTIPLY;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PremultiplyFilter} functionality.
 */
public class PremultiplyFilterTest {
	private PremultiplyFilter premultiplyFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		premultiplyFilter = new PremultiplyFilter();
		src = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(premultiplyFilter);
	}
	
	@Test
	public void testPremultiplyFilter(){
		BufferedImage result = premultiplyFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + PREMULTIPLY.to(), "gif");
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(src.getWidth());
		int y = TestUtils.genInteger(src.getWidth());
		int rgb = src.getRGB(x, y);
		int result = premultiplyFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}

	@Test
	public void testToString() {
		assertTrue(premultiplyFilter.toString().contains(PremultiplyFilter.class.getSimpleName()));
	}
}
