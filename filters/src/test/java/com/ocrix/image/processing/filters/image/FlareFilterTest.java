/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FLARE;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link FlareFilter} functionality.
 */
public class FlareFilterTest {

	private static FlareFilter flareFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		flareFilter = new FlareFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(flareFilter);
	}


	@Test
	public void testFlareFilter(){
		flareFilter.setRayAmount(2);
		flareFilter.setRadius(70);
		flareFilter.setColor(Color.BLACK.getRGB());
		BufferedImage result = flareFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FLARE.to(), "png");
	}
	
	@Test
	public void testSetColor() {
		int color = TestUtils.genInteger();
		flareFilter.setColor(color);
		assertEquals(color, flareFilter.getColor());
	}
	
	@Test
	public void testSetRingWidth() {
		float ringWidth = TestUtils.genFloat();
		flareFilter.setRingWidth(ringWidth);
		assertEquals(ringWidth, flareFilter.getRingWidth(), EPSILON);
	}
	
	@Test
	public void testSetBaseAmount() {
		float baseAmount = TestUtils.genFloat();
		flareFilter.setBaseAmount(baseAmount);
		assertEquals(baseAmount, flareFilter.getBaseAmount(), EPSILON);
	}
	
	@Test
	public void testSetRingAmount() {
		float ringAmount = TestUtils.genFloat();
		flareFilter.setRingAmount(ringAmount);
		assertEquals(ringAmount, flareFilter.getRingAmount(), EPSILON);
	}
	
	@Test
	public void testSetRayAmount() {
		float rayAmount = TestUtils.genFloat();
		flareFilter.setRayAmount(rayAmount);
		assertEquals(rayAmount, flareFilter.getRayAmount(), EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D centre = new Point2D.Float();
		flareFilter.setCentre(centre);
		assertEquals(centre, flareFilter.getCentre());
	}
	
	@Test
	public void testGetRadius() {
		float radius = TestUtils.genFloat();
		flareFilter.setRadius(radius);
		assertEquals(radius, flareFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(); 
		int y = TestUtils.genInteger(); 
		int rgb = TestUtils.genInteger();
		flareFilter.filterRGB(x, y, rgb);
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(flareFilter.toString().contains(FlareFilter.class.getName()));
	}
	
	@Test
	public void testSetSigma() {
		
		Method method;
		try {
			method = FlareFilter.class.getDeclaredMethod("setSigma", Float.TYPE);
			method.setAccessible(true);
			float sigma = TestUtils.genFloat();
			method.invoke(flareFilter, sigma);
			float result = flareFilter.getSigma();
			assertEquals(sigma, result, EPSILON);
		} catch (NoSuchMethodException e) {
			assertTrue(false);
		} catch (SecurityException e) {
			assertTrue(false);
		} catch (IllegalAccessException e) {
			assertTrue(false);
		} catch (IllegalArgumentException e) {
			assertTrue(false);
		} catch (InvocationTargetException e) {
			assertTrue(false);
		}
	}
}
