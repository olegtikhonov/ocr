package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.EMBOSS;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


public class EmbossFilterTest {

	private static EmbossFilter embossFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		embossFilter = new EmbossFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(embossFilter);
	}
	
	
	@Test
	public void testFilter(){
		embossFilter.setEmboss(true);
		BufferedImage result = embossFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + EMBOSS.to(), "png");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFilterNullSrc(){
		BufferedImage result = embossFilter.filter(null, null);
		assertNull(result);
	}
	
	@Test
	public void testSetAzimuth(){
		float azimuth = TestUtils.genFloat();
		embossFilter.setAzimuth(azimuth);
		assertEquals(azimuth, embossFilter.getAzimuth(), EPSILON);
	}
	
	@Test
	public void testSetElevation() {
		float elevation = TestUtils.genFloat();
		embossFilter.setElevation(elevation);
		assertEquals(elevation, embossFilter.getElevation(), EPSILON);
	}
	
	@Test
	public void testSetBumpHeight() {
		float bumpHeight = TestUtils.genFloat();
		embossFilter.setBumpHeight(bumpHeight);
		assertEquals(bumpHeight, embossFilter.getBumpHeight(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(embossFilter.toString().contains(EmbossFilter.class.getName()));
	}
}
