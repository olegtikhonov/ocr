/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.MOTION_BLUR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * 
 */
public class MotionBlurFilterTest {

	private static MotionBlurFilter motionBlurFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		motionBlurFilter = new MotionBlurFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(motionBlurFilter);
	}
	
	
	@Test
	public void testMotionBlurFilter(){
		motionBlurFilter.setZoom(0.025f);
		BufferedImage result = motionBlurFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MOTION_BLUR.to(), "tiff");
	}
	
	@Test
	public void testMotionBlurFilterDstNotNull() throws IOException{
		motionBlurFilter.setRotation(0.025f);
		motionBlurFilter.setPremultiplyAlpha(false);
		motionBlurFilter.setWrapEdges(true);
		BufferedImage result = motionBlurFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MOTION_BLUR.to() + "_DST_NOT_NULL", "tiff");
	}
	
	@Test
	public void testCitor4Paramteres(){
		float distance = TestUtils.genFloat();
		float angle = TestUtils.genFloat();
		float rotation = TestUtils.genFloat();
		float zoom = TestUtils.genFloat();
		motionBlurFilter = new MotionBlurFilter(distance, angle, rotation, zoom);
		assertNotNull(motionBlurFilter);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		motionBlurFilter.setAngle(angle);
		assertEquals(angle, motionBlurFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		motionBlurFilter.setDistance(distance);
		assertEquals(distance, motionBlurFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRotation() {
		float rotation = TestUtils.genFloat();
		motionBlurFilter.setRotation(rotation);
		assertEquals(rotation, motionBlurFilter.getRotation(), EPSILON);
	}
	
	@Test
	public void testSetZoom() {
		float zoom = TestUtils.genFloat();
		motionBlurFilter.setZoom(zoom);
		assertEquals(zoom, motionBlurFilter.getZoom(), EPSILON);
	}
	
	@Test
	public void testSetWrapEdges() {
		boolean wrapEdges = true;
		motionBlurFilter.setWrapEdges(wrapEdges);
		assertEquals(wrapEdges, motionBlurFilter.getWrapEdges());
		
		wrapEdges = false;
		motionBlurFilter.setWrapEdges(wrapEdges);
		assertEquals(wrapEdges, motionBlurFilter.getWrapEdges());
	}
	
	@Test
	public void testSetPremultiplyAlpha() {
		boolean premultiplyAlpha = true;
		motionBlurFilter.setPremultiplyAlpha(premultiplyAlpha);
		assertEquals(premultiplyAlpha, motionBlurFilter.getPremultiplyAlpha());
		
		premultiplyAlpha = false;
		motionBlurFilter.setPremultiplyAlpha(premultiplyAlpha);
		assertEquals(premultiplyAlpha, motionBlurFilter.getPremultiplyAlpha());
	}
	
	@Test 
	public void testToString() {
		assertTrue(motionBlurFilter.toString().contains(MotionBlurFilter.class.getSimpleName()));
	}
}
