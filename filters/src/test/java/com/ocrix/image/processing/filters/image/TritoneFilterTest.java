package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class TritoneFilterTest {

	private static TritoneFilter tritoneFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		tritoneFilter = new TritoneFilter();
		src =ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(tritoneFilter);
	}
	
	@Test
	public void testTritoneFilter(){
		tritoneFilter.setHighColor(Color.YELLOW.getRGB());
		tritoneFilter.setMidColor(Color.GREEN.getRGB());
		BufferedImage result = tritoneFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "TritoneFilter", "jpeg");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
