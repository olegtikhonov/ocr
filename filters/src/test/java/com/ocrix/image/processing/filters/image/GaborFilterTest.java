/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GABOR;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.NONAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GaborFilter} functionality.
 */
public class GaborFilterTest {

	private static final double DELTA = 0.000001d;
	private GaborFilter gaborFilter;
	
	@Before
	public void setUp() throws Exception {
		gaborFilter = new GaborFilter();
	}

	@Test
	public void testGaborFilter() {
		assertNotNull(gaborFilter);
	}

	@Test
	public void testGaborFilterDouble() {
		double waveLength = TestUtils.genDouble();
		gaborFilter = new GaborFilter(waveLength);
		assertEquals(waveLength, gaborFilter.getWaveLength(), DELTA);
	}

	@Test
	public void testGaborFilterDoubleDoubleArray() {
		double waveLength = TestUtils.genDouble();
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		gaborFilter = new GaborFilter(waveLength, orientations);
		assertEquals(orientations, gaborFilter.getOrientations());
	}

	@Test
	public void testGaborFilterDoubleDoubleArrayDouble() {
		double waveLength = TestUtils.genDouble();
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		double phaseOffset = TestUtils.genDouble();
		gaborFilter = new GaborFilter(waveLength, orientations, phaseOffset);
		assertEquals(phaseOffset, gaborFilter.getPhaseOffset(), DELTA);
	}

	@Test
	public void testGaborFilterDoubleDoubleArrayDoubleDouble() {
		double waveLength = TestUtils.genDouble();
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		double phaseOffset = TestUtils.genDouble();
		double aspectRatio = TestUtils.genDouble();
		gaborFilter = new GaborFilter(waveLength, orientations, phaseOffset, aspectRatio);
		assertEquals(aspectRatio, gaborFilter.getAspectRatio(), DELTA);
	}

	@Test
	public void testGaborFilterDoubleDoubleArrayDoubleDoubleDouble() {
		double waveLength = TestUtils.genDouble();
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		double phaseOffset = TestUtils.genDouble();
		double aspectRatio = TestUtils.genDouble();
		double bandwidth = TestUtils.genDouble();
		gaborFilter = new GaborFilter(waveLength, orientations, phaseOffset, aspectRatio, bandwidth);
		assertEquals(bandwidth, gaborFilter.getBandwidth(), DELTA);
	}

	@Test
	public void testGaborFilterDoubleDoubleArrayDoubleDoubleDoubleIntInt() {
		double waveLength = TestUtils.genDouble();
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		double phaseOffset = TestUtils.genDouble();
		double aspectRatio = TestUtils.genDouble();
		double bandwidth = TestUtils.genDouble();
		int width = TestUtils.genInteger();
		int height = TestUtils.genInteger();
		gaborFilter = new GaborFilter(waveLength, orientations, phaseOffset, aspectRatio, bandwidth, width, height);
		assertEquals(width, gaborFilter.getWidth());
		assertEquals(height, gaborFilter.getHeight());
	}

	@Test
	public void testGetOrientations() {
		double[] orientations = TestUtils.getDoubleArray(NONAGON);
		gaborFilter.setOrientations(orientations);
		assertEquals(orientations, gaborFilter.getOrientations());
	}

	@Test
	public void testSetOrientations() {
		testGetOrientations();
	}

	@Test
	public void testGetWaveLength() {
		double waveLength = TestUtils.genDouble();
		gaborFilter.setWaveLength(waveLength);
		assertEquals(waveLength, gaborFilter.getWaveLength(), DELTA);
	}

	@Test
	public void testSetWaveLength() {
		testGetWaveLength();
	}

	@Test
	public void testGetPhaseOffset() {
		double phaseOffset = TestUtils.genDouble();
		gaborFilter.setPhaseOffset(phaseOffset);
		assertEquals(phaseOffset, gaborFilter.getPhaseOffset(), DELTA);
	}

	@Test
	public void testSetPhaseOffset() {
		testGetPhaseOffset();
	}

	@Test
	public void testGetAspectRatio() {
		double aspectRatio = TestUtils.genDouble();
		gaborFilter.setAspectRatio(aspectRatio);
		assertEquals(aspectRatio, gaborFilter.getAspectRatio(), DELTA);
	}

	@Test
	public void testSetAspectRatio() {
		testGetAspectRatio();
	}

	@Test
	public void testGetBandwidth() {
		double bandwidth = TestUtils.genDouble();
		gaborFilter.setBandwidth(bandwidth);
		assertEquals(bandwidth, gaborFilter.getBandwidth(), DELTA);
	}

	@Test
	public void testSetBandwidth() {
		testGetBandwidth();
	}

	@Test
	public void testGetConvolveOp() {
		ConvolveOp result = gaborFilter.getConvolveOp();
		assertNotNull(result);
	}

	@Test
	public void testGetKernel() {
		Kernel kernel = gaborFilter.getKernel();
		assertNotNull(kernel);
	}

	@Test
	public void testGetWidth() {
		int width = TestUtils.genInteger();
		gaborFilter.setWidth(width);
		assertEquals(width, gaborFilter.getWidth());
	}

	@Test
	public void testSetWidth() {
		testGetWidth();
	}

	@Test
	public void testGetHeight() {
		int height = TestUtils.genInteger();
		gaborFilter.setHeight(height);
		assertEquals(height, gaborFilter.getHeight());
	}

	@Test
	public void testSetHeight() {
		testGetHeight();
	}

	@Test
	public void testFilter() throws IOException {
		gaborFilter.setWaveLength(7);
		BufferedImage src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH + IMAGENAME)));
		BufferedImage result = (BufferedImage) gaborFilter.filter(src, null);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GABOR.to(), "bmp");
	}
}
