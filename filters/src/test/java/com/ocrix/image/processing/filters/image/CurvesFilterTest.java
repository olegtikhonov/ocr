package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.CURVES;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link CurlFilter} functionality.
 */
public class CurvesFilterTest {
	private static CurvesFilter curvesFilter = null;
	private static BufferedImage src = null;

	
	@BeforeClass
	public static void setUp() throws Exception {
		curvesFilter = new CurvesFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(curvesFilter);
	}
	
	@Test
	public void testCurvesFilter(){
		Curve[] curves = new Curve[1];
		curves[0] = new Curve();
		curves[0].addKnot(0.5f, 1.067f);
		curvesFilter.setCurves(curves);
		BufferedImage result = curvesFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CURVES.to(), "png");
	}
	
	@Test
	public void testSetCurve() {
		Curve curve = new Curve();
		curvesFilter.setCurve(curve);
		assertEquals(curve, curvesFilter.getCurves()[0]);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetCurvesIsNull() {
		Curve[] curve = null;
		curvesFilter.setCurves(curve);
		assertEquals(curve, curvesFilter.getCurves()[0]);
	}
	
	@Test
	public void testGetCurves() {
		testSetCurve();
	}
	
	@Test
	public void testToString() {
		assertTrue(curvesFilter.toString().contains(CurvesFilter.class.getName()));
	}
	
	@Test
	public void testInitialize() {
		Curve[] curves = {new Curve(), new Curve(), new Curve()};
		curvesFilter.setCurves(curves);
		curvesFilter.initialize();
		assertTrue(true);
	}
}
