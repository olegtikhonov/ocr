/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.MAXIMUM;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link MaximumFilter} functionality.
 */
public class MaximumFilterTest {

	private static MaximumFilter maximumFilter = null;
	private static BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		maximumFilter = new MaximumFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(maximumFilter);
	}
	
	
	@Test
	public void testMaximumFilter(){
		BufferedImage result = maximumFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MAXIMUM.to(), "bmp");
	}
	
	@Test
	public void testToString() {
		assertTrue(maximumFilter.toString().contains(MaximumFilter.class.getSimpleName()));
	}
}
