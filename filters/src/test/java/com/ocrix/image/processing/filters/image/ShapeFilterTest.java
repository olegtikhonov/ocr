/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SHAPE;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link ShapeFilter} functionality.
 */
public class ShapeFilterTest {
	private ShapeFilter shapeFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		shapeFilter = new ShapeFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(shapeFilter);
	}

	@Test
	public void testShapeFilter() {
		shapeFilter.setFactor(ALPHA);
		shapeFilter.setInvert(true);
		shapeFilter.setMerge(true);
		shapeFilter.setType(ShapeFilter.SMOOTH);
		BufferedImage result = shapeFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHAPE.to(), "png");
	}

	@Test
	public void testSetFactor() {
		float factor = TestUtils.genFloat();
		shapeFilter.setFactor(factor);
		assertEquals(factor, shapeFilter.getFactor(), EPSILON);
	}

	@Test
	public void testSetColormap() {
		Colormap colormap = new GrayscaleColormap();
		shapeFilter.setColormap(colormap);
		assertEquals(colormap, shapeFilter.getColormap());
	}

	@Test
	public void testSetUseAlpha() {
		boolean useAlpha = true;
		shapeFilter.setUseAlpha(useAlpha);
		assertTrue(shapeFilter.getUseAlpha());
	}

	@Test
	public void testSetType() {
		// Case 1: SMOOTH
		int type = ShapeFilter.SMOOTH;
		shapeFilter.setType(type);
		assertEquals(ShapeFilter.SMOOTH, shapeFilter.getType());

		// Case 2: CIRCLE_DOWN
		type = ShapeFilter.CIRCLE_DOWN;
		shapeFilter.setType(type);
		assertEquals(ShapeFilter.CIRCLE_DOWN, shapeFilter.getType());

		// Case 3: CIRCLE_CIRCLE_UP
		type = ShapeFilter.CIRCLE_UP;
		shapeFilter.setType(type);
		assertEquals(ShapeFilter.CIRCLE_UP, shapeFilter.getType());
	}

	@Test
	public void testSetInvert() {
		boolean invert = true;
		shapeFilter.setInvert(invert);
		assertTrue(shapeFilter.getInvert());
	}

	@Test
	public void testSetMerge() {
		boolean merge = true;
		shapeFilter.setMerge(merge);
		assertTrue(shapeFilter.getMerge());
	}

	@Test
	public void testToString() {
		assertTrue(shapeFilter.toString().contains(ShapeFilter.class.getSimpleName()));
	}

	@Test
	public void testFilterPixels() {
		int width = src.getWidth();
		int height = src.getHeight();
		int[] inPixels = src.getRGB(0, 0, width, height, null, 0, width);
		Rectangle transformedSpace = new Rectangle();

		// Case 1: applyMap - CIRCLE_DOWN
		shapeFilter.setType(ShapeFilter.CIRCLE_DOWN);
		int[] result = shapeFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertTrue(result.length > 0);

		// Case 2: applyMap - CIRCLE_UP
		shapeFilter.setType(ShapeFilter.CIRCLE_UP);
		result = shapeFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertTrue(result.length > 0);

		// Case 3: applyMap - SMOOTH
		shapeFilter.setType(ShapeFilter.SMOOTH);
		result = shapeFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertTrue(result.length > 0);
		
		// Case 4: colormap = null
		shapeFilter.setColormap(null);
		shapeFilter.setType(ShapeFilter.SMOOTH);
		result = shapeFilter.filterPixels(width, height, inPixels, transformedSpace);
		assertTrue(result.length > 0);
	}
	
	@Test
	public void testDistanceMap() {
		int[] map = new int[] {-123, -123, -78, 0, 334}; 
		int width = 1;
		int height = 1;
		int result = shapeFilter.distanceMap(map, width, height);
		System.out.println(result);
	}
	
	@Test
	public void testSetValue() {
		int[] map = new int[] { 5, 4, 3, 6, 0, 23, 2};
		int width = 1; 
		int offset = 5;
		
		try {
			Method method = shapeFilter.getClass().getDeclaredMethod("setValue", int[].class, Integer.TYPE, Integer.TYPE);
			method.setAccessible(true);
			method.invoke(shapeFilter, map,width, offset);
			assertTrue(true);
			
			// Case 2: max = 0
			method.invoke(shapeFilter, map,width, 0);
			assertTrue(true);
			
			// Case 3: offset > map.len
			method.invoke(shapeFilter, map,width, 10);
			assertTrue(true);
			
		} catch(Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void test() {
		//(int[] map, int[] pixels, int width, int height, int max) 
		try {
			int[] map = new int[] { 0, 4, 3, 6, 0, 23, 2};
			Method applyMap = shapeFilter.getClass().getDeclaredMethod("applyMap", int[].class, int[].class, Integer.TYPE, Integer.TYPE, Integer.TYPE);
			applyMap.setAccessible(true);
			applyMap.invoke(shapeFilter, map, src.getRGB(0, 0, 2, 2, null, 0, 2), 2, 2, 0);
			assertTrue(true);
			
		} catch(Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
}
