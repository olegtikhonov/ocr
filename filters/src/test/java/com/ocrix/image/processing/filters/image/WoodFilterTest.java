package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class WoodFilterTest {

	private static WoodFilter woodFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		woodFilter = new WoodFilter();
		src =ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(woodFilter);
	}

	@Test
	public void testWoodFilter(){
		woodFilter.setFibres(0.92f);
		woodFilter.setRings(ALPHA);
		BufferedImage result = woodFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "WoodFilter", "gif");
	}
	
	@AfterClass
	public static void tearDown() throws Exception {
	}
}
