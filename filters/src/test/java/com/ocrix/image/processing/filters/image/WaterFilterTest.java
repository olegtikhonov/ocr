package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class WaterFilterTest {

	private static WaterFilter waterFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		waterFilter = new WaterFilter();
		src =ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(waterFilter);
	}
	
	@Test
	public void testWatherFilter(){
		waterFilter.setWavelength(2.0f);
		waterFilter.setAmplitude(1.5f);
		waterFilter.setRadius(100.0f);
		BufferedImage result = waterFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "WaterFilter", "jpeg");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
