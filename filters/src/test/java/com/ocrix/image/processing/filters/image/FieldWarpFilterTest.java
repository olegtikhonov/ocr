/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.FIELDWARP;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TETRA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.image.FieldWarpFilter.Line;


/**
 * {@link FieldWarpFilter} functionality.
 */
public class FieldWarpFilterTest {

	private static FieldWarpFilter fieldWarpFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		fieldWarpFilter = new FieldWarpFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(fieldWarpFilter);
	}

	@Test
	public void testFieldWarpFilter() throws IOException {
		Line[] inLines = TestUtils.genLines(20, src.getHeight());
		Line[] outLines = TestUtils.genLines(20, src.getWidth());
		
		fieldWarpFilter.setInLines(inLines);
		fieldWarpFilter.setOutLines(outLines);
		fieldWarpFilter.setInterpolation(0);
		BufferedImage result = fieldWarpFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FIELDWARP.to(), "png");
	}
	
	@Test
	public void testInnerLine(){
		int x1 = TestUtils.genInteger(); 
		int y1 = TestUtils.genInteger(); 
		int x2 = TestUtils.genInteger(); 
		int y2 = TestUtils.genInteger();
		Line line = new Line(x1, y1, x2, y2);
		System.out.println(line.toString());
		assertTrue(line.toString().contains(Line.class.getName()));
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		fieldWarpFilter.setAmount(amount);
		assertEquals(amount, fieldWarpFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testSetPower() {
		float power = TestUtils.genFloat();
		fieldWarpFilter.setPower(power);
		assertEquals(power, fieldWarpFilter.getPower(), EPSILON);
	}
	
	@Test
	public void testSetStrength() {
		float strength = TestUtils.genFloat();
		fieldWarpFilter.setStrength(strength);
		assertEquals(strength, fieldWarpFilter.getStrength(), EPSILON);
	}
	
	@Test
	public void testSetInLines() {
		Line[] inLines = TestUtils.genLines(1, src.getHeight());
		fieldWarpFilter.setInLines(inLines);
		assertArrayEquals(inLines, fieldWarpFilter.getInLines());
	}
	
	@Test
	public void testStOutLines() {
		Line[] outLines = TestUtils.genLines(1, src.getHeight());
		fieldWarpFilter.setOutLines(outLines);
		assertArrayEquals(outLines, fieldWarpFilter.getOutLines());
	}
	
	@Test
	public void testTransformInverse() {
		int x = TestUtils.genInteger(); 
		int y = TestUtils.genInteger(); 
		float[] out = TestUtils.generateFloatArray(TETRA);
		Line[] inLines = TestUtils.genLines(1, src.getHeight());
		Line[] outLines = TestUtils.genLines(1, src.getHeight());
		fieldWarpFilter.setInLines(inLines);
		fieldWarpFilter.setOutLines(outLines);
		fieldWarpFilter.transformInverse(x, y, out);
		assertTrue(true);
	}

//	@Test
	public void testGetIntermediateLines() {
//		Line[] intermediateLines = TestUtils.genLines(1, src.getHeight());
//		fieldWarpFilter.setIntermediateLines(intermediateLines);
//		assertArrayEquals(intermediateLines, fieldWarpFilter.getIntermediateLines());
	}
	
	@Test
	public void testToString() {
		assertTrue(fieldWarpFilter.toString().contains(FieldWarpFilter.class.getName()));
	}
}
