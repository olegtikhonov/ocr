package com.ocrix.image.processing.moment;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.moment.Moments;


public class MomentsTest {

	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH + OLD_TEXT)));
	}
	
	@Test
	public void testMoment(){
		assertTrue(Moments.moment(src, 1, 1) > 0);
		assertTrue(Moments.centralMoment(src, 0, 0) > 0);
		assertTrue(Moments.normalCentralMoment(src, 2, 2) > 0);
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
