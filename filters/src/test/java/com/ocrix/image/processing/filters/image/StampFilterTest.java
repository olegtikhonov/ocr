package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


public class StampFilterTest {
	private static StampFilter stampFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		stampFilter = new StampFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(stampFilter);
	}
	
	@Test
	public void testStampFilter(){
		stampFilter.setThreshold(0.75f);
		stampFilter.setSoftness(0.55f);
		BufferedImage result = stampFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "StampFilter", "bmp");
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat(TestParams.DIME);
		stampFilter.setRadius(radius);
		assertEquals(radius, stampFilter.getRadius(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetThreshold() {
		float threshold = TestUtils.genFloat(TestParams.DIME);
		stampFilter.setThreshold(threshold);
		assertEquals(threshold, stampFilter.getThreshold(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetSoftness() {
		float softness = TestUtils.genFloat();
		stampFilter.setSoftness(softness);
		assertEquals(softness, stampFilter.getSoftness(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetWhite() {
		int white = Color.WHITE.getRGB();
		stampFilter.setWhite(white);
		assertEquals(white, stampFilter.getWhite());
	}
	
	@Test
	public void testSetBlack() {
		int black = Color.BLACK.getRGB();
		stampFilter.setBlack(black);
		assertEquals(black, stampFilter.getBlack());
	}
	
	@Test
	public void testToString() {
		assertTrue(stampFilter.toString().contains(StampFilter.class.getSimpleName()));
	}

}
