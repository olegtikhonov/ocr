/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link ImageFunction2D} functionality.
 */
public class TestImageFunction2D {
	private BufferedImage src = null;
	private ImageFunction2D if2d;
	private static final Logger log = Logger.getLogger(TestImageFunction2D.class);
	
	@Before
	public void setUp() throws Exception {
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
		if2d = new ImageFunction2D(src);
	}

	@Test
	public void testImageFunction2DBufferedImage() {
		assertNotNull(if2d);
		log.info(if2d);
	}

	@Test
	public void testImageFunction2DBufferedImageBoolean() {
		if2d = new ImageFunction2D(src, true);
		assertNotNull(if2d);
	}

	@Test
	public void testImageFunction2DBufferedImageIntBoolean() {
		if2d = new ImageFunction2D(src, TestUtils.genInteger() ,true);
		assertNotNull(if2d);
	}

	@Test
	public void testImageFunction2DIntArrayIntIntIntBoolean() {
		int[] pixels = TestUtils.fillArray(src.getHeight() * src.getWidth());
		if2d = new ImageFunction2D(pixels, src.getWidth(), src.getHeight(), 2, false);
		assertNotNull(if2d);
	}

	@Test
	public void testImageFunction2DImage() {
		if2d = new ImageFunction2D(new ImageIcon((new File(PREFIXPATH + DEST_IMAGENAME)).getAbsolutePath()).getImage());
		assertNotNull(if2d);
	}

	@Test
	public void testImageFunction2DImageIntBoolean() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetRGB() {
//		fail("Not yet implemented");
	}

	@Test
	public void testInit() {
//		fail("Not yet implemented");
	}

	@Test
	public void testEvaluate() {
//		fail("Not yet implemented");
	}

	@Test
	public void testSetEdgeAction() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetEdgeAction() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetWidth() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetHeight() {
//		fail("Not yet implemented");
	}

	@Test
	public void testGetPixels() {
//		fail("Not yet implemented");
	}

}
