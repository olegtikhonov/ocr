/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.SHADOW;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ShadowFilter} functionality.
 */
public class ShadowFilterTest {
	private ShadowFilter shadowFilter = null;
	private BufferedImage src = null;

	
	@Before
	public void setUp() throws Exception {
		shadowFilter = new ShadowFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor() {
		assertNotNull(shadowFilter);
	}
	
	@Test
	public void testShadowFilter() throws IOException{
		shadowFilter.setAddMargins(true);
		shadowFilter.setShadowColor(Color.RED.getRGB());
		shadowFilter.setDistance(3);
		shadowFilter.setOpacity(0.0f);
		shadowFilter.setShadowOnly(false);
		BufferedImage result = shadowFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), ImageIO.read(new File(PREFIXPATH + COW)));//ImageIO.read(new File(PREFIXPATH + COW)),
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHADOW.to(), "jpeg");
	}
	
	@Test
	public void testFilterDstNullAddMarginsTrue() {
		shadowFilter.setAddMargins(true);
		BufferedImage result = shadowFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testParamCitor() {
		float radius = TestUtils.genFloat();
		float xOffset = TestUtils.genFloat();
		float yOffset = TestUtils.genFloat(); 
		float opacity= TestUtils.genFloat();
		ShadowFilter filter = new ShadowFilter(radius,xOffset, yOffset, opacity);
		BufferedImage result = filter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		shadowFilter.setAngle(angle);
		assertEquals(angle, shadowFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		shadowFilter.setDistance(distance);
		assertEquals(distance, shadowFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRadius() {
		float radius = TestUtils.genFloat();
		shadowFilter.setRadius(radius);
		assertEquals(radius, shadowFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testSetOpacity() {
		float opacity = TestUtils.genFloat();
		shadowFilter.setOpacity(opacity);
		assertEquals(opacity, shadowFilter.getOpacity(), EPSILON);
	}
	
	@Test
	public void testsetShadowColor() {
		int shadowColor = Color.GREEN.getRGB();
		shadowFilter.setShadowColor(shadowColor);
		assertEquals(shadowColor, shadowFilter.getShadowColor());
	}
	
	@Test
	public void testSetAddMargins() {
		boolean addMargins = true;
		shadowFilter.setAddMargins(addMargins);
		assertEquals(addMargins, shadowFilter.getAddMargins());
	}
	
	@Test
	public void testSetShadowOnly() {
		boolean shadowOnly = true;
		shadowFilter.setShadowOnly(shadowOnly);
		assertEquals(shadowOnly, shadowFilter.getShadowOnly());
	}
	
	@Test
	public void testGetPoint2D() {
		// Case 1: dest = null
		Point2D result = shadowFilter.getPoint2D(new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat()), null);
		assertNotNull(result);
		
		// Case 2: dest != null
		shadowFilter.setAddMargins(true);
		result = shadowFilter.getPoint2D(new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat()), 
				                         new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat()));
		assertNotNull(result);
		
		// Case 3: addMargins = false;
		shadowFilter.setAddMargins(false);
		result = shadowFilter.getPoint2D(new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat()), 
                                         new Point2D.Float(TestUtils.genFloat(), TestUtils.genFloat()));
		assertNotNull(result);
	}
	
	
	@Test
	public void testToString() {
		assertTrue(shadowFilter.toString().contains(ShadowFilter.class.getSimpleName()));
	}
	
	
	@Test
	public void testGetBounds2D() {
		Rectangle2D result = shadowFilter.getBounds2D(src);
		assertNotNull(result);
		
		// Case 2: addMargins true
		shadowFilter.setAddMargins(true);
		result = shadowFilter.getBounds2D(src);
		assertNotNull(result);
 	}
}
