/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.ROTATE;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link RotateFilter} functionality.
 */
public class RotateFilterTest {

	private RotateFilter rotateFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		rotateFilter = new RotateFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(rotateFilter);
	}
	
	@Test
	public void testRotateFilter(){
		rotateFilter.setAngle((float) (Math.cos(30)));
		BufferedImage result = rotateFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + ROTATE.to(), "png");
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		rotateFilter.setAngle(angle);
		assertEquals(angle, rotateFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(rotateFilter.toString().contains(RotateFilter.class.getSimpleName()));
	}
}
