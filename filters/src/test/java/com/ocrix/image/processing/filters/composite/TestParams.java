/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

public class TestParams {
	private TestParams() {
	}

	public static final float ALPHA = 0.5f;
	public static final float BETA = 1.0f;
	public static final float EPSILON = 0.001f;
	public static final String PREFIXPATH = "src/test/resources/images/";
	public static final String IMAGENAME = "decision.jpg";
	public static final String AUTHOR = "autor.jpg";
	public static final String DEST_IMAGENAME = "Mickey-Mouse-Club.jpg";
	public static final String JUPITER = "spnoise.gif";
	public static final String WIPE_FILTER_MASK = "WipeFilter6.jpg";
	public static final String CLOUDS = "clouds.gif";
	public static final String COW = "cow.jpeg";
	public static final String TUX = "think-linux-gallery-pc-1280x960.jpg";
	public static final String CONRTST = "ContrastFilter.png";
	public static final String TO_BE_SAVED = "../filters/target/";
	public static final String OLD_TEXT = "old_text_texture_2_by_cazcastalla-d338wfv.jpg";
	public static final String SALT_AND_PEPPER = "Noise_salt_and_pepper.png";
	public static final String GRAY_MASK = "HalftoneFilterMask.jpg";
	public static final String TIFF = "tiff";
	public static final String TEXT = "printed_text.jpeg";
	public static final String VP_COUNTOR = "ContourCompositeContext";
	public static final String VP_DARKEN = "DarkenComposite";
	public static final int FRENCH_DEPARTMENT_VAR = 83;
	public static final int QUAD = 255;
	public static final int RONNA = 256;
	public static final int ANGEL_NUMBER = 257;
	public static final int BINARY_STAR = 2;
	public static final int UNITY = 1;
	public static final int HEPTAGON = 7;
	public static final int TETRA = 4;
	public static final int OCTAHEDRON = 8;
	public static final int NONAGON = 9;
	public static float HRADIUS = 5.3f;
	public static float VRADIUS = 12.5678f;
	public static final int FIBONACCI_PRIME = 514229;
	public static final int CDLVI = 456;
	public static final int NUMBER_OF_KNUTS_IN_ONE_SICKLE = 29;
	public static final int DIME = 10;
	public static final int HENDECAGON = 11;
}
