/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.MARBLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link MarbleFilter} functionality.
 */
public class MarbleFilterTest {

	private MarbleFilter marbleFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		marbleFilter = new MarbleFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(marbleFilter);
	}
	
	@Test
	public void testMarbleFilter(){
		marbleFilter.setAmount(ALPHA);
		marbleFilter.setXScale(3);
		marbleFilter.setYScale(10);
		BufferedImage result = marbleFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + MARBLE.to(), "jpeg");
	}
	
	@Test
	public void testSetXScale() {
		float xScale = TestUtils.genFloat();
		marbleFilter.setXScale(xScale);
		assertEquals(xScale, marbleFilter.getXScale(), EPSILON);
	}
	
	@Test
	public void testSetYScale() {
		float yScale = TestUtils.genFloat();
		marbleFilter.setYScale(yScale);
		assertEquals(yScale, marbleFilter.getYScale(), EPSILON);
	}
	
	@Test
	public void testAmount() {
		float amount = TestUtils.genFloat();
		marbleFilter.setAmount(amount);
		assertEquals(amount, marbleFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloat();
		marbleFilter.setTurbulence(turbulence);
		assertEquals(turbulence, marbleFilter.getTurbulence(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(marbleFilter.toString().contains(MarbleFilter.class.getName()));
	}
}
