/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.IMAGE_COMBINING;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.ImageProducer;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link ImageCombiningFilter} functionality.
 */
public class ImageCombiningFilterTest {
	/* Test members */
	private static ImageCombiningFilter imageCombiningFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dest = null;
	
	
	@Before
	public void setUp() throws Exception {
		imageCombiningFilter = new ImageCombiningFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dest = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(imageCombiningFilter);
	}
	
	
	@Test
	public void testImageCombiningFilter(){
		ImageProducer result = imageCombiningFilter.filter(CommonUtils.toImage(src), 
				                                           CommonUtils.toImage(dest),
				                                           src.getMinX(), src.getMinY(), 
				                                           src.getWidth(), src.getHeight());
		assertNotNull(result);
		
		CommonUtils.saveBufferedImage(CommonUtils.toBufferedImage(Toolkit.getDefaultToolkit().createImage(result)), 
				                                                  TO_BE_SAVED + IMAGE_COMBINING.to(), "jpeg");
	}
}
