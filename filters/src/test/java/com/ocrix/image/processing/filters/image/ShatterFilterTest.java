/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SHATTER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link ShatterFilter} functionality.
 */
public class ShatterFilterTest {

	private ShatterFilter shatterFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		shatterFilter = new ShatterFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(shatterFilter);
	}
	
	@Test
	public void testShatterFilter() throws IOException{
		shatterFilter.setTile(50);
		shatterFilter.setTransition(0.4f);
		shatterFilter.setIterations(7);
		shatterFilter.setStartAlpha(0.0f);
		BufferedImage result = shatterFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SHATTER.to(), "png");
	}
	
	@Test
	public void testShatterFilterDstIsNull() throws IOException{
		BufferedImage result = shatterFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testSetTransition() {
		float transition = TestUtils.genFloat();
		shatterFilter.setTransition(transition);
		assertEquals(transition, shatterFilter.getTransition(), EPSILON);
	}
	
	@Test
	public void testSetDistance() {
		float distance = TestUtils.genFloat();
		shatterFilter.setDistance(distance);
		assertEquals(distance, shatterFilter.getDistance(), EPSILON);
	}
	
	@Test
	public void testSetRotation() {
		float rotation = TestUtils.genFloat();
		shatterFilter.setRotation(rotation);
		assertEquals(rotation, shatterFilter.getRotation(), EPSILON);
	}
	
	@Test
	public void testSetZoom() {
		float zoom = TestUtils.genFloat();
		shatterFilter.setZoom(zoom);
		assertEquals(zoom, shatterFilter.getZoom(), EPSILON);
	}
	
	@Test
	public void testSetStartAlpha() {
		float startAlpha = TestUtils.genFloat();
		shatterFilter.setStartAlpha(startAlpha);
		assertEquals(startAlpha, shatterFilter.getStartAlpha(), EPSILON);
	}
	
	@Test
	public void testSetEndAlpha() {
		float endAlpha = TestUtils.genFloat();
		shatterFilter.setEndAlpha(endAlpha);
		assertEquals(endAlpha, shatterFilter.getEndAlpha(), EPSILON);
	}
	
	@Test
	public void testSetCentreX() {
		float centreX = TestUtils.genFloat();
		shatterFilter.setCentreX(centreX);
		assertEquals(centreX, shatterFilter.getCentreX(), EPSILON);
	}
	
	@Test
	public void testSetCentreY() {
		float centreY = TestUtils.genFloat();
		shatterFilter.setCentreY(centreY);
		assertEquals(centreY, shatterFilter.getCentreY(), EPSILON);
	}
	
	@Test
	public void testSetCentre() {
		Point2D	centre = new Point2D.Float();	
		shatterFilter.setCentre(centre);
		assertEquals(centre, shatterFilter.getCentre());
	}
	
	@Test
	public void testSetIterations() {
		int iterations = TestUtils.genInteger();
		shatterFilter.setIterations(iterations);
		assertEquals(iterations, shatterFilter.getIterations());
	}
	
	@Test
	public void testSetTile() {
		int tile = TestUtils.genInteger();
		shatterFilter.setTile(tile);
		assertEquals(tile, shatterFilter.getTile());
	}
	
	@Test
	public void testToString() {
		assertTrue(shatterFilter.toString().contains(ShatterFilter.class.getSimpleName()));
	}
}
