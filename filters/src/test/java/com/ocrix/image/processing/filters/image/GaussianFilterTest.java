/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.GAUSSIAN;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GaussianFilter} functionality.
 */
public class GaussianFilterTest {

	private static GaussianFilter gaussianFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		gaussianFilter = new GaussianFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(gaussianFilter);
	}
	
	@Test
	public void testGaussianFilter(){
		gaussianFilter.setRadius(5.0f);
		BufferedImage result = gaussianFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GAUSSIAN.to(), "png");
	}
	
	@Test
	public void testGaussianFilterDistNotNull() throws IOException{
		gaussianFilter.setRadius(5.0f);
		BufferedImage result = gaussianFilter.filter(ImageIO.read(new File(PREFIXPATH + CLOUDS)), src);
		assertNotNull(result);
	}
	
	
	@Test
	public void testGetRadius() {
		float radius = TestUtils.genFloat();
		gaussianFilter.setRadius(radius);
		assertEquals(radius, gaussianFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(gaussianFilter.toString().contains(GaussianFilter.class.getName()));
	}
}
