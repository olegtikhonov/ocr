/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CONTOUR;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link ContourFilter} functionality.
 */
public class ContourFilterTest {

	private static ContourFilter contourFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		contourFilter = new ContourFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(contourFilter);
	}

	@Test
	public void testFilter() {
		BufferedImage result = contourFilter.filter(src, null);
		assertNotNull(result);
		
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CONTOUR.to(), "bmp");
	}

	@Test
	public void testFilterUsingSetters() throws IOException {
		int contour = TestUtils.genInteger(RONNA);
		contourFilter.setContourColor(contour);
		contourFilter.setScale(ALPHA);
		BufferedImage result = contourFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), null);
		assertNotNull(result);
		
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CONTOUR.to(), "tiff");
	}

	@Test
	public void testSetOffset() {
		float offset = TestUtils.genFloat(RONNA);
		contourFilter.setOffset(offset);
		assertEquals(offset, contourFilter.getOffset(), EPSILON);
	}
	
	@Test
	public void testGetOffset() {
		testSetOffset();
	}
	
	@Test
	public void testSetLevels() {
		float levels = TestUtils.genFloat(RONNA);
		contourFilter.setLevels(levels);
		assertEquals(levels, contourFilter.getLevels(), EPSILON);
	}
	
	@Test
	public void testGetLevels() {
		testSetLevels();
	}
	
	@Test
	public void testSetScale() {
		float scale = TestUtils.genFloat();
		contourFilter.setScale(scale);
		assertEquals(scale, contourFilter.getScale(), EPSILON);
	}
	
	@Test
	public void testGetScale() {
		testSetScale();
	}
	
	
	
	@Test
	public void testSetContourColor() {
		int contourColor = TestUtils.genInteger(RONNA); 
		contourFilter.setContourColor(contourColor);
		assertEquals(contourColor, contourFilter.getContourColor());
	}
	
	@Test
	public void testGetContourColor() {
		testSetContourColor();
	}
	
	@Test
	public void testToString() {
		assertTrue(contourFilter.toString().contains(ContourFilter.class.getName()));
	}
}
