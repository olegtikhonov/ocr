/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.Operations.ADD;
import static com.ocrix.image.processing.filters.Operations.AVERAGE;
import static com.ocrix.image.processing.filters.Operations.BURN;
import static com.ocrix.image.processing.filters.Operations.COLOR;
import static com.ocrix.image.processing.filters.Operations.COLOR_BURN;
import static com.ocrix.image.processing.filters.Operations.COLOR_DODGE;
import static com.ocrix.image.processing.filters.Operations.DARKEN;
import static com.ocrix.image.processing.filters.Operations.DIFFERENCE;
import static com.ocrix.image.processing.filters.Operations.DODGE;
import static com.ocrix.image.processing.filters.Operations.EXCLUSION;
import static com.ocrix.image.processing.filters.Operations.HARD_LIGHT;
import static com.ocrix.image.processing.filters.Operations.HUE;
import static com.ocrix.image.processing.filters.Operations.LIGHTEN;
import static com.ocrix.image.processing.filters.Operations.MULTIPLY;
import static com.ocrix.image.processing.filters.Operations.NEGATION;
import static com.ocrix.image.processing.filters.Operations.OVERLAY;
import static com.ocrix.image.processing.filters.Operations.PIN_LIGHT;
import static com.ocrix.image.processing.filters.Operations.SATURATION;
import static com.ocrix.image.processing.filters.Operations.SCREEN;
import static com.ocrix.image.processing.filters.Operations.SOFT_LIGHT;
import static com.ocrix.image.processing.filters.Operations.SUBTRACT;
import static com.ocrix.image.processing.filters.Operations.VALUE;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Point;
import java.awt.color.ColorSpace;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.MockColorSpace;
import com.ocrix.image.processing.filters.composite.MiscCompositeContext;

/**
 * Tests a {@link MiscCompositeContext} functionality.
 */
public class MiscCompositeContextTest {

	private static MiscCompositeContext miscCompositeContext = null;
	private static ColorModel srcColorModel = null;
	private static ColorModel dstColorModel = null;
	private static int bitMasks[] = new int[] { (byte) 0xf };
	private static BufferedImage bi = null;
	private static BufferedImage bi_2 = null;
	private static SampleModel sm = null;
	private static byte[] pixels = null;
	private static DataBuffer dbuf = null;
	private static Raster src = null;
	private static Raster dst = null;
	private static WritableRaster dstOut = null;

	@BeforeClass
	public static void setUp() throws Exception {
		srcColorModel = CommonUtils.getColorModel(ImageIO.read(new File(
				PREFIXPATH + IMAGENAME)));
		dstColorModel = CommonUtils.getColorModel(ImageIO.read(new File(
				PREFIXPATH + DEST_IMAGENAME)));
		miscCompositeContext = new MiscCompositeContext(EXCLUSION.ordinal(),
				ALPHA, srcColorModel, srcColorModel);
		bi = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		bi_2 = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
		sm = new SinglePixelPackedSampleModel(DataBuffer.TYPE_BYTE,
				bi.getWidth(), bi.getHeight(), bitMasks);
		pixels = CommonUtils.generatePixels(bi.getWidth(), bi.getHeight(),
				new Rectangle2D.Float(-2.0f, -1.2f, 3.2f, 2.4f));
		dbuf = new DataBufferByte(pixels, bi.getWidth() * bi.getHeight(), 0);
		src = Raster.createRaster(sm, dbuf, new Point(0, 0));
		dbuf = new DataBufferByte(pixels, bi_2.getWidth() * bi_2.getHeight(), 0);
		dst = Raster.createRaster(sm, dbuf, new Point(0, 0));
		dstOut = Raster.createWritableRaster(sm, new Point(0, 0));
	}

	@Test
	public void testCitor() {
		assertNotNull(miscCompositeContext);
	}

	@Test
	public void testComposeExclusion() throws IOException {
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeAdd() throws IOException {
		miscCompositeContext = new MiscCompositeContext(ADD.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeSubtract() throws IOException {
		miscCompositeContext = new MiscCompositeContext(SUBTRACT.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeDifference() throws IOException {
		miscCompositeContext = new MiscCompositeContext(DIFFERENCE.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeMultiply() throws IOException {
		miscCompositeContext = new MiscCompositeContext(MULTIPLY.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeScreen() throws IOException {
		miscCompositeContext = new MiscCompositeContext(SCREEN.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeOverlay() throws IOException {
		miscCompositeContext = new MiscCompositeContext(OVERLAY.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeDarken() throws IOException {
		miscCompositeContext = new MiscCompositeContext(DARKEN.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeLighten() throws IOException {
		miscCompositeContext = new MiscCompositeContext(LIGHTEN.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeAverage() throws IOException {
		miscCompositeContext = new MiscCompositeContext(AVERAGE.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeHue() throws IOException {
		miscCompositeContext = new MiscCompositeContext(HUE.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeSaturation() throws IOException {
		miscCompositeContext = new MiscCompositeContext(SATURATION.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeValue() throws IOException {
		miscCompositeContext = new MiscCompositeContext(VALUE.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeColor() throws IOException {
		miscCompositeContext = new MiscCompositeContext(COLOR.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeBurn() throws IOException {
		miscCompositeContext = new MiscCompositeContext(BURN.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeColorBurn() throws IOException {
		miscCompositeContext = new MiscCompositeContext(COLOR_BURN.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeDodge() throws IOException {
		miscCompositeContext = new MiscCompositeContext(DODGE.ordinal(), ALPHA,
				srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeColorDodge() throws IOException {
		miscCompositeContext = new MiscCompositeContext(COLOR_DODGE.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeSoftLight() throws IOException {
		miscCompositeContext = new MiscCompositeContext(SOFT_LIGHT.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeHardLight() throws IOException {
		miscCompositeContext = new MiscCompositeContext(HARD_LIGHT.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testDispose() {
		miscCompositeContext.dispose();
		assertTrue(true);
	}

	@Test
	public void testComposePinLight() throws IOException {
		miscCompositeContext = new MiscCompositeContext(PIN_LIGHT.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testComposeNegation() throws IOException {
		miscCompositeContext = new MiscCompositeContext(NEGATION.ordinal(),
				ALPHA, srcColorModel, dstColorModel);
		assertNotNull(miscCompositeContext);
		miscCompositeContext.compose(src, dst, dstOut);
		assertTrue(true);
	}

	@Test
	public void testGetSrcColorModel() {
		miscCompositeContext.setSrcColorModel(srcColorModel);
		assertEquals(srcColorModel, miscCompositeContext.getSrcColorModel());
	}

	@Test
	public void testGetDstColorModel() {
		miscCompositeContext.setDstColorModel(srcColorModel);
		assertEquals(srcColorModel, miscCompositeContext.getDstColorModel());
	}

	@Test
	public void testGetSrcColorSpace() {
		ColorSpace cmyk = new MockColorSpace(20, 30);
		miscCompositeContext.setSrcColorSpace(cmyk);
		assertEquals(cmyk, miscCompositeContext.getSrcColorSpace());
	}

	// getDstColorSpace()
	@Test
	public void testGetDstColorSpace() {
		ColorSpace cmyk = new MockColorSpace(20, 30);
		miscCompositeContext.setDstColorSpace(cmyk);
		assertEquals(cmyk, miscCompositeContext.getDstColorSpace());
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
