/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.PERSPECTIVE;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link PerspectiveFilter} functionality.
 */
public class PerspectiveFilterTest {

	private PerspectiveFilter perspectiveFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		perspectiveFilter = new PerspectiveFilter();
		perspectiveFilter.setScaled(true);
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(perspectiveFilter);
	}

	@Test
	public void testPerspectiveFilterScaledTrue(){
		int x = src.getHeight();
		int y = src.getWidth();
		perspectiveFilter.setClip(true);
		perspectiveFilter.setScaled(true);
		perspectiveFilter.setCorners(0xA, 0xB, 0xC, 0xD, 0xE, 0xFA, x, y);
		BufferedImage result = perspectiveFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testPerspectiveFilterScaledFalse(){
		int x = src.getHeight();
		int y = src.getWidth();
		perspectiveFilter.setCorners(0xA, 0xB, 0xC, 0xD, 0xE, 0xFA, x, y);
		perspectiveFilter.setScaled(false);
		BufferedImage result = perspectiveFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + PERSPECTIVE.to(), "tiff");
	}
	
	@Test
	public void testClip() {
		// Case 1: clip is true.
		perspectiveFilter.setClip(true);
		assertTrue(perspectiveFilter.getClip());
		
		// Case 2: clip is false
		perspectiveFilter.setClip(false);
		assertFalse(perspectiveFilter.getClip());
	}
	

	@Test
	public void testQuadToUnitSquare() {
		float x0 = TestUtils.genFloat();
		float y0 = TestUtils.genFloat();
		float x1 = TestUtils.genFloat();
		float y1 = TestUtils.genFloat();
		float x2 = TestUtils.genFloat();
		float y2 = TestUtils.genFloat();
		float x3 = TestUtils.genFloat();
		float y3 = TestUtils.genFloat();
		perspectiveFilter.quadToUnitSquare(x0, y0, x1, y1, x2, y2, x3, y3);
		assertTrue(true);
	}
	
	@Test
	public void testGetOriginX() {
		float result = perspectiveFilter.getOriginX();
		assertEquals(result, 0.0f, EPSILON);
	}
	
	@Test
	public void testGetOriginY() {
		float result = perspectiveFilter.getOriginY();
		assertEquals(result, 0.0f, EPSILON);
	}
	
	@Test
	public void testGetBounds2D() {
		// Case 1: clip is true
		perspectiveFilter.setClip(true);
		Rectangle2D result = perspectiveFilter.getBounds2D(src);
		assertNotNull(result);
		
		// Case 2: clip is false
		perspectiveFilter.setClip(false);
		result = perspectiveFilter.getBounds2D(src);
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(perspectiveFilter.toString().contains(PerspectiveFilter.class.getSimpleName()));
	}
	
	@Test
	public void testTransformSpace() {
		Rectangle rect = new Rectangle(src.getWidth(), src.getHeight());
		perspectiveFilter.setClip(false);
		perspectiveFilter.transformSpace(rect);
		assertTrue(true);
	}
	
	@Test
	public void testGetPoint2DNullCase() {
		Point2D srcPt = new Point2D.Float();
		perspectiveFilter.setScaled(false);
		Point2D result = perspectiveFilter.getPoint2D(srcPt, null);
		assertNotNull(result);
	}
}
