/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.SaturationComposite;
import com.ocrix.image.processing.filters.composite.SaturationComposite.Context;

/**
 * Tests a {@link SaturationComposite} functionality.
 */
public class SaturationCompositeTest {

	private static SaturationComposite saturationComposite = null;

	@BeforeClass
	public static void setUp() throws Exception {
		saturationComposite = new SaturationComposite(ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(saturationComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(PREFIXPATH + IMAGENAME)));
		CompositeContext comp = saturationComposite.createContext(
				srcColorModel, srcColorModel, new RenderingHints(
						KEY_ANTIALIASING, VALUE_ANTIALIAS_ON));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.OCTAHEDRON);
		int[] dst = TestUtils.fillArray(TestParams.OCTAHEDRON);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(saturationComposite.toString().toString()
				.contains(SaturationComposite.class.getName()));
	}
}
