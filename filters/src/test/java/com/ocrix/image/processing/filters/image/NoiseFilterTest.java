/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.NOISE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;



/**
 * Tests NoiseFilter.
 */
public class NoiseFilterTest {
	private NoiseFilter noiseFilter = null;
	private BufferedImage src = null;
	
	
	@Before
	public void setUp() throws Exception {
		noiseFilter = new NoiseFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(noiseFilter);
	}
	
	
	@Test
	public void testNoiseFilter() throws IOException{
		noiseFilter.setDensity(ALPHA);
		BufferedImage result = noiseFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + NOISE.to(), "png");
	}
	
	@Test
	public void testSetAmount() {
		int amount = TestUtils.genInteger();
		noiseFilter.setAmount(amount);
		assertEquals(amount, noiseFilter.getAmount());
	}
	
	@Test
	public void testSetDistribution() {
		int distribution = TestUtils.genInteger();
		noiseFilter.setDistribution(distribution);
		assertEquals(distribution, noiseFilter.getDistribution());
	}
	
	@Test 
	public void setMonochrome() {
		boolean monochrome = true;
		// Case 1: true
		noiseFilter.setMonochrome(monochrome);
		assertTrue(noiseFilter.getMonochrome());
		
		// Case 2: false
		monochrome = false;
		noiseFilter.setMonochrome(monochrome);
		assertFalse(noiseFilter.getMonochrome());
	}
	
	@Test
	public void testSetDensity() {
		float density = TestUtils.genFloat();
		noiseFilter.setDensity(density);
		assertEquals(density, noiseFilter.getDensity(), EPSILON);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(src.getWidth()) / 5;
		int y = TestUtils.genInteger(src.getWidth()) / 5;
		// Can rise OutOfBound exception.
		int rgb = src.getRGB(x, y);
		int result = noiseFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testFilterRGBMonoCromeIsTrue() {
		int x = TestUtils.genInteger(src.getWidth()) / 5;
		int y = TestUtils.genInteger(src.getWidth()) / 5;
		noiseFilter.setMonochrome(true);
		// Can rise OutOfBound exception.
		int rgb = src.getRGB(x, y);
		int result = noiseFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	
	@Test
	public void testToString() {
		assertTrue(noiseFilter.toString().contains(NoiseFilter.class.getSimpleName()));
	}
}
