/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.CONTRAST;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link ContrastFilter} functionality.
 */
public class ContrastFilterTest {

	private static ContrastFilter contrastFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		contrastFilter = new ContrastFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(contrastFilter);
	}
	
	@Test
	public void testFilter(){
		float brightness = (float)(Math.random() * 0.1f + ALPHA);
		contrastFilter.setBrightness(brightness);
		assertTrue(contrastFilter.getBrightness() == brightness);
		
		float contrast = (float)(Math.random() * 0.1f + ALPHA);
		contrastFilter.setContrast(contrast);
		assertTrue(contrastFilter.getContrast() == contrast);
		
		BufferedImage result = contrastFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CONTRAST.to(), "tiff");
	}

	@Test
	public void testToString() {
		assertTrue(contrastFilter.toString().contains(ContrastFilter.class.getName()));
	}

}
