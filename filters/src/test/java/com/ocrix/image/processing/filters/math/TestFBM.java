/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link FBM} functionality.
 */
public class TestFBM {

	private FBM fbm;

	@Before
	public void setUp() throws Exception {

		fbm = new FBM(TestUtils.genFloat(), TestUtils.genFloat(),
				TestUtils.genFloat());
	}

	@Test
	public void testFBMFloatFloatFloat() {
		assertNotNull(fbm);
	}

	@Test
	public void testFBMFloatFloatFloatFunction2D() {
		fbm = new FBM(TestUtils.genFloat(), TestUtils.genFloat(),
				TestUtils.genFloat(), new VLNoise());
		assertNotNull(fbm);
	}

	@Test
	public void testSetBasis() {
		Function2D func = new Noise();
		fbm.setBasis(func);
		assertEquals(func, fbm.getBasisType());
	}

	@Test
	public void testGetBasisType() {
		testSetBasis();
	}

	@Test
	public void testEvaluate() {
		float x = TestUtils.genFloat();
		float y = TestUtils.genFloat();
		fbm.octaves = 3;
		float result = fbm.evaluate(x, y);
		assertTrue(result != 0.0f);
	}

	@Test
	public void testToString() {
		assertTrue(fbm.toString().contains(FBM.class.getName()));
	}
}
