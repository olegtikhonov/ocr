/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.SPARKLE;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link SparkleFilter} functionality.
 */
public class SparkleFilterTest {
	private SparkleFilter sparkleFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		sparkleFilter = new SparkleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(sparkleFilter);
	}
	
	
	@Test
	public void testSparkleFilter(){
		BufferedImage result = sparkleFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SPARKLE.to(), "jpeg");
	}
	
	@Test
	public void testSetRandomness() {
		int randomness = TestUtils.genInteger();
		sparkleFilter.setRandomness(randomness);
		assertEquals(randomness, sparkleFilter.getRandomness());
	}
	
	@Test
	public void testSetAmount() {
		int amount = TestUtils.genInteger(1);
		sparkleFilter.setAmount(amount);
		assertEquals(amount, sparkleFilter.getAmount());
	}
	
	@Test
	public void testSetRays() {
		int rays = TestUtils.genInteger();
		sparkleFilter.setRays(rays);
		assertEquals(rays, sparkleFilter.getRays());
	}
	
	@Test
	public void testSetRadius() {
		int radius = TestUtils.genInteger();
		sparkleFilter.setRadius(radius);
		assertEquals(radius, sparkleFilter.getRadius());
	}
	
	@Test
	public void testToString() {
		assertTrue(sparkleFilter.toString().equals(SparkleFilter.class.getSimpleName()));
	}
	
	@Test
	public void testSetColor() {
		int color = TestUtils.genInteger();
		sparkleFilter.setColor(color);
		assertEquals(color, sparkleFilter.getColor());
	}
}
