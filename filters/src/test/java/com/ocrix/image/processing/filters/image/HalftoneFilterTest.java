/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.HALFTONE;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.GRAY_MASK;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link HalftoneFilter} functionality.
 */
public class HalftoneFilterTest {

	private HalftoneFilter halftoneFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		halftoneFilter = new HalftoneFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(halftoneFilter);
	}
	
	@Test
	public void testGetSoftness() {
		float softness = TestUtils.genFloat();
		halftoneFilter.setSoftness(softness);
		assertEquals(softness, halftoneFilter.getSoftness(), EPSILON);
	}
	
	@Test
	public void testGetMask() throws IOException{
		BufferedImage mask = ImageIO.read(new File(PREFIXPATH + GRAY_MASK));
		halftoneFilter.setMask(mask);
		assertEquals(mask, halftoneFilter.getMask());
	}
	
	@Test
	public void testGetInvert() {
		boolean invert = true;
		// case: true
		halftoneFilter.setInvert(invert);
		assertTrue(halftoneFilter.getInvert());
		
		// case: false
		invert = false;
		halftoneFilter.setInvert(invert);
		assertFalse(halftoneFilter.getInvert());
	}
	
	@Test
	public void testGetMonochrome() {
		// case: true
		boolean monochrome = true;
		halftoneFilter.setMonochrome(monochrome);
		assertTrue(halftoneFilter.getMonochrome());

		// case: false
		monochrome = false;
		halftoneFilter.setMonochrome(monochrome);
		assertFalse(halftoneFilter.getMonochrome());
	}
	
	@Test
	public void testToString() {
		assertTrue(halftoneFilter.toString().contains(HalftoneFilter.class.getName()));
	}
	
	@Test
	public void testHalftoneFilter() throws IOException{
		halftoneFilter.setSoftness(0.2f);
		halftoneFilter.setMonochrome(true);
		halftoneFilter.setInvert(false);
		//Have to set a mask
		halftoneFilter.setMask(ImageIO.read(new File(PREFIXPATH + GRAY_MASK)));
		BufferedImage result = halftoneFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + HALFTONE.to(), "png");
	}
	
	@Test
	public void testHalftoneFilterCases() throws IOException{
		//case: dst != null
		BufferedImage result = halftoneFilter.filter(src, src);
		assertNotNull(result);
		
		//case: mask == null
		halftoneFilter.setMask(null);
		result = halftoneFilter.filter(src, src);
		assertNotNull(result);
		
		//case: invert = true
		halftoneFilter.setInvert(true);
		halftoneFilter.setMask(ImageIO.read(new File(PREFIXPATH + GRAY_MASK)));
		result = halftoneFilter.filter(src, src);
		assertNotNull(result);
		
		//case: !monochrome
		halftoneFilter.setMonochrome(false);
		halftoneFilter.setMask(ImageIO.read(new File(PREFIXPATH + GRAY_MASK)));
		result = halftoneFilter.filter(src, src);
		assertNotNull(result);
	}
}
