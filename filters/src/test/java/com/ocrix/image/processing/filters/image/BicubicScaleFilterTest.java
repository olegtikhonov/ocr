/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.BICUBIC_SCALE;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;

/**
 * Tests the {@link BicubicScaleFilter} functionality.
 */
public class BicubicScaleFilterTest {

	private static BicubicScaleFilter bicubicScaleFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dest = null;

	@BeforeClass
	public static void setUp() throws Exception {
		bicubicScaleFilter = new BicubicScaleFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dest = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(bicubicScaleFilter);
	}

	@Test
	public void testCitorTwoParams() {
		BicubicScaleFilter bsf = new BicubicScaleFilter(src.getWidth(),
				src.getHeight());
		assertNotNull(bsf);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorTwoNullParams() {
		BicubicScaleFilter bsf = new BicubicScaleFilter((~QUAD), (~QUAD));
		assertNull(bsf);
	}

	@Test
	public void testFiler() throws IOException {
		BicubicScaleFilter bsf = new BicubicScaleFilter(src.getWidth(), src.getHeight());
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO.read(new File(TestParams.PREFIXPATH + IMAGENAME)));
		BufferedImage bi = bsf.filter(src, bicubicScaleFilter.createCompatibleDestImage(dest, srcColorModel));
		assertNotNull(bi);
		CommonUtils.saveBufferedImage(bi, TO_BE_SAVED + BICUBIC_SCALE.to(), "bmp");
	}
	
	@Test
	public void testFilterDstIsNull() {
		BufferedImage bi = bicubicScaleFilter.filter(src, null);
		assertNotNull(bi);
		CommonUtils.saveBufferedImage(bi, TO_BE_SAVED + BICUBIC_SCALE.to(), "png");
	}
	
	@Test
	public void testToString() {
		assertTrue(bicubicScaleFilter.toString().contains(BicubicScaleFilter.class.getName()));
	}
	
	@Test
	public void testGetHeight() {
		int height = TestUtils.genInteger(QUAD);
		bicubicScaleFilter.setHeight(height);
		assertEquals(height, bicubicScaleFilter.getHeight());
	}
	
	@Test
	public void testSetHeight() {
		testGetHeight();
	}
	
	@Test
	public void testGetWidth() {
		int width = TestUtils.genInteger(QUAD);
		bicubicScaleFilter.setWidth(width);
		assertEquals(width, bicubicScaleFilter.getWidth());
	}
	
	@Test
	public void testSetWidth() {
		testGetWidth();
	}
}
