/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OCTAHEDRON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static javax.imageio.ImageIO.read;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.HueComposite;
import com.ocrix.image.processing.filters.composite.HueComposite.Context;

/**
 * Tests an {@link HueComposite} functionality.
 */
public class HueCompositeTest {
	/* Hue composite */
	private static HueComposite hueComposite;

	@Before
	public void setUp() throws Exception {
		hueComposite = new HueComposite(TestParams.ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(hueComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(read(new File(
				PREFIXPATH + IMAGENAME)));
		CompositeContext comp = hueComposite.createContext(srcColorModel,
				srcColorModel, new RenderingHints(
						RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_DEFAULT));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel scm = CommonUtils.getColorModel(read(new File(PREFIXPATH
				+ IMAGENAME)));
		ColorModel dcm = CommonUtils.getColorModel(read(new File(PREFIXPATH
				+ IMAGENAME)));
		Context context = new Context(ALPHA, scm, dcm);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(OCTAHEDRON);
		int[] dst = TestUtils.fillArray(OCTAHEDRON);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);

		assertTrue(context.toString().contains(HueComposite.class.getName()));
	}

	@Test
	public void testComposeRGBElseCase() throws IOException {
		ColorModel scm = CommonUtils.getColorModel(read(new File(PREFIXPATH
				+ IMAGENAME)));
		ColorModel dcm = CommonUtils.getColorModel(read(new File(PREFIXPATH
				+ IMAGENAME)));
		Context context = new Context(ALPHA, scm, dcm);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(2);
		int[] dst = TestUtils.fillArray(1);

		context.composeRGB(src, dst, ALPHA);
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(hueComposite.toString().contains(
				HueComposite.class.getName()));
	}

	@After
	public void tearDown() throws Exception {
	}
}
