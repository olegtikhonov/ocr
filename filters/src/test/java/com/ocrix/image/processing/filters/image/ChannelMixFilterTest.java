/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.CHANNEL_MIX;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link ChannelMixFilter} functionality.
 */
public class ChannelMixFilterTest {

	private static ChannelMixFilter channelMixFilter;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		channelMixFilter = new ChannelMixFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(channelMixFilter);
	}

	@Test
	public void testFilter() {
		channelMixFilter.setIntoB(TestUtils.genInteger(RONNA));
		channelMixFilter.setIntoG(TestUtils.genInteger(RONNA));
		channelMixFilter.setIntoR(TestUtils.genInteger(RONNA));
		channelMixFilter.setBlueGreen(TestUtils.genInteger(RONNA));
		channelMixFilter.setGreenRed(TestUtils.genInteger(RONNA));
		channelMixFilter.setRedBlue(TestUtils.genInteger(RONNA));
		BufferedImage result = channelMixFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + CHANNEL_MIX.to(), "png");
	}

	@Test
	public void testGetBlueGreen() {
		/*
		 * A pixel is represented by a 4-byte (32 bit) integer, like so:
		 * 
		 * 00000000 00000000 00000000 11111111
		 * ^ Alpha  ^Red     ^Green   ^Blue
		 */
		channelMixFilter.setBlueGreen(Color.BLUE.getBlue());
		assertEquals(Color.BLUE.getBlue(), channelMixFilter.getBlueGreen());
	}
	
	@Test
	public void testSetRedBlue() {
		int redBlue = TestUtils.genInteger();
		channelMixFilter.setRedBlue(redBlue);
		assertEquals(redBlue, channelMixFilter.getRedBlue());
	}
	
	@Test
	public void testGetRedBlue() {
		testSetRedBlue();
	}
	
	@Test
	public void testSetGreenRed(){
		int greenRed = TestUtils.genInteger();
		channelMixFilter.setGreenRed(greenRed);
		assertEquals(greenRed, channelMixFilter.getGreenRed());
	}
	
	@Test
	public void testGetGreenRed(){
		testSetGreenRed();
	}
	
	@Test
	public void testSetIntoR(){
		int intoR = TestUtils.genInteger();
		channelMixFilter.setIntoR(intoR);
		assertEquals(intoR, channelMixFilter.getIntoR());
	}
	
	@Test
	public void testGetIntoR(){
		testSetIntoR();
	}
	
	@Test
	public void testSetIntoG() {
		int intoG = TestUtils.genInteger();
		channelMixFilter.setIntoG(intoG);
		assertEquals(intoG, channelMixFilter.getIntoG());
	}
	
	@Test
	public void testGetIntoG() {
		testSetIntoG(); 
	}
	
	@Test
	public void testSetIntoB() {
		int intoB = TestUtils.genInteger();
		channelMixFilter.setIntoB(intoB);
		assertEquals(intoB, channelMixFilter.getIntoB());
	}
	
	@Test
	public void testGetIntoB() {
		testSetIntoB();
	}
	
	@Test
	public void testToString() {
		assertTrue(channelMixFilter.toString().contains(ChannelMixFilter.class.getName()));
	}
}
