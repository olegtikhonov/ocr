/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.RIPPLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link RippleFilter} functionality.
 */
public class RippleFilterTest {
	private RippleFilter rippleFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		rippleFilter = new RippleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(rippleFilter);
	}

	@Test
	public void testRippleFilter() {
		// SINE, SAWTOOTH, TRIANGLE, NOISE
		rippleFilter.setYAmplitude(1.5f);
		rippleFilter.setWaveType(RippleFilter.SAWTOOTH);
		BufferedImage result = rippleFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils
				.saveBufferedImage(result, TO_BE_SAVED + RIPPLE.to(), "jpeg");
	}

	@Test
	public void testSetXAmplitude() {
		float xAmplitude = TestUtils.genFloat();
		rippleFilter.setXAmplitude(xAmplitude);
		assertEquals(xAmplitude, rippleFilter.getXAmplitude(), EPSILON);
	}
	
	@Test
	public void testSetYAmplitude() {
		float yAmplitude = TestUtils.genFloat();
		rippleFilter.setYAmplitude(yAmplitude);
		assertEquals(yAmplitude, rippleFilter.getYAmplitude(), EPSILON);
	}

	@Test
	public void testSetXWavelength() {
		float xWavelength = TestUtils.genFloat();
		rippleFilter.setXWavelength(xWavelength);
		assertEquals(xWavelength, rippleFilter.getXWavelength(), EPSILON);
	}

	@Test
	public void testSetYWavelength() {
		float yWavelength = TestUtils.genFloat();
		rippleFilter.setYWavelength(yWavelength);
		assertEquals(yWavelength, rippleFilter.getYWavelength(), EPSILON);
	}

	@Test
	public void testSetWaveType() {
		// Case 1: RippleFilter.TRIANGLE
		int waveType = RippleFilter.TRIANGLE;
		rippleFilter.setWaveType(waveType);
		assertEquals(RippleFilter.TRIANGLE, rippleFilter.getWaveType());

		// Case 2: RippleFilter.NOISE
		waveType = RippleFilter.NOISE;
		rippleFilter.setWaveType(waveType);
		assertEquals(RippleFilter.NOISE, rippleFilter.getWaveType());

		// Case 3: RippleFilter.SINE
		waveType = RippleFilter.SINE;
		rippleFilter.setWaveType(waveType);
		assertEquals(RippleFilter.SINE, rippleFilter.getWaveType());

		// Case 4: RippleFilter.SAWTOOTH
		waveType = RippleFilter.SAWTOOTH;
		rippleFilter.setWaveType(waveType);
		assertEquals(RippleFilter.SAWTOOTH, rippleFilter.getWaveType());
	}
	
	@Test
	public void testToString() {
		assertTrue(rippleFilter.toString().contains(RippleFilter.class.getSimpleName()));
	}
	
	
	@Test
	public void testTransformSpace() {
		Rectangle r = new Rectangle();
		rippleFilter.setEdgeAction(RippleFilter.ZERO);
		// Case 1: RippleFilter.ZERO
		rippleFilter.transformSpace(r);
	}
	
	@Test
	public void testTransformInverse() {
		int x = TestUtils.genInteger();
		int y = TestUtils.genInteger();
		float[] out = TestUtils.generateFloatArray(5);
		// Case: 1
		rippleFilter.setWaveType(RippleFilter.SINE);
		rippleFilter.transformInverse(x, y, out);
		assertTrue(true);
		
		// Case 2: SAWTOOTH
		rippleFilter.setWaveType(RippleFilter.SAWTOOTH);
		rippleFilter.transformInverse(x, y, out);
		assertTrue(true);
		
		// Case 3: TRIANGLE
		rippleFilter.setWaveType(RippleFilter.TRIANGLE);
		rippleFilter.transformInverse(x, y, out);
		assertTrue(true);
		
		// Case 4: NOISE
		rippleFilter.setWaveType(RippleFilter.NOISE);
		rippleFilter.transformInverse(x, y, out);
		assertTrue(true);
	}
}
