package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


public class UnpremultiplyFilterTest {

	private static UnpremultiplyFilter unpremultiplyFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		unpremultiplyFilter = new UnpremultiplyFilter();
		src =ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(unpremultiplyFilter);
	}
	
	@Test
	public void testUnpremultiplyFilter(){
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();
		BufferedImage dest = gc.createCompatibleImage(src.getWidth(), src.getHeight(), Transparency.TRANSLUCENT);
		BufferedImage result = unpremultiplyFilter.filter(src, dest);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "UnpremultiplyFilter", "png");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
