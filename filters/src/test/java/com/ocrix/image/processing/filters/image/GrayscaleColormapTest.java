/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GrayscaleColormap} functionality.
 */
public class GrayscaleColormapTest {

	private GrayscaleColormap gscm;
	
	@Before
	public void setUp() throws Exception {
		gscm = new GrayscaleColormap();
	}

	@Test
	public void testGrayscaleColormap() {
		assertNotNull(gscm);
	}

	@Test
	public void testGetColor() {
		int color = gscm.getColor(TestUtils.genFloat());
		assertTrue(color != 0);
	}
}
