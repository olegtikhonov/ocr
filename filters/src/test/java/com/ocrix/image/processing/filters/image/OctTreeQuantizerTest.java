/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.lang.reflect.Method;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.image.OctTreeQuantizer.OctTreeNode;


/**
 * Tests {@link OctTreeQuantizer} functionality.
 */
public class OctTreeQuantizerTest {
	private OctTreeQuantizer octTreeQuantizer = null; 
	private BufferedImage src = null;
	private final Logger LOG = Logger.getLogger(OctTreeQuantizerTest.class);
	
	
	@Before
	public void setUp() throws Exception {
		octTreeQuantizer = new OctTreeQuantizer();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testBuildColorTable(){
		int[] colorTable = octTreeQuantizer.buildColorTable();
		assertNotNull(colorTable);
	}
	
	
	@Test
	public void testBuildColorTableNoReturnValues(){
		int width = src.getWidth();// current
		int height = src.getHeight();// current
		int[] orig = new int[width * height];
		int[] table = new int[width * height];
		/* Creates a pixel grabber */
		PixelGrabber grabber = new PixelGrabber(src, 0, 0, width, height, orig, 0, width);
		try {
			grabber.grabPixels();
		} catch (InterruptedException e) {
			LOG.error(e);
			assertTrue(false);
		}

		octTreeQuantizer.buildColorTable(orig, table);
		assertNotNull(table);
	}
	
	@Test
	public void testOctTreeNode() {
		OctTreeNode inner = new OctTreeQuantizer().new OctTreeNode();
		inner.list(System.out, 1);
		assertTrue(true);
	}
	
	@Test
	public void test() {
		try {
			Method reduceTree = octTreeQuantizer.getClass().getDeclaredMethod("reduceTree", Integer.TYPE);
			reduceTree.setAccessible(true);
			reduceTree.invoke(octTreeQuantizer, new Integer(100));
			assertTrue(true);
		} catch(Exception e) {
			LOG.error(e);
			assertTrue(false);
		}
	}
	
}
