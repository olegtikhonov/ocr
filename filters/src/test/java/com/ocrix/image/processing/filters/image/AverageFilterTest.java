/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.AVERAGE;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TIFF;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.image.ConvolveFilter.EDGES_ACTION;

/**
 * Tests the {@link AverageFilter} functionality.
 */
public class AverageFilterTest {

	private static AverageFilter averageFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dest = null;

	@BeforeClass
	public static void setUp() throws Exception {
		averageFilter = new AverageFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		dest = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor() {
		assertNotNull(averageFilter);
	}

	// Just for viewing
	@Test
	public void testSave() {
		averageFilter.setEdgeAction(EDGES_ACTION.ZERO);
		BufferedImage bi = averageFilter.filter(src, dest);
		CommonUtils.saveBufferedImage(bi, TO_BE_SAVED + AVERAGE.to(), TIFF);
		assertNotNull(bi);
	}
	
	@Test
	public void testToString(){
		assertTrue(averageFilter.toString().contains(AverageFilter.class.getName()));
	}
}
