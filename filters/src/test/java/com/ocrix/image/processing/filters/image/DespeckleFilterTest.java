/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.DESPECKLE;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;

/**
 * Tests {@link DespeckleFilter} functionality.
 */
public class DespeckleFilterTest {

	private static DespeckleFilter despeckleFilter = null;
	private static BufferedImage src = null;

	@BeforeClass
	public static void setUp() throws Exception {
		despeckleFilter = new DespeckleFilter();
		src = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
	}

	@Test
	public void testCitor() {
		assertNotNull(despeckleFilter);
	}

	@Test
	public void testDespeckleFilter() {
		BufferedImage result = despeckleFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DESPECKLE.to(), "gif");
	}
	
	@Test
	public void testToString(){
		assertTrue(despeckleFilter.toString().contains(DespeckleFilter.class.getName()));
	}
}
