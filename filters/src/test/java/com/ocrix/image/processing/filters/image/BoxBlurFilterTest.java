/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.BOXBLUR;
import static com.ocrix.image.processing.filters.composite.TestParams.HRADIUS;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.QUAD;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.UNITY;
import static com.ocrix.image.processing.filters.composite.TestParams.VRADIUS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests a {@link BoxBlurFilter} functionality.
 */
public class BoxBlurFilterTest {

	private static BoxBlurFilter boxBlurFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dest = null;

	@BeforeClass
	public static void setUp() throws Exception {
		boxBlurFilter = new BoxBlurFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		dest = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}

	@Test
	public void testCitorNoParams() {
		assertNotNull(boxBlurFilter);
	}

	@Test
	public void testCitorParams() {
		boxBlurFilter = new BoxBlurFilter(HRADIUS, VRADIUS, UNITY);
		assertTrue(boxBlurFilter.getHRadius() == HRADIUS);
		assertTrue(boxBlurFilter.getVRadius() == VRADIUS);
		assertEquals(boxBlurFilter.getIterations(), UNITY);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCitorParamsNeg() {
		boxBlurFilter = new BoxBlurFilter(-HRADIUS, -VRADIUS, ~UNITY);
		assertTrue(false);
	}

	@Test
	public void testSetPremultiplyAlpha() {
		boxBlurFilter.setPremultiplyAlpha(true);
		assertTrue(boxBlurFilter.getPremultiplyAlpha());
	}

	@Test
	public void testSetHRadius() {
		boxBlurFilter.setHRadius(HRADIUS);
		assertTrue(HRADIUS == boxBlurFilter.getHRadius());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegHRadius() {
		boxBlurFilter.setHRadius(-HRADIUS);
		assertTrue(HRADIUS != boxBlurFilter.getHRadius());
	}

	@Test
	public void testSetVRadius() {
		boxBlurFilter.setVRadius(VRADIUS);
		assertTrue(VRADIUS == boxBlurFilter.getVRadius());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegVRadius() {
		boxBlurFilter.setVRadius(-VRADIUS);
		assertTrue(VRADIUS != boxBlurFilter.getVRadius());
	}

	@Test
	public void testSetRadius() {
		boxBlurFilter.setRadius(HRADIUS);
		assertTrue(HRADIUS == boxBlurFilter.getRadius());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegRadius() {
		boxBlurFilter.setRadius(-HRADIUS);
		assertTrue(HRADIUS != boxBlurFilter.getRadius());
	}

	@Test
	public void testSetIterations() {
		boxBlurFilter.setIterations(QUAD);
		assertEquals(QUAD, boxBlurFilter.getIterations());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegIterations() {
		boxBlurFilter.setRadius(~UNITY);
		assertTrue(~UNITY != boxBlurFilter.getRadius());
	}

	@Test
	public void testFilterSecParamNull() {
		boxBlurFilter.setRadius(UNITY);
		BufferedImage result = boxBlurFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + BOXBLUR.to(), "png");
	}

	@Test
	public void testFilterSecParam() {
		boxBlurFilter.setRadius(UNITY);
		BufferedImage temp = CommonUtils.resizeImage(dest, src.getWidth(), src.getHeight(), src.getType());
		BufferedImage result = boxBlurFilter.filter(src, temp);
		assertNotNull(result);
	}
	
	@Test
	public void testFilterPremultiplyAlpha() {
		boxBlurFilter.setPremultiplyAlpha(false);
		BufferedImage result = boxBlurFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(boxBlurFilter.toString().contains(BoxBlurFilter.class.getName()));
	}
}
