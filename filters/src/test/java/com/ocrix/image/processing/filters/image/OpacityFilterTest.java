/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.OPACITY;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link OpacityFilter} functionality.
 */
public class OpacityFilterTest {

	private OpacityFilter opacityFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		opacityFilter = new OpacityFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(opacityFilter);
	}
	
	@Test
	public void testSetOpacity() {
		int opacity = TestUtils.genInteger();
		opacityFilter.setOpacity(opacity);
		assertEquals(opacity, opacityFilter.getOpacity());
	}
	
	
	@Test
	public void testOpacityFilter(){
		BufferedImage result = opacityFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + OPACITY.to(), "jpeg");
	}
	
	@Test
	public void filterRGB() {
		int x = TestUtils.genInteger();
		int y = TestUtils.genInteger();
		int rgb = src.getRGB(x, y);
		int result = opacityFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void filterRgbIsZero() {
		int result = opacityFilter.filterRGB(TestUtils.genInteger(), TestUtils.genInteger(), 0);
		assertTrue(result == 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(opacityFilter.toString().contains(OpacityFilter.class.getSimpleName()));
	}
}
