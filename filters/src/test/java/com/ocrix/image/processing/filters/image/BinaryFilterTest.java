/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static org.junit.Assert.*;
import java.awt.Color;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.math.BlackFunction;


/**
 * Tests {@link BinaryFilter} functionality.
 */
public class BinaryFilterTest {
	// this class is abstract, so choose the any derived class.
	private DilateFilter binaryFilter;

	@Before
	public void setUp() throws Exception {
		binaryFilter = new DilateFilter();
	}

	@Test
	public void testSetIterations() {
		int iterations = TestUtils.genInteger(20);
		binaryFilter.setIterations(iterations);
		assertEquals(iterations, binaryFilter.getIterations());
	}

	@Test
	public void testGetIterations() {
		testSetIterations();
	}

	@Test
	public void testSetColormap() {
		Colormap colorMap = new LinearColormap(Color.GREEN.getRGB(), Color.PINK.getRGB());
		binaryFilter.setColormap(colorMap);
		assertEquals(colorMap, binaryFilter.getColormap());
	}

	@Test
	public void testGetColormap() {
		testSetColormap();
	}

	@Test
	public void testSetNewColor() {
		binaryFilter.setNewColor(Color.GREEN.getRGB());
		assertEquals(Color.GREEN.getRGB(), binaryFilter.getNewColor());
	}

	@Test
	public void testGetNewColor() {
		testSetNewColor();
	}

	@Test
	public void testSetBlackFunction() {
		BlackFunction bf = new BlackFunction();
		binaryFilter.setBlackFunction(bf);
		assertEquals(bf, binaryFilter.getBlackFunction());
	}

	@Test
	public void testGetBlackFunction() {
		testSetBlackFunction();
	}
}
