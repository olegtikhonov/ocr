/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.COW;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests {@link BorderFilter} functionality.
 */
public class BorderFilterTest {

	private BorderFilter borderFilter;
	private int[] coords;

	enum BORDERS {
		LEFT, TOP, RIGHT, BOTTOM;
	};
	
	@Before
	public void setUp() throws Exception {
		borderFilter = new BorderFilter();
		coords = TestUtils.fillArray(TestParams.TETRA);
	}

	@Test
	public void testBorderFilter() {
		assertNotNull(borderFilter);
	}

	@Test
	public void testBorderFilterIntIntIntIntPaint() {
		
		borderFilter = new BorderFilter( coords[BORDERS.LEFT.ordinal()],
				                         coords[BORDERS.TOP.ordinal()],
				                         coords[BORDERS.RIGHT.ordinal()],
				                         coords[BORDERS.BOTTOM.ordinal()], null);
		assertNotNull(borderFilter);
	}

	@Test
	public void testSetLeftBorder() {
		borderFilter.setLeftBorder(coords[BORDERS.LEFT.ordinal()]);
		assertEquals(coords[BORDERS.LEFT.ordinal()], borderFilter.getLeftBorder());
	}

	@Test
	public void testGetLeftBorder() {
		testSetLeftBorder();
	}

	@Test
	public void testSetRightBorder() {
		borderFilter.setRightBorder(coords[BORDERS.RIGHT.ordinal()]);
		assertEquals(coords[BORDERS.RIGHT.ordinal()], borderFilter.getRightBorder());
	}

	@Test
	public void testGetRightBorder() {
		testSetRightBorder();
	}

	@Test
	public void testSetTopBorder() {
		borderFilter.setTopBorder(coords[BORDERS.TOP.ordinal()]);
		assertEquals(coords[BORDERS.TOP.ordinal()], borderFilter.getTopBorder());
	}

	@Test
	public void testGetTopBorder() {
		testSetTopBorder();
	}

	@Test
	public void testSetBottomBorder() {
		borderFilter.setBottomBorder(coords[BORDERS.BOTTOM.ordinal()]);
		assertEquals(coords[BORDERS.BOTTOM.ordinal()], borderFilter.getBottomBorder());
	}

	@Test
	public void testGetBottomBorder() {
		testSetBottomBorder();
	}

	@Test
	public void testSetBorderPaint() {
		Paint borderPaint = new TestPaint();
		borderFilter.setBorderPaint(borderPaint);
		assertEquals(borderPaint, borderFilter.getBorderPaint());
	}

	@Test
	public void testGetBorderPaint() {
		testSetBorderPaint();
	}

	@Test
	public void testFilter() throws IOException {
		BufferedImage result = borderFilter.filter(ImageIO.read(new File(PREFIXPATH + COW)), ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER)));
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "BorderFilter", "gif");
		assertNotNull(result);
	}

	@Test
	public void testFilterDestIsNull() throws IOException {
		BufferedImage bi = ImageIO.read(new File(PREFIXPATH + COW));
		
		borderFilter = new BorderFilter(bi.getMinX() + 10, bi.getMinY() + 10, bi.getHeight() - 10,  bi.getWidth() - 10, null);
		
		
		BufferedImage result = borderFilter.filter(bi, null);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "BorderFilter", "gif");
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(borderFilter.toString().contains(BorderFilter.class.getSimpleName()));
	}
	
	/**
	 * Stab object for testing.
	 */
	class TestPaint implements Paint {

		public int getTransparency() {
			return 0;
		}

		public PaintContext createContext(ColorModel cm, Rectangle deviceBounds, Rectangle2D userBounds, AffineTransform xform, RenderingHints hints) {
			
			return new PaintContext() {
				
				public Raster getRaster(int x, int y, int w, int h) {
					Raster edgeRaster = null;
					try {
						edgeRaster = ImageIO.read(new File(PREFIXPATH + OLD_TEXT)).getData();
					} catch (IOException e) {
					}
					return edgeRaster;
				}
				
				public ColorModel getColorModel() {
					ColorModel cm = null;
					try {
						return CommonUtils.getColorModel(ImageIO.read(new File(PREFIXPATH + OLD_TEXT)));
					} catch (IOException e) {
					}
					
					return cm;
				}
				
				public void dispose() {
					
				}
			};
		}
		
	}

}
