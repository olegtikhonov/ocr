/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SKY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 *
 */
public class SkyFilterTest {

	private SkyFilter skyFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH + OLD_TEXT)));
		skyFilter = new SkyFilter(src);
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(skyFilter);
	}
	
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		skyFilter.setAmount(amount);
		assertEquals(amount, skyFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testSetOperation() {
		int operation = TestUtils.genInteger();
		skyFilter.setOperation(operation);
		assertEquals(operation, skyFilter.getOperation());
	}
	
	@Test
	public void testSetScale() {
		float scale = TestUtils.genFloat();
		skyFilter.setScale(scale);
		assertEquals(scale, skyFilter.getScale(), EPSILON);
	}
	
	@Test
	public void testSetStretch() {
		float stretch = TestUtils.genFloat();
		skyFilter.setStretch(stretch);
		assertEquals(stretch, skyFilter.getStretch(), EPSILON);
	}
	
	@Test
	public void testSetT() {
		float t = TestUtils.genFloat();
		skyFilter.setT(t);
		assertEquals(t, skyFilter.getT(), EPSILON);
	}
	
	@Test
	public void testSetFOV() {
		float fov = TestUtils.genFloat();
		skyFilter.setFOV(fov);
		assertEquals(fov, skyFilter.getFOV(), EPSILON);
	}
	
	@Test 
	public void testSetCloudCover() {
		float cloudCover = TestUtils.genFloat();
		skyFilter.setCloudCover(cloudCover);
		assertEquals(cloudCover, skyFilter.getCloudCover(), EPSILON);
	}
	
	@Test
	public void testSetCloudSharpness() {
		float cloudSharpness = TestUtils.genFloat();
		skyFilter.setCloudSharpness(cloudSharpness);
		assertEquals(cloudSharpness, skyFilter.getCloudSharpness(), EPSILON);
	}
	
	@Test
	public void testSetTime() {
		float time = TestUtils.genFloat();
		skyFilter.setTime(time);
		assertEquals(time, skyFilter.getTime(), EPSILON);
	}
	
	@Test
	public void testSetGlow() {
		float glow = TestUtils.genFloat();
		skyFilter.setGlow(glow);
		assertEquals(glow, skyFilter.getGlow(), EPSILON);
	}
	
	@Test
	public void testSetGlowFalloff() {
		float glowFalloff = TestUtils.genFloat();
		skyFilter.setGlowFalloff(glowFalloff);
		assertEquals(glowFalloff, skyFilter.getGlowFalloff(), EPSILON);
	}
	
	@Test
	public void testSkyFilter(){
		BufferedImage result = skyFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SKY.to(), "gif");
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		skyFilter.setAngle(angle);
		assertEquals(angle, skyFilter.getAngle(), EPSILON);
	}
	
	@Test
	public void testSetOctaves() {
		float octaves = TestUtils.genFloat();
		skyFilter.setOctaves(octaves);
		assertEquals(octaves, skyFilter.getOctaves(), EPSILON);
	}
	
	@Test
	public void testSetH() {
		float h = TestUtils.genFloat();
		skyFilter.setH(h);
		assertEquals(h, skyFilter.getH(), EPSILON);
	}
	
	@Test
	public void testSetLacunarity() {
		float lacunarity = TestUtils.genFloat();
		skyFilter.setLacunarity(lacunarity);
		assertEquals(lacunarity, skyFilter.getLacunarity(), EPSILON);
	}
	
	@Test
	public void testSetGain() {
		float gain = TestUtils.genFloat();
		skyFilter.setGain(gain);
		assertEquals(gain, skyFilter.getGain(), EPSILON);
	}
	
	@Test
	public void testSetBias() {
		float bias = TestUtils.genFloat();
		skyFilter.setBias(bias);
		assertEquals(bias, skyFilter.getBias(), EPSILON);
	}
	
	@Test
	public void testSetHaziness() {
		float haziness = TestUtils.genFloat();
		skyFilter.setHaziness(haziness);
		assertEquals(haziness, skyFilter.getHaziness(), EPSILON);
	}
	
	@Test
	public void testSetSunElevation() {
		float sunElevation = TestUtils.genFloat();
		skyFilter.setSunElevation(sunElevation);
		assertEquals(sunElevation, skyFilter.getSunElevation(), EPSILON);
	}
	
	@Test
	public void testSetSunAzimuth() {
		float sunAzimuth = TestUtils.genFloat();
		skyFilter.setSunAzimuth(sunAzimuth);
		assertEquals(sunAzimuth, skyFilter.getSunAzimuth(), EPSILON);
	}
	
	@Test
	public void testSetSunColor() {
		int sunColor = Color.YELLOW.getRGB();
		skyFilter.setSunColor(sunColor);
		assertEquals(sunColor, skyFilter.getSunColor());
	}
	
	@Test
	public void testSetCameraElevation() {
		float cameraElevation = TestUtils.genFloat();
		skyFilter.setCameraElevation(cameraElevation);
		assertEquals(cameraElevation, skyFilter.getCameraElevation(), EPSILON);
	}
	
	@Test
	public void testSetCameraAzimuth() {
		float cameraAzimuth = TestUtils.genFloat();
		skyFilter.setCameraAzimuth(cameraAzimuth);
		assertEquals(cameraAzimuth, skyFilter.getCameraAzimuth(), EPSILON);
	}
	
	@Test
	public void testSetWindSpeed() {
		float windSpeed = TestUtils.genFloat();
		skyFilter.setWindSpeed(windSpeed);
		assertEquals(windSpeed, skyFilter.getWindSpeed(), EPSILON);
	}
	
	@Test
	public void testEvaluate() {
		float x = TestUtils.genFloat(); 
		float y = TestUtils.genFloat();
		float[] exponents = TestUtils.generateFloatArray((int)skyFilter.getOctaves());
		skyFilter.setExponents(exponents);
 		float result = skyFilter.evaluate(x, y);
		
		assertTrue(result != 0);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(); 
		int y = TestUtils.genInteger(); 
		int rgb = Color.RED.getRGB();
		float[] exponents = TestUtils.generateFloatArray((int)skyFilter.getOctaves());
		skyFilter.setExponents(exponents);
		float[] tan = TestUtils.generateFloatArray(src.getHeight());
		skyFilter.setTan(tan);
		skyFilter.filterRGB(x, y, rgb);
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(skyFilter.toString().contains(SkyFilter.class.getSimpleName()));
	}
}
