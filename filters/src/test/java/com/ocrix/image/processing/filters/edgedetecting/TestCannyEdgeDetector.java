/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.edgedetecting.CannyEdgeDetector;

/**
 * Tests {@link CannyEdgeDetector} functionality.
 */
public class TestCannyEdgeDetector {

	private static CannyEdgeDetector edgeDetector = null;

	@BeforeClass
	public static void setUp() throws Exception {
		edgeDetector = new CannyEdgeDetector();
	}

	@Test
	public void testCannyEdgeDetector() throws IOException {
		// adjust its parameters as desired
		edgeDetector.setLowThreshold(0.5f);
		edgeDetector.setHighThreshold(1f);

		BufferedImage src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));

		edgeDetector.setSourceImage(src);
		edgeDetector.process();
		BufferedImage edges = edgeDetector.getEdgesImage();
		CommonUtils.saveBufferedImage(edges, TO_BE_SAVED
				+ "TestCannyEdgeDetector", "tiff");
	}
}
