/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TETRA;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import static com.ocrix.image.processing.filters.Filters.RESCALE;


/**
 * Tests {@link RescaleFilter} functionality.
 */
public class RescaleFilterTest {

	private RescaleFilter rescaleFilter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		rescaleFilter = new RescaleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testCitor() {
		assertNotNull(rescaleFilter);
	}

	@Test
	public void testRescaleFilter() {
		rescaleFilter.setScale(1.36f);
		BufferedImage result = rescaleFilter.filter(src, src);
		assertNotNull(rescaleFilter);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + RESCALE.to(), "jpeg");
	}
	
	@Test
	public void testRescaleFilterParameterCitor() {
		float scale = TestUtils.genFloat(TETRA);
		RescaleFilter rescaleFilter = new RescaleFilter(scale);
		assertNotNull(rescaleFilter);
	}
	
	@Test
	public void testSetScale() {
		rescaleFilter.setScale(TETRA);
		assertEquals(TETRA, rescaleFilter.getScale(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(rescaleFilter.toString().contains(RescaleFilter.class.getSimpleName()));
	}
}
