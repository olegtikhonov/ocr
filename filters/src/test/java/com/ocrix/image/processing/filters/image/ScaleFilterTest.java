/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.Filters.SCALE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link ScaleFilter} functionality.
 */
public class ScaleFilterTest {
	private static final int HEIGHT = 300;
	private static final int WIDTH = 400;
	private ScaleFilter scaleFilter = null;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		scaleFilter = new ScaleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(scaleFilter);
	}
	
	
	@Test
	public void testScaleFilter(){
		scaleFilter = new ScaleFilter(WIDTH, HEIGHT);
		BufferedImage result = scaleFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SCALE.to(), "jpg");
	}
	
	// Case 2: dst != null
	@Test
	public void testScaleFilterDstNotNull() throws IOException{
		BufferedImage dst = ImageIO.read(new File(PREFIXPATH + CLOUDS)); 
		scaleFilter = new ScaleFilter(WIDTH, HEIGHT);
		BufferedImage result = scaleFilter.filter(src, dst);
		assertNotNull(result);
	}
	
	@Test
	public void testToString() {
		assertTrue(scaleFilter.toString().contains(ScaleFilter.class.getSimpleName()));
	}
}
