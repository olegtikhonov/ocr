/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static com.ocrix.image.processing.filters.Filters.RAYS;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link RaysFilter} functionality.
 */
public class RaysFilterTest {

	private static RaysFilter raysFilter = null;
	private static BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		raysFilter = new RaysFilter();
		src = ImageIO.read(new File(PREFIXPATH + CLOUDS));
	}

	@Test
	public void testCitor() {
		assertNotNull(raysFilter);
	}

	@Test
	public void testRaysFilter() {
		raysFilter.setOpacity(0.5f);
		BufferedImage result = raysFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + RAYS.to(), "png");
	}
	
	@Test
	public void testFilterDstNotNull() {
		raysFilter.setRaysOnly(true);
		raysFilter.setColormap(new LinearColormap());
		BufferedImage result = raysFilter.filter(src, src);
		assertNotNull(result);
	}
	
	@Test
	public void testSetOpacity() {
		float opacity = TestUtils.genFloat();
		raysFilter.setOpacity(opacity);
		assertEquals(opacity, raysFilter.getOpacity(), EPSILON);
	}
	
	@Test
	public void testSetThreshold() {
		float threshold = TestUtils.genFloat();
		raysFilter.setThreshold(threshold);
		assertEquals(threshold, raysFilter.getThreshold(), EPSILON);
	}
	
	@Test
	public void testSetStrength() {
		float strength = TestUtils.genFloat();
		raysFilter.setStrength(strength);
		assertEquals(strength, raysFilter.getStrength(), EPSILON);
	}
	
	@Test
	public void testSetRaysOnly() {
		// Case 1: true
		boolean raysOnly = true;
		raysFilter.setRaysOnly(raysOnly);
		assertEquals(raysOnly, raysFilter.getRaysOnly());
		
		// Case 2: false
		raysOnly = false;
		raysFilter.setRaysOnly(raysOnly);
		assertEquals(raysOnly, raysFilter.getRaysOnly());
	}
	
	@Test
	public void testSetColormap() {
		Colormap colormap = new LinearColormap();
		raysFilter.setColormap(colormap);
		assertEquals(colormap, raysFilter.getColormap());
	}
	
	@Test
	public void testToString() {
		assertTrue(raysFilter.toString().contains(RaysFilter.class.getSimpleName()));
	}
}
