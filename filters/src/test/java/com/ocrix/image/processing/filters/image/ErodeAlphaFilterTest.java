package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.ERODEALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;

/**
 * Tests {@link ErodeAlphaFilter} functionality.
 */
public class ErodeAlphaFilterTest {

	private static ErodeAlphaFilter erodeAlphaFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		erodeAlphaFilter = new ErodeAlphaFilter();
		src = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
		
	}
	
	@Test
	public void testCitor(){
		assertNotNull(erodeAlphaFilter);
	}

	@Test
	public void testErodeAlphaFilter() throws IOException{
		erodeAlphaFilter.setSoftness(ALPHA);
		erodeAlphaFilter.setRadius(100);
		BufferedImage result = erodeAlphaFilter.filter(src, ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME)));
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + ERODEALPHA.to(), "png");
	}
	
	@Test
	public void testGetRadius(){
		float radius = TestUtils.genFloat(100);
		erodeAlphaFilter.setRadius(radius);
		assertEquals(radius, erodeAlphaFilter.getRadius(), EPSILON);
	}
	
	@Test
	public void testSetThreshold() {
		float threshold = TestUtils.genFloat();
		erodeAlphaFilter.setThreshold(threshold);
		assertEquals(threshold, erodeAlphaFilter.getThreshold(), EPSILON);
	}
	
	@Test
	public void testGetSoftness() {
		float softness = TestUtils.genFloat();
		erodeAlphaFilter.setSoftness(softness);
		assertEquals(softness, erodeAlphaFilter.getSoftness(), EPSILON);
	}
	
	@Test
	public void testToString() {
		assertTrue(erodeAlphaFilter.toString().contains(ErodeAlphaFilter.class.getName()));
	}
	
	@Test
	public void testFilterRGBLess255(){
		int VP = 16777215;
		int x = src.getHeight() / 2; 
		int y = src.getWidth() / 2;
		int rgb = src.getRGB(x, y);
		int result = erodeAlphaFilter.filterRGB(x, y, ~rgb);
		assertTrue(result != 0);
//		assertEquals(VP, result);
	}
}
