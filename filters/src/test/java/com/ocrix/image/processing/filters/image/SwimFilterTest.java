package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.SWIM;
import static com.ocrix.image.processing.filters.composite.TestParams.HEPTAGON;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Tests a {@link SwimFilter} functionality.
 */
public class SwimFilterTest {
	private static SwimFilter swimFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		swimFilter = new SwimFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(swimFilter);
	}
	
	@Test
	public void testSwimFilter(){
		swimFilter.setAmount(HEPTAGON);
		BufferedImage result = swimFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SWIM.to(), "gif");
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		swimFilter.setAmount(amount);
		assertEquals(amount, swimFilter.getAmount(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetScale() {
		float scale = TestUtils.genFloat() + 1;
		swimFilter.setScale(scale);
		assertEquals(scale, swimFilter.getScale(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetStretch() {
		float stretch = TestUtils.genFloat() + 1;
		swimFilter.setStretch(stretch);
		assertEquals(stretch, swimFilter.getStretch(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetAngle() {
		float angle = TestUtils.genFloat();
		swimFilter.setAngle(angle);
		assertEquals(angle, swimFilter.getAngle(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetTurbulence() {
		float turbulence = TestUtils.genFloat();
		swimFilter.setTurbulence(turbulence);
		assertEquals(turbulence, swimFilter.getTurbulence(), TestParams.EPSILON);
	}
	
	@Test
	public void testSetTime() {
		// Strange - why time is a float ???
		float time = TestUtils.genFloat(System.currentTimeMillis());
		swimFilter.setTime(time);
		assertEquals(time, swimFilter.getTime(), TestParams.EPSILON);
	}
}
