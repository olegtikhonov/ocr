/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.FOUR_COLOR;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * Tests {@link FourColorFilter} functionality.
 */
public class FourColorFilterTest {

	private static FourColorFilter fourColorFilter = null;
	private static BufferedImage src = null;
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		fourColorFilter = new FourColorFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(fourColorFilter);
	}
	
	
	@Test
	public void testFourColourFilter(){
		BufferedImage result = fourColorFilter.filter(src, null);
		assertNotNull(result);
	}
	
	@Test
	public void testFourColourFilterWithAccessors(){
		/* Changes the default colors */
		fourColorFilter.setColorNE(Color.CYAN.getRGB());
		fourColorFilter.setColorNW(Color.GRAY.getRGB());
		fourColorFilter.setColorSE(Color.GREEN.getRGB());
		fourColorFilter.setColorSW(Color.YELLOW.getRGB());
		
		BufferedImage result = fourColorFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + FOUR_COLOR.to(), "png");
	}
	
	@Test
	public void testGetColorNW() {
		fourColorFilter.setColorNW(Color.RED.getRGB());
		assertEquals(Color.RED.getRGB(), fourColorFilter.getColorNW());
	}
	
	@Test
	public void testGetColorNE() {
		fourColorFilter.setColorNE(Color.RED.getRGB());
		assertEquals(Color.RED.getRGB(), fourColorFilter.getColorNE());
	}
	
	@Test
	public void testGetColorSW() {
		fourColorFilter.setColorSW(Color.RED.getRGB());
		assertEquals(Color.RED.getRGB(), fourColorFilter.getColorSW());
	}
	
	@Test
	public void testGetColorSE() {
		fourColorFilter.setColorSE(Color.RED.getRGB());
		assertEquals(Color.RED.getRGB(), fourColorFilter.getColorSE());
	}
	
	@Test
	public void testFilterRGB() {
		int result = fourColorFilter.filterRGB(0, 0, Color.BLUE.getRGB());
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(fourColorFilter.toString().contains(FourColorFilter.class.getName()));
	}
}
