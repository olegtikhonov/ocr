/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.RONNA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests a {@link ChromaKeyFilter} public functionality. 
 */
public class ChromaKeyFilterTest {
	/* Test' members */
	private static ChromaKeyFilter chromaKeyFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		chromaKeyFilter = new ChromaKeyFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(chromaKeyFilter);
	}
	
	@Test
	public void testFilter(){
		chromaKeyFilter.setColor(TestUtils.genInteger(RONNA));
		chromaKeyFilter.setSTolerance(TestUtils.genFloat(1));
		chromaKeyFilter.setHTolerance(TestUtils.genFloat());
		chromaKeyFilter.setBTolerance(TestUtils.genFloat(1));
		BufferedImage result = chromaKeyFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "ChromaKeyFilter", "png");
	}
	
	@Test
	public void testGetColor() {
		int vb_color = TestUtils.genInteger();
		chromaKeyFilter.setColor(vb_color);
		assertEquals(vb_color, chromaKeyFilter.getColor());
	}
	
	@Test
	public void testToSting(){
		assertTrue(chromaKeyFilter.toString().contains(ChromaKeyFilter.class.getName()));
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}
}
