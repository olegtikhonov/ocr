/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.Filters.SATURATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * 
 * Tests {@link SaturationFilter} functionality.
 *
 */
public class SaturationFilterTest {
	private SaturationFilter saturationFilter = null;
	private BufferedImage src = null;

	
	@Before
	public void setUp() throws Exception {
		saturationFilter = new SaturationFilter();
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(saturationFilter);
	}
	
	@Test
	public void testCitorOneParam() {
		float amount = TestUtils.genFloat();
		SaturationFilter filter = new SaturationFilter(amount);
		assertNotNull(filter);
	}
	
	@Test
	public void testSaturationFilter(){
		saturationFilter.setAmount(0.01f);
		BufferedImage result = saturationFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SATURATION.to(), "png");
	}
	
	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		saturationFilter.setAmount(amount);
		assertEquals(amount, saturationFilter.getAmount(), EPSILON);
	}
	
	@Test
	public void testFilterRGB() {
		int x = TestUtils.genInteger(); 
		int y = TestUtils.genInteger();
		int rgb = Color.GREEN.getRGB();
		int result = saturationFilter.filterRGB(x, y, rgb);
		assertTrue(result != 0);
	}
	
	@Test
	public void testToString() {
		assertTrue(saturationFilter.toString().contains(SaturationFilter.class.getSimpleName()));
	}
}
