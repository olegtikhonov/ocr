/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.GLOW;
import static com.ocrix.image.processing.filters.composite.TestParams.EPSILON;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static com.ocrix.image.processing.filters.composite.TestParams.TUX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link GlowFilter} functionality.
 */
public class GlowFilterTest {

	private GlowFilter glowFilter;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		glowFilter = new GlowFilter();
		src = ImageIO.read(new File(PREFIXPATH + TUX));
	}

	@Test
	public void testFilter() {
		glowFilter.setAmount(0.15f);
		BufferedImage result = glowFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + GLOW.to(), "png");
	}

	@Test
	public void testToString() {
		assertTrue(glowFilter.toString().contains(GlowFilter.class.getName()));
	}

	@Test
	public void testGlowFilter() {
		assertNotNull(glowFilter);
	}

	@Test
	public void testSetAmount() {
		float amount = TestUtils.genFloat();
		glowFilter.setAmount(amount);
		assertEquals(amount, glowFilter.getAmount(), EPSILON);
	}

	@Test
	public void testGetAmount() {
		testSetAmount();
	}
}
