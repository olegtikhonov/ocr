/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.ColorDodgeComposite;
import com.ocrix.image.processing.filters.composite.ColorDodgeComposite.Context;

/**
 * Tests {@link ColorDodgeComposite} functionality.
 */
public class ColorDodgeCompositeTest {
	/* Test's member */
	private static ColorDodgeComposite colorDodgeComposite;

	@Before
	public void setUp() throws Exception {
		colorDodgeComposite = new ColorDodgeComposite(TestParams.ALPHA);
	}

	@Test
	public void testCitor() {
		assertNotNull(colorDodgeComposite);
	}

	@Test
	public void testCreateContext() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		CompositeContext comp = colorDodgeComposite.createContext(
				srcColorModel, srcColorModel, new RenderingHints(
						RenderingHints.KEY_ALPHA_INTERPOLATION,
						RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY));
		assertNotNull(comp);
	}

	@Test
	public void testComposeRGB() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.HEPTAGON);
		int[] dst = TestUtils.fillArray(TestParams.HEPTAGON);

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
	}

	@Test
	public void testComposeRGBNegative() throws IOException {
		ColorModel srcColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		ColorModel dstColorModel = CommonUtils.getColorModel(ImageIO
				.read(new File(TestParams.PREFIXPATH + TestParams.IMAGENAME)));
		Context context = new Context(TestParams.ALPHA, srcColorModel,
				dstColorModel);
		assertNotNull(context);

		int[] src = TestUtils.fillArray(TestParams.HEPTAGON + 1);
		int[] dst = TestUtils.fillArray(TestParams.HEPTAGON + 1);
		/* Checks ifs */
		/* sr = 255 */
		src[0] = 255;
		/* sg = 255 */
		src[1] = 255;
		/* sb = 255 */
		src[2] = 255;

		context.composeRGB(src, dst, TestParams.ALPHA);
		assertTrue(true);
	}

	@After
	public void tearDown() throws Exception {
	}
}
