/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.ITERATED;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link IteratedFilter} functionality.
 */
public class IteratedFilterTest {

	private IteratedFilter iteratedFilter;
	private BufferedImageOp filter = null;
	private BufferedImage src = null;

	@Before
	public void setUp() throws Exception {
		float[] sharpKernel = { 0.0f, -1.0f, 0.0f, -1.0f, 5.0f, -1.0f, 0.0f,
				-1.0f, 0.0f };
		filter = new ConvolveOp(new Kernel(3, 3, sharpKernel));
		int iterations = TestUtils.genInteger();
		iteratedFilter = new IteratedFilter(filter, iterations);
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testFilter() {
		try {
			BufferedImage result = iteratedFilter.filter(src, null);
			assertNotNull(result);
			CommonUtils.saveBufferedImage(result, TO_BE_SAVED + ITERATED.to(), "jpeg");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test
	public void testFilterIterOne() {
		try {
			iteratedFilter = new IteratedFilter(filter, 1);
			BufferedImage result = iteratedFilter.filter(src, null);
			assertNotNull(result);
		} catch (Exception e) {
			// nothing to do
		}
	}
	

	@Test
	public void testIteratedFilter() {
		assertNotNull(filter);
	}

	@Test
	public void testToString() {
		assertTrue(iteratedFilter.toString().contains(
				IteratedFilter.class.getName()));
	}
}
