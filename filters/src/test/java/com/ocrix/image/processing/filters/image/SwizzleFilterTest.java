package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.SWIZZLE;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.filters.composite.TestParams;


/**
 * Test {@link SwimFilter} functionality
 */
public class SwizzleFilterTest {
	private static SwizzleFilter swizzleFilter = null;
	private static BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		swizzleFilter = new SwizzleFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}
	
	@Test
	public void testCitor(){
		assertNotNull(swizzleFilter);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testNullMatrix(){
		swizzleFilter.setMatrix(null);
	}
	
	@Test
	public void testSwizzleFilter(){
		swizzleFilter.setMatrix(new int[]{1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0});
		BufferedImage result = swizzleFilter.filter(src, src);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + SWIZZLE.to(), "jpeg");
		System.out.println(result);
	}
	
	@Test
	public void testGetMatrix() {
		int[] matrix = TestUtils.fillArray(TestParams.DIME);
		swizzleFilter.setMatrix(matrix);
		assertArrayEquals(matrix, swizzleFilter.getMatrix());
	}

	@Test
	public void testToString() {
		assertTrue(swizzleFilter.toString().contains(SWIZZLE.to()));
	}
	
	@Test
	public void testFilterRGB() {
		int result = swizzleFilter.filterRGB(TestParams.RONNA, TestParams.RONNA, Color.YELLOW.getRGB());
		assertTrue(result != 0);
	}
	
	@After
	public void tearDown() throws Exception {
	}

}
