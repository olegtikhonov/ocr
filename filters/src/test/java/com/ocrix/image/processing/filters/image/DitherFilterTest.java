/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import static com.ocrix.image.processing.filters.Filters.DITHER;
import static com.ocrix.image.processing.filters.composite.TestParams.BINARY_STAR;
import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TETRA;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;


/**
 * 
 * Tests {@link DitherFilter} functionality.
 *
 */
public class DitherFilterTest {

	private DitherFilter ditherFilter;
	private BufferedImage src = null;
	
	@Before
	public void setUp() throws Exception {
		ditherFilter = new DitherFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
	}

	@Test
	public void testFilterRGB() {
		int result = ditherFilter.filterRGB(src.getHeight() / BINARY_STAR, src.getHeight() / BINARY_STAR, 
				                            src.getRGB(src.getWidth() / BINARY_STAR, src.getWidth() / TETRA));
		assertTrue(result != 0);
		
		
		
		// for code coverage
		ditherFilter.setColorDither(false);
		result = ditherFilter.filterRGB(src.getHeight() / BINARY_STAR,
				                        src.getHeight() / BINARY_STAR, 
				                        src.getRGB(src.getWidth() / BINARY_STAR, 
				                        src.getWidth() / TETRA));
		assertTrue(result != 0);
	}

	@Test
	public void testDitherFilter()  throws Exception{
		int[] matrix = new int[] { 0, 14, 3, 13, 11, 5, 8, 6, 12, 2, 15, 1, 7, 9, 4, 10 }; 
		ditherFilter.setMatrix(matrix);
		ditherFilter.setLevels(TETRA);
		BufferedImage result =  ditherFilter.filter(src, ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + DITHER.to(), "tiff");
	}

	@Test
	public void testSetMatrix() {
		int[] matrix = new int[] { 0, 2, 3, 1}; 
		ditherFilter.setMatrix(matrix);
		assertEquals(matrix, ditherFilter.getMatrix());
	}

	@Test
	public void testGetMatrix() {
		testSetMatrix();
	}

	@Test
	public void testSetLevels() {
		ditherFilter.setLevels(TETRA);
		assertEquals(TETRA, ditherFilter.getLevels());
	}

	@Test
	public void testGetLevels() {
		testSetLevels();
	}

	@Test
	public void testSetColorDither() {
		ditherFilter.setColorDither(false);
		assertFalse(ditherFilter.getColorDither());
		
		ditherFilter.setColorDither(true);
		assertTrue(ditherFilter.getColorDither());
	}

	@Test
	public void testGetColorDither() {
		testSetColorDither();
	}

	@Test
	public void testInitialize() {
		ditherFilter.initialize();
		assertTrue(true);
	}

	@Test
	public void testToString() {
		assertTrue(ditherFilter.toString().contains(DitherFilter.class.getName()));
	}
}
