/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.LAPLACIAN;
import static com.ocrix.image.processing.filters.composite.TestParams.IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.File;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.edgedetecting.Laplacian;


/*
 * Tests {@link LaplaceFilter} functionality. 
 */
public class LaplaceFilterTest {

	private static LaplaceFilter laplaceFilter = null;
	private static Laplacian laplacianFilter = null;
	private static BufferedImage src = null;
	private static final Logger LOG = Logger.getLogger(LaplaceFilterTest.class);
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		src = ImageIO.read(new File(PREFIXPATH + IMAGENAME));
		laplaceFilter = new LaplaceFilter();
		laplacianFilter = new Laplacian();
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(laplaceFilter);
	}
	
	
	@Test
	public void testLaplaceFilter() {
		BufferedImage result = laplaceFilter.filter(src, null);
		assertNotNull(result);

		int width = src.getWidth();// current
		int height = src.getHeight();// current

		int[] orig = new int[width * height];// creates orig int array
		/* Creates a pixel grabber */
		PixelGrabber grabber = new PixelGrabber(src, 0, 0, width, height, orig, 0, width);
		try {
			grabber.grabPixels();
		} catch (InterruptedException e) {
			LOG.error(e);
		}

		/* Inits an edgeDetector */
		laplacianFilter.init(orig, width, height);
		/* Does a Sobel edge detection */
		int[] res = laplacianFilter.process();

		/* Saves a new image */
		Image mis = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(width, height, res, 0, width));
		BufferedImage output = CommonUtils.toBufferedImage(mis);
		assertNotNull(output);
		CommonUtils.saveBufferedImage(output, TO_BE_SAVED + LAPLACIAN.to(), "tiff");
	}
	
	@Test
	public void testToString() {
		System.out.println(laplaceFilter.toString());
		assertTrue(LaplaceFilter.class.getName().contains(laplaceFilter.toString()));
	}
}
