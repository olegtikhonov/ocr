/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.image.processing.filters.image;


import static com.ocrix.image.processing.filters.Filters.INTERPOLATE;
import static com.ocrix.image.processing.filters.composite.TestParams.ALPHA;
import static com.ocrix.image.processing.filters.composite.TestParams.DEST_IMAGENAME;
import static com.ocrix.image.processing.filters.composite.TestParams.OLD_TEXT;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.SALT_AND_PEPPER;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;


/**
 * Tests {@link InterpolateFilter} functionality.
 */
public class InterpolateFilterTest {

	private static InterpolateFilter interpolateFilter = null;
	private static BufferedImage src = null;
	private static BufferedImage dst = null;
	
	@Before
	public void setUp() throws Exception {
		interpolateFilter = new InterpolateFilter();
		src = ImageIO.read(new File(PREFIXPATH + OLD_TEXT));
		dst = ImageIO.read(new File(PREFIXPATH + DEST_IMAGENAME));
	}

	@Test
	public void testCitor(){
		assertNotNull(interpolateFilter);
	}
	
	@Test
	public void testInterpolateFilter(){
		interpolateFilter.setDestination(dst);
		interpolateFilter.setInterpolation(ALPHA);
		BufferedImage result = interpolateFilter.filter(src, null);
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + INTERPOLATE.to(), "bmp");
	}

	@Test
	public void testGetDestination() throws IOException {
		BufferedImage dest = ImageIO.read(new File(PREFIXPATH + SALT_AND_PEPPER));
		interpolateFilter.setDestination(dest);
		assertEquals(dest, interpolateFilter.getDestination());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetDestinationNullParam(){
		interpolateFilter.setDestination(null);
	}
	
	@Test
	public void testFilterCases() {
		//if (destination == null)
		interpolateFilter = new InterpolateFilter(null, TestUtils.genFloat());
		//if (dst != null)
		BufferedImage result = interpolateFilter.filter(src, dst);
		assertNotNull(result);
	}
}
