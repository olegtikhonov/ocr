/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import java.awt.geom.Rectangle2D;

/**
 * Defines common messages.
 *
 */
public enum CommonMessage {

    /**
     * Defines a warn message 'The provided image is null, default BufferedImage
     * will be returned'.
     */
    DEFAULT_BUF_IMAGE(
	    "The provided image is null, default BufferedImage will be returned"),

    /**
     * Defines a msg 'provided image is NULL'.
     */
    IMAGE_NULL("The provided image is NULL"),

    /**
     * Defines a message 'Cannot insert value because (i+3) is bigger then a
     * length of the array.'.
     */
    WARN_LENGTH_BIGGER(
	    "Cannot insert value because (i+3) is bigger then a length of the array."),

    /**
     * Defines a {@link Rectangle2D.Float} message meaning this object is NULL.
     */
    FLOAT_NULL("The float is NULL");

    private String value;

    private CommonMessage(String toBeSet) {
	this.value = toBeSet;
    }

    public String to() {
	return this.value;
    }
}
