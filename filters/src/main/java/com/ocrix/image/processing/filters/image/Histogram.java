/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;

import java.awt.Color;

import com.ocrix.image.processing.filters.Validator;




/**
 * An image histogram.
 */
public class Histogram {
	enum COLORS {RED, GREEN, BLUE, GRAY};

	private int[][] histogram;
	private int numSamples;
	private int[] minValue;
	private int[] maxValue;
	private int[] minFrequency;
	private int[] maxFrequency;
	private float[] mean;
	private boolean isGray;

	
	/**
	 * Constructs an {@link Histogram} with default parameters:
	 * <br>
	 * <li>histogram = null;</li>
	 * <li>numSamples = 0;</li>
	 * <li>isGray = true;</li>
	 * <li>minValue = null;</li>
	 * <li>maxValue = null;</li>
	 * <li>minFrequency = null;</li>
	 * <li>maxFrequency = null;</li>
	 * <li>mean = null;</li>
	 */
	public Histogram() {
		histogram = null;
		numSamples = 0;
		isGray = true;
		minValue = null;
		maxValue = null;
		minFrequency = null;
		maxFrequency = null;
		mean = null;
	}

	public Histogram(int[] pixels, int w, int h, int offset, int stride) {
		Validator.validateIntArrayEmptiness(pixels);
		histogram = new int[3][256];
		minValue = new int[4];
		maxValue = new int[4];
		minFrequency = new int[3];
		maxFrequency = new int[3];
		mean = new float[3];

		numSamples = w * h;
		isGray = true;

		int index = 0;
		for (int y = 0; y < h; y++) {
			index = offset + y * stride;
			for (int x = 0; x < w; x++) {
				int rgb = Color.ORANGE.getRGB();
				
				if(index < pixels.length){
					rgb = pixels[index++];
				}
				int r = (rgb >> 16) & 0xff;
				int g = (rgb >> 8) & 0xff;
				int b = rgb & 0xff;
				histogram[COLORS.RED.ordinal()][r]++;
				histogram[COLORS.GREEN.ordinal()][g]++;
				histogram[COLORS.BLUE.ordinal()][b]++;
			}
		}

		for (int i = 0; i < 256; i++) {
			if (histogram[COLORS.RED.ordinal()][i] != histogram[COLORS.GREEN.ordinal()][i]
					|| histogram[COLORS.GREEN.ordinal()][i] != histogram[COLORS.BLUE.ordinal()][i]) 
			{
				isGray = false;
				break;
			}
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 256; j++) {
				if (histogram[i][j] > 0) {
					minValue[i] = j;
					break;
				}
			}

			for (int j = 255; j >= 0; j--) {
				if (histogram[i][j] > 0) {
					maxValue[i] = j;
					break;
				}
			}

			minFrequency[i] = Integer.MAX_VALUE;
			maxFrequency[i] = 0;
			for (int j = 0; j < 256; j++) {
				minFrequency[i] = Math.min(minFrequency[i], histogram[i][j]);
				maxFrequency[i] = Math.max(maxFrequency[i], histogram[i][j]);
				mean[i] += (float) (j * histogram[i][j]);
			}
			mean[i] /= (float)numSamples;
		}
		
		minValue[COLORS.GRAY.ordinal()] = Math.min(Math.min(minValue[COLORS.RED.ordinal()],
						                                    minValue[COLORS.GREEN.ordinal()]),
						                                    minValue[COLORS.BLUE.ordinal()]);
		
		maxValue[COLORS.GRAY.ordinal()] = Math.max(Math.max(maxValue[COLORS.RED.ordinal()],
						                                    maxValue[COLORS.GREEN.ordinal()]),
				                                            maxValue[COLORS.BLUE.ordinal()]);
	}

	protected boolean isGray() {
		return isGray;
	}

	protected void setisGary(boolean isGray){
		this.isGray = isGray;
	}
	
	
	protected int getNumSamples() {
		return numSamples;
	}

	
	/**
	 * 
	 * @param value [0 : 255], otherwise -1
	 * 
	 * @return either -1 or histogram[0][value]
	 */
	protected int getFrequency(int value) {
		if (numSamples > 0 && isGray && value >= 0 && value <= 255)
			return histogram[0][value];
		return -1;
	}

	protected int getFrequency(int channel, int value) {
		if (numSamples < 1 || channel < 0 || channel > 2 || value < 0 || value > 255)
			return -1;
		return histogram[channel][value];
	}

	protected int getMinFrequency() {
		if (numSamples > 0 && isGray)
			return minFrequency[0];
		return -1;
	}

	/**
	 * 
	 * @param channel (0 : 2)
	 * @return
	 */
	protected int getMinFrequency(int channel) {
		if (numSamples < 1 || channel < 0 || channel > 2)
			return -1;
		return minFrequency[channel];
	}


	protected int getMaxFrequency() {
		if (numSamples > 0 && isGray)
			return maxFrequency[0];
		return -1;
	}

	protected int getMaxFrequency(int channel) {
		if (numSamples < 1 || channel < 0 || channel > 2)
			return -1;
		return maxFrequency[channel];
	}


	protected int getMinValue() {
		if (numSamples > 0 && isGray)
			return minValue[0];
		return -1;
	}

	protected int getMinValue(int channel) {
		if(channel >= 0 && channel < minValue.length)
			return minValue[channel];
		else
			return -1;
	}

	protected int getMaxValue() {
		if (numSamples > 0 && isGray)
			return maxValue[0];
		return -1;
	}

	protected int getMaxValue(int channel) {
		if(channel >= 0 && channel < maxValue.length)
			return maxValue[channel];
		else
			 return -1;
	}

	protected float getMeanValue() {
		if (numSamples > 0 && isGray)
			return mean[0];
		return -1.0F;
	}

	protected float getMeanValue(int channel) {
		if (numSamples > 0 && COLORS.RED.ordinal() <= channel && channel <= COLORS.BLUE.ordinal())
			return mean[channel];
		return -1.0F;
	}
}
