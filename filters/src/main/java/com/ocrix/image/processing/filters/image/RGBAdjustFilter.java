/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

/**
 * Defaults are:
 * <ul>
 * <li>rFactor = 0.0f</li>
 * <li>gFactor = 0.0f</li>
 * <li>bFactor = 0.0f</li>
 * </ul>
 * 
 */
public class RGBAdjustFilter extends PointFilter {

	private float rFactor, gFactor, bFactor;

	/**
	 * Creates {@link RGBAdjustFilter}.
	 * All factors set to zero.
	 */
	public RGBAdjustFilter() {
		this(0, 0, 0);
	}

	/**
	 * Creates a {@link RGBAdjustFilter}.
	 * Note: Puts canFilterIndexColorModel to <code>true</code>.
	 * 
	 * @param r for red factor.
	 * @param g for green factor.
	 * @param b for the blue factor.
	 */
	public RGBAdjustFilter(float r, float g, float b) {
		rFactor = 1 + r;
		gFactor = 1 + g;
		bFactor = 1 + b;
		canFilterIndexColorModel = true;
	}

	/**
	 * Sets a red factor.
	 * 
	 * @param rFactor read factor.
	 */
	public void setRFactor(float rFactor) {
		this.rFactor = 1 + rFactor;
	}

	/**
	 * Gets a red factor.
	 * 
	 * @return
	 */
	public float getRFactor() {
		return rFactor - 1;
	}

	/**
	 * Sets a green factor.
	 * 
	 * @param gFactor green factor.
	 */
	public void setGFactor(float gFactor) {
		this.gFactor = 1 + gFactor;
	}

	/**
	 * Gets a green factor.
	 * 
	 * @return
	 */
	public float getGFactor() {
		return gFactor - 1;
	}

	/**
	 * Sets a blue factor.
	 * 
	 * @param bFactor to be set.
	 */
	public void setBFactor(float bFactor) {
		this.bFactor = 1 + bFactor;
	}

	/**
	 * Gets a blue factor.
	 * 
	 * @return
	 */
	public float getBFactor() {
		return bFactor - 1;
	}

	/**
	 * Gets a lookup table. Changes color from one value to another based on a
	 * list of values. Is useful for matching footage from one source to
	 * another. 1D, or one dimensional, LUTs are LUTs that usually have an exact
	 * output value for a corresponding input value. 1D LUTs are limited in that
	 * they cannot alter color saturation without affecting contrast or
	 * brightness along with it. As an example the start of a 1D LUT could look
	 * something like this: Note: strictly speaking this is 3x 1D LUTs, as each
	 * colour (R,G,B) is a 1D LUT
	 * 
	 * R, G, B 3, 0, 0 5, 2, 1 7, 5, 3 9, 9, 9
	 * 
	 * Which means that: For an input value of 0 for R, G, and B, the output is
	 * R=3, G=0, B=0 For an input value of 1 for R, G, and B, the output is R=5,
	 * G=2, B=1 For an input value of 2 for R, G, and B, the output is R=7, G=5,
	 * B=3 For an input value of 3 for R, G, and B, the output is R=9, G=9, B=9
	 * 
	 * 
	 * @return
	 */
	public int[] getLUT() {
		int[] lut = new int[256];
		for (int i = 0; i < 256; i++) {
			lut[i] = filterRGB(0, 0, (i << 24) | (i << 16) | (i << 8) | i);
		}
		return lut;
	}

	public int filterRGB(int x, int y, int rgb) {
		int a = rgb & 0xff000000;
		int r = (rgb >> 16) & 0xff;
		int g = (rgb >> 8) & 0xff;
		int b = rgb & 0xff;
		r = PixelUtils.clamp((int) (r * rFactor));
		g = PixelUtils.clamp((int) (g * gFactor));
		b = PixelUtils.clamp((int) (b * bFactor));
		return a | (r << 16) | (g << 8) | b;
	}

	public String toString() {
		return this.getClass().getName();
	}
}
