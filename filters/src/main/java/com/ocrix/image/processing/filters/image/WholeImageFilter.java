/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;

import java.awt.*;
import java.awt.image.*;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.ImageComparator;
import com.ocrix.image.processing.filters.Validator;



/**
 * A filter which acts as a superclass for filters which need to have the whole image in memory
 * to do their stuff.
 */
public abstract class WholeImageFilter extends AbstractBufferedImageOp {

	/**
     * The output image bounds.
     */
    protected Rectangle transformedSpace;

	/**
     * The input image bounds.
     */
	protected Rectangle originalSpace;
	
	/**
	 * Construct a WholeImageFilter.
	 */
	public WholeImageFilter() {
	}

	
    public BufferedImage filter( BufferedImage src, BufferedImage dst ) {
    	Validator.validateBufferedImage(src);

        int width = 0;
        int height = 0;
    	
        if ( dst == null ) {
            ColorModel dstCM = src.getColorModel();
            
            width = src.getWidth();
            height = src.getHeight();
            
            originalSpace = new Rectangle(0, 0, width, height);
    		transformedSpace = new Rectangle(0, 0, width, height);
            
			dst = new BufferedImage(dstCM, dstCM.createCompatibleWritableRaster(transformedSpace.width, transformedSpace.height), dstCM.isAlphaPremultiplied(), null);
		} else {
			//checks which image is bigger & resize
			ImageComparator ic = new ImageComparator();
			
			if(ic.compare(src, dst) == 1) {
				//src > dst
				dst = CommonUtils.resizeImage(dst, src.getWidth(), src.getHeight(), src.getType());
				
				 width = src.getWidth();
		         height = src.getHeight();
//		         transformedSpace = new Rectangle(0, 0, width, height);
				
			}else if(ic.compare(src, dst) == -1) {
				// src < dst
				src = CommonUtils.resizeImage(src, dst.getWidth(), dst.getHeight(), dst.getType());

				width = dst.getWidth();
				height = dst.getHeight();
//				transformedSpace = new Rectangle(0, 0, width, height);
			} else {
				width = dst.getWidth();
				height = dst.getHeight();
			}
		}

        transformedSpace = new Rectangle(0, 0, width, height);
		transformSpace(transformedSpace);

		int[] inPixels = getRGB( src, 0, 0, width, height, null );
		inPixels = filterPixels( width, height, inPixels, transformedSpace );
		setRGB( dst, 0, 0, transformedSpace.width, transformedSpace.height, inPixels );

        return dst;
    }

	/**
     * Calculate output bounds for given input bounds.
     * @param rect input and output rectangle
     */
	protected void transformSpace(Rectangle rect) {
	}
	
	/**
     * Actually filter the pixels.
     * @param width the image width
     * @param height the image height
     * @param inPixels the image pixels
     * @param transformedSpace the output bounds
     * @return the output pixels
     */
	protected abstract int[] filterPixels( int width, int height, int[] inPixels, Rectangle transformedSpace );
}

