/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import java.awt.image.BufferedImage;
import java.util.Comparator;


/**
 * Compares two {@link BufferedImage}s which one is a bigger by either height or weight.
 * <pre>
 * if <code>-1</code> means src is smaller than dst
 * if <code>1</code> means src is bigger that dst
 * if <code>0</code> means src eq dst
 */
public class ImageComparator implements Comparator<BufferedImage>{

	@Override
	public int compare(BufferedImage src, BufferedImage dst) {
		if(src.getHeight() > dst.getHeight() || src.getWidth() > dst.getWidth()){
			return 1;
		}else if(src.getHeight() < dst.getHeight() || src.getWidth() < dst.getWidth()){
			return -1;
		}else {
			return 0;
		}
	}
}
