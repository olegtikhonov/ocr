/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;

import java.awt.Rectangle;


/**
 * Defines a shear filter, i.e the Shear filter distorts an image along a curve. 
 * <br>The defaults are:
 * <ul>
 * <li>xangle = <code>0.0f</code></li>
 * <li>yangle = <code>0.0f</code></li>
 * <li>shx = <code>0.0f</code></li>
 * <li>shy = <code>0.0f</code></li>
 * <li>xoffset = <code>0.0f</code></li>
 * <li>yoffset = <code>0.0f</code></li>
 * <li>resize = <code>true</code></li>
 * </ul>
 * 
 */
public class ShearFilter extends TransformFilter {
	/* Class member declarations */
	private float xangle = 0.0f;
	private float yangle = 0.0f;
	private float shx = 0.0f;
	private float shy = 0.0f;
	private float xoffset = 0.0f;
	private float yoffset = 0.0f;
	private boolean resize = true;
	/* End of class member declarations */
	

	/**
	 * Constructs a shear filter with its defaults.
	 */
	public ShearFilter() {
	}

	
	/**
	 * Sets a flag meaning of resize
	 * 
	 * @param resize <code>true</code> or <code>false</code>
	 */
	public void setResize(boolean resize) {
		this.resize = resize;
	}

	/**
	 * Gets a resize flag.
	 * 
	 * @return
	 */
	public boolean isResize() {
		return resize;
	}

	/**
	 * Sets a X-coord angle
	 * 
	 * @param xangle
	 */
	public void setXAngle(float xangle) {
		this.xangle = xangle;
		initialize();
	}

	
	/**
	 * Gets a X-coord angle.
	 * 
	 * @return
	 */
	public float getXAngle() {
		return xangle;
	}

	
	/**
	 * Sets a Y-coord angle.
	 * 
	 * @param yangle
	 */
	public void setYAngle(float yangle) {
		this.yangle = yangle;
		initialize();
	}

	
	/**
	 * Gets a Y-coord angle.
	 * 
	 * @return
	 */
	public float getYAngle() {
		return yangle;
	}

	
	/**
	 * Initializes shx and shy using {@link Math} sin function.
	 */
	private void initialize() {
		shx = (float)Math.sin(xangle);
		shy = (float)Math.sin(yangle);
	}
	
	
	/**
	 * Transforms a space by given {@link Rectangle}.
	 * 
	 * @param {@link Rectangle}
	 */
	protected void transformSpace(Rectangle r) {
		float tangent = (float)Math.tan(xangle);
		xoffset = -r.height * tangent;
		if (tangent < 0.0)
			tangent = -tangent;
		r.width = (int)(r.height * tangent + r.width + 0.999999f);
		tangent = (float)Math.tan(yangle);
		yoffset = -r.width * tangent;
		if (tangent < 0.0)
			tangent = -tangent;
		r.height = (int)(r.width * tangent + r.height + 0.999999f);
	}

	
	protected void transformInverse(int x, int y, float[] out) {
		out[0] = x + xoffset + (y * shx);
		out[1] = y + yoffset + (x * shy);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getName()).append(" [xangle=");
		builder.append(xangle);
		builder.append(", yangle=");
		builder.append(yangle);
		builder.append(", shx=");
		builder.append(shx);
		builder.append(", shy=");
		builder.append(shy);
		builder.append(", xoffset=");
		builder.append(xoffset);
		builder.append(", yoffset=");
		builder.append(yoffset);
		builder.append(", resize=");
		builder.append(resize);
		builder.append("]");
		return builder.toString();
	}
}
