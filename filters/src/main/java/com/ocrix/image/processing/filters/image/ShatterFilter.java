/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.util.*;
import com.ocrix.image.processing.filters.Validator;


/**
 * Defines a shatter filter. The shatter effect explodes images. <a href=
 * "http://help.adobe.com/en_US/AfterEffects/9.0/WS3878526689cb91655866c1103a9d3c597-7a8da.html"
 * >More info if you like</a> <br>
 * Defaults are:
 * <ul>
 * <li>centreX = <code>0.5f</code></li>
 * <li>centreY = <code>0.5f</code></li>
 * <li>startAlpha = <code>1.0f</code></li>
 * <li>endAlpha = <code>1.0f</code></li>
 * <li>iterations = <code>5.0f</code></li>
 * </ul>
 * 
 */
public class ShatterFilter extends AbstractBufferedImageOp {
	private float centreX = 0.5f, centreY = 0.5f;
	private float distance;
	private float transition;
	private float rotation;
	private float zoom;
	private float startAlpha = 1;
	private float endAlpha = 1;
	private int iterations = 5;
	private int tile;

    /**
     * Creates a {@link ShatterFilter}.	
     */
	public ShatterFilter() {
	}

	/**
	 * Sets a transition.
	 * 
	 * @param transition to be set.
	 */
	public void setTransition(float transition) {
		this.transition = transition;
	}

	/**
	 * Gets a transition.
	 * 
	 * @return a transition. 
	 */
	public float getTransition() {
		return transition;
	}

	/**
	 * Sets a distance.
	 * 
	 * @param distance
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	/**
	 * Gets the distance.
	 * 
	 * @return the distance.
	 */
	public float getDistance() {
		return distance;
	}

	/**
	 * Sets a rotation.
	 * 
	 * @param rotation to be set.
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	/**
	 * Gets a rotation.
	 * 
	 * @return the rotation.
	 */
	public float getRotation() {
		return rotation;
	}

	/**
	 * Sets a zoom.
	 * 
	 * @param zoom to be set.
	 */
	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

	/**
	 * Gets the zoom.
	 * 
	 * @return the zoom.
	 */
	public float getZoom() {
		return zoom;
	}

	/**
	 * Sets a start alpha.
	 * 
	 * @param startAlpha to be set.
	 */
	public void setStartAlpha(float startAlpha) {
		this.startAlpha = startAlpha;
	}

	/**
	 * Gets the start alpha.
	 * 
	 * @return the start alpha.
	 */
	public float getStartAlpha() {
		return startAlpha;
	}

	/**
	 * Sets an end alpha.
	 * 
	 * @param endAlpha to be set.
	 */
	public void setEndAlpha(float endAlpha) {
		this.endAlpha = endAlpha;
	}

	/**
	 * Gets the end alpha.
	 * 
	 * @return the end alpha.
	 */
	public float getEndAlpha() {
		return endAlpha;
	}

	/**
	 * Sets a centre x.
	 * 
	 * @param centreX to be set.
	 */
	public void setCentreX(float centreX) {
		this.centreX = centreX;
	}

	/**
	 * Gets the centre x.
	 * 
	 * @return the centre x.
	 */
	public float getCentreX() {
		return centreX;
	}

	/**
	 * Sets a centre Y.
	 * 
	 * @param centreY to be set.
	 */
	public void setCentreY(float centreY) {
		this.centreY = centreY;
	}

	/**
	 * Gets the centre Y.
	 * 
	 * @return the centre Y.
	 */
	public float getCentreY() {
		return centreY;
	}

	/**
	 * Sets the centre.
	 * 
	 * @param centre the centre.
	 */
	public void setCentre(Point2D centre) {
		this.centreX = (float) centre.getX();
		this.centreY = (float) centre.getY();
	}

	/**
	 * Gets the centre.
	 * 
	 * @return the centre.
	 */
	public Point2D getCentre() {
		return new Point2D.Float(centreX, centreY);
	}

	/**
	 * Sets iterations.
	 * 
	 * @param iterations to be set.
	 */
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	/**
	 * Gets the iterations.
	 * 
	 * @return the iterations.
	 */
	public int getIterations() {
		return iterations;
	}

	/**
	 * Sets a tile.
	 */
	public void setTile(int tile) {
		this.tile = tile;
	}

	/**
	 * Gets the tile.
	 * 
	 * @return the tile.
	 */
	public int getTile() {
		return tile;
	}

	/**
	 * Defines a Tile data structure.
	 */
	static class Tile {
		float x, y, vx, vy, w, h;
		float rotation;
		Shape shape;
	}

	/**
	 * Applies the shatter filter.
	 * 
	 * @param src a source image.
	 * @param dst a destination image.
	 */
	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		if (dst == null)
			dst = createCompatibleDestImage(src, null);
		float width = (float) src.getWidth();
		float height = (float) src.getHeight();
		float cx = (float) src.getWidth() * centreX;
		float cy = (float) src.getHeight() * centreY;

		int numTiles = iterations * iterations;
		Tile[] shapes = new Tile[numTiles];
		float[] rx = new float[numTiles];
		float[] ry = new float[numTiles];
		float[] rz = new float[numTiles];

		Graphics2D g = dst.createGraphics();

		Random random = new Random(0);

		for (int y = 0; y < iterations; y++) {
			int y1 = (int) height * y / iterations;
			int y2 = (int) height * (y + 1) / iterations;
			for (int x = 0; x < iterations; x++) {
				int i = y * iterations + x;
				int x1 = (int) width * x / iterations;
				int x2 = (int) width * (x + 1) / iterations;
				rx[i] = tile * random.nextFloat();
				ry[i] = tile * random.nextFloat();
				rx[i] = 0;
				ry[i] = 0;
				rz[i] = tile * (2 * random.nextFloat() - 1);
				Shape p = new Rectangle(x1, y1, x2 - x1, y2 - y1);
				shapes[i] = new Tile();
				shapes[i].shape = p;
				shapes[i].x = (x1 + x2) * 0.5f;
				shapes[i].y = (y1 + y2) * 0.5f;
				shapes[i].vx = width - (cx - x);
				shapes[i].vy = height - (cy - y);
				shapes[i].w = x2 - x1;
				shapes[i].h = y2 - y1;
			}
		}

		
		for (int i = 0; i < numTiles; i++) {
			float h = (float) i / numTiles;
			double angle = h * 2 * Math.PI;
			float x = transition * width * (float) Math.cos(angle);
			float y = transition * height * (float) Math.sin(angle);

			Tile tile = shapes[i];
			AffineTransform t = g.getTransform();
			x = tile.x + transition * tile.vx;
			y = tile.y + transition * tile.vy;
			g.translate(x, y);
			g.rotate(transition * rz[i]);
			g.setColor(Color.getHSBColor(h, 1, 1));
			Shape clip = g.getClip();
			g.clip(tile.shape);
			g.drawImage(src, 0, 0, null);
			g.setClip(clip);
			g.setTransform(t);
		}

		g.dispose();
		return dst;
	}

    @Override
	public String toString() {
		return this.getClass().getName();
	}
}
