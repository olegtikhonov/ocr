/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import com.ocrix.image.processing.filters.Validator;
import com.ocrix.image.processing.filters.math.Function2D;
import com.ocrix.image.processing.filters.math.Noise;


/**
 * <br>The defaults are:
 * <ul>
 * <li>scale = 32.0f</li>
 * <li>stretch = 1.0f</li>
 * <li>angle = 0.0f</li>
 * <li>amount = 1.0f</li>
 * <li>turbulence = 1.0f</li>
 * <li>gain = 0.5f</li>
 * <li>bias = 0.5f</li>
 * <li>m00 = 1.0f</li>
 * <li>m01 = 0.0f</li>
 * <li>m10 = 0.0f</li>
 * <li>m11 = 1.0f</li>
 * <ul>
 */
public class TextureFilter extends PointFilter {
	/* Class member declarations */
	private float scale = 32;
	private float stretch = 1.0f;
	private float angle = 0.0f;
	public float amount = 1.0f;
	public float turbulence = 1.0f;
	public float gain = 0.5f;
	public float bias = 0.5f;
	public int operation;
	private float m00 = 1.0f;
	private float m01 = 0.0f;
	private float m10 = 0.0f;
	private float m11 = 1.0f;
	private Colormap colormap = new Gradient();
	private Function2D function = new Noise();
	/* End of class member declarations */

	
	/**
	 * Constructs a texture filter with its defaults.
	 */
	public TextureFilter() {
	}

	
	/**
	 * Sets the amount of texture.
	 * 
	 * @param amount - the amount
	 * @min-value 0
	 * @max-value 1
	 * @see #getAmount
	 */
	public void setAmount(float amount) {
		Validator.validateFloatInRange(amount, 0, 1);
		this.amount = amount;
	}

	
	/**
	 * Gets the amount of texture.
	 * 
	 * @return the amount
	 * @see #setAmount
	 */
	public float getAmount() {
		return amount;
	}

	
	/**
	 * Sets a {@link Function2D}
	 * 
	 * @param function
	 * @see #getFunction()
	 */
	public void setFunction(Function2D function) {
		this.function = function;
	}

	
	/**
	 * Gets a {@link Function2D}
	 * 
	 * @return
	 * @see #setFunction(Function2D)
	 */
	public Function2D getFunction() {
		return function;
	}

	
	/**
	 * Sets an operation
	 * 
	 * @param operation
	 * @see #getOperation()
	 */
	public void setOperation(int operation) {
		this.operation = operation;
	}

	/**
	 * Gets an operation
	 * 
	 * @return
	 * @see #setOperation(int)
	 */
	public int getOperation() {
		return operation;
	}

	/**
	 * Specifies the scale of the texture.
	 * 
	 * @param scale - the scale of the texture.
	 * @min-value 1
	 * @max-value 300+
	 * @see #getScale
	 */
	public void setScale(float scale) {
		Validator.validateFloatInRange(scale, 1, 300);
		this.scale = scale;
	}

	/**
	 * Returns the scale of the texture.
	 * 
	 * @return the scale of the texture.
	 * @see #setScale
	 */
	public float getScale() {
		return scale;
	}

	/**
	 * Specifies the stretch factor of the texture.
	 * 
	 * @param stretch - the stretch factor of the texture.
	 * @min-value 1
	 * @max-value 50+
	 * @see #getStretch
	 */
	public void setStretch(float stretch) {
		Validator.validateFloatInRange(stretch, 1, 50);
		this.stretch = stretch;
	}

	/**
	 * Returns the stretch factor of the texture.
	 * 
	 * @return the stretch factor of the texture.
	 * @see #setStretch
	 */
	public float getStretch() {
		return stretch;
	}

	/**
	 * Specifies the angle of the texture.
	 * 
	 * @param angle - the angle of the texture.
	 * @angle
	 * @see #getAngle
	 */
	public void setAngle(float angle) {
		this.angle = angle;
		float cos = (float) Math.cos(angle);
		float sin = (float) Math.sin(angle);
		m00 = cos;
		m01 = sin;
		m10 = -sin;
		m11 = cos;
	}

	
	/**
	 * Returns the angle of the texture.
	 * 
	 * @return the angle of the texture.
	 * @see #setAngle
	 */
	public float getAngle() {
		return angle;
	}

	/**
	 * Specifies the turbulence of the texture.
	 * 
	 * @param turbulence - the turbulence of the texture.
	 * @min-value 0
	 * @max-value 1
	 * @see #getTurbulence
	 */
	public void setTurbulence(float turbulence) {
		Validator.validateFloatInRange(turbulence, 0, 1);
		this.turbulence = turbulence;
	}

	/**
	 * Returns the turbulence of the texture.
	 * 
	 * @return the turbulence of the texture.
	 * @see #setTurbulence
	 */
	public float getTurbulence() {
		return turbulence;
	}

	
	/**
	 * Sets the colormap to be used for the filter.
	 * 
	 * @param colormap - the colormap
	 * @see #getColormap
	 */
	public void setColormap(Colormap colormap) {
		this.colormap = colormap;
	}

	
	/**
	 * Gets the colormap to be used for the filter.
	 * 
	 * @return the colormap
	 * @see #setColormap
	 */
	public Colormap getColormap() {
		return colormap;
	}

	public int filterRGB(int x, int y, int rgb) {
		float nx = m00 * x + m01 * y;
		float ny = m10 * x + m11 * y;
		nx /= scale;
		ny /= scale * stretch;
		float f = turbulence == 1.0 ? Noise.noise2(nx, ny) : Noise.turbulence2(
				nx, ny, turbulence);
		f = (f * 0.5f) + 0.5f;
		f = ImageMath.gain(f, gain);
		f = ImageMath.bias(f, bias);
		f *= amount;
		int a = rgb & 0xff000000;
		int v;
		if (colormap != null)
			v = colormap.getColor(f);
		else {
			v = PixelUtils.clamp((int) (f * 255));
			int r = v << 16;
			int g = v << 8;
			int b = v;
			v = a | r | g | b;
		}
		if (operation != PixelUtils.REPLACE)
			v = PixelUtils.combinePixels(rgb, v, operation);
		return v;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append(" [scale=");
		builder.append(scale);
		builder.append(", stretch=");
		builder.append(stretch);
		builder.append(", angle=");
		builder.append(angle);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", turbulence=");
		builder.append(turbulence);
		builder.append(", gain=");
		builder.append(gain);
		builder.append(", bias=");
		builder.append(bias);
		builder.append(", operation=");
		builder.append(operation);
		builder.append(", m00=");
		builder.append(m00);
		builder.append(", m01=");
		builder.append(m01);
		builder.append(", m10=");
		builder.append(m10);
		builder.append(", m11=");
		builder.append(m11);
		builder.append(", ");
		if (colormap != null) {
			builder.append("colormap=");
			builder.append(colormap);
			builder.append(", ");
		}
		if (function != null) {
			builder.append("function=");
			builder.append(function);
		}
		builder.append("]");
		return builder.toString();
	}
}
