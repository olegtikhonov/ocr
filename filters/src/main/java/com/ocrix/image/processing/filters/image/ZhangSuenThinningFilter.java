/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import com.ocrix.image.processing.filters.Validator;

/**
 * Based on work of Nayef Reza.
 * 
 * http://nayefreza.wordpress.com/2013/05/11/zhang-suen-thinning-algorithm-java-
 * implementation/.
 * 
 */
public class ZhangSuenThinningFilter {

	
	public BufferedImage filter(BufferedImage src) {
		int[][] imageData = new int[src.getHeight()][src.getWidth()];

		for (int y = 0; y < imageData.length; y++) {
			for (int x = 0; x < imageData[y].length; x++) {

				if (src.getRGB(x, y) == Color.BLACK.getRGB()) {
					imageData[y][x] = 1;
				} else {
					imageData[y][x] = 0;

				}
			}
		}

		// Does the magic :-)
		doZhangSuenThinning(imageData);

		for (int y = 0; y < imageData.length; y++) {
			for (int x = 0; x < imageData[y].length; x++) {
				if (imageData[y][x] == 1) {
					src.setRGB(x, y, Color.BLACK.getRGB());

				} else {
					src.setRGB(x, y, Color.WHITE.getRGB());
				}
			}
		}
		
		return src;
	}
	
	
	
	/**
	 * 
	 * @param binaryImage
	 * @return
	 * 
	 * @throws IllegalArgumentException
	 *             if th ebinaryImage is null;
	 */
	private int[][] doZhangSuenThinning(int[][] binaryImage) {
		Validator.notNull(binaryImage);
		int a, b;
		List<Point> pointsToChange = new LinkedList<Point>();
		boolean hasChange;

		do {

			hasChange = false;
			for (int y = 1; y + 1 < binaryImage.length; y++) {
				for (int x = 1; x + 1 < binaryImage[y].length; x++) {
					a = getA(binaryImage, y, x);
					b = getB(binaryImage, y, x);
					if (binaryImage[y][x] == 1
							&& 2 <= b
							&& b <= 6
							&& a == 1
							&& (binaryImage[y - 1][x] * binaryImage[y][x + 1]
									* binaryImage[y + 1][x] == 0)
							&& (binaryImage[y][x + 1] * binaryImage[y + 1][x]
									* binaryImage[y][x - 1] == 0)) 
					{
						pointsToChange.add(new Point(x, y));
						// binaryImage[y][x] = 0;
						hasChange = true;
					}
				}
			}

			for (Point point : pointsToChange) {
				binaryImage[point.getY()][point.getX()] = 0;
			}

			pointsToChange.clear();

			for (int y = 1; y + 1 < binaryImage.length; y++) {
				for (int x = 1; x + 1 < binaryImage[y].length; x++) {
					a = getA(binaryImage, y, x);
					b = getB(binaryImage, y, x);
					if (binaryImage[y][x] == 1
							&& 2 <= b
							&& b <= 6
							&& a == 1
							&& (binaryImage[y - 1][x] * binaryImage[y][x + 1]
									* binaryImage[y][x - 1] == 0)
							&& (binaryImage[y - 1][x] * binaryImage[y + 1][x]
									* binaryImage[y][x - 1] == 0)) {
						pointsToChange.add(new Point(x, y));

						hasChange = true;
					}
				}
			}

			for (Point point : pointsToChange) {
				binaryImage[point.getY()][point.getX()] = 0;
			}

			pointsToChange.clear();

		} while (hasChange);

		return binaryImage;
	}

	private int getA(int[][] binaryImage, int y, int x) {
		int count = 0;
		// p2 p3
		if (binaryImage[y - 1][x] == 0 && binaryImage[y - 1][x + 1] == 1) {
			count++;
		}
		// p3 p4
		if (binaryImage[y - 1][x + 1] == 0 && binaryImage[y][x + 1] == 1) {
			count++;
		}
		// p4 p5
		if (binaryImage[y][x + 1] == 0 && binaryImage[y + 1][x + 1] == 1) {
			count++;
		}
		// p5 p6
		if (binaryImage[y + 1][x + 1] == 0 && binaryImage[y + 1][x] == 1) {
			count++;
		}
		// p6 p7
		if (binaryImage[y + 1][x] == 0 && binaryImage[y + 1][x - 1] == 1) {
			count++;
		}
		// p7 p8
		if (binaryImage[y + 1][x - 1] == 0 && binaryImage[y][x - 1] == 1) {
			count++;
		}
		// p8 p9
		if (binaryImage[y][x - 1] == 0 && binaryImage[y - 1][x - 1] == 1) {
			count++;
		}
		// p9 p2
		if (binaryImage[y - 1][x - 1] == 0 && binaryImage[y - 1][x] == 1) {
			count++;
		}

		return count;
	}

	private int getB(int[][] binaryImage, int y, int x) {

		return binaryImage[y - 1][x] + binaryImage[y - 1][x + 1]
				+ binaryImage[y][x + 1] + binaryImage[y + 1][x + 1]
				+ binaryImage[y + 1][x] + binaryImage[y + 1][x - 1]
				+ binaryImage[y][x - 1] + binaryImage[y - 1][x - 1];
	}

	/**
	 * Internal class represents a Point.
	 */
	private class Point {
		private int x, y;

		private Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		private int getX() {
			return x;
		}

		private int getY() {
			return y;
		}
	}
}
