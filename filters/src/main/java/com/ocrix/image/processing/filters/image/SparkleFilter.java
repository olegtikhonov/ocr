/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;


import java.util.Random;

import com.ocrix.image.processing.filters.Validator;



/**
 * SparkleFilter draws a sparkle or sunburst effect on an image. You can change
 * the number of rays in the sparkle, the randomness of the ray lengths and the
 * radius of the centre of the sparkle. You can use this filter multiple times
 * to create sparkles on corners in images. It's also fun to animate the
 * sparkling effect.<br>
 * The defaults are:
 * <ul>
 * <li>rays = <code>50</code></li>
 * <li>radius = <code>25</code></li>
 * <li>amount = <code>50</code></li>
 * <li>color = <code>0xffffffff</code></li>
 * <li>randomness = <code>25</code></li>
 * <li>seed = <code>371</code></li>
 * </ul>
 * 
 */
public class SparkleFilter extends PointFilter {
	/* Class member declarations */
	private int rays = 50;
	private int radius = 25;
	private int amount = 50;
	private int color = 0xffffffff;
	private int randomness = 25;
	protected int width = 0, height = 0;
	private int centreX, centreY;
	private long seed = 371;
	private float[] rayLengths;
	private Random randomNumbers = null;
	
	
	/**
	 * Constructs a sparkle filter with its defaults.
	 */
	public SparkleFilter() {
		randomNumbers = new Random();
	}

	/**
	 * Sets a color.
	 * 
	 * @param color
	 */
	public void setColor(int color) {
		this.color = color;
	}

	/**
	 * Gets a color.
	 * 
	 * @return
	 */
	public int getColor() {
		return color;
	}

	
	/**
	 * Sets a randomness.
	 * 
	 * @param randomness
	 */
	public void setRandomness(int randomness) {
		this.randomness = randomness;
	}

	/**
	 * Gets a randomness.
	 * 
	 * @return
	 */
	public int getRandomness() {
		return randomness;
	}

	/**
	 * Set the amount of sparkle.
	 * 
	 * @param amount - the amount
	 * @min-value 0
	 * @max-value 1
	 * @see #getAmount
	 */
	public void setAmount(int amount) {
		Validator.validateIntInRange(amount, 0, 1);
		this.amount = amount;
	}
	
	/**
	 * Get the amount of sparkle.
	 * 
	 * @return the amount
	 * @see #setAmount
	 */
	public int getAmount() {
		return amount;
	}
	
	/**
	 * Sets the rays.
	 * 
	 * @param rays
	 * @see #getRays()
	 */
	public void setRays(int rays) {
		this.rays = rays;
	}

	/**
	 * Gets the rays.
	 * 
	 * @return
	 * @see #setRadius(int)
	 */
	public int getRays() {
		return rays;
	}

	/**
	 * Set the radius of the effect.
	 * 
	 * @param radius - the radius
	 * @min - value 0
	 * @see #getRadius
	 */
	public void setRadius(int radius) {
		Validator.validateFloatInRange(radius, 0, Float.MAX_VALUE);
		this.radius = radius;
	}

	/**
	 * Get the radius of the effect.
	 * 
	 * @return the radius
	 * @see #setRadius
	 */
	public int getRadius() {
		return radius;
	}

	
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
		centreX = width/2;
		centreY = height/2;
		super.setDimensions(width, height);
		randomNumbers.setSeed(seed);
		rayLengths = new float[rays];
		for (int i = 0; i < rays; i++)
			rayLengths[i] = radius + randomness / 100.0f * radius * (float)randomNumbers.nextGaussian();
	}
	
	public int filterRGB(int x, int y, int rgb) {
		float dx = x - centreX;
		float dy = y - centreY;
		float distance = dx * dx + dy * dy;
		float angle = (float) Math.atan2(dy, dx);
		float d = (angle + ImageMath.PI) / (ImageMath.TWO_PI) * rays;
		int i = (int) d;
		float f = d - i;

		if (radius != 0) {
			float length = ImageMath.lerp(f, rayLengths[i % rays], rayLengths[(i + 1) % rays]);
			float g = length * length / (distance + 0.0001f);
			g = (float) Math.pow(g, (100 - amount) / 50.0);
			f -= 0.5f;
			f = 1 - f * f;
			f *= g;
		}
		f = ImageMath.clamp(f, 0, 1);
		return ImageMath.mixColors(f, rgb, color);
	}

	public String toString() {
		return this.getClass().getSimpleName();
	}
}
