package com.ocrix.image.processing.moment;

import java.awt.image.BufferedImage;

public class Moments {
	private static final int BACKGROUND = 0;
	private Moments (){}
	
	public static double moment(BufferedImage bi, int p, int q){
		double mpq = 0.0d;
		for(int v = 0; v < bi.getHeight(); v++){
			for(int u = 0; u < bi.getWidth(); u++){
				if(bi.getRGB(u, v) != BACKGROUND){
					mpq += Math.pow(u, p) * Math.pow(v, q);
				}
			}
		}
		return mpq;
	}
	
	public static double centralMoment(BufferedImage bi, int p, int q){
		/* Region area */
		double m00 = moment(bi, 0, 0);
		double xCtr = moment(bi, 1, 0) / m00;
		double yCtr = moment(bi, 0, 1) / m00;
		double cMpq = 0.0d;
		for(int v =0; v < bi.getHeight(); v++){
			for(int u = 0; u < bi.getWidth(); u++){
				if(bi.getRGB(u, v) != BACKGROUND){
					cMpq += Math.pow(u - xCtr, p) * Math.pow(v - yCtr, p);
				}
			}
		}
		return cMpq;
	}
	
	public static double normalCentralMoment(BufferedImage bi, int p, int q){
		double m00 = moment(bi, 0, 0);
		double norm = Math.pow(m00, (p + q + 2) / 2);
		return centralMoment(bi, p, q) / norm;
	}

}
