/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;


import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import com.ocrix.image.processing.filters.Validator;
import com.ocrix.image.processing.filters.composite.MiscComposite;


/**
 * A filter which produces the effect of light rays shining out of an image. <br>
 * Defaults are:
 * <ul>
 * <li>opacity = <code>1.0f</code></li>
 * <li>threshold = <code>0.0f</code></li>
 * <li>strength = <code>0.5f</code></li>
 * <li>raysOnly = <code>false</code></li>
 * </ul>
 */
public class RaysFilter extends MotionBlurOp {
	private float opacity = 1.0f;
	private float threshold = 0.0f;
	private float strength = 0.5f;
	private boolean raysOnly = false;
	private Colormap colormap;

	/**
	 * Constructs a {@link RaysFilter} with its defaults.
	 */
	public RaysFilter() {
	}

	/**
	 * Sets the opacity of the rays.
	 * 
	 * @param opacity
	 *            - the opacity.
	 * @see #getOpacity
	 */
	public void setOpacity(float opacity) {
		this.opacity = opacity;
	}

	/**
	 * Gets the opacity of the rays.
	 * 
	 * @return the opacity.
	 * @see #setOpacity
	 */
	public float getOpacity() {
		return opacity;
	}

	/**
	 * Sets the threshold value.
	 * 
	 * @param threshold
	 *            - the threshold value
	 * @see #getThreshold
	 */
	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

	/**
	 * Gets the threshold value.
	 * 
	 * @return the threshold value
	 * @see #setThreshold
	 */
	public float getThreshold() {
		return threshold;
	}

	/**
	 * Sets the strength of the rays.
	 * 
	 * @param strength
	 *            - the strength.
	 * @see #getStrength
	 */
	public void setStrength(float strength) {
		this.strength = strength;
	}

	/**
	 * Gets the strength of the rays.
	 * 
	 * @return the strength.
	 * @see #setStrength
	 */
	public float getStrength() {
		return strength;
	}

	/**
	 * Sets whether to render only the rays.
	 * 
	 * @param raysOnly
	 *            - <code>true</code> to render rays only.
	 * @see #getRaysOnly
	 */
	public void setRaysOnly(boolean raysOnly) {
		this.raysOnly = raysOnly;
	}

	/**
	 * Gets whether to render only the rays.
	 * 
	 * @return <code>true</code> to render rays only.
	 * @see #setRaysOnly
	 */
	public boolean getRaysOnly() {
		return raysOnly;
	}

	/**
	 * Sets the colormap to be used for the filter.
	 * 
	 * @param colormap
	 *            - the colormap
	 * @see #getColormap
	 */
	public void setColormap(Colormap colormap) {
		this.colormap = colormap;
	}

	/**
	 * Gets the colormap to be used for the filter.
	 * 
	 * @return the colormap
	 * @see #setColormap
	 */
	public Colormap getColormap() {
		return colormap;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		int width = src.getWidth();
		int height = src.getHeight();
		int[] pixels = new int[width];
		int[] srcPixels = new int[width];

		BufferedImage rays = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		int threshold3 = (int) (threshold * 3 * 255);
		for (int y = 0; y < height; y++) {
			getRGB(src, 0, y, width, 1, pixels);
			for (int x = 0; x < width; x++) {
				int rgb = pixels[x];
				int a = rgb & 0xff000000;
				int r = (rgb >> 16) & 0xff;
				int g = (rgb >> 8) & 0xff;
				int b = rgb & 0xff;
				int l = r + g + b;
				
				if (l < threshold3) {
					pixels[x] = 0xff000000;
				}
				else {
					l /= 3;
					pixels[x] = a | (l << 16) | (l << 8) | l;
				}
			}
			
			setRGB(rays, 0, y, width, 1, pixels);
		}

		rays = super.filter(rays, null);

		for (int y = 0; y < height; y++) {
			getRGB(rays, 0, y, width, 1, pixels);
			getRGB(src, 0, y, width, 1, srcPixels);
			for (int x = 0; x < width; x++) {
				int rgb = pixels[x];
				int a = rgb & 0xff000000;
				int r = (rgb >> 16) & 0xff;
				int g = (rgb >> 8) & 0xff;
				int b = rgb & 0xff;

				if (colormap != null) {
					int l = r + g + b;
					rgb = colormap.getColor(l * strength * (1 / 3f));
				} else {
					r = PixelUtils.clamp((int) (r * strength));
					g = PixelUtils.clamp((int) (g * strength));
					b = PixelUtils.clamp((int) (b * strength));
					rgb = a | (r << 16) | (g << 8) | b;
				}

				pixels[x] = rgb;
			}
			
			setRGB(rays, 0, y, width, 1, pixels);
		}

		if (dst == null) {
			dst = createCompatibleDestImage(src, null);
		}

		Graphics2D g = dst.createGraphics();
		
		if (!raysOnly) {
			g.setComposite(AlphaComposite.SrcOver);
			g.drawRenderedImage(src, null);
		}
		
		g.setComposite(MiscComposite.getInstance(MiscComposite.ADD, opacity));
		g.drawRenderedImage(rays, null);
		g.dispose();

		return dst;
	}

	public String toString() {
		return this.getClass().getName();
	}
}
