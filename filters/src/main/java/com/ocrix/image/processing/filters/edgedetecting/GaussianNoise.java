/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import java.util.Random;
import com.ocrix.image.processing.filters.Validator;

/**
 * Defines a gaussian noise generator.
 */
public class GaussianNoise {
	/* Class member declarations */
	private int[] input;
	private int[] output;
	private int progress;
	private float variance;
	private int width;
	private int height;
	private Random randgen;

	public void gaussianNoise() {
		progress = 0;
	}

	public void init(int[] original, int widthIn, int heightIn, float varianceIn) {
		Validator.validateIntArrayEmptiness(original);
		Validator.validateIntNotZero(widthIn, heightIn);
		randgen = new Random();
		variance = varianceIn;
		width = widthIn;
		height = heightIn;
		input = new int[width * height];
		output = new int[width * height];
		input = original;
	}

	public int[] process() {
		double rand;
		int result = 0;

		for (int x = 0; x < width; x++) {
			progress++;
			for (int y = 0; y < height; y++) {

				// Generate amount to be added
				rand = (variance * (randgen.nextGaussian()));
				result = (int) ((input[y * width + x] & 0xff) + rand);

				// Clip final result
				if (result < 0) {
					result = 0;
				} else if (result > 255) {
					result = 255;
				}

				// Convert back to grayscale pixel
				output[y * width + x] = 0xff000000 | (result + (result << 16) + (result << 8));
			}
		}
		return output;
	}

	public int getProgress() {
		return progress;
	}
}
