/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

/**
 * A simple embossing filter. Image embossing is a computer graphics technique
 * in which each pixel of an image is replaced either by a highlight or a
 * shadow, depending on light/dark boundaries on the original image. Low
 * contrast areas are replaced by a gray background. The filtered image will
 * represent the rate of color change at each location of the original image.
 * Applying an embossing filter to an image often results in an image resembling
 * a paper or metal embossing of the original image, hence the name. An emboss
 * filter gives a 3D shadow effect to the image, the result is very useful for a
 * bumpmap of the image. Emboss Matrix looks like this:
 * 
 * <pre>
 *       [ -1.0  -1.0  0.0 ]
 *       [ -1.0   1.0  1.0 ]
 *       [  0.0   1.0  1.0 ]
 * </pre>
 */
public class BumpFilter extends ConvolveFilter {

	private static float[] embossMatrix = { -1.0f, -1.0f, 0.0f, -1.0f, 1.0f,
			1.0f, 0.0f, 1.0f, 1.0f };

	public BumpFilter() {
		super(embossMatrix);
	}

	public String toString() {
		return this.getClass().getName();
	}
}
