/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.image.*;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.ImageComparator;
import com.ocrix.image.processing.filters.Validator;

/**
 * An abstract superclass for point filters. The interface is the same as the
 * old RGBImageFilter.
 */
public abstract class PointFilter extends AbstractBufferedImageOp {

	protected boolean canFilterIndexColorModel = false;

	/**
	 * 
	 */
	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		int width = 0;
		int height = 0;
		int type = src.getType();

		if (dst == null) {
			dst = createCompatibleDestImage(src, null);
		} else {
			// checks which image is bigger & resize
			ImageComparator ic = new ImageComparator();
			if (ic.compare(src, dst) == 1) {
				// src > dst
				dst = CommonUtils.resizeImage(dst, src.getWidth(),
						src.getHeight(), src.getType());
				width = src.getWidth();
				height = src.getHeight();

			} else if (ic.compare(src, dst) == -1) {
				// src < dst
				src = CommonUtils.resizeImage(src, dst.getWidth(),
						dst.getHeight(), dst.getType());
				width = dst.getWidth();
				height = dst.getHeight();
			} else {
				width = dst.getWidth();
				height = dst.getHeight();
			}
		}

		WritableRaster srcRaster = src.getRaster();
		WritableRaster dstRaster = dst.getRaster();

		setDimensions(width, height);

		int[] inPixels = src.getRaster()
				.getPixel(0, 0, new int[src.getWidth()]);

		for (int y = 0; y < height; y++) {
			// We try to avoid calling getRGB on images as it causes them to
			// become unmanaged, causing horrible performance problems.
			if (type == BufferedImage.TYPE_INT_ARGB) {
				srcRaster.getDataElements(0, y, width, 1, inPixels);
				for (int x = 0; x < width; x++) {
					inPixels[x] = filterRGB(x, y, inPixels[x]);
				}
				dstRaster.setDataElements(0, y, width, 1, inPixels);
			} else {
				src.getRGB(0, y, width, 1, inPixels, 0, width);
				for (int x = 0; x < width; x++) {
					inPixels[x] = filterRGB(x, y, inPixels[x]);
				}
				dst.setRGB(0, y, width, 1, inPixels, 0, width);
			}
		}

		return dst;
	}

	public void setDimensions(int width, int height) {
	}

	public abstract int filterRGB(int x, int y, int rgb);
}
