/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.awt.image.ImageProducer;
import java.awt.image.Raster;
import java.util.logging.Logger;

import com.ocrix.image.processing.filters.image.Curve;
import com.ocrix.image.processing.filters.vecmath.AxisAngle4f;

/**
 * Defines a set of validation methods.
 */
public final class Validator {
	/* Local logger */
	private static final Logger log = Logger.getLogger(Validator.class.getSimpleName());

	private Validator() {
	}

	/**
	 * validates a {@link ColorModel} is not null.
	 * 
	 * @param colorModel
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateColorModel(ColorModel colorModel) {
		if (colorModel == null) {
			throw new IllegalArgumentException(
					ColorModel.class.getCanonicalName() + " should not be NULL");
		}
	}

	/**
	 * Validates each element of color model array in not null.
	 * 
	 * @param colorModels
	 *            to be validated.
	 */
	public static void validateColorModel(ColorModel... colorModels) {
		if (colorModels != null) {
			for (ColorModel cm : colorModels) {
				if (cm == null) {
					throw new IllegalArgumentException(
							ColorModel.class.getCanonicalName()
									+ " should not be NULL");
				}
			}
		} else {
			log.warning(ColorModel.class.getName() + " array is null");
		}
	}

	/**
	 * Validates {@link BufferedImage} is not null.
	 * 
	 * @param bufImg
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateBufferedImage(BufferedImage bufImg) {
		if (bufImg == null) {
			throw new IllegalArgumentException(
					BufferedImage.class.getCanonicalName() + " is NULL");
		}
	}

	/**
	 * Validates {@link BufferedImage} array.
	 * 
	 * @param bufImgs
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criteria.
	 */
	public static void validateBufferedImage(BufferedImage... bufImgs) {
		if (bufImgs != null) {
			for (BufferedImage bufImg : bufImgs) {
				if (bufImg == null) {
					throw new IllegalArgumentException(BufferedImage.class.getCanonicalName() + " is NULL");
				}
			}
		} else {
			log.warning(BufferedImage.class.getName() + " array is null");
		}
	}

	/**
	 * Validates that size of two given images is equal.
	 * 
	 * @param src
	 * @param dest
	 */
	public static void validateSize(BufferedImage src, BufferedImage dest) {
		validateBufferedImage(src, dest);
		if ((src.getHeight() != dest.getHeight())
				&& (src.getWidth() != dest.getWidth())) {
			throw new IllegalArgumentException(
					"the size of src_image is not equal to the size of dest_image, please rescale one of them");
		}
	}

	public static void validatePoint2D(Point2D srcPoint2D) {
		if (srcPoint2D == null) {
			throw new IllegalArgumentException(Point2D.class.getCanonicalName()
					+ " is NULL");
		}
	}

	/**
	 * Validates a set of {@link Point2D}s.
	 * 
	 * @param srcPoint2Ds
	 *            to be validated, one by one.
	 * 
	 * 
	 */
	public static void validatePoint2D(Point2D... srcPoint2Ds) {
		for (Point2D srcPoint2D : srcPoint2Ds) {
			if (srcPoint2D == null) {
				throw new IllegalArgumentException(
						Point2D.class.getCanonicalName() + " is NULL");
			}
		}
	}

	/**
	 * Checks if alpha was between [0.0f and 1.0f]
	 * 
	 * @param alpha
	 */
	public static void validateAlpha(float alpha) {
		if ((alpha < 0.0f) || (alpha > 1)) {
			throw new IllegalArgumentException(
					"alpha value out of range, must be between 0 and 1");
		}
	}

	/**
	 * Checks if rule was in range of possible values
	 * 
	 * @param rule
	 * @param minVal
	 * @param maxVal
	 */
	public static void validateRule(int rule, int minVal, int maxVal) {
		if (rule < minVal || rule > maxVal) {
			throw new IllegalArgumentException("unknown composite rule");
		}
	}

	/**
	 * Checks if raster was not null
	 * 
	 * @param raster
	 */
	public static void validateRaster(Raster raster) {
		if (raster == null) {
			throw new IllegalArgumentException(Raster.class.getCanonicalName()
					+ " is NULL");
		}
	}

	/**
	 * Checks if any element of rasters[] was not null
	 * 
	 * @param rasters
	 */
	public static void validateRaster(Raster... rasters) {
		if (rasters != null) {
			for (Raster raster : rasters) {
				if (raster == null) {
					throw new IllegalArgumentException(
							Raster.class.getCanonicalName() + " is NULL");
				}
			}
		} else {
			log.warning(Raster.class.getName() + " array is null");
		}
	}

	/**
	 * Checks of int[] was not null and its length was bigger than zero
	 * 
	 * @param array
	 */
	public static void validateIntArrayEmptiness(int[] array) {
		if (array == null || array.length == 0) {
			throw new IllegalArgumentException("int[] is empty");
		}
	}

	/**
	 * Checks if any element of String[] was not null.
	 * 
	 * @param strings
	 */
	public static void validateStringArray(String... strings) {
		if (strings != null) {
			for (String str : strings) {
				if (str == null) {
					throw new IllegalArgumentException(
							String.class.getCanonicalName() + " is NULL");
				}
			}
		} else {
			log.warning(String.class.getName() + " array is null");
		}
	}

	/**
	 * Checks if int value was positive.
	 * 
	 * @param value
	 */
	public static void validateIntPositiveness(int value) {
		if (value < 0) {
			throw new IllegalArgumentException("int is negative");
		}
	}

	/**
	 * Checks if an int variable was in range.
	 * 
	 * @param toBeValidated
	 * @param min
	 * @param max
	 */
	public static void validateIntInRange(int toBeValidated, int min, int max) {
		if (toBeValidated < min || toBeValidated > max) {
			throw new IllegalArgumentException("int must be between [" + min
					+ " ... " + max + "], current value is " + toBeValidated);
		}
	}

	/**
	 * Checks if ints ware positive, i.e. >= 0
	 * 
	 * @param values
	 */
	public static void validateIntPositiveness(int... values) {
		if (values != null) {
			for (int num : values) {
				if (num < 0) {
					throw new IllegalArgumentException("int is negative");
				}
			}
		} else {
			log.severe(Integer.class.getName() + " array is null");
		}
	}

	/**
	 * Validates the a float number is positive.
	 * 
	 * @param value
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateFloatPositiveness(float value) {
		if (value < 0) {
			throw new IllegalArgumentException("float is negative");
		}
	}

	/**
	 * Checks if a first number is greater than second.
	 * If so, the {@link IllegalArgumentException} thrown.
	 * 
	 * @param first to be compared.
	 * @param second to be compared.
	 */
	public static void compareFirstIsGreater(int first, int second){
		if(first > second){
			throw new IllegalArgumentException("The offset cannot be greater than count");
		}
	}
	
	/**
	 * Validates a float number is in range.
	 * 
	 * @param toBeChecked
	 *            to be tested.
	 * @param min
	 *            a lower bound.
	 * @param max
	 *            an upper bound.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criteria.
	 */
	public static void validateFloatInRange(float toBeChecked, float min,
			float max) {
		if (toBeChecked < min || toBeChecked > max) {
			throw new IllegalArgumentException("float must be between [" + min
					+ " ... " + max + "], currently is " + toBeChecked);
		}
	}

	/**
	 * Takes a set of float numbers and tests each if it is positive.
	 * 
	 * @param values
	 *            to be tested.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateFloatPositiveness(float... values) {
		for (float flt : values) {
			if (flt < 0) {
				throw new IllegalArgumentException("float is negative");
			}
		}
	}

	/**
	 * Validates if an int is not a zero.
	 * 
	 * @param number
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateIntNotZero(int number) {
		if (number == 0) {
			throw new IllegalArgumentException("int is zero");
		}
	}

	/**
	 * Validates if a set of ints is not a zero.
	 * 
	 * @param numbers
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criteria.
	 */
	public static void validateIntNotZero(int... numbers) {
		for (int number : numbers) {
			if (number == 0) {
				throw new IllegalArgumentException("int is zero");
			}
		}
	}

	/**
	 * Validates a rectangle is not null.
	 * 
	 * @param rec
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateRectangle(Rectangle rec) {
		if (rec == null) {
			throw new IllegalArgumentException(
					Rectangle.class.getCanonicalName() + " is null");
		}
	}

	/**
	 * Validates if a double is not zero.
	 * 
	 * @param number
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateDouble(double number) {
		if (number == 0) {
			throw new IllegalArgumentException("double is zero");
		}
	}

	/**
	 * Validates a set of doubles is not equals to zero.
	 * 
	 * @param numbers
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateDouble(double... numbers) {
		for (Double number : numbers) {
			if (number == 0) {
				throw new IllegalArgumentException("double is zero");
			}
		}
	}

	/**
	 * Validates {@link Composite} if it was null.
	 * 
	 * @param comp
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateComposite(Composite comp) {
		if (comp == null) {
			throw new IllegalArgumentException(
					Composite.class.getCanonicalName() + " is null");
		}
	}

	/**
	 * Validates if an {@link AffineTransform} was not a null
	 * 
	 * @param transform
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateAffineTransform(AffineTransform transform) {
		if (transform == null) {
			throw new IllegalArgumentException(
					AffineTransform.class.getCanonicalName() + " is null");
		}
	}

	/**
	 * Validates {@link BufferedImageOp} if it was not null
	 * 
	 * @param op
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateBufferedImageOp(BufferedImageOp op) {
		if (op == null) {
			throw new IllegalArgumentException(
					BufferedImageOp.class.getCanonicalName() + " is null");
		}
	}

	/**
	 * Validates array of {@link BufferedImageOp} for the nullity
	 * 
	 * @param ops
	 */
	public static void validateBufferedImageOp(BufferedImageOp... ops) {
		for (BufferedImageOp op : ops) {
			if (op == null) {
				throw new IllegalArgumentException(
						BufferedImageOp.class.getCanonicalName() + " is null");
			}
		}
	}

	/**
	 * Validates a Curve is not null.
	 * 
	 * @param curve
	 *            to be validated.
	 * 
	 *            throws IllegalArgumentException if a parameter does not
	 *            satisfy the criterion.
	 */
	public static void validateCurve(Curve curve) {
		if (curve == null) {
			throw new IllegalArgumentException(Curve.class.getCanonicalName()
					+ " is null");
		}
	}

	/**
	 * Validates byte[] for non-nullities and non-emptiness
	 * 
	 * @param arrayToBeValidated
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateByteArray(byte[] arrayToBeValidated) {
		if (arrayToBeValidated == null || arrayToBeValidated.length < 1) {
			throw new IllegalArgumentException(byte.class.getCanonicalName()
					+ " array is null");
		}
	}

	/**
	 * Validates an image is not null
	 * 
	 * @param imageToBeValidated
	 *            - {@link Image}.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateImage(Image imageToBeValidated) {
		if (imageToBeValidated == null) {
			throw new IllegalArgumentException(Image.class.getCanonicalName()
					+ " is null");
		}
	}

	/**
	 * Validates an array of images
	 * 
	 * @param images
	 *            - an array of images.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateImage(Image... images) {
		for (Image image : images) {
			if (image == null) {
				throw new IllegalArgumentException(
						Image.class.getCanonicalName() + " is null");
			}
		}
	}

	/**
	 * Validates an {@link ImageProducer} is not null.
	 * 
	 * @param producer
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateImageProducer(ImageProducer producer) {
		if (producer == null) {
			throw new IllegalArgumentException(
					ImageProducer.class.getCanonicalName() + " is null");
		}
	}

	/**
	 * Validates a {@link Component} is not null.
	 * 
	 * @param canvas
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateComponent(Component canvas) {
		if (canvas == null) {
			throw new IllegalArgumentException(
					Component.class.getCanonicalName() + " is null");
		}
	}// Graphics

	/**
	 * Validates a {@link Graphics} is not null.
	 * 
	 * @param graphics
	 *            to be validated.
	 * 
	 * @throws IllegalArgumentException
	 *             if a parameter does not satisfy the criterion.
	 */
	public static void validateGraphics(Graphics graphics) {
		if (graphics == null) {
			throw new IllegalArgumentException(
					Graphics.class.getCanonicalName() + " is null");
		}
	}
	
	/**
	 * Validates a float array is not null.
	 * 
	 * @param fa to be validated.
	 * 
	 * @throws @{@link IllegalArgumentException}. 
	 */
	public static void validateFloatArrayIsNotNull(float[] fa){
		if(fa == null){
			log.severe("float[] is null");
			throw new IllegalArgumentException("float[] is null");
		}
	}
	
	/**
	 * Validates the float array if its size requires the desired.
	 * 
	 * @param fa to be checked.
	 * @param desiredSize
	 */
	public static void validateFloatArrayLenght(float[] fa, int desiredSize){
		if(fa == null || fa.length != desiredSize){
			log.severe("float[] is null or lenght is not as expected.");
			throw new IllegalArgumentException("float[] is null");
		}
	}
	
	/**
	 * Validates {@link AxisAngle4f} not to be null.
	 * 
	 * @param aa4f to be validated.
	 * 
	 * @throws @{@link IllegalArgumentException} if an arg is null.
	 */
	public static void validateAxisAngle4f(AxisAngle4f aa4f){
		if(aa4f == null){
			log.severe(AxisAngle4f.class.getName() + " is null");
			throw new IllegalArgumentException(AxisAngle4f.class.getName() + " is null");
		}
	}
	
	/**
	 * Checks if two numbers are equals each other, and if so
	 * {@link IllegalArgumentException} is thrown.
	 * 
	 * @param first
	 * @param second
	 */
	public static void isEquals(int first, int second){
		if(first == second){
			throw new IllegalArgumentException("The number should be different.");
		}
	}
	
	/**
	 * 
	 * @param array2D
	 */
	public static void notNull(int[][] array2D) {
		if(array2D == null){
			throw new IllegalArgumentException("int[][] array2D should not be null.");
		}
	}
}
