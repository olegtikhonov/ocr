/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

/**
 * A filter which averages the 3x3 neighborhood of each pixel, providing a
 * simple blur.
 * The convolution matrix looks like that:
 * <pre>
 *     [ 0.1  0.1  0.1 ]
 *     [ 0.1  0.2  0.1 ]
 *     [ 0.1  0.1  0.1 ]
 * </pre>
 * 
 * Have a look at <a href="http://docs.gimp.org/en/plug-in-convmatrix.html">an example</a>
 */
public class AverageFilter extends ConvolveFilter {

	/**
	 * The convolution kernel for the averaging.
	 */
	protected static float[] theMatrix = { 0.1f, 0.1f, 0.1f, 0.1f, 0.2f, 0.1f,
			0.1f, 0.1f, 0.1f };

	public AverageFilter() {
		super(theMatrix);
	}

	/**
	 * Overridden of toString. Returns "Blur/Average Blur";
	 */
	public String toString() {
		return this.getClass().getName();
	}
}
