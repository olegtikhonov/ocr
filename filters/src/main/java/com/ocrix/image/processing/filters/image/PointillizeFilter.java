/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import com.ocrix.image.processing.filters.Validator;


/**
 * Draws an image as a series of spots. You can achieve a variety of effects by
 * changing the spot size, layout and fuzziness. You can also cause the spots to
 * be faded smoothly between each other which produces star or painting effects.
 * <br>
 * Defaults are:
 * <ul>
 * <li>edgeThickness = <code>0.4f</code></li>
 * <li>fadeEdges = <code>false</code></li>
 * <li>edgeColor = <code>0xff000000</code></li>
 * <li>fuzziness = <code>0.1f</code></li>
 * <li>scale = <code>16</code></li>
 * <li>randomness = <code>0.0f</code></li>
 * </ul>
 */
public class PointillizeFilter extends CellularFilter {
	private float edgeThickness = 0.4f;
	private boolean fadeEdges = false;
	private int edgeColor = 0xff000000;
	private float fuzziness = 0.1f;

	
	/**
	 * Creates a {@link PointillizeFilter}.
	 * Defaults are:
	 * scale = <code>16</code>, randomness = <code>0.0f</code>.
	 */
	public PointillizeFilter() {
		setScale(16);
		setRandomness(0.0f);
	}

	
	/**
	 * Sets edge thickness.
	 * 
	 * @param edgeThickness
	 */
	public void setEdgeThickness(float edgeThickness) {
		this.edgeThickness = edgeThickness;
	}

	
	/**
	 * Gets edge thickness.
	 * 
	 * @return
	 */
	public float getEdgeThickness() {
		return edgeThickness;
	}

	
	/**
	 * Sets a fade edges, i.e. a possibility to get fade edges.
	 * 
	 * @param fadeEdges <code>true</code> if use fade edges.
	 */
	public void setFadeEdges(boolean fadeEdges) {
		this.fadeEdges = fadeEdges;
	}

	/**
	 * Gets a boolean value of fade edges.
	 * 
	 * @return
	 */
	public boolean getFadeEdges() {
		return fadeEdges;
	}

	/**
	 * Sets the edge color.
	 * 
	 * @param edgeColor to be set as an edge color.
	 */
	public void setEdgeColor(int edgeColor) {
		this.edgeColor = edgeColor;
	}

	/**
	 * Gets the edge color.
	 * 
	 * @return
	 */
	public int getEdgeColor() {
		return edgeColor;
	}

	/**
	 * Sets a fuzziness.
	 * 
	 * @param fuzziness
	 */
	public void setFuzziness(float fuzziness) {
		this.fuzziness = fuzziness;
	}

	/**
	 * Gets a fuzziness.
	 * 
	 * @return
	 */
	public float getFuzziness() {
		return fuzziness;
	}

	/**
	 * Gets pixel from particular position.
	 * 
	 * @param x top bottom right corner, the beginning.
	 * @param y top bottom right corner.
	 * @param inPixels the array of the area of the image.
	 * @param width of the image.
	 * @param height of the image.
	 * 
	 * @return a pixel.
	 * 
	 * @throws IllegalArgumentException if inPixels is null.
	 */
	public int getPixel(int x, int y, int[] inPixels, int width, int height) {
		Validator.validateIntArrayEmptiness(inPixels);
		float nx = m00 * x + m01 * y;
		float ny = m10 * x + m11 * y;
		nx /= scale;
		ny /= scale * stretch;
		nx += 1000;
		ny += 1000; // Reduce artifacts around 0,0
		float f = evaluate(nx, ny);

		float f1 = results[0].distance;
		int srcx = ImageMath.clamp((int) ((results[0].x - 1000) * scale), 0,
				width - 1);
		int srcy = ImageMath.clamp((int) ((results[0].y - 1000) * scale), 0,
				height - 1);
		
		int v = (srcy * width + srcx < inPixels.length) ? inPixels[srcy * width + srcx] : 0;

		if (fadeEdges) {
			float f2 = results[1].distance;
			srcx = ImageMath.clamp((int) ((results[1].x - 1000) * scale), 0,
					width - 1);
			srcy = ImageMath.clamp((int) ((results[1].y - 1000) * scale), 0,
					height - 1);
			int v2 = inPixels[srcy * width + srcx];
			v = ImageMath.mixColors(0.5f * f1 / f2, v, v2);
		} else {
			f = 1 - ImageMath.smoothStep(edgeThickness, edgeThickness
					+ fuzziness, f1);
			v = ImageMath.mixColors(f, edgeColor, v);
		}
		return v;
	}

	public String toString() {
		return this.getClass().getName();
	}
}
