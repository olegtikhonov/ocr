/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import java.awt.Composite;
import java.awt.CompositeContext;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import com.ocrix.image.processing.filters.Validator;



public abstract class RGBComposite implements Composite {

    protected float extraAlpha;

    public RGBComposite() {
	this(1.0f);
    }

    public RGBComposite(float alpha) {
	Validator.validateAlpha(alpha);
	this.extraAlpha = alpha;
    }

    public float getAlpha() {
	return extraAlpha;
    }

    @Override
    public int hashCode() {
	return Float.floatToIntBits(extraAlpha);
    }

    @Override
    public boolean equals(Object o) {
	if (!(o instanceof RGBComposite)) {
	    return false;
	}
	RGBComposite c = (RGBComposite) o;

	if (extraAlpha != c.extraAlpha) {
	    return false;
	}
	return true;
    }

    public abstract static class RGBCompositeContext implements
	    CompositeContext {

	private final float alpha;
	private ColorModel srcColorModel;
	private ColorModel dstColorModel;

	/**
	 * Creates {@link RGBCompositeContext}
	 * 
	 * @param alpha
	 * @param srcColorModel
	 *            should not be NULL, otherwise thrown an
	 *            {@link IllegalArgumentException}
	 * @param dstColorModel
	 *            should not be NULL, otherwise thrown an
	 *            {@link IllegalArgumentException}
	 */
	public RGBCompositeContext(float alpha, ColorModel srcColorModel,
	        ColorModel dstColorModel) {
	    Validator.validateColorModel(srcColorModel, dstColorModel);
	    this.alpha = alpha;
	    this.setSrcColorModel(srcColorModel);
	    this.setDstColorModel(dstColorModel);
	}

	public void dispose() {
	}

	// Multiply two numbers in the range 0..255 such that 255*255=255
	protected static int multiply255(int a, int b) {
	    int t = a * b + 0x80;
	    return ((t >> 8) + t) >> 8;
	}

	protected static int clamp(int a) {
	    return a < 0 ? 0 : a > 255 ? 255 : a;
	}

	public abstract void composeRGB(int[] src, int[] dst, float alpha);

	public void compose(Raster src, Raster dstIn, WritableRaster dstOut) {
	    float alpha = this.alpha;

	    int[] srcPix = null;
	    int[] dstPix = null;

	    int x = dstOut.getMinX();
	    int w = dstOut.getWidth();
	    int y0 = dstOut.getMinY();
	    int y1 = y0 + dstOut.getHeight();

	    for (int y = y0; y < y1; y++) {
		srcPix = src.getPixels(x, y, w, 1, srcPix);
		dstPix = dstIn.getPixels(x, y, w, 1, dstPix);
		composeRGB(srcPix, dstPix, alpha);
		dstOut.setPixels(x, y, w, 1, dstPix);
	    }
	}

	private void setSrcColorModel(ColorModel srcColorModel) {
	    this.srcColorModel = srcColorModel;
	}

	protected ColorModel getSrcColorModel() {
	    return srcColorModel;
	}

	private void setDstColorModel(ColorModel dstColorModel) {
	    this.dstColorModel = dstColorModel;
	}

	protected ColorModel getDstColorModel() {
	    return dstColorModel;
	}
    }
}
