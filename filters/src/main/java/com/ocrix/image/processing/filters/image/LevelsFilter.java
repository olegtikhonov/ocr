/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.Rectangle;

import com.ocrix.image.processing.filters.image.Histogram.COLORS;

/**
 * A filter which allows levels adjustment on an image.
 * 
 * <p>Parameters:</p>
 * <code><ul>
 * <li>float lowLevel - The low input level, default is 0.</li>
 * <li>float highLevel - The high input level, default is 1.</li>
 * <li>float lowOutputLevel - The low output level, default is 0.</li>
 * <li>float highOutputLevel - The high output level, default is 1.</li>
 * </code><ul>
 */
public class LevelsFilter extends WholeImageFilter {

	private int[][] lut;
	private float lowLevel = 0;
	private float highLevel = 1;
	private float lowOutputLevel = 0;
	private float highOutputLevel = 1;

	/**
	 * Constructs a {@link LevelsFilter}.
	 */
	public LevelsFilter() {
	}

	/**
	 * Sets a low level.
	 * 
	 * @param lowLevel
	 *            to be set.
	 */
	public void setLowLevel(float lowLevel) {
		this.lowLevel = lowLevel;
	}

	/**
	 * Gets a low level.
	 * 
	 * @return low level.
	 */
	public float getLowLevel() {
		return lowLevel;
	}

	/**
	 * Sets an high level.
	 * 
	 * @param highLevel
	 *            to be set.
	 */
	public void setHighLevel(float highLevel) {
		this.highLevel = highLevel;
	}

	/**
	 * Gets an high level.
	 * 
	 * @return the high level.
	 */
	public float getHighLevel() {
		return highLevel;
	}

	/**
	 * Sets a low output level.
	 * 
	 * @param lowOutputLevel
	 *            to be set.
	 */
	public void setLowOutputLevel(float lowOutputLevel) {
		this.lowOutputLevel = lowOutputLevel;
	}

	/**
	 * Gets a low output level.
	 * 
	 * @return the low output level.
	 */
	public float getLowOutputLevel() {
		return lowOutputLevel;
	}

	/**
	 * Sets an high output level.
	 * 
	 * @param highOutputLevel
	 *            to be set.
	 */
	public void setHighOutputLevel(float highOutputLevel) {
		this.highOutputLevel = highOutputLevel;
	}

	/**
	 * Gets an high output level.
	 * 
	 * @return the high output level.
	 */
	public float getHighOutputLevel() {
		return highOutputLevel;
	}

	/**
	 * Filters pixels out.
	 * 
	 * @param width
	 *            of the image.
	 * @param height
	 *            of the image.
	 * @param inPixels
	 *            of the area.
	 * @param transformedSpace
	 *            on which area will be applied.
	 */
	protected int[] filterPixels(int width, int height, int[] inPixels,
			Rectangle transformedSpace) {
		Histogram histogram = new Histogram(inPixels, width, height, 0, width);

		int i, j;

		if (histogram.getNumSamples() > 0) {
			// float scale = 255.0f / histogram.getNumSamples();
			lut = new int[3][256];

			float low = lowLevel * 255;
			float high = highLevel * 255;
			if (low == high)
				high++;
			for (i = 0; i < 3; i++) {
				for (j = 0; j < 256; j++)
					lut[i][j] = PixelUtils
							.clamp((int) (255 * (lowOutputLevel + (highOutputLevel - lowOutputLevel)
									* (j - low) / (high - low))));
			}
		} else
			lut = null;

		i = 0;
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++) {
				inPixels[i] = filterRGB(x, y, inPixels[i]);
				i++;
			}
		lut = null;

		return inPixels;
	}

	/**
	 * Filters RGB pixels.
	 * 
	 * @param x
	 * @param y
	 * @param rgb
	 * 
	 * @return
	 */
	public int filterRGB(int x, int y, int rgb) {
		if (lut != null) {
			int a = rgb & 0xff000000;
			int r = lut[COLORS.RED.ordinal()][(rgb >> 16) & 0xff];
			int g = lut[COLORS.GREEN.ordinal()][(rgb >> 8) & 0xff];
			int b = lut[COLORS.BLUE.ordinal()][rgb & 0xff];

			return a | (r << 16) | (g << 8) | b;
		}
		return rgb;
	}

	/**
	 * Overrides {@link Object.toString()}.
	 */
	public String toString() {
		return this.getClass().getName();
	}
}
