/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import com.ocrix.image.processing.filters.Validator;

/**
 * Wiki suggests:<br>
 * In mathematics the Laplace operator or Laplacian is a differential operator
 * given by the divergence of the gradient of a function on Euclidean space. It
 * is usually denoted by the symbols, ∇*∇, ∇2 or Δ. The Laplacian Δƒ(p) of a
 * function ƒ at a point p, up to a constant depending on the dimension, is the
 * rate at which the average value of ƒ over spheres centered at p, deviates
 * from ƒ(p) as the radius of the sphere grows. In a Cartesian coordinate
 * system, the Laplacian is given by sum of all the (unmixed) second partial
 * derivatives of the function. In other coordinate systems such as cylindrical
 * and spherical coordinates, the Laplacian also has a useful form. <br>
 * Marr-Hildreth edge detection finds edges by second order differentiation. It
 * also has the ability to detect edges at different scales, much like the human
 * vision system.<br>
 * Marr-Hildreth uses the Gaussian smoothing operator to improve the response to
 * noise, and by differentiation the Laplacian of Gaussian is called the LoG
 * operator.Edges are at the 'zero crossings' of the LoG, which is where there
 * is a change in gradient.
 */
public class Laplacian {
	/* Class member declarations */
	private int[] input;
	private int[] output;
	private float[] template = { 0, -1, 0, -1, 4, -1, 0, -1, 0 };
	private int[] processedValues;
	private int[] MHValues;
	private int progress;
	// private int sigma;
	private int templateSize = 3;
	private int width;
	private int height;

	public Laplacian() {

	}

	public void init(int[] original, int widthIn, int heightIn) {
		Validator.validateIntArrayEmptiness(original);
		Validator.validateIntNotZero(widthIn, heightIn);
		width = widthIn;
		height = heightIn;
		input = new int[width * height];
		output = new int[width * height];
		input = original;
	}

	public int[] process() {
		int sum;
		int max = 0;
		int min = 0;
		progress = 0;
		processedValues = new int[width * height];

		for (int x = 1; x < width - 2; x++) {
			progress++;
			for (int y = 1; y < height - 2; y++) {
				sum = 0;
				for (int x1 = 0; x1 < templateSize; x1++) {
					for (int y1 = 0; y1 < templateSize; y1++) {
						int x2 = (x - (templateSize - 1) / 2 + x1);
						int y2 = (y - (templateSize - 1) / 2 + y1);
						float value = (input[y2 * width + x2] & 0xff)
								* (template[y1 * templateSize + x1]);
						sum += value;
					}
				}
				if (sum > max)
					max = sum;
				if (sum < min)
					min = sum;
				processedValues[y * width + x] = sum;
			}
		}
		float ratio;
		ratio = (float) 255 / (float) (max + min);
		ratio = Math.abs(ratio);

		for (int x = 1; x < width - 2; x++) {
			for (int y = 1; y < height - 2; y++) {
				processedValues[y * width + x] = (int) ((double) (processedValues[y
						* width + x] - min) * ratio);
				int val = processedValues[y * width + x];
	
				if (val >= 0) {
					output[y * width + x] = 0xff000000 | (val << 16);
				}
				if (val < 0) {
					output[y * width + x] = 0xff000000 | ((-val & 0xff) << 8);
				}
			}
		}
		return output;
	}

	public int[] getMH() {
		progress = 0;
		MHValues = new int[width * height];
		int max = 1;/* To prevent zero division */
		for (int x = 1; x < width - 2; x++) {
			progress++;
			for (int y = 1; y < height - 2; y++) {
				int val = get_change(x, y);
				MHValues[y * width + x] = val;
				if (val > max)
					max = val;
			}
		}
		for (int x = 1; x < width - 2; x++) {
			for (int y = 1; y < height - 2; y++) {
				int val = (int) ((double) MHValues[y * width + x] * 255) / max;
				MHValues[y * width + x] = 0xff000000 | (val << 16 | val << 8 | val);
			}
		}
		return MHValues;
	}

	private int get_change(int x, int y) {
		// int value = 0;
		int total = 0;

		if (processedValues[(y - 1) * width + (x - 1)]
				* processedValues[(y + 1) * width + (x + 1)] < 0) {
			total += Math.abs(processedValues[(y - 1) * width + (x - 1)]
					- processedValues[(y + 1) * width + (x + 1)]);
		}
		if (processedValues[(y + 1) * width + (x - 1)]
				* processedValues[(y - 1) * width + (x + 1)] < 0) {
			total += Math.abs(processedValues[(y + 1) * width + (x - 1)]
					- processedValues[(y - 1) * width + (x + 1)]);
		}
		if (processedValues[(y + 1) * width + (x)]
				* processedValues[(y - 1) * width + (x)] < 0) {
			total += Math.abs(processedValues[(y + 1) * width + (x)]
					- processedValues[(y - 1) * width + (x)]);
		}
		if (processedValues[(y) * width + (x + 1)]
				* processedValues[(y) * width + (x - 1)] < 0) {
			total += Math.abs(processedValues[(y) * width + (x + 1)]
					- processedValues[(y) * width + (x - 1)]);
		}

		return total;
	}

	public int getProgress() {
		return progress;
	}
}
