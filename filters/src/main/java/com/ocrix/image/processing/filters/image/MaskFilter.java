/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

/**
 * Applies a bit mask to each ARGB pixel of an image. You can use this for, say,
 * masking out the red channel.
 */
public class MaskFilter extends PointFilter {

	private int mask;

	/**
	 * Constructs a {@link MaskFilter} with a mask equals
	 * <code>0xff00ffff</code>
	 */
	public MaskFilter() {
		this(0xff00ffff);
	}

	/**
	 * Creates a mask filter with provided mask.
	 * 
	 * @param mask to be set as a mask.
	 */
	public MaskFilter(int mask) {
		canFilterIndexColorModel = true;
		setMask(mask);
	}

	/**
	 * Sets a mask.
	 * 
	 * @param mask to be set.
	 */
	public void setMask(int mask) {
		this.mask = mask;
	}

	/**
	 * Gets a mask.
	 * 
	 * @return the mask.
	 */
	public int getMask() {
		return mask;
	}

	/**
	 * Performs filter operation, i.e. <code>rgb & mask</code>.
	 * 
	 * @param x a x coordinate.
	 * @param y an y coordinate.
	 * @param rgb a value.
	 */
	public int filterRGB(int x, int y, int rgb) {
		return rgb & mask;
	}

	
	/**
	 * Overrides toString.
	 */
	public String toString() {
		return this.getClass().getName();
	}
}
