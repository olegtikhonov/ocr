/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;

import com.ocrix.image.processing.filters.Validator;



/**
 * Creates the following composites:
 * <p>
 * <b>BLEND</b> Alpha blending is a convex combination of two colors allowing
 * for transparency effects
 * <p>
 * <b>COLOR_DODGE</b> blend mode divides the bottom layer by the inverted top
 * layer. This decreases the contrast to make the bottom layer reflect the top
 * layer: the brighter the top layer, the more its colour affects the bottom
 * layer. Blending with white gives white. Blending with black does not change
 * the image. This effect is similar to changing the white point. The operation
 * is not invertible.
 * <p>
 * <b>HUE</b> blend mode preserves the luma and chroma of the bottom layer,
 * while adopting the hue of the top layer.
 * <p>
 * <b>SATURATION</b> blend mode preserves the luma and hue of the bottom layer,
 * while adopting the chroma of the top layer.
 * <p>
 * <b>COLOR</b> blend mode preserves the luma of the bottom layer, while
 * adopting the hue and chroma of the top layer.
 * <p>
 * <b>ADD</b> blend mode simply adds pixel values of one layer with the other.
 * In case of values above 255 (in the case of RGB), white is displayed.
 * <p>
 * <b>SUBTRACT</b> blend mode simply subtracts pixel values of one layer with
 * the other. In case of negative values, black is displayed.
 * <p>
 * <b>DIFFERENCE</b> subtracts the top layer from the bottom layer or the other
 * way round, to always get a positive value. Blending with black produces no
 * change, as values for all colours are 0. (The RGB value for black is 0,0,0).
 * Blending with white inverts the picture.
 * <p>
 * <b>DARKEN</b> takes the darkest value for each pixel from each layer.
 * <p>
 * <b>LIGHTEN</b> takes the lightest pixel from each layer.
 * <p>
 * <b>MULTIPLY</b> simply multiplies each component in the two layers.
 * <p>
 * <b>COLOR_BURN</b> divides the inverted bottom layer by the top layer, and
 * then inverts the result. This darkens the top layer increasing the contrast
 * to reflect the colour of the bottom layer. The darker the bottom layer, the
 * more its colour is used. Blending with white produces no difference.
 * <p>
 * <b>SOFT_LIGHT</b> refers to light that tends to "wrap" around objects,
 * casting shadows with soft edges.
 * <p>
 * <b>HARD_LIGHT</b> sources cast shadows whose appearance of the shadow depends
 * on the lighting instrument.
 * <p>
 * <b>STENCIL</b> is used to limit the area of rendering (stenciling).
 * <p>
 * <b>SILHOUETTE</b> is the image of a person, an object or scene consisting of
 * the outline and a basically featureless interior, with the silhouetted object
 * usually being black.
 * 
 * @author otikhonov
 * 
 */
public final class MiscComposite implements Composite {

    /* Class' members */
    protected float extraAlpha;
    protected int rule;

    /* TODO should be re-factored */
    public final static int BLEND = 0;
    public final static int ADD = 1;
    public final static int SUBTRACT = 2;
    public final static int DIFFERENCE = 3;

    public final static int MULTIPLY = 4;
    public final static int DARKEN = 5;
    public final static int BURN = 6;
    public final static int COLOR_BURN = 7;

    public final static int SCREEN = 8;
    public final static int LIGHTEN = 9;
    public final static int DODGE = 10;
    public final static int COLOR_DODGE = 11;

    public final static int HUE = 12;
    public final static int SATURATION = 13;
    public final static int VALUE = 14;
    public final static int COLOR = 15;

    public final static int OVERLAY = 16;
    public final static int SOFT_LIGHT = 17;
    public final static int HARD_LIGHT = 18;
    public final static int PIN_LIGHT = 19;

    public final static int EXCLUSION = 20;
    public final static int NEGATION = 21;
    public final static int AVERAGE = 22;

    public final static int STENCIL = 23;
    public final static int SILHOUETTE = 24;

    private static final int MIN_RULE = BLEND;
    private static final int MAX_RULE = SILHOUETTE + 1;

    private MiscComposite(int rule) {
	this(rule, 1.0f);
    }

    private MiscComposite(int rule, float alpha) {
	Validator.validateAlpha(alpha);
	Validator.validateRule(rule, MIN_RULE, MAX_RULE);
	this.rule = rule;
	this.extraAlpha = alpha;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(this.getClass().getName());
	builder.append(" [extraAlpha=");
	builder.append(extraAlpha);
	builder.append(", rule=");
	builder.append(rule);
	builder.append("]");
	return builder.toString();
    }

    private float getAlpha() {
	return extraAlpha;
    }

    private int getRule() {
	return rule;
    }

    public static Composite getInstance(int rule, float alpha) {
	switch (rule) {
	case MiscComposite.BLEND:
	    return AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
	case MiscComposite.ADD:
	    return new AddComposite(alpha);
	case MiscComposite.SUBTRACT:
	    return new SubtractComposite(alpha);
	case MiscComposite.DIFFERENCE:
	    return new DifferenceComposite(alpha);
	case MiscComposite.MULTIPLY:
	    return new MultiplyComposite(alpha);
	case MiscComposite.DARKEN:
	    return new DarkenComposite(alpha);
	case MiscComposite.BURN:
	    return new BurnComposite(alpha);
	case MiscComposite.COLOR_BURN:
	    return new ColorBurnComposite(alpha);
	case MiscComposite.SCREEN:
	    return new ScreenComposite(alpha);
	case MiscComposite.LIGHTEN:
	    return new LightenComposite(alpha);
	case MiscComposite.DODGE:
	    return new DodgeComposite(alpha);
	case MiscComposite.COLOR_DODGE:
	    return new ColorDodgeComposite(alpha);
	case MiscComposite.HUE:
	    return new HueComposite(alpha);
	case MiscComposite.SATURATION:
	    return new SaturationComposite(alpha);
	case MiscComposite.VALUE:
	    return new ValueComposite(alpha);
	case MiscComposite.COLOR:
	    return new ColorComposite(alpha);
	case MiscComposite.OVERLAY:
	    return new OverlayComposite(alpha);
	case MiscComposite.SOFT_LIGHT:
	    return new SoftLightComposite(alpha);
	case MiscComposite.HARD_LIGHT:
	    return new HardLightComposite(alpha);
	case MiscComposite.PIN_LIGHT:
	    return new PinLightComposite(alpha);
	case MiscComposite.EXCLUSION:
	    return new ExclusionComposite(alpha);
	case MiscComposite.NEGATION:
	    return new NegationComposite(alpha);
	case MiscComposite.AVERAGE:
	    return new AverageComposite(alpha);
	case MiscComposite.STENCIL:
	    return AlphaComposite.getInstance(AlphaComposite.DST_IN, alpha);
	case MiscComposite.SILHOUETTE:
	    return AlphaComposite.getInstance(AlphaComposite.DST_OUT, alpha);
	default:
	    return new MiscComposite(rule, alpha);
	}
    }

    public CompositeContext createContext(ColorModel srcColorModel,
	    ColorModel dstColorModel, RenderingHints hints) {
	return new MiscCompositeContext(getRule(), getAlpha(), srcColorModel,
	        dstColorModel);
    }

    @Override
    public int hashCode() {
	return (Float.floatToIntBits(getAlpha()) * 31 + rule);
    }

    @Override
    public boolean equals(Object o) {
	if (!(o instanceof MiscComposite)) {
	    return false;
	}
	MiscComposite c = (MiscComposite) o;

	if (getRule() != c.rule) {
	    return false;
	}
	if (getAlpha() != c.extraAlpha) {
	    return false;
	}
	return true;
    }
}
