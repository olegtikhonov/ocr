/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.vecmath;

/**
 * Vector math package, converted to look similar to javax.vecmath.
 */
public class Quat4f extends Tuple4f {

	/**
	 * Constructs a {@link Quat4f}.
	 */
	public Quat4f() {
		this(0, 0, 0, 0);
	}

	/**
	 * Constructs a {@link Quat4f}.
	 * 
	 * @param x will be constructed {@link Quat4f}. Should be eq or greater than 4. 
	 */
	public Quat4f(float[] x) {
		if(x != null && x.length > 3) {
			this.x = x[0];
			this.y = x[1];
			this.z = x[2];
			this.w = x[3];
		}
	}

	/**
	 * Constructs a {@link Quat4f}.
	 * 
	 * @param x value.
	 * @param y value.
	 * @param z value.
	 * @param w value.
	 */
	public Quat4f(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}

	/**
	 * Constructs a {@link Quat4f}.
	 * 
	 * @param t from this object will be constructed another one.
	 */
	public Quat4f(Quat4f t) {
		if(t != null){
			this.x = t.x;
			this.y = t.y;
			this.z = t.z;
			this.w = t.w;
		}
	}

	/**
	 * Constructs a {@link Quat4f}.
	 * 
	 * @param t from this object will be constructed another one.
	 */
	public Quat4f(Tuple4f t) {
		if(t != null){
			this.x = t.x;
			this.y = t.y;
			this.z = t.z;
			this.w = t.w;
		}
	}

	/**
	 * Sets an {@link AxisAngle4f}.
	 * 
	 * @param a
	 */
	public void set(AxisAngle4f a) {
		float halfTheta = a.getAngle() * 0.5f;
		float cosHalfTheta = (float) Math.cos(halfTheta);
		float sinHalfTheta = (float) Math.sin(halfTheta);
		x = a.getX() * sinHalfTheta;
		y = a.getY() * sinHalfTheta;
		z = a.getZ() * sinHalfTheta;
		w = cosHalfTheta;
	}

	/*
	 * public void EulerToQuaternion(float roll, float pitch, float yaw) { float
	 * cr, cp, cy, sr, sp, sy, cpcy, spsy; cr = cos(roll/2); cp = cos(pitch/2);
	 * cy = cos(yaw/2); sr = sin(roll/2); sp = sin(pitch/2); sy = sin(yaw/2);
	 * cpcy = cp * cy; spsy = sp * sy; w = cr * cpcy + sr * spsy; x = sr * cpcy
	 * - cr * spsy; y = cr * sp * cy + sr * cp * sy; z = cr * cp * sy - sr * sp
	 * * cy; }
	 */

	/**
	 * Normalizes by calculating 
	 * <code>
	 *     1.0f / (x * x + y * y + z * z + w * w)
	 * </code>
	 */
	public void normalize() {
		float d = 1.0f / (x * x + y * y + z * z + w * w);
		x *= d;
		y *= d;
		z *= d;
		w *= d;
	}

	/*
	 * public void mul( Quat4f q ) { Quat4f q3 = new Quat4f(); Vector3f vectorq1
	 * = new Vector3f( x, y, z ); Vector3f vectorq2 = new Vector3f( q.x, q.y,
	 * q.z );
	 * 
	 * Vector3f tempvec1 = new Vector3f( vectorq1 ); Vector3f tempvec2; Vector3f
	 * tempvec3; q3.w = (w*q.w) - tempvec1.dot(vectorq2);
	 * tempvec1.cross(vectorq2); tempvec2.x = w * q.x; tempvec2.y = w * q.y;
	 * tempvec2.z = w * q.z; tempvec3.x = q.w * x; tempvec3.y = q.w * y;
	 * tempvec3.z = q.w * z; q3.x = tempvec1.x + tempvec2.x + tempvec3.x; q3.y =
	 * tempvec1.y + tempvec2.y + tempvec3.y; q3.z = tempvec1.z + tempvec2.z +
	 * tempvec3.z; set(q3); }
	 */

	public void set(Matrix4f m) {
		float s;
		int i;

		float tr = m.getM00() + m.getM11() + m.getM22();

		if (tr > 0.0) {
			s = (float) Math.sqrt(tr + 1.0f);
			w = s / 2.0f;
			s = 0.5f / s;
			x = (m.getM12() - m.getM21()) * s;
			y = (m.getM20() - m.getM02()) * s;
			z = (m.getM01() - m.getM10()) * s;
		} else {
			i = 0;
			if (m.getM11() > m.getM00()) {
				i = 1;
				if (m.getM22() > m.getM11()) {
					i = 2;
				} else {
				}
			} else {
				if (m.getM22() > m.getM00()) {
					i = 2;
				} else {
				}
			}

			switch (i) {
			case 0:
				s = (float) Math.sqrt((m.getM00() - (m.getM11() + m.getM22())) + 1.0f);
				x = s * 0.5f;
				if (s != 0.0)
					s = 0.5f / s;
				w = (m.getM12() - m.getM21()) * s;
				y = (m.getM01() + m.getM10()) * s;
				z = (m.getM02() + m.getM20()) * s;
				break;
			case 1:
				s = (float) Math.sqrt((m.getM11() - (m.getM22() + m.getM00())) + 1.0f);
				y = s * 0.5f;
				if (s != 0.0)
					s = 0.5f / s;
				w = (m.getM20() - m.getM02()) * s;
				z = (m.getM12() + m.getM21()) * s;
				x = (m.getM10() + m.getM01()) * s;
				break;
			case 2:
				s = (float) Math.sqrt((m.getM00() - (m.getM11() + m.getM22())) + 1.0f);
				z = s * 0.5f;
				if (s != 0.0)
					s = 0.5f / s;
				w = (m.getM01() - m.getM10()) * s;
				x = (m.getM20() + m.getM02()) * s;
				y = (m.getM21() + m.getM12()) * s;
				break;
			}
		}
	}
}
