/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

/**
 * Defines a named rules.
 */
public enum RuleNames {

	NORMAL("Normal"),

	ADD("Add"),

	SUBTRACT("Subtract"),

	DIFFERENCE("Difference"),

	MULTIPLY("Multiply"),

	DARKEN("Darken"),

	BURN("Burn"),

	COLOR_BURN("Color Burn"),

	SCREEN("Screen"),

	LIGHTEN("Lighten"),

	DODGE("Dodge"),

	COLOR_DODGE("Color Dodge"),

	HUE("Hue"),

	SATURATION("Saturation"),

	BRIGHTNESS("Brightness"),

	COLOR("Color"),

	OVERLAY("Overlay"),

	SOFT_LIGHT("Soft Light"),

	HARD_LIGHT("Hard Light"),

	PIN_LIGHT("Pin Light"),

	EXCLUSION("Exclusion"),

	NEGATION("Negation"),

	AVERAGE("Average"),

	STENCIL("Stencil"),

	SILHOUETTE("Silhouette");

	/* Holds the value of the named rule */
	private String value = null;

	private RuleNames(String val) {
		this.value = val;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
