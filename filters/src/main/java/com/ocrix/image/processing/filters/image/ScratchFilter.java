/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.ocrix.image.processing.filters.Validator;

/**
 * A scratch filter. <br>
 * Defaults are:
 * <ul>
 * <li>density = <code>0.1f</code></li>
 * <li>angleVariation = <code>1.0f</code></li>
 * <li>width = <code>0.5f</code></li>
 * <li>length = <code>0.5f</code></li>
 * <li>color = <code>0xffffffff</code></li>
 * <li>seed = <code>0</code></li>
 * </ul>
 * 
 */
public class ScratchFilter extends AbstractBufferedImageOp {
	private float density = 0.1f;
	private float angle;
	private float angleVariation = 1.0f;
	private float width = 0.5f;
	private float length = 0.5f;
	private int color = 0xffffffff;
	private int seed = 0;

	/**
	 * Constructs a {@link ScratchFilter} with its defaults.
	 */
	public ScratchFilter() {
	}

	/**
	 * Sets an angle up
	 * 
	 * @param angle
	 */
	public void setAngle(float angle) {
		this.angle = angle;
	}

	/**
	 * Gets an angle
	 * 
	 * @return
	 */
	public float getAngle() {
		return angle;
	}

	/**
	 * Sets an angle variation up
	 * 
	 * @param angleVariation
	 */
	public void setAngleVariation(float angleVariation) {
		this.angleVariation = angleVariation;
	}

	/**
	 * Gets an angle variation
	 * 
	 * @return
	 */
	public float getAngleVariation() {
		return angleVariation;
	}

	/**
	 * Sets a density up
	 * 
	 * @param density
	 */
	public void setDensity(float density) {
		this.density = density;
	}

	/**
	 * Gets a density
	 * 
	 * @return
	 */
	public float getDensity() {
		return density;
	}

	/**
	 * Sets a length
	 * 
	 * @param length
	 */
	public void setLength(float length) {
		this.length = length;
	}

	/**
	 * Gets a length
	 * 
	 * @return
	 */
	public float getLength() {
		return length;
	}

	/**
	 * Sets a width up
	 * 
	 * @param width
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * Gets a width
	 * 
	 * @return
	 */
	public float getWidth() {
		return width;
	}

	/**
	 * Sets a color up
	 * 
	 * @param color
	 */
	public void setColor(int color) {
		this.color = color;
	}

	/**
	 * Gets a color
	 * 
	 * @return
	 */
	public int getColor() {
		return color;
	}

	/**
	 * Sets a seed up
	 * 
	 * @param seed
	 */
	public void setSeed(int seed) {
		this.seed = seed;
	}

	public int getSeed() {
		return seed;
	}

	/**
	 * Overrides a base filter
	 * 
	 * @param source
	 *            = {@link BufferedImage}
	 * @param destination
	 *            - {@link BufferedImage}
	 */
	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		if (dst == null)
			dst = createCompatibleDestImage(src, null);

		int width = src.getWidth();
		int height = src.getHeight();
		int numScratches = (int) (density * width * height / 100);
		float l = length * width;
		Random random = new Random(seed);
		Graphics2D g = dst.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(new Color(color));
		g.setStroke(new BasicStroke(this.width));
		for (int i = 0; i < numScratches; i++) {
			float x = width * random.nextFloat();
			float y = height * random.nextFloat();
			float a = angle + ImageMath.TWO_PI
					* (angleVariation * (random.nextFloat() - 0.5f));
			float s = (float) Math.sin(a) * l;
			float c = (float) Math.cos(a) * l;
			float x1 = x - c;
			float y1 = y - s;
			float x2 = x + c;
			float y2 = y + s;
			g.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
		}
		g.dispose();

		return dst;
	}

	public String toString() {
		return this.getClass().getName();
	}
}
