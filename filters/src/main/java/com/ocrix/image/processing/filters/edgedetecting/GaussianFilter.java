/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;

import org.apache.log4j.Logger;

import com.ocrix.image.processing.filters.Validator;

/**
 * Wiki suggests: <br>
 * In electronics and signal processing, a Gaussian filter is a filter whose
 * impulse response is a Gaussian function. Gaussian filters are designed to
 * give no overshoot to a step function input while minimizing the rise and fall
 * time. This behavior is closely connected to the fact that the Gaussian filter
 * has the minimum possible group delay. Mathematically, a Gaussian filter
 * modifies the input signal by convolution with a Gaussian function; this
 * transformation is also known as the Weierstrass transform.
 */
public class GaussianFilter {
	/* Class member declarations */
	private int[] input;
	private int[] output;
	private float[] template;
	private int progress;
	private double sigma;
	private int templateSize;
	private int width;
	private int height;
	private static final Logger LOG = Logger.getLogger(GaussianFilter.class);
	
	

	public GaussianFilter() {
		progress = 0;
	}

	public void init(int[] original, int sigmaIn, int tempSize, int widthIn,
			int heightIn) {
		Validator.validateIntArrayEmptiness(original);
		Validator.validateIntNotZero(sigmaIn, tempSize, widthIn, heightIn);
		if ((tempSize % 2) == 0)
			templateSize = tempSize - 1;
		sigma = (double) sigmaIn;
		templateSize = tempSize;
		width = widthIn;
		height = heightIn;
		input = new int[width * height];
		output = new int[width * height];
		template = new float[templateSize * templateSize];
		input = original;
	}

	public void init(int[] original, double sigmaIn, int tempSize, int widthIn,
			int heightIn) {
		Validator.validateIntArrayEmptiness(original);
		Validator.validateIntNotZero(tempSize, widthIn, heightIn);
		Validator.validateDouble(sigmaIn);

		if ((tempSize % 2) == 0)
			templateSize = tempSize - 1;
		sigma = sigmaIn;
		templateSize = tempSize;
		width = widthIn;
		height = heightIn;
		input = new int[width * height];
		output = new int[width * height];
		template = new float[templateSize * templateSize];
		input = original;
	}

	public void generateTemplate() {
		float center = (templateSize - 1) / 2;

		float total = 0;

		for (int x = 0; x < templateSize; x++) {
			for (int y = 0; y < templateSize; y++) {
				template[x * templateSize + y] = (float) (1 / (float) (2
						* Math.PI * sigma * sigma))
						* (float) Math
								.exp((float) (-((x - center) * (x - center) + (y - center)
										* (y - center)) / (2 * sigma * sigma)));
				total += template[x * templateSize + y];
			}
		}
		for (int x = 0; x < templateSize; x++) {
			for (int y = 0; y < templateSize; y++) {
				template[x * templateSize + y] = template[x * templateSize + y]
						/ total;
			}
		}
	}

	public int[] process() {
		float sum;
		progress = 0;

		int arraySize = Math.abs((width - (templateSize - 1))
				* (height - (templateSize - 1)));

		int outputsmaller[] = new int[arraySize];
		float value = 0;

		for (int x = (templateSize - 1) / 2; x < width - (templateSize + 1) / 2; x++) {
			progress++;
			for (int y = (templateSize - 1) / 2; y < height
					- (templateSize + 1) / 2; y++) {
				sum = 0;
				for (int x1 = 0; x1 < templateSize; x1++) {
					for (int y1 = 0; y1 < templateSize; y1++) {
						int x2 = (x - (templateSize - 1) / 2 + x1);
						int y2 = (y - (templateSize - 1) / 2 + y1);

						if ((y2 * width + x2) < input.length) {
							value = (input[y2 * width + x2] & 0xff)
									* (template[y1 * templateSize + x1]);
							sum += value;
						}
					}
				}
				outputsmaller[(y - (templateSize - 1) / 2)
						* (width - (templateSize - 1))
						+ (x - (templateSize - 1) / 2)] = 0xff000000 | ((int) sum << 16
						| (int) sum << 8 | (int) sum);
			}
		}
		progress = width;

		Toolkit tk = Toolkit.getDefaultToolkit();

		Image tempImage = tk.createImage(
				new MemoryImageSource((width - (templateSize - 1)),
						(height - (templateSize - 1)), outputsmaller, 0,
						(width - (templateSize - 1)))).getScaledInstance(256,
				256, Image.SCALE_SMOOTH);
		PixelGrabber grabber = new PixelGrabber(tempImage, 0, 0, width, height,
				output, 0, width);
		try {
			grabber.grabPixels();
		} catch (InterruptedException e2) {
			LOG.error(e2);
		}
		progress = width;

		return output;
	}

	public int getProgress() {
		return progress;
	}
}
