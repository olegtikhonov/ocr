/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.ocrix.image.processing.filters.Validator;
import com.ocrix.image.processing.filters.composite.AddComposite;



/**
 * Defines a shine filter.
 * <br>The defaults are:
 * <ul>
 * <li>radius = 5.0f</li>
 * <li>angle = (float) Math.PI * 7 / 4</li>
 * <li>distance = 5.0f</li>
 * <li>bevel = 0.5f</li>
 * <li>shadowOnly = false</li>
 * <li>shineColor = 0xffffffff</li>
 * <li>brightness = 0.2f</li>
 * <li>softness = 0.0f</li>
 * </ul>
 */
public class ShineFilter extends AbstractBufferedImageOp {
	/* Class member declarations */
	private float radius = 5;
	private float angle = (float) Math.PI * 7 / 4;
	private float distance = 5;
	private float bevel = 0.5f;
	private boolean shadowOnly = false;
	private int shineColor = 0xffffffff;
	private float brightness = 0.2f;
	private float softness = 0;
	/* End of class member declarations */
	

	/**
	 * Constructs a shine filter with its defaults.
	 */
	public ShineFilter() {
	}

	
	/**
	 * Sets an angle.
	 * 
	 * @param angle
	 */
	public void setAngle(float angle) {
		this.angle = angle;
	}

	/**
	 * Gets an angle.
	 * 
	 * @return
	 */
	public float getAngle() {
		return angle;
	}

	
	/**
	 * Sets a distance up.
	 * 
	 * @param distance
	 */
	public void setDistance(float distance) {
		this.distance = distance;
	}

	
	/**
	 * Gets a distance.
	 * 
	 * @return
	 */
	public float getDistance() {
		return distance;
	}

	/**
	 * Set the radius of the kernel, and hence the amount of blur. The bigger
	 * the radius, the longer this filter will take.
	 * 
	 * @param radius - the radius of the blur in pixels.
	 */
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	/**
	 * Get the radius of the kernel.
	 * @return the radius
	 */
	public float getRadius() {
		return radius;
	}

	/**
	 * Sets a bevel up.
	 * 
	 * @param bevel
	 */
	public void setBevel(float bevel) {
		this.bevel = bevel;
	}

	/**
	 * Gets a bevel.
	 * 
	 * @return
	 */
	public float getBevel() {
		return bevel;
	}

	
	/**
	 * Sets a shine color.
	 * 
	 * @param shineColor
	 */
	public void setShineColor(int shineColor) {
		this.shineColor = shineColor;
	}

	/**
	 * Gets a shine color.
	 * 
	 * @return
	 */
	public int getShineColor() {
		return shineColor;
	}

	/**
	 * Sets a shadow only flag.
	 * 
	 * @param shadowOnly
	 */
	public void setShadowOnly(boolean shadowOnly) {
		this.shadowOnly = shadowOnly;
	}

	/**
	 * Gets a shadow only flag.
	 * 
	 * @return
	 */
	public boolean getShadowOnly() {
		return shadowOnly;
	}

	
	/**
	 * Sets a brightness up.
	 * 
	 * @param brightness
	 */
	public void setBrightness(float brightness) {
		this.brightness = brightness;
	}
	
	/**
	 * Gets a brightness.
	 * 
	 * @return
	 */
	public float getBrightness() {
		return brightness;
	}
	
	/**
	 * Sets a softness.
	 * 
	 * @param softness
	 */
	public void setSoftness(float softness) {
		this.softness = softness;
	}

	/**
	 * Gets a softness.
	 * 
	 * @return
	 */
	public float getSoftness() {
		return softness;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		int width = src.getWidth();
		int height = src.getHeight();

		if (dst == null)
			dst = createCompatibleDestImage(src, null);

		float xOffset = distance * (float) Math.cos(angle);
		float yOffset = -distance * (float) Math.sin(angle);

		BufferedImage matte = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		ErodeAlphaFilter s = new ErodeAlphaFilter(bevel * 10, 0.75f, 0.1f);
		matte = s.filter(src, null);
		
		BufferedImage shineLayer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = shineLayer.createGraphics();
		g.setColor(new Color(shineColor));
		g.fillRect(0, 0, width, height);
		g.setComposite(AlphaComposite.DstIn);
		g.drawRenderedImage(matte, null);
		g.setComposite(AlphaComposite.DstOut);
		g.translate(xOffset, yOffset);
		g.drawRenderedImage(matte, null);
		g.dispose();
		shineLayer = new GaussianFilter(radius).filter(shineLayer, null);
		shineLayer = new RescaleFilter(3 * brightness).filter(shineLayer, shineLayer);

		g = dst.createGraphics();
		g.drawRenderedImage(src, null);
		g.setComposite(new AddComposite(1.0f));
		g.drawRenderedImage(shineLayer, null);
		g.dispose();

		return dst;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getName());
		builder.append(" [radius=");
		builder.append(radius);
		builder.append(", angle=");
		builder.append(angle);
		builder.append(", distance=");
		builder.append(distance);
		builder.append(", bevel=");
		builder.append(bevel);
		builder.append(", shadowOnly=");
		builder.append(shadowOnly);
		builder.append(", shineColor=");
		builder.append(shineColor);
		builder.append(", brightness=");
		builder.append(brightness);
		builder.append(", softness=");
		builder.append(softness);
		builder.append("]");
		return builder.toString();
	}
}
