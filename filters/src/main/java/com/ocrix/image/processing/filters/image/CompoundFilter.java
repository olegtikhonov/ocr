/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.image;


import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import com.ocrix.image.processing.filters.Validator;


/**
 * A BufferedImageOp which combines two other BufferedImageOps, one after the other.
 */
public class CompoundFilter extends AbstractBufferedImageOp {
	private BufferedImageOp filter1;
	private BufferedImageOp filter2;
	
	/**
	 * Construct a CompoundFilter from two filters, i.e. in first step applies
	 * the first one, and in the second step - the other one.
	 * 
	 * @param filter1
	 *            the first filter
	 * @param filter2
	 *            the second filter
	 */
    public CompoundFilter( BufferedImageOp filter1, BufferedImageOp filter2 ) {
    	setFilter1(filter1);
    	setFilter2(filter2);
	}
	
    
    /**
     * Applies two filters on that image without changing it
     * 
     * @param src - apply to
     * 
     * @return {@link BufferedImage}
     */
	public BufferedImage filter(BufferedImage src, BufferedImage dummy) {
		Validator.validateBufferedImage(src);
		/* Creates a buffer of the image */
		BufferedImage mBufferedImage = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
		/* Loads a source image */
		Graphics2D g2 = mBufferedImage.createGraphics(); 
		g2.drawImage(src, null, null);
		
		/* Applies the first filter */		
		BufferedImage image = getFilter1().filter(mBufferedImage, null);
		image = getFilter2().filter(image, null);
		return image;
	}
	
	
	//-----------------------------------------
	//			Private functions area
	//-----------------------------------------
	private BufferedImageOp getFilter1() {
		return filter1;
	}

	private void setFilter1(BufferedImageOp filter1) {
		Validator.validateBufferedImageOp(filter1);
		this.filter1 = filter1;
	}

	private BufferedImageOp getFilter2() {
		return filter2;
	}

	private void setFilter2(BufferedImageOp filter2) {
		Validator.validateBufferedImageOp(filter2);
		this.filter2 = filter2;
	}
}
