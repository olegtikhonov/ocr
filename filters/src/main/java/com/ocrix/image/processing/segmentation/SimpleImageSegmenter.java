/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.segmentation;


import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Vector;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;


/**
 * <pre>
 * The algorithm in pseudo code:
 * for each pixel in image
 *     if pixel is not in segment
 *         create new segment
 *         add the pixel as new "seed" point to list of candidates
 *         while segment has candidate points
 *             remove first point from the list of candidates
 *             if the first candidate point is within threshold limit
 *                 add the first candidate point to the segment
 *                 add neighbor pixel above to the candidate list
 *                 add neighbor pixel below to the candidate list
 *                 add neighbor pixel right to the candidate list
 *                 add neighbor pixel left  to the candidate list 
 * </pre>
 * The source taken from
 * {@link http://popscan.blogspot.com/2013/06/color-image-segmentation-by.html}
 */
public class SimpleImageSegmenter {
	// value for visited pixel
	private static final int VISITED = 0x00FF0000;
	// value for not visited pixel
	private static final int NOT_VISITED = 0x00000000;
	// the destination image
	private BufferedImage dstImage;
	// threshold value 0 - 255
	private int threshold;
	// image width
	private int width;
	// image height
	private int height;
	// "seed" color / segment color
	private int color;
	// red value from seed
	private int red;
	// green value from seed
	private int green;
	// blue value from seed
	private int blue;
	// pixels from source image
	private int[] pixels;
	// table for keeping track or visits
	private int[] visited;
	// keeping for candidate points
	private Vector<SPoint> points;
	// A logger.
	private static final Logger LOG = Logger.getLogger(SimpleImageSegmenter.class);

	
	private class SPoint {
		int x;
		int y;

		public SPoint(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String src = "/home/oleg/develop/eclipse_workspace/filters/src/test/resources/images/decision.jpg";
		String dst = "./target/" + "printed_text2.png";
		int threshold = 87;

		// creates new Segmentize object
		SimpleImageSegmenter s = new SimpleImageSegmenter(loadImage(src), threshold);
		// calls the function to actually start the segmentation
		BufferedImage dstImage = s.segmentize();
		// saves the resulting image
		saveImage(dst, dstImage);
	}

	public SimpleImageSegmenter(BufferedImage srcImage, int threshold) {
		this.threshold = threshold;
		this.width = srcImage.getWidth();
		this.height = srcImage.getHeight();
		// extracts pixels from source image
		this.pixels = srcImage.getRGB(0, 0, width, height, null, 0, width);
		// creates empty destination image
		this.dstImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		this.visited = new int[pixels.length];
		this.points = new Vector<SPoint>();
	}

	private BufferedImage segmentize() {
		// initializes points
		points.clear();
		// clears table with NOT_VISITED value
		Arrays.fill(visited, NOT_VISITED);
		// loops through all pixels
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				// if not visited, start new segment
				if (visited[width * y + x] == NOT_VISITED) {
					// extracts segment color info from pixel
					color = pixels[width * y + x];
					red = color >> 16 & 0xff;
					green = color >> 8 & 0xff;
					blue = color & 0xff;
					// adds "seed"
					points.add(new SPoint(x, y));
					// starts finding neighboring pixels
					flood();
				}
			}
		}
		
		// saves the result image
		dstImage.setRGB(0, 0, width, height, pixels, 0, width);
		return dstImage;
	}

	private void flood() {
		// while there are candidates in points vector
		while (points.size() > 0) {
			// removes the first candidate
			SPoint current = points.remove(0);
			int x = current.x;
			int y = current.y;
			if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
				// checks if the candidate is NOT_VISITED yet
				if (visited[width * y + x] == NOT_VISITED) {
					// extracts color info from candidate pixel
					int c = pixels[width * y + x];
					int red = c >> 16 & 0xff;
					int green = c >> 8 & 0xff;
					int blue = c >> 0 & 0xff;
					// calculates difference between
					// seed's and candidate's
					// red, green and blue values
					int rx = Math.abs(red - this.red);
					int gx = Math.abs(green - this.green);
					int bx = Math.abs(blue - this.blue);
					// if all colors are under threshold
					if (rx <= threshold && gx <= threshold && bx <= threshold) {
						// adds the candidate to the segment (image)
						pixels[width * y + x] = color;
						// mark the candidate as visited
						visited[width * y + x] = VISITED;
						// adds neighboring pixels as candidate
						// (8-connected here)
						points.add(new SPoint(x - 1, y - 1));
						points.add(new SPoint(x, y - 1));
						points.add(new SPoint(x + 1, y - 1));
						points.add(new SPoint(x - 1, y));
						points.add(new SPoint(x + 1, y));
						points.add(new SPoint(x - 1, y + 1));
						points.add(new SPoint(x, y + 1));
						points.add(new SPoint(x + 1, y + 1));
					}
				}
			}
		}
	}

	public static void saveImage(String filename, BufferedImage image) {
		File file = new File(filename);
		try {
			ImageIO.write(image, "png", file);
		} catch (Exception e) {
			LOG.error(e.toString() + " Image '" + filename + "' saving failed.");
		}
	}

	public static BufferedImage loadImage(String filename) {
		BufferedImage result = null;
		try {
			result = ImageIO.read(new File(filename));
		} catch (Exception e) {
			LOG.error(e.toString() + " Image '" + filename + "' not found.");
		}
		return result;
	}
}
