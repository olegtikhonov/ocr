/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters;

/**
 * Defines a smear shapes.
 */
public enum SmearShape {
	/**
	 * The geometrical figure consisting of two lines or bars perpendicular to
	 * each other.
	 */
	CROSSES, 
	
	/**
	 * An independent object, distinct from the set of points which lie on it.
	 */
	LINES, 
	
	/**
	 * A set of all points in a plane that are a given distance from a given
	 * point, the center.
	 */
	CIRCLES, 
	
	/**
	 * A regular quadrilateral.
	 */
	SQUARES;
}
