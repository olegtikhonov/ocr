/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.Color;
import java.awt.image.BufferedImage;

import com.ocrix.image.processing.filters.Validator;

/**
 * A filter which can be used to produce wipes by transferring the luma of a
 * Destination image into the alpha channel of the source.
 */
public class ChromaKeyFilter extends AbstractBufferedImageOp {
	/* ChromaKeyFilter members */
	private float hTolerance = 0;
	private float sTolerance = 0;
	private float bTolerance = 0;
	private int color;

	/**
	 * Constructs a chroma key filter Defaults are all 0.0f, change tham using
	 * setters
	 */
	public ChromaKeyFilter() {
	}

	/**
	 * Set the tolerance of the image in the range 0..1. *arg tolerance The
	 * tolerance
	 */
	public void setHTolerance(float hTolerance) {
		Validator.validateFloatInRange(hTolerance, 0.0f, 1.0f);
		this.hTolerance = hTolerance;
	}

	public float getHTolerance() {
		return hTolerance;
	}

	public void setSTolerance(float sTolerance) {
		this.sTolerance = sTolerance;
	}

	public float getSTolerance() {
		return sTolerance;
	}

	public void setBTolerance(float bTolerance) {
		this.bTolerance = bTolerance;
	}

	public float getBTolerance() {
		return bTolerance;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getColor() {
		return color;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		/* In case of null */
		Validator.validateBufferedImage(src);

		int width = src.getWidth();
		int height = src.getHeight();

		if (dst == null) {
			dst = createCompatibleDestImage(src, null);
		}
		/* In case when src is different than dst by width and height */
		Validator.validateSize(src, dst);

		float[] hsb1 = null;
		float[] hsb2 = null;
		int rgb2 = color;
		int r2 = (rgb2 >> 16) & 0xff;
		int g2 = (rgb2 >> 8) & 0xff;
		int b2 = rgb2 & 0xff;
		hsb2 = Color.RGBtoHSB(r2, b2, g2, hsb2);
		int[] inPixels = null;
		for (int y = 0; y < height; y++) {
			inPixels = getRGB(src, 0, y, width, 1, inPixels);
			for (int x = 0; x < width; x++) {
				int rgb1 = inPixels[x];
				int r1 = (rgb1 >> 16) & 0xff;
				int g1 = (rgb1 >> 8) & 0xff;
				int b1 = rgb1 & 0xff;
				hsb1 = Color.RGBtoHSB(r1, b1, g1, hsb1);

				if (Math.abs(hsb1[0] - hsb2[0]) < getHTolerance()
						&& Math.abs(hsb1[1] - hsb2[1]) < getSTolerance()
						&& Math.abs(hsb1[2] - hsb2[2]) < getBTolerance()) {
					inPixels[x] = rgb1 & 0xffffff;
				} else {
					inPixels[x] = rgb1;
				}
			}
			setRGB(dst, 0, y, width, 1, inPixels);
		}

		return dst;
	}

	public String toString() {
		return this.getClass().getName();
	}
}
