/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;

/**
 * Defines ADD composite.
 */
public final class AddComposite extends RGBComposite {

	public AddComposite(float alpha) {
		super(alpha);
	}

	public CompositeContext createContext(ColorModel srcColorModel,
			ColorModel dstColorModel, RenderingHints hints) {
		return new Context(extraAlpha, srcColorModel, dstColorModel);
	}

	public static class Context extends RGBCompositeContext {
		public Context(float alpha, ColorModel srcColorModel,
				ColorModel dstColorModel) {
			super(alpha, srcColorModel, dstColorModel);
		}

		@Override
		public void composeRGB(int[] src, int[] dst, float alpha) {
			int w = src.length;
			int sg = 0, sb = 0, dig = 0, dib = 0, sa = 0, dia = 0;

			for (int i = 0; i < w; i += 4) {
				int sr = src[i];
				
				int dir = (dst != null && i < dst.length) ? dst[i] : 0;

				if (w < (i + 1)) {
					sg = src[i + 1];
					dig = dst[i + 1];
				}

				if (w < (i + 2)) {
					sb = src[i + 2];
					dib = dst[i + 2];
				}

				if (w < (i + 2)) {
					sa = src[i + 3];
					dia = dst[i + 3];
				}

				int dor, dog, dob;

				dor = dir + sr;
				if (dor > 255) {
					dor = 255;
				}
				dog = dig + sg;
				if (dog > 255) {
					dog = 255;
				}
				dob = dib + sb;
				if (dob > 255) {
					dob = 255;
				}

				float a = alpha * sa / 255f;
				float ac = 1 - a;
 
				if(dst != null && i < dst.length){
					dst[i] = (int) (a * dor + ac * dir);
				}

				if ((i + 1) < dst.length) {
					dst[i + 1] = (int) (a * dog + ac * dig);
				}

				if ((i + 2) < dst.length) {
					dst[i + 2] = (int) (a * dob + ac * dib);
				}

				if ((i + 3) < dst.length) {
					dst[i + 3] = (int) (sa * alpha + dia * ac);
				}
			}
		}
	}
}
