/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.composite;

import java.awt.CompositeContext;
import java.awt.RenderingHints;
import java.awt.image.ColorModel;
import java.util.logging.Logger;

import com.ocrix.image.processing.filters.CommonMessage;
import com.ocrix.image.processing.filters.Validator;



/**
 * Defines a negation composite extending {@link RGBComposite}.
 */
public final class NegationComposite extends RGBComposite {

    /* Local logger */
    private static Logger log = Logger.getLogger(NegationComposite.class
	    .getName());

    /**
     * Creates a {@link NegationComposite}.
     * 
     * @param alpha
     */
    public NegationComposite(float alpha) {
	super(alpha);
    }

    /**
     * Creates a context.
     * 
     * @param srcColorModel
     *            - a source color model.
     * @param dstColorModel
     *            - a destination color model.
     * @param hints
     *            - {@link RenderingHints}.
     */
    public CompositeContext createContext(ColorModel srcColorModel,
	    ColorModel dstColorModel, RenderingHints hints) {
	return new Context(extraAlpha, srcColorModel, dstColorModel);
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(this.getClass().getName());
	builder.append(" [extraAlpha=");
	builder.append(extraAlpha);
	builder.append("]");
	return builder.toString();
    }

    static class Context extends RGBCompositeContext {
	/**
	 * Defines a {@link Context}.
	 * 
	 * @param alpha
	 * @param srcColorModel
	 * @param dstColorModel
	 */
	public Context(float alpha, ColorModel srcColorModel,
	        ColorModel dstColorModel) {
	    super(alpha, srcColorModel, dstColorModel);
	}

	@Override
	public String toString() {
	    StringBuilder builder = new StringBuilder();
	    builder.append(this.getClass().getName());
	    return builder.toString();
	}

	@Override
	public void composeRGB(int[] src, int[] dst, float alpha) {
	    Validator.validateIntArrayEmptiness(src);
	    Validator.validateIntArrayEmptiness(dst);

	    int w = src.length;

	    for (int i = 0; i < w; i += 4) {
		if ((i + 3) < src.length && (i + 3) < dst.length) {
		    int sr = src[i];
		    int dir = dst[i];
		    int sg = src[i + 1];
		    int dig = dst[i + 1];
		    int sb = src[i + 2];
		    int dib = dst[i + 2];
		    int sa = src[i + 3];
		    int dia = dst[i + 3];
		    int dor, dog, dob;

		    dor = 255 - Math.abs(255 - sr - dir);
		    dog = 255 - Math.abs(255 - sg - dig);
		    dob = 255 - Math.abs(255 - sb - dib);

		    float a = alpha * sa / 255f;
		    float ac = 1 - a;

		    dst[i] = (int) (a * dor + ac * dir);
		    dst[i + 1] = (int) (a * dog + ac * dig);
		    dst[i + 2] = (int) (a * dob + ac * dib);
		    dst[i + 3] = (int) (sa * alpha + dia * ac);
		} else {
		    log.warning(CommonMessage.WARN_LENGTH_BIGGER.to());
		}
	    }
	}
    }
}
