/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import com.ocrix.image.processing.filters.Validator;



/**
 * A filter which produces a water ripple distortion.
 * <br>The defaults are:
 * <ul>
 * <li>wavelength = <code>16.0f</code></li>
 * <li>amplitude = <code>10.0f</code></li>
 * <li>phase = <code>0.0f</code></li>
 * <li>centreX = <code>0.5f</code></li>
 * <li>centreY = <code>0.5f</code></li>
 * <li>radius = <code>50.0f</code></li>
 * <li>radius2 = <code>0.0f</code></li>
 * </ul>
 */
public class WaterFilter extends TransformFilter {
	/* Class member declarations */
	private float wavelength = 16.0f;
	private float amplitude = 10.0f;
	private float phase = 0.0f;
	private float centreX = 0.5f;
	private float centreY = 0.5f;
	private float radius = 50.0f;
	private float radius2 = 0.0f;
	private float icentreX;
	private float icentreY;
	/* End of class member declarations */
	

	/**
	 * Constructs a {@link WaterFilter} setting edge action to TransformFilter.CLAMP 
	 */
	public WaterFilter() {
		setEdgeAction(CLAMP);
	}

	/**
	 * Sets the wavelength of the ripples.
	 * 
	 * @param wavelength - the wavelength
	 * @see #getWavelength
	 */
	public void setWavelength(float wavelength) {
		this.wavelength = wavelength;
	}

	
	/**
	 * Gets the wavelength of the ripples.
	 * 
	 * @return the wavelength
	 * @see #setWavelength
	 */
	public float getWavelength() {
		return wavelength;
	}

	
	/**
	 * Sets the amplitude of the ripples.
	 * 
	 * @param amplitude - the amplitude
	 * @see #getAmplitude
	 */
	public void setAmplitude(float amplitude) {
		this.amplitude = amplitude;
	}

	
	/**
	 * Gets the amplitude of the ripples.
	 * 
	 * @return the amplitude
	 * @see #setAmplitude
	 */
	public float getAmplitude() {
		return amplitude;
	}

	
	/**
	 * Sets the phase of the ripples.
	 * 
	 * @param phase - the phase
	 * @see #getPhase
	 */
	public void setPhase(float phase) {
		this.phase = phase;
	}

	
	/**
	 * Gets the phase of the ripples.
	 * 
	 * @return the phase
	 * @see #setPhase
	 */
	public float getPhase() {
		return phase;
	}

	
	/**
	 * Sets the center of the effect in the X direction as a proportion of the
	 * image size.
	 * 
	 * @param centreX - the center
	 * @see #getCentreX
	 */
	public void setCentreX(float centreX) {
		this.centreX = centreX;
	}

	
	/**
	 * Gets the center of the effect in the X direction as a proportion of the
	 * image size.
	 * 
	 * @return the center
	 * @see #setCentreX
	 */
	public float getCentreX() {
		return centreX;
	}

	/**
	 * Sets the center of the effect in the Y direction as a proportion of the
	 * image size.
	 * 
	 * @param centreY - the center
	 * @see #getCentreY
	 */
	public void setCentreY(float centreY) {
		this.centreY = centreY;
	}

	
	/**
	 * Gets the center of the effect in the Y direction as a proportion of the
	 * image size.
	 * 
	 * @return the center
	 * @see #setCentreY
	 */
	public float getCentreY() {
		return centreY;
	}

	
	/**
	 * Sets the center of the effect as a proportion of the image size.
	 * 
	 * @param centre - the center
	 * @see #getCentre
	 */
	public void setCentre(Point2D centre) {
		this.centreX = (float) centre.getX();
		this.centreY = (float) centre.getY();
	}

	
	/**
	 * Gets the center of the effect as a proportion of the image size.
	 * 
	 * @return the center
	 * @see #setCentre
	 */
	public Point2D getCentre() {
		return new Point2D.Float(centreX, centreY);
	}

	/**
	 * Sets the radius of the effect.
	 * 
	 * @param radius - the radius
	 * @min-value 0
	 * @see #getRadius
	 */
	public void setRadius(float radius) {
		Validator.validateFloatInRange(radius, 0, Float.MAX_VALUE);
		this.radius = radius;
	}

	/**
	 * Gets the radius of the effect.
	 * 
	 * @return the radius
	 * @see #setRadius
	 */
	public float getRadius() {
		return radius;
	}

//	private boolean inside(int v, int a, int b) {
//		return a <= v && v <= b;
//	}

	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		icentreX = src.getWidth() * centreX;
		icentreY = src.getHeight() * centreY;
		if (radius == 0)
			radius = Math.min(icentreX, icentreY);
		radius2 = radius * radius;
		return super.filter(src, dst);
	}

	protected void transformInverse(int x, int y, float[] out) {
		float dx = x - icentreX;
		float dy = y - icentreY;
		float distance2 = dx * dx + dy * dy;
		if (distance2 > radius2) {
			out[0] = x;
			out[1] = y;
		} else {
			float distance = (float) Math.sqrt(distance2);
			float amount = amplitude * (float) Math.sin(distance / wavelength * ImageMath.TWO_PI - phase);
			amount *= (radius - distance) / radius;
			if (distance != 0)
				amount *= wavelength / distance;
			out[0] = x + dx * amount;
			out[1] = y + dy * amount;
		}
	}

	public String toString() {
		return "Distort/Water Ripples...";
	}
}
