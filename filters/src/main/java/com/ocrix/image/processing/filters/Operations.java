/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters;

/**
 * Defines the image operations. Explanation has been taking for: <a
 * href="http://en.wikipedia.org/wiki/Blend_modes">1</a> or <a
 * href="http://mapbox.com/tilemill/docs/guides/comp-op/">2</a>
 */
public enum Operations {
	/**
	 * f(a,b) = b. Where a is the value of a color channel in the underlying
	 * layer, and b is that of the corresponding channel of the upper layer. The
	 * result is most typically merged into the bottom layer using "simple" (b
	 * over a) alpha compositing, but other Porter-Duff operations are
	 * possible.[1] The compositing step results in the top layer's shape, as
	 * defined by its alpha channel, appearing over the bottom layer.
	 */
	BLEND,

	/**
	 * Adds the color of the source to the destination. For example, if your
	 * source color is dark red, this operation will add a small amount of red
	 * color to the destination causing it to brighten and also turn red. The
	 * lighter your source color, the lighter your result will be because a lot
	 * of color will be added. A completely black source will not affect the
	 * destination at all because no color will be added. Using this mode on
	 * darker source layers is recommended.
	 */
	ADD,

	/**
	 * This blend mode simply subtracts pixel values of one layer with the
	 * other. In case of negative values, black is displayed.
	 */
	SUBTRACT,

	/**
	 * Difference subtracts the top layer from the bottom layer or the other way
	 * round, to always get a positive value. Blending with black produces no
	 * change, as values for all colors are 0. (The RGB value for black is
	 * 0,0,0). Blending with white inverts the picture. One of the main
	 * utilities for this is during the editing process, when it can be used to
	 * verify alignment of pictures with similar content. Exclusion is a very
	 * similar blend mode with lower contrast.
	 */
	DIFFERENCE,

	/**
	 * f(a,b) = a * b. Multiply blend mode multiplies the numbers for each pixel
	 * of the top layer with the corresponding pixel for the bottom layer. The
	 * result is a darker picture. One of the many uses for multiply is to
	 * simulate the way ink colors would blend with each other or with a
	 * textured surface. It can also be used for other kinds of texure effects.
	 */
	MULTIPLY,

	/**
	 * Darken Only creates a resultant pixel that retains the smallest
	 * components of the foreground and background pixels. If the foreground
	 * pixel has the components r1, g1, and b1, and the background has r2, g2,
	 * b2, the resultant pixel is:
	 * <code>[min(r1, r2), min(g1, g2), min(b1, b2)]</code>. Compares the
	 * individual red, green, and blue components of the source and destination
	 * and takes the lower of each. This operation can be useful when applied to
	 * textures or raster layers.
	 */
	DARKEN,

	/**
	 * Linear burn - sums the value in the two layers and subtracts 1. This is
	 * the same as inverting each layer, adding them together (as in Linear
	 * Dodge), and then inverting the result. Blending with white leaves the
	 * image unchanged.
	 */
	BURN,

	/**
	 * Color mode - divides the inverted bottom layer by the top layer, and then
	 * inverts the result. This darkens the top layer increasing the contrast to
	 * reflect the colour of the bottom layer. The darker the bottom layer, the
	 * more its colour is used. Blending with white produces no difference.
	 */
	COLOR_BURN,

	/**
	 * Inverts both layers, multiplies them, and then inverts that result. will
	 * paint white pixels from the source over the destination, but black pixels
	 * Will have no affect. This operation can be useful when applied to
	 * textures or raster layers.
	 */
	SCREEN,

	/**
	 * Lighten Only has the opposite action of Darken Only. It selects the
	 * maximum of each component from the foreground and background pixels. The
	 * mathematical expression for Lighten Only is:
	 * <code>[max(r1, r2), max(g1, g2), max(b1, b2)]</code>. In other words,
	 * compares the individual red, green, and blue components of the source and
	 * destination and takes the higher of each.
	 */
	LIGHTEN,

	/**
	 * Linear dodge - sums the values in the two layers. Blending with white
	 * gives white. Blending with black does not change the image. 
	 */
	DODGE,
	
	/**
	 * Divides the bottom layer by the inverted top layer. This decreases the
	 * contrast to make the bottom layer reflect the top layer: the brighter the
	 * top layer, the more its colour affects the bottom layer. Blending with
	 * white gives white. Blending with black does not change the image. This
	 * effect is similar to changing the white point. The operation is not
	 * invertible. brightens the colors of the destination based on the source.
	 * The lighter the source, the more intense the effect. You’ll get nicer
	 * results when using this on dark to mid-tone colors, otherwise the colors
	 * can become too intense.
	 */
	COLOR_DODGE,
	
	/**
	 * Preserves the luma and chroma of the bottom layer, while adopting the hue
	 * of the top layer. Applies the hue of the source pixels to the destination
	 * pixels, keeping the destination saturation and value.
	 */
	HUE,

	/**
	 * Preserves the luma and hue of the bottom layer, while adopting the chroma
	 * of the top layer. Applies the saturation of the source pixels to the
	 * destination pixels, keeping the destination hue and value.
	 */
	SATURATION,

	/**
	 * Applies the value of the source pixels to the destination pixels, keeping
	 * the destination hue and saturation.
	 */
	VALUE,

	/**
	 * Applies the saturation of the source pixels to the destination pixels,
	 * keeping the destination hue and value.
	 */
	COLOR,

	/**
	 * Combines the colors from the source image, and also uses them to
	 * exaggerate the brightness or darkness of the destination. Overlay is one
	 * of a few composite operations that works well for texturing, including
	 * using it for terrain data layers. Overlay combines Multiply and Screen
	 * blend modes.[2] Light parts of the picture become lighter and dark parts
	 * become darker. An overlay with the same picture looks like an S-curve.
	 * <pre>
	 *           [ 2ab,                  if(a < 0.5)
	 * f(a, b) = [
	 *           [ 1 -2(1 - a)(1 - b),   otherwise
	 * </pre>
	 * where a is the base layer value & b is the blend mode.
	 * 
	 */
	OVERLAY,

	/**
	 * This is a softer version of Overlay. Applying pure black or white does
	 * not result in pure black or white[examples needed. There are a variety of
	 * different methods of applying a soft light blend.The formula used by
	 * Photoshop has a discontinuity[dubious – discuss], and other formulas
	 * correct it. Photoshop's formula is:
	 * 
	 * <pre>
	 *                     [ 2ab + pow(2,a)(1 - 2b),      if(b < 0.5)
	 * f_photoshop(a, b) = [
	 *                     [ 2a(1 - b) + sqrt(a)(2b - 1), otherwise
	 * </pre>
	 * 
	 * The formula specified by recent W3C drafts for SVG and Canvas is
	 * mathematically equivalent to the Photoshop formula with a small variation
	 * where b ≥ 0.5 and a ≤ 0.25:
	 * 
	 * <pre>
	 *               [ a - 1(1 - 2b) * a * (1 - a)      if(b <= 0.5)
	 * f_w3c(a, b) = [
	 *               [ a + (2b - 1) * (g_w3c(a) - a)    otherwise
	 * </pre>
	 * where
	 * <pre>
	 *            [ ((16a - 12) * a + 4) * a      if(a <= 0.25)
	 * g_w3c(a) = [
	 *            [ sqrt(a)                       otherwise
	 * </pre>
	 */
	SOFT_LIGHT,

	/**
	 * Hard Light combines Multiply and Screen blend modes. Equivalent to
	 * Overlay, but with the bottom and top images swapped.
	 */
	HARD_LIGHT,

	/**
	 * 
	 */
	PIN_LIGHT,

	/**
	 * Exclusion is a very similar to DIFFERENCE with lower contrast.
	 */
	EXCLUSION,

	/**
	 * 
	 */
	NEGATION,

	/**
	 * 
	 */
	AVERAGE,

	/**
	 * 
	 */
	STENCIL,

	/**
	 * 
	 */
	SILHOUETTE;
}
