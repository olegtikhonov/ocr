/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.filters.edgedetecting;

import com.ocrix.image.processing.filters.Validator;

/**
 * Performs Sobel edge detecting. The Sobel operator is a classic first order
 * edge detection operator that finds contrast by a process akin with
 * differentiation. Here we detect the magnitude of the edges by convolving two
 * 3*3 templates with the grey level image.
 * 
 * The source has been taken from: <a href="http://users.ecs.soton.ac.uk/msn/book/new_demo/sobel/">soton.ac.uk</a>
 */
// ---------------------------------------------------------
// 
// ---------------------------------------------------------
public class Sobel {
	/* Class member declarations */
	private int[] input;
	private int[] output;
	private float[] template = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };
	private int progress;
	private int templateSize = 3;
	private int width;
	private int height;
	private double[] direction;

	public Sobel() {
		progress = 0;
	}

	public void init(int[] original, int widthIn, int heightIn) {
		Validator.validateIntArrayEmptiness(original);
		Validator.validateIntNotZero(widthIn, heightIn);
		width = widthIn;
		height = heightIn;
		input = new int[width * height];
		output = new int[width * height];
		direction = new double[width * height];
		input = original;
	}

	public int[] process() {
		float[] GY = new float[width * height];
		float[] GX = new float[width * height];
		int[] total = new int[width * height];
		progress = 0;
		int sum = 0;
		int max = 0;

		for (int x = (templateSize - 1) / 2; x < width - (templateSize + 1) / 2; x++) {
			progress++;
			for (int y = (templateSize - 1) / 2; y < height
					- (templateSize + 1) / 2; y++) {
				sum = 0;

				for (int x1 = 0; x1 < templateSize; x1++) {
					for (int y1 = 0; y1 < templateSize; y1++) {
						int x2 = (x - (templateSize - 1) / 2 + x1);
						int y2 = (y - (templateSize - 1) / 2 + y1);
						float value = (input[y2 * width + x2] & 0xff)
								* (template[y1 * templateSize + x1]);
						sum += value;
					}
				}
				GY[y * width + x] = sum;
				for (int x1 = 0; x1 < templateSize; x1++) {
					for (int y1 = 0; y1 < templateSize; y1++) {
						int x2 = (x - (templateSize - 1) / 2 + x1);
						int y2 = (y - (templateSize - 1) / 2 + y1);
						float value = (input[y2 * width + x2] & 0xff)
								* (template[x1 * templateSize + y1]);
						sum += value;
					}
				}
				GX[y * width + x] = sum;
			}
		}
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				total[y * width + x] = (int) Math.sqrt(GX[y * width + x]
						* GX[y * width + x] + GY[y * width + x]
						* GY[y * width + x]);
				direction[y * width + x] = Math.atan2(GX[y * width + x], GY[y
						* width + x]);
				if (max < total[y * width + x])
					max = total[y * width + x];
			}
		}
		float ratio = (float) max / 255;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				sum = (int) (total[y * width + x] / ratio);
				output[y * width + x] = 0xff000000 | ((int) sum << 16
						| (int) sum << 8 | (int) sum);
			}
		}
		progress = width;
		return output;
	}

	public double[] getDirection() {
		return direction;
	}

	public int getProgress() {
		return progress;
	}
}
