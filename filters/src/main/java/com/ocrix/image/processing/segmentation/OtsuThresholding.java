/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.image.processing.segmentation;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class OtsuThresholding {
	// TODO: remove un-relevant methods.
	protected double getOtsuThreshold(int[] grayScaleValues) {
		int[] n = getHistogram(grayScaleValues);
		double[] p = getProbabilities(n, grayScaleValues.length);
		double[] Wo = getWo(p);
		double W = getW(p);
		double[] W1 = getW1(Wo, W);
		double UT = getUT(p);
		double[] Ut = getUt(p);
		double[] Uo = getUo(Ut, Wo);
		double[] U1 = getU1(UT, Ut, Uo);
		double sigmaSqrT = getSigmaSqrT(UT, p);
		double[] sigmaSqrBt = getSigmaSqrBt(Wo, W1, U1, Uo);
		double[] eta = getEta(sigmaSqrBt, sigmaSqrT);

		return getMaxIndex(eta);

	}

	private int[] getHistogram(int[] grayScaleValues) {
		int[] histogram = new int[256];

		for (int index = 0; index < grayScaleValues.length; index++) {
			histogram[grayScaleValues[index]]++;
		}
		return histogram;
	}

	   // Returns histogram of grayscale image
	private int[] imageHistogram(BufferedImage input) {

		int[] histogram = new int[256];

		for (int i = 0; i < histogram.length; i++)
			histogram[i] = 0;

		for (int i = 0; i < input.getWidth(); i++) {
			for (int j = 0; j < input.getHeight(); j++) {
				int red = new Color(input.getRGB(i, j)).getRed();
				histogram[red]++;
			}
		}

		return histogram;

	}
	
	
	private double[] getProbabilities(int[] histogram, int totalPixels) {

		double[] probability = new double[histogram.length];

		for (int index = 0; index < probability.length; index++) {
			probability[index] = ((double) histogram[index])
					/ ((double) totalPixels);
		}

		return probability;
	}

	private double[] getWo(double[] probability) {

		double[] Wo = new double[probability.length];
		Wo[0] = probability[0];

		for (int index = 1; index < Wo.length; index++) {
			Wo[index] = Wo[index - 1] + probability[index];
		}

		return Wo;
	}

	private double getW(double[] probability) {

		double W = 0;

		for (int index = 0; index < probability.length; index++) {
			W += probability[index];
		}

		return W;
	}

	private double[] getW1(double[] Wo, double W) {

		double[] W1 = new double[Wo.length];

		for (int index = 0; index < W1.length; index++) {
			W1[index] = W - Wo[index];
		}

		return W1;
	}

	private double getUT(double[] probability) {

		double UT = 0;

		for (int index = 0; index < probability.length; index++) {
			UT += (((double) index) * probability[index]);
		}

		return UT;

	}

	private double[] getUt(double[] probability) {

		double[] Ut = new double[probability.length];

		Ut[0] = 0;
		for (int index = 1; index < probability.length; index++) {
			Ut[index] = Ut[index - 1] + (((double) index) * probability[index]);
		}

		return Ut;
	}

	private double[] getUo(double[] Ut, double[] Wo) {

		double[] Uo = new double[Ut.length];

		for (int index = 0; index < Ut.length; index++) {
			Uo[index] = Ut[index] / Wo[index];
		}

		return Uo;

	}

	private double[] getU1(double UT, double[] Ut, double[] Uo) {

		double[] U1 = new double[Ut.length];

		for (int index = 0; index < U1.length; index++) {
			U1[index] = (UT - Ut[index]) / (1 - Uo[index]);
		}

		return U1;

	}

	private double getSigmaSqrT(double UT, double[] probability) {

		double sigmaSqrT = 0;

		for (int index = 0; index < probability.length; index++) {
			sigmaSqrT += (Math.pow((index - UT), 2) * probability[index]);
		}

		return sigmaSqrT;

	}

	private double[] getSigmaSqrBt(double[] Wo, double[] W1, double[] U1, double[] Uo) {
		double sigmaSqrBt[] = new double[Wo.length];

		for (int index = 0; index < sigmaSqrBt.length; index++) {
			sigmaSqrBt[index] = Wo[index] * W1[index] * Math.pow((U1[index] - Uo[index]), 2);
		}

		return sigmaSqrBt;
	}

	private int getMaxIndex(double[] array) {

		int maxIndex = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[maxIndex] < array[i]) {
				maxIndex = i;
			}
		}
		return maxIndex;

	}

	private double[] getEta(double[] sigmaSqrBt, double sigmaSqrT) {
		double eta[] = new double[sigmaSqrBt.length];
		for (int index = 0; index < sigmaSqrBt.length; index++) {
			eta[index] = sigmaSqrBt[index] / sigmaSqrT;
		}
		return eta;
	}
	
    // Gets binary threshold using Otsu's method
	public int otsuTreshold(BufferedImage original) {

		int[] histogram = imageHistogram(original);
		int total = original.getHeight() * original.getWidth();

		float sum = 0;
		
		for (int i = 0; i < 256; i++){
			sum += i * histogram[i];
		}

		float sumB = 0;
		int wB = 0;
		int wF = 0;

		float varMax = 0;
		int threshold = 0;

		for (int i = 0; i < 256; i++) {
			wB += histogram[i];
			
			if (wB == 0){
				continue;
			}
			wF = total - wB;

			if (wF == 0){
				break;
			}

			sumB += (float) (i * histogram[i]);
			float mB = sumB / wB;
			float mF = (sum - sumB) / wF;

			float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);

			if (varBetween > varMax) {
				varMax = varBetween;
				threshold = i;
			}
		}

		return threshold;
	}
}
