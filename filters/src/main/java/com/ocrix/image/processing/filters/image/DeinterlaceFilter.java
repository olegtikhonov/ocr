/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.ocrix.image.processing.filters.image;

import java.awt.image.*;

import com.ocrix.image.processing.filters.Validator;



/**
 * A filter for de-interlacing video frames. 
 * <br>Wiki suggests: Deinterlacing is the
 * process of converting interlaced video, such as common analog television
 * signals or 1080i format HDTV signals, into a non-interlaced form.
 * <a href="http://en.wikipedia.org/wiki/Deinterlacing">for more info ...</a>
 * 
 */
public class DeinterlaceFilter extends AbstractBufferedImageOp {
	
	/* Defines modes */
	enum OPS { EVEN, ODD, AVERAGE };

	/* Default mode */
	private int mode = OPS.EVEN.ordinal();

	/**
	 * Sets a mode <br>
	 * The value should be 0,1 or 2
	 * 
	 * @param mode
	 */
	public void setMode(int mode) {
		Validator.validateIntInRange(mode, 0, 2);
		this.mode = mode;
	}

	public int getMode() {
		return mode;
	}

	/**
	 * Applies the filter. 
	 * <br>The second param can be null, otherwise should have the same size as src image 
	 */
	public BufferedImage filter(BufferedImage src, BufferedImage dst) {
		Validator.validateBufferedImage(src);
		int width = src.getWidth();
		int height = src.getHeight();

		if (dst == null)
			dst = createCompatibleDestImage(src, null);

		int[] pixels = null;

		if (mode == OPS.EVEN.ordinal()) {
			for (int y = 0; y < height - 1; y += 2) {
				pixels = getRGB(src, 0, y, width, 1, pixels);
				if (src != dst)
					setRGB(dst, 0, y, width, 1, pixels);
				setRGB(dst, 0, y + 1, width, 1, pixels);
			}
		} else if (mode == OPS.ODD.ordinal()) {
			for (int y = 1; y < height; y += 2) {
				pixels = getRGB(src, 0, y, width, 1, pixels);
				if (src != dst)
					setRGB(dst, 0, y, width, 1, pixels);
				setRGB(dst, 0, y - 1, width, 1, pixels);
			}
		} else if (mode == OPS.AVERAGE.ordinal()) {
			int[] pixels2 = null;
			for (int y = 0; y < height - 1; y += 2) {
				pixels = getRGB(src, 0, y, width, 1, pixels);
				pixels2 = getRGB(src, 0, y + 1, width, 1, pixels2);
				for (int x = 0; x < width; x++) {
					int rgb1 = pixels[x];
					int rgb2 = pixels2[x];
					int a1 = (rgb1 >> 24) & 0xff;
					int r1 = (rgb1 >> 16) & 0xff;
					int g1 = (rgb1 >> 8) & 0xff;
					int b1 = rgb1 & 0xff;
					int a2 = (rgb2 >> 24) & 0xff;
					int r2 = (rgb2 >> 16) & 0xff;
					int g2 = (rgb2 >> 8) & 0xff;
					int b2 = rgb2 & 0xff;
					a1 = (a1 + a2) / 2;
					r1 = (r1 + r2) / 2;
					g1 = (g1 + g2) / 2;
					b1 = (b1 + b2) / 2;
					pixels[x] = (a1 << 24) | (r1 << 16) | (g1 << 8) | b1;
				}
				setRGB(dst, 0, y, width, 1, pixels);
				setRGB(dst, 0, y + 1, width, 1, pixels);
			}
		}

		return dst;
	}

	public String toString() {
		return this.getClass().getName() + ", mode=" + getMode();
	}
}
