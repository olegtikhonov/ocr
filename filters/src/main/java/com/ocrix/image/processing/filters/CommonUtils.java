/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.image.processing.filters;

import static com.ocrix.image.processing.filters.CommonMessage.DEFAULT_BUF_IMAGE;
import static com.ocrix.image.processing.filters.CommonMessage.FLOAT_NULL;
import static com.ocrix.image.processing.filters.CommonMessage.IMAGE_NULL;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.sanselan.ImageFormat;
import org.apache.sanselan.ImageWriteException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.SanselanConstants;
import org.apache.sanselan.formats.tiff.constants.TiffConstants;

/**
 * Util class, contains of common methods.
 */
public final class CommonUtils {

	/* Local logger */
	private final static Logger log = Logger.getLogger(CommonUtils.class
			.getName());

	private CommonUtils() {
	}

	/**
	 * Fetches a {@link ColorModel}
	 * 
	 * @param image
	 *            a {@link BufferedImage}
	 * 
	 * @return a {@link ColorModel}
	 */
	public static ColorModel getColorModel(Image image) {
		/* If buffered image, the color model is readily available */
		if (image instanceof BufferedImage) {
			BufferedImage bimage = (BufferedImage) image;
			return bimage.getColorModel();
		}

		/*
		 * Use a pixel grabber to retrieve the image's color model; grabbing a
		 * single pixel is usually sufficient
		 */
		PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
			log.error(stackToString(e, 2));
		}
		ColorModel cm = pg.getColorModel();
		return cm;
	}

	/**
	 * Creates byte array by generating pixels.
	 * 
	 * @param w
	 *            - a width.
	 * @param h
	 *            - an height
	 * @param loc
	 *            - a location.
	 * 
	 * @return generated array of pixels.
	 */
	public static byte[] generatePixels(int w, int h, Rectangle2D.Float loc) {
		if (loc != null) {
			float xmin = loc.x;
			float ymin = loc.y;
			float xmax = loc.x + loc.width;
			float ymax = loc.y + loc.height;

			byte[] pixels = new byte[w * h];
			int pIx = 0;
			float[] p = new float[w];
			float q = ymin;
			float dp = (xmax - xmin) / w;
			float dq = (ymax - ymin) / h;

			p[0] = xmin;
			for (int i = 1; i < w; i++) {
				p[i] = p[i - 1] + dp;
			}

			for (int r = 0; r < h; r++) {
				for (int c = 0; c < w; c++) {
					int color = 1;
					float x = 0.0f;
					float y = 0.0f;
					float xsqr = 0.0f;
					float ysqr = 0.0f;
					do {
						xsqr = x * x;
						ysqr = y * y;
						y = 2 * x * y + q;
						x = xsqr - ysqr + p[c];
						color++;
					} while (color < 512 && xsqr + ysqr < 4);
					pixels[pIx++] = (byte) (color % 16);
				}
				q += dq;
			}
			return pixels;
		} else {
			log.warn(FLOAT_NULL.to());
		}
		return null;
	}

	/**
	 * Save {@link BufferedImage} as file extension.
	 * 
	 * @param imageToBeSaved
	 * @param pathPrefix
	 *            where to save.
	 * @param fileExtension
	 *            type of the image (.tiff, .gpeg, .bmp etc).
	 */
	public static void saveBufferedImage(BufferedImage imageToBeSaved,
			String pathPrefix, String fileExtension) {
		Validator.validateBufferedImage(imageToBeSaved);
		Validator.validateStringArray(pathPrefix, fileExtension);

		if (fileExtension.equals("tiff") || fileExtension.equals("tif")) {
			try {
				saveBufferedImageAsTIFF(imageToBeSaved, pathPrefix, fileExtension);
			} catch (ImageWriteException e) {
				log.error(e.getMessage());
			} catch (IOException e) {
				log.error(e.getMessage());
			}
		} else {
			File savedImg = new File(pathPrefix + "." + fileExtension);

			if (savedImg.exists()) {
				savedImg.delete();
			}

			try {
				ImageIO.write(imageToBeSaved, fileExtension, savedImg);
			} catch (IOException e) {
				log.error("Could not write an image due to " + e.getMessage());
			}
		}
	}
	
	/**
	 * Creates compatible {@link BufferedImage}.
	 * 
	 * @param src will be taken as a template.
	 * 
	 * @return newly created {@link BufferedImage}.
	 */
	public static BufferedImage createCompatibleImage(BufferedImage src) {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();
		return gc.createCompatibleImage(src.getWidth(), src.getHeight(), Transparency.BITMASK);
	}

	/**
	 * Saves {@link BufferedImage} as TIFF image.
	 * 
	 * @param imageToBeSaved
	 *            an original {@link BufferedImage}.
	 * @param pathPrefix
	 *            - where to save.
	 * @param fileExtension
	 *            - image that be converted extension.
	 * 
	 * @throws ImageWriteException
	 * @throws IOException
	 */
	public static void saveBufferedImageAsTIFF(BufferedImage imageToBeSaved,
			String pathPrefix, String fileExtension)
			throws ImageWriteException, IOException {

		if (imageToBeSaved != null) {
			ImageFormat format = ImageFormat.IMAGE_FORMAT_TIFF;
			Map<String, Integer> params = new HashMap<String, Integer>();

			// set optional parameters if you like
			params.put(SanselanConstants.PARAM_KEY_COMPRESSION, new Integer(
					TiffConstants.TIFF_COMPRESSION_UNCOMPRESSED));

			File savedImg = new File(pathPrefix + "." + fileExtension);
			if (savedImg.exists()) {
				savedImg.delete();
			}
			OutputStream out = new FileOutputStream(savedImg);
			out.write(Sanselan
					.writeImageToBytes(imageToBeSaved, format, params));
			out.close();
		} else {
			log.warn(IMAGE_NULL.to());
		}
	}

	/**
	 * Resizes the {@link BufferedImage}.
	 * 
	 * @param destOrg
	 *            - original image.
	 * @param width
	 *            - the desired width.
	 * @param height
	 *            - the desired height.
	 * @param imageType
	 *            - the image type.
	 * 
	 * @return resized {@link BufferedImage}.
	 */
	public static BufferedImage resizeImage(BufferedImage destOrg, int width, int height, int imageType) {
		Validator.validateBufferedImage(destOrg);
		Validator.validateIntPositiveness(width, height, imageType);

		BufferedImage resizedImage = new BufferedImage(width, height, imageType);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(destOrg, 0, 0, width, height, null);
		g.dispose();
		return resizedImage;
	}
	

	/**
	 * Transforms {@link Image} to {@link BufferedImage}.
	 * 
	 * @param src
	 *            an {@link Image} to be transformed.
	 * 
	 * @return a {@link BufferedImage}.
	 */
	public static BufferedImage toBufferedImage(Image src) {
		BufferedImage dest = null;
		if (src != null) {
			int w = src.getWidth(null);
			int h = src.getHeight(null);
			int type = BufferedImage.TYPE_INT_RGB; // other options
			dest = new BufferedImage(w, h, type);
			Graphics2D g2 = dest.createGraphics();
			g2.drawImage(src, 0, 0, null);
			g2.dispose();

		} else {
			log.warn(IMAGE_NULL.to());
		}
		return dest;
	}

	/**
	 * Converts a {@link BufferedImage} to the {@link Image}
	 * 
	 * @param bufImage
	 *            a buffered image to be converted
	 * 
	 * @return an {@link Image}
	 * 
	 * @throws IllegalArgumentException
	 *             if the bufImage is NULL.
	 */
	public static Image toImage(BufferedImage bufImage) {
		Validator.validateBufferedImage(bufImage);
		return Toolkit.getDefaultToolkit().createImage(bufImage.getSource());
	}

	/**
	 * Converts {@link BufferedImage} to Gray-scaled.
	 * 
	 * @param src
	 * @return
	 */
	public static BufferedImage convertToGrayScale(BufferedImage src) {
		if (src != null) {
			BufferedImage image = new BufferedImage(src.getWidth(),
					src.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
			Graphics g = image.getGraphics();
			g.drawImage(src, 0, 0, null);
			g.dispose();
			return image;
		} else {
			log.warn(DEFAULT_BUF_IMAGE.to());
			/* Instead of returning null */
			return new BufferedImage(1, 1, 1);
		}
	}

	/**
	 * Converts exception to string representation by traversal stack trace.
	 * 
	 * @param e
	 *            an exception to be converted.
	 * @param depth
	 *            of the stack trace to be traveled.
	 * 
	 * @return textual representation of the exception.
	 */
	public static String stackToString(Exception e, int depth) {
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] elems = e.getStackTrace();
		for (StackTraceElement el : elems) {
			if (depth >= 0) {
				sb.append(el.getClassName());
				sb.append(" ");
				sb.append(el.getMethodName());
				sb.append(" ");
				sb.append(el.getLineNumber());
				sb.append(" ");
			} else {
				break;
			}
			depth--;
		}

		return sb.toString();
	}
}
