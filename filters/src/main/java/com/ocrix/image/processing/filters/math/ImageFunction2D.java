/*
Copyright 2006 Jerry Huxtable

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.ocrix.image.processing.filters.math;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;
import org.apache.log4j.Logger;
import com.ocrix.image.processing.filters.image.ImageMath;
import com.ocrix.image.processing.filters.image.PixelUtils;


/**
 * Defines an image function 2d.
 */
public class ImageFunction2D implements Function2D {
	public final static int ZERO = 0;
	public final static int CLAMP = 1;
	public final static int WRAP = 2;
	
	protected int[] pixels;
	protected int width;
	protected int height;
	protected int edgeAction = ZERO;
	protected boolean alpha = false;
	private static final Logger LOG = Logger.getLogger(ImageFunction2D.class);

	/**
	 * Constructs an {@link ImageFunction2D}.
	 * 
	 * @param image to be used.
	 * Note: alpha is <code>false</code.>
	 */
	public ImageFunction2D(BufferedImage image) {
		this(image, false);
	}
	
	/**
	 * Constructs an {@link ImageFunction2D}.
	 * 
	 * @param image
	 * @param alpha
	 * Note: The action is ZERO.
	 */
	public ImageFunction2D(BufferedImage image, boolean alpha) {
		this(image, ZERO, alpha);
	}
	
	/**
	 * Constructs an {@link ImageFunction2D}.
	 * 
	 * @param image
	 * @param edgeAction
	 * @param alpha
	 */
	public ImageFunction2D(BufferedImage image, int edgeAction, boolean alpha) {
		init( getRGB( image, 0, 0, image.getWidth(), image.getHeight(), null), image.getWidth(), image.getHeight(), edgeAction, alpha);
	}
	
	/**
	 * Constructs an {@link ImageFunction2D}..
	 * 
	 * @param pixels
	 * @param width
	 * @param height
	 * @param edgeAction
	 * @param alpha
	 */
	public ImageFunction2D(int[] pixels, int width, int height, int edgeAction, boolean alpha) {
		init(pixels, width, height, edgeAction, alpha);
	}
	
	/**
	 * Constructs an {@link ImageFunction2D}.
	 * 
	 * @param image
	 * Note: the alpha is false & action - ZERO
	 */
 	public ImageFunction2D(Image image) {
		this( image, ZERO, false );
 	}
	
 	/**
 	 * Constructs an {@link ImageFunction2D}.
 	 * 
 	 * @param image
 	 * @param edgeAction
 	 * @param alpha
 	 */
	public ImageFunction2D(Image image, int edgeAction, boolean alpha) {
 		PixelGrabber pg = new PixelGrabber(image, 0, 0, -1, -1, null, 0, -1);
 		try {
 			pg.grabPixels();
 		} catch (InterruptedException e) {
 			LOG.error("interrupted waiting for pixels!");
 			throw new RuntimeException("interrupted waiting for pixels!");
 		}
 		if ((pg.status() & ImageObserver.ABORT) != 0) {
 			LOG.error("image fetch aborted");
 			throw new RuntimeException("image fetch aborted");
 		}
 		init((int[])pg.getPixels(), pg.getWidth(), pg.getHeight(), edgeAction, alpha);
  	}

	/**
	 * A convenience method for getting ARGB pixels from an image. This tries to avoid the performance
	 * penalty of BufferedImage.getRGB unmanaging the image.
	 */
	public int[] getRGB( BufferedImage image, int x, int y, int width, int height, int[] pixels ) {
		int type = image.getType();
		if ( type == BufferedImage.TYPE_INT_ARGB || type == BufferedImage.TYPE_INT_RGB )
			return (int [])image.getRaster().getDataElements( x, y, width, height, pixels );
		return image.getRGB( x, y, width, height, pixels, 0, width );
    }

	/**
	 * Initializes the {@link ImageFunction2D}.
	 * 
	 * @param pixels
	 * @param width
	 * @param height
	 * @param edgeAction
	 * @param alpha
	 */
	public void init(int[] pixels, int width, int height, int edgeAction, boolean alpha) {
		this.pixels = pixels;
		this.width = width;
		this.height = height;
		this.edgeAction = edgeAction;
		this.alpha = alpha;
	}
	
	/**
	 * 
	 */
	public float evaluate(float x, float y) {
		int ix = (int)x;
		int iy = (int)y;
		if (edgeAction == WRAP) {
			ix = ImageMath.mod(ix, width);
			iy = ImageMath.mod(iy, height);
		} else if (ix < 0 || iy < 0 || ix >= width || iy >= height) {
			if (edgeAction == ZERO)
				return 0;
			if (ix < 0)
				ix = 0;
			else if (ix >= width)
				ix = width-1;
			if (iy < 0)
				iy = 0;
			else if (iy >= height)
				iy = height-1;
		}
		return alpha ? ((pixels[iy*width+ix] >> 24) & 0xff) / 255.0f : PixelUtils.brightness(pixels[iy*width+ix]) / 255.0f;
	}
	
	/**
	 * Sets an edge action.
	 * Available actions are:
	 * <code>ZERO, WRAP, CLAMP</code>
	 * 
	 * @param edgeAction
	 */
	public void setEdgeAction(int edgeAction) {
		this.edgeAction = edgeAction;
	}

	/**
	 * Gets an edge action.
	 * 
	 * @return
	 */
	public int getEdgeAction() {
		return edgeAction;
	}

	
	/**
	 * Gets a width.
	 * 
	 * @return the width.
	 */
	public int getWidth() {
		return width;
	}
	
	
	/**
	 * Gets an height.
	 * 
	 * @return the height.
	 */
	public int getHeight() {
		return height;
	}
	
	
	/**
	 * Gets pixels.
	 * 
	 * @return the pixels.
	 */
	public int[] getPixels() {
		return pixels;
	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}
}

