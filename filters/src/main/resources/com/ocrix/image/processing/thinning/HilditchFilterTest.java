package com.ocrix.image.processing.thinning;


import static com.ocrix.image.processing.filters.composite.TestParams.CLOUDS;
import static com.ocrix.image.processing.filters.composite.TestParams.PREFIXPATH;
import static com.ocrix.image.processing.filters.composite.TestParams.TO_BE_SAVED;
import static org.junit.Assert.assertNotNull;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.image.processing.filters.CommonUtils;
import com.ocrix.image.processing.filters.TestUtils;
import com.ocrix.image.processing.thinning.HilditchFilter;


public class HilditchFilterTest {

	private static HilditchFilter hilditchFilter = null;
	private static BufferedImage src = null;
	
	@BeforeClass
	public static void setUp() throws Exception {
		src = TestUtils.convertToGrayScale(ImageIO.read(new File(PREFIXPATH + CLOUDS)));
		hilditchFilter = new HilditchFilter(src);
	}
	
	
	@Test
	public void testCitor(){
		assertNotNull(hilditchFilter);
	}
	
	@Test
	public void testHilditchFilter(){
		BufferedImage result = hilditchFilter.thin();
		assertNotNull(result);
		CommonUtils.saveBufferedImage(result, TO_BE_SAVED + "HilditchFilter", "jpeg");
	}

	@AfterClass
	public static void tearDown() throws Exception {
	}

}
