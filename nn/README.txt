Properties:
#########################################
# Sigma for Gaussian tf. default is 0.5d
#########################################
transferFunction.sigma 


#########################################
# Slope of linear tf. default is 1d
#########################################
transferFunction.slope


#########################################
# Output value for the high output level 
# default is 1d (RAMP tf)
#########################################
transferFunction.yHigh


#########################################
# Output value for the low output level 
# default is 0d (RAMP tf)
#########################################
transferFunction.yLow


#########################################
# Threshold for the high output level 
# default is 1d (RAMP tf)
#########################################
transferFunction.xHigh


#########################################
# Threshold for the high output level 
# default is 0d (RAMP tf)
#########################################
transferFunction.xLow



#########################################
# The point of trapezoid function 
# (Trapezoid tf)
#########################################
transferFunction.leftLow



#########################################
# The point of trapezoid function 
# (Trapezoid tf)
#########################################
transferFunction.leftHigh



#########################################
# The point of trapezoid function 
# (Trapezoid tf)
#########################################
transferFunction.rightLow



#########################################
# The point of trapezoid function 
# (Trapezoid tf)
#########################################
transferFunction.rightHigh

inputFunction

#########################################
# Transfer function type 
# Linear
#########################################
transferFunction.Linear

#########################################
# Transfer function type 
# Ramp
#########################################
transferFunction.Ramp


#########################################
# Transfer function type 
# Step
#########################################
transferFunction.Step

#########################################
# Transfer function type 
# Sigmoid
#########################################
transferFunction.Sigmoid

#########################################
# Transfer function type 
# Tanh
#########################################
transferFunction.Tanh

#########################################
# Transfer function type 
# Gaussian
#########################################
transferFunction.Gaussian

#########################################
# Transfer function type 
# Trapezoid
#########################################
transferFunction.Trapezoid

#########################################
# Transfer function type 
# Sgn
#########################################
transferFunction.Sgn

#########################################
# Transfer function type 
# Sin
#########################################
transferFunction.Sin

#########################################
# Transfer function type 
# Log
#########################################
transferFunction.Log

neuronType






#---------------------------------------------
# Useful mvn plug-ins
#---------------------------------------------
1. FInd bugs 
mvn findbugs:findbugs

2. Code coverage
mvn clean install cobertura:cobertura

3. Finding TODO tags
mvn clean install taglist:taglist

TODO:
Add logging




