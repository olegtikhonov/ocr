/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.learning.DynamicBackPropagation;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;



/**
 * Tests {@link DynamicBackPropagation} functionality.
 */
public class TestDynamicBackPropagation {
    /* Test's members */
    private DynamicBackPropagation dbp = null;
    private NeuralNetwork nn = null;
    private TrainingSet<SupervisedTrainingElement> ts = null;

    @Before
    public void setUp() throws Exception {
        nn = TestUtil.getDefaultNN();
        dbp = new DynamicBackPropagation();
        dbp.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDoLearningEpoch() {
        setWinner();
        dbp.doLearningEpoch(TestUtil.generateSupervisedTrainingSet(KHAMSA, KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochCurrentIterationBiggerThanZeroUseDynamicLearningRateTrue() {
        setPreconditions();
        dbp.setUseDynamicLearningRate(true);
        dbp.doLearningEpoch(ts);
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochCurrentIterationBiggerThanZeroUseDynamicLearningRateFalse() {
        setPreconditions();
        dbp.setUseDynamicLearningRate(false);
        dbp.doLearningEpoch(ts);
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochCurrentIterationBiggerThanZeroUseDynamicLearningRateFalseUseDynamicMomentumFalse() {
        setPreconditions();
        dbp.setUseDynamicLearningRate(false);
        dbp.setUseDynamicMomentum(false);
        dbp.doLearningEpoch(ts);
        assertTrue(true);
    }


    @Test
    public void testDynamicBackPropagation() {
        assertNotNull(dbp);
    }

    @Test
    public void testAdjustLearningRate() {
        dbp.adjustLearningRate();
        assertTrue(true);
    }

    @Test
    public void testAdjustMomentumMomentumBiggerMaxMomentum() {
        dbp.setMomentum(MAX);
        dbp.setMaxMomentum(MIN);
        dbp.adjustMomentum();
        assertTrue(true);
    }

    @Test
    public void testAdjustMomentumMomentumLessMaxMomentum() {
        dbp.setMomentum(MIN);
        dbp.setMaxMomentum(MAX);
        dbp.adjustMomentum();
        assertTrue(true);
    }

    @Test
    public void testAdjustMomentumMomentumLessMinMomentum() {
        dbp.setMomentum(MAX);
        dbp.setMinMomentum(MIN);
        dbp.adjustMomentum();
        assertTrue(true);
    }

    @Test
    public void testAdjustMomentum(){

    }

    @Test
    public void testGetLearningRateChange() {
        dbp.setLearningRateChange(WEIGHT);
        assertEquals(WEIGHT, dbp.getLearningRateChange(), EPSILON);
    }

    @Test
    public void testSetLearningRateChange() {
        testGetLearningRateChange();
    }

    @Test
    public void testGetMaxLearningRate() {
        dbp.setMaxLearningRate(MAX);
        assertEquals(MAX, dbp.getMaxLearningRate(), EPSILON);
    }

    @Test
    public void testSetMaxLearningRate() {
        testGetMaxLearningRate();
    }

    @Test
    public void testAdjustLearningRateLearningRateLessMaxLearningRate(){
        dbp.setLearningRate(MIN);
        dbp.setMaxLearningRate(MAX);
        dbp.adjustLearningRate();
        assertTrue(true);
    }

    @Test
    public void testAdjustLearningRateLearningRateBiggerMaxLearningRate(){
        dbp.setLearningRate(MAX);
        dbp.setMaxLearningRate(MIN);
        dbp.adjustLearningRate();
        assertTrue(true);
    }



    @Test
    public void testGetMaxMomentum() {
        dbp.setMaxMomentum(MAX);
        assertEquals(MAX, dbp.getMaxMomentum(), EPSILON);
    }

    @Test
    public void testSetMaxMomentum() {
        testGetMaxMomentum();
    }

    @Test
    public void testGetMinLearningRate() {
        dbp.setMinLearningRate(MIN);
        assertEquals(MIN, dbp.getMinLearningRate(), EPSILON);
    }

    @Test
    public void testSetMinLearningRate() {
        testGetMinLearningRate();
    }

    @Test
    public void testGetMinMomentum() {
        dbp.setMinMomentum(MIN);
        assertEquals(MIN, dbp.getMinMomentum(), EPSILON);
    }

    @Test
    public void testSetMinMomentum() {
        testGetMinMomentum();
    }

    @Test
    public void testGetMomentumChange() {
        dbp.setMomentumChange(WEIGHT);
        assertEquals(WEIGHT, dbp.getMomentumChange(), EPSILON);
    }

    @Test
    public void testSetMomentumChange() {
        testGetMomentumChange();
    }

    @Test
    public void testGetUseDynamicLearningRate() {
        dbp.setUseDynamicLearningRate(true);
        assertTrue(dbp.getUseDynamicLearningRate());
    }

    @Test
    public void testSetUseDynamicLearningRate() {
        testGetUseDynamicLearningRate();
        dbp.setUseDynamicLearningRate(false);
        assertFalse(dbp.getUseDynamicLearningRate());
    }

    @Test
    public void testGetUseDynamicMomentum() {
        dbp.setUseDynamicMomentum(true);
        assertTrue(dbp.getUseDynamicMomentum());
    }

    @Test
    public void testSetUseDynamicMomentum() {
        testGetUseDynamicMomentum();
        dbp.setUseDynamicMomentum(false);
        assertFalse(dbp.getUseDynamicMomentum());
    }

    private void setWinner(){
        for (Layer l : nn.getLayers()) {
            TestUtil.connectNeuronsCustomInput(l.getNeurons(), WEIGHT);
            CompetitiveNeuron cn = new CompetitiveNeuron(new InputFunction(), new Linear());
            for (Connection con : cn.getInputConnections()) {
                cn.getConnectionsFromOtherLayers().add(con);
                //                con.getWeight().setTrainingData(new MomentumBackpropagation().new MomentumWeightTrainingData());
                con.getWeight().setTrainingData(MomentumBackpropagation.MomentumWeightTrainingData.getInstance());
            }
            ((CompetitiveLayer) nn.getLayerAt(1)).setWinner(cn);
        }
    }

    private void setPreconditions(){
        setWinner();
        ts = TestUtil.generateSupervisedTrainingSet(KHAMSA, KHAMSA);
        dbp.learn(ts);
    }
}
