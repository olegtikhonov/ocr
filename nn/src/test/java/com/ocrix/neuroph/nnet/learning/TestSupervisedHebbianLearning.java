/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.SupervisedHebbianLearning;


/**
 * Tests {@link SupervisedHebbianLearning} functionality.
 */
public class TestSupervisedHebbianLearning {

    /* Tests members */
    private SupervisedHebbianLearning shl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        shl = new SupervisedHebbianLearning();
        shl.setNeuralNetwork(nn);
    }

    @Test
    public void testLearnPattern() {
        shl.learnPattern(new SupervisedTrainingElement(TestUtil.genDoubleVector(KHAMSA), TestUtil.genDoubleVector(KHAMSA)));
        assertNotNull(shl);
    }

    @Test
    public void testLearnPatternNull() {
        shl.logOn();
        shl.learnPattern(null);
        assertNotNull(shl);
    }

    @Test
    public void testToString() {
        assertTrue(shl.toString().contains(SupervisedHebbianLearning.class.getName()));
    }

    @Test
    public void testSupervisedHebbianLearning() {
        assertNotNull(shl);
    }

    @Test
    public void testUpdateNetworkWeights() {
        shl.updateNetworkWeights(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testUpdateNeuronWeights() {
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            shl.updateNeuronWeights(neuron, TestUtil.genDouble());
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testUpdateNetworkWeightsNull(){
        shl.logOn();
        shl.updateNetworkWeights(null);
        assertTrue(true);
        shl.logOff();
    }

    @After
    public void tearDown() throws Exception {
    }
}
