/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.core.transfer.Sin;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.core.transfer.Tanh;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;

/**
 * Tests the {@link CompetitiveNeuron} public functionality.
 */
public class TestCompetitiveNeuron {

    /* Class member */
    private CompetitiveNeuron neuron = null;

    @Before
    public void setUp() throws Exception {
        neuron = new CompetitiveNeuron(new InputFunction(), new Step());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        neuron = new CompetitiveNeuron(new InputFunction(), new Step());
        TestUtil.addInputConnection(neuron, KHAMSA);
        neuron.calculate();
        assertTrue(true);
    }

    @Test
    public void testReset() {
        boolean isComp = true;
        double input = TestUtil.genDouble();
        double output = TestUtil.genDouble();
        neuron.setInput(input);
        neuron.setOutput(output);
        neuron.setIsCompeting(isComp);

        neuron.reset();
        assertTrue(isComp != neuron.isCompeting());
        assertTrue(!new Double(input).equals(neuron.getNetInput()));
        assertTrue(!new Double(output).equals(neuron.getOutput()));
        assertTrue(true);
    }

    @Test
    public void testAddInputConnectionConnection() {
        Connection con = new Connection(new CompetitiveNeuron(
                new InputFunction(), new Step()), new CompetitiveNeuron(
                        new InputFunction(), new Step()));
        assertNotNull(con);
        neuron.addInputConnection(con);
        assertNotNull(neuron.getInputConnections());
        List<Connection> cons = neuron.getInputConnections();
        for (Connection cntn : cons) {
            assertNotNull(cntn);
        }
    }

    @Test
    public void testToString() {
        String result = neuron.toString();
        assertNotNull(result);
    }

    @Test
    public void testCompetitiveNeuron() {
        CompetitiveNeuron cn = new CompetitiveNeuron(new InputFunction(), new Tanh());
        assertNotNull(cn);
    }

    @Test
    public void testGetConnectionsFromOtherLayers() {
        Connection con = new Connection(new CompetitiveNeuron(new InputFunction(), new Sin()), new CompetitiveNeuron(new InputFunction(), new Gaussian()));
        Layer testLayer = new Layer();
        neuron.setParentLayer(testLayer);
        neuron.addInputConnection(con);

        List<Connection> result = neuron.getConnectionsFromOtherLayers();
        Connection res = result.get(0);
        assertEquals(con, res);
    }

    @Test
    public void testIsCompeting() {
        neuron.setIsCompeting(true);
        assertTrue(neuron.isCompeting());
    }

    @Test
    public void testSetIsCompeting() {
        testIsCompeting();
    }
}
