package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.util.TransferFunctionType.TANH;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.SupervisedHebbianNetwork;


/**
 * Tests a {@link SupervisedHebbianNetwork} public functionality.
 */
public class TestSupervisedHebbianNetwork {

    /* ------------- TEST AREA ------------- */
    @Test
    public void testSupervisedHebbianNetworkIntInt() {
        SupervisedHebbianNetwork shn = new SupervisedHebbianNetwork(KHAMSA, KHAMSA);
        assertNotNull(shn);
    }

    @Test
    public void testSupervisedHebbianNetworkIntIntTransferFunctionType() {
        SupervisedHebbianNetwork shn = new SupervisedHebbianNetwork(KHAMSA, KHAMSA, TANH);
        assertNotNull(shn);
    }

    @Test
    public void testToString(){
        String result = new SupervisedHebbianNetwork(KHAMSA, KHAMSA).toString();
        assertNotNull(result);
    }


    //---------------------------------------
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}
