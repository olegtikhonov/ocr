/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.flat;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.encog.engine.EncogEngineError;
import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.engine.network.activation.ActivationLOG;
import org.encog.engine.network.flat.FlatLayer;
import org.encog.engine.network.flat.FlatNetwork;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.flat.FlatLearningType;
import com.ocrix.neuroph.nnet.flat.FlatNetworkLearning;
import com.ocrix.neuroph.nnet.flat.FlatNetworkPlugin;

/**
 * Tests a {@link FlatNetworkLearning} public functionality.
 */
public class TestFlatNetworkLearning {

    /* Test member */
    private FlatNetworkLearning fnl = null;

    @Before
    public void setUp() throws Exception {
        FlatLayer flBiPolar = new FlatLayer(new ActivationBiPolar(), KHAMSA, 1,
                null);
        FlatLayer flLog = new FlatLayer(new ActivationLOG(), KHAMSA, 1, null);
        FlatLayer[] layers = { flBiPolar, flLog };
        /* Do not use default constructor */
        FlatNetwork fn = new FlatNetwork(layers);
        fnl = new FlatNetworkLearning(fn);
        fnl.setNumThreads(1);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetLearningRate() {
        double result = fnl.getLearningRate();
        assertTrue(result > NIL);
    }

    @Test
    public void testSetLearningRate() {
        fnl.setLearningRate(WEIGHT);
        assertEquals(WEIGHT, fnl.getLearningRate(), EPSILON);
    }

    // NPL :-(((
    // TODO:
    @Test
    public void testDoLearningEpoch() {
        TrainingSet<TrainingElement> trainingSet = TestUtil
        .generateTrainingSet(KHAMSA, KHAMSA);
        try {
            fnl.doLearningEpoch(trainingSet);
        } catch (Throwable e) {
            // FOR DEBUGGING
            // e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochLearningTypeManhattanUpdateRule() {
        TrainingSet<TrainingElement> trainingSet = TestUtil
        .generateTrainingSet(KHAMSA, KHAMSA);
        try {
            fnl.setLearningType(FlatLearningType.ManhattanUpdateRule);
            fnl.doLearningEpoch(trainingSet);
        } catch (Throwable e) {
            // FOR DEBUGGING
            // e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochLearningTypeResilientPropagation() {
        TrainingSet<TrainingElement> trainingSet = TestUtil
        .generateTrainingSet(KHAMSA, KHAMSA);
        try {
            fnl.setLearningType(FlatLearningType.ResilientPropagation);
            fnl.doLearningEpoch(trainingSet);
        } catch (Throwable e) {
            // FOR DEBUGGING
            // e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochLearningTypeBackPropagation() {
        TrainingSet<TrainingElement> trainingSet = TestUtil.generateTrainingSet(KHAMSA, KHAMSA);
        TrainingSet<TrainingElement> lastTrainingSet = TestUtil.generateTrainingSet(KHAMSA, KHAMSA);
        try {
            fnl.setLastLearningType(FlatLearningType.BackPropagation);
            fnl.setLearningType(FlatLearningType.BackPropagation);
            fnl.setLastTrainingSet(lastTrainingSet);
            fnl.setTrainingSet(trainingSet);
            fnl.setMaxError(-100.0d);
            fnl.doLearningEpoch(trainingSet);
        } catch (Throwable e) {
            // FOR DEBUGGING
            // e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test(expected = EncogEngineError.class)
    public void testAddToSquaredErrorSum() {
        double[] outputError = TestUtil.genDoubleVector(KHAMSA);
        fnl.addToSquaredErrorSum(outputError);
    }

    @Test(expected = EncogEngineError.class)
    public void testUpdateNetworkWeights() {
        double[] patternError = TestUtil.genDoubleVector(KHAMSA);
        fnl.updateNetworkWeights(patternError);
    }

    @Test
    public void testFlatNetworkLearningFlatNetwork() {
        fnl = new FlatNetworkLearning(new FlatNetwork());
        assertNotNull(fnl);
    }

    @Test
    public void testFlatNetworkLearningNeuralNetwork() {
        NeuralNetwork network = new NeuralNetwork();
        network.addPlugin(new FlatNetworkPlugin(new FlatNetwork()));
        fnl = new FlatNetworkLearning(network);
        assertNotNull(fnl);
    }

    @Test
    public void testGetMomentum() {
        fnl.setMomentum(WEIGHT);
        assertEquals(WEIGHT, fnl.getMomentum(), EPSILON);
    }

    @Test
    public void testSetMomentum() {
        testGetMomentum();
    }

    @Test
    public void testGetNumThreads() {
        fnl.setNumThreads(KHAMSA);
        assertEquals(KHAMSA, fnl.getNumThreads());
    }

    @Test
    public void testSetNumThreads() {
        testGetNumThreads();
    }

    @Test
    public void testGetLearningType() {
        fnl.setLearningType(FlatLearningType.BackPropagation);
        assertEquals(FlatLearningType.BackPropagation, fnl.getLearningType());
    }

    @Test
    public void testSetLearningType() {
        testGetLearningType();
    }

    @Test
    public void testToString() {
        assertNotNull(fnl.toString());

        /* last learning type */
        fnl.setLastLearningType(null);
        /* learning type = null */
        fnl.setLearningType(null);
        assertNotNull(fnl.toString());

        fnl.setLastLearningType(FlatLearningType.BackPropagation);
        assertNotNull(fnl.toString());
        System.out.println(fnl.toString());
    }

    @Test
    public void testFlatNetworkLearningNullNetworkArg() {
        NeuralNetwork nn = null;
        fnl = new FlatNetworkLearning(nn);
    }
}
