/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.nnet.CompetitiveNetwork;

/**
 * Tests a {@link CompetitiveNetwork} public functionality.
 */
public class TestCompetitiveNetwork {
    /* Test member */
    private CompetitiveNetwork cn = null;

    @Before
    public void setUp() throws Exception {
        cn = new CompetitiveNetwork(KHAMSA, ParamsTest.KHAMSA);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCompetitiveNetwork() {
        assertNotNull(cn);
    }

    @Test
    public void testToString(){
        assertNotNull(cn.toString());
    }
}
