/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.learning.CompetitiveLearning;

/**
 * Tests {@link CompetitiveLearning} functionality.
 */
public class TestCompetitiveLearning {
    /* Test's member */
    private CompetitiveLearning cl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = TestUtil.getDefaultNN();
        /* Constructs the competitive learning */
        cl = new CompetitiveLearning();
        /* Sets initialized ANN */
        cl.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDoLearningEpoch() {
        setWinner();
        cl.doLearningEpoch(TestUtil.generateTrainingSet(KHAMSA, KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testUpdateNetworkWeights() {
        setWinner();
        cl.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testUpdateNetworkWeightsNoWinner() {
        cl.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testCompetitiveLearning() {
        assertNotNull(cl);
    }

    private void setWinner(){
        for (Layer l : nn.getLayers()) {
            TestUtil.connectNeurons(l.getNeurons());
            CompetitiveNeuron cn = new CompetitiveNeuron(new InputFunction(), new Linear());
            for (Connection con : cn.getInputConnections()) {
                cn.getConnectionsFromOtherLayers().add(con);
            }
            ((CompetitiveLayer) nn.getLayerAt(1)).setWinner(cn);
        }
    }
}
