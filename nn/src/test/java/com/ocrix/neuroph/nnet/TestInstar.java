package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.Instar;

public class TestInstar {

    private Instar instar = null;

    @Before
    public void setUp() throws Exception {
        instar = new Instar(DECIMAL);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testInstar() {
        assertNotNull(instar);
    }

    @Test
    public void testToString(){
        String result = instar.toString();
        assertNotNull(result);
    }
}
