/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.THREE;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.nnet.comp.DelayedConnection;
import com.ocrix.neuroph.nnet.comp.DelayedNeuron;

/**
 * Tests the {@link DelayedConnection} public functionality.
 */
public class TestDelayedConnection {
    /* Test's member */
    private DelayedConnection dc = null;

    @Before
    public void setUp() throws Exception {
        dc = new DelayedConnection(new Neuron(), new Neuron(), WEIGHT, GLYPH);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetInput() {
        double result = dc.getInput();
        assertEquals(NIL, result, EPSILON);
    }

    @Test
    public void testDelayedConnection() {
        assertNotNull(dc);
    }

    @Test
    public void testDelayedConnectionInstanceofDelayedNeuron() {
        DelayedNeuron fromNeuron = new DelayedNeuron(new InputFunction(), new Gaussian());
        DelayedNeuron toNeuron = new DelayedNeuron(new InputFunction(), new Step());
        DelayedConnection delayedNeuron = new DelayedConnection(fromNeuron, toNeuron, WEIGHT, GLYPH);
        assertNotNull(delayedNeuron);
        assertTrue(delayedNeuron.getInput() >= NIL);
    }

    @Test
    public void testGetDelay() {
        int result = dc.getDelay();
        assertEquals(GLYPH, result);
    }

    @Test
    public void testSetDelay() {
        dc.setDelay(THREE);
        int result = dc.getDelay();
        assertEquals(THREE, result);
    }

    @Test
    public void testToString() {
        String result = dc.toString();
        assertTrue(result.contains(DelayedConnection.class.getName()));
    }
}
