/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.BinaryDeltaRule;

/**
 * Tests {@link BinaryDeltaRule} functionality.
 */
public class TestBinaryDeltaRule {
    /* Test member */
    private BinaryDeltaRule bdr = null;

    @Before
    public void setUp() throws Exception {
        bdr = new BinaryDeltaRule();
        NeuralNetwork nn = new NeuralNetwork();
        /* Creates and adds layers to the NN */
        TestUtil.addLayersToNN(KHAMSA, nn);
        /* Adds input neurons */
        nn.setInputNeurons(TestUtil.generateThresholdNeuronList(KHAMSA));
        /* Adds output neurons */
        nn.setOutputNeurons(TestUtil.generateThresholdNeuronList(KHAMSA));
        for (Layer layer : nn.getLayers()) {
            for (Neuron n : layer.getNeurons()) {
                /* Connects this input neuron with the rest */
                TestUtil.addInputConnection(n, 1);
                /* Connects this output neuron with the rest */
                TestUtil.addOutputConnection(n, 1);
            }
        }
        /* Sets up the ANN */
        bdr.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNetworkWeights() {
        double[] patternError = TestUtil.genDoubleVector(KHAMSA);
        bdr.updateNetworkWeights(patternError);
        assertTrue(true);
    }

    @Test
    public void testBinaryDeltaRule() {
        assertNotNull(bdr);
    }

    @Test
    public void testGetErrorCorrection() {
        bdr.setErrorCorrection(WEIGHT);
        assertEquals(WEIGHT, bdr.getErrorCorrection(), EPSILON);
    }

    @Test
    public void testSetErrorCorrection() {
        testGetErrorCorrection();
    }

    @Test
    public void testToString() {
        assertNotNull(bdr.toString());
    }

    @Test
    public void testIllegalCastExceptionCase() {
        bdr = new BinaryDeltaRule();
        NeuralNetwork nn = new NeuralNetwork();
        TestUtil.addLayersToNN(KHAMSA, nn);
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        nn.setOutputNeurons(TestUtil.generateNeuronList(KHAMSA));
        for (Layer layer : nn.getLayers()) {
            for (Neuron n : layer.getNeurons()) {
                TestUtil.addInputConnection(n, 1);
                TestUtil.addOutputConnection(n, 1);

            }
        }
        bdr.setNeuralNetwork(nn);

        bdr.updateNetworkWeights(TestUtil.genDoubleVector(KHAMSA));
    }
}
