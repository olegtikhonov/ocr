package com.ocrix.neuroph.nnet.learning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.PerceptronLearning;

/**
 * Tests the {@link PerceptronLearning} functionality.
 */
public class TestPerceptronLearning {

    /* An instance to be tested */
    private PerceptronLearning pl = null;
    /* To be worked with */
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        pl = new PerceptronLearning();
        pl.setNeuralNetwork(nn);
    }

    @Test
    public void testUpdateNeuronWeights() {
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            pl.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testUpdateNeuronWeightsNeuronIsNull() {
        pl.updateNeuronWeights(null);
        assertTrue(true);
    }

    @Test
    public void testPerceptronLearning() {
        assertNotNull(pl);
    }

    @Test
    public void testToString() {
        assertTrue(pl.toString().contains(PerceptronLearning.class.getName()));
    }

    @After
    public void tearDown() throws Exception {
    }
}
