/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;

/**
 * Tests {@link MomentumBackpropagation} functionality.
 */
public class TestMomentumBackpropagation {

    /* Test members */
    private MomentumBackpropagation mb = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        mb = new MomentumBackpropagation();
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        //        TestUtil.setPrerequirements(nn);
        mb.setNeuralNetwork(nn);
    }



    @Test
    public void testOnStart() {
        mb.onStart();
        assertTrue(true);
    }

    @Test
    public void testUpdateNeuronWeights() {
        mb.onStart();
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            mb.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testUpdateNeuronWeightsBatchModeTrue() {

        mb.onStart();
        mb.setBatchMode(true);
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            mb.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertTrue(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testMomentumBackpropagation() {
        assertNotNull(mb);
    }

    @Test
    public void testGetMomentum() {
        mb.setMomentum(WEIGHT);
        assertEquals(WEIGHT, mb.getMomentum(), EPSILON);
    }

    @Test
    public void testSetMomentum() {
        testGetMomentum();
    }

    @Test
    public void testToString(){
        assertTrue(mb.toString().contains(MomentumBackpropagation.class.getName()));
    }

    @After
    public void tearDown() throws Exception {
    }
}
