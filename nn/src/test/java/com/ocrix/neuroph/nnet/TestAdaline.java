/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.neuroph.nnet.Adaline;

/**
 * Tests {@link Adaline} functionality.
 */
public class TestAdaline {

    private static Adaline adaline = null;

    @BeforeClass
    public static void setUp() throws Exception {
        adaline = new Adaline(DECIMAL);
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void testAdaline() {
        assertNotNull(adaline);
    }

    @Test
    public void testToString(){
        assertNotNull(adaline.toString());
    }
}
