package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.UNITY;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.NeuroFuzzyPerceptron;

public class TestNeuroFuzzyPerceptron {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNeuroFuzzyPerceptronDoubleArrayArrayDoubleArrayArray() {
        double[][] pointsSets = TestUtil.gen2DDoubleArray(KHAMSA, KHAMSA);
        double[][] timeSets = TestUtil.gen2DDoubleArray(KHAMSA, KHAMSA);
        NeuroFuzzyPerceptron fuzzyPerceptron = new NeuroFuzzyPerceptron(
                pointsSets, timeSets);
        assertNotNull(fuzzyPerceptron);
    }

    @Test
    public void testNeuroFuzzyPerceptronIntVectorOfIntegerInt() {
        NeuroFuzzyPerceptron fuzzyPerceptron = new NeuroFuzzyPerceptron(UNITY,
                TestUtil.genIntVector(UNITY), UNITY);
        assertNotNull(fuzzyPerceptron);
    }

    /*------------------------------------------------------
     *               NEGATIVE TESTING
     -------------------------------------------------------*/
    @Test(expected = IllegalArgumentException.class)
    public void testNEGATIVENeuroFuzzyPerceptronDoubleArrayArrayDoubleArrayArray() {
        NeuroFuzzyPerceptron fuzzyPerceptron = new NeuroFuzzyPerceptron(null,
                null);
        assertNotNull(fuzzyPerceptron);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNEGATIVENeuroFuzzyPerceptronIntVectorOfIntegerInt() {
        NeuroFuzzyPerceptron fuzzyPerceptron = new NeuroFuzzyPerceptron(UNITY,
                null, UNITY);
        assertNotNull(fuzzyPerceptron);
    }
}
