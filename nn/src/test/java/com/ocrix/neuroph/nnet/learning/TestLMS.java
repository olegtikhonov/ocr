/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.LMS;

/**
 * Tests the {@link LMS} functionality.
 */
public class TestLMS {

    /* Test members */
    private LMS lms = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        lms = new LMS();
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        lms.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNetworkWeights() {
        double[] outputError = TestUtil.genDoubleVector(KHAMSA);
        lms.updateNetworkWeights(outputError);
        assertTrue(true);
    }

    @Test
    public void testLMS() {
        assertNotNull(lms);
    }

    @Test
    public void testUpdateNeuronWeights() {
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = getWeight(neuron);
            lms.updateNeuronWeights(neuron);
            List<String> after = getWeight(neuron);
            assertFalse(before.equals(after));
        }

    }

    @Test
    public void testUpdateNeuronWeightsIsBatchModeTrue() {
        lms.setBatchMode(true);
        for (Neuron neuron : nn.getInputNeurons()) {
            lms.updateNeuronWeights(neuron);
            assertTrue(true);
        }

    }

    private List<String> getWeight(Neuron neuron) {
        List<String> response = new ArrayList<String>(1);

        for (Connection connection : neuron.getInputConnections()) {
            response.add(String.valueOf(connection.getWeight()));
        }

        return response;
    }
}
