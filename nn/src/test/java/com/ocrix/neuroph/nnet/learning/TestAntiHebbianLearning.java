/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.AntiHebbianLearning;

/**
 * Tests an {@link AntiHebbianLearning} public functionality.
 */
public class TestAntiHebbianLearning {
    /* The test member */
    private AntiHebbianLearning ahl = null;

    @Before
    public void setUp() throws Exception {
        ahl = new AntiHebbianLearning();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNeuronWeights() {
        Neuron n =  new Neuron();
        TestUtil.addInputConnection(n, KHAMSA);
        ahl.updateNeuronWeights(n);
    }

    @Test
    public void testToString(){
        assertNotNull(ahl.toString());
    }
}
