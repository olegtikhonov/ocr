/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.BackPropagation;

/**
 * Tests the {@link BackPropagation} public functionality.
 */
public class TestBackPropagation {
    /* Test's member */
    private BackPropagation bp = null;

    @Before
    public void setUp() throws Exception {
        bp = new BackPropagation();
        NeuralNetwork nn = new NeuralNetwork();
        TestUtil.addLayersToNN(KHAMSA, nn);
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        nn.setOutputNeurons(TestUtil.generateNeuronList(KHAMSA));
        for (Layer layer : nn.getLayers()) {
            for (Neuron n : layer.getNeurons()) {
                TestUtil.addInputConnection(n, 1);
                TestUtil.addOutputConnection(n, 1);

            }
        }
        bp.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNetworkWeights() {
        double[] netWeights = TestUtil.genDoubleVector(KHAMSA);
        bp.updateNetworkWeights(netWeights);
        assertTrue(true);
    }

    @Test
    public void testBackPropagation() {
        assertNotNull(bp);
    }

    @Test
    public void testCalculateErrorAndUpdateHiddenNeurons() {
        bp.calculateErrorAndUpdateHiddenNeurons();
        assertTrue(true);
    }

    @Test
    public void testCalculateHiddenNeuronError() {
        double result = -1.0d;
        for (Layer layer : bp.getNeuralNetwork().getLayers()) {
            for (Neuron n : layer.getNeurons()) {
                result = bp.calculateHiddenNeuronError(n);
                assertTrue(result >= NIL);
            }
        }
    }

    @Test
    public void testToString(){
        assertTrue(bp.toString().contains(BackPropagation.class.getName()));
    }
}
