package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.USE_BIAS;
import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.MultiLayerPerceptron;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

public class TestMultiLayerPerceptron {

    private static MultiLayerPerceptron mlp = null;


    @Test
    public void testMultiLayerPerceptronListOfInteger(){
        List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(DECIMAL);
        mlp = new MultiLayerPerceptron(neuronsInLayers);
        assertNotNull(mlp);
    }

    @Test
    public void testMultiLayerPerceptronIntArray() {
        int[] input = TestUtil.genArrayInt(DECIMAL);
        mlp = new MultiLayerPerceptron(input);
        assertEquals(DECIMAL, mlp.getLayersCount());
    }

    @Test
    public void testMultiLayerPerceptronTransferFunctionTypeIntArray() {
        int[] input = TestUtil.genArrayInt(DECIMAL);
        TransferFunctionType tft = TransferFunctionType.TANH;
        mlp = new MultiLayerPerceptron(tft, input);
        assertNotNull(mlp);
    }

    @Test
    public void testMultiLayerPerceptronListOfIntegerTransferFunctionType() {
        List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(DECIMAL);
        /* For output neurons !!! */
        TransferFunctionType tft = TransferFunctionType.TRAPEZOID;
        mlp = new MultiLayerPerceptron(neuronsInLayers, tft);
        assertNotNull(mlp);
    }

    @Test
    public void testMultiLayerPerceptronListOfIntegerNeuronProperties() {
        List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(DECIMAL);

        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(USE_BIAS.to(), true);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.GAUSSIAN);
        mlp = new MultiLayerPerceptron(neuronsInLayers, neuronProperties);
        assertNotNull(mlp);
    }

    @Test
    public void testConnectInputsToOutputs() {
        testMultiLayerPerceptronListOfIntegerNeuronProperties();
        mlp.connectInputsToOutputs();
        assertTrue(true);
    }


    @BeforeClass
    public static void setUp() throws Exception {
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }
}
