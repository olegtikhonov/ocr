/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Log;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Tests {@link InputOutputNeuron} public functionality.
 */
public class TestInputOutputNeuron {
    /* Test member */
    private InputOutputNeuron ion = null;

    @Before
    public void setUp() throws Exception {
        ion = new InputOutputNeuron();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        TestUtil.addInputConnection(ion, KHAMSA, TransferFunctionType.TRAPEZOID);
        ion.calculate();
        assertTrue(true);
    }

    @Test
    public void testCalculateExternalInputSet() {
        TestUtil.addInputConnection(ion, KHAMSA, TransferFunctionType.TRAPEZOID);
        ion.setInput(WEIGHT);
        ion.calculate();
        assertTrue(true);
    }

    @Test
    public void testSetInput() {
        ion.setInput(WEIGHT);
        double result = ion.getNetInput();
        assertEquals(WEIGHT, result, EPSILON);
    }

    @Test
    public void testInputOutputNeuron() {
        assertNotNull(ion);
    }

    @Test
    public void testInputOutputNeuronInputFunctionTransferFunction() {
        ion = new InputOutputNeuron(new InputFunction(), new Log());
        assertNotNull(ion);
    }

    @Test
    public void testGetBias() {
        ion.setBias(WEIGHT);
        double result = ion.getBias();
        assertEquals(WEIGHT, result, EPSILON);
    }

    @Test
    public void testSetBias() {
        testGetBias();
    }

    @Test
    public void testToString() {
        String result = ion.toString();
        assertTrue(result.contains(InputOutputNeuron.class.getName()));
    }
}
