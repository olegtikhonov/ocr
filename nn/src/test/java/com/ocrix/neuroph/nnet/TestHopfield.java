/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.BIAS;
import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.util.TransferFunctionType.STEP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.Hopfield;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * Tests a {@link Hopfield} public functionality.
 */
public class TestHopfield {

    /* Test member */
    private Hopfield hopfield = null;

    @Before
    public void setUp() throws Exception {
        hopfield = new Hopfield(DECIMAL);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testHopfieldInt() {
        hopfield = new Hopfield(DECIMAL);
        assertNotNull(hopfield);
    }

    @Test
    public void testHopfieldIntNeuronProperties() {
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(NEURON_TYPE.to(), InputOutputNeuron.class);
        neuronProperties.setProperty(BIAS.to(), new Double(0));
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        neuronProperties.setProperty(Y_HIGH.to(), new Double(1));
        neuronProperties.setProperty(Y_LOW.to(), new Double(0));
        hopfield = new Hopfield(KHAMSA, neuronProperties);
        assertNotNull(hopfield);
    }

    @Test
    public void testToString(){
        String result = hopfield.toString();
        assertEquals(Hopfield.class.getName(), result);
    }
}
