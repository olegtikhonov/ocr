package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.UnsupervisedHebbianNetwork;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Tests {@link UnsupervisedHebbianNetwork} public functionality.
 */
public class TestUnsupervisedHebbianNetwork {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUnsupervisedHebbianNetworkIntInt() {
        UnsupervisedHebbianNetwork uhn = new UnsupervisedHebbianNetwork(DECIMAL, DECIMAL);
        assertNotNull(uhn);
    }

    @Test
    public void testUnsupervisedHebbianNetworkIntIntTransferFunctionType() {
        UnsupervisedHebbianNetwork uhn = new UnsupervisedHebbianNetwork(DECIMAL, DECIMAL, TransferFunctionType.TANH);
        assertNotNull(uhn);
    }
}
