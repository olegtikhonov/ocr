/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.ResilientPropagation;
import com.ocrix.neuroph.nnet.learning.ResilientPropagation.ResilientWeightTrainingtData;


/**
 * Tests the {@link ResilientPropagation} functionality.
 */
public class TestResilientPropagation {

    /* To be tested */
    private ResilientPropagation rp = null;
    /* To be work with */
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        rp = new ResilientPropagation();
        rp.setNeuralNetwork(nn);
    }


    @Test
    public void testOnStart() {
        rp.onStart();
        assertTrue(true);
    }

    @Test
    public void testDoBatchWeightsUpdate() {
        rp.doBatchWeightsUpdate();
        assertTrue(true);
    }

    @Test
    public void testUpdateNeuronWeights() {
        rp.onStart();
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            rp.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    //resillientWeightUpdate(Weight weight)
    @Test
    public void testResillientWeightUpdateGradientSignChangeGreaterThanZero(){
        //        ResilientWeightTrainingtData w = ResilientWeightTrainingtData.create();
        //        w.setGradient(1.2d);
        //        w.setPreviousDelta(0.34d);
        //        w.setPreviousGradient(0.987d);
        //        w.setPreviousWeightChange(0.004d);
        rp.resillientWeightUpdate(new Weight(EPSILON));
    }

    @Test
    public void testUpdateNeuronWeightsNeuronIsNull() {
        rp.updateNeuronWeights(null);
        assertTrue(true);
    }

    @Test
    public void testUpdateNeuronWeightsInputZero(){
        Neuron neuron = TestUtil.generateNeuronList(1).get(0);
        neuron.setOutput(0.0d);
        neuron.addInputConnection(new Connection(neuron, new Neuron()));
        rp.updateNeuronWeights(neuron);
    }


    @Test
    public void testResilientPropagation() {
        assertNotNull(rp);
    }

    @Test
    public void testResillientWeightUpdate() {
        for (int i = rp.getNeuralNetwork().getLayersCount() - 1; i > 0; i--) {
            Layer layer = rp.getNeuralNetwork().getLayers().get(i);
            /* iterates over neurons at each layer */
            for (Neuron neuron : layer.getNeurons()) {
                /* Iterates connections/weights for each neuron */
                for (Connection connection : neuron.getInputConnections()) {
                    /* For each connection weight applies the following changes */
                    Weight weight = connection.getWeight();
                    rp.resillientWeightUpdate(weight);
                    assertTrue(true);
                }
            }
        }
    }

    @Test
    public void testGetPreviousGradient() {
        ResilientWeightTrainingtData rtd = ResilientWeightTrainingtData.create();
        rtd.setPreviousGradient(WEIGHT);
        assertEquals(WEIGHT, rtd.getPreviousGradient(), EPSILON);
    }

    @Test
    public void testGetPreviousWeightChange(){
        ResilientWeightTrainingtData rtd = ResilientWeightTrainingtData.create();
        rtd.setPreviousWeightChange(WEIGHT);
        assertEquals(WEIGHT, rtd.getPreviousWeightChange(), EPSILON);
    }

    @Test
    public void testGetPreviousDelta(){
        ResilientWeightTrainingtData rtd = ResilientWeightTrainingtData.create();
        rtd.setPreviousDelta(WEIGHT);
        assertEquals(WEIGHT, rtd.getPreviousDelta(), EPSILON);
    }


    @Test
    public void testGetDecreaseFactor() {
        rp.setDecreaseFactor(WEIGHT);
        assertEquals(WEIGHT, rp.getDecreaseFactor(), EPSILON);
    }

    @Test
    public void testSetDecreaseFactor() {
        testGetDecreaseFactor();
    }

    @Test
    public void testGetIncreaseFactor() {
        rp.setIncreaseFactor(WEIGHT);
        assertEquals(WEIGHT, rp.getIncreaseFactor(), EPSILON);
    }

    @Test
    public void testSetIncreaseFactor() {
        testGetIncreaseFactor();
    }

    @Test
    public void testGetInitialDelta() {
        rp.setInitialDelta(WEIGHT);
        assertEquals(WEIGHT, rp.getInitialDelta(), EPSILON);
    }

    @Test
    public void testSetInitialDelta() {
        testGetInitialDelta();
    }

    @Test
    public void testGetMaxDelta() {
        rp.setMaxDelta(WEIGHT);
        assertEquals(WEIGHT, rp.getMaxDelta(), EPSILON);
    }

    @Test
    public void testSetMaxDelta() {
        testGetMaxDelta();
    }

    @Test
    public void testGetMinDelta() {
        rp.setMinDelta(WEIGHT);
        assertEquals(WEIGHT, rp.getMinDelta(), EPSILON);
    }

    @Test
    public void testSetMinDelta() {
        testGetMinDelta();
    }

    @After
    public void tearDown() throws Exception {
    }
}
