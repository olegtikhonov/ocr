/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.BinaryHebbianLearning;


/**
 * Tests {@link BinaryHebbianLearning} functionality.
 */
public class TestBinaryHebbianLearning {

    /* Test's member */
    private BinaryHebbianLearning bhl = null;

    @Before
    public void setUp() throws Exception {
        bhl = new BinaryHebbianLearning();
        NeuralNetwork nn = new NeuralNetwork();
        /* Creates and adds layers to the NN */
        TestUtil.addLayersToNN(KHAMSA, nn);
        /* Adds input neurons */
        nn.setInputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        TestUtil.connectNeurons(nn.getInputNeurons());
        /* Adds output neurons */
        nn.setOutputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        TestUtil.connectNeurons(nn.getOutputNeurons());
        /* Sets up the network */
        bhl.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNeuronWeights() {
        bhl.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testUpdateNeuronWeightsNegative() {
        NeuralNetwork nn = new NeuralNetwork();
        /* Creates and adds layers to the NN */
        TestUtil.addLayersToNN(KHAMSA, nn);
        /* Adds input neurons */
        nn.setInputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        TestUtil.connectNeuronsCustomInput(nn.getInputNeurons(), -0.45d);
        /* Adds output neurons */
        nn.setOutputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        TestUtil.connectNeuronsCustomInput(nn.getOutputNeurons(), -0.56d);
        /* Sets up the network */
        bhl.setNeuralNetwork(nn);

        bhl.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testBinaryHebbianLearning() {
        assertNotNull(bhl);
    }

    @Test
    public void testToString() {
        assertTrue(bhl.toString().contains(BinaryHebbianLearning.class.getName()));
    }
}
