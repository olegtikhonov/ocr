package com.ocrix.neuroph.nnet.comp;
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Tests the {@link CompetitiveLayer} public functionality.
 */
public class TestCompetitiveLayer {

    private CompetitiveLayer compLayer = null;

    @Before
    public void setUp() throws Exception {
        /* Creates input layer neuron settings for this network */
        NeuronProperties neuronProps = new NeuronProperties();
        /* Important here: the transfer function should be chosen carefully */
        neuronProps.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.GAUSSIAN);
        compLayer = new CompetitiveLayer(1, neuronProps);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        List<Neuron> neurons = compLayer.getNeurons();
        for(Neuron neuron : neurons){
            TestUtil.addInputConnection(neuron, 1);
        }
        compLayer.calculate();
        assertTrue(true);
    }

    @Test
    public void testCompetitiveLayer() {
        assertNotNull(compLayer);
    }

    @Test
    public void testGetWinner() {
        List<Neuron> neurons = compLayer.getNeurons();
        for(Neuron neuron : neurons){
            TestUtil.addInputConnection(neuron, 1);
        }
        compLayer.calculate();
        CompetitiveNeuron cn = compLayer.getWinner();
        assertNotNull(cn);
    }

    @Test
    public void testGetMaxIterations() {
        assertEquals((int)MAX, compLayer.getMaxIterations());
    }

    @Test
    public void testSetMaxIterations() {
        compLayer.setMaxIterations(KHAMSA);
        assertEquals(KHAMSA, compLayer.getMaxIterations());
    }

    @Test
    public void testToString(){
        String result = compLayer.toString();
        assertNotNull(result);
    }
}
