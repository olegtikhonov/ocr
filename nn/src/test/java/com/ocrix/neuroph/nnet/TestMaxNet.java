/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.MaxNet;



/**
 * Tests a {@link MaxNet} public functionality.
 */
public class TestMaxNet {

    /* Test's member */
    private MaxNet maxNet = null;

    @Before
    public void setUp() throws Exception {
        maxNet = new MaxNet(KHAMSA);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testMaxNet() {
        assertNotNull(maxNet);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testMaxNetZeroNeuronNumber() {
        /* Should throw the IAE exception*/
        maxNet = new MaxNet(NIL);
    }

    @Test
    public void testToString() {
        String result = maxNet.toString();
        assertNotNull(result);
        System.out.println("result =" + result);
    }
}
