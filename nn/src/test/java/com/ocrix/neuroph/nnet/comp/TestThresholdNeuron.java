/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Tests a {@link ThresholdNeuron} public functionality.
 */
public class TestThresholdNeuron {

    /* Tests member */
    ThresholdNeuron tn = null;

    @Before
    public void setUp() throws Exception {
        tn = new ThresholdNeuron(new InputFunction(), new Linear());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        TestUtil.addInputConnection(tn, KHAMSA, TransferFunctionType.TRAPEZOID);
        tn.calculate();
        assertTrue(true);
    }

    @Test
    public void testThresholdNeuron() {
        assertNotNull(tn);
    }

    @Test
    public void testGetThresh() {
        tn.setThresh(WEIGHT);
        assertEquals(WEIGHT, tn.getThresh(), EPSILON);
    }

    @Test
    public void testSetThresh() {
        testGetThresh();
    }

    @Test
    public void testToString() {
        String result = tn.toString();
        assertTrue(result.contains(ThresholdNeuron.class.getName()));
    }
}
