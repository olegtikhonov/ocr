/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.comp;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.comp.InputNeuron;


/**
 * Tests the {@link InputNeuron} public functionality.
 */
public class TestInputNeuron {

    /* Test's member */
    private InputNeuron in = null;

    @Before
    public void setUp() throws Exception {
        in = new InputNeuron();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        in.calculate();
        assertTrue(true);
    }

    @Test
    public void testInputNeuron() {
        assertNotNull(in);
    }

    @Test
    public void testToString(){
        String result = in.toString();
        assertTrue(result.contains(InputNeuron.class.getName()));
    }
}
