/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.InstarLearning;

/**
 * Tests the {@link InstarLearning} functionality.
 */
public class TestInstarLearning {

    /* Test members */
    private InstarLearning il = null;
    private NeuralNetwork nn = null;


    @Before
    public void setUp() throws Exception {
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        il = new InstarLearning();
        il.setNeuralNetwork(nn);

    }

    @Test
    public void testUpdateNeuronWeights() {
        il.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testInstarLearning() {
        assertNotNull(il);
    }

    @Test
    public void testToString(){
        assertTrue(il.toString().contains(InstarLearning.class.getName()));
    }


    @After
    public void tearDown() throws Exception {
    }
}
