package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.UNITY;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;

/**
 * Tests public functions of the {@link BiasNeuron}.
 */
public class TestBiasNeuron {
    /* Test's member */
    private static BiasNeuron biasNeuron = null;

    @BeforeClass
    public static void setUp() throws Exception {
        /* Creates a bias neuron */
        biasNeuron = new BiasNeuron();
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        assertEquals(UNITY, biasNeuron.getOutput(), EPSILON);
    }

    @Test
    public void testAddInputConnectionConnection() {
        biasNeuron.removeInputConnections();
        Connection conn = new Connection(new Neuron(), new Neuron());
        biasNeuron.addInputConnection(conn);
        assertNotNull(biasNeuron.getInputConnections());
    }

    @Test
    public void testAddInputConnectionNeuron() {
        biasNeuron.removeInputConnections();
        biasNeuron.addInputConnection(new Neuron());
    }

    @Test
    public void testAddInputConnectionNeuronDouble() {
        biasNeuron.removeInputConnections();
        biasNeuron.addInputConnection(new Neuron(), WEIGHT);
        assertEquals(WEIGHT, biasNeuron.getInputConnections().get(0).getWeight().value, EPSILON);
    }

    @Test
    public void testBiasNeuron() {
        /* Created in the SetUp() */
        assertNotNull(biasNeuron);
    }
}
