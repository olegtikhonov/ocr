/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.flat;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.flat.FlatWeight;

/**
 * Tests a {@link FlatWeight} public functionality.
 */
public class TestFlatWeight {

    /* Test's member */
    private FlatWeight fw = null;


    @Before
    public void setUp() throws Exception {
        fw = new FlatWeight(TestUtil.genDouble());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testInc() {
        double before = fw.getValue();
        fw.inc(WEIGHT);
        double after = fw.getValue();
        assertTrue(before != after);
    }

    @Test
    public void testDec() {
        double before = fw.getValue();
        fw.dec(WEIGHT);
        double after = fw.getValue();
        assertTrue(before != after);
    }

    @Test
    public void testSetValue() {
        fw.setValue(WEIGHT);
        assertEquals(WEIGHT, fw.getValue(), EPSILON);
    }

    @Test
    public void testGetValue() {
        testSetValue();
    }

    @Test
    public void testToString() {
        String result = fw.toString();
        assertNotNull(result);
    }

    @Test
    public void testRandomize() {
        double before = fw.getPreviousValue();
        fw.randomize();
        double after = fw.getPreviousValue();
        assertTrue(before != after);
    }

    @Test
    public void testRandomizeDoubleDouble() {
        double before = fw.getPreviousValue();
        fw.randomize(MIN, MAX);
        double after = fw.getPreviousValue();
        assertTrue(before != after);
    }

    @Test
    public void testFlatWeightDoubleArrayIntIndexGreaterThanArrSize() {
        double[] vector = TestUtil.genDoubleVector(KHAMSA);
        fw = new FlatWeight(vector, KHAMSA);
    }

    @Test
    public void testFlatWeightDoubleArrayInt() {
        double[] vector = TestUtil.genDoubleVector(KHAMSA);
        fw = new FlatWeight(vector, KHAMSA - 1);
    }

    @Test
    public void testFlatWeightDouble() {
        assertNotNull(fw);
    }

    @Test
    public void testSetPreviousValue() {
        double expected = TestUtil.genDouble();
        fw.setPreviousValue(expected);
        assertEquals(expected, fw.getPreviousValue(), EPSILON);
    }

    @Test
    public void testGetPreviousValue() {
        testSetPreviousValue();
    }
}
