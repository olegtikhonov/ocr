/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests {@link SimulatedAnnealingLearning} functionality.
 */
public class TestSimulatedAnnealingLearning {

    /* Test members */
    private SimulatedAnnealingLearning sal = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        sal = new SimulatedAnnealingLearning(nn);
    }

    @Test
    public void testDoLearningEpoch() {
        sal.doLearningEpoch(TestUtil.generateSupervisedTrainingSet(KHAMSA,
                KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testDoLearningEpochIsStopped() {
        sal.stopLearning();
        sal.doLearningEpoch(TestUtil.generateSupervisedTrainingSet(KHAMSA,
                KHAMSA));
        assertTrue(true);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAddToSquaredErrorSum() {
        sal.addToSquaredErrorSum(TestUtil.genDoubleVector(KHAMSA));
    }

    @Test
    public void testUpdateNetworkWeights() {
        sal.updateNetworkWeights(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testSimulatedAnnealingLearningNeuralNetworkDoubleDoubleInt() {
        double startTemp = TestUtil.genDouble();
        double stopTemp = TestUtil.genDouble();
        int cycles = TestUtil.genInteger();
        sal = new SimulatedAnnealingLearning(nn, startTemp, stopTemp, cycles);
        assertNotNull(sal);
    }

    @Test
    public void testSimulatedAnnealingLearningNeuralNetwork() {
        assertNotNull(sal);
    }

    @Test
    public void testGetNetwork() {
        assertEquals(nn, sal.getNetwork());
    }

    @Test
    public void testRandomize() {
        double startTemp = TestUtil.genDouble();
        double stopTemp = TestUtil.genDouble();
        int cycles = TestUtil.genInteger();
        sal = new SimulatedAnnealingLearning(nn, startTemp, stopTemp, cycles);
        sal.randomize();
        assertTrue(true);
    }

    @Test
    public void testUpdateTotalNetworkError() {
        double before = sal.getTotalNetworkError();
        sal.updateTotalNetworkError(TestUtil.genDoubleVector(KHAMSA));
        double after = sal.getTotalNetworkError();
        assertTrue(before != after);
    }

    @Test
    public void testToString() {
        assertTrue(sal.toString().contains(
                SimulatedAnnealingLearning.class.getName()));
    }

    @Test
    public void testGetLearningRate() {
        double result = sal.getLearningRate();
        assertTrue(result >= 0.0d);
    }

    @Test
    public void testSetMaxIterations() {
        sal.setMaxIterations(KHAMSA);
        assertTrue(true);
    }

    @Test
    public void testGetCurrentIteration() {
        int result = sal.getCurrentIteration();
        assertTrue(result == 0);
    }

    @Test
    public void testIsPausedLearning() {
        sal.pause();
        assertTrue(sal.isPausedLearning());
    }

    @Test
    public void testDoOneLearningIteration() {
        TrainingSet<SupervisedTrainingElement> trainingSet = TestUtil
                .generateSupervisedTrainingSet(KHAMSA, KHAMSA);
        sal.doOneLearningIteration(trainingSet);
        assertTrue(true);
    }

    @Test
    public void testDoOneLearningIterationSetIsNull() {
        sal.doOneLearningIteration(null);
        assertTrue(true);
    }

    @Test
    public void testDoOneLearningIterationNotSupervisedTrainingSet() {
        TrainingSet<TrainingElement> trainingSet = TestUtil
                .generateTrainingSet(KHAMSA, KHAMSA);
        sal.doOneLearningIteration(trainingSet);
        assertTrue(true);
    }

    @Test
    public void testLearn() {
        TrainingSet<SupervisedTrainingElement> trainingSet = TestUtil
                .generateSupervisedTrainingSet(KHAMSA, KHAMSA);
        sal.learn(trainingSet, KHAMSA);
        assertTrue(true);
    }

    @After
    public void tearDown() throws Exception {
    }
}
