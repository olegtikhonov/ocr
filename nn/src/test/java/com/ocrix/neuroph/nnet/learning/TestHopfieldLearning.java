/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.HopfieldLearning;

/**
 * Tests the {@link HopfieldLearning} functionality.
 */
public class TestHopfieldLearning {

    /* Test members */
    private HopfieldLearning hl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        hl = new HopfieldLearning();
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        hl.setNeuralNetwork(nn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testLearn() {
        hl.learn(TestUtil.generateTrainingSet(KHAMSA, KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testLearnNoConnections() {
        hl = new HopfieldLearning();
        nn = TestUtil.getDefaultNN();
        hl.setNeuralNetwork(nn);
        /* Turns the logger off */
        hl.logOff();
        hl.learn(TestUtil.generateTrainingSet(KHAMSA, KHAMSA));
        assertTrue(true);
        hl.logOn();
    }

    @Test
    public void testHopfieldLearning() {
        assertNotNull(hl);
    }

    @Test
    public void testToString() {
        assertTrue(hl.toString().contains(HopfieldLearning.class.getName()));
    }
}
