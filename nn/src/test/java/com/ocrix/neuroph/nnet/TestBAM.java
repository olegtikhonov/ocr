package com.ocrix.neuroph.nnet;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.nnet.BAM;


/**
 * Tests the {@link BAM} public functionality.
 */
public class TestBAM {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBAM() {
        BAM bam = new BAM(ParamsTest.DECIMAL, ParamsTest.DECIMAL);
        assertNotNull(bam);
    }

//    @Test (expected=IllegalArgumentException.class)
    public void testNEGATIVEBAM() {
        BAM bam = new BAM(~ParamsTest.DECIMAL, ~ParamsTest.DECIMAL);
        assertNotNull(bam);
    }

    @Test
    public void tesToString(){
        BAM bam = new BAM(ParamsTest.DECIMAL, ParamsTest.DECIMAL);
        String str = bam.toString();
        assertNotNull(str);
    }
}
