/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.VP_SIGMA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.nnet.comp.DelayedConnection;
import com.ocrix.neuroph.nnet.comp.DelayedNeuron;


/**
 * Tests the {@link DelayedConnection} public functionality.
 */
public class TestDelayedNeuron {

    private DelayedNeuron dn = null;

    @Before
    public void setUp() throws Exception {
        dn = new DelayedNeuron(new InputFunction(), new Sigmoid());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCalculate() {
        dn.calculate();
        assertTrue(true);
    }

    @Test
    public void testDelayedNeuron() {
        assertNotNull(dn);
    }

    @Test
    public void testGetOutputInt() {
        dn.calculate();
        double result = dn.getOutput(NIL);
        assertEquals(VP_SIGMA, result, EPSILON);
    }

    @Test
    public void testGetOutputoutputHistorySizeIfCond() {
        for(int i = 0; i < DECIMAL; i++){
            dn.calculate();
        }

        double result = dn.getOutput(NIL);
        assertEquals(VP_SIGMA, result, EPSILON);
    }

    @Test
    public void testToString(){
        String result = dn.toString();
        assertTrue(result.contains(DelayedNeuron.class.getName()));
    }
}
