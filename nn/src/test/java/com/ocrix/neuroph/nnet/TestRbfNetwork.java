package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ocrix.neuroph.nnet.RbfNetwork;

/**
 * Tests a {@link RbfNetwork} public functionality.
 */
public class TestRbfNetwork {

    @BeforeClass
    public static void setUp() throws Exception {
    }

    @AfterClass
    public static void tearDown() throws Exception {
    }

    @Test
    public void testRbfNetwork() {
        RbfNetwork rbfNetwork = new RbfNetwork(DECIMAL, DECIMAL, DECIMAL);
        assertNotNull(rbfNetwork);
    }

}
