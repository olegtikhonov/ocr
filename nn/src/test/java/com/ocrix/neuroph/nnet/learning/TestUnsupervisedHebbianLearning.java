package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.UnsupervisedHebbianLearning;

/**
 * Tests {@link UnsupervisedHebbianLearning} functionality.
 */
public class TestUnsupervisedHebbianLearning {
    /* Test members */
    private UnsupervisedHebbianLearning uhl = null;
    private NeuralNetwork nn = null;


    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        uhl = new UnsupervisedHebbianLearning();
        uhl.setNeuralNetwork(nn);
    }

    @Test
    public void testDoLearningEpoch() {
        TrainingSet<SupervisedTrainingElement> trainSet = TestUtil.generateSupervisedTrainingSet(KHAMSA, KHAMSA);
        uhl.doLearningEpoch(trainSet);
        assertTrue(true);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testDoLearningEpochNull() {
        uhl.doLearningEpoch(null);
        assertTrue(true);
    }

    @Test
    public void testUpdateNetworkWeights() {
        uhl.updateNetworkWeights();
        assertTrue(true);
    }

    @Test
    public void testUnsupervisedHebbianLearning() {
        assertNotNull(uhl);
    }

    @Test
    public void testUpdateNeuronWeights() {
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            uhl.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testUpdateNeuronWeightsNull() {
        uhl.logOn();
        uhl.updateNeuronWeights(null);
        assertTrue(true);
        uhl.logOff();
    }

    @Test
    public void testUpdateNetworkWeightsNullANN(){
        uhl.logOn();
        uhl.setNeuralNetwork(null);
        uhl.updateNetworkWeights();
        uhl.logOff();
        assertTrue(true);
    }

    @After
    public void tearDown() throws Exception {
    }
}
