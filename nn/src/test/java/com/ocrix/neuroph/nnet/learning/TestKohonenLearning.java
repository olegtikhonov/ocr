/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.UNITY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.KohonenLearning;

/**
 * Tests the functionality of {@link KohonenLearning}.
 */
public class TestKohonenLearning {

    /* Test member */
    private KohonenLearning kl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        kl = new KohonenLearning();
        kl.setNeuralNetwork(nn);
    }

    @Test
    public void testSetNeuralNetwork() {
        NeuralNetwork testANN = new NeuralNetwork();
        kl.setNeuralNetwork(testANN);
        assertEquals(testANN, kl.getNeuralNetwork());
    }

    @Test
    public void testLearn() {
        double beforeLearning = kl.getLearningRate();
        kl.learn(TestUtil.generateTrainingSet(KHAMSA, KHAMSA));
        double afterLearning = kl.getLearningRate();
        assertTrue(beforeLearning > afterLearning);
    }

    @Test
    public void testKohonenLearning() {
        assertNotNull(kl);
    }

    @Test
    public void testGetLearningRate() {
        double learningRate = TestUtil.genDouble();
        kl.setLearningRate(learningRate);
        assertEquals(learningRate, kl.getLearningRate(), EPSILON);
    }

    @Test
    public void testSetLearningRate() {
        testGetLearningRate();
    }

    @Test
    public void testSetIterations() {
        kl.setIterations(KHAMSA, DECIMAL);
        assertEquals(KHAMSA, kl.getIterations()[NIL]);
        assertEquals(DECIMAL, kl.getIterations()[UNITY]);
    }

    @Test
    public void testGetCurrentIteration() {
        testLearn();
        assertTrue(kl.getCurrentIteration() > NIL);
    }

    @Test
    public void testGetMapSize() {
        assertEquals(GLYPH, kl.getMapSize());
    }

    @Test
    public void testToString() {
        assertTrue(kl.toString().contains(KohonenLearning.class.getName()));
    }

    @After
    public void tearDown() throws Exception {
    }
}
