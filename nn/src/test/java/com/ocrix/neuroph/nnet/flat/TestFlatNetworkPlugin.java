/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.flat;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;

import org.encog.engine.network.activation.ActivationBiPolar;
import org.encog.engine.network.activation.ActivationLOG;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSoftMax;
import org.encog.engine.network.flat.FlatLayer;
import org.encog.engine.network.flat.FlatNetwork;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.MultiLayerPerceptron;
import com.ocrix.neuroph.nnet.flat.FlatNetworkPlugin;
import com.ocrix.neuroph.nnet.flat.FlatWeight;

/**
 * Tests {@link FlatNetworkPlugin} public functionality.
 */
public class TestFlatNetworkPlugin {
    /* Test member */
    private FlatNetworkPlugin fnp = null;
    private FlatNetwork fn = null;
    private static final Logger log = Logger.getLogger(TestFlatNetworkPlugin.class.getName());

    @Before
    public void setUp() throws Exception {
        FlatLayer flBiPolar = new FlatLayer(new ActivationBiPolar(), KHAMSA, 1,
                null);
        FlatLayer flLog = new FlatLayer(new ActivationLOG(), KHAMSA, 1, null);
        FlatLayer[] layers = { flBiPolar, flLog };
        fn = new FlatNetwork(layers);
        fnp = new FlatNetworkPlugin(fn);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFlatNetworkPlugin() {
        assertNotNull(fnp);
    }

    @Test
    public void testGetFlatNetwork() {
        assertNotNull(fnp.getFlatNetwork());
    }

    @Test
    public void testFlattenNeuralNetworkNetworkMultiLayerPerceptron() {
        FlatLayer flBiPolar = new FlatLayer(new ActivationSigmoid(), KHAMSA, 1,
                null);
        FlatLayer flLog = new FlatLayer(new ActivationSoftMax(), KHAMSA, 1,
                null);
        FlatLayer[] layers = { flBiPolar, flLog };
        FlatNetwork fn = new FlatNetwork(layers);
        fnp = new FlatNetworkPlugin(fn);
        List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(KHAMSA);
        try {
            FlatNetworkPlugin.flattenNeuralNetworkNetwork(new MultiLayerPerceptron(neuronsInLayers));
        } catch (Exception e) {
            log.severe(e.getMessage());
        }
        assertNotNull(fnp);
    }

    @Test
    public void testFlattenNeuralNetworkNetworkOther() {
        FlatLayer flBiPolar = new FlatLayer(new ActivationSigmoid(), KHAMSA, 1,
                null);
        FlatLayer flLog = new FlatLayer(new ActivationSoftMax(), KHAMSA, 1,
                null);
        FlatLayer[] layers = { flBiPolar, flLog };
        FlatNetwork fn = new FlatNetwork(layers);
        fnp = new FlatNetworkPlugin(fn);
        try {
            FlatNetworkPlugin.flattenNeuralNetworkNetwork(new NeuralNetwork());
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(fnp);
    }

    @Test
    public void testInitCL() {
        try {
            FlatNetworkPlugin.initCL();
        } catch (Exception e) {
            
//            log.severe(e.getMessage());
        }

    }

    @Test
    public void testShutdown() {
        FlatNetworkPlugin.shutdown();
        assertTrue(true);
    }

    @Test
    public void testUnFlattenNeuralNetworkNetwork() {
        NeuralNetwork nn = new NeuralNetwork();
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        TestUtil.addLayersToNN(KHAMSA, nn);
        for (Layer layer : nn.getLayers()) {
            TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
            for (Neuron neuron : layer.getNeurons()) {
                TestUtil.addInputConnection(neuron, 2, true);
            }
        }
        FlatNetworkPlugin.unFlattenNeuralNetworkNetwork(nn);
    }

    @Test
    public void testUnFlattenNeuralNetworkNetworkOther() {
        NeuralNetwork nn = new NeuralNetwork();
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        TestUtil.addLayersToNN(KHAMSA, nn);
        for (Layer layer : nn.getLayers()) {
            TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
            for (Neuron neuron : layer.getNeurons()) {
                TestUtil.addInputConnection(neuron, 2, false);
            }
        }
        FlatNetworkPlugin.unFlattenNeuralNetworkNetwork(nn);
    }

    @Test
    public void testUnFlattenNeuralNetworkNetworkTrainedDataNull() {
        NeuralNetwork nn = new NeuralNetwork();
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        TestUtil.addLayersToNN(KHAMSA, nn);
        for (Layer layer : nn.getLayers()) {
            TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
            for (Neuron neuron : layer.getNeurons()) {
                FlatWeight fw = new FlatWeight(TestUtil.genDouble());
                fw.setTrainingData(null);
                neuron.addInputConnection(new Connection(new Neuron(),
                        new Neuron(), fw));
            }
        }
        FlatNetworkPlugin.unFlattenNeuralNetworkNetwork(nn);
    }

    @Test
    public void testFlattenLayerNeuronClass() {
        try {
            Method method = fnp.getClass().getDeclaredMethod("flattenLayer", Layer.class);
            method.setAccessible(true);
            NeuralNetwork nn = new NeuralNetwork();
            nn.setInputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
            TestUtil.addLayersToNN(1, nn);
            method.invoke(fnp, nn);
            assertTrue(true);
        } catch (SecurityException e) {
            log.severe(e.getMessage());
        } catch (NoSuchMethodException e) {
            log.severe(e.getMessage());
        } catch (IllegalArgumentException e) {
            log.severe(e.getMessage());
        } catch (IllegalAccessException e) {
            log.severe(e.getMessage());
        } catch (InvocationTargetException e) {
            log.severe(e.getMessage());
        }
    }

    @Test
    public void testFlattenLayerCompetativeNeuronClass() {
        try {
            Method method = fnp.getClass().getDeclaredMethod("flattenLayer", Layer.class);
            method.setAccessible(true);
            NeuralNetwork nn = new NeuralNetwork();
            nn.setInputNeurons(TestUtil.generateCompetativeNeuronList(KHAMSA));
            TestUtil.addLayersToNN(1, nn);
            method.invoke(fnp, nn);
            assertTrue(true);
        } catch (SecurityException e) {
            log.severe(e.getMessage());
        } catch (NoSuchMethodException e) {
            log.severe(e.getMessage());
        } catch (IllegalArgumentException e) {
            log.severe(e.getMessage());
        } catch (IllegalAccessException e) {
            log.severe(e.getMessage());
        } catch (InvocationTargetException e) {
            log.severe(e.getMessage());
        }
    }

    @Test
    public void testFlattenMultiLayerPerceptronFalse() {
        try {
            Method method = fnp.getClass().getDeclaredMethod("flattenMultiLayerPerceptron", MultiLayerPerceptron.class);
            method.setAccessible(true);
            List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(KHAMSA);
            MultiLayerPerceptron nn = new MultiLayerPerceptron(neuronsInLayers);
            nn.getLayers().add(null);
            method.invoke(fnp, nn);
            assertTrue(true);
        } catch (SecurityException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void testFlattenMultiLayerPerceptronTrue() {
        try {
            Method method = fnp.getClass().getDeclaredMethod("flattenMultiLayerPerceptron", MultiLayerPerceptron.class);
            method.setAccessible(true);
            List<Integer> neuronsInLayers = TestUtil.genListOfIntegers(KHAMSA);
            MultiLayerPerceptron nn = new MultiLayerPerceptron(neuronsInLayers);
            method.invoke(fnp, nn);
            assertTrue(true);
        } catch (SecurityException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            log.severe(e.getMessage());
            e.printStackTrace();
        }
    }


    @Test
    public void testFlattenLayerBiasNeuronClass() {
        try {
            Method method = fnp.getClass().getDeclaredMethod("flattenLayer", Layer.class);
            method.setAccessible(true);
            NeuralNetwork nn = new NeuralNetwork();
            nn.setInputNeurons(TestUtil.generateBiasNeuronList(KHAMSA));
            TestUtil.addLayersToNN(1, nn);
            method.invoke(fnp, nn);
            assertTrue(true);
        } catch (SecurityException e) {
            log.severe(e.getMessage());
        } catch (NoSuchMethodException e) {
            log.severe(e.getMessage());
        } catch (IllegalArgumentException e) {
            log.severe(e.getMessage());
        } catch (IllegalAccessException e) {
            log.severe(e.getMessage());
        } catch (InvocationTargetException e) {
            log.severe(e.getMessage());
        }
    }



    @Test
    public void testUnFlattenNeuralNetworkNetworkFlatWeight() {
        NeuralNetwork nn = new NeuralNetwork();

        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));

        TestUtil.addLayersToNN(KHAMSA, nn);

        for (Layer layer : nn.getLayers()) {
            TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
            TestUtil.connectAllNeuronsEachOtherOnSameLayer(layer);
        }
        /* Learn a network */
        nn.learn(TestUtil.generateTrainingSet(KHAMSA, KHAMSA));
        FlatNetworkPlugin.unFlattenNeuralNetworkNetwork(nn);
    }

    @Test
    public void testToString() {
        assertNotNull(fnp.toString());
    }

    // @Test
    // public void testToStringFlatNull(){
    // FlatNetwork fn1 = null;
    // fnp = new FlatNetworkPlugin(fn1);
    // assertNotNull(fnp.toString());
    // }

    // @Test
    // public void testFlattenNeuralNetworkNetwork(){
    //
    // }

}
