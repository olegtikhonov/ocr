/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.OutstarLearning;

/**
 * Tests {@link OutstarLearning} functionality.
 */
public class TestOutstarLearning {

    /* The instance of TestOutstarLearning to be tested */
    private OutstarLearning ol = null;
    /* A neural network to be worked with */
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        ol = new OutstarLearning();
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        ol.setNeuralNetwork(nn);
    }

    @Test
    public void testUpdateNeuronWeights() {
        for (Neuron neuron : nn.getInputNeurons()) {
            List<String> before = TestUtil.getWeight(neuron);
            ol.updateNeuronWeights(neuron);
            List<String> after = TestUtil.getWeight(neuron);
            assertFalse(TestUtil.isEqual(before.toArray(), after.toArray()));
        }
    }

    @Test
    public void testUpdateNeuronWeightsNeuronIsNull() {
        ol.updateNeuronWeights(null);
        assertTrue(true);
    }

    @Test
    public void testOutstarLearning() {
        assertNotNull(ol);
    }

    @Test
    public void testToString() {
        assertTrue(ol.toString().contains(OutstarLearning.class.getName()));
    }

    @After
    public void teardown() throws Exception {
    }
}
