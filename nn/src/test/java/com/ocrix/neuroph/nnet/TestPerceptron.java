package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.util.TransferFunctionType.LOG;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.Perceptron;

/**
 * Tests {@link Perceptron} public functionality.
 */
public class TestPerceptron {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testPerceptronIntInt() {
        Perceptron perceptron = new Perceptron(KHAMSA, KHAMSA);
        assertNotNull(perceptron);
    }

    @Test
    public void testPerceptronIntIntTransferFunctionType() {
        Perceptron perceptron = new Perceptron(KHAMSA, KHAMSA, LOG);
        assertNotNull(perceptron);
    }
}
