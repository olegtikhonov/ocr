/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.GeneralizedHebbianLearning;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;

/**
 * Tests {@link GeneralizedHebbianLearning} functionality.
 */
public class TestGeneralizedHebbianLearning {

    /* Test member */
    private GeneralizedHebbianLearning ghl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        ghl = new GeneralizedHebbianLearning();
        nn = TestUtil.getDefaultNN();
        ghl.setNeuralNetwork(nn);
        setPrerequirements();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNeuronWeights() {
        List<Neuron> input = ghl.getNeuralNetwork().getInputNeurons();
        for (Neuron neuron : input) {
            ghl.updateNeuronWeights(neuron);
            assertTrue(true);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateNeuronWeightsNullNeuron() {
        ghl.updateNeuronWeights(null);
    }

    @Test
    public void testToString() {
        assertNotNull(ghl.toString());
    }

    private void setPrerequirements() {
        for (Layer l : nn.getLayers()) {
            TestUtil.connectNeuronsCustomInput(l.getNeurons(), WEIGHT);
            Neuron cn = new Neuron(new InputFunction(), new Linear());
            for (Connection con : cn.getInputConnections()) {
                con.setFromNeuron(cn);
                //                con.getWeight().setTrainingData(new MomentumBackpropagation().new MomentumWeightTrainingData());
                con.getWeight().setTrainingData(MomentumBackpropagation.MomentumWeightTrainingData.getInstance());
            }
        }
    }
}
