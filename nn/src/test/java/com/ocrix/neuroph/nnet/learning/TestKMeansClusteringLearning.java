package com.ocrix.neuroph.nnet.learning;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nnet.learning.KMeansClusteringLearning;

public class TestKMeansClusteringLearning {

    //TODO: re-test when KMeansClusteringLearning will be implemented !!!
    private KMeansClusteringLearning kMeansCL = null;

    @Before
    public void setUp() throws Exception {
        kMeansCL = new KMeansClusteringLearning();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUpdateNetworkWeights() {
        kMeansCL.updateNetworkWeights();
        assertTrue(true);
    }
}
