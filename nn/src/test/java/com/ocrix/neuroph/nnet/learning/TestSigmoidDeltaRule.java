/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.SigmoidDeltaRule;

/**
 * Tests {@link SigmoidDeltaRule} functionality.
 */
public class TestSigmoidDeltaRule {

    /* Test members */
    private SigmoidDeltaRule sdr = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        sdr = new SigmoidDeltaRule();
        sdr.setNeuralNetwork(nn);
    }


    @Test
    public void testUpdateNetworkWeights() {
        sdr.updateNetworkWeights(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testSigmoidDeltaRule() {
        assertNotNull(sdr);
    }

    @Test
    public void testCalculateErrorAndUpdateOutputNeurons() {
        sdr.calculateErrorAndUpdateOutputNeurons(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(true);
    }

    @Test
    public void testCalculateErrorAndUpdateOutputNeuronsWithZeroOutput(){
        double[] outputError = {0, 0, 0, 0, 0};
        sdr.calculateErrorAndUpdateOutputNeurons(outputError);
    }


    @Test
    public void testToString(){
        assertTrue(sdr.toString().contains(SigmoidDeltaRule.class.getName()));
    }

    @After
    public void tearDown() throws Exception {
    }

}
