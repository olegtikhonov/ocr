/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nn;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Vector;


import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.core.transfer.Log;
import com.ocrix.neuroph.core.transfer.Ramp;
import com.ocrix.neuroph.core.transfer.Sgn;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.core.transfer.Sin;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.core.transfer.Tanh;
import com.ocrix.neuroph.core.transfer.TransferFunction;
import com.ocrix.neuroph.core.transfer.Trapezoid;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;
import com.ocrix.neuroph.nnet.flat.FlatWeight;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Test utility class, uses inside the tests. All methods are public, i.e. <b>do
 * not</b> use constructor to create the instance of this class.
 */
public final class TestUtil {
    private TestUtil() {
    }

    /**
     * Generates a random double.
     * 
     * @return randomly generated double.
     */
    public static double genDouble() {
        Random generator = new Random();
        return generator.nextDouble();
    }

    /**
     * Generates a double vector/array.
     * 
     * @param size
     *            - the size of generated array.
     * 
     * @return - an array of the doubles.
     */
    public static double[] genDoubleVector(int size) {
        double gend = -1;
        if (size <= 0)
            throw new IllegalArgumentException();
        /* An array to be returned */
        double[] array = new double[size];

        for (int i = 0; i < size; i++) {
            gend = genDouble();

            while (gend < 0.5d) {
                gend = genDouble();
            }

            array[i] = gend;
        }
        return array;
    }

    /**
     * Generates a double list.
     * 
     * @param size
     *            of the {@link List}.
     * 
     * @return generated {@link List} of doubles.
     */
    public static List<Double> genDoubleList(int size) {
        if (size <= 0)
            throw new IllegalArgumentException();
        /* An array to be returned */
        List<Double> list = new ArrayList<Double>();

        for (int i = 0; i < size; i++) {
            list.add(genDouble());
        }
        return list;
    }

    /**
     * Checks if two double arrays are equals going through each element.
     * 
     * @param ideal
     *            to be compared with.
     * @param idealArray
     *            to be compared with.
     */
    public static void assertArrayEquals(double[] ideal, double[] idealArray) {
        Validator.validateDouble(ideal);
        Validator.validateDouble(idealArray);
        for (int i = 0; i < idealArray.length; i++) {
            assertEquals(ideal[i], idealArray[i], ParamsTest.EPSILON);
        }
    }

    /**
     * Add one element to the {@link TrainingSet}.
     * 
     * @param set
     *            where to set the training element.
     */
    public static void addOneElement(TrainingSet<TrainingElement> set) {
        ArrayList<Double> input = (ArrayList<Double>) TestUtil.genDoubleList(DECIMAL);
        ArrayList<Double> ideal = (ArrayList<Double>) TestUtil.genDoubleList(DECIMAL);
        TrainingElement elem = new TrainingElement(input);
        elem.setIdealArray(convertArrayListToDoubleArray(ideal));
        set.addElement(elem);
    }

    /**
     * Converts array list to double array.
     * 
     * @param listToBoConverted
     * @return
     */
    public static double[] convertArrayListToDoubleArray(
            ArrayList<Double> listToBoConverted) {
        if (listToBoConverted == null)
            return null;
        double[] transformed = new double[listToBoConverted.size()];
        for (int i = 0; i < listToBoConverted.size(); i++)
            transformed[i] = listToBoConverted.get(i);
        return transformed;
    }

    /**
     * Generates a list of Integers by given size.
     * 
     * @param size
     *            - of list of Integers.
     * 
     * @return
     */
    public static List<Integer> genListOfIntegers(int size) {
        if (size <= 0)
            throw new IllegalArgumentException();
        /* An array to be returned */
        List<Integer> list = new ArrayList<Integer>(size);

        for (int i = 0; i < size; i++) {
            list.add(genInteger());
        }
        return list;
    }

    /**
     * Generates array of integers.
     * 
     * @param size
     *            of the integer array.
     * 
     * @return
     */
    public static int[] genArrayInt(int size) {
        if (size <= 0)
            throw new IllegalArgumentException();

        int[] toBeGenerated = new int[size];
        for (int i = 0; i < size; i++) {
            toBeGenerated[i] = genInteger();
        }
        return toBeGenerated;
    }

    /**
     * Generates a single Integer.
     * 
     * @return
     */
    public static Integer genInteger() {
        Random generator = new Random();
        return Math.abs(generator.nextInt(100) + 1);
    }

    /**
     * Generates a 2d double array.
     * 
     * @param sizeX
     * @param sizeY
     * 
     * @return
     */
    public static double[][] gen2DDoubleArray(int sizeX, int sizeY) {
        double[][] result = new double[sizeX][sizeY];

        for (int i = 0; i < sizeX; i++) {
            result[i] = genDoubleVector(sizeY);
        }
        return result;
    }

    /**
     * Generates 2D integer array.
     * 
     * @param sizeX
     *            - x dim.
     * @param sizeY
     *            - y dim.
     * 
     * @return
     */
    public static int[][] gen2DIntegerArray(int sizeX, int sizeY) {
        int[][] result = new int[sizeX][sizeY];

        for (int i = 0; i < sizeX; i++) {
            result[i] = genArrayInt(sizeY);
        }
        return result;
    }

    /**
     * Generates a Vector of integers.
     * 
     * @param vectorSize
     *            - a size of the Vector.
     * 
     * @return randomly generated Vector of integers.
     */
    public static Vector<Integer> genIntVector(int vectorSize) {
        Vector<Integer> toBeRet = new Vector<Integer>(vectorSize);
        int[] temp = genArrayInt(vectorSize);
        for (int i = 0; i < temp.length; i++) {
            toBeRet.add(temp[i]);
        }
        return toBeRet;
    }

    /**
     * Adds neurons the the given layer.
     * 
     * @param numNeurons
     *            - a number of neurons to be added.
     * @param layer
     *            - a parent layer where the neurons will be added.
     * 
     * @return
     */
    public static Layer addDefaultNeuronsToLayer(int numNeurons, Layer layer) {
        for (int i = 0; i < numNeurons; i++) {
            layer.addNeuron(getRandomNeuronWithError());
        }
        return layer;
    }

    /**
     * Adds layers to the {@link NeuralNetwork}.
     * 
     * @param numberOfLayers
     *            - the number of layers to be added. The only one neuron will
     *            be added to the layer.
     * @param toBeAdded
     *            - a neural network where the layers to be added to.
     * 
     * @return a neural network with added layers.
     */
    public static NeuralNetwork addLayersToNN(int numberOfLayers,
            NeuralNetwork toBeAdded) {
        for (int i = 0; i < numberOfLayers; i++) {
            toBeAdded.addLayer(addDefaultNeuronsToLayer(numberOfLayers
                    / numberOfLayers, new Layer()));
        }
        return toBeAdded;
    }

    /**
     * Generates a list of neurons putting random input and output.
     * 
     * @param capacity
     *            - the number of generated neurons.
     * 
     * @return a list of neurons.
     */
    public static List<Neuron> generateNeuronList(int capacity) {
        List<Neuron> neurons = new ArrayList<Neuron>(capacity);

        for (int i = 0; i < capacity; i++) {
            Neuron n = new Neuron();
            n.setInput(genDouble());
            n.setOutput(genDouble());
            n.setError(genDouble());
            neurons.add(n);
        }
        return neurons;
    }

    /**
     * Creates a list of rendomly generated {@link ThresholdNeuron}s.
     * 
     * @param capacity
     *            - the amount of neurons to be generated.
     * 
     * @return {@code List<Neuron>} generated list of Threshold neurons.
     */
    public static List<Neuron> generateThresholdNeuronList(int capacity) {
        List<Neuron> neurons = new ArrayList<Neuron>(capacity);

        for (int i = 0; i < capacity; i++) {
            ThresholdNeuron n = new ThresholdNeuron(new InputFunction(),
                    randomTransferFunction());
            n.setInput(genDouble());
            n.setOutput(genDouble());
            n.setThresh(genDouble());
            n.setError(genDouble());
            neurons.add(n);
        }
        return neurons;
    }

    /**
     * Generates a list of {@link BiasNeuron}s.
     * 
     * @param capacity
     *            the amount of {@link BiasNeuron}s in the list.
     * 
     * @return generated {@code List<Neuron>}
     */
    public static List<Neuron> generateBiasNeuronList(int capacity) {
        List<Neuron> neurons = new ArrayList<Neuron>(capacity);

        for (int i = 0; i < capacity; i++) {
            Neuron n = new BiasNeuron();
            n.setTransferFunction(randomTransferFunction());
            n.setInput(genDouble());
            n.setOutput(genDouble());
            n.setError(genDouble());
            neurons.add(n);
        }
        return neurons;
    }

    /**
     * Generates a list of {@link CompetitiveNeuron}s.
     * 
     * @param capacity
     *            the amount of {@link CompetitiveNeuron}s in the list.
     * 
     * @return the list of generated {@link CompetitiveNeuron}s.
     */
    public static List<Neuron> generateCompetativeNeuronList(int capacity) {
        List<Neuron> neurons = new ArrayList<Neuron>(capacity);

        for (int i = 0; i < capacity; i++) {
            Neuron n = new CompetitiveNeuron(new InputFunction(),
                    randomTransferFunction());
            n.setInput(genDouble());
            n.setOutput(genDouble());
            n.setError(genDouble());
            neurons.add(n);
        }
        return neurons;
    }

    /**
     * Generates a list of {@link Neuron}s with randomly chosen
     * {@link TransferFunction}.
     * 
     * @param capacity
     *            the amount of generated neurons to be in the list.
     * 
     * @return a list of neurons.
     */
    public static List<Neuron> generateNeuronListTF(int capacity) {
        List<Neuron> neurons = new ArrayList<Neuron>(capacity);

        for (int i = 0; i < capacity; i++) {
            Neuron n = new Neuron();
            n.setError(genDouble());
            n.setInput(genDouble());
            n.setOutput(genDouble());
            n.setTransferFunction(randomTransferFunction());
            /* Connects neuron to itself */
            n.addOutputConnection(new Connection(n, n));
            neurons.add(n);
        }
        return neurons;
    }

    /**
     * Chooses a {@link TransferFunction}. The option are:
     * <ul>
     * <li><code>Linear</code></li>
     * <li><code>Tanh</code></li>
     * <li><code>Gaussian</code></li>
     * <li><code>Sigmoid</code></li>
     * <li><code>Sgn</code></li>
     * <li><code>Step</code></li>
     * </ul>
     * 
     * @return
     */
    public static TransferFunction randomTransferFunction() {
        Random rand = new Random();
        int type = rand.nextInt(KHAMSA) + 1;
        TransferFunction tf = null;
        switch (type) {
        case 1:
            tf = new Linear();
            break;
        case 2:
            tf = new Tanh();
            break;
        case 3:
            tf = new Gaussian();
            break;
        case 4:
            tf = new Sigmoid();
            break;
        case 5:
            tf = new Sgn();
            break;

        default:
            tf = new Step();
            break;
        }
        return tf;
    }

    /**
     * Initializes training set, adding training element.
     * 
     * @param set
     *            - to be initialized.
     * 
     * @return the initialized set.
     */
    public static TrainingSet<TrainingElement> initSet(
            TrainingSet<TrainingElement> set) {
        for (int i = 0; i < KHAMSA; i++) {
            TrainingElement te = new TrainingElement(
                    TestUtil.genDoubleVector(KHAMSA));
            set.addElement(te);
        }
        return set;
    }

    /**
     * Connects each input neuron to its output neuron, i.e. IN_1 --> ON_1 etc.
     * IN - stands for the input neuron, ON - stands for the output neuron.
     * 
     * @param nn
     *            the neural network where input neurons will be connected to
     *            its output neurons.
     * 
     * @return the nn with connected neurons.
     */
    public static NeuralNetwork connectNeurons(NeuralNetwork nn) {
        /*
         * For each layer connect its input neurons with next layer input
         * neurons.
         */
        for (int lc = 0; lc < nn.getLayersCount(); lc++) {
            if ((lc + 1) < nn.getLayersCount()) {
                Layer from = nn.getLayerAt(lc);
                Layer to = nn.getLayerAt((lc + 1));

                List<Neuron> inputFrom = from.getNeurons();
                List<Neuron> inputTo = to.getNeurons();

                if (inputFrom.size() == inputTo.size()) {
                    int outputNeuronCounter = 0;
                    for (Neuron neuron : inputFrom) {
                        neuron.setError(genDouble());
                        neuron.addInputConnection(new Connection(neuron,
                                inputTo.get(outputNeuronCounter++)));
                    }
                }

            }
        }
        return nn;
    }

    /**
     * Adds connection(s) to the neuron, from neuron, to neuron.
     * 
     * @param neuron
     *            - where the connection will be added.
     * @param howManyConnectionToBeAdded
     *            - the number of connections.
     * 
     * @return the neuron with added connection(s).
     */
    public static Neuron addInputConnection(Neuron neuron,
            int howManyConnectionToBeAdded) {
        for (int i = 0; i < howManyConnectionToBeAdded; i++) {
            neuron.addInputConnection(new Connection(
                    getRandomNeuronWithError(), getRandomNeuronWithError()));
        }
        return neuron;
    }

    /**
     * Adds the input connection to the neuron.
     * 
     * @param neuron
     *            to be connected to.
     * @param howManyConnectionToBeAdded
     *            the amount of connection to be connected to.
     * @param doUseFlatWeight
     *            if use {@link FlatWeight} or not.
     * 
     * @return connected neuron.
     */
    public static Neuron addInputConnection(Neuron neuron,
            int howManyConnectionToBeAdded, boolean doUseFlatWeight) {

        for (int i = 0; i < howManyConnectionToBeAdded; i++) {
            if (doUseFlatWeight) {
                neuron.addInputConnection(new Connection(
                        getRandomNeuronWithError(), getRandomNeuronWithError(),
                        new FlatWeight(genDouble())));
            } else {
                neuron.addInputConnection(new Connection(
                        getRandomNeuronWithError(), getRandomNeuronWithError(),
                        new Weight()));
            }
        }
        return neuron;
    }

    private static Neuron getRandomNeuronWithError() {
        Neuron n = new Neuron();
        n.setError(genDouble());
        n.addOutputConnection(new Connection(n, new Neuron()));
        return n;
    }

    /**
     * Adds {@link Neuron} to the input connection.
     * 
     * @param neuron
     *            to be connected.
     * @param howManyConnectionToBeAdded
     *            the number of neurons to be connected each other.
     * @param trancFunc
     *            transfer function type.
     * 
     * @return the connected neuron.
     */
    public static Neuron addInputConnection(Neuron neuron,
            int howManyConnectionToBeAdded, TransferFunctionType trancFunc) {
        for (int i = 0; i < howManyConnectionToBeAdded; i++) {
            neuron.addInputConnection(new Connection(new Neuron(
                    new InputFunction(), typeToConcretFunction(trancFunc)),
                    new Neuron(new InputFunction(),
                            typeToConcretFunction(trancFunc))));
        }
        return neuron;
    }

    /**
     * Creates specific transfer function indicating by function type.
     * 
     * @param type
     *            transfer function type.
     * 
     * @return a concrete transfer function.
     */
    public static TransferFunction typeToConcretFunction(
            TransferFunctionType type) {
        TransferFunction tf = null;
        switch (type) {
        case GAUSSIAN:
            tf = new Gaussian();
            break;
        case LINEAR:
            tf = new Linear();
            break;
        case LOG:
            tf = new Log();
            break;
        case RAMP:
            tf = new Ramp();
            break;
        case SGN:
            tf = new Sgn();
            break;
        case SIGMOID:
            tf = new Sigmoid();
            break;
        case SIN:
            tf = new Sin();
            break;
        case STEP:
            tf = new Step();
            break;
        case TANH:
            tf = new Tanh();
            break;
        case TRAPEZOID:
            tf = new Trapezoid();
            break;
        default:
            tf = new Step();
            break;
        }
        return tf;
    }

    /**
     * Adds output connection to the specific neuron.
     * 
     * @param neuron
     *            to be connected.
     * @param howManyConnectionToBeAdded
     *            how many neurons to be connected.
     * 
     * @return the connected neuron.
     */
    public static Neuron addOutputConnection(Neuron neuron,
            int howManyConnectionToBeAdded) {
        for (int i = 0; i < howManyConnectionToBeAdded; i++) {
            neuron.addOutputConnection(new Connection(new Neuron(),
                    new Neuron()));
        }
        return neuron;
    }

    /**
     * Checks if two double arrays are equals.
     * 
     * @param original
     *            the first double array.
     * @param toBeTested
     *            the second double array.
     * 
     * @return <code style="color:PURPLE">true</code> if they are equal.
     */
    public static boolean assertDoubleArray(double[] original,
            double[] toBeTested) {
        int sum = 0;
        List<Double> lst = convertTo(toBeTested);
        Collections.sort(lst);
        for (int i = 0; i < original.length; i++) {
            int key = Collections.binarySearch(lst, original[i]);
            if (key != -1)
                sum++;
        }
        return (sum == original.length);
    }

    /**
     * Converts double[] to List<Double>
     * 
     * @param list
     *            the array to be converted.
     * 
     * @return
     */
    public static List<Double> convertTo(double[] list) {
        List<Double> converted = new ArrayList<Double>();
        if (list != null) {
            for (Double elem : list) {
                converted.add(elem);
            }
        }
        return converted;
    }

    /**
     * Generates a {@link TrainingSet}.
     * 
     * @param sizeOfTrainingSet
     *            - the size of the training set. Means how many elements will
     *            be in set.
     * @param sizeOfTrainingElementVectorInput
     *            - the size of the {@link TrainingElement}, i.e. the size of
     *            input vector.
     * 
     * @return the training set.
     */
    public static TrainingSet<TrainingElement> generateTrainingSet(
            int sizeOfTrainingSet, int sizeOfTrainingElementVectorInput) {
        TrainingSet<TrainingElement> trainingSet = new TrainingSet<TrainingElement>(
                sizeOfTrainingSet, sizeOfTrainingSet);

        for (int i = 0; i < sizeOfTrainingSet; i++) {
            trainingSet.addElement(new TrainingElement(TestUtil
                    .genDoubleVector(sizeOfTrainingElementVectorInput)));
        }

        return trainingSet;
    }

    public static TrainingSet<SupervisedTrainingElement> generateSupervisedTrainingSet(
            int sizeOfTrainingSet, int sizeOfTrainingElementVectorInput) {
        TrainingSet<SupervisedTrainingElement> trainingSet = new TrainingSet<SupervisedTrainingElement>(
                sizeOfTrainingSet, sizeOfTrainingSet);

        for (int i = 0; i < sizeOfTrainingSet; i++) {
            trainingSet.addElement(new SupervisedTrainingElement(
                    genDoubleList(sizeOfTrainingElementVectorInput),
                    genDoubleList(sizeOfTrainingElementVectorInput)));
        }

        return trainingSet;
    }

    /**
     * Connects each neuron to other in the same layer. For instance, suppose
     * ANN has three neurons, say A, B, C then A --> B, A --> C; B --> A, B -->
     * C; C --> A, C --> B however the same neuron cannot be connected to
     * itself.
     * 
     * @param layer
     */
    public static void connectAllNeuronsEachOtherOnSameLayer(Layer layer) {
        if (layer != null) {
            int neuronAmount = layer.getNeurons().size();
            List<Neuron> neurons = layer.getNeurons();
            for (int i = 0; i < neuronAmount; i++) {
                for (int j = 0; j < neuronAmount; j++) {
                    if (i != j) {
                        neurons.get(i).setError(genDouble());
                        neurons.get(i).addInputConnection(
                                new Connection(neurons.get(i), neurons.get(j)));
                    }
                }
            }
        }
    }

    /**
     * Connects neurons each other.
     * 
     * @param neurons
     *            a list of neurons to be connected.
     */
    public static void connectNeurons(List<Neuron> neurons) {
        if (neurons != null && !neurons.isEmpty()) {
            for (int i = 0; i < neurons.size(); i++) {
                for (int j = 0; j < neurons.size(); j++) {
                    if (i != j) {
                        neurons.get(i).setError(genDouble());
                        neurons.get(j).setError(genDouble());
                        neurons.get(i).addInputConnection(getConnection(neurons.get(i), neurons.get(j)));
                        neurons.get(j).addInputConnection(getConnection(neurons.get(j), neurons.get(i)));
                        //getConnection
                    }
                }
            }
        }
    }

    private static Connection getConnection(Neuron a, Neuron b){
        Connection toBeReturned = new Connection(a, b);
        toBeReturned.setWeight(getWeight());
        return toBeReturned;
    }

    private static Weight getWeight() {
        Weight w = new Weight();
        w.setTrainingData(new TrainingElement(genDoubleList(KHAMSA)));

        return w;
    }

    /**
     * Sets custom input during the neuron connection.
     * 
     * @param neurons
     *            to be connected each other.
     * @param value
     */
    public static void connectNeuronsCustomInput(List<Neuron> neurons, double value) {
        if (neurons != null && !neurons.isEmpty()) {
            for (int i = 0; i < neurons.size(); i++) {
                for (int j = 0; j < neurons.size(); j++) {
                    if (i != j) {
                        neurons.get(i).setInput(value);
                        neurons.get(j).setInput(value);
                        neurons.get(i).setOutput(value);
                        neurons.get(j).setOutput(value);
                        neurons.get(j).setError(genDouble());
                        neurons.get(i).setError(genDouble());
                        neurons.get(i).addInputConnection(new Connection(neurons.get(i), neurons.get(j)));
                    }
                }
            }
        }
    }

    /**
     * Creates new {@link NeuralNetwork}, adds layers, set input/output
     * connections and connects neurons each other.
     * 
     * @return the initialized ANN with random chosen transfer function.
     */
    public static NeuralNetwork getDefaultNN() {
        NeuralNetwork nn = new NeuralNetwork();
        /* Creates and adds layers to the NN */
        addCompetitiveLayersToNN(KHAMSA, nn);
        /* Adds input neurons */
        nn.setInputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        connectNeurons(nn.getInputNeurons());
        /* Adds output neurons */
        nn.setOutputNeurons(TestUtil.generateNeuronListTF(KHAMSA));
        /* Connects each other */
        connectNeurons(nn.getOutputNeurons());
        return nn;
    }

    public static NeuralNetwork addCompetitiveLayersToNN(int numberOfLayers,
            NeuralNetwork toBeAdded) {
        for (int i = 0; i < numberOfLayers; i++) {
            toBeAdded.addLayer(addDefaultNeuronsToLayer(numberOfLayers
                    / numberOfLayers, new CompetitiveLayer(KHAMSA,
                            new NeuronProperties())));
        }
        return toBeAdded;
    }

    public static void setPrerequirements(NeuralNetwork nn) {
        for (Layer l : nn.getLayers()) {
            connectNeuronsCustomInput(l.getNeurons(), WEIGHT);
            Neuron cn = new Neuron(new InputFunction(), new Linear());
            for (Connection con : cn.getInputConnections()) {
                /* Sets the neuron error, used for update neurons */
                cn.setError(genDouble());
                con.setFromNeuron(cn);
                System.out.println("WEIGHT=" + con.getWeight());
                con.getWeight().setTrainingData(new TrainingElement(genDoubleList(KHAMSA)));
            }
        }
    }

    //MomentumWeightTrainingData
    public static void setPrerequirementsMomentumWeightTrainingData(NeuralNetwork nn) {
        for (Layer l : nn.getLayers()) {
            connectNeuronsCustomInput(l.getNeurons(), WEIGHT);
            Neuron cn = new Neuron(new InputFunction(), new Linear());
            for (Connection con : cn.getInputConnections()) {
                /* Sets the neuron error, used for update neurons */
                cn.setError(genDouble());
                con.setFromNeuron(cn);
                //new MomentumBackpropagation(). new MomentumWeightTrainingData()
                con.getWeight().setTrainingData(new TrainingElement(genDoubleList(KHAMSA)));
            }
        }
    }



    /**
     * Checks if two arrays are equals. Comparison is done on each element.
     * 
     * @param arrayOne
     *            - to be tested.
     * @param arrayTwo
     *            - to be compared to.
     * 
     * @return <code>true</code> if two arrays are equals or <code>false</code>
     *         - otherwise.
     */
    public static boolean isEqual(Object[] arrayOne, Object[] arrayTwo) {
        int successCount = 0;

        for (int i = 0; i < arrayOne.length; i++) {
            if (arrayOne[i].equals(arrayTwo[i])) {
                successCount++;
            }
        }
        return (successCount == arrayOne.length);
    }

    public static List<String> getWeight(Neuron neuron) {
        List<String> response = new ArrayList<String>(1);

        for (Connection connection : neuron.getInputConnections()) {
            response.add(String.valueOf(connection.getWeight()));
        }

        Collections.sort(response);
        return response;
    }
    
    public static boolean assertEqualsStringArray(String[] orig, String[] dest){
        boolean result = true;
        
        for(int i = 0; i < orig.length; i++){
            result = orig[i].equals(dest[i]);
            
            if(!result)
                break;
        }
        
        return result;
    }
}
