/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.Difference;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.input.Intensity;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests the {@link InputFunction} public functionality.
 */
public class TestInputFunction {
    /* Test's member */
    private InputFunction inputFunction = null;

    @Before
    public void setUpBeforeClass() throws Exception {
        inputFunction = new InputFunction();
    }

    @After
    public void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testInputFunction() {
        assertNotNull(inputFunction);
    }

    @Test
    public void testInputFunctionWeightsFunctionSummingFunction() {
        inputFunction = new InputFunction(new Difference(), new Intensity());
        assertNotNull(inputFunction);
    }

    @Test
    public void testGetOutput() {
        NeuralNetwork nn = new NeuralNetwork();
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);

        double result = inputFunction.getOutput(nn.getLayerAt(NIL).getNeuronAt(NIL).getInputConnections());
        assertTrue(result >= NIL);
    }

    @Test
    public void testGetSummingFunction() {
        inputFunction = new InputFunction(new Difference(), new Intensity());
        assertEquals(Intensity.class.getName(), inputFunction.getSummingFunction().toString());
    }

    @Test
    public void testGetWeightsFunction() {
        inputFunction = new InputFunction(new Difference(), new Intensity());
        assertEquals(Difference.class.getName(), inputFunction.getWeightsFunction().toString());
    }

    @Test
    public void testToString(){
        assertNotNull(inputFunction.toString());
    }
}
