/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;

/**
 * Tests {@link Connection} public functionality.
 */
public class TestConnection {
    /* Test's members */
    private Connection connection = null;
    private Neuron fromNeuron = null;
    private Neuron toNeuron = null;

    @Before
    public void setUp() throws Exception {
        fromNeuron = new Neuron();
        toNeuron = new Neuron();
        connection = new Connection(fromNeuron, toNeuron);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testConnectionNeuronNeuron() {
        connection = new Connection(fromNeuron, toNeuron);
        assertNotNull(connection);
    }

    @Test
    public void testConnectionNeuronNeuronWeight() {
        connection = new Connection(fromNeuron, toNeuron, new Weight(WEIGHT));
        assertEquals(WEIGHT, connection.getWeight().value, ParamsTest.EPSILON);
        assertNotNull(connection);
    }

    @Test
    public void testConnectionNeuronNeuronDouble() {
        connection = new Connection(fromNeuron, toNeuron, WEIGHT);
        assertEquals(WEIGHT, connection.getWeight().value, ParamsTest.EPSILON);
        assertNotNull(connection);
    }

    @Test
    public void testGetWeight() {
        connection.setWeight(new Weight(WEIGHT));
        assertEquals(WEIGHT, connection.getWeight().value, ParamsTest.EPSILON);
    }

    @Test
    public void testSetWeight() {
        testGetWeight();
    }

    @Test
    public void testGetInput() {
        assertEquals(fromNeuron.getOutput(), connection.getInput(),
                ParamsTest.EPSILON);
    }

    @Test
    public void testGetWeightedInput() {
        double result = (fromNeuron.getOutput() * connection.getWeight()
                .getValue());
        assertEquals(result, connection.getWeightedInput(), ParamsTest.EPSILON);

    }

    @Test
    public void testGetFromNeuron() {
        assertEquals(fromNeuron, connection.getFromNeuron());
    }

    @Test
    public void testSetFromNeuron() {
        fromNeuron = new Neuron();
        connection.setFromNeuron(fromNeuron);
        assertEquals(fromNeuron, connection.getFromNeuron());
    }

    @Test
    public void testGetToNeuron() {
        assertEquals(toNeuron, connection.getToNeuron());
    }

    @Test
    public void testSetToNeuron() {
        toNeuron = new Neuron();
        connection.setToNeuron(toNeuron);
        assertEquals(toNeuron, connection.getToNeuron());
    }

    @Test
    public void testToString(){
        assertNotNull(connection.toString());
    }
}
