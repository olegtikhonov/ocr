/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.Difference;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests a {@link Difference} functionality.
 */
public class TestDifference {

    private Difference difference = null;

    @Before
    public void setUpBeforeClass() throws Exception {
        difference = new Difference();
    }

    @After
    public void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testGetOutputListOfConnection() {
        NeuralNetwork nn = new NeuralNetwork();
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        double[] result = difference.getOutput(nn.getLayerAt(NIL).getNeuronAt(NIL).getInputConnections());
        assertNotNull(result);
    }

    @Test
    public void testGetOutputDoubleArrayDoubleArray() {
        double[] inputs = TestUtil.genDoubleVector(DECIMAL);
        double[] weights = TestUtil.genDoubleVector(DECIMAL);
        double[] result = difference.getOutput(inputs, weights);
        assertNotNull(result);
    }
}
