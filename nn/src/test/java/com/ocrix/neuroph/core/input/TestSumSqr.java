package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.SumSqr;
import com.ocrix.neuroph.nn.TestUtil;

public class TestSumSqr {
    /* Test's member */
    private SumSqr sumSqr = null;

    @Before
    public void setUp() throws Exception {
        sumSqr = new SumSqr();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = sumSqr.getOutput(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(result >= NIL);
    }
}
