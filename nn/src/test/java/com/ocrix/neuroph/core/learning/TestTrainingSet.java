/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import com.ocrix.neuroph.core.exceptions.VectorSizeMismatchException;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.norm.DecimalScaleNormalizer;
import org.apache.commons.lang.RandomStringUtils;
import org.encog.engine.data.EngineData;
import org.encog.engine.data.EngineIndexableSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.*;


public class TestTrainingSet {
    private static final String TARGET = "target/";
    private static TrainingSet<TrainingElement> ts = null;

    /* ----------------- TESTS ----------------- */
    @Test
    public void testTrainingSet() {
        assertNotNull(ts);
    }

    @Test
    public void testTrainingSetString() {
        String label = RandomStringUtils.random(DECIMAL, true, true);//RandomStringUtils.randomAlphabetic(DECIMAL);
        ts = new TrainingSet<TrainingElement>(label);
        assertEquals(label, ts.getLabel());
    }

    @Test
    public void testTrainingSetInt() {
        ts = new TrainingSet<TrainingElement>(DECIMAL);
        assertEquals(DECIMAL, ts.getInputSize());
    }

    @Test
    public void testTrainingSetIntInt() {
        ts = new TrainingSet<TrainingElement>(DECIMAL, DECIMAL);
        assertEquals(DECIMAL, ts.getInputSize());
        assertEquals(DECIMAL, ts.getOutputSize());
    }

    @Test
    public void testAddElement() {
        TestUtil.addOneElement(ts);

        Iterator<TrainingElement> it = ts.iterator();

        while (it.hasNext()) {
            TrainingElement trainingElement = it.next();
            assertNotNull(trainingElement);
        }
    }

    @Test
    public void testRemoveElementAt() {
        ts.clear();
        TestUtil.addOneElement(ts);

        int sizeBeforeRemoving = ts.size();
        ts.removeElementAt(sizeBeforeRemoving - 1);
        int sizeAfterRemoving = ts.size();

        assertEquals(sizeAfterRemoving, 0);
    }

    @Test
    public void testIterator() {
        try {
            TestUtil.addOneElement(ts);
            Iterator<TrainingElement> it = ts.iterator();
            assertNotNull(it);
            assertTrue(it.hasNext());
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Test
    public void testElements() {
        List<?> te = ts.elements();
        assertNotNull(te);
    }

//    @Test
    public void testElementAt() {
        int size = ts.size();
        TrainingElement telem = ts.elementAt(size - 1);
        assertNotNull(telem);
    }

    @Test
    public void testClear() {
        int sizeBefore = ts.size();
        ts.clear();
        int sizeAfter = ts.size();
        assertTrue(sizeBefore >= sizeAfter);
    }

    @Test
    public void testIsEmpty() {
        ts.clear();
        assertTrue(ts.isEmpty());
    }

//    @Test
    public void testSize() {
        int sizeBefore = ts.size();
        assertTrue(sizeBefore == 0);
        TestUtil.addOneElement(ts);
        assertEquals(1, ts.size());
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setLabel(label);
        assertEquals(label, ts.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testSetFilePath() {
        String filePath = TARGET
                + RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setFilePath(filePath);
        assertEquals(filePath, ts.getFilePath());
    }

    @Test
    public void testGetFilePath() {
        testSetFilePath();
    }

    @Test
    public void testSaveString() {
        ts.save(ts.getFilePath());
        assertTrue(new File(ts.getFilePath()).exists());

    }

    @Test
    public void testSaveException() {
        ts.setFilePath(null);
        ts.save();
        String filePath = TARGET
                + RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setFilePath(filePath);
        assertTrue(true);
    }

//    @Test
    public void testSave() {
        TestUtil.addOneElement(ts);
        ts.save();
        try {
            assertTrue(new File(ts.getFilePath()).exists());
        } catch (Exception e) {
        }
    }

    @Test
    public void testLoad() {
        String filePath = TARGET
                + RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setFilePath(filePath);
        ts.save();
        @SuppressWarnings("unchecked")
        TrainingSet<TrainingElement> newTraS = TrainingSet.load(ts
                .getFilePath());
        Iterator<TrainingElement> it = newTraS.iterator();
        while (it.hasNext()) {
            TrainingElement trainingElement = it.next();
            assertNotNull(trainingElement);
        }
    }

    @Test
    public void testLoadFileDoesNotExist() {
        TrainingSet.load("");
        assertTrue(true);
    }

    @Test
    public void testLoadFileIsNull() {
        TrainingSet.load(null);
        assertTrue(true);
    }

    @Test
    public void testSaveAsTxt() {
        String filePath = TARGET
                + RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setFilePath(filePath);

        ts.saveAsTxt(ts.getFilePath(), "|");
        assertTrue(new File(ts.getFilePath()).exists());
    }

    @Test
    public void testCreateFromFile() {
        @SuppressWarnings("unchecked")
        TrainingSet<TrainingElement> newTraS = TrainingSet.createFromFile(ts.getFilePath(), DECIMAL, DECIMAL, "|");
        Iterator<TrainingElement> it = newTraS.iterator();
        while (it.hasNext()) {
            TrainingElement trainingElement = it.next();
            assertTrue(trainingElement.getInputArray().length >= DECIMAL);
        }

    }

    @Test
    public void testNormalize() {
        try {
            TestUtil.addOneElement(ts);
            List<TrainingElement> trBeforeNormElems = ts.elements();
            ts.normalize();
            List<TrainingElement> trAfterNormElems = ts.elements();

            Iterator<?> before = trBeforeNormElems.iterator();
            Iterator<?> after = trAfterNormElems.iterator();

            while (after.hasNext() && before.hasNext()) {
                TrainingElement afterEl = (TrainingElement) after.next();
                TrainingElement beforeEl = (TrainingElement) before.next();
                TestUtil.assertArrayEquals(afterEl.getInput(),
                        beforeEl.getInput());
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Test
    public void testNormalizeNormalizer() {

        try {
            for (int i = 0; i < DECIMAL; i++) {
                TestUtil.addOneElement(ts);
            }

            List<TrainingElement> trBeforeNormElems = ts.elements();
            ts.normalize(new DecimalScaleNormalizer());
            List<TrainingElement> trAfterNormElems = ts.elements();

            Iterator<?> before = trBeforeNormElems.iterator();
            Iterator<?> after = trAfterNormElems.iterator();

            while (after.hasNext() && before.hasNext()) {
                TrainingElement afterEl = (TrainingElement) after.next();
                TrainingElement beforeEl = (TrainingElement) before.next();
                TestUtil.assertArrayEquals(afterEl.getInput(),
                        beforeEl.getInput());
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Test
    public void testGetIdealSize() {
        ts.getIdealSize();
        assertTrue(true);
    }

    @Test
    public void testSetInputSize() {
        ts.setInputSize((int) Math.pow(DECIMAL, 2));
        assertEquals((int) Math.pow(DECIMAL, 2), ts.getInputSize());
    }

    @Test
    public void testSetOutputSize() {
        ts.setOutputSize((int) Math.pow(DECIMAL, 2));
        assertEquals((int) Math.pow(DECIMAL, 2), ts.getOutputSize());
    }

    @Test
    public void testCreateTrainingAndTestSubsets() {
        @SuppressWarnings("rawtypes")
        TrainingSet[] trained = ts.createTrainingAndTestSubsets(50, 100);
        assertNotNull(trained);
    }

    @Test
    public void testGetOutputSize() {
        testSetOutputSize();
    }

    @Test
    public void testGetInputSize() {
        testSetInputSize();
    }

    @Test
    public void testShuffle() {
        ts.clear();
        ts.setInputSize(DECIMAL);
        ts.setOutputSize(DECIMAL);

        for (int i = 0; i < DECIMAL; i++) {
            TestUtil.addOneElement(ts);
        }

        /* TODO: add more efficient comparison of sets */
        ts.shuffle();
        List<TrainingElement> shuffled = ts.elements();
        assertNotNull(shuffled);
    }

    @Test
    public void testIsSupervised() {
        ts.setOutputSize(1);
        assertTrue(ts.isSupervised());
    }

    @Test
    public void testIsSupervisedZero() {
        ts.setOutputSize(0);
        assertFalse(ts.isSupervised());
    }

    // @Test
    public void testGetRecord() {
        TestUtil.addOneElement(ts);
        long recordCount = ts.getRecordCount();

        for (int i = 0; i < DECIMAL; i++) {
            TestUtil.addOneElement(ts);
        }

        for (int i = 0; i < recordCount; i++) {
            EngineData pair = ts.elementAt(i);
            ts.getRecord(i, pair);
            assertNotNull(pair);
        }
    }

    @Test
    public void testGetRecordCount() {
        ts.getRecordCount();
        assertTrue(true);
    }

    @Test
    public void testOpenAdditional() {
        EngineIndexableSet set = ts.openAdditional();
        assertNotNull(set);
    }

    @Test(expected = VectorSizeMismatchException.class)
    public void testAddElementInputVectorSizeZero() {
        ts.setInputSize(1);
        ts.addElement(new TrainingElement(TestUtil.genDoubleVector(DECIMAL)));
    }

    @Test
    public void testToString() {
        String result = ts.toString();
        assertNotNull(result);
    }

    @Test
    public void testSaveAsTxtDelimiterIsNull() {
        ts.saveAsTxt(TARGET, null);
    }

    @Test
    public void testSaveAsTxtDelimiterIsEmpty() {
        ts.saveAsTxt(TARGET, "");
        assertTrue(true);
    }

    @Test
    public void testCreateFromFileFileIsNotExist() {
        ts.setFilePath("./noFile.txt");
        TrainingSet<?> tran = TrainingSet.createFromFile("./noFile.txt", DECIMAL, DECIMAL, "");
        assertTrue(!tran.isEmpty());
    }

    // @Test
    public void testAddElementSupervised() {
        // new SupervisedTrainingElement(new double[0],
        // TestUtil.genDoubleVector(DECIMAL)).set;
        // ts.setInputSize(0);
        // ts.addElement(new SupervisedTrainingElement(new double[0],
        // TestUtil.genDoubleVector(DECIMAL)));
    }

    /* ----------------- END OF TESTS ----------------- */

    @BeforeClass
    public static void setUp() throws Exception {
        ts = new TrainingSet<TrainingElement>();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        if(ts.getFilePath() != null) {
            new File(ts.getFilePath()).delete();
        }
    }
}
