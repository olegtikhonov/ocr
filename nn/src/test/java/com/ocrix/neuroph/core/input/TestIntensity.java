package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.Intensity;
import com.ocrix.neuroph.nn.TestUtil;


/**
 * Tests a {@link Intensity} public functionality.
 */
public class TestIntensity {
    /* Test's member */
    private Intensity intensity = null;

    @Before
    public void setUp() throws Exception {
        intensity = new Intensity();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        assertNotNull(intensity);
        double result = intensity.getOutput(TestUtil.genDoubleVector(DECIMAL));
        assertTrue(result > NIL);
    }
}
