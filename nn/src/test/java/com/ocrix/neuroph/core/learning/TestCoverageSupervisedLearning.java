/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * 
 */
public class TestCoverageSupervisedLearning {

    private CoverageSupervisedLearning csl = null;
    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        csl = new CoverageSupervisedLearning();
        nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        csl.setNeuralNetwork(nn);
    }

    @Test
    public void testLearnTrainingSetDoubleMaxError() {
        TrainingSet<?> set = TestUtil.generateSupervisedTrainingSet(KHAMSA,
                KHAMSA);
        try {
            csl.learn(set, WEIGHT);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Test
    public void testLearnTrainingSetDoubleMaxErrorMaxIteration() {
        TrainingSet<?> set = TestUtil.generateSupervisedTrainingSet(KHAMSA,
                KHAMSA);
        csl.learn(set, WEIGHT, GLYPH);
    }

    @Test
    public void testGetMaxError() {
        assertTrue((csl.getMaxError() != 0));
    }

    @Test
    public void testDoBatchWeightsUpdate() {
        csl.doBatchWeightsUpdate();
        assertTrue(true);
    }

    @Test
    public void testHasReachedStopCondition() {
        assertTrue(csl.hasReachedStopCondition());
    }

    @Test
    public void testGetPreviousEpochError() {
        csl.setPreviousEpochError();
        assertTrue((csl.getPreviousEpochError() != 0.0d));
    }
    
    
    @Test
    public void testGetMinErrorChangeIterationsLimit() {
        int minError = TestUtil.genInteger();
        csl.setMinErrorChangeIterationsLimit(minError);
        assertEquals(minError, csl.getMinErrorChangeIterationsLimit());
    }
    
    @Test
    public void testGetMinErrorChange() {
        csl.setMinErrorChange(TestUtil.genDouble());
        assertTrue((csl.getMinErrorChange() != 0.0d));
    }
    
    @Test
    public void testMinErrorChangeIterationsCount() {
       csl.getMinErrorChangeIterationsCount();
       assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {

    }
}
