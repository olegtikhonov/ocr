/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests {@link DataSetRow} functionality.
 */
public class TestDataSetRow {

    private DataSetRow dsr;

    @Before
    public void setUp() throws Exception {
        dsr = new DataSetRow(TestUtil.genDoubleVector(KHAMSA),
                TestUtil.genDoubleVector(KHAMSA));
    }

    @Test
    public void testDataSetRowStringString() {
        dsr = new DataSetRow(Arrays.toString(TestUtil.genDoubleVector(KHAMSA)),
                Arrays.toString(TestUtil.genDoubleVector(KHAMSA)));
        assertNotNull(dsr);
    }

    @Test
    public void testDataSetRowDoubleArrayDoubleArray() {
        assertNotNull(dsr);
    }

    @Test
    public void testDataSetRowDoubleArray() {
        dsr = new DataSetRow(TestUtil.genDoubleVector(KHAMSA));
        assertNotNull(dsr);
    }

    @Test
    public void testDataSetRowArrayListOfDoubleArrayListOfDouble() {
        ArrayList<Double> input = (ArrayList<Double>) TestUtil
                .convertTo(TestUtil.genDoubleVector(KHAMSA));
        ArrayList<Double> output = (ArrayList<Double>) TestUtil
                .convertTo(TestUtil.genDoubleVector(KHAMSA));
        dsr = new DataSetRow(input, output);
        assertNotNull(dsr);
    }

    @Test
    public void testDataSetRowArrayListOfDouble() {
        ArrayList<Double> input = (ArrayList<Double>) TestUtil
                .convertTo(TestUtil.genDoubleVector(KHAMSA));
        dsr = new DataSetRow(input);
        assertNotNull(dsr);
    }

    @Test
    public void testGetInput() {
        double[] input = TestUtil.genDoubleVector(KHAMSA);
        dsr.setInput(input);
        assertEquals(input, dsr.getInput());
    }

    @Test
    public void testSetInput() {
        testGetInput();
    }

    @Test
    public void testGetDesiredOutput() {
        double[] desiredOutput = TestUtil.genDoubleVector(KHAMSA);
        dsr.setDesiredOutput(desiredOutput);
        assertEquals(desiredOutput, dsr.getDesiredOutput());
    }

    @Test
    public void testSetDesiredOutput() {
        testGetDesiredOutput();
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.randomAlphabetic(KHAMSA);
        dsr.setLabel(label);
        assertEquals(label, dsr.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testIsSupervised() {
        // false case
        dsr.setDesiredOutput(null);
        assertFalse(dsr.isSupervised());
        // true case
        dsr.setDesiredOutput(TestUtil.genDoubleVector(KHAMSA));
        assertTrue(dsr.isSupervised());
    }

    @Test
    public void testToString() {
        assertTrue(dsr.toString().contains(DataSetRow.class.getName()));
    }
}
