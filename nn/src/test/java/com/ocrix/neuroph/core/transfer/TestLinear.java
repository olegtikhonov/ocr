/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests a {@link Linear} public functionality.
 */
public class TestLinear {
    /* Test's member */
    private Linear linear = null;

    @Before
    public void setUp() throws Exception {
        linear = new Linear();
    }

    @Test
    public void testGetOutput() {
        double result = linear.getOutput(WEIGHT);
        assertTrue(result >= 0);
    }

    @Test
    public void testGetDerivative() {
        double result = linear.getDerivative(WEIGHT);
        assertEquals(1, result, EPSILON);
    }

    @Test
    public void testLinear() {
        assertNotNull(linear);
    }

    @Test
    public void testLinearDouble() {
        linear = new Linear(WEIGHT);
        double result = linear.getSlope();
        assertEquals(WEIGHT, result, EPSILON);
    }

    @Test
    public void testLinearProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), WEIGHT);
        linear = new Linear(props);
        assertNotNull(linear);
    }


    @Test
    public void testLinearNullProperties() {
        linear = new Linear(null);
        assertNotNull(linear);
    }

    @Test
    public void testLinearNumberFormatExceptionProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), WEIGHT + "~!@#$%^&*()_+");
        linear = new Linear(props);
        assertNotNull(linear);
    }


    @Test
    public void testGetSlope() {
        linear.setSlope(EPSILON);
        double result = linear.getSlope();
        assertEquals(EPSILON, result, EPSILON);
    }

    @Test
    public void test(){
        String str = linear.toString();
        assertNotNull(str);
    }

    @Test
    public void testSetSlope() {
        testGetSlope();
    }

    @After
    public void tearDown() throws Exception {
    }
}
