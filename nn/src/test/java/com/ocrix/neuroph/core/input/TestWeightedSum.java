package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.WeightedSum;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests {@link WeightedSum} public functionality.
 */
public class TestWeightedSum {

    /* Test's member */
    private WeightedSum ws = null;

    @Before
    public void setUp() throws Exception {
        ws = new WeightedSum();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutputListOfConnection() {
        NeuralNetwork nn = new NeuralNetwork();
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        double result = ws.getOutput(nn.getLayerAt(NIL).getNeuronAt(NIL)
                .getInputConnections());
        assertNotNull(result);
    }

    @Test
    public void testGetOutputDoubleArrayDoubleArray() {
        double[] inputs = TestUtil.genDoubleVector(DECIMAL);
        double[] weights = TestUtil.genDoubleVector(DECIMAL);
        double[] result = ws.getOutput(inputs, weights);
        assertNotNull(result);
    }
}
