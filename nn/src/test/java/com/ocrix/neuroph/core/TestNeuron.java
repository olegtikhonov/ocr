package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.core.input.WeightedSum;
import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.core.transfer.Log;
import com.ocrix.neuroph.core.transfer.Ramp;
import com.ocrix.neuroph.core.transfer.Sgn;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.core.transfer.Sin;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.core.transfer.Tanh;
import com.ocrix.neuroph.core.transfer.TransferFunction;
import com.ocrix.neuroph.core.transfer.Trapezoid;
import com.ocrix.neuroph.nn.TestUtil;

public class TestNeuron {

    private Neuron neuron = null;

    @Before
    public void setUpBeforeTest() throws Exception {
        neuron = new Neuron();
    }

    @After
    public void tearDownAfterTest() throws Exception {
    }

    @Test
    public void testNeuron() {
        assertNotNull(neuron);
    }

    @Test
    public void testNeuronInputFunctionTransferFunction() {
        neuron = new Neuron(new WeightedSum(), new Ramp());
        assertNotNull(neuron);
    }

    @Test
    public void testCalculate() {
        neuron.addInputConnection(new Connection(new Neuron(new WeightedSum(),
                new Ramp()), new Neuron(new WeightedSum(), new Sigmoid())));
        neuron.calculate();
        assertEquals(0.0d, neuron.getNetInput(), EPSILON);
        assertEquals(0.0d, neuron.getOutput(), EPSILON);
    }

    @Test
    public void testReset() {
        neuron.reset();
        assertEquals(0.0d, neuron.getNetInput(), EPSILON);
        assertEquals(0.0d, neuron.getOutput(), EPSILON);
    }

    @Test
    public void testSetInput() {
        neuron.setInput(EPSILON);
        assertEquals(EPSILON, neuron.getNetInput(), EPSILON);
    }

    @Test
    public void testGetNetInput() {
        testSetInput();
    }

    @Test
    public void testGetOutput() {
        neuron.setOutput(EPSILON);
        assertEquals(EPSILON, neuron.getOutput(), EPSILON);
    }

    @Test
    public void testHasInputConnections() {
        neuron.addInputConnection(new Connection(new Neuron(), new Neuron()));
        boolean result = neuron.hasInputConnections();
        assertTrue(result);
    }

    @Test
    public void testGetInputsIterator() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        Iterator<Connection> it = neuron.getInputsIterator();
        assertNotNull(it);
        int counter = 0;
        while (it.hasNext()) {
            Connection connection = it.next();
            assertNotNull(connection);
            ++counter;
        }
        assertEquals(KHAMSA, counter);
    }

    @Test
    public void testAddInputConnectionConnection() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        assertEquals(KHAMSA, neuron.getInputConnections().size());

    }

    @Test
    public void testAddInputConnectionNeuron() {
        Neuron n = new Neuron();
        neuron.addInputConnection(n);
        assertEquals(1, neuron.getInputConnections().size());
    }

    @Test
    public void testAddInputConnectionNeuronDouble() {
        Neuron n = new Neuron();
        neuron.addInputConnection(n, EPSILON);
        assertEquals(1, neuron.getInputConnections().size());
    }

    @Test
    public void testAddOutputConnection() {
        neuron.addOutputConnection(new Connection(new Neuron(), new Neuron()));
        assertEquals(1, neuron.getOutConnections().size());
    }

    @Test
    public void testGetInputConnections() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        List<Connection> cons = neuron.getInputConnections();
        assertEquals(KHAMSA, cons.size());
    }

    @Test
    public void testGetOutConnections() {
        TestUtil.addOutputConnection(neuron, KHAMSA);
        List<Connection> outCons = neuron.getOutConnections();
        assertEquals(KHAMSA, outCons.size());
    }

    @Test
    public void testRemoveInputConnectionFrom() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        List<Connection> cons = neuron.getInputConnections();
        assertEquals(KHAMSA, cons.size());

        /* Here we removing the connection */
        Neuron from = cons.get(0).getFromNeuron();
        neuron.removeInputConnectionFrom(from);
        assertTrue(true);
        /* Checking if the remove is succeeded */
        assertTrue((KHAMSA - 1) == neuron.getInputConnections().size());
    }

    @Test
    public void testGetConnectionFrom() {
        NeuralNetwork nn = new NeuralNetwork();
        TestUtil.addLayersToNN(KHAMSA, nn);

        for (Layer layer : nn.getLayers()) {
            TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
            TestUtil.connectAllNeuronsEachOtherOnSameLayer(layer);
        }

        //        TestUtil.addInputConnection(neuron, KHAMSA);
        List<Connection> cons = neuron.getInputConnections();

        for (Connection c : cons) {
            Connection conn = neuron.getConnectionFrom(c.getFromNeuron());
            assertNotNull(conn);
        }
    }

    @Test
    public void testSetInputFunction() {
        WeightedSum wsf = new WeightedSum();
        neuron.setInputFunction(wsf);
        assertEquals(wsf, neuron.getInputFunction());
    }

    @Test
    public void testSetTransferFunction() {
        TransferFunction tf = new Sigmoid();
        checkTransferFunction(tf);

        tf = new Gaussian();
        checkTransferFunction(tf);

        tf = new Linear();
        checkTransferFunction(tf);

        tf = new Log();
        checkTransferFunction(tf);

        tf = new Ramp();
        checkTransferFunction(tf);

        tf = new Sgn();
        checkTransferFunction(tf);

        tf = new Sin();
        checkTransferFunction(tf);

        tf = new Step();
        checkTransferFunction(tf);

        tf = new Tanh();
        checkTransferFunction(tf);

        tf = new Trapezoid();
        checkTransferFunction(tf);
    }

    private void checkTransferFunction(TransferFunction tf) {
        neuron.setTransferFunction(tf);
        assertEquals(tf, neuron.getTransferFunction());
    }

    @Test
    public void testGetInputFunction() {
        testSetInputFunction();
    }

    @Test
    public void testGetTransferFunction() {
        testSetTransferFunction();
    }

    @Test
    public void testSetParentLayer() {
        Layer parent = new Layer();
        neuron.setParentLayer(parent);
        assertEquals(parent, neuron.getParentLayer());
    }

    @Test
    public void testGetParentLayer() {
        testSetParentLayer();
    }

    @Test
    public void testGetWeightsVector() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        List<Weight> weights = neuron.getWeightsVector();
        assertNotNull(weights);
        assertEquals(KHAMSA, weights.size());
    }

    @Test
    public void testGetError() {
        neuron.setError(EPSILON);
        assertEquals(EPSILON, neuron.getError(), EPSILON);
    }

    @Test
    public void testSetError() {
        testGetError();
    }

    @Test
    public void testSetOutput() {
        neuron.setOutput(KHAMSA);
        assertEquals(KHAMSA, neuron.getOutput(), EPSILON);
    }

    @Test
    public void testRandomizeInputWeights() {
        neuron.randomizeInputWeights();
        assertTrue(true);
    }

    @Test
    public void testRandomizeInputWeightsDoubleDouble() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        neuron.randomizeInputWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsDouble() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        neuron.initializeWeights(MAX);
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsRandom() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        neuron.initializeWeights(new Random());
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsDoubleDouble() {
        TestUtil.addInputConnection(neuron, KHAMSA);
        neuron.initializeWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testGetLabel() {
        String randLabel = RandomStringUtils.randomAlphabetic(KHAMSA);
        neuron.setLabel(randLabel);
        assertEquals(randLabel, neuron.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testToString() {
        String toString = neuron.toString();
        assertNotNull(toString);
    }

}
