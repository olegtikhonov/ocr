package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.UNITY;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests {@link Layer} public functionality.
 */
public class TestLayer {
    /* Test's member(s) */
    private Layer layer = null;
    private Neuron toBeAdded = null;

    @Test
    public void testLayer() {
        assertNotNull(layer);
    }

    @Test
    public void testLayerIntNeuronProperties() {
        Properties props = new NeuronProperties();
        layer = new Layer(DECIMAL, (NeuronProperties) props);
        assertNotNull(layer);
    }

    @Test
    public void testSetParentNetwork() {
        NeuralNetwork nn = new NeuralNetwork();
        layer.setParentNetwork(nn);
        assertEquals(nn, layer.getParentNetwork());
    }

    @Test
    public void testGetParentNetwork() {
        NeuralNetwork nn = new NeuralNetwork();
        layer.setParentNetwork(nn);
        assertEquals(nn, layer.getParentNetwork());
    }

    @Test
    public void testGetNeuronsIterator() {
        layer.getNeurons().clear();
        /* Adds neurons to the parent n. network */
        layer = TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);

        /* Added neuron counter, for validation */
        int result = 0;
        Iterator<Neuron> it = layer.getNeuronsIterator();

        while (it.hasNext()) {
            Neuron neuron = it.next();
            assertNotNull(neuron);
            ++result;
        }

        assertEquals(KHAMSA, result);
    }

    @Test
    public void testGetNeurons() {
        layer.getNeurons().clear();
        /* Adds neurons to the parent n. network */
        layer = TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
        assertEquals(KHAMSA, layer.getNeurons().size());
    }

    @Test
    public void testAddNeuronNeuron() {
        layer.getNeurons().clear();
        /* Adds neurons to the parent n. network */
        layer = TestUtil.addDefaultNeuronsToLayer(KHAMSA, layer);
        assertEquals(KHAMSA, layer.getNeurons().size());
    }

    @Test
    public void testAddNeuronIntNeuron() {
        layer.addNeuron(UNITY, toBeAdded);
        assertEquals(UNITY + GLYPH, layer.getNeurons().size());
    }

    @Test
    public void testSetNeuron() {
        /*
         * What is the difference between addNeuron and setNeuron ??? addNeuron
         * - adds neuron to the specific place and if other neuron exists,
         * shifts it. setNeuron - put the given neuron into specific place and
         * if other neuron is in there replaces it, removes the old one and puts
         * a new one.
         */
        layer.setNeuron(UNITY, toBeAdded);
        assertEquals(toBeAdded, layer.getNeuronAt(UNITY));
    }

    @Test
    public void testRemoveNeuron() {
        layer.setNeuron(UNITY, toBeAdded);
        layer.removeNeuron(toBeAdded);
        int index = layer.indexOf(toBeAdded);
        assertEquals((~UNITY + 1), index);
    }

    @Test
    public void testRemoveNeuronAt() {
        layer.setNeuron(UNITY, toBeAdded);
        layer.removeNeuronAt(UNITY);
        int index = layer.indexOf(toBeAdded);
        assertEquals((~UNITY + 1), index);
    }

    @Test
    public void testGetNeuronAt() {
        Neuron toBeGotten = layer.getNeuronAt(UNITY);
        assertNotNull(toBeGotten);
    }

    @Test
    public void testIndexOf() {
        layer.addNeuron(UNITY, toBeAdded);

    }

    @Test
    public void testGetNeuronsCount() {
        assertEquals(GLYPH, layer.getNeuronsCount());
    }

    @Test
    public void testCalculate() {
        layer.calculate();
        assertTrue(true);
    }

    @Test
    public void testReset() {
        layer.reset();
        assertTrue(true);
    }

    @Test
    public void testRandomizeWeights() {
        layer.randomizeWeights();
        assertTrue(true);
    }

    @Test
    public void testRandomizeWeightsDoubleDouble() {
        layer.randomizeWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsDouble() {
        layer.initializeWeights(WEIGHT);
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsRandom() {
        RandomGaussian gaussian = new RandomGaussian();
        layer.initializeWeights(gaussian);
    }

    @Test
    public void testInitializeWeightsDoubleDouble() {
        layer.initializeWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.randomAlphabetic(KHAMSA);
        layer.setLabel(label);
        assertEquals(label, layer.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testToString() {
        String to = layer.toString();
        assertNotNull(to);
    }

    /**
     * Internal class extending {@link Random}.
     */
    final class RandomGaussian extends Random {
        private static final long serialVersionUID = -1916083095794595684L;

        protected double getGaussian(double aMean, double aVariance) {
            return aMean + super.nextGaussian() * aVariance;
        }
    }

    @Before
    public void setUp() throws Exception {
        toBeAdded = new Neuron();
        layer = new Layer();
        layer = TestUtil.addDefaultNeuronsToLayer(GLYPH, layer);
    }

    @After
    public void tearDown() throws Exception {
        /* Removes added neurons from the ANN */
        layer.getNeurons().clear();
    }
}
