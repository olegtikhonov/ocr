/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.exceptions.NeurophException;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.norm.DecimalScaleNormalizer;
import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.PRICE_ENDING;

/**
 * Tests {@link DataSet} functionality.
 */
public class TestDataSet {
    private static final String PATH_FILE = ".";
    private DataSet dataSet;

    @Before
    public void setUp() throws Exception {
        dataSet = new DataSet(DECIMAL);
    }

    @Test
    public void testDataSetInt() {
        assertNotNull(dataSet);
    }

    @Test
    public void testDataSetIntInt() {
        dataSet = new DataSet(DECIMAL, DECIMAL);
        assertNotNull(dataSet);
    }

    @Test
    public void testAddRowDataSetRow() {
        ArrayList<Double> input = (ArrayList<Double>) TestUtil
                .genDoubleList(DECIMAL);
        DataSetRow dataSetRow = new DataSetRow(input);
        dataSet.addRow(dataSetRow);
        DataSetRow result = dataSet.getRowAt(0);
        assertEquals(dataSetRow, result);
    }

    @Test
    public void testAddRowDoubleArray() {
        double[] input = TestUtil.genDoubleVector(DECIMAL);
        dataSet.addRow(input);
        double[] result = dataSet.getRowAt(0).getInput();
        assertEquals(result, input);
    }

    @Test
    public void testAddRowDoubleArrayDoubleArray() {
        double[] input = TestUtil.genDoubleVector(DECIMAL);
        double[] output = TestUtil.genDoubleVector(DECIMAL);

        dataSet.addRow(input, output);
        double[] resIn = dataSet.getRowAt(0).getInput();
        assertEquals(resIn, input);

        double[] resOut = dataSet.getRowAt(0).getDesiredOutput();
        assertEquals(resOut, output);
    }

    @Test
    public void testRemoveRowAt() {
        addRow(dataSet, 1);
        int vp_num_rows = dataSet.getRows().size();
        dataSet.removeRowAt(0);
        int result = dataSet.getRows().size();
        assertTrue(vp_num_rows > result);
    }

    @Test
    public void testIterator() {
        addRow(dataSet, 1);
        @SuppressWarnings("unchecked")
        Iterator<DataSetRow> it = dataSet.iterator();
        assertTrue(it.hasNext());
    }

    @Test
    public void testGetRows() {
        addRow(dataSet, 1);
        assertEquals(dataSet.getRows().size(), 1);
    }

    @Test
    public void testGetRowAt() {
        double[] input = TestUtil.genDoubleVector(DECIMAL);
        dataSet.addRow(input);

        assertEquals(input, dataSet.getRowAt(0).getInput());
    }

    @Test
    public void testClear() {
        addRow(dataSet, 1);

        int size = dataSet.getRows().size();
        dataSet.clear();
        int result = dataSet.getRows().size();
        assertTrue(size > result);
    }

    @Test
    public void testIsEmpty() {
        addRow(dataSet, 1);
        boolean not_empty = dataSet.isEmpty();
        assertFalse(not_empty);
        dataSet.clear();
        boolean empty = dataSet.isEmpty();
        assertTrue(empty);
    }

    @Test
    public void testIsSupervised() {
        addRow(dataSet, 1);
        assertFalse(dataSet.isSupervised());
    }

    @Test
    public void testSize() {
        addRow(dataSet, 1);
        assertEquals(1, dataSet.getRows().size());
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.randomAlphabetic(DECIMAL);
        dataSet.setLabel(label);
        assertEquals(label, dataSet.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testGetColumnNames() {
        String[] names = new String[DECIMAL];
        for (int i = 0; i < DECIMAL; i++) {
            names[i] = RandomStringUtils.randomAlphabetic(DECIMAL);
        }

        dataSet.setColumnNames(names);
        assertTrue(TestUtil.assertEqualsStringArray(names,
                dataSet.getColumnNames()));
    }

    @Test
    public void testSetColumnNames() {
        testGetColumnNames();
    }

    @Test
    public void testSetFilePath() {
        dataSet.setFilePath(PATH_FILE);
        assertEquals(PATH_FILE, dataSet.getFilePath());
    }

    @Test
    public void testGetFilePath() {
        testSetFilePath();
    }

    @Test
    public void testToString() {
        assertTrue(dataSet.toString().contains(dataSet.getLabel()));
    }

    @Test
    public void testSaveString() {
        addRow(dataSet, DECIMAL);
        dataSet.save(ParamsTest.PATH_TO_SAVE_NN
                + TestDataSet.class.getSimpleName());
    }

    @Test
    public void testSave() {
        addRow(dataSet, DECIMAL);
        dataSet.setFilePath(ParamsTest.PATH_TO_SAVE_NN
                + System.currentTimeMillis());
        dataSet.save();
    }

    @Test
    public void testSaveAsTxt() {
        addRow(dataSet, DECIMAL);
        dataSet.saveAsTxt(
                ParamsTest.PATH_TO_SAVE_NN + "_as_text_"
                        + System.currentTimeMillis(), "-->");
    }

    @Test
    public void testLoad() {
        addRow(dataSet, DECIMAL);
        dataSet.save(ParamsTest.PATH_TO_SAVE_NN + "_to_be_loaded_");

        DataSet loaded = DataSet.load(ParamsTest.PATH_TO_SAVE_NN
                + "_to_be_loaded_");
        assertEquals(dataSet.getRows().size(), loaded.getRows().size());
    }

    @Test
    public void testCreateFromFile() {
        String separator = "-->";
        addRow(dataSet, DECIMAL);
        dataSet.saveAsTxt(
                ParamsTest.PATH_TO_SAVE_NN + TestDataSet.class.getSimpleName(),
                separator);

        DataSet loaded = DataSet.createFromFile(ParamsTest.PATH_TO_SAVE_NN
                + TestDataSet.class.getSimpleName(), DECIMAL, DECIMAL,
                separator);

        assertEquals(dataSet.getRows().size(), loaded.getRows().size());
    }

    @Test
    public void testNormalize() {
        addRow(dataSet, DECIMAL);
        dataSet.normalize();
        assertTrue(true);
    }

    @Test
    public void testNormalizeNormalizer() {
        addRow(dataSet, DECIMAL);
        dataSet.normalize(new DecimalScaleNormalizer());
        assertTrue(true);
    }

    @Test
    public void testCreateTrainingAndTestSubsets() {
        addRow(dataSet, DECIMAL);
        DataSet[] set = dataSet.createTrainingAndTestSubsets(PRICE_ENDING,
                PRICE_ENDING);
        assertEquals(GLYPH, set.length);
    }

    @Test
    public void testGetOutputSize() {
        addRow(dataSet, 1);
        assertEquals(DECIMAL, dataSet.getOutputSize());
    }

    @Test
    public void testGetInputSize() {
        addRow(dataSet, 1);
        assertEquals(DECIMAL, dataSet.getInputSize());
    }

    @Test
    public void testShuffle() {
        addRow(dataSet, 1);
        dataSet.shuffle();
        assertTrue(true);
    }

    // ------------ Negative testing --------------

    @Test
    public void testloadFileIsNotExist() {
        String path = "../.." + this.getClass().getName();
        dataSet.setFilePath(path);
        assertNull(DataSet.load(path));
    }

    @Test
    public void testSaveAsTxtDelimiterNull() {
        // null case
        dataSet.saveAsTxt(null, null);
        assertTrue(true);
        // empty case
        dataSet.saveAsTxt(null, "");
        assertTrue(true);
    }

    @Test
    public void testSavePathNull() {
        // null case
        dataSet.setFilePath(null);
        dataSet.save();
        assertTrue(true);
    }

    @Test(expected = NeurophException.class)
    public void testAddRowNullRow() {
        DataSetRow row = null;
        dataSet.addRow(row);
        assertTrue(true);
    }

    private void addRow(DataSet dataSet, int numberOrRows) {
        for (int i = 0; i < numberOrRows; i++) {
            dataSet.addRow(TestUtil.genDoubleVector(DECIMAL),
                    TestUtil.genDoubleVector(DECIMAL));
        }
    }
}
