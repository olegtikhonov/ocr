package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests a {@link Weight} functionality.
 */
public class TestWeight {
    /* Test member */
    private Weight weight = null;

    @Before
    public void setUpBeforeClass() throws Exception {
        weight = new Weight();
    }

    @After
    public void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testWeight() {
        assertNotNull(weight);
    }

    @Test
    public void testWeightDouble() {
        weight = new Weight(EPSILON);
        assertNotNull(weight);
    }

    @Test
    public void testInc() {
        weight = new Weight(EPSILON);
        weight.inc(MAX);
        assertEquals(EPSILON + MAX, weight.getValue(), EPSILON);
    }

    @Test
    public void testDec() {
        weight = new Weight(EPSILON);
        weight.dec(EPSILON);
        assertEquals(NIL, weight.getValue(), EPSILON);
    }

    @Test
    public void testSetValue() {
        weight.setValue(MAX);
        assertEquals(MAX, weight.getValue(), EPSILON);
    }

    @Test
    public void testGetValue() {
        testSetValue();
    }

    @Test
    public void testToString() {
        assertNotNull(weight.toString());
    }

    @Test
    public void testRandomize() {
        weight.randomize();
        assertTrue(true);
    }

    @Test
    public void testRandomizeDoubleDouble() {
        weight.randomize(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testGetTrainingData() {
        TrainingSet<TrainingElement> trainingData = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        TestUtil.initSet(trainingData);
        weight.setTrainingData(trainingData);
        @SuppressWarnings("unchecked")
        TrainingSet<TrainingElement> resultData = (TrainingSet<TrainingElement>) weight
        .getTrainingData();
        assertNotNull(resultData);
        assertEquals(trainingData, resultData);
    }

    @Test
    public void testSetTrainingData() {
        testGetTrainingData();
    }

}
