/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.EVENT_HAPPENED;
import static com.ocrix.neuroph.common.ParamsTest.INIT_STATE;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.PATH_TO_SAVE_NN;
import static com.ocrix.neuroph.common.ParamsTest.UNITY;
import static com.ocrix.neuroph.util.NeuralNetworkType.ADALINE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.learning.AntiHebbianLearning;
import com.ocrix.neuroph.nnet.learning.BackPropagation;
import com.ocrix.neuroph.nnet.learning.KohonenLearning;
import com.ocrix.neuroph.util.plugins.PluginBase;
import com.ocrix.neuroph.util.random.WeightsRandomizer;

public class TestNeuralNetwork {

    private NeuralNetwork nn = null;

    @Before
    public void setUp() throws Exception {
        nn = new NeuralNetwork();
    }

    @After
    public void tearDown() throws Exception {
        nn.getLayers().clear();
    }

    @Test
    public void testNeuralNetwork() {
        assertNotNull(nn);
    }

    @Test
    public void testAddLayerLayer() {
        Layer layer = new Layer();
        nn.addLayer(layer);
        Layer result = nn.getLayerAt(0);
        assertEquals(layer, result);
    }

    @Test
    public void testAddLayerIntLayer() {
        nn = TestUtil.addLayersToNN(DECIMAL, nn);
        Layer toBeAdded = TestUtil.addDefaultNeuronsToLayer(UNITY, new Layer());
        nn.addLayer(KHAMSA, toBeAdded);
        Layer result = nn.getLayerAt(KHAMSA);
        assertEquals(toBeAdded, result);
    }

    @Test
    public void testRemoveLayer() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        Layer toBeAdded = TestUtil.addDefaultNeuronsToLayer(UNITY, new Layer());
        nn.addLayer(toBeAdded);
        int layerCountBefore = nn.getLayers().size();
        nn.removeLayer(toBeAdded);
        int layerCountAfter = nn.getLayers().size();
        assertTrue(layerCountBefore > layerCountAfter);
    }

    @Test
    public void testRemoveLayerAt() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        Layer toBeAdded = TestUtil.addDefaultNeuronsToLayer(UNITY, new Layer());
        nn.addLayer(UNITY, toBeAdded);
        nn.removeLayerAt(UNITY);
        Layer result = nn.getLayerAt(UNITY);
        assertFalse(toBeAdded.equals(result));
    }

    @Test
    public void testGetLayersIterator() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        Iterator<Layer> it = nn.getLayersIterator();
        assertNotNull(it);
        int layerCount = 0;
        while (it.hasNext()) {
            Layer layer = it.next();
            assertNotNull(layer);
            layerCount++;
        }
        assertEquals(KHAMSA, layerCount);
    }

    @Test
    public void testGetLayers() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        List<Layer> layers = nn.getLayers();
        assertNotNull(layers);
        assertTrue(!layers.isEmpty());
    }

    @Test
    public void testGetLayerAt() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        Layer toBeAdded = TestUtil.addDefaultNeuronsToLayer(UNITY, new Layer());
        nn.addLayer(UNITY, toBeAdded);
        Layer result = nn.getLayerAt(UNITY);
        assertEquals(toBeAdded, result);
    }


    @Test
    public void testGetLayerAtWhenLayersNull() {
        Layer result = nn.getLayerAt(UNITY);
        assertNotNull(result);
        //        assertEquals(toBeAdded, result);
    }

    @Test
    public void testIndexOf() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        Layer toBeAdded = TestUtil.addDefaultNeuronsToLayer(UNITY, new Layer());
        nn.addLayer(UNITY, toBeAdded);
        int index = nn.indexOf(toBeAdded);
        assertEquals(UNITY, index);
    }

    @Test
    public void testGetLayersCount() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        int result = nn.getLayersCount();
        assertEquals(KHAMSA, result);
    }

    @Test
    public void testSetInput() {
        double[] arr = TestUtil.genDoubleVector(DECIMAL);
        nn.setInput(arr);
        assertEquals(DECIMAL, nn.getInputNeurons().size());
    }

    @Test
    public void testSetInputEmpty() {
        double[] arr = new double[1];
        nn.setInput(arr);
        assertTrue(true);
    }

    @Test
    public void testGetOutput() {
        nn.setOutputNeurons(TestUtil.generateNeuronList(KHAMSA));
        assertEquals(KHAMSA, nn.getOutput().length);
    }

    @Test
    public void testCalculate() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.calculate();
        assertTrue(true);
    }

    @Test
    public void testReset() {
        nn.setOutputNeurons(TestUtil.generateNeuronList(KHAMSA));
        nn.setInputNeurons(TestUtil.generateNeuronList(KHAMSA));
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.reset();
        List<Layer> input = nn.getLayers();
        for (Layer l : input) {
            List<Neuron> neurons = l.getNeurons();
            for (Neuron n : neurons) {
                assertEquals(0.0d, n.getNetInput(), EPSILON);
                assertEquals(0.0d, n.getOutput(), EPSILON);
            }
        }
    }

    @Test
    public void testLearnInNewThreadTrainingSetOfQ() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn.learnInNewThread(set);
        assertTrue(true);
        nn.stopLearning();
    }

    @Test
    public void testLearnInNewThreadTrainingSetOfQRuleIsNull() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        //        nn.setLearningRule(null);
        nn.learnInNewThread(set);
        assertTrue(true);
        nn.stopLearning();
    }

    @Test
    public void testLearnInNewThreadTrainingSetOfQLearningRule() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        LearningRule lr = new KohonenLearning();
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        lr.setNeuralNetwork(nn);
        nn.learnInNewThread(set, lr);
        assertTrue(true);
        nn.stopLearning();
    }

    @Test
    public void testLearnTrainingSetOfQ() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.learn(set);
        assertTrue(true);
    }

    @Test
    public void testLearnTrainingSetOfQLearningRule() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.setLearningRule(new KohonenLearning());
        LearningRule lr = nn.getLearningRule();
        nn.learn(set, lr);
        assertTrue(true);
    }

    @Test
    public void testStopLearning() {
        nn.setLearningRule(new KohonenLearning());
        nn.stopLearning();
        assertTrue(true);
    }

    @Test
    public void testPauseLearning() {
        // System.setProperty(CommonMessages.LOG_LEVEL_PROPERTY.to(),
        // Level.WARNING.getName());
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.setLearningRule(new AntiHebbianLearning());
        LearningRule lr = nn.getLearningRule();
        nn.learn(set, lr);
        nn.pauseLearning();
        assertTrue(true);
        // System.setProperty(CommonMessages.LOG_LEVEL_PROPERTY.to(),
        // Level.OFF.getName());
    }

    @Test
    public void testResumeLearning() {
        // System.setProperty(CommonMessages.LOG_LEVEL_PROPERTY.to(),
        // Level.WARNING.getName());
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.setLearningRule(new AntiHebbianLearning());
        LearningRule lr = nn.getLearningRule();
        nn.learn(set, lr);
        nn.resumeLearning();
        assertTrue(true);
        // System.setProperty(CommonMessages.LOG_LEVEL_PROPERTY.to(),
        // Level.OFF.getName());
    }

    @Test
    public void testRandomizeWeights() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        nn.randomizeWeights();
        assertTrue(true);
    }

    @Test
    public void testRandomizeWeightsDoubleDouble() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        nn.randomizeWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testRandomizeWeightsRandom() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        nn.randomizeWeights(new Random());
        assertTrue(true);
    }

    @Test
    public void testRandomizeWeightsWeightsRandomizer() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn = TestUtil.connectNeurons(nn);
        nn.randomizeWeights(WeightsRandomizer.getInstance());
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsDouble() {
        TestUtil.addLayersToNN(KHAMSA, nn);
        TestUtil.connectNeurons(nn);
        nn.initializeWeights(KHAMSA);
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsRandom() {
        TestUtil.addLayersToNN(KHAMSA, nn);
        TestUtil.connectNeurons(nn);
        nn.initializeWeights(new Random());
        assertTrue(true);
    }

    @Test
    public void testInitializeWeightsDoubleDouble() {
        TestUtil.addLayersToNN(KHAMSA, nn);
        TestUtil.connectNeurons(nn);
        nn.initializeWeights(MIN, MAX);
        assertTrue(true);
    }

    @Test
    public void testGetNetworkType() {
        nn.setNetworkType(ADALINE);
        assertEquals(ADALINE, nn.getNetworkType());
    }

    @Test
    public void testSetNetworkType() {
        testGetNetworkType();
    }

    @Test
    public void testGetInputNeurons() {
        List<Neuron> input = TestUtil.generateNeuronList(KHAMSA);
        nn.setInputNeurons(input);
        List<Neuron> result = nn.getInputNeurons();
        assertEquals(input.size(), result.size());
    }

    @Test
    public void testSetInputNeurons() {
        testGetInputNeurons();
    }

    @Test
    public void testGetOutputNeurons() {
        List<Neuron> output = TestUtil.generateNeuronList(KHAMSA);
        nn.setOutputNeurons(output);
        List<Neuron> result = nn.getOutputNeurons();
        assertEquals(output.size(), result.size());
    }

    @Test
    public void testSetOutputNeurons() {
        testGetOutputNeurons();
    }

    @Test
    public void testGetLearningRule() {
        LearningRule rule = new BackPropagation();
        nn.setLearningRule(rule);
        assertEquals(rule, nn.getLearningRule());
    }

    @Test
    public void testSetLearningRule() {
        testGetLearningRule();
    }

    @Test
    public void testGetLearningThread() {
        TrainingSet<TrainingElement> set = new TrainingSet<TrainingElement>(
                KHAMSA, KHAMSA);
        set = TestUtil.initSet(set);
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.learnInNewThread(set);
        Thread lt = nn.getLearningThread();
        assertNotNull(lt);
        nn.stopLearning();
    }

    @Test
    public void testNotifyChange() {
        ANNHandler handler = new ANNHandler();
        nn.addObserver(handler);
        nn.notifyChange();
        assertEquals(EVENT_HAPPENED, handler.getState());
    }

    @Test
    public void testCreateConnection() {
        /* Neurons to be connected to/from */
        Neuron from = new Neuron();
        Neuron to = new Neuron();
        /* List of input neurons */
        List<Neuron> in = new ArrayList<Neuron>();
        in.add(from);
        nn.setInputNeurons(in);
        /* List of output neurons */
        List<Neuron> out = new ArrayList<Neuron>();
        out.add(from);
        nn.setOutputNeurons(out);
        /* Creates connection between in and out neurons */
        nn.createConnection(from, to, EPSILON);
        List<Neuron> neurons = nn.getInputNeurons();
        assertTrue(neurons != null && !neurons.isEmpty());
        for (Neuron n : neurons) {
            List<Connection> cons = n.getOutConnections();
            for (Connection con : cons) {
                assertEquals(from, con.fromNeuron);
                assertEquals(to, con.toNeuron);
            }
        }
        assertTrue(true);
    }

    @Test
    public void testSave() {
        nn.save(PATH_TO_SAVE_NN);
        assertTrue(true);
    }

    @Test
    public void testLoadString() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.save(PATH_TO_SAVE_NN);
        NeuralNetwork nnet = NeuralNetwork.load(PATH_TO_SAVE_NN);
        assertNotNull(nnet);
    }

    @Test
    public void testLoadInputStream() {
        nn = TestUtil.addLayersToNN(KHAMSA, nn);
        nn.save(PATH_TO_SAVE_NN);
        try {
            NeuralNetwork.load(new FileInputStream(new File(PATH_TO_SAVE_NN)));
        } catch (FileNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).severe(e.getMessage());
        }
    }

    @Test
    public void testAddPlugin() {
        PluginBase pb = new PluginBase();
        nn.addPlugin(pb);
        PluginBase pBase = nn.getPlugin(PluginBase.class);
        assertEquals(pb, pBase);
    }

    @Test
    public void testGetPlugin() {
        testAddPlugin();
    }

    @Test
    public void testRemovePlugin() {
        PluginBase pb = new PluginBase();
        nn.addPlugin(pb);
        nn.removePlugin(PluginBase.class);
        PluginBase pBase = nn.getPlugin(PluginBase.class);
        assertNull(pBase);
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.random(KHAMSA);
        nn.setLabel(label);
        assertEquals(label, nn.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testToString(){
        assertNotNull(nn.toString());
    }


    class ANNHandler implements Observer {
        private String state = INIT_STATE;

        public void update(Observable o, Object arg) {
            assertTrue(true);
            setState(EVENT_HAPPENED);
        }

        private void setState(String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }
    }
}
