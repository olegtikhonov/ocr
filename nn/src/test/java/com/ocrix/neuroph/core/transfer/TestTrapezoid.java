/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.LEFT_HIGH;
import static com.ocrix.neuroph.common.NetProperty.LEFT_LOW;
import static com.ocrix.neuroph.common.NetProperty.RIGHT_HIGH;
import static com.ocrix.neuroph.common.NetProperty.RIGHT_LOW;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Trapezoid;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests a {@link Trapezoid} public functionality.
 */
public class TestTrapezoid {
    /* Test's member */
    private Trapezoid trapezoid = null;

    @Before
    public void setUp() throws Exception {
        trapezoid = new Trapezoid();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = trapezoid.getOutput(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testGetOutputMoreLeftHigh() {
        double result = trapezoid.getOutput(2.0d);
        assertTrue(result > NIL);
    }

    @Test
    public void testToString() {
        assertNotNull(trapezoid.toString());
    }

    @Test
    public void testTrapezoid() {
        assertNotNull(trapezoid);
    }

    @Test
    public void testTrapezoidDoubleDoubleDoubleDouble() {
        /* Pseudo random generated values */
        double leftLow = Math.abs(TestUtil.genDouble());
        double leftHigh = Math.abs(TestUtil.genDouble());
        double rightLow = Math.abs(TestUtil.genDouble());
        double rightHigh = Math.abs(TestUtil.genDouble());
        trapezoid = new Trapezoid(leftLow, leftHigh, rightLow, rightHigh);
        /* Validations */
        assertNotNull(trapezoid);
        assertEquals(leftLow, trapezoid.getLeftLow(), EPSILON);
        assertEquals(leftHigh, trapezoid.getLeftHigh(), EPSILON);
        assertEquals(rightLow, trapezoid.getRightLow(), EPSILON);
        assertEquals(rightHigh, trapezoid.getRightHigh(), EPSILON);
    }

    @Test
    public void testTrapezoidProperties() {
        Properties props = new Properties();
        props.setProperty(LEFT_LOW.to(), Math.abs(TestUtil.genDouble()));
        props.setProperty(LEFT_HIGH.to(), Math.abs(TestUtil.genDouble()));
        props.setProperty(RIGHT_LOW.to(), Math.abs(TestUtil.genDouble()));
        props.setProperty(RIGHT_HIGH.to(), Math.abs(TestUtil.genDouble()));

        trapezoid = new Trapezoid(props);
        assertNotNull(trapezoid);
    }

    @Test
    public void testTrapezoidPropertiesNotNumber() {
        Properties props = new Properties();
        props.setProperty(LEFT_LOW.to(), Math.abs(TestUtil.genDouble()) + RandomStringUtils.random(1));
        props.setProperty(LEFT_HIGH.to(), Math.abs(TestUtil.genDouble()) + RandomStringUtils.random(1));
        props.setProperty(RIGHT_LOW.to(), Math.abs(TestUtil.genDouble()) + RandomStringUtils.random(1));
        props.setProperty(RIGHT_HIGH.to(), Math.abs(TestUtil.genDouble()) + RandomStringUtils.random(1));

        trapezoid = new Trapezoid(props);
        assertNotNull(trapezoid);
    }

    @Test
    public void testTrapezoidPropertiesNull() {
        trapezoid = new Trapezoid(null);
        assertNotNull(trapezoid);
    }

    @Test
    public void testSetLeftLow() {
        double leftLow = Math.abs(TestUtil.genDouble());
        trapezoid.setLeftLow(leftLow);
        assertEquals(leftLow, trapezoid.getLeftLow(), EPSILON);
    }

    @Test
    public void testSetLeftHigh() {
        double leftHigh = Math.abs(TestUtil.genDouble());
        trapezoid.setLeftHigh(leftHigh);
        assertEquals(leftHigh, trapezoid.getLeftHigh(), EPSILON);
    }

    @Test
    public void testSetRightLow() {
        double rightLow = Math.abs(TestUtil.genDouble());
        trapezoid.setRightLow(rightLow);
        assertEquals(rightLow, trapezoid.getRightLow(), EPSILON);
    }

    @Test
    public void testSetRightHigh() {
        double rightHigh = Math.abs(TestUtil.genDouble());
        trapezoid.setRightHigh(rightHigh);
        assertEquals(rightHigh, trapezoid.getRightHigh(), EPSILON);
    }

    @Test
    public void testGetLeftLow() {
        testSetLeftLow();
    }

    @Test
    public void testGetLeftHigh() {
        testSetLeftHigh();
    }

    @Test
    public void testGetRightLow() {
        testSetRightLow();
    }

    @Test
    public void testGetRightHigh() {
        testSetRightHigh();
    }

}
