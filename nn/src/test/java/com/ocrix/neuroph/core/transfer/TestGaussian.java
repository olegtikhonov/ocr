/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.SIGMA;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.GLYPH;
import static com.ocrix.neuroph.common.ParamsTest.VP_SIGMA;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests a {@link Gaussian} public functionality.
 */
public class TestGaussian {
    /* Test's member */
    private Gaussian gaussian = null;

    @Before
    public void setUp() throws Exception {
        gaussian = new Gaussian();
    }

    @Test
    public void testGetOutput() {
        assertNotNull(gaussian);
        assertTrue(gaussian.getOutput(WEIGHT) >= 0);
    }

    @Test
    public void testGetDerivative() {
        double result = gaussian.getDerivative(GLYPH);
        assertTrue(Math.abs(result) >= 0);
    }

    @Test
    public void testGaussian() {
        assertNotNull(gaussian);
    }

    @Test
    public void testGaussianProperties() {
        Properties props = new Properties();
        props.setProperty(SIGMA.to(), WEIGHT);
        gaussian = new Gaussian(props);
        assertNotNull(gaussian);
    }

    @Test
    public void testGaussianNumberFormatExceptionProperties() {
        Properties props = new Properties();
        props.setProperty(SIGMA.to(), "WEIGHT@3");
        gaussian = new Gaussian(props);
        assertNotNull(gaussian);
    }


    @Test
    public void testGetSigma() {
        double result = gaussian.getSigma();
        assertEquals(VP_SIGMA, result, EPSILON);
    }

    @Test
    public void testSetSigma() {
        gaussian.setSigma(WEIGHT);
        double result = gaussian.getSigma();
        assertEquals(WEIGHT, result, EPSILON);
    }

    @Test
    public void testToString(){
        assertNotNull(gaussian.toString());
    }

    @After
    public void tearDown() throws Exception {
    }

}
