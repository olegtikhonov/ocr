
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests a {@link SupervisedTrainingElement} public functionality.
 */
public class TestSupervisedTrainingElement {

    private SupervisedTrainingElement ste = null;

    @Before
    public void setUp() throws Exception {
        List<Double> input = TestUtil.genDoubleList(ParamsTest.DECIMAL);
        List<Double> desiredOutput = TestUtil.genDoubleList(ParamsTest.DECIMAL);
        ste = new SupervisedTrainingElement(input, desiredOutput);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testIsSupervised() {
        boolean supervised = ste.isSupervised();
        assertTrue(supervised);
    }

    @Test
    public void testGetIdealArray() {
        double[] result = ste.getIdealArray();
        assertNotNull(result);
    }

    @Test
    public void testSetIdealArray() {
        double[] data = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        ste.setIdealArray(data);
        double[] result = ste.getIdealArray();
        assertEquals(data, result);
    }

    @Test
    public void testSupervisedTrainingElementArrayListOfDoubleArrayListOfDouble() {
        List<Double> input = TestUtil.genDoubleList(ParamsTest.DECIMAL);
        List<Double> desiredOutput = TestUtil.genDoubleList(ParamsTest.DECIMAL);
        ste = new SupervisedTrainingElement(input, desiredOutput);
        assertNotNull(ste);
    }

    @Test
    public void testSupervisedTrainingElementStringString() {
        /* Generated arrays for the converting to the String */
        double[] input = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        double[] desiredOutput = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        /* Converted double arrays to the String */
        String strInput = Arrays.toString(input);
        String strDesiredOutput = Arrays.toString(desiredOutput);
        /* Creating ste from String as array */
        ste = new SupervisedTrainingElement(strInput, strDesiredOutput);
        /* Results */
        double[] inputResult = ste.getInputArray();
        double[] desOutResult = ste.getDesiredOutput();
        /* Results testing */
        assertTrue(assertDoubleArray(input, inputResult));
        assertTrue(assertDoubleArray(desiredOutput, desOutResult));
    }

    @Test
    public void testSupervisedTrainingElementDoubleArrayDoubleArray() {
        double[] input = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        double[] desiredOutput = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        ste = new SupervisedTrainingElement(input, desiredOutput);
        assertNotNull(ste);
        assertTrue(assertDoubleArray(input, ste.getInput()));
        assertTrue(assertDoubleArray(desiredOutput, ste.getDesiredOutput()));
    }

    @Test
    public void testGetDesiredOutput() {
        double[] desiredOutput = TestUtil.genDoubleVector(ParamsTest.DECIMAL);
        ste.setDesiredOutput(desiredOutput);
        assertTrue(assertDoubleArray(desiredOutput, ste.getDesiredOutput()));
    }

    @Test
    public void testSetDesiredOutput() {
        testGetDesiredOutput();
    }

    public void testSetMaxIterations(){

    }

    private boolean assertDoubleArray(double[] original, double[] toBeTested) {
        int sum = 0;
        List<Double> lst = convertTo(toBeTested);
        Collections.sort(lst);
        for (int i = 0; i < original.length; i++) {
            int key = Collections.binarySearch(lst, original[i]);
            if (key != -1)
                sum++;
        }
        return (sum == original.length);
    }

    private List<Double> convertTo(double[] list) {
        List<Double> converted = new ArrayList<Double>();
        for (Double elem : list) {
            converted.add(elem);
        }
        return converted;
    }

}
