/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.exceptions;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.exceptions.VectorSizeMismatchException;

/**
 * Tests a {@link VectorSizeMismatchException} public functionality.
 */
public class TestVectorSizeMismatchException {

    /* Test's member */
    private VectorSizeMismatchException vsme = null;

    @Before
    public void setUp() throws Exception {
        vsme = new VectorSizeMismatchException();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testVectorSizeMismatchException() {
        assertNotNull(vsme);
    }

    @Test
    public void testVectorSizeMismatchExceptionString() {
        String msg = RandomStringUtils.randomAlphabetic(KHAMSA);
        vsme = new VectorSizeMismatchException(msg);
        assertEquals(msg, vsme.getMessage());
    }

    @Test
    public void testVectorSizeMismatchExceptionStringThrowable() {
        String msg = RandomStringUtils.randomAlphabetic(KHAMSA);
        VectorSizeMismatchException thrwbl = new VectorSizeMismatchException();
        vsme = new VectorSizeMismatchException(msg, thrwbl);
        assertEquals(msg, vsme.getMessage());
        assertEquals(thrwbl, vsme.getCause());
    }

    @Test
    public void testVectorSizeMismatchExceptionThrowable() {
        VectorSizeMismatchException thrwbl = new VectorSizeMismatchException();
        vsme = new VectorSizeMismatchException(thrwbl);
        assertEquals(thrwbl, vsme.getCause());
    }

}
