/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.exceptions;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.exceptions.NeurophException;

/**
 * Tests a {@link NeurophException} public functionality.
 */
public class TestNeurophException {

    private NeurophException ne = null;

    @Before
    public void setUp() throws Exception {
        ne = new NeurophException();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNeurophException() {
        assertNotNull(ne);
    }

    @Test
    public void testNeurophExceptionString() {
        String msg = RandomStringUtils.randomAlphabetic(KHAMSA);
        ne = new NeurophException(msg);
        assertEquals(msg, ne.getMessage());
    }

    @Test
    public void testNeurophExceptionThrowable() {
        NeurophException thrwbl = new NeurophException();
        ne = new NeurophException(thrwbl);
        assertEquals(thrwbl, ne.getCause());
    }

    @Test
    public void testNeurophExceptionStringThrowable() {
        String msg = RandomStringUtils.randomAlphabetic(KHAMSA);
        NeurophException thrwbl = new NeurophException();
        ne = new NeurophException(msg, thrwbl);
        assertEquals(thrwbl, ne.getCause());
        assertEquals(msg, ne.getMessage());
    }

}
