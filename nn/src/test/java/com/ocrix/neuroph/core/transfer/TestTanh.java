/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Tanh;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests a {@link Tanh} public functionality.
 */
public class TestTanh {

    /* Test's member */
    private Tanh tanh = null;

    @Before
    public void setUp() throws Exception {
        tanh = new Tanh();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = tanh.getOutput(WEIGHT);
        assertTrue(result > NIL);

        double more100 = 101.0d;
        result = tanh.getOutput(more100);
        assertEquals(1.0d, result, EPSILON);

        //-100
        double less100 = -101.0d;
        result = tanh.getOutput(less100);
        assertEquals(-1.0d, result, EPSILON);

    }

    @Test
    public void testGetDerivative() {
        double result = tanh.getDerivative(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testToString() {
        assertNotNull(tanh.toString());
    }

    @Test
    public void testTanh() {
        assertNotNull(tanh);
    }

    @Test
    public void testTanhDouble() {
        tanh = new Tanh(EPSILON);
        assertEquals(EPSILON, tanh.getSlope(), EPSILON);
    }

    @Test
    public void testTanhProperties() {
        Properties properties = new Properties();
        properties.setProperty(SLOPE.to(), WEIGHT);
        tanh = new Tanh(properties);
        assertEquals(WEIGHT, tanh.getSlope(), EPSILON);
    }

    @Test
    public void testTanhNullProperties() {
        tanh = new Tanh(null);
        assertTrue(true);
    }

    @Test
    public void testTanhNumberFormatExceptionProperties() {
        Properties properties = new Properties();
        properties.setProperty(SLOPE.to(), "WEIGHT~0");
        tanh = new Tanh(properties);
        assertTrue(true);
    }

    @Test
    public void testGetSlope() {
        tanh.setSlope(WEIGHT);
        assertEquals(WEIGHT, tanh.getSlope(), EPSILON);
    }

    @Test
    public void testSetSlope() {
        testGetSlope();
    }
}
