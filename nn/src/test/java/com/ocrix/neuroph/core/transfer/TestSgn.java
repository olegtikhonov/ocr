package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.transfer.Sgn;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests {@link Sgn} public functionality.
 */
public class TestSgn {
    /* Test's member */
    private Sgn sgn = null;

    @Before
    public void setUp() throws Exception {
        sgn = new Sgn();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = sgn.getOutput(WEIGHT);
        assertTrue(result > ParamsTest.NIL);
    }

    @Test
    public void testGetOutputMoreThanZero() {
        double result = sgn.getOutput(1 + WEIGHT);
        assertTrue(result > ParamsTest.NIL);
    }

    @Test
    public void testGetOutputLessThanZero() {
        double result = sgn.getOutput(-WEIGHT);
        assertTrue(result < ParamsTest.NIL);
    }

    @Test
    public void testGetProperties() {
        Properties props = sgn.getProperties();
        assertNotNull(props);
    }

    @Test
    public void testToString(){
        assertNotNull(sgn.toString());
    }
}
