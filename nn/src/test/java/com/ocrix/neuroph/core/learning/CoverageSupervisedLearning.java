/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import com.ocrix.neuroph.nn.TestUtil;

/**
 * <p>
 * For test purposes only.
 * </p>
 */
public class CoverageSupervisedLearning extends SupervisedLearning {
    private static final long serialVersionUID = 4428494134453805502L;
    final double ERROR_DELTA = 0.0001d;

    @Override
    protected void updateNetworkWeights(double[] outputError) {
        if (outputError != null) {
            int index = 0;
            for (Double d : outputError) {
                outputError[index++] = d * ERROR_DELTA;
            }
        }
    }

    @Override
    protected void doBatchWeightsUpdate() {
        super.doBatchWeightsUpdate();
    }

    @Override
    protected boolean hasReachedStopCondition() {
        return super.hasReachedStopCondition();
    }

    @Override
    public double getPreviousEpochError() {
        return super.getPreviousEpochError();
    }
    
    
    @Override
    public double getMinErrorChange() {
        return super.getMinErrorChange();
    }
    
    @Override
    public int getMinErrorChangeIterationsCount() {
        return super.getMinErrorChangeIterationsCount();
    }

    
    public void setPreviousEpochError(){
        super.previousEpochError = TestUtil.genDouble();
    }
}
