/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.Properties;

/**
 * Tests a {@link Sigmoid} public functionality.
 */
public class TestSigmoid {
    /* Test's member */
    private Sigmoid sigmoid = null;

    @Before
    public void setUp() throws Exception {
        sigmoid = new Sigmoid();
    }

    @Test
    public void testGetOutput() {
        double result = sigmoid.getOutput(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testGetOutputMoreThan100() {
        double result = sigmoid.getOutput(101.0d);
        assertTrue(result > NIL);
    }

    @Test
    public void testGetOutputLessThanMinus100() {
        double result = sigmoid.getOutput(-101.0d);
        assertTrue(result >= NIL);
    }

    @Test
    public void testGetDerivative() {
        double result = sigmoid.getDerivative(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testSigmoid() {
        assertNotNull(sigmoid);
    }

    @Test
    public void testSigmoidDouble() {
        sigmoid = new Sigmoid(WEIGHT);
        assertNotNull(sigmoid);
    }

    @Test
    public void testSigmoidProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), 0.567d);
        sigmoid = new Sigmoid(props);
        assertNotNull(sigmoid);
        assertEquals(0.567d, sigmoid.getSlope(), EPSILON);
    }

    @Test
    public void testSigmoidNumberFormatExceptionProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), 0.567d + "!@#$%^&*()_+");
        sigmoid = new Sigmoid(props);
        assertNotNull(sigmoid);
    }

    @Test
    public void testSigmoidNullProperties() {
        sigmoid = new Sigmoid(null);
        assertNotNull(sigmoid);
    }

    @Test
    public void testGetSlope() {
        sigmoid.setSlope(WEIGHT);
        assertEquals(WEIGHT, sigmoid.getSlope(), EPSILON);
    }

    @Test
    public void testSetSlope() {
        testGetSlope();
    }

    @Test
    public void testToString() {
        assertNotNull(sigmoid.toString());
    }
}
