package com.ocrix.neuroph.core.input;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.input.And;
import com.ocrix.neuroph.nn.TestUtil;

public class TestAnd {

    private And and = null;

    @Before
    public void setUpBeforeClass() throws Exception {
        and = new And();
    }

    @After
    public void tearDownAfterClass() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double[] gen = TestUtil.genDoubleVector(DECIMAL);
        assertEquals(1.0d, and.getOutput(gen), EPSILON);
    }

}
