/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.MAX;
import static com.ocrix.neuroph.common.ParamsTest.MIN;
import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.util.Properties;

/**
 * Tests the {@link Step} public functionality.
 */
public class TestStep {
    /* Test's member */
    private Step step = null;

    @Before
    public void setUp() throws Exception {
        step = new Step();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        assertTrue(step.getOutput(WEIGHT) > NIL);
    }

    @Test
    public void testStep() {
        assertNotNull(step);
    }

    @Test
    public void testStepProperties() {
        Properties props = new Properties();
        props.setProperty(Y_HIGH.to(), MAX);
        props.setProperty(Y_LOW.to(), MIN);
        step = new Step(props);
        assertNotNull(step);
    }

    @Test
    public void testStepNumberFormatExceptionProperties() {
        Properties props = new Properties();
        props.setProperty(Y_HIGH.to(), MAX + "~!@#$%^&*()_+");
        props.setProperty(Y_LOW.to(), MIN);
        step = new Step(props);
        assertNotNull(step);
    }

    @Test
    public void testStepNullProperties() {
        step = new Step(null);
        assertNotNull(step);
    }

    @Test
    public void testGetYHigh() {
        step.setYHigh(WEIGHT);
        assertEquals(WEIGHT, step.getYHigh(), EPSILON);
    }

    @Test
    public void testSetYHigh() {
        testGetYHigh();
    }

    @Test
    public void testGetYLow() {
        step.setYLow(WEIGHT);
        assertEquals(WEIGHT, step.getYLow(), EPSILON);
    }

    @Test
    public void testSetYLow() {
        testGetYLow();
    }

    @Test
    public void testGetProperties() {
        Properties prop = step.getProperties();
        assertNotNull(prop);
    }
}
