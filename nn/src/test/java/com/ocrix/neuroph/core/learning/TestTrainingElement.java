
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests a {@link TrainingElement} public functionality.
 */
public class TestTrainingElement {
    /* Test's member */
    private TrainingElement te = null;

    @Before
    public void setUp() throws Exception {
        double[] input = TestUtil.genDoubleVector(DECIMAL);
        te = new TrainingElement(input);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testTrainingElementListOfDouble() {
        double[] inputVector = TestUtil.genDoubleVector(DECIMAL);
        te = new TrainingElement(inputVector);
        assertNotNull(te);
    }

    @Test
    public void testTrainingElementString() {
        double[] inputVector = TestUtil.genDoubleVector(DECIMAL);
        /* Converted double arrays to the String */
        String strInput = Arrays.toString(inputVector);
        te = new TrainingElement(strInput);
        assertNotNull(te);
    }

    @Test
    public void testTrainingElementDoubleArray() {
        double[] inputVector = TestUtil.genDoubleVector(DECIMAL);
        te = new TrainingElement(inputVector);
        assertNotNull(te);
    }

    @Test
    public void testGetInput() {
        double[] input = TestUtil.genDoubleVector(DECIMAL);
        te.setInput(input);
        double[] result = te.getInput();
        TestUtil.assertDoubleArray(input, result);

    }

    @Test
    public void testSetInput() {
        testGetInput();
    }

    @Test
    public void testGetLabel() {
        String label = RandomStringUtils.randomAlphabetic(DECIMAL);
        te.setLabel(label);
        assertEquals(label, te.getLabel());
    }

    @Test
    public void testSetLabel() {
        testGetLabel();
    }

    @Test
    public void testIsSupervised() {
        assertFalse(te.isSupervised());
    }

    @Test
    public void testGetIdealArray() {
        double[] data = TestUtil.genDoubleVector(DECIMAL);
        te.setIdealArray(data);
        /* Tests the results */
        double[] result = te.getIdealArray();
        TestUtil.assertDoubleArray(data, result);
    }

    @Test
    public void testGetInputArray() {
        double[] data = TestUtil.genDoubleVector(DECIMAL);
        te.setInputArray(data);
        /* Tests the results */
        double[] result = te.getIdealArray();
        TestUtil.assertDoubleArray(data, result);
    }

    @Test
    public void testSetIdealArray() {
        testGetIdealArray();
    }

    @Test
    public void testSetInputArray() {
        testGetInputArray();
    }
}
