/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.NetProperty.X_HIGH;
import static com.ocrix.neuroph.common.NetProperty.X_LOW;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.transfer.Ramp;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.Properties;

public class TestRamp {

    private Ramp ramp = null;

    @Before
    public void setUp() throws Exception {
        ramp = new Ramp();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = ramp.getOutput(WEIGHT);
        assertTrue(result >= ParamsTest.NIL);
    }

    @Test
    public void testGetOutputXLow() {
        double xLow = -0.008d;
        double result = ramp.getOutput(xLow);
        assertTrue(result >= ParamsTest.NIL);
    }

    @Test
    public void testGetOutputXHigh() {
        double xHigh = 1.008d;
        double result = ramp.getOutput(xHigh);
        assertTrue(result >= ParamsTest.NIL);
    }

    @Test
    public void testRamp() {
        assertNotNull(ramp);
    }

    @Test
    public void testRampDoubleDoubleDoubleDoubleDouble() {
        double slope = TestUtil.genDouble();
        double xLow = TestUtil.genDouble();
        double xHigh = TestUtil.genDouble();
        double yLow = TestUtil.genDouble();
        double yHigh = TestUtil.genDouble();

        ramp = new Ramp(slope, xLow, xHigh, yLow, yHigh);
        assertNotNull(ramp);
    }

    @Test
    public void testRampProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), TestUtil.genDouble());
        props.setProperty(Y_HIGH.to(), TestUtil.genDouble());
        props.setProperty(Y_LOW.to(), TestUtil.genDouble());
        props.setProperty(X_HIGH.to(), TestUtil.genDouble());
        props.setProperty(X_LOW.to(), TestUtil.genDouble());

        ramp = new Ramp(props);
        assertNotNull(ramp);
    }

    @Test
    public void testRampNullProperties() {
        ramp = new Ramp(null);
        assertNotNull(ramp);
    }

    @Test
    public void testRampNumberFormatExceptionProperties() {
        Properties props = new Properties();
        props.setProperty(SLOPE.to(), TestUtil.genDouble() + "!@#$%^&*()-=+~");
        props.setProperty(Y_HIGH.to(), TestUtil.genDouble());
        props.setProperty(Y_LOW.to(), TestUtil.genDouble());
        props.setProperty(X_HIGH.to(), TestUtil.genDouble());
        props.setProperty(X_LOW.to(), TestUtil.genDouble());

        ramp = new Ramp(props);
        assertNotNull(ramp);
    }

    @Test
    public void testGetXLow() {
        ramp.setXLow(WEIGHT);
        assertEquals(WEIGHT, ramp.getXLow(), EPSILON);
    }

    @Test
    public void testSetXLow() {
        testGetXLow();
    }

    @Test
    public void testGetXHigh() {
        ramp.setXHigh(WEIGHT);
        assertEquals(WEIGHT, ramp.getXHigh(), EPSILON);
    }

    @Test
    public void testSetXHigh() {
        testGetXHigh();
    }

    @Test
    public void testGetYLow() {
        ramp.setYLow(WEIGHT);
        assertEquals(WEIGHT, ramp.getYLow(), EPSILON);
    }

    @Test
    public void testSetYLow() {
        testGetYLow();
    }

    @Test
    public void testGetYHigh() {
        ramp.setYHigh(WEIGHT);
        assertEquals(WEIGHT, ramp.getYHigh(), EPSILON);
    }

    @Test
    public void testSetYHigh() {
        testGetYHigh();
    }

    @Test
    public void testToString() {
        assertNotNull(ramp.toString());
    }
}
