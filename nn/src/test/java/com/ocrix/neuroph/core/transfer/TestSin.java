/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.ParamsTest.NIL;
import static com.ocrix.neuroph.common.ParamsTest.WEIGHT;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.transfer.Sin;

public class TestSin {

    private Sin sin = null;

    @Before
    public void setUp() throws Exception {
        sin = new Sin();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetOutput() {
        double result = sin.getOutput(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testGetDerivative() {
        double result = sin.getDerivative(WEIGHT);
        assertTrue(result > NIL);
    }

    @Test
    public void testToString() {
        assertNotNull(sin.toString());
    }
}
