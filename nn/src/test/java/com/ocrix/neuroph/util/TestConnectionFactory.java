/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.DelayedConnection;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.NeuronProperties;

public class TestConnectionFactory {
    /* Test's members */
    private Neuron from = null;
    private Neuron to = null;
    private Layer fromLayer = null;
    private Layer toLayer = null;

    @Before
    public void setUp() throws Exception {
        from = new Neuron();
        to = new Neuron();
        fromLayer = new Layer(KHAMSA, new NeuronProperties());
        toLayer = new Layer(KHAMSA, new NeuronProperties());
    }


    @Test
    public void testCreateConnectionNeuronNeuron() {
        assertTrue(to.getInputConnections().isEmpty());
        /* Connects neuron A to neuron B as input connection */
        ConnectionFactory.createConnection(from, to);
        assertFalse(to.getInputConnections().isEmpty());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNeuronNull() {
        ConnectionFactory.createConnection(from, null);
        assertTrue(true);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNullNeuron() {
        ConnectionFactory.createConnection(null, to);
        assertTrue(true);
    }

    @Test
    public void testCreateConnectionNeuronNeuronDouble() {
        double weight = TestUtil.genDouble();
        assertTrue(to.getInputConnections().isEmpty());
        ConnectionFactory.createConnection(from, to, weight);
        assertFalse(to.getInputConnections().isEmpty());
        assertEquals(weight, to.getConnectionFrom(to).getWeight().value, ParamsTest.EPSILON);

    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNullNeuronDouble() {
        ConnectionFactory.createConnection(null, to, TestUtil.genDouble());
        assertTrue(true);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNeuronNullDouble() {
        ConnectionFactory.createConnection(from, null, TestUtil.genDouble());
        assertTrue(true);
    }

    @Test
    public void testCreateConnectionNeuronNeuronDoubleInt() {
        double weight = TestUtil.genDouble();
        int delay = TestUtil.genInteger();
        ConnectionFactory.createConnection(from, to, weight, delay);
        DelayedConnection dc = (DelayedConnection) to.getConnectionFrom(to);
        assertEquals(delay, dc.getDelay());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNullNeuronDoubleInt() {
        ConnectionFactory.createConnection(null, to, TestUtil.genDouble(), TestUtil.genInteger());
        assertTrue(true);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNeuronNullDoubleInt() {
        ConnectionFactory.createConnection(from, null, TestUtil.genDouble(), TestUtil.genInteger());
        assertTrue(true);
    }

    @Test
    public void testCreateConnectionNeuronNeuronWeight() {
        Weight weight = new Weight(TestUtil.genDouble());
        ConnectionFactory.createConnection(from, to, weight);
        assertNotNull(to.getConnectionFrom(to).getWeight());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNullNeuronWeight() {
        ConnectionFactory.createConnection(null, to, new Weight(TestUtil.genDouble()));
        assertTrue(true);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNeuronNullWeight() {
        ConnectionFactory.createConnection(from, null, new Weight(TestUtil.genDouble()));
        assertTrue(true);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testCreateConnectionNeuronNeuronWeightNull() {
        ConnectionFactory.createConnection(from, to , null);
        assertTrue(true);
    }

    @Test
    public void testFullConnectLayerLayer() {
        ConnectionFactory.fullConnect(fromLayer, toLayer);

        for(Neuron neuron : toLayer.getNeurons()){
            assertNotNull(neuron.getConnectionFrom(neuron).getToNeuron());
        }
    }

    @Test (expected=IllegalArgumentException.class)
    public void testFullConnectLayerNullLayer() {
        ConnectionFactory.fullConnect(null, toLayer);
        assertTrue(true);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testFullConnectLayerLayerNull() {
        ConnectionFactory.fullConnect(fromLayer, null);
        assertTrue(true);
    }

    @Test
    public void testFullConnectLayerLayerBooleanTrueBaseNeuronInside() {
        /* Case: layer contains Base neuron, so they won't be connected, even though a flag indicates true */
        ConnectionFactory.fullConnect(fromLayer, toLayer, true);
        for(Neuron neuron : toLayer.getNeurons()){
            assertNull(neuron.getConnectionFrom(neuron));
        }
    }

    @Test
    public void testFullConnectLayerLayerBooleanTrueBiasNeuronInside() {
        NeuronProperties np = new NeuronProperties();
        /* Here is important to create Bias neuron */
        np.setProperty(NEURON_TYPE.to(), BiasNeuron.class);
        Layer fromLayer = new Layer(KHAMSA, np);
        Layer toLayer = new Layer(KHAMSA, np);

        ConnectionFactory.fullConnect(fromLayer, toLayer, true);

        for(Neuron neuron : toLayer.getNeurons()){
            assertNotNull(neuron.getConnectionFrom(neuron).getToNeuron());
        }
    }

    @Test
    public void testFullConnectLayerLayerBooleanFalseBiasNeuronInside() {
        NeuronProperties np = new NeuronProperties();
        /* Here is important to create Bias neuron */
        np.setProperty(NEURON_TYPE.to(), BiasNeuron.class);
        Layer fromLayer = new Layer(KHAMSA, np);
        Layer toLayer = new Layer(KHAMSA, np);

        ConnectionFactory.fullConnect(fromLayer, toLayer, false);

        for(Neuron neuron : toLayer.getNeurons()){
            assertNull(neuron.getConnectionFrom(neuron));
        }
    }

    @Test
    public void testFullConnectLayerLayerDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.fullConnect(fromLayer, toLayer, weight);
        for(Neuron neuron : toLayer.getNeurons()){
            assertEquals(weight, neuron.getConnectionFrom(neuron).getWeight().value, EPSILON);
        }
    }

    @Test
    public void testFullConnectLayer() {
        ConnectionFactory.fullConnect(fromLayer);
        for(Neuron neuron : fromLayer.getNeurons()){
            assertNotNull(neuron.getConnectionFrom(neuron).getToNeuron());
        }
    }

    @Test (expected=IllegalArgumentException.class)
    public void testFullConnectLayerNull() {
        ConnectionFactory.fullConnect(null);
        assertTrue(true);
    }

    @Test
    public void testFullConnectLayerDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.fullConnect(fromLayer, weight);
        for(Neuron neuron : fromLayer.getNeurons()){
            assertEquals(weight, neuron.getConnectionFrom(neuron).getWeight().value, EPSILON);
        }
    }

    @Test (expected=IllegalArgumentException.class)
    public void testFullConnectLayerNullDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.fullConnect(null, weight);
        assertTrue(true);
    }

    @Test
    public void testFullConnectLayerDoubleInt() {
        double weight = TestUtil.genDouble();
        int delay = TestUtil.genInteger();
        ConnectionFactory.fullConnect(fromLayer, weight, delay);

        for(Neuron neuron : fromLayer.getNeurons()){
            DelayedConnection dc = (DelayedConnection) neuron.getConnectionFrom(neuron);
            assertEquals(delay, dc.getDelay());
        }
    }

    @Test
    public void testForwardConnectLayerLayerDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.forwardConnect(fromLayer, toLayer, weight);
        /* Verifications */
        for(Neuron neuron : toLayer.getNeurons()){
            assertEquals(weight, neuron.getConnectionFrom(neuron).getWeight().value, EPSILON);
        }
    }

    @Test (expected=IllegalArgumentException.class)
    public void testForwardConnectLayerNullLayerDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.forwardConnect(null, toLayer, weight);
        assertTrue(true);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testForwardConnectLayerLayerNullDouble() {
        double weight = TestUtil.genDouble();
        ConnectionFactory.forwardConnect(fromLayer, null, weight);
        assertTrue(true);
    }

    @Test
    public void testForwardConnectLayerLayer() {
        ConnectionFactory.forwardConnect(fromLayer, toLayer);
        /* Verifications */
        for(Neuron neuron : toLayer.getNeurons()){
            assertNotNull(neuron.getConnectionFrom(neuron));
        }
    }

    @Test (expected=IllegalArgumentException.class)
    public void testForwardConnectLayerNullLayer() {
        ConnectionFactory.forwardConnect(null, toLayer);
        assertTrue(true);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testForwardConnectLayerLayerNull() {
        ConnectionFactory.forwardConnect(fromLayer, null);
        assertTrue(true);
    }

    @Test
    public void testPrivateCitor(){
        try {
            Constructor<ConnectionFactory> constructor = ConnectionFactory.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            ConnectionFactory instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }


    @After
    public void tearDown() throws Exception {
    }
}
