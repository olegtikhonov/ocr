/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static org.junit.Assert.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JList.DropLocation;

import org.junit.Test;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.comp.DelayedNeuron;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;
import com.ocrix.neuroph.util.NeuronFactory;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * Tests {@link NeuronFactory} functionality.
 */
public class TestNeuronFactory {

    @Test
    public void testCreateNeuron() {
        Neuron neuron = NeuronFactory.createNeuron(new NeuronProperties());
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronBiasNeuron() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), BiasNeuron.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronCompetitiveNeuron() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), CompetitiveNeuron.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronDelayedNeuron() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), DelayedNeuron.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronInputOutputNeuron() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), InputOutputNeuron.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronThresholdNeuron() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), ThresholdNeuron.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertNotNull(neuron);
    }
    
    @Test
    public void testCreateNeuronNoSuchMethodException(){
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), DropLocation.class);
        Neuron neuron = NeuronFactory.createNeuron(np);
        assertTrue(neuron == null);
    }
    
    @Test
    public void testCreateNeuronInstantiationException() {
        NeuronProperties np = new NeuronProperties();
        np.setProperty(NEURON_TYPE.to(), MineNeuron.class);

        Neuron neuron = NeuronFactory.createNeuron(np);
        assertTrue(neuron ==  null);
    }

    
    @Test
    public void testPrivetCitor(){
        try {
            Constructor<NeuronFactory> constructor = NeuronFactory.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            NeuronFactory instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }
}
