/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.io.FileInputAdapter;
import com.ocrix.neuroph.util.io.FileOutputAdapter;
import com.ocrix.neuroph.util.io.IOHelper;


/**
 * Tests {@link IOHelper} functionality.
 */
public class TestIOHelper {

    private final static String path = "../nn/src/test/resources/reader_file.txt";

    @Test
    public void testProcess() throws IOException {
        /* Creates a 'default' ANN */
        NeuralNetwork neuralNet = TestUtil.getDefaultNN();
        /* Creates an input adaptor */
        FileInputAdapter in = new FileInputAdapter(new File(path));
        /* Creates an output adapter */
        FileOutputAdapter out = new FileOutputAdapter(new File(ParamsTest.TEST_OUTPUT_FILE));
        /* Tests the method itself */
        IOHelper.process(neuralNet, in, out);
        /* Verification point */
        assertTrue(true);
    }

    //process
    @Test
    public void testProcessFileInputAdapter() throws IOException {
        /* Creates a 'default' ANN */
        NeuralNetwork neuralNet = TestUtil.getDefaultNN();
        /* Creates an input adaptor */
        FileInputAdapter in = new FileInputAdapter(new BufferedReader(new FileReader(new File(ParamsTest.TEST_FILE))));
        /* Creates an output adapter */
        FileOutputAdapter out = new FileOutputAdapter(new File(ParamsTest.TEST_OUTPUT_FILE));
        /* Tests the method itself */
        IOHelper.process(neuralNet, in, out);
        /* Verification point */
        assertTrue(true);
        
        in.close();
        out.close();
    }
    
    
    @Test
    public void testPrivateConstructor() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        try {
            @SuppressWarnings("rawtypes")
            Class[] cl = null;
            @SuppressWarnings("rawtypes")
            Constructor constr = IOHelper.class.getDeclaredConstructor(cl);
            constr.setAccessible(true);
            Object[] ob = null;
            IOHelper helper = (IOHelper) constr.newInstance(ob);
            assertNotNull(helper);
        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        }
    }
    
    
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        
    }
}
