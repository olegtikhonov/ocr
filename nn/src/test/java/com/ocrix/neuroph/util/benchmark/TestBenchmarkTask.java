/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.*;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.benchmark.BenchmarkTask;
import com.ocrix.neuroph.util.benchmark.DefaultBenchmarkTask;



/**
 * Tests {@link BenchmarkTask} functionality.
 */
public class TestBenchmarkTask {
    private DefaultBenchmarkTask task = null;
    
    
    @Test
    public void testBenchmarkTask() {
        assertNotNull(task);
    }

    @Test
    public void testGetName() {
        String name = RandomStringUtils.randomAlphabetic(KHAMSA);
        task.setName(name);
        assertEquals(name, task.getName());
    }

    @Test
    public void testSetName() {
        testGetName();
    }

    @Test
    public void testGetTestIterations() {
        task.setTestIterations(KHAMSA);
        assertEquals(KHAMSA, task.getTestIterations());
    }

    @Test
    public void testSetTestIterations() {
        testGetTestIterations();
    }

    @Test
    public void testGetWarmupIterations() {
        task.setWarmupIterations(KHAMSA);
        assertEquals(KHAMSA, task.getWarmupIterations());
    }

    @Test
    public void testSetWarmupIterations() {
        testGetWarmupIterations();
    }

    @Test
    public void testPrepareTest() {
        task.prepareTest();
        assertTrue(true);
    }

    @Test
    public void testRunTest() {
        task.prepareTest();
        task.runTest();
        assertTrue(true);
    }
    
    
    @Before
    public void setUp() throws Exception {
        task = new DefaultBenchmarkTask(RandomStringUtils.randomAlphabetic(KHAMSA));
    }

    @After
    public void tearDown() throws Exception {
    }
}
