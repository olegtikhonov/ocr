package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.TEST_OUTPUT_FILE;
import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.io.OutputStreamAdapter;

/**
 * Tests {@link OutputStreamAdapter} functionality.
 */
public class TestOutputStreamAdapter {
    private OutputStreamAdapter outputStreamAdapter = null;

    @Before
    public void setUp() throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File(TEST_OUTPUT_FILE)));
        outputStreamAdapter = new OutputStreamAdapter(bw);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testOutputStreamAdapterOutputStream() {
        try {
            outputStreamAdapter = new OutputStreamAdapter(new FileOutputStream(new File(TEST_OUTPUT_FILE)));
            assertNotNull(outputStreamAdapter);
        } catch (FileNotFoundException e) {
        }
    }

    @Test
    public void testOutputStreamAdapterBufferedWriter() {
        assertNotNull(outputStreamAdapter);
    }

    @Test
    public void testWriteOutput() {
        double[] trainVector = TestUtil.genDoubleVector(KHAMSA);
        outputStreamAdapter.writeOutput(trainVector);
        try {
            Scanner scan = new Scanner( new File(TEST_OUTPUT_FILE)); 
            
            if(scan.hasNext()){
                String text = scan.nextLine();
                assertEquals(text, Arrays.toString(trainVector).replace(",", "").replace("[", "").replace("]", ""));
            }
            
            
        } catch (FileNotFoundException e) {
        }
    }

    @Test
    public void testClose() {
        outputStreamAdapter.close();
    }

}
