/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.TrainingSetImport;

/**
 * Tests {@link TrainingSetImport} functionality.
 */
public class TestTrainingSetImport {

    @Test
    public void testImportFromFile() {
        /* Creates a training set */
        TrainingSet<TrainingElement> ts = new TrainingSet<TrainingElement>(DECIMAL, DECIMAL);
        for (int i = 0; i < DECIMAL; i++) {
            TestUtil.addOneElement(ts);
        }

        String filePath = "target/" + RandomStringUtils.randomAlphabetic(DECIMAL);
        ts.setFilePath(filePath);

        ts.saveAsTxt(ts.getFilePath(), "~");

        try {
            TrainingSet<?> set = TrainingSetImport.importFromFile(ts.getFilePath(), DECIMAL, DECIMAL, "~");
            assertNotNull(set);
        } catch (NumberFormatException e) {
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

    
    @Test
    public void testPrivetCitor(){
        try {
            Constructor<TrainingSetImport> constructor = TrainingSetImport.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            TrainingSetImport instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }

}
