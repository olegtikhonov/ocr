package com.ocrix.neuroph.util;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ocrix.neuroph.util.NeuralNetworkType;

/**
 * Mostly used for code coverage.
 */
public class TestNeuralNetworkType {

    @Test
    public void testGetTypeLabel() {
        for (NeuralNetworkType type : NeuralNetworkType.values()) {
            assertNotNull(type.getTypeLabel());
        }
    }
}
