/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.util.benchmark;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static org.junit.Assert.*;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.benchmark.DefaultBenchmarkTask;



/**
 * Tests a {@link DefaultBenchmarkTask} functionality.
 */
public class TestDefaultBenchmarkTask {

    /* Test's member */
    DefaultBenchmarkTask defaultTask = null;
    
    
    @Before
    public void setUp() throws Exception {
        defaultTask = new DefaultBenchmarkTask(RandomStringUtils.randomAscii(DECIMAL));
    }


    @Test
    public void testPrepareTest() {
        defaultTask.prepareTest();
        assertTrue(true);
    }

    @Test
    public void testRunTest() {
        defaultTask.prepareTest();
        defaultTask.runTest();
        assertTrue(true);
    }

    @Test
    public void testDefaultBenchmarkTask() {
        assertNotNull(defaultTask);
    }

    @Test
    public void testToString() {
        String result = defaultTask.toString();
        assertNotNull(result);
    }
    
    
    @After
    public void tearDown() throws Exception {
    }
}
