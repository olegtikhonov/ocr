/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.*;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.benchmark.Benchmark;
import com.ocrix.neuroph.util.benchmark.BenchmarkTask;
import com.ocrix.neuroph.util.benchmark.DefaultBenchmarkTask;


public class TestBenchmark {

    private Benchmark benchmark = null;
    private BenchmarkTask task = null;
    
    @Before
    public void setUp() throws Exception {
        benchmark = new Benchmark();
        task = new DefaultBenchmarkTask(RandomStringUtils.randomAlphabetic(KHAMSA));
        task.prepareTest();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBenchmark() {
        assertNotNull(benchmark);
    }

    @Test
    public void testAddTask() {
        benchmark.logOn();
        int sizeBeforeAddingTask = benchmark.getTasks().size();
        benchmark.addTask(task);
        int sizeAfterAddingTask = benchmark.getTasks().size();
        assertTrue(sizeAfterAddingTask > sizeBeforeAddingTask);
        benchmark.logOff();
    }

    @Test
    public void testRunTask() {
        Benchmark.runTask(task);
        assertTrue(true);
    }

    @Test
    public void testRun() {
        benchmark.addTask(task);
        benchmark.run();
        assertTrue(true);
    }
    
    @Test
    public void testToString() {
        assertNotNull(benchmark.toString());
    }
}
