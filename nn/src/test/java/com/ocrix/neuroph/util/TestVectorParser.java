/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.VectorParser;

/**
 * Tests {@link VectorParser} functionality.
 */
public class TestVectorParser {

    @Test
    public void testParseInteger() {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < KHAMSA; i++){
            sb.append(String.valueOf(TestUtil.genInteger()));
            sb.append(" ");
        }
        
        ArrayList<Integer> result = VectorParser.parseInteger(sb.toString());
        assertNotNull(result);
        assertEquals(KHAMSA, result.size());
    }
    
    @Test
    public void testParseIntegerNotWellFormatted() {
        String strNum = "~" + String.valueOf(TestUtil.genInteger());
        VectorParser.parseInteger(strNum);
        assertTrue(true);
    }

    @Test
    public void testParseDoubleArray() {
        StringBuffer inputStr = new StringBuffer();
        for(int i = 0; i < KHAMSA; i++){
            inputStr.append(String.valueOf(TestUtil.genDouble()));
            inputStr.append(" ");
        } 
        
        double[] result = VectorParser.parseDoubleArray(inputStr.toString());
        assertNotNull(result);
        assertEquals(KHAMSA, result.length);
    }
    
    @Test
    public void testParseDoubleArrayNotWellFormatted() {
        String strNum = "~" + String.valueOf(TestUtil.genDouble());
        VectorParser.parseDoubleArray(strNum);
        assertTrue(true);
    }

    @Test
    public void testToDoubleArray() {
        List<Double> toBeConverted = TestUtil.genDoubleList(KHAMSA);
        double[] result = VectorParser.toDoubleArray(toBeConverted);
        assertNotNull(result);
        assertEquals(KHAMSA, result.length);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testToDoubleArrayNullParam() {
        VectorParser.toDoubleArray(null);
    }


    @Test
    public void testConvertToVector() {
        double[] input = TestUtil.genDoubleVector(KHAMSA);
        List<Double> result = VectorParser.convertToVector(input);
        assertNotNull(result);
        assertEquals(KHAMSA, result.size());
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testConvertToVectorNullParam() {
        VectorParser.convertToVector(null);
    }

    @Test
    public void testConvertToArray() {
        List<Double> input = TestUtil.genDoubleList(KHAMSA);
        double[] result = VectorParser.convertToArray(input);
        assertNotNull(result);
        assertEquals(KHAMSA, result.length);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testConvertToArrayNullParam() {
        VectorParser.convertToArray(null);
    }
    
    
    @Test
    public void testPrivateDefaultCitor(){
        
        try {
            Constructor<VectorParser> constructor = VectorParser.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            VectorParser instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }
}
