package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;

/**
 * Tests {@link JDBCOutputAdapter} functionality.
 */
public class TestJDBCOutputAdapter {
    /* Connection to the DB */
    private Connection dbConnection = null;
    /* JDBC adapter to be tested */
    private JDBCOutputAdapter jdbcOA = null;
    /* A table name */
    private static String tableName = "TEST_JDBC_OUTPUT_ADAPTER";
    /* A column table */
    private static String columnName = "TRAIN_ITEM";

    @Before
    public void setUp() throws Exception {
        try {
            System.setProperty("derby.system.home", "target");
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            dbConnection = DriverManager.getConnection("jdbc:derby:"
                    + "target/testdb" + ";create=true");
        } catch (SQLException ex) {
        } catch (ClassNotFoundException e) {
        }

        jdbcOA = new JDBCOutputAdapter(dbConnection, tableName);
        setTable();
    }

    @After
    public void tearDown() throws Exception {
        if (jdbcOA != null) {
            jdbcOA.close();
        }
    }

    @Test
    public void testJDBCOutputAdapter() {
        assertNotNull(jdbcOA);
    }

    @Test
    public void testClose() {
        jdbcOA.close();
    }

    @Test
    public void testCloseIfNull() {
        jdbcOA = new JDBCOutputAdapter(null, tableName);
        jdbcOA.close();
        assertTrue(true);
    }

    @Test
    public void testWriteOutput() throws SQLException {
        double[] trainVector = TestUtil.genDoubleVector(KHAMSA);

        jdbcOA.setColumnName(columnName);
        jdbcOA.writeOutput(trainVector);

        String query = "select  " + columnName + " from " + tableName;
        Statement sta = dbConnection.createStatement();

        ResultSet result = sta.executeQuery(query);
        String original = parseResultSet(result);

        for (int i = 0; i < trainVector.length; i++) {
            assertTrue(original.contains(String.valueOf(trainVector[i])));
        }

        sta.close();
    }

    @Test
    // (expected=SQLSyntaxErrorException.class)
    public void testWriteOutputNegative() {
        try {
            double[] trainVector = TestUtil.genDoubleVector(KHAMSA);
            jdbcOA.setColumnName(columnName + "<><>");
            jdbcOA.writeOutput(trainVector);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    @Test
    public void testToString() {
        assertTrue(jdbcOA.toString()
                .contains(JDBCOutputAdapter.class.getName()));
    }

    @Test
    public void testGetTable() {
        try {
            Method method = jdbcOA.getClass().getDeclaredMethod("getTableName");
            method.setAccessible(true);
            String table = (String) method.invoke(jdbcOA, new Object[] {});
            assertEquals(tableName, table);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }

    private void setTable() {
        try {
            Statement sta = dbConnection.createStatement();

            /* deletes the table if exists */
            try {
                sta.executeUpdate("DROP TABLE " + tableName);
            } catch (Exception e) {
            }

            /* Creates table */
            sta.executeUpdate("CREATE TABLE " + tableName + " (ID INT, "
                    + columnName + " DOUBLE PRECISION )");
            /* Generates random vector for training */
            double[] trainVector = TestUtil.genDoubleVector(KHAMSA);
            /* Inserts the data */
            for (int i = 0; i < trainVector.length; i++) {
                sta.executeUpdate("INSERT INTO " + tableName + " (ID, "
                        + columnName + " )" + " VALUES(" + i + " , "
                        + trainVector[i] + " )");
            }

            sta.close();
        } catch (Exception e) {
        }
    }

    private String parseResultSet(ResultSet resultSet) {
        StringBuilder builder = new StringBuilder();

        try {
            if (resultSet != null) {
                int columnCount = resultSet.getMetaData().getColumnCount();
                while (resultSet.next()) {
                    for (int i = 0; i < columnCount;) {
                        builder.append(resultSet.getString(i + 1));
                        if (++i < columnCount) {
                            builder.append(",");
                        }
                    }
                    builder.append(" ");
                }
            }
        } catch (Exception e) {
        }

        return builder.toString();
    }
}
