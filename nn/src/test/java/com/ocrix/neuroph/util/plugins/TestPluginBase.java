/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.util.plugins;

import static org.junit.Assert.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.plugins.PluginBase;

public class TestPluginBase {

    private PluginBase pb = null;
    
    @Before
    public void setUp() throws Exception {
        pb = new PluginBase();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testPluginBase() {
        assertNotNull(pb);
    }

    @Test
    public void testPluginBaseString() {
        String name = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        pb = new PluginBase(name);
        assertNotNull(pb);
    }

    @Test
    public void testGetName() {
        String name = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        pb = new PluginBase(name);
        assertEquals(name, pb.getName());
    }

    @Test
    public void testGetParentNetwork() {
        NeuralNetwork nn = TestUtil.getDefaultNN();
        pb.setParentNetwork(nn);
        assertEquals(nn, pb.getParentNetwork());
    }

    @Test
    public void testSetParentNetwork() {
        testGetParentNetwork();
    }

    @Test
    public void testToString() {
        assertTrue(pb.toString().contains(PluginBase.class.getName()));
    }
}
