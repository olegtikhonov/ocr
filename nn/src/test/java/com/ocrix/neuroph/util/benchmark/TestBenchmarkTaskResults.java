/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.EPSILON;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.benchmark.BenchmarkTaskResults;



/**
 * Tests {@link BenchmarkTaskResults} functionality.
 */
public class TestBenchmarkTaskResults {
    /* Test's member */
    private BenchmarkTaskResults taskResults = null;

    @Test
    public void testBenchmarkTaskResults() {
        assertNotNull(taskResults);
    }

    @Test
    public void testGetAverageTestTime() {
        taskResults.addElapsedTime(DECIMAL);
        taskResults.calculateStatistics();
        assertEquals(DECIMAL, taskResults.getAverageTestTime(), EPSILON);
    }

    @Test
    public void testGetElapsedTimes() {
        taskResults.addElapsedTime(DECIMAL);
        assertEquals(DECIMAL, taskResults.getElapsedTimes()[0]);
    }

    @Test
    public void testGetMaxTestTime() {
        taskResults.addElapsedTime(DECIMAL);
        taskResults.calculateStatistics();
        assertEquals(DECIMAL, taskResults.getMaxTestTime(), EPSILON);
    }

    @Test
    public void testGetMinTestTime() {
        taskResults.addElapsedTime(DECIMAL);
        taskResults.calculateStatistics();
        assertEquals(DECIMAL, taskResults.getMinTestTime(), EPSILON);
    }

    @Test
    public void testGetStandardDeviation() {
        double expected = 2.449489742783178d;
        taskResults.addElapsedTime(KHAMSA);
        taskResults.addElapsedTime(DECIMAL);
        taskResults.calculateStatistics();
        assertEquals(expected, taskResults.getStandardDeviation(), EPSILON);
    }

    @Test
    public void testGetTestIterations() {
        taskResults = new BenchmarkTaskResults(DECIMAL);
        assertEquals(DECIMAL, taskResults.getTestIterations());
    }

    @Test
    public void testAddElapsedTime() {
        taskResults.addElapsedTime(DECIMAL);
        assertEquals(DECIMAL, taskResults.getElapsedTimes()[0], EPSILON);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddElapsedTimeNegative() {
        taskResults.addElapsedTime(~DECIMAL);
    }

    @Test
    public void testCalculateStatistics() {
        taskResults.addElapsedTime(DECIMAL);
        taskResults.addElapsedTime(KHAMSA);
        taskResults.calculateStatistics();
        assertTrue(true);
    }
    
    @Test
    public void testCalculateStatisticsTimesCounterIsZero(){
        taskResults.calculateStatistics();
        assertTrue(true);
    }

    @Test
    public void testToString() {
        assertNotNull(taskResults.toString());
    }
    
    
    @Before
    public void setUp() throws Exception {
        taskResults = new BenchmarkTaskResults(DECIMAL);
    }

    @After
    public void tearDown() throws Exception {
    }
}
