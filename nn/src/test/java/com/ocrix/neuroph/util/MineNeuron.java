package com.ocrix.neuroph.util;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.TransferFunction;

public abstract class MineNeuron extends Neuron {
    private static final long serialVersionUID = -4953469538940415575L;

    public MineNeuron() {
        super();
    }

    public MineNeuron(InputFunction inpF, TransferFunction tf){
        super(inpF, tf);
        
        foo();
    }
    
    public void foo(){
        System.out.println("FOO");
    }
}
