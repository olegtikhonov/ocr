/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.util.io.FileOutputAdapter;

public class TestFileOutputAdapter {

    /* Test member */
    private FileOutputAdapter fileOutputAdapter = null;
    
    
    @Before
    public void setUp() throws Exception {
        fileOutputAdapter = new FileOutputAdapter(new File(ParamsTest.TEST_FILE));
    }


    @Test
    public void testFileOutputAdapterFile() {
        assertNotNull(fileOutputAdapter);
    }
    
    
    @Test (expected=IllegalArgumentException.class)
    public void testFileOutputAdapterFileIsNull() throws FileNotFoundException, IOException {
        File nullFile = null;
        fileOutputAdapter = new FileOutputAdapter(nullFile);
    }

    
    @Test
    public void testFileOutputAdapterString() throws FileNotFoundException, IOException {
        fileOutputAdapter = new FileOutputAdapter(ParamsTest.TEST_FILE);
        assertNotNull(fileOutputAdapter);
    }
    
    @Test  (expected=IllegalArgumentException.class)
    public void testFileOutputAdapterStringIsNull() throws FileNotFoundException, IOException {
        String nullString = null;
        fileOutputAdapter = new FileOutputAdapter(nullString);
    }

    @Test
    public void testToString(){
        String result = fileOutputAdapter.toString();
        assertEquals(FileOutputAdapter.class.getName(), result);
    }
    
    
    @After
    public void tearDown() throws Exception {
    }
}
