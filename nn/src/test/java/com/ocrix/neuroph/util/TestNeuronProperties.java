/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.NetProperty.WEIGHTS_FUNCTION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.And;
import com.ocrix.neuroph.core.input.Difference;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.Properties;
import com.ocrix.neuroph.util.SummingFunctionType;
import com.ocrix.neuroph.util.TransferFunctionType;
import com.ocrix.neuroph.util.WeightsFunctionType;

/**
 * Tests {@link NeuronProperties} functionality.
 */
public class TestNeuronProperties {

    private NeuronProperties np = null;
    private static final String prop = "testProperty";
    
    @Before
    public void setUp() throws Exception {
        np = new NeuronProperties();
    }
    
    
    @Test
    public void testSetProperty() {
        Neuron mn = new Neuron(new InputFunction(), new Sigmoid());
        np.setProperty(prop, mn);
        assertEquals(mn, np.getProperty(prop));
    }

    @Test
    public void testNeuronProperties() {
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesClassOfQextendsNeuron() {
        np = new NeuronProperties(ThresholdNeuron.class);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesClassOfQextendsNeuronClassOfQextendsTransferFunction() {
        np = new NeuronProperties(BiasNeuron.class, Sigmoid.class);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesClassOfQextendsNeuronClassOfQextendsInputFunctionClassOfQextendsTransferFunction() {
        np = new NeuronProperties(BiasNeuron.class, InputFunction.class , Sigmoid.class);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesClassOfQextendsNeuronClassOfQextendsWeightsFunctionClassOfQextendsSummingFunctionClassOfQextendsTransferFunction() {
        np = new NeuronProperties(Neuron.class, Difference.class, And.class , Sigmoid.class);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesClassOfQextendsNeuronTransferFunctionType() {
        np = new NeuronProperties(InputOutputNeuron.class, TransferFunctionType.TRAPEZOID);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesTransferFunctionTypeBooleanTrue() {
        np = new NeuronProperties(TransferFunctionType.LOG, true);
        assertNotNull(np);
    }
    
    @Test
    public void testNeuronPropertiesTransferFunctionTypeBooleanFalse() {
        np = new NeuronProperties(TransferFunctionType.LOG, false);
        assertNotNull(np);
    }

    @Test
    public void testNeuronPropertiesWeightsFunctionTypeSummingFunctionTypeTransferFunctionType() {
        np = new NeuronProperties(WeightsFunctionType.WEIGHTED_INPUT, SummingFunctionType.SUMSQR, TransferFunctionType.GAUSSIAN);
        assertNotNull(np);
    }

    @Test
    public void testGetWeightsFunction() {
        np = new NeuronProperties(WeightsFunctionType.WEIGHTED_INPUT, SummingFunctionType.SUMSQR, TransferFunctionType.GAUSSIAN);
        assertTrue(np.getWeightsFunction().toString().equals(WeightsFunctionType.WEIGHTED_INPUT.getTypeClass().toString()));
    }

    @Test
    public void testGetSummingFunction() {
        np = new NeuronProperties(WeightsFunctionType.WEIGHTED_INPUT, SummingFunctionType.SUMSQR, TransferFunctionType.GAUSSIAN);
        assertTrue(np.getSummingFunction().toString().equals(SummingFunctionType.SUMSQR.getTypeClass().toString()));
    }

    @Test
    public void testGetInputFunction() {
        np = new NeuronProperties(BiasNeuron.class, InputFunction.class, Sigmoid.class);
        assertTrue(np.getInputFunction().getName().equals(InputFunction.class.getName()));
    }

    @Test
    public void testGetTransferFunction() {
        np = new NeuronProperties(BiasNeuron.class, InputFunction.class, Sigmoid.class);
        assertTrue(np.getTransferFunction().getName().equals(Sigmoid.class.getName()));
    }

    @Test
    public void testGetNeuronType() {
        np = new NeuronProperties(ThresholdNeuron.class, InputFunction.class, Sigmoid.class);
        assertTrue(np.getNeuronType().getName().equals(ThresholdNeuron.class.getName()));
    }

    @Test
    public void testGetTransferFunctionProperties() {
        Properties props = np.getTransferFunctionProperties();
        assertNotNull(props);
    }
    
    @Test
    public void testSetNeuronPropertyWeightsFunctionType(){
        np.setProperty(WEIGHTS_FUNCTION.to(), WeightsFunctionType.DIFFERENCE);
        System.out.println(np.get(WEIGHTS_FUNCTION.to()));
    }
}
