package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.io.JDBCInputAdapter;
import com.ocrix.neuroph.util.io.NeurophInputException;

/**
 * Tests {@link JDBCInputAdapter} functionality.
 */
public class TestJDBCInputAdapter {
    /* JDBC adapter to be tested */
    private JDBCInputAdapter jdbcIA = null;
    /* Connection to the DB */
    private Connection dbConnection = null;
    /* A table name */
    private static String tableName = "TEST_JDBC_INPUT_ADAPTER";
    /* A column table */
    private static String columnName = "TRAIN_ITEM";
    

    @Before
    public void setUp() throws Exception {
        try {
            System.setProperty("derby.system.home", "target");
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            dbConnection = DriverManager.getConnection("jdbc:derby:" + "target/testdb" + ";create=true");
        } catch (SQLException ex) {
        } catch (ClassNotFoundException e) {
        }

        jdbcIA = new JDBCInputAdapter(dbConnection);
    }

    
    @After
    public void tearDown() throws Exception {
        jdbcIA.close();
    }

    
    @Test
    public void testJDBCInputAdapterConnectionString() throws SQLException {
        String query = "select * from " + tableName;
        Statement sta = dbConnection.createStatement();
        
        try {
            sta.executeUpdate("DROP TABLE " + tableName);
        } catch (Exception e) {
        }
        
        
        int count = sta.executeUpdate("CREATE TABLE " + tableName + " (ID INT, " + columnName + " DOUBLE PRECISION )");

        double[] trainVector = TestUtil.genDoubleVector(KHAMSA);
        
        for(int i = 0; i < trainVector.length; i++){
            count += sta.executeUpdate("INSERT INTO " + tableName + " (ID, " + columnName + " )" + " VALUES(" + i + " , " + trainVector[i] + " )");
        }

        sta.close();
        
        assertEquals(count, KHAMSA);
        
        jdbcIA = new JDBCInputAdapter(dbConnection, query);
        
        assertNotNull(jdbcIA.toString());
    }

    @Test
    public void testJDBCInputAdapterConnection() {
        jdbcIA = new JDBCInputAdapter(dbConnection);
        assertNotNull(jdbcIA);
    }

    @Test
    public void testReadInput() throws SQLException {
        String query = "select  " + columnName + " from " + tableName;
        Statement sta = dbConnection.createStatement();
        
        try {
            sta.executeUpdate("DROP TABLE " + tableName);
        } catch (Exception e) {
        }
        
        
        int count = sta.executeUpdate("CREATE TABLE " + tableName + " (ID INT, " + columnName + " DOUBLE PRECISION )");
        double[] trainVector = TestUtil.genDoubleVector(KHAMSA);
        
        for(int i = 0; i < trainVector.length; i++){
            count += sta.executeUpdate("INSERT INTO " + tableName + " (ID, " + columnName + " )" + " VALUES(" + i + " , " + trainVector[i] + " )");
        }
        
        sta.close();
        
        assertEquals(count, KHAMSA);
        jdbcIA = new JDBCInputAdapter(dbConnection, query);
        double[] input = jdbcIA.readInput();
        assertTrue(Arrays.equals(trainVector, input));
    }


    @Test
    public void testClose() {
        jdbcIA.close();
        assertTrue(true);
    }

    
    @Test
    public void testGetConnection() {
        assertEquals(dbConnection, jdbcIA.getConnection());
    }
    
    
    @Test
    public void testToString(){
        assertTrue(jdbcIA.toString().contains(JDBCInputAdapter.class.getName()));
    }
    
    
    @Test (expected=NeurophInputException.class)
    public void testSQLException(){
        jdbcIA = new JDBCInputAdapter(dbConnection, null);
    }
}
