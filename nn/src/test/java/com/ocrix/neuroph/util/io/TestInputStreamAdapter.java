/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.ParamsTest.READER_FILE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.io.InputStreamAdapter;

/**
 * Tests an {@link InputStreamAdapter} functionality.
 */
public class TestInputStreamAdapter {

    /* Test's members */
    private InputStreamAdapter isa = null;
    
    
    @Before
    public void setUp() throws Exception {
        isa = new InputStreamAdapter(new BufferedReader(new FileReader(new File(READER_FILE))));
    }

    @After
    public void tearDown() throws Exception {
        if(isa != null){
            isa.close();
        }
    }

    @Test
    public void testRead() {
        double[] result = isa.readInput();
        assertNotNull(result);
    }

    @Test
    public void testInputStreamAdapterFile() throws IOException {
        isa = new InputStreamAdapter(new File(READER_FILE));
        assertNotNull(isa);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testInputStreamAdapterFileIsNull() throws IOException {
        File nullFile = null;
        isa = new InputStreamAdapter(nullFile);
    }
    
    @Test
    public void testInputStreamAdapterBufferedReaderIsNull() throws IOException {
        BufferedReader nullReader = null;
        isa = new InputStreamAdapter(nullReader);
        double[] result = isa.readInput();
        assertNull(result);
    }
    
    @Test
    public void testInputStreamAdapterInputStream() throws IOException {
        InputStream is = new FileInputStream(READER_FILE);
        isa = new InputStreamAdapter(is);
        assertNotNull(isa);
        
        double[] result = isa.readInput();
        assertNotNull(result);
        
        if(is != null){
            is.close();
        }
    }
    
    @Test
    public void testInputStreamAdapterInputStreamIsNull() throws IOException {
        InputStream is = null;
        isa = new InputStreamAdapter(is);
    }
    
    @Test
    public void testSetFile() {
        File tF = new File(READER_FILE);
        isa.setFile(tF);
        assertEquals(tF, isa.getFile());
    }
    
    @Test
    public void testGetFile() {
        testSetFile();
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testSetNullAsFile() {
        File tF = null;
        isa.setFile(tF);
    }
    
    @Test
    public void testToString() {
        String result = isa.toString();
        assertNotNull(result);
    }
}
