/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static org.junit.Assert.*;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.util.io.NeurophInputException;

/**
 * Tests {@link NeurophInputException} functionality.
 */
public class TestNeurophInputException {
    /* Test's member */
    private NeurophInputException nie = null;

    
    @Before
    public void setUp() throws Exception {
        nie = new NeurophInputException();
    }

    
    @After
    public void tearDown() throws Exception {
    }

    
    @Test
    public void testNeurophInputException() {
        assertNotNull(nie);
    }

    @Test
    public void testNeurophInputExceptionString() {
        String description = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        nie = new NeurophInputException(description);
        assertEquals(description, nie.getMessage());
    }

    @Test
    public void testNeurophInputExceptionStringThrowable() {
        String description = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        String message = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        nie = new NeurophInputException(description, new Throwable(message));
        assertEquals(nie.getMessage(), description);
        assertEquals(nie.getCause().getMessage(), message);
    }

    @Test
    public void testNeurophInputExceptionThrowable() {
        String message = RandomStringUtils.randomAscii(ParamsTest.KHAMSA);
        nie = new NeurophInputException(new Throwable(message));
        assertEquals(nie.getCause().getMessage(), message);
    }
}
