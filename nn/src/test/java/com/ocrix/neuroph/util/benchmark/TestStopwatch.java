/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.util.benchmark;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.benchmark.Stopwatch;


/**
 * Tests {@link Stopwatch} functionality.
 */
public class TestStopwatch {

    /* Test member */
    private Stopwatch stopwatch = null;
    private final static long TIME_TO_WAIT = 500;
    
    @Before
    public void setUp() throws Exception {
        stopwatch = new Stopwatch();
    }


    @Test
    public void testStopwatch() {
        assertNotNull(stopwatch);
    }

    @Test
    public void testStart() {
        stopwatch.start();
        assertTrue(true);
    }

    @Test
    public void testStop() {
        stopwatch.stop();
        assertTrue(true);
    }

    @Test
    public void testGetElapsedTime() throws InterruptedException {
        stopwatch.reset();
        stopwatch.start();
        Thread.sleep(TIME_TO_WAIT);
        stopwatch.stop();
        assertTrue(stopwatch.getElapsedTime() > 0);
    }

    @Test
    public void testReset() throws InterruptedException {
        stopwatch.start();
        Thread.sleep(TIME_TO_WAIT);
        stopwatch.stop();
        long beforeResetting = stopwatch.getElapsedTime();
        stopwatch.reset();
        long afterResetting = stopwatch.getElapsedTime();
        assertTrue(beforeResetting != afterResetting);
    }
    
    @Test
    public void testGetElapsedTimeIsRunningTrue(){
        stopwatch.start();
        stopwatch.getElapsedTime();
        stopwatch.stop();
        assertTrue(true);
    }
    
    
    @After
    public void tearDown() throws Exception {
        stopwatch.reset();
    }
}
