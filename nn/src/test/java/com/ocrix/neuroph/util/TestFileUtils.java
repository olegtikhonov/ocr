/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static com.ocrix.neuroph.common.ParamsTest.PATH_FILE_TO_SAVE;
import static com.ocrix.neuroph.common.ParamsTest.TEST_FILE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.util.FileUtils;

public class TestFileUtils {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testWriteStringToFile() {
        try {
            assertNotNull(FileUtils.readStringFromFile(new File(TEST_FILE)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWriteStringToFileNotFound() {
        try {
            FileUtils.writeStringToFile(new File("."), TestFileUtils.class.getName());
            assertTrue(true);
        } catch (IOException e) {
        }
    }

    @Test
    public void testReadStringFromFileFileNotFound() {
        try {
            FileUtils.readStringFromFile(new File("."));
            assertTrue(true);
        } catch (IOException e) {
        }
    }

    @Test
    public void testReadStringFromFile() {
        File file = new File(PATH_FILE_TO_SAVE
                + TestFileUtils.class.getCanonicalName() + ".txt");
        try {

            if (file.exists()) {
                file.delete();
            }

            file.createNewFile();

            String before = FileUtils.readStringFromFile(file);
            String xmlString = RandomStringUtils.randomAlphabetic(KHAMSA);
            FileUtils.writeStringToFile(file, xmlString);
            String after = FileUtils.readStringFromFile(file);
            assertNotNull(after);
            assertFalse(before.equals(after));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
