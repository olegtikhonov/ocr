/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.ParamsTest;
import com.ocrix.neuroph.util.io.FileInputAdapter;


/**
 * Tests {@link FileInputAdapter} functionality.
 */
public class TestFileInputAdapter {

    /* Test's member */
    private FileInputAdapter fileInputAdapter = null;
    
    
    @Before
    public void setUp() throws Exception {
        fileInputAdapter = new FileInputAdapter(new File(ParamsTest.TEST_FILE));
    }

    @Test
    public void testFileInputAdapterFile() {
        assertNotNull(fileInputAdapter);
    }

    @Test
    public void testFileInputAdapterFileArg() throws IOException {
        fileInputAdapter = new FileInputAdapter(new BufferedReader(new FileReader(new File(ParamsTest.TEST_FILE))));
        assertNotNull(fileInputAdapter);
    }
    
    
    @Test
    public void testFileInputAdapterArgString() throws IOException {
        fileInputAdapter = new FileInputAdapter(ParamsTest.TEST_FILE);
        assertNotNull(fileInputAdapter);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testFileInputAdapterFileIsNull() throws IOException{
        File file = null;
        fileInputAdapter = new FileInputAdapter(file);
    }

    @Test (expected=IllegalArgumentException.class)
    public void testFileInputAdapterStringIsNull() throws IOException{
        String path2file = null;
        fileInputAdapter = new FileInputAdapter(path2file);
    }
    
    @Test
    public void testToString(){
        String result = fileInputAdapter.toString();
        assertEquals(FileInputAdapter.class.getName(), result);
    }
    

    @Test
    public void testCreateFileReader(){
        try {
            Method createFileReader = fileInputAdapter.getClass().getDeclaredMethod("createFileReader", File.class);
            createFileReader.setAccessible(true);
            
            Object result = createFileReader.invoke(fileInputAdapter, new File(ParamsTest.TEST_FILE));
            assertTrue(result.getClass().getName().contains("FileReader"));
            
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    
    //createFile
    @Test
    public void testCreateFile(){
        try {
            Method createFile = fileInputAdapter.getClass().getDeclaredMethod("createFile", String.class);
            createFile.setAccessible(true);
            
            Object result = createFile.invoke(fileInputAdapter, ParamsTest.TEST_FILE);
            assertTrue(result.getClass().getName().contains("File"));
            
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    

    @After
    public void tearDown() throws Exception {
    }

}
