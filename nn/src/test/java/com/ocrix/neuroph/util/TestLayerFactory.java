/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;


/**
 * Tests {@link LayerFactory} functionality.
 */
public class TestLayerFactory {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreateLayerIntNeuronProperties() {
        NeuronProperties neuClass = new NeuronProperties(CompetitiveNeuron.class);
        Layer layer = LayerFactory.createLayer(KHAMSA, neuClass);
        assertNotNull(layer);
        assertTrue(layer.getNeuronAt(0).getClass().getName().contains(CompetitiveNeuron.class.getName()));
    }

    @Test
    public void testCreateLayerIntTransferFunctionType() {
        Layer layer = LayerFactory.createLayer(KHAMSA, TransferFunctionType.RAMP);
        assertNotNull(layer);
    }

    @Test
    public void testCreateLayerIntClassOfQextendsTransferFunction() {
        Layer layer = LayerFactory.createLayer(KHAMSA, Sigmoid.class);
        assertNotNull(layer);
    }

    @Test
    public void testCreateLayerListOfNeuronProperties() {
        List<NeuronProperties> props = new ArrayList<NeuronProperties>();
        NeuronProperties npDefault = new NeuronProperties();
        NeuronProperties neuClass = new NeuronProperties(BiasNeuron.class);
        props.add(npDefault);
        props.add(neuClass);
        Layer layer = LayerFactory.createLayer(props);
        assertNotNull(layer);
    }

    @Test
    public void testPrivateCitor(){
        try {
            Constructor<LayerFactory> constructor = LayerFactory.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            LayerFactory instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }

}
