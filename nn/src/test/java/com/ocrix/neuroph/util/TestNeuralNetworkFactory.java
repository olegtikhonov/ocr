/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.nnet.Adaline;
import com.ocrix.neuroph.nnet.BAM;
import com.ocrix.neuroph.nnet.CompetitiveNetwork;
import com.ocrix.neuroph.nnet.Hopfield;
import com.ocrix.neuroph.nnet.Instar;
import com.ocrix.neuroph.nnet.Kohonen;
import com.ocrix.neuroph.nnet.MaxNet;
import com.ocrix.neuroph.nnet.MultiLayerPerceptron;
import com.ocrix.neuroph.nnet.Outstar;
import com.ocrix.neuroph.nnet.Perceptron;
import com.ocrix.neuroph.nnet.RbfNetwork;
import com.ocrix.neuroph.nnet.SupervisedHebbianNetwork;
import com.ocrix.neuroph.nnet.UnsupervisedHebbianNetwork;
import com.ocrix.neuroph.nnet.learning.AntiHebbianLearning;
import com.ocrix.neuroph.nnet.learning.BackPropagation;
import com.ocrix.neuroph.nnet.learning.BinaryDeltaRule;
import com.ocrix.neuroph.nnet.learning.DynamicBackPropagation;
import com.ocrix.neuroph.nnet.learning.InstarLearning;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;
import com.ocrix.neuroph.nnet.learning.PerceptronLearning;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Tests {@link NeuralNetworkFactory} functionality.
 */
public class TestNeuralNetworkFactory {

    @Test
    public void testCreateAdaline() {
        Adaline adaline = NeuralNetworkFactory.createAdaline(KHAMSA);
        assertNotNull(adaline);
    }

    @Test
    public void testCreatePerceptronIntIntTransferFunctionType() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, KHAMSA, TransferFunctionType.TRAPEZOID);
        assertNotNull(perceptron);
    }

    @Test( expected=IllegalArgumentException.class)
    public void testCreatePerceptronIntIntTransferFunctionTypeNull() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, KHAMSA, null);
        assertNotNull(perceptron);
    }

    @Test//( expected=IllegalArgumentException.class)
    public void testCreatePerceptronIntNegIntTransferFunctionType() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, ~KHAMSA, TransferFunctionType.SIGMOID);
        assertNotNull(perceptron);
    }


    @Test
    public void testCreatePerceptronIntIntTransferFunctionTypeClassOfQ() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, KHAMSA, TransferFunctionType.SIN, PerceptronLearning.class);
        assertNotNull(perceptron);
    }

    @Test
    public void testCreatePerceptronIntIntTransferFunctionTypeClassOfQSecondCase() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, KHAMSA, TransferFunctionType.SIN, BinaryDeltaRule.class);
        assertNotNull(perceptron);
    }
    
    @Test
    public void testCreatePerceptronIntIntTransferFunctionTypeClassOfQThirdCase() {
        Perceptron perceptron = NeuralNetworkFactory.createPerceptron(KHAMSA, KHAMSA, TransferFunctionType.SIN, InstarLearning.class);
        assertNotNull(perceptron);
    }


    @Test
    public void testCreateMLPerceptronStringTransferFunctionType() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        NeuralNetworkFactory.createMLPerceptron(numbers,  TransferFunctionType.SIN);
    }


    @Test
    public void testCreateMLPerceptronStringTransferFunctionTypeClassOfQBooleanBooleanBackPropagationLR() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, TransferFunctionType.STEP, BackPropagation.class, true, true);
        assertNotNull(mlp);
    }
    
    @Test
    public void testCreateMLPerceptronStringTransferFunctionTypeClassOfQBooleanMomentumBackpropagationLR() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, TransferFunctionType.STEP, MomentumBackpropagation.class, true, true);
        assertNotNull(mlp);
    }
    
    @Test
    public void testCreateMLPerceptronStringTransferFunctionTypeClassOfQBooleanDynamicBackPropagationLR() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, TransferFunctionType.STEP, DynamicBackPropagation.class, true, true);
        assertNotNull(mlp);
    }
    
    @Test
    public void testCreateMLPerceptronStringTransferFunctionTypeClassOfQBooleanAntiHebbianLearningLR() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, TransferFunctionType.STEP, AntiHebbianLearning.class, true, false);
        assertNotNull(mlp);
    }
    
    
    @Test (expected=IllegalArgumentException.class)
    public void testCreateMLPerceptronNegativeLayersNull() {
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(null, TransferFunctionType.STEP, DynamicBackPropagation.class, true, false);
        assertNotNull(mlp);
    }
    
    
    @Test (expected=IllegalArgumentException.class)
    public void testCreateMLPerceptronNegativeTransferFunctionTypeNull() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, null, DynamicBackPropagation.class, true, true);
        assertNotNull(mlp);
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testCreateMLPerceptronNegativeLearningRuleNull() {
        String  numbers = String.valueOf(TestUtil.genInteger()) + " " + String.valueOf(TestUtil.genInteger());
        MultiLayerPerceptron mlp = NeuralNetworkFactory.createMLPerceptron(numbers, TransferFunctionType.GAUSSIAN, null, false, true);
        assertNotNull(mlp);
    }
    

    @Test
    public void testCreateHopfield() {
        Hopfield hopfield = NeuralNetworkFactory.createHopfield(KHAMSA);
        assertNotNull(hopfield);
    }
    
    @Test //(expected=IllegalArgumentException.class)
    public void testCreateHopfieldNegParam() {
        Hopfield hopfield = NeuralNetworkFactory.createHopfield(~KHAMSA);
        assertNotNull(hopfield);
    }

    @Test
    public void testCreateBam() {
        BAM bam = NeuralNetworkFactory.createBam(KHAMSA, KHAMSA);
        assertNotNull(bam);
    }
    
    @Test //(expected=IllegalArgumentException.class)
    public void testCreateBamNegative() {
        BAM bam = NeuralNetworkFactory.createBam(~KHAMSA, ~KHAMSA);
        assertNotNull(bam);
    }
    

    @Test
    public void testCreateKohonen() {
        Kohonen kohonen = NeuralNetworkFactory.createKohonen(KHAMSA, KHAMSA);
        assertNotNull(kohonen);
    }
    
    
    @Test //(expected=IllegalArgumentException.class)
    public void testCreateKohonenNegative() {
        Kohonen kohonen = NeuralNetworkFactory.createKohonen(~KHAMSA, ~KHAMSA);
        assertNotNull(kohonen);
    }

    @Test
    public void testCreateSupervisedHebbian() {
        SupervisedHebbianNetwork sh = NeuralNetworkFactory.createSupervisedHebbian(KHAMSA, KHAMSA, TransferFunctionType.RAMP);
        assertNotNull(sh);
    }
    
    
//    @Test //(expected=IllegalArgumentException.class)
    public void testCreateSupervisedHebbianNegative() {
        SupervisedHebbianNetwork sh = NeuralNetworkFactory.createSupervisedHebbian(~KHAMSA, ~KHAMSA, null);
        assertNotNull(sh);
    }

    @Test
    public void testCreateUnsupervisedHebbian() {
        UnsupervisedHebbianNetwork uhn = NeuralNetworkFactory.createUnsupervisedHebbian(KHAMSA, KHAMSA, TransferFunctionType.TANH);
        assertNotNull(uhn);
    }
    
//    @Test //(expected=IllegalArgumentException.class)
    public void testCreateUnsupervisedHebbianNegative() {
        UnsupervisedHebbianNetwork uhn = NeuralNetworkFactory.createUnsupervisedHebbian(~KHAMSA, ~KHAMSA, null);
        assertNotNull(uhn);
    }

    @Test
    public void testCreateMaxNet() {
        MaxNet maxNet = NeuralNetworkFactory.createMaxNet(KHAMSA);
        assertNotNull(maxNet);
    }

    @Test //(expected=IllegalArgumentException.class)
    public void testCreateMaxNetNegativeParam() {
        MaxNet maxNet = NeuralNetworkFactory.createMaxNet(~KHAMSA);
        assertNotNull(maxNet);
    }
    
    
    @Test
    public void testCreateInstar() {
        Instar instar = NeuralNetworkFactory.createInstar(KHAMSA);
        assertNotNull(instar);
    }
    
    @Test //(expected=IllegalArgumentException.class)
    public void testCreateInstarNegativeParam() {
        Instar instar = NeuralNetworkFactory.createInstar(~KHAMSA);
        assertNotNull(instar);
    }

    @Test
    public void testCreateOutstar() {
        Outstar outstar = NeuralNetworkFactory.createOutstar(KHAMSA);
        assertNotNull(outstar);
    }
    
    @Test //(expected=IllegalArgumentException.class)
    public void testCreateOutstarNegativeParam() {
        Outstar outstar = NeuralNetworkFactory.createOutstar(~KHAMSA);
        assertNotNull(outstar);
    }

    @Test
    public void testCreateCompetitiveNetwork() {
        CompetitiveNetwork cn = NeuralNetworkFactory.createCompetitiveNetwork(KHAMSA, KHAMSA);
        assertNotNull(cn);
    }
    
//    @Test //(expected=IllegalArgumentException.class)
    public void testCreateCompetitiveNetworkNegativeParams() {
        CompetitiveNetwork cn = NeuralNetworkFactory.createCompetitiveNetwork(~KHAMSA, ~KHAMSA);
        assertNotNull(cn);
    }

    @Test
    public void testCreateRbfNetwork() {
        RbfNetwork rbfNetwork = NeuralNetworkFactory.createRbfNetwork(KHAMSA, KHAMSA, KHAMSA);
        assertNotNull(rbfNetwork);
    }
    
//    @Test //(expected=IllegalArgumentException.class)
    public void testCreateRbfNetworkNegParams() {
        RbfNetwork rbfNetwork = NeuralNetworkFactory.createRbfNetwork(KHAMSA, KHAMSA, ~KHAMSA);
        assertNotNull(rbfNetwork);
    }
    

    @Test
    public void testSetDefaultIO() {
        NeuralNetwork nn = new NeuralNetwork();
        nn = TestUtil.getDefaultNN();
        NeuralNetworkFactory.setDefaultIO(nn);
        assertTrue(true);
    }
    
//    @Test //(expected=IllegalArgumentException.class)
    public void testSetDefaultIONullNN() {
        NeuralNetworkFactory.setDefaultIO(null);
        assertTrue(true);
    }
    
    @Test
    public void testDefaultCitor(){
        
        try {
            Constructor<NeuralNetworkFactory> constructor = NeuralNetworkFactory.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            NeuralNetworkFactory instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}
