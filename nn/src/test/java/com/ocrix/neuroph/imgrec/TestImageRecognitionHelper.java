/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.imgrec;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.DataSet;
import com.ocrix.neuroph.imgrec.image.Dimension;
import com.ocrix.neuroph.util.TransferFunctionType;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;

/**
 * Tests {@link ImageRecognitionHelper} functionality.
 */
public class TestImageRecognitionHelper {
    private BufferedImage src;
    
    @Before
    public void setUp() throws Exception {
        src = ImageIO.read(new File("../nn/src/test/resources/super_mario.gif"));
    }

    @Test
    public void testCreateNewNeuralNetwork() {
        String label = RandomStringUtils.randomAlphabetic(DECIMAL);
        Dimension samplingResolution = new Dimension(src.getWidth(), src.getHeight());
        List<String> imageLabels = new ArrayList<String>(1);
        List<Integer> layersNeuronsCount = new ArrayList<Integer>(1);
        NeuralNetwork nn = ImageRecognitionHelper.createNewNeuralNetwork(label, samplingResolution, ColorMode.FULL_COLOR, imageLabels, layersNeuronsCount, TransferFunctionType.SGN);
        assertNotNull(nn);
    }
    
    @Test
    public void testCreateNewNeuralNetworkNotFullColor() {
        String label = RandomStringUtils.randomAlphabetic(DECIMAL);
        Dimension samplingResolution = new Dimension(src.getWidth(), src.getHeight());
        List<String> imageLabels = new ArrayList<String>(1);
        List<Integer> layersNeuronsCount = new ArrayList<Integer>(1);
        NeuralNetwork nn = ImageRecognitionHelper.createNewNeuralNetwork(label, samplingResolution, ColorMode.BLACK_AND_WHITE, imageLabels, layersNeuronsCount, TransferFunctionType.SGN);
        assertNotNull(nn);
    }


    @Test
    public void testCreateTrainingSet() {
        List<String> imageLabels = new ArrayList<String>(DECIMAL);
        FractionRgbData frgbd = new FractionRgbData(src);
        Map<String, FractionRgbData> rgbDataMap = new HashMap<String, FractionRgbData>();
        rgbDataMap.put("key", frgbd);
        DataSet result = ImageRecognitionHelper.createTrainingSet(imageLabels, rgbDataMap);
        assertNotNull(result);
    }

    @Test
    public void testCreateBlackAndWhiteTrainingSet() {
        List<String> imageLabels = new ArrayList<String>(DECIMAL);
        initList(imageLabels);
        FractionRgbData frgbd = new FractionRgbData(src);
        Map<String, FractionRgbData> rgbDataMap = new HashMap<String, FractionRgbData>();
        rgbDataMap.put("key", frgbd);
        DataSet result = ImageRecognitionHelper.createBlackAndWhiteTrainingSet(imageLabels, rgbDataMap);
        assertNotNull(result);
    }
    
    private void initList(List<String> imageLabels) {
        if(imageLabels != null) {
            for(int i = 0; i < DECIMAL; i++){
                imageLabels.add(RandomStringUtils.randomAlphabetic(DECIMAL));
            }
        }
    }
}
