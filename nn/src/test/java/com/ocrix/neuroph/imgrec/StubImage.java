package com.ocrix.neuroph.imgrec;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import com.ocrix.neuroph.imgrec.image.Image;

public class StubImage implements Image {

    private BufferedImage image;

    public StubImage(BufferedImage im) {
        this.image = im;
    }

    public int getPixel(int x, int y) {
        int[] arr = new int[3];
        int result = 0;
        try {
            result = image.getData().getPixel(x, y, arr)[2];
        } catch (Exception e) {
            // TODO: handle exception
        }
        
        return result;
    }

    public void setPixel(int x, int y, int color) {
        int[] arr = null;
        image.getAlphaRaster().setPixel(x, y, arr);
    }

    public int[] getPixels(int offset, int stride, int x, int y, int width,
            int height) {
        int[] arr = null;
        return image.getData().getPixels(x, y, width, height, arr);
    }

    public void setPixels(int[] pixels, int offset, int stride, int x, int y,
            int width, int height) {
        int[] arr = null;
        image.getAlphaRaster().setPixels(x, y, width, height, arr);
    }

    public int getWidth() {
        return image.getWidth();
    }

    public int getHeight() {
        return image.getWidth();
    }

    public Image resize(int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height,
                BufferedImage.SCALE_FAST);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();

        return new StubImage(resizedImage);
    }

    public Image crop(int x1, int y1, int x2, int y2) {
        return new StubImage(image.getSubimage(x1, y1, x2, y2));
    }

    public int getType() {
        return image.getType();
    }

}
