/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.imgrec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.junit.BeforeClass;
import org.junit.Test;
import com.ocrix.neuroph.imgrec.image.Image;


/**
 * Tests {@link FractionRgbData} functionality.
 */
public class TestFractionRgbData {

    private static FractionRgbData frd;
    private static BufferedImage src;
    private static Image stubImage;
    
    @BeforeClass
    public static void setUp() throws Exception {
        src = ImageIO.read(new File("../nn/src/test/resources/pre5.jpg"));
        frd = new FractionRgbData(src);
//        stubImage = new StubImage(src);
    }

    @Test
    public void testHashCode() {
        assertTrue(frd.hashCode() > 0);
    }

//    @Test
    public void testFractionRgbDataImage() {
        frd = new FractionRgbData(new StubImage(src)); 
        assertNotNull(frd);
    }

    @Test
    public void testFractionRgbDataBufferedImage() {
        assertNotNull(frd);
    }

//    @Test
    public void testPopulateRGBArrays() {
        frd.populateRGBArrays(stubImage);
        assertTrue(true);
    }

    @Test
    public void testConvertRgbInputToBinaryBlackAndWhite() {
        double[] arr = null;
        double[] vp = src.getData().getPixel(0, 0, arr);
        double[] result = FractionRgbData.convertRgbInputToBinaryBlackAndWhite(vp);
        
        assertNotNull(vp);
        assertNotNull(result);
    }

    @Test
    public void testGetWidth() {
        assertEquals(src.getWidth(), frd.getWidth());
    }

    @Test
    public void testGetHeight() {
        assertEquals(src.getHeight(), frd.getHeight());
        
    }

    @Test
    public void testGetRedValues() {
        assertTrue(frd.getRedValues()[0][0] != 0.0);
    }

    @Test
    public void testGetGreenValues() {
        assertTrue(frd.getGreenValues()[0][0] != 0.0);
    }

    @Test
    public void testGetBlueValues() {
        assertTrue(frd.getBlueValues()[0][0] != 0.0);
    }

    @Test
    public void testGetFlattenedRgbValues() {
        assertNotNull(frd.getFlattenedRgbValues());
    }

    @Test
    public void testEqualsObject() {
        assertTrue(frd.equals(frd));
    }

    @Test
    public void testToString() {
        assertTrue(frd.toString().contains(FractionRgbData.class.getName()));
    }
}
