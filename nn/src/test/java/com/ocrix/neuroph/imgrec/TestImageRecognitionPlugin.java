/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.imgrec;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.imgrec.image.Dimension;
import com.ocrix.neuroph.nn.TestUtil;

public class TestImageRecognitionPlugin {

    private ImageRecognitionPlugin irp;
    private Dimension samplingResolution;
    private static final String SUPER_MARIO = "../nn/src/test/resources/super_mario.gif";
    
    @Before
    public void setUp() throws Exception {
        BufferedImage src = ImageIO.read(new File(SUPER_MARIO));
        samplingResolution = new Dimension(src.getWidth(), src.getHeight());
        irp = new ImageRecognitionPlugin(samplingResolution);
        
        NeuralNetwork nn = TestUtil.getDefaultNN();
        TestUtil.setPrerequirements(nn);
        irp.setParentNetwork(nn);
    }

    @Test
    public void testImageRecognitionPluginDimension() {
        irp = new ImageRecognitionPlugin(samplingResolution);
        assertNotNull(irp);
    }

    @Test
    public void testImageRecognitionPluginDimensionColorMode() {
        irp = new ImageRecognitionPlugin(samplingResolution, ColorMode.FULL_COLOR);
        System.out.println(irp);
    }

    @Test
    public void testGetSamplingResolution() {
        assertEquals(samplingResolution, irp.getSamplingResolution());
    }

    @Test
    public void testGetColorMode() {
        irp = new ImageRecognitionPlugin(samplingResolution, ColorMode.FULL_COLOR);
        assertEquals(ColorMode.FULL_COLOR, irp.getColorMode());
        
        irp = new ImageRecognitionPlugin(samplingResolution, ColorMode.BLACK_AND_WHITE);
        assertEquals(ColorMode.BLACK_AND_WHITE, irp.getColorMode());
    }

    @Test
    public void testSetInputImage() throws ImageSizeMismatchException, IOException {
        StubImage si = new StubImage(ImageIO.read(new File(SUPER_MARIO)));
        irp.setInput(si);
        assertTrue(true);
    }

    @Test
    public void testSetInputFile() throws ImageSizeMismatchException, IOException {
       irp.setInput(new File(SUPER_MARIO));
       assertTrue(true);
    }

    @Test
    public void testSetInputURL() {
        try {
            irp.setInput(new File(SUPER_MARIO).toURI().toURL());
            assertNotNull(irp);
        } catch (ImageSizeMismatchException e) {
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
    }

    @Test
    public void testProcessInput() {
        TestUtil.addLayersToNN(DECIMAL, irp.getParentNetwork());
        irp.processInput();
        assertTrue(true);
    }

    @Test
    public void testGetOutput() {
        HashMap<String, Double> hash = irp.getOutput();
        assertNotNull(hash);
    }

    @Test
    public void testRecognizeImageImage() {
        try {
            HashMap<String, Double> hash = irp.recognizeImage(new StubImage(ImageIO.read(new File(SUPER_MARIO))));
            assertNotNull(hash);
        } catch (ImageSizeMismatchException e) {
        } catch (IOException e) {
        }
    }

    @Test
    public void testRecognizeImageBufferedImage() {
        try {
            HashMap<String, Double> hash = irp.recognizeImage(ImageIO.read(new File(SUPER_MARIO)));
            assertNotNull(hash);
        } catch (ImageSizeMismatchException e) {
        } catch (IOException e) {
        }
    }

    @Test
    public void testRecognizeImageFile() {
        try {
            HashMap<String, Double> hash = irp.recognizeImage(new File(SUPER_MARIO));
            assertNotNull(hash);
        } catch (ImageSizeMismatchException e) {
        } catch (IOException e) {
        }
    }

    @Test
    public void testRecognizeImageURL() {
        try {
            HashMap<String, Double> hash = irp.recognizeImage(new File(SUPER_MARIO).toURI().toURL());
            assertNotNull(hash);
        } catch (ImageSizeMismatchException e) {
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
    }

    @Test
    public void testGetMaxOutput() {
        HashMap<String, Neuron> hash = irp.getMaxOutput();
        assertNotNull(hash);
    }

}
