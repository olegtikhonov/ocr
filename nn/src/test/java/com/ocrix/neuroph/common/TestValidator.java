/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.common;

import static com.ocrix.neuroph.common.ParamsTest.DECIMAL;
import static com.ocrix.neuroph.common.ParamsTest.KHAMSA;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.encog.engine.network.flat.FlatNetwork;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.input.And;
import com.ocrix.neuroph.core.input.WeightedInput;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.nn.TestUtil;
import com.ocrix.neuroph.util.Properties;
import com.ocrix.neuroph.util.io.InputAdapter;
import com.ocrix.neuroph.util.io.JDBCInputAdapter;
import com.ocrix.neuroph.util.io.OutputAdapter;

/**
 * Tests {@link Validator} functionality.
 * 
 */
public class TestValidator {

    @Test
    public void testPrivateDefaultCitor() {

        try {
            Constructor<Validator> constructor = Validator.class
                    .getDeclaredConstructor();
            constructor.setAccessible(true);
            Validator instance = constructor.newInstance();
            assertNotNull(instance);

        } catch (SecurityException e) {
        } catch (NoSuchMethodException e) {
        } catch (IllegalArgumentException e) {
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateLenghts() {
        double[] input = TestUtil.genDoubleVector(KHAMSA);
        double[] output = TestUtil.genDoubleVector(DECIMAL);
        Validator.validateLenghts(input, output);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateDoubleInRangeParLessOrEqThanMinimum() {
        double parToBeValidated = 0.05d;
        double min = parToBeValidated;

        Validator
                .validateDoubleInRange(parToBeValidated, min, Double.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateDoubleBoundsParLessThanMinimum() {
        // if (parToBeValidated < min && parToBeValidated > max)
        double parToBeValidated = 0.05d;
        double min = 0.06d;
        double max = 0.045d;
        Validator.validateDoubleBounds(parToBeValidated, min, max);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateWeightsFunctionIsNull() {
        WeightedInput wi = null;
        Validator.validateWeightsFunction(wi);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateSummingFunctionIsNull() {
        And and = null;
        Validator.validateSummingFunction(and);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStringIsNull() {
        String toBeTested = null;
        Validator.validateString(toBeTested);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateStringIsEmpty() {
        String toBeTested = "";
        Validator.validateString(toBeTested);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateIntInRangeValueLessThanMinimum() {
        // if (valueToBeValidated < min || valueToBeValidated > max)
        int valueToBeValidated = ParamsTest.KHAMSA;
        int min = ParamsTest.DECIMAL;
        Validator
                .validateIntInRange(valueToBeValidated, min, Integer.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateFileIsNull() {
        File file = null;
        Validator.validateFile(file);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateFlatNetworkIsNull() {
        FlatNetwork flatNetwork = null;
        Validator.validateFlatNetwork(flatNetwork);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateInputAdaptorIsNull() {
        InputAdapter inputAdapter = null;
        Validator.validateInputAdapter(inputAdapter);
    }

    @Test
    public void testValidateInputAdaptor() throws SQLException {
        Connection dbConnection = null;
        try {
            System.setProperty("derby.system.home", "target");
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            dbConnection = DriverManager.getConnection("jdbc:derby:" + "target/testdb" + ";create=true");//;logDevice=target/dblogs
        } catch (SQLException ex) {
            // ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            // e.printStackTrace();
        }
        InputAdapter inputAdapter = new JDBCInputAdapter(dbConnection);
        Validator.validateInputAdapter(inputAdapter);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateOutputAdapter() {
        OutputAdapter outputAdapter = null;
        Validator.validateOutputAdapter(outputAdapter);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateProperties() {
        Properties propsToBeValidated = null;
        Validator.validateProperties(propsToBeValidated);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateLearningRule() {
        LearningRule lr = null;
        Validator.validateLearningRule(lr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateTransferFunctionIsNull() {
        Gaussian gaussian = null;
        Validator.validateTransferFunction(gaussian);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDoubleInRangeParIsGreaterThanMaximum() {
        double toBeValidated = 0.05d;
        double max = 0.045d;
        Validator.doubleInRange(toBeValidated, max, max);
    }

    // parToBeValidated <= min
    @Test(expected = IllegalArgumentException.class)
    public void testDoubleInRangeParIsSmallerThanMinimum() {
        double toBeValidated = 0.05d;
        double min = 0.055d;
        Validator.doubleInRange(toBeValidated, min, Double.MAX_VALUE);
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}
