package com.ocrix.neuroph.common;

public class ParamsTest {
    private ParamsTest() { }

    public static final double WEIGHT = 0.345678d;

    public static final double EPSILON = 0.001d;

    public static final int STR_LEN = 11;

    public static final double MIN = 0.0d;

    public static final double MAX = 100.0d;

    public static final double VP_SIGMA = 0.5d;

    public static final int DECIMAL = 10;

    public static final int UNITY = 1;

    public static final int THREE = 3;

    public static final int NIL = 0;

    public static final int MILLENIUM = 1000;

    public static final int KHAMSA = 5;

    public static final int GLYPH = 2;

    public static final String SGN = "SGN";

    public static final String EVENT_HAPPENED = "event happended";

    public static final String INIT_STATE = "init state";

    public static final String PATH_TO_SAVE_NN = "../nn/target/TEST";

    public static final String TEST_FILE = "../nn/src/test/resources/testUtil.txt";
    
    public static final String READER_FILE = "../nn/src/test/resources/reader_file.txt";
    
    public static final String TEST_OUTPUT_FILE = "../nn/src/test/resources/outputUtil.txt";

    public static final String PATH_FILE_TO_SAVE = "../nn/src/test/resources/";

    public static final double VP_MOMENTUM = 0.1d;
    
    public static final int PRICE_ENDING = 99;
}
