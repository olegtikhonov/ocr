/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.common;

/**
 * Defines a network properties.
 */
public enum NetProperty {
    /**
     * A transfer function slope.
     */
    SLOPE("transferFunction.slope"),

    /**
     * A transfer function parameter y high.
     */
    Y_HIGH("transferFunction.yHigh"),

    /**
     * A transfer function parameter y low.
     */
    Y_LOW("transferFunction.yLow"),

    /**
     * A transfer function parameter x high.
     */
    X_HIGH("transferFunction.xHigh"),

    /**
     * A transfer function parameter x low.
     */
    X_LOW("transferFunction.xLow"),

    /**
     * A neuron type.
     */
    NEURON_TYPE("neuronType"),

    /**
     * A bias.
     */
    BIAS("bias"),

    /**
     * A transfer function tag.
     */
    TRANSFER_FUNCTION("transferFunction"),

    /**
     * Defines a message 'transferFunction.leftLow'.
     */
    LEFT_LOW("transferFunction.leftLow"),

    /**
     * Defines a message 'transferFunction.leftHigh'.
     */
    LEFT_HIGH("transferFunction.leftHigh"),

    /**
     * Defines a message 'transferFunction.rightLow'.
     */
    RIGHT_LOW("transferFunction.rightLow"),

    /**
     * Defines a message 'transferFunction.rightHigh'.
     */
    RIGHT_HIGH("transferFunction.rightHigh"),

    /**
     * A weight function tag.
     */
    WEIGHTS_FUNCTION("weightsFunction"),

    /**
     * A summing function tag.
     */
    SUMMING_FUNCTION("summingFunction"),

    /**
     * Use a bias tag.
     */
    USE_BIAS("useBias"),

    /**
     * An input function tag.
     */
    INPUT_FUNCTION("inputFunction"),

    /**
     * Defines a property key 'transferFunction.sigma'.
     */
    SIGMA("transferFunction.sigma"),

    /**
     * Defines a threshold.
     */
    THRESHOLD("thresh");

    /**
     * Local variables, using as a holder.
     */
    private String value;

    /**
     * Constructs a net property enums. *
     * 
     * @param toBeSet
     *            a value to be set.
     */
    NetProperty(final String toBeSet) {
        setValue(toBeSet);
    }

    /**
     * Sets a value.
     * 
     * @param val
     *            to be set.
     */
    private void setValue(final String val) {
        this.value = val;
    }

    /**
     * Gets a value of the desired parameter.
     * 
     * @return a value of particular parameter.
     */
    public String to() {
        return this.value;
    }
}
