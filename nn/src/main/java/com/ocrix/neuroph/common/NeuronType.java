/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.common;


import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.comp.DelayedNeuron;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;

/**
 * Defines a type of {@link Neuron}. The types are:
 * <ul>
 * <li>BASIC</li>
 * <li>BIAS</li>
 * <li>COMPETITIVE</li>
 * <li>DELAYED</li>
 * <li>INPUT_OUTPUT</li>
 * <li>THRESHOULD</li>
 * </ul>
 */
public enum NeuronType {
    /**
     * @see {@link Neuron}.
     */
    BASIC,

    /**
     * @see {@link BiasNeuron}.
     */
    BIAS,

    /**
     * @see {@link CompetitiveNeuron}.
     */
    COMPETITIVE,

    /**
     * @see {@link DelayedNeuron}.
     */
    DELAYED,

    /**
     * @see {@link InputOutputNeuron}.
     */
    INPUT_OUTPUT,

    /**
     * @see {@link ThresholdNeuron}.
     */
    THRESHOULD;
}
