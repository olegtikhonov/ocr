/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.common;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_ANN_IS_NULL;
import static com.ocrix.neuroph.common.CommonMessages.INIT_SUM_FUN;
import static com.ocrix.neuroph.common.CommonMessages.INIT_WGT_FUN;
import static com.ocrix.neuroph.common.CommonMessages.PROVIDED_PARAM;
import static com.ocrix.neuroph.common.CommonMessages.SHOULD_BE_IN_RANGE;
import static com.ocrix.neuroph.common.CommonMessages.SHOULD_BE_SAME;
import static com.ocrix.neuroph.common.CommonMessages.SHOULD_NOT_BE_ZERO;
import static com.ocrix.neuroph.common.CommonMessages.SHOULD_NOT_NULL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Logger;

import org.encog.engine.network.flat.FlatNetwork;

import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.core.input.SummingFunction;
import com.ocrix.neuroph.core.input.WeightsFunction;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.core.transfer.TransferFunction;
import com.ocrix.neuroph.nnet.flat.FlatNetworkLearning;
import com.ocrix.neuroph.util.Properties;
import com.ocrix.neuroph.util.TransferFunctionType;
import com.ocrix.neuroph.util.io.InputAdapter;
import com.ocrix.neuroph.util.io.OutputAdapter;

/**
 * Validates an input values.
 */
public final class Validator {
    /* Local logger */
    private static final Logger log = Logger.getLogger(Validator.class.getName());

    /**
     * Hidden default constructor.
     */
    private Validator() {
    }

    /**
     * 
     * @param inputVector
     *            to be validated.
     */
    public static void validateDouble(final double[] inputVector)
    throws IllegalArgumentException {
        if (inputVector == null) {
            throw new IllegalArgumentException(double[].class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates two length arrays to be the same.
     * 
     * @param input
     *            - an input vector.
     * @param output
     *            - an output vector.
     */
    public static void validateLenghts(final double[] input,
            final double[] output) {
        if (input.length != output.length) {
            throw new IllegalArgumentException(SHOULD_BE_SAME.to());
        }
    }

    /**
     * Validates two dimensional array of doubles.
     * 
     * @param vectorToBeValidated
     *            - to be validated.
     */
    public static void validate2DDouble(final double[][] vectorToBeValidated) {
        if (vectorToBeValidated == null) {
            throw new IllegalArgumentException(double[].class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a double value if it is in range.
     * 
     * @param parToBeValidated
     *            - a parameter to be validated.
     * @param min
     *            - a minimum value in the range.
     * @param max
     *            - a maximim value in the range.
     */
    public static void validateDoubleInRange(final double parToBeValidated,
            final double min, final double max) {
        if (parToBeValidated <= min || parToBeValidated > max) {
            throw new IllegalArgumentException(double.class.getName()
                    + SHOULD_BE_IN_RANGE.to() + "[ " + min + ":" + max + "]");
        }
    }

    public static void validateDoubleBounds(final double parToBeValidated,
            final double min, final double max) {
        if (parToBeValidated < min && parToBeValidated > max) {
            throw new IllegalArgumentException(double.class.getName()
                    + SHOULD_BE_IN_RANGE.to() + "[ " + min + ":" + max + "]");
        }
    }

    /**
     * Validates a {@link List} to be not null.
     * 
     * @param listToBeValidated
     *            a list to be validated.
     */
    public static void validateList(final List<?> listToBeValidated) {
        if (listToBeValidated == null) {
            throw new IllegalArgumentException(List.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a weights function not being null.
     * 
     * @param weightsFunction
     *            - to be validated
     */
    public static void validateWeightsFunction(
            final WeightsFunction weightsFunction) {
        if (weightsFunction == null) {
            throw new IllegalArgumentException(INIT_WGT_FUN.to());
        }
    }

    /**
     * Validates a summing function not being null.
     * 
     * @param summingFunction
     *            to be validated
     */
    public static void validateSummingFunction(
            final SummingFunction summingFunction) {
        if (summingFunction == null) {
            throw new IllegalArgumentException(INIT_SUM_FUN.to());
        }
    }

    /**
     * Validates that {@link String} is not NULL or empty.
     * 
     * @param stringToBeValidated
     *            to be validated.
     */
    public static void validateString(final String stringToBeValidated) {
        if (stringToBeValidated == null || stringToBeValidated.length() <= 0) {
            throw new IllegalArgumentException(String.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates that {@link Integer} is in range.
     * 
     * @param valueToBeValidated
     *            - the parameter to be validated.
     * @param min
     *            - a minimum value to be validated.
     * @param max
     *            - a maximum value to be validated.
     */
    public static void validateIntInRange(final int valueToBeValidated,
            final int min, final int max) {
        if (valueToBeValidated < min || valueToBeValidated > max) {
            throw new IllegalArgumentException(SHOULD_BE_IN_RANGE.to() + "[ "
                    + min + ":" + max + "]");
        }
    }

    /**
     * Validates that a {@link File} is not
     * <code style="color:#990099">NULL</code>.
     * 
     * @param fileToBeValidated
     *            - a file to be validated.
     */
    public static void validateFile(final File fileToBeValidated) {
        if (fileToBeValidated == null) {
            throw new IllegalArgumentException(File.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a {@link NeuralNetwork} to be not
     * <code style="color:#990099">NULL</code>.
     * 
     * @param nn
     *            a {@link NeuralNetwork} to be validated.
     */
    public static void validateNeuralNetwork(final NeuralNetwork nn) {
        if (nn == null) {
            log.severe(ERROR_ANN_IS_NULL.to());
            throw new IllegalArgumentException(NeuralNetwork.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a {@link FlatNetwork}.
     * 
     * @param network
     *            - a {@link FlatNetwork} to be validated.
     */
    public static void validateFlatNetwork(FlatNetwork network) {
        if (network == null) {
            throw new IllegalArgumentException(FlatNetworkLearning.class.getName()
                    + SHOULD_NOT_NULL);
        }
    }

    /**
     * Validates an {@link InputAdapter}.
     * 
     * @param toBeValidated
     *            - an input adaptor to be validated.
     */
    public static void validateInputAdapter(final InputAdapter toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(InputAdapter.class.getName()
                    + SHOULD_NOT_NULL);
        }
    }

    /**
     * Validates an {@link OutputAdapter}.
     * 
     * @param toBeValidated
     *            - an output adapter to be validated.
     */
    public static void validateOutputAdapter(final OutputAdapter toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(OutputAdapter.class.getName()
                    + SHOULD_NOT_NULL);
        }
    }

    /**
     * Validates a neuron is not NULL.
     * 
     * @param toBeTested
     *            - a neuron to be validated.
     * @see #validateNeuron(Neuron...)
     */
    public static void validateNeuron(final Neuron toBeTested) {
        if (toBeTested == null) {
            throw new IllegalArgumentException(Neuron.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a neuron not to be NULL.
     * 
     * @param toBeTested
     *            - a neuron to be validated.
     * @see #validateNeuron(Neuron)
     */
    public static void validateNeuron(final Neuron... toBeTested) {
        for (Neuron neuron : toBeTested) {
            if (neuron == null) {
                throw new IllegalArgumentException(Neuron.class.getName()
                        + SHOULD_NOT_NULL.to());
            }
        }
    }

    /**
     * Checks the value is in range.
     * 
     * @param toBeValidated
     *            a value to be validated.
     * @param min
     *            - a left most limit.
     * @param max
     *            - a right most limit.
     */
    public static void doubleInRange(final double toBeValidated,
            final double min, final double max) {
        if (toBeValidated > max || toBeValidated < min) {
            throw new IllegalArgumentException(SHOULD_BE_IN_RANGE.to() + " ["
                    + min + ":" + max + "]");
        }
    }

    /**
     * Validates a {@link Weight}.
     * 
     * @param toBeValidated
     *            a weight to be validated.
     */
    public static void validateWeight(final Weight toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(Weight.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a {@link Properties}.
     * 
     * @param propsToBeValidated
     *            a properties to be validated.
     */
    public static void validateProperties(final Properties propsToBeValidated) throws IllegalArgumentException{
        if (propsToBeValidated == null) {
            throw new IllegalArgumentException(Properties.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates the learning rule.
     * 
     * @param learnRule
     *            a rule to be validated.
     * 
     * @throws IllegalArgumentException
     *             if rule is null.
     */
    public static void validateLearningRule(LearningRule learnRule) {
        if (learnRule == null) {
            throw new IllegalArgumentException(LearningRule.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates the training set.
     * 
     * @param trainingSetToLearn
     *            to be validated.
     * 
     * @throws IllegalArgumentException
     *             if set is null.
     */
    public static void validateTraining(TrainingSet<?> trainingSetToLearn) {
        if (trainingSetToLearn == null) {
            throw new IllegalArgumentException(TrainingSet.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates the {@link TransferFunction} not to be NULL.
     * 
     * @param toBeValidated
     *            a transfer function to be validated.
     * 
     * @throws IllegalArgumentException
     *             if the argument is NULL.
     */
    public static void validateTransferFunction(TransferFunction toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(TransferFunction.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates the {@link TransferFunctionType} is not NULL.
     * 
     * @param toBeValidated to be validated.
     * 
     * @throws IllegalArgumentException if the param is NULL.
     */
    public static void validateTransferFunctionType(TransferFunctionType toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(TransferFunctionType.class.getName()
                    + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates the given parameter is not a zero.
     * 
     * @param toBeChecked to be validated.
     * 
     * @throws IllegalArgumentException - if the argument is zero.
     */
    public static void notZero(double toBeChecked) {
        if(toBeChecked == 0.0d){
            log.severe(PROVIDED_PARAM.to() + SHOULD_NOT_BE_ZERO.to());
            throw new IllegalArgumentException(PROVIDED_PARAM.to() + SHOULD_NOT_BE_ZERO.to());
        }
    }

    /**
     * Validates a {@link Layer} not to be null.
     * 
     * @param toBeValidated a layer to be validated.
     */
    public static void validateLayer(Layer toBeValidated) {
        if (toBeValidated == null) {
            throw new IllegalArgumentException(Layer.class.getName() + SHOULD_NOT_NULL.to());
        }
    }

    /**
     * Validates a {@link Layer} not to be null.
     * 
     * @param toBeValidated the array of layers to be validated.
     */
    public static void validateLayer(Layer... toBeValidated) {
        for(Layer layer : toBeValidated){
            if (layer == null) {
                throw new IllegalArgumentException(Layer.class.getName() + SHOULD_NOT_NULL.to());
            }
        }
    }
    
    /**
     * Validates a Class<?> not to be null.
     * 
     * @throws IllegalArgumentException if a parameter is NULL.
     * 
     * @param toBeValidated
     */
    public static void validateObject(Class<?> toBeValidated){
        if (toBeValidated == null) {
            throw new IllegalArgumentException(Class.class.getName() + SHOULD_NOT_NULL.to());
        }
    }
    
    /**
     * Validates that provided parameter is in the min:max range.
     * 
     * @param toBeValidated a parameter to be validated.
     * @param min a minimal value.
     * @param max a maximum value.
     */
    public static void validateLongInRange(long toBeValidated, long min, long max){
        if(toBeValidated < min || toBeValidated > max){
            throw new IllegalArgumentException(PROVIDED_PARAM.to() + "{" + toBeValidated + "} " + SHOULD_BE_IN_RANGE.to() + " [ " + min + " : " + max + " ]");
        }
    }
    
    /**
     * Validates {@link OutputStream} not to be null.
     * 
     * @param outStream to be validated.
     * 
     * @throws IllegalArgumentException if an argument is null.
     */
    public static void validateOutputStream(OutputStream outStream){
        if(outStream == null){
            throw new IllegalArgumentException(OutputStream.class.getName() + SHOULD_NOT_NULL.to());
        }
    }
    
    /**
     * Validates a {@link BufferedWriter} not to be null.
     * 
     * @param writer to be validated if null.
     * 
     * @throws IllegalArgumentException if an argument is null.
     */
    public static void validateBufferedWroter(BufferedWriter writer){
        if(writer == null){
            throw new IllegalArgumentException(BufferedWriter.class.getName() + SHOULD_NOT_NULL.to());
        }
    }
}
