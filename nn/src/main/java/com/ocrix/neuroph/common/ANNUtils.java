package com.ocrix.neuroph.common;

import java.util.logging.Logger;

public final class ANNUtils {
    private static final Logger log = Logger.getLogger(ANNUtils.class.getName());

    private ANNUtils() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Constructs the string parsing {@link StackTraceElement}.
     * The text contains: class_name, method_name and line_number.
     * 
     * @param e - to be parsed.
     * 
     * @return a {@link String}.
     */
    public static String stackToString(Exception e){
        log.info("calling to stackToString ...");

        StringBuilder sb = new StringBuilder();
        StackTraceElement[] elems = e.getStackTrace();

        for(StackTraceElement el : elems){
            sb.append(el.getClassName());
            sb.append(" ");
            sb.append(el.getMethodName());
            sb.append(" ");
            sb.append(el.getMethodName());
            sb.append(" ");
            sb.append(el.getLineNumber());
        }
        return sb.toString();
    }

    public static String stackToString(Exception e, int depth){
        log.info("calling to stackToString ...");

        StringBuilder sb = new StringBuilder();
        StackTraceElement[] elems = e.getStackTrace();
        for(StackTraceElement el : elems){
            if(depth >= 0){
                sb.append(el.getClassName());
                sb.append(" ");
                sb.append(el.getMethodName());
                sb.append(" ");
                sb.append(el.getLineNumber());
                sb.append(" ");
            }else {
                break;
            }
            depth--;
        }

        return sb.toString();
    }
}
