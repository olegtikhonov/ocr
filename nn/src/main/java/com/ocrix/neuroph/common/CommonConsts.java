/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.common;

/**
 * Contains a common constants.
 */
public enum CommonConsts {
    /**
     * A size for allocating list.
     */
    DEFAULT_LIST_SIZE(5),

    /**
     * Defines a neuron amount.
     */
    ONE(1),

    /**
     * Defines -1.
     */
    NEG_ONE(-1),

    /**
     * Defines '2'.
     */
    TWO(2),

    /**
     * Defines '3'.
     */
    THREE(3),

    /**
     * Defines '4'.
     */
    FOUR(4),

    /**
     * Starting temperature.
     */
    START_TEMP(10),

    /**
     * Defines a cycles default value.
     */
    CYCLES(1000),
    
    /**
     * Defines a default stack depth.
     */
    DEFAULT_STACK_DEPTH(2),

    /**
     * Defines a zero.
     */
    ZERO(0);

    /**
     * A parameter value holder.
     */
    private int value;

    /**
     * Constructs common consts.
     * 
     * @param var
     *            - a parameter to be set.
     */
    private CommonConsts(final int var) {
        this.value = var;
    }

    /**
     * Gets a value of the parameter.
     * 
     * @return a value of the parameter.
     */
    public int value() {
        return this.value;
    }
}
