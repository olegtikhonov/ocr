/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.common;

/**
 * Contains a common messages.
 */
public enum CommonMessages {

    /**
     * A message containing 'should not be NULL'.
     */
    SHOULD_NOT_NULL(" should not be NULL"),

    /**
     * should be the same.
     */
    SHOULD_BE_SAME(" the length of the arrays should be the same"),

    /**
     * A message containing 'should be in range'.
     */
    SHOULD_BE_IN_RANGE(" should be in range "),

    /**
     * Defines a message 'should not be zero'.
     */
    SHOULD_NOT_BE_ZERO(" should not be zero"),

    /**
     * A message containing 'Initialize the summing function'.
     */
    INIT_SUM_FUN("Initialize the summing function"),

    /**
     * A message containing 'Initialize weights function'.
     */
    INIT_WGT_FUN("Initialize weights function"),

    /**
     * A message containing 'This learning rule only works with a network that
     * has a FlatNetworkPlugin attached.'
     */
    FLAT_NET_PLUG_SHOULD_BE_ATTACHED(
    "This learning rule only works with a network that has a FlatNetworkPlugin attached."),

    /**
     * A message containing 'Weight size mismatch.'.
     */
    WEIGHT_SIZE_MIS("Weight size mismatch."),

    /**
     * A message containing 'Error executing query at JdbcInputAdapter'.
     */
    JDBC_ERROR("Error executing query at JdbcInputAdapter"),

    /**
     * Defines the message indicating about impossibility to find a file.
     */
    FILE_NOT_FOUND("Cannot find file: "),

    /**
     * Used for toString method.
     */
    CLASS_NAME("class name="),

    /**
     * Defines a message 'method name'.
     */
    METHOD_NAME("method name="),

    /**
     * Defines a message 'line number'.
     */
    LINE_NUMBER("line number="),
    
    /**
     * Defines a message 'The provided parameter'.
     */
    PROVIDED_PARAM("The provided parameter "),

    /**
     * A message containing 'Error reading input value from the result set!'.
     */
    ERROR_READING_INPUT("Error reading input value from the result set!"),

    /**
     * A message containing 'Error closing database connection!'.
     */
    ERROR_CLOSING_DATABASE("Error closing database connection!"),

    /**
     * Defines an error 'Input vector size does not match network input
     * dimension'.
     */
    ERROR_INCORRECT_DIM(
    "Input vector size does not match network input dimension. Might be input neuron vector is NULL."),

    /**
     * Defines an error 'InstantiationException while creating Neuron!'.
     */
    ERROR_INIT_NEURON("InstantiationException while creating Neuron!"),

    /**
     * Defines the message 'No permission to invoke method while creating
     * Neuron!'.
     */
    ERROR_NEURON_NO_PERRMISSIONS(
    "No permission to invoke method while creating Neuron!"),

    /**
     * Defines the message 'couldn't find the constructor while creating
     * TransferFunction!'.
     */
    ERROR_CANT_FIND_CITOR(
    "getConstructor() couldn't find the constructor while creating TransferFunction!"),

    /**
     * Defines the message 'InstantiationException while creating
     * TransferFunction'.
     */
    ERROR_TF_CREATE("InstantiationException while creating TransferFunction"),

    /**
     * Defines the message 'InstantiationException while creating
     * SummingFunction'.
     */
    ERROR_SF_INIT("InstantiationException while creating SummingFunction!"),

    /**
     * Defines the message 'No permission to invoke method while creating
     * TransferFunction!'.
     */
    ERROR_TF_NO_PERMISSION(
    "No permission to invoke method while creating TransferFunction!"),

    /**
     * "InstantiationException while creating WeightsFunction!"
     */
    ERROR_INIT_CRTNG_WF(
    "InstantiationException while creating WeightsFunction!"),

    /**
     * Defines a 'The list of neurons is NULL. The following actions will be
     * taken: i. Allocate memory; ii. Add new default neuron.' message.
     */
    ERROR_NEURON_LIST_NULL(
    "The list of neurons is NULL. The following actions will be taken: i. Allocates memory; ii. Adds new default neuron."),

    /**
     * Defines an 'Invalid transfer function properties! Using default values.'
     */
    ERROR_INVALID_TRANSFER_FUNCTION_PROPS(
    "Invalid transfer function properties! Using default values."),

    /**
     * Defines partial message 'Method threw an: '.
     */
    PART_MSG_METHOD_THREW("Method threw an: "),

    /**
     * Defines partial message 'while creating Neuron!'.
     */
    PART_MSG_WHILE_CRTNG_NRN(" while creating Neuron!"),

    /**
     * Defines partial message ' while creating TransferFunction!'.
     */
    PART_MSG_CRTNG_TF(" while creating TransferFunction!"),

    /**
     * Defines the error msg 'Method (updateNetworkWeights) is unimplemented and should not have been called'.
     */
    ERROR_UPDATE_NETWORK_WEIGHTS("Method (updateNetworkWeights) is unimplemented and should not have been called."),

    /**
     * Defines the error msg 'Not supported, and should not be called'.
     */
    ERROR_NOT_SUPPORTED("Not supported, and should not be called."),

    /**
     * Defines an error message 'Make sure that a neural network is not NULL'.
     */
    ERROR_ANN_IS_NULL("Make sure that a neural network is not NULL"),

    /**
     * Defines a warning message 'The neural network is NULL'.
     */
    WARN_ANN_IS_NULL("The neural network is NULL"),

    /**
     * Defines a warning message 'Winning neuron is NULL'.
     */
    WARN_WINNING_NULL("Winning neuron is NULL"),

    /**
     * Defines a warning msg 'Training element is NULL'.
     */
    WARN_TRAIN_ELEM_NULL("Training element is NULL"),
    
    /**
     * Defines a warning 'Please check out why the input stream seems to be NULL'.
     */
    WARN_INPUT_STREAM_IS_NULL("Please check out why the input stream seems to be NULL"),
    
    /**
     * Defines a warning 'Please check out why the input stream seems to be NULL'.
     */
    WARN_BUFFERED_READER_IS_NULL("Please check out why the buffered reader seems to be NULL"),

    /**
     * Defines a msg 'Desired output is NULL',
     */
    WARN_DESIRED_OUT_NULL("Desired output is NULL"),

    /**
     * Defines a msg 'Neuron is NULL',
     */
    WARN_NEURON_NULL("Neuron is NULL"),
    
    /**
     * Defines a warning message 'Training set is NULL'.
     */
    WARN_TRAINNING_SET_NULL("Training set is NULL"),
    
    /**
     * Defines a warning message 'Provided training set is not supervised'.
     */
    WARN_TS_IS_NOT_SUPERVISED("Provided training set is not supervised"),
    
    /**
     * Defines a message 'Could not parse the given stream, because of buffered reader seems to be NULL'. 
     */
    WARN_CANNOT_PARSE("Could not parse the given stream, because of buffered reader seems to be NULL"),
    
    /**
     * Defines a message 'Nothing has written, because the provided double[] is null or empty'.
     */
    WARN_DOUBLE_ARR_NULL_EMPTY("Nothing has written, because the provided double[] is null or empty"),
    
    /**
     * Defines a warning message 'Error reading input from stream!'.
     */
    ERROR_READ_INPUT("Error reading input from stream!"),
    
    /**
     * Defines a warning message 'Error closing stream!'.
     */
    ERROR_CLOSE_STREAM("Error closing stream!"),
    
    /**
     * Defines a message 'Error writing output to stream!'.
     */
    ERROR_WRITE_OUTPUT_STREAM("Error writing output to stream!"),
    
    /**
     * Defines a message 'Error closing output stream!';
     */
    ERROR_CLOSE_OUTPUT_STREAM("Error closing output stream!"),
    
    /**
     * Defines a hint 'May be the sql statement is null or incorrect'.
     */
    HINT_JDBC_ERROR(" May be the sql statement is null or incorrect."),

    /**
     * Defines a log level property key.
     */
    LOG_LEVEL_PROPERTY("nn.log.level");

    /* Value place holder */
    private String message;

    /**
     * Constructs this enum.
     * 
     * @param msg
     */
    private CommonMessages(String msg) {
        this.message = msg;
    }

    /**
     * Gets a textual value of the enum.
     * 
     * @return
     */
    public String to() {
        return this.message;
    }
}
