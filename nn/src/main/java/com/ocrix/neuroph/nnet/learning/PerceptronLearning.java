/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;


import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;

/**
 * Perceptron learning rule for perceptron neural networks.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class PerceptronLearning extends LMS {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    /* For local purposes only - cast Neuron to ThresholdNeuron, uses as container, initialized just once */
    private ThresholdNeuron tNeuron = null;


    /**
     * Creates new PerceptronLearning instance
     */
    public PerceptronLearning() {
        super();
        tNeuron = new ThresholdNeuron(new InputFunction(), new Sigmoid());
    }

    /**
     * This method implements weights update procedure for the single neuron In
     * addition to weights change in LMS it applies change to neuron's threshold.
     * 
     * @param neuron
     *            neuron to update weights.
     */
    @Override
    protected void updateNeuronWeights(Neuron neuron) {
        if(neuron != null){
            /* Adjusts the input connection weights with method from superclass */
            super.updateNeuronWeights(neuron);

            /* Adjusts the neuron's threshold */
            //            ThresholdNeuron thresholdNeuron = (ThresholdNeuron) neuron;
            ThresholdNeuron thresholdNeuron = neuronToThresholdNeuron(neuron);
            /* Gets neuron's error */
            double neuronError = thresholdNeuron.getError();
            /* Gets the neuron's threshold */
            double thresh = thresholdNeuron.getThresh();
            /* Calculates the new threshold value */
            thresh = thresh - this.learningRate * neuronError;
            /* Applies the new threshold */
            thresholdNeuron.setThresh(thresh);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }

    /**
     * Converts {@link Neuron} to {@link ThresholdNeuron}.
     * 
     * @param neuron to be converted.
     * 
     * @return a threshold neuron.
     */
    private ThresholdNeuron neuronToThresholdNeuron(Neuron neuron){
        tNeuron.setInputFunction(neuron.getInputFunction());
        tNeuron.setTransferFunction(neuron.getTransferFunction());
        tNeuron.setError(neuron.getError());
        return tNeuron;
    }
}