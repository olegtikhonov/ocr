/**
 * Provides implementations of specific neural network learning algorithms.
 */

package com.ocrix.neuroph.nnet.learning;

