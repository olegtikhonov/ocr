/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.CommonMessages.WARN_DESIRED_OUT_NULL;
import static com.ocrix.neuroph.common.CommonMessages.WARN_TRAIN_ELEM_NULL;

import java.util.logging.Level;
import java.util.logging.Logger;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;

/**
 * Supervised hebbian learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class SupervisedHebbianLearning extends LMS {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(SupervisedHebbianLearning.class.getName());


    /**
     * Creates new instance of SupervisedHebbianLearning algorithm
     */
    public SupervisedHebbianLearning() {
        super();
    }

    /**
     * Learn method override without network error and iteration limit
     * Implements just one pass through the training set Used for testing -
     * debugging algorithm
     * 
     * public void learn(TrainingSet trainingSet) { Iterator
     * iterator=trainingSet.iterator(); while(iterator.hasNext()) {
     * SupervisedTrainingElement trainingElement =
     * (SupervisedTrainingElement)iterator.next();
     * this.learnPattern(trainingElement); } }
     */

    /**
     * Trains network with the pattern from the specified training element
     * 
     * @param trainingElement
     *            supervised training element which contains input and desired
     *            output
     */
    @Override
    protected void learnPattern(SupervisedTrainingElement trainingElement) {
        if(trainingElement != null){
            /* Not sure if I need it here */
            Validator.validateNeuralNetwork(getNeuralNetwork());
            double[] input = trainingElement.getInput();
            this.neuralNetwork.setInput(input);
            this.neuralNetwork.calculate();
            double[] output = this.neuralNetwork.getOutput();
            double[] desiredOutput = trainingElement.getDesiredOutput();
            double[] outputError = this.calculateOutputError(desiredOutput, output);
            this.addToSquaredErrorSum(outputError);
            this.updateNetworkWeights(desiredOutput);
        }else{
            log.warning(WARN_TRAIN_ELEM_NULL.to());
        }
    }

    /**
     * This method implements weight update procedure for the whole network for
     * this learning rule
     * 
     * @param desiredOutput
     *            desired network output
     */
    @Override
    protected void updateNetworkWeights(double[] desiredOutput) {
        if(desiredOutput != null){
            int i = 0;
            for (Neuron neuron : neuralNetwork.getOutputNeurons()) {
                this.updateNeuronWeights(neuron, desiredOutput[i]);
                i++;
            }
        }else {
            log.warning(WARN_DESIRED_OUT_NULL.to());
        }
    }

    /**
     * This method implements weights update procedure for the single neuron.
     * 
     * @param neuron - neuron to update weights desiredOutput desired output of the neuron.
     */
    protected void updateNeuronWeights(Neuron neuron, double desiredOutput) {
        for (Connection connection : neuron.getInputConnections()) {
            double input = connection.getInput();
            double deltaWeight = input * desiredOutput * this.learningRate;
            connection.getWeight().inc(deltaWeight);
        }

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }

    public void logOn(){
        log.setLevel(Level.ALL);
    }

    public void logOff() {
        log.setLevel(Level.OFF);
    }
}
