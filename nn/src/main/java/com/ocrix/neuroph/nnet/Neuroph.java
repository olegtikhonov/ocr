/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet;

import org.encog.engine.EncogEngine;

/**
 * This singleton holds global settings for the whole framework
 * 
 * @author Jeff Heaton
 */
public class Neuroph {

    private static Neuroph instance;

    /**
     * Flag to determine if flat network support from Encog is turned on
     */
    private boolean flattenNetworks = false;

    public static Neuroph getInstance() {
        if (instance == null)
            instance = new Neuroph();
        return instance;
    }

    /**
     * Get setting for flatten network (from Encog engine)
     * 
     * @return the flattenNetworks
     */
    public boolean shouldFlattenNetworks() {
        return flattenNetworks;
    }

    /**
     * Turn on/off flat networ support from Encog
     * 
     * @param flattenNetworks
     *            the flattenNetworks to set
     */
    public void setFlattenNetworks(boolean flattenNetworks) {
        this.flattenNetworks = flattenNetworks;
    }

    /**
     * Shuts down the Encog engine
     */
    public void shutdown() {
        EncogEngine.getInstance().shutdown();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [flatten networks=");
        builder.append(flattenNetworks);
        builder.append("]");
        return builder.toString();
    }


}
