/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;

/**
 * Oja learning rule which is a modification of unsupervised Hebbian learning. <br>
 * <code style="color:PURPLE">deltaWeight = (input - output * weight) * output * learningRate</code>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class OjaLearning extends UnsupervisedHebbianLearning {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of OjaLearning algorithm.
     */
    public OjaLearning() {
        super();
    }

    /**
     * This method implements weights update procedure for the single neuron.
     * 
     * @param neuron
     *            neuron to update weights.
     */
    @Override
    protected void updateNeuronWeights(Neuron neuron) {
        if (neuron != null) {
            double output = neuron.getOutput();
            for (Connection connection : neuron.getInputConnections()) {
                double input = connection.getInput();
                double weight = connection.getWeight().getValue();
                double deltaWeight = (input - output * weight) * output
                * this.learningRate;
                connection.getWeight().inc(deltaWeight);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }
}
