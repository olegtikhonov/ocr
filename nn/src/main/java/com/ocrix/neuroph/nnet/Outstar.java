/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.learning.OutstarLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Outstar neural network with Outstar learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Outstar extends NeuralNetwork {

    /* The class fingerprint that is set to indicate serialization compatibility with a previous version of the class. */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of Outstar network with specified number of neurons
     * in output layer.
     * 
     * @param outputNeuronsCount
     *            number of neurons in output layer
     */
    public Outstar(int outputNeuronsCount) {
        this.createNetwork(outputNeuronsCount);
    }

    /**
     * Creates Outstar architecture with specified number of neurons in output
     * layer.
     * 
     * @param outputNeuronsCount
     *            number of neurons in output layer
     */
    private void createNetwork(int outputNeuronsCount) {

        /* Sets network type */
        this.setNetworkType(NeuralNetworkType.OUTSTAR);

        /* Inits neuron settings for this type of network */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(),
                TransferFunctionType.STEP);

        // create input layer
        Layer inputLayer = LayerFactory.createLayer(1, neuronProperties);
        this.addLayer(inputLayer);

        // createLayer output layer
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(),
                TransferFunctionType.RAMP);
        Layer outputLayer = LayerFactory.createLayer(outputNeuronsCount,
                neuronProperties);
        this.addLayer(outputLayer);

        // create full conectivity between input and output layer
        ConnectionFactory.fullConnect(inputLayer, outputLayer);

        // set input and output cells for this network
        NeuralNetworkFactory.setDefaultIO(this);

        // set outstar learning rule for this network
        this.setLearningRule(new OutstarLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }
        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }
        builder.append("]");
        return builder.toString();
    }



}
