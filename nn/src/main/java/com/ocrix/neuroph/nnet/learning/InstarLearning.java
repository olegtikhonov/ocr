/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;

/**
 * Hebbian-like learning rule for Instar network.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class InstarLearning extends UnsupervisedHebbianLearning {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new instance of InstarLearning algorithm. Sets learning rate to
     * <code>0.1</code>.
     */
    public InstarLearning() {
        super();
        this.setLearningRate(0.1);
    }

    /**
     * This method implements weights update procedure for the single neuron.
     * 
     * @param neuron
     *            - the neuron to update weights for.
     */
    @Override
    protected void updateNeuronWeights(Neuron neuron) {
        double output = neuron.getOutput();
        for (Connection connection : neuron.getInputConnections()) {
            double input = connection.getInput();
            double weight = connection.getWeight().getValue();
            double deltaWeight = this.learningRate * output * (input - weight);
            connection.getWeight().inc(deltaWeight);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [learningRate=");
        builder.append(learningRate);
        builder.append("]");
        return builder.toString();
    }

}
