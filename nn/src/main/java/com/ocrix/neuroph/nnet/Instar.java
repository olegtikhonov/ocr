/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.util.NeuralNetworkType.INSTAR;
import static com.ocrix.neuroph.util.TransferFunctionType.STEP;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.learning.InstarLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * Instar neural network with Instar learning rule. This network can learn to
 * recognize (rather than reproduce) any arbitrary spatial pattern. The dual of
 * the outstar termed the instar and is the minimalnetwork capable of learning
 * to chunk any pattern of activity across a filed of cells. Stabilizing instar
 * learning by using feedback.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Instar extends NeuralNetwork {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new Instar with specified number of input neurons.
     * 
     * @param inputNeuronsCount
     *            - number of neurons in input layer
     */
    public Instar(int inputNeuronsCount) {
        this.createNetwork(inputNeuronsCount);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Instar [");
        builder.append("network type=" + INSTAR);
        builder.append(", ");

        if (getInputNeurons() != null) {
            builder.append("input neurons=");
            builder.append(getInputNeurons().size());
            builder.append(", ");
        }
        if (getOutputNeurons() != null) {
            builder.append("output neurons=");
            builder.append(getOutputNeurons().size());
            builder.append(", ");
        }

        builder.append("learning rule=");
        builder.append(STEP);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Creates Instar architecture with specified number of input neurons
     * 
     * @param inputNeuronsCount
     *            - number of neurons in input layer
     */
    private void createNetwork(int inputNeuronsCount) {

        /* Sets network type */
        this.setNetworkType(INSTAR);

        /* Inits neuron settings for this type of network */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);

        /* Creates input layer */
        Layer inputLayer = LayerFactory.createLayer(inputNeuronsCount,
                neuronProperties);
        this.addLayer(inputLayer);

        /* Creates output layer */
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        Layer outputLayer = LayerFactory.createLayer(1, neuronProperties);
        this.addLayer(outputLayer);

        /* Creates full connectivity between input and output layer */
        ConnectionFactory.fullConnect(inputLayer, outputLayer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets appropriate learning rule for this network */
        this.setLearningRule(new InstarLearning());
    }
}
