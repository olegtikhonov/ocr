/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import java.util.logging.Logger;


import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.common.CommonConsts;
import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;

/**
 * Delta rule learning algorithm for perceptrons with step functions.
 * 
 * The difference to Perceptronlearning is that Delta Rule calculates error
 * before the non-lnear step transfer function
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class BinaryDeltaRule extends PerceptronLearning {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /* The errorCorrection parameter of this learning algorithm. */
    private double errorCorrection = 0.1;

    private final static Logger log = Logger.getLogger(BinaryDeltaRule.class
            .getName());

    /**
     * Creates new BinaryDeltaRule learning
     */
    public BinaryDeltaRule() {
        super();
    }

    /**
     * This method implements weight update procedure for the whole network for
     * this learning rule.
     * 
     * @param patternError
     *            - the single pattern error vector.
     * 
     *            if the output is <code>0</code> and required value is
     *            <code>1</code>, increase the weights if the output is 1 and
     *            required value is 0, decrease the weights otherwise leave
     *            weights unchanged.
     * 
     * @throws IllegalArgumentException if patternError is <code>null</code> or neuralNetwork is <code>null</code>.
     */
    @Override
    protected void updateNetworkWeights(double[] patternError) {
        Validator.validateDouble(patternError);
        Validator.validateNeuralNetwork(neuralNetwork);
        int i = 0;
        for (Neuron outputNeuron : neuralNetwork.getOutputNeurons()) {
            try {
                Neuron neuron = outputNeuron;
                double outputError = patternError[i];
                double thresh = ((ThresholdNeuron) neuron).getThresh();
                double netInput = neuron.getNetInput();
                double threshError = thresh - netInput; // distance from zero
                // use output error to decide weathet to inrease, decrase or
                // leave
                // unchanged weights
                // add errorCorrection to threshError to move above or below
                // zero
                double neuronError = outputError * (Math.abs(threshError) + errorCorrection);

                /*
                 * use same adjustment principle as PerceptronLearning, just
                 * with different neuronError
                 */
                neuron.setError(neuronError);
                updateNeuronWeights(neuron);

                i++;
            } catch (ClassCastException e) {
                log.severe(ANNUtils.stackToString(e, CommonConsts.TWO.value()));
            }

        } /* for closing */
    }

    /**
     * Gets the errorCorrection parameter.
     * 
     * @return errorCorrection parameter.
     */
    public double getErrorCorrection() {
        return this.errorCorrection;
    }

    /**
     * Sets the errorCorrection parameter.
     * 
     * @param errorCorrection
     *            - the value for errorCorrection parameter.
     */
    public void setErrorCorrection(double errorCorrection) {
        this.errorCorrection = errorCorrection;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [errorCorrection=");
        builder.append(errorCorrection);
        builder.append("]");
        return builder.toString();
    }
}