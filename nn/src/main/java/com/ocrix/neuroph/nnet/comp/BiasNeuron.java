/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.comp;

import static com.ocrix.neuroph.common.CommonConsts.ONE;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;

/**
 * Neuron with constant high output (1), used as bias. The bias neuron lies in
 * one layer, is connected to all the neurons in the next layer, but none in the
 * previous layer and it always emits 1. Since the bias neuron emits 1 the
 * weights, connected to the bias neuron, are added directly to the combined sum
 * of the other weights.
 * 
 * @see Neuron
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class BiasNeuron extends Neuron {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of BiasedNeuron.
     */
    public BiasNeuron() {
        super();
    }

    @Override
    public double getOutput() {
        return ONE.value();
    }

    @Override
    public void addInputConnection(Connection connection) {
        super.addInputConnection(connection);
    }

    @Override
    public void addInputConnection(Neuron fromNeuron, double weightVal) {
        super.addInputConnection(fromNeuron, weightVal);
    }

    @Override
    public void addInputConnection(Neuron fromNeuron) {
        super.addInputConnection(fromNeuron);
    }

    /**
     * Clears the input connections.
     */
    public void removeInputConnections() {
        super.getInputConnections().clear();
    }
}
