/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.CommonConsts.FOUR;
import static com.ocrix.neuroph.common.CommonConsts.ONE;
import static com.ocrix.neuroph.common.CommonConsts.THREE;
import static com.ocrix.neuroph.common.CommonConsts.TWO;
import static com.ocrix.neuroph.common.CommonConsts.ZERO;
import static com.ocrix.neuroph.common.NetProperty.SUMMING_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.util.NeuralNetworkType.NEURO_FUZZY_REASONER;
import static com.ocrix.neuroph.util.SummingFunctionType.MIN;
import static com.ocrix.neuroph.util.TransferFunctionType.LINEAR;
import static com.ocrix.neuroph.util.TransferFunctionType.STEP;
import static com.ocrix.neuroph.util.TransferFunctionType.TRAPEZOID;
import static com.ocrix.neuroph.util.WeightsFunctionType.WEIGHTED_INPUT;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.transfer.Trapezoid;
import com.ocrix.neuroph.nnet.learning.LMS;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * The NeuroFuzzyReasoner class represents Neuro Fuzzy Reasoner architecture.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class NeuroFuzzyPerceptron extends NeuralNetwork {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a {@link NeuroFuzzyPerceptron}.
     * 
     * @param pointsSets
     *            - 2D set of points.
     * @param timeSets
     *            - 2D set of time sets.
     */
    public NeuroFuzzyPerceptron(double[][] pointsSets, double[][] timeSets) {
        Vector<Integer> inputSets = new Vector<Integer>();
        inputSets.addElement(new Integer(FOUR.value()));
        inputSets.addElement(new Integer(THREE.value()));

        this.createStudentNFR(TWO.value(), inputSets, FOUR.value(), pointsSets,
                timeSets);
    }

    public NeuroFuzzyPerceptron(int inputNum, Vector<Integer> inputSets, int outNum) {
        this.createNetwork(inputNum, inputSets, outNum);
    }

    /**
     * Builds the example network for student classification.
     * 
     * @param inputNum
     * @param inputSets
     * @param outNum
     * @param pointsSets
     * @param timeSets
     */
    private void createStudentNFR(int inputNum, Vector<Integer> inputSets,
            int outNum, double[][] pointsSets, double[][] timeSets) {

        /* Validates gotten parameters */
        Validator.validate2DDouble(pointsSets);
        Validator.validate2DDouble(timeSets);

        /* Sets network type */
        this.setNetworkType(NEURO_FUZZY_REASONER);

        /* Creates the input layer */
        NeuronProperties neuronProperties = new NeuronProperties();
        Layer inLayer = LayerFactory.createLayer(new Integer(inputNum),
                neuronProperties);
        this.addLayer(inLayer);

        /* Creates the fuzzy set layer */
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TRAPEZOID);
        Enumeration<Integer> e = inputSets.elements();
        int fuzzySetsNum = 0;
        while (e.hasMoreElements()) {
            Integer i = e.nextElement();
            fuzzySetsNum = fuzzySetsNum + i.intValue();
        }
        Layer setLayer = LayerFactory.createLayer(fuzzySetsNum,
                neuronProperties);
        this.addLayer(setLayer);

        /* <TRANSLATED> */
        // TODO: Set the parameters of membership function arrays with elements,
        // bring it out of class and transmits them as parameters.
        Iterator<Neuron> ii = setLayer.getNeuronsIterator();
        Enumeration<Integer> en;
        int c = 0;
        while (ii.hasNext()) {
            Neuron cell = ii.next();
            Trapezoid tf = (Trapezoid) cell.getTransferFunction();

            if (c <= 3) {
                tf.setLeftLow(pointsSets[c][ZERO.value()]);
                tf.setLeftHigh(pointsSets[c][ONE.value()]);
                tf.setRightHigh(pointsSets[c][TWO.value()]);
                tf.setRightLow(pointsSets[c][THREE.value()]);

            } else {
                tf.setLeftLow(timeSets[c - FOUR.value()][ZERO.value()]);
                tf.setLeftHigh(timeSets[c - FOUR.value()][ONE.value()]);
                tf.setRightLow(timeSets[c - FOUR.value()][THREE.value()]);
                tf.setRightHigh(timeSets[c - FOUR.value()][TWO.value()]);
            }
            c++;
        }

        /* <TRANSLATED> - Links the first and second layer */
        int s = 0; /* <TRANSLATED> - counter of cell layer sets (Fuzzification) */
        for (int i = 0; i < inputNum; i++) { /*
         * <TRANSLATED> - counter of the
         * input cells
         */
            Neuron from = inLayer.getNeuronAt(i);
            int jmax = inputSets.elementAt(i).intValue();
            for (int j = 0; j < jmax; j++) {
                Neuron to = setLayer.getNeuronAt(s);
                ConnectionFactory.createConnection(from, to, 1);
                s++;
            }
        }

        // ----------------------------------------------------------

        /* Creates the rules layer */
        NeuronProperties ruleNeuronProperties = new NeuronProperties(
                WEIGHTED_INPUT, MIN, LINEAR);
        en = inputSets.elements();
        int fuzzyAntNum = 1;
        while (en.hasMoreElements()) {
            Integer i = en.nextElement();
            fuzzyAntNum = fuzzyAntNum * i.intValue();
        }

        Layer ruleLayer = LayerFactory.createLayer(fuzzyAntNum,
                ruleNeuronProperties);

        this.addLayer(ruleLayer);

        /* Sets cell index */
        int scIdx = 0;
        /* <TRANSLATED> - counter inputs (a group of fuzzy sets) */
        for (int i = 0; i < inputNum; i++) {
            int setsNum = inputSets.elementAt(i).intValue();
            /* <TRANSLATED> - cell count of fuzzy sets */
            for (int si = 0; si < setsNum; si++) {
                if (i == 0) {
                    Neuron from = setLayer.getNeuronAt(si);
                    int connPerCell = fuzzyAntNum / setsNum;
                    scIdx = si;
                    /* <TRANSLATED> - counter cell hypothesis */
                    for (int k = 0; k < connPerCell; k++) {
                        Neuron to = ruleLayer.getNeuronAt(si * connPerCell + k);
                        ConnectionFactory.createConnection(from, to,
                                new Double(ONE.value()));
                    } // for
                } // if
                else {
                    scIdx++;
                    Neuron from = setLayer.getNeuronAt(scIdx);
                    int connPerCell = fuzzyAntNum / setsNum;
                    /* <TRANSLATED> - counter cell hypothesis */
                    for (int k = 0; k < connPerCell; k++) {
                        int toIdx = si + k * setsNum;
                        Neuron to = ruleLayer.getNeuronAt(toIdx);
                        ConnectionFactory.createConnection(from, to,
                                new Double(ONE.value()));
                    } // for k
                } // else
            } // for si
        } // for i

        /* <TRANSLATED> - generates the output layer */
        neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        Layer outLayer = LayerFactory.createLayer(new Integer(outNum),
                neuronProperties);
        this.addLayer(outLayer);

        ConnectionFactory.fullConnect(ruleLayer, outLayer);

        /* <TRANSLATED> - initializes the input and output cells */
        NeuralNetworkFactory.setDefaultIO(this);

        this.setLearningRule(new LMS());
    }

    /**
     * Creates custom NFR architecture.
     * 
     * @param inputNum
     *            - number of getInputsIterator.
     * @param inputSets
     *            - input fuzzy sets.
     * @param outNum
     *            - number of outputs.
     */
    private void createNetwork(int inputNum, Vector<Integer> inputSets, int outNum) {
        /* Validates input sets */
        Validator.validateList(inputSets);

        /* Sets network type */
        this.setNetworkType(NEURO_FUZZY_REASONER);

        /* Creates the input layer */
        NeuronProperties neuronProperties = new NeuronProperties();
        Layer inLayer = LayerFactory.createLayer(new Integer(inputNum),
                neuronProperties);
        this.addLayer(inLayer);

        /* Creates the fuzzy set layer */
        neuronProperties.setProperty(SUMMING_FUNCTION.to(), TRAPEZOID);
        Enumeration<Integer> e = inputSets.elements();
        int fuzzySetsNum = 0;
        while (e.hasMoreElements()) {
            Integer i = e.nextElement();
            fuzzySetsNum = fuzzySetsNum + i.intValue();
        }
        Layer setLayer = LayerFactory.createLayer(new Integer(fuzzySetsNum),
                neuronProperties);
        this.addLayer(setLayer);

        /*
         * TODO: <TRANSLATED> - Set the parameters of membership function arrays
         * with elements of the training, outside of class and transmits them as
         * parameters
         */
        // Iterator<Neuron> ii = setLayer.getNeuronsIterator();
        Enumeration<Integer> en;// =setLayer.neurons();
        // int c = 0;
        // while (ii.hasNext()) {
        // Neuron cell = ii.next();
        // Trapezoid tf = (Trapezoid) cell.getTransferFunction();
        /*
         * if (c<=3) { tf.setLeftLow(pointsSets[c][0]);
         * tf.setLeftHigh(pointsSets[c][1]); tf.setRightLow(pointsSets[c][3]);
         * tf.setRightHigh(pointsSets[c][2]); } else {
         * tf.setLeftLow(timeSets[c-4][0]); tf.setLeftHigh(timeSets[c-4][1]);
         * tf.setRightLow(timeSets[c-4][3]); tf.setRightHigh(timeSets[c-4][2]);
         * } c++;
         */
        // }

        /* Creates connections between input and fuzzy set getLayersIterator */
        int s = 0; /* <TRANSLATED> - counter cell layer sets (Fuzzification) */
        for (int i = 0; i < inputNum; i++) { /*
         * <TRANSLATED> - counter input
         * cells
         */
            Neuron from = inLayer.getNeuronAt(i);
            int jmax = inputSets.elementAt(i).intValue();
            for (int j = 0; j < jmax; j++) {
                Neuron to = setLayer.getNeuronAt(s);
                ConnectionFactory.createConnection(from, to, new Double(ONE.value()));
                s++;
            }
        }
        // ----------------------------------------------------------

        // kreiraj sloj pravila
        neuronProperties.setProperty(SUMMING_FUNCTION.to(), MIN);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), LINEAR);
        en = inputSets.elements();
        int fuzzyAntNum = 1;
        while (en.hasMoreElements()) {
            Integer i = en.nextElement();
            fuzzyAntNum = fuzzyAntNum * i.intValue();
        }
        Layer ruleLayer = LayerFactory.createLayer(new Integer(fuzzyAntNum),
                neuronProperties);
        this.addLayer(ruleLayer);

        /* Links the rule layer */

        int scIdx = 0; /* Sets the cell index */

        for (int i = 0; i < inputNum; i++) { /*
         * <TRANSLATED> - counter inputs (a
         * group of fuzzy sets)
         */
            int setsNum = inputSets.elementAt(i).intValue();

            for (int si = 0; si < setsNum; si++) { /*
             * <TRANSLATED> - cell count
             * of fuzzy sets
             */
                if (i == 0) {
                    Neuron from = setLayer.getNeuronAt(si);
                    int connPerCell = fuzzyAntNum / setsNum;
                    scIdx = si;

                    for (int k = 0; k < connPerCell; k++) { /*
                     * <TRANSLATED> -
                     * counter cell
                     * hypothesis
                     */
                        Neuron to = ruleLayer.getNeuronAt(si * connPerCell + k);
                        ConnectionFactory.createConnection(from, to,
                                new Double(ONE.value()));
                    } // for
                } // if
                else {
                    scIdx++;
                    Neuron from = setLayer.getNeuronAt(scIdx);
                    int connPerCell = fuzzyAntNum / setsNum;

                    for (int k = 0; k < connPerCell; k++) { /*
                     * <TRANSLATED> -
                     * counter cell
                     * hypothesis
                     */
                        int toIdx = si + k * setsNum;
                        Neuron to = ruleLayer.getNeuronAt(toIdx);
                        ConnectionFactory.createConnection(from, to,
                                new Double(ONE.value()));
                    } // for k
                } // else
            } // for si
        } // for i

        /* Sets input and output cells for this network */
        neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        Layer outLayer = LayerFactory.createLayer(new Integer(outNum),
                neuronProperties);
        this.addLayer(outLayer);

        ConnectionFactory.fullConnect(ruleLayer, outLayer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        this.setLearningRule(new LMS());
    }

}
