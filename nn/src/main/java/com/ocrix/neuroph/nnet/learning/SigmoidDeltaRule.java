/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.transfer.TransferFunction;

/**
 * Delta rule learning algorithm for perceptrons with sigmoid (or any other
 * differentiable continuous) functions.
 * 
 * TODO: Rename to DeltaRuleContinuous (ContinuousDeltaRule) or something like
 * that, but that will break backward compatibility, possibly with
 * backpropagation which is the most used.
 * <ul>
 * Applies the following rule for each training row:
 * <li>DW = h(error)*(input activations)</li>
 * <li>DW = h * (target - input) * input^T</li>
 * <li>Auto-association DW = h * (x - W.x) * x^T</li>
 * <li>Hetero-association DW = h * (y - W.x) * xT</li>
 * </ul>
 * 
 * @see LMS
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class SigmoidDeltaRule extends LMS {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new SigmoidDeltaRule.
     */
    public SigmoidDeltaRule() {
        super();
    }

    /**
     * This method implements weight update procedure for the whole network for
     * this learning rule.
     * 
     * @param outputError
     *            output error vector.
     */
    @Override
    protected void updateNetworkWeights(double[] outputError) {
        this.calculateErrorAndUpdateOutputNeurons(outputError);
    }

    /**
     * This method implements weights update procedure for the output neurons
     * Calculates delta/error and calls updateNeuronWeights to update neuron's
     * weights for each output neuron
     * 
     * @param outputError
     *            error vector for output neurons
     */
    protected void calculateErrorAndUpdateOutputNeurons(double[] outputError) {
        /* Validates output error array */
        Validator.validateDouble(outputError);

        /* Validates neural network */
        Validator.validateNeuralNetwork(getNeuralNetwork());

        Validator.validateDoubleBounds(outputError.length, neuralNetwork.getOutputNeurons().size(), neuralNetwork.getOutputNeurons().size());

        int i = 0;
        /* Iterates over all output neurons */
        for (Neuron neuron : neuralNetwork.getOutputNeurons()) {
            /* if error is zero, just sets zero error and continues to next neuron */
            if (outputError[i] == 0) {
                neuron.setError(0);
                i++;
                continue;
            }

            /* Otherwise calculates and sets error/delta for the current neuron */
            TransferFunction transferFunction = neuron.getTransferFunction();
            double neuronInput = neuron.getNetInput();
            /* delta = (d-y)*df(net) */
            double delta = outputError[i] * transferFunction.getDerivative(neuronInput);
            neuron.setError(delta);

            /* updates weights of the current neuron */
            this.updateNeuronWeights(neuron);
            i++;
        } // for
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }
}
