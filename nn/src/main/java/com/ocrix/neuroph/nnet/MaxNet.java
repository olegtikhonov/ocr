/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Max Net neural network with competitive learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class MaxNet extends NeuralNetwork {

    /* The class fingerprint that is set to indicate serialization compatibility with a previous version of the class. */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new Maxnet network with specified neuron number.
     * 
     * @param neuronsCount
     *            number of neurons in MaxNet network (same number in input and output layer).
     */
    public MaxNet(int neuronsCount) {
        this.createNetwork(neuronsCount);
    }

    /**
     * Creates MaxNet network architecture.
     * 
     * @param neuronNum
     *            neuron number in network.
     * @param neuronProperties
     *            neuron properties.
     */
    private void createNetwork(int neuronsCount) {
        /* Validates neurons count not to be zero */
        Validator.notZero(neuronsCount);

        /* Sets network type */
        this.setNetworkType(NeuralNetworkType.MAXNET);

        /* Creates input layer */
        Layer inputLayer = LayerFactory.createLayer(neuronsCount, new NeuronProperties());
        this.addLayer(inputLayer);

        /* Creates properties for neurons in output layer */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(NEURON_TYPE.to(), CompetitiveNeuron.class);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.RAMP);

        /* Creates full connectivity in competitive layer */
        CompetitiveLayer competitiveLayer = new CompetitiveLayer(neuronsCount, neuronProperties);

        /* Adds competitive layer to network */
        this.addLayer(competitiveLayer);

        double competitiveWeight = -(1 / (double) neuronsCount);
        /* Creates full connectivity within competitive layer */
        ConnectionFactory.fullConnect(competitiveLayer, competitiveWeight, 1);

        /* Creates forward connectivity from input to competitive layer */
        ConnectionFactory.forwardConnect(inputLayer, competitiveLayer, 1);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }
        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }
        builder.append("]");
        return builder.toString();
    }
}
