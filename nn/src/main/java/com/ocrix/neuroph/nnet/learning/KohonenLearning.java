/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import java.util.Arrays;
import java.util.Iterator;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Learning algorithm for <a href="http://goo.gl/6LZru">Kohonen network</a>.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class KohonenLearning extends LearningRule {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    private double learningRate = 0.9d;
    private final int[] iterations = { 100, 0 };
    private final double decStep[] = new double[2];
    private int mapSize = 0;
    /* neighborhood radius */
    private final int[] nR = { 1, 1 };
    private int currentIteration;

    /**
     * Creates the instance of {@link KohonenLearning}.
     */
    public KohonenLearning() {
        super();
    }

    @Override
    public void learn(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {

        for (int phase = 0; phase < 2; phase++) {
            for (int k = 0; k < iterations[phase]; k++) {
                @SuppressWarnings("unchecked")
                Iterator<TrainingElement> e = trainingSet.iterator();
                while (e.hasNext() && !isStopped()) {
                    TrainingElement tE = e.next();
                    learnPattern(tE, nR[phase]);
                } // while
                currentIteration = k;
                this.notifyChange();
                if (isStopped())
                    return;
            } // for k
            learningRate = learningRate * 0.5;
        } // for phase
    }

    /**
     * Learns the given pattern. A Kohonen unit computes the Euclidian distance
     * between an input x and its weight vector w.
     * 
     * @param tE
     *            - training element.
     * @param neighborhood
     */
    private void learnPattern(TrainingElement tE, int neighborhood) {
        Validator.validateNeuralNetwork(getNeuralNetwork());
        neuralNetwork.setInput(tE.getInput());
        neuralNetwork.calculate();
        Neuron winner = getClosest();
        if (winner.getOutput() == 0) {
            /* If it is already trained a cell, nothing to do */
            return;
        }

        Layer mapLayer = neuralNetwork.getLayerAt(1);
        int winnerIdx = mapLayer.indexOf(winner);
        adjustCellWeights(winner, 0);

        int cellNum = mapLayer.getNeuronsCount();
        for (int p = 0; p < cellNum; p++) {
            if (p == winnerIdx)
                continue;
            if (isNeighbor(winnerIdx, p, neighborhood)) {
                Neuron cell = mapLayer.getNeuronAt(p);
                adjustCellWeights(cell, 1);
            } // if
        } // for

    }

    /**
     * Gets unit with closest weight vector
     * 
     * @return the neuron.
     */
    private Neuron getClosest() {
        Iterator<Neuron> i = this.neuralNetwork.getLayerAt(1)
        .getNeuronsIterator();
        Neuron winner = new Neuron();
        double minOutput = 100;
        while (i.hasNext()) {
            Neuron n = i.next();
            double out = n.getOutput();
            if (out < minOutput) {
                minOutput = out;
                winner = n;
            } /* if closing */
        } /* while closing */
        return winner;
    }

    /**
     * Adjusts the connection's weight.
     * 
     * @param cell
     *            a neuron to whom it will be adjusted.
     * @param r
     *            the radius.
     */
    private void adjustCellWeights(Neuron cell, int r) {
        Iterator<Connection> i = cell.getInputsIterator();
        while (i.hasNext()) {
            Connection conn = i.next();
            double dWeight = (learningRate / (r + 1))
            * (conn.getInput() - conn.getWeight().getValue());
            conn.getWeight().inc(dWeight);
        }/* while closing */
    }

    /**
     * Answers if it is a neighbor.
     * 
     * @param i
     * @param j
     * @param n
     * @return
     */
    private boolean isNeighbor(int i, int j, int n) {
        // i - central cell
        // n - size of the neighborhood
        // j - cells for testing
        //        n = 1 why ???;
        int d = mapSize;
        /* number of cells up to */
        int rt = n;

        while ((i - rt * d) < 0){
            rt--;
        }

        /* number of cells to the bottom */
        int rb = n;
        while ((i + rb * d) > (d * d - 1))
            rb--;

        for (int g = -rt; g <= rb; g++) {
            /* number of cells in the left side */
            int rl = n;
            int rl_mod = (i - rl) % d;
            int i_mod = i % d;
            while (rl_mod > i_mod) {
                rl--;
                rl_mod = (i - rl) % d;
            }

            /* number of cells in the right side */
            int rd = n;
            int rd_mod = (i + rd) % d;

            while (rd_mod < i_mod) {
                rd--;
                rd_mod = (i + rd) % d;
            }

            if ((j >= (i + g * d - rl)) && (j <= (i + g * d + rd)))
                return true;
            // else if (j<(i+g*d-rl)) return false;
        } /* for closing */
        return false;
    }

    /**
     * Gets a learning rate.
     * 
     * @return - the learning rate.
     * @see #setLearningRate(double)
     */
    public double getLearningRate() {
        return learningRate;
    }

    /**
     * Sets a learning rate.
     * 
     * @param learningRate
     *            - the learning rate.
     * @see #getLearningRate()
     */
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    /**
     * Sets iterations.
     * 
     * @param Iphase
     * @param IIphase
     * @see #getIteration()
     */
    public void setIterations(int Iphase, int IIphase) {
        this.iterations[0] = Iphase;
        this.iterations[1] = IIphase;
    }

    /**
     * Gets 2D array of the iterations.
     * 
     * @return
     */
    public int[] getIterations() {
        return this.iterations;
    }

    /**
     * Gets the current iteration.
     * 
     * @return the current iteration.
     */
    public Integer getCurrentIteration() {
        return new Integer(currentIteration);
    }

    /**
     * Returns a map size.
     * 
     * @return the map size.
     */
    public int getMapSize() {
        return mapSize;
    }

    @Override
    public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
        super.setNeuralNetwork(neuralNetwork);
        int neuronsNum = neuralNetwork.getLayerAt(1).getNeuronsCount();
        mapSize = (int) Math.sqrt(neuronsNum);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [learningRate=");
        builder.append(learningRate);
        builder.append(", ");
        builder.append("iterations=");
        builder.append(Arrays.toString(iterations));
        builder.append(", ");
        builder.append("decStep=");
        builder.append(Arrays.toString(decStep));
        builder.append(", ");
        builder.append("mapSize=");
        builder.append(mapSize);
        builder.append(", ");
        builder.append("nR=");
        builder.append(Arrays.toString(nR));
        builder.append(", ");
        builder.append("currentIteration=");
        builder.append(currentIteration);
        builder.append("]");
        return builder.toString();
    }
}
