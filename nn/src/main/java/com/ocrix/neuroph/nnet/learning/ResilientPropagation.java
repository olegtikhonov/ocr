package com.ocrix.neuroph.nnet.learning;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;

/**
 * Resilient Propagation learning rule used for Multi Layer Perceptron neural
 * networks. Its one of the most efficient learning rules for this type of
 * networks, and it does not require setting of learning rule parameter. <br>
 * The defaults are:
 * <ul>
 * <li>decreaseFactor = 0.5d</li>
 * <li>increaseFactor = 1.2d</li>
 * <li>initialDelta = 0.1d</li>
 * <li>maxDelta = 1.0d</li>
 * <li>minDelta = 1e-6</li>
 * <li>ZERO_TOLERANCE = 1e-27</li>
 * </ul>
 * 
 * @author Borislav Markov
 * @author Zoran Sevarac
 */
public class ResilientPropagation extends BackPropagation {

    /*
     * The fingerprint of this instance of class.
     */
    private static final long serialVersionUID = -411850897903428188L;

    private double decreaseFactor = 0.5d;
    private double increaseFactor = 1.2d;
    private double initialDelta = 0.1d;
    private double maxDelta = 1.0d;
    private double minDelta = 1e-6;
    private final double initialWeightChange = 0.0d;
    /* the lowest limit when something is considered to be zero */
    private static final double ZERO_TOLERANCE = 1e-27;

    /**
     * Constructs the {@link ResilientPropagation}.
     */
    public ResilientPropagation() {
        super();
        super.setBatchMode(true);
    }

    /**
     * Returns the sign of the value.
     * 
     * @param value
     *            to be checked.
     * 
     * @return <code>0</code> if abs(value) < ZERO_TOLERANCE, <code>1</code> if
     *         value > <code>0</code>, and <code>-1</code> otherwise.
     */
    private int sign(final double value) {
        if (Math.abs(value) < ZERO_TOLERANCE) {
            return 0;
        } else if (value > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    protected void onStart() {
        Validator.validateNeuralNetwork(getNeuralNetwork());
        /*
         * Inits all stuff from super classes create
         * ResilientWeightTrainingtData objects that will hold additional data
         * (resilient specific) during the training process.
         */
        super.onStart();
        for (Layer layer : this.neuralNetwork.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                    // connection.getWeight().setTrainingData(new
                    // ResilientWeightTrainingtData());
                    connection.getWeight().setTrainingData(ResilientWeightTrainingtData.create());
                }
            }
        }
    }

    /**
     * Calculates and sums gradients for each neuron's weight, the actual weight
     * update is done in batch mode.
     * 
     * @see ResilientPropagation#resillientWeightUpdate(com.ocrix.neuroph.core.Weight)
     */
    @Override
    protected void updateNeuronWeights(Neuron neuron) {
        if (neuron != null) {
            for (Connection connection : neuron.getInputConnections()) {
                double input = connection.getInput();
                if (input == 0) {
                    continue;
                }

                /* Gets the error for specified neuron */
                double neuronError = neuron.getError();
                /* Gets the current connection's weight */
                Weight weight = connection.getWeight();
                /*
                 * Gets the object that stores reislient training data for
                 * that weight
                 */
                // ResilientWeightTrainingtData weightData =
                // (ResilientWeightTrainingtData) weight.getTrainingData();
                ResilientWeightTrainingtData weightData = baseWeightToResilent(weight);

                /* Sets previous gradient */
                weightData.setPreviousGradient(weight.getValue());
                weightData.setPreviousWeightChange(initialWeightChange);
                weightData.setPreviousDelta(minDelta);

                /*
                 * Calculates the weight gradient (and sum gradients since
                 * learning is done in batch mode). Note: 'cause resilent weight
                 * gradient = weight.gradient
                 */
                weightData.setGradient(weightData.getGradient() + (neuronError * input));
                // weightData.gradient += neuronError * input;
                weight.setValue(weightData.getGradient());
            }
        }
    }

    @Override
    protected void doBatchWeightsUpdate() {
        Validator.validateNeuralNetwork(getNeuralNetwork());
        /* Iterates over layers from output to input */
        for (int i = neuralNetwork.getLayersCount() - 1; i > 0; i--) {
            Layer layer = neuralNetwork.getLayers().get(i);
            /* iterates over neurons at each layer */
            for (Neuron neuron : layer.getNeurons()) {
                /* Iterates connections/weights for each neuron */
                for (Connection connection : neuron.getInputConnections()) {
                    /* For each connection weight applies the following changes */
                    Weight weight = connection.getWeight();
                    resillientWeightUpdate(weight);
                }
            }
        }
    }

    /**
     * <b>Note:</b>
     * Problematic function !!!
     * Weight update by done by ResilientPropagation learning rule Executed at
     * the end of epoch (in batch mode).
     * 
     * @param weight
     *            to be updated.
     */
    protected void resillientWeightUpdate(Weight weight) {
        /* Gets resilient training data for the current weight */
        // ResilientWeightTrainingtData weightData =
        // (ResilientWeightTrainingtData) weight.getTrainingData();
        //        ResilientWeightTrainingtData weightData = ResilientWeightTrainingtData.create();
        ResilientWeightTrainingtData weightData = baseWeightToResilent(weight);
        // weightData.setGradient(weight.);
        // (ResilientWeightTrainingtData) weight.getTrainingData();

        /*
         * Multiplies the current and previous gradient, and takes the sign. We
         * want to see if the gradient has changed its sign.
         */
        int gradientSignChange = sign(weightData.previousGradient * weightData.gradient);

        /* weight change to apply (delta weight) */
        double weightChange = 0;
        /* Adaptation factor */
        double delta;

        if (gradientSignChange > 0) {
            /*
             * If the gradient has retained its sign, then we increase delta
             * (adaptation factor) so that it will converge faster
             */
            delta = Math.min(weightData.previousDelta * increaseFactor, maxDelta);
            /*
             * If error is increasing (gradient is positive) then subtract
             * delta, if error is decreasing (gradient negative) then add delta.
             * Note that our gradient has different sign eg. -dE_dw so we omit
             * the minus here.
             */
            weightChange = sign(weightData.gradient) * delta;
            weightData.previousDelta = delta;
        } else if (gradientSignChange < 0) {
            /*
             * if gradientSignChange<0, then the sign has changed, and the last
             * weight change was too big
             */
            delta = Math.max(weightData.previousDelta * decreaseFactor, minDelta);
            /* Could be a problem. if it skipped. */
            weightChange = -weightData.previousWeightChange; //
            /* Min in previous step go back avoid double punishment */
            weightData.gradient = 0;
            weightData.previousGradient = 0;

            /* Moves values in the past */
            weightData.previousDelta = delta;
        } else if (gradientSignChange == 0) {
            /* If gradientSignChange==0 then there is no change to the delta */
            delta = weightData.previousDelta;
            /* Note that encog does this */
            weightChange = sign(weightData.gradient) * delta;
        }

        weight.value += weightChange;
        weightData.previousWeightChange = weightChange;
        weightData.previousGradient = weightData.gradient;
        /* As in moveNowValuesToPreviousEpochValues */
        weightData.gradient = 0;
    }

    /**
     * Gets decreased factor.
     * 
     * @return a value of the decreased factor.
     */
    public double getDecreaseFactor() {
        return decreaseFactor;
    }

    /**
     * Sets a decreased factor.
     * 
     * @param decreaseFactor
     */
    public void setDecreaseFactor(double decreaseFactor) {
        this.decreaseFactor = decreaseFactor;
    }

    /**
     * Gets the increased factor.
     * 
     * @return the increased factor.
     */
    public double getIncreaseFactor() {
        return increaseFactor;
    }

    /**
     * Sets the increased factor.
     * 
     * @param increaseFactor
     *            to be set.
     */
    public void setIncreaseFactor(double increaseFactor) {
        this.increaseFactor = increaseFactor;
    }

    /**
     * Gets the initial delta.
     * 
     * @return a delta.
     */
    public double getInitialDelta() {
        return initialDelta;
    }

    /**
     * Sets the initial delta.
     * 
     * @param initialDelta
     *            to be set.
     */
    public void setInitialDelta(double initialDelta) {
        this.initialDelta = initialDelta;
    }

    /**
     * Gets the Max delta.
     * 
     * @return the max delta.
     */
    public double getMaxDelta() {
        return maxDelta;
    }

    /**
     * Sets a max delta.
     * 
     * @param maxDelta
     *            to be set as max delta.
     */
    public void setMaxDelta(double maxDelta) {
        this.maxDelta = maxDelta;
    }

    /**
     * Gets a min delta.
     * 
     * @return the min delat.
     */
    public double getMinDelta() {
        return minDelta;
    }

    /**
     * Sets the minimal delta.
     * 
     * @param minDelta
     *            to be set.
     */
    public void setMinDelta(double minDelta) {
        this.minDelta = minDelta;
    }


    /**
     * Transforms {@link Weight} to {@link ResilientWeightTrainingtData}.
     * 
     * @param weight from which to be transformed.
     * 
     * @return a new instance of {@link ResilientWeightTrainingtData}.
     */
    private ResilientWeightTrainingtData baseWeightToResilent(Weight weight) {
        ResilientWeightTrainingtData resilentWeight = ResilientWeightTrainingtData.create();
        resilentWeight.setGradient(weight.getValue());
        return resilentWeight;
    }

    /**
     * Inner class, uses as container. Should it be protected/private?
     */
    // public class ResilientWeightTrainingtData {
    // public double gradient;
    // public double previousGradient;
    // public double previousWeightChange;
    // public double previousDelta = initialDelta;
    // }
    /*
     * TODO: Put it class as separated class.
     * TODO: Write Factory for different kind of weight trained data.
     */
    static class ResilientWeightTrainingtData {
        /* Class' members */
        private double gradient;
        private double previousGradient;
        private double previousWeightChange;
        // public double previousDelta = initialDelta;
        private double previousDelta;
        private static ResilientWeightTrainingtData instance = null;

        /**
         * Constructs the resilent weight trained data.
         */
        private ResilientWeightTrainingtData() {

        }

        /**
         * Creates the resilent weight trained data.
         * 
         * @return instance of {@link ResilientWeightTrainingtData}.
         */
        protected static ResilientWeightTrainingtData create() {
            // if (instance == null) {
            instance = new ResilientWeightTrainingtData();
            // }
            return instance;
        }

        /**
         * Sets the gradient.
         * 
         * @param grdnt to be put.
         */
        protected void setGradient(double grdnt) {
            this.gradient = grdnt;
        }

        /**
         * Gets the gradient.
         * 
         * @return the gradient.
         */
        protected double getGradient() {
            return this.gradient;
        }

        /**
         * Sets previous gradient.
         * 
         * @param prevGrad
         */
        protected void setPreviousGradient(double prevGrad) {
            this.previousGradient = prevGrad;
        }

        protected double getPreviousGradient() {
            return this.previousGradient;
        }

        protected void setPreviousWeightChange(double prevWeightChange) {
            this.previousWeightChange = prevWeightChange;
        }

        protected double getPreviousWeightChange() {
            return this.previousWeightChange;
        }

        // previousDelta
        protected void setPreviousDelta(double prevDelta) {
            this.previousDelta = prevDelta;
        }

        protected double getPreviousDelta() {
            return this.previousDelta;
        }
    }
}
