/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.CommonMessages.WARN_WINNING_NULL;

import java.util.List;
import java.util.logging.Logger;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.core.learning.UnsupervisedLearning;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;

/**
 * Defines a competitive learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class CompetitiveLearning extends UnsupervisedLearning {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    /* Local logger */
    private static final Logger log = Logger
            .getLogger(CompetitiveLearning.class.getName());

    /**
     * Creates new instance of CompetitiveLearning.
     */
    public CompetitiveLearning() {
        super();
    }

    /**
     * This method does one learning epoch for the unsupervised learning rules.
     * It iterates through the training set and trains network weights for each
     * element. Stops learning after one epoch.
     * 
     * @param trainingSet
     *            - the training set for training network.
     */
    @Override
    public void doLearningEpoch(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        super.doLearningEpoch(trainingSet);
        /*
         * stops learning after one learning epoch - because we don't have any
         * stopping criteria for unsupervised...
         */
        stopLearning();
    }

    /**
     * Adjusts weights for the winning neuron
     */
    @Override
    protected void updateNetworkWeights() {
        /* find active neuron in output layer */
        // TODO : change idx, in general case not 1
        CompetitiveNeuron winningNeuron = null;
        for (Layer cl : neuralNetwork.getLayers()) {
            winningNeuron = ((CompetitiveLayer) cl).getWinner();
            if (winningNeuron != null) {
                break;
            }
        }
        // Vector<Connection> inputConnections =
        // winningNeuron.getConnectionsFromOtherLayers();
        if (winningNeuron != null) {
            List<Connection> inputConnections = winningNeuron
                    .getConnectionsFromOtherLayers();
            for (Connection connection : inputConnections) {
                double weight = connection.getWeight().getValue();
                double input = connection.getInput();
                double deltaWeight = this.learningRate * (input - weight);
                connection.getWeight().inc(deltaWeight);
            }
        } else {
            log.warning(WARN_WINNING_NULL.to());
        }
    }
}
