/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.learning;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;

/**
 * <a href="http://goo.gl/8bJrp">Backpropagation learning</a> rule with momentum.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class MomentumBackpropagation extends BackPropagation {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Momentum factor
     */
    protected double momentum = 0.25d;

    /**
     * Creates new instance of MomentumBackpropagation learning.
     */
    public MomentumBackpropagation() {
        super();
    }

    /**
     * This method implements weights update procedure for the single neuron for
     * the back propagation with momentum factor.
     * 
     * @param neuron
     *            neuron to update weights.
     */
    @Override
    protected void updateNeuronWeights(Neuron neuron) {
        for (Connection connection : neuron.getInputConnections()) {
            double input = connection.getInput();
            if (input == 0) {
                continue;
            }

            /*
             * Gets the error for specified neuron, tanh can be used to minimise
             * the impact of big error values, which can cause network
             * instability suggested at
             * https://sourceforge.net/tracker/?func=detail
             * &atid=1107579&aid=3130561&group_id=238532 double neuronError =
             * Math.tanh(neuron.getError());
             */
            double neuronError = neuron.getError();
            Weight weight = connection.getWeight();

            // TODO: set more suitable approach of casting
            //            MomentumWeightTrainingData weightTrainingData = (MomentumWeightTrainingData) weight.getTrainingData(); <-- original version

            //TODO: Why do we need that data ??? I've changed it to be worked, however it's useless IMO. Need more time to investigate.

            MomentumWeightTrainingData weightTrainingData = MomentumWeightTrainingData.getInstance();
            weightTrainingData.setPreviousValue(weight.getValue());

            if (weightTrainingData != null) {
                double previousWeightValue = weightTrainingData.previousValue;
                double weightChange = this.learningRate * neuronError * input + momentum * (weight.value - previousWeightValue);
                weightTrainingData.previousValue = weight.value;

                /* If the learning is in batch mode apply the weight change immediately */
                if (this.isInBatchMode() == false) {
                    weight.weightChange = weightChange;
                    weight.value += weightChange;
                } else {
                    /* Otherwise, sum the weight changes and apply them after at the end of epoch */
                    weight.weightChange += weightChange;
                }
            }

        }
    }

    /**
     * Returns the momentum factor.
     * 
     * @return momentum factor.
     */
    public double getMomentum() {
        return momentum;
    }

    /**
     * Sets the momentum factor.
     * 
     * @param momentum
     *            momentum factor.
     */
    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public static class MomentumWeightTrainingData{
        private static MomentumWeightTrainingData instance;
        private double previousValue;


        public static MomentumWeightTrainingData getInstance(){
            if(instance == null){
                instance = new MomentumWeightTrainingData();
            }
            return instance;
        }

        public void setPreviousValue(double prevData){
            this.previousValue = prevData;
        }
    }

    @Override
    protected void onStart() {
        Validator.validateNeuralNetwork(getNeuralNetwork());
        super.onStart();
        /*
         * Now creates MomentumWeightData objects that will be used during the
         * training to store previous weight value
         */
        for (Layer layer : this.neuralNetwork.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()){
                    //                    connection.getWeight().setTrainingData(new MomentumWeightTrainingData());
                    connection.getWeight().setTrainingData(MomentumWeightTrainingData.getInstance());
                }

            } // for
        } // for

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [momentum=");
        builder.append(momentum);
        builder.append("]");
        return builder.toString();
    }
}
