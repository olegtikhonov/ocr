/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.BIAS;
import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.util.TransferFunctionType.STEP;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.learning.BinaryHebbianLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * <a href="http://is.gd/I92BHK">Hopfield neural network.</a> Notes: try to use
 * [1, -1] activation levels, sgn as transfer function, or real numbers for
 * activation.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */

public class Hopfield extends NeuralNetwork {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;

    /*
     * Creates new Hopfield network with specified neuron number.
     * @param neuronsCount neurons number in Hopfield network.
     */
    public Hopfield(int neuronsCount) {
        /* Inits neuron settings for Hopfield network */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(NEURON_TYPE.to(), InputOutputNeuron.class);
        neuronProperties.setProperty(BIAS.to(), new Double(0));
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        neuronProperties.setProperty(Y_HIGH.to(), new Double(1));
        neuronProperties.setProperty(Y_LOW.to(), new Double(0));

        this.createNetwork(neuronsCount, neuronProperties);
    }

    /**
     * Creates new Hopfield network with specified neuron number and neuron
     * properties.
     * 
     * @param neuronsCount
     *            neurons number in Hopfield network.
     * @param neuronProperties
     *            neuron properties.
     */
    public Hopfield(int neuronsCount, NeuronProperties neuronProperties) {
        this.createNetwork(neuronsCount, neuronProperties);
    }

    /**
     * Creates Hopfield network architecture.
     * 
     * @param neuronsCount
     *            neurons number in Hopfield network.
     * @param neuronProperties
     *            neuron properties.
     */
    private void createNetwork(int neuronsCount,
            NeuronProperties neuronProperties) {

        /* Sets network type */
        this.setNetworkType(NeuralNetworkType.HOPFIELD);

        /* Creates neurons in the layer */
        Layer layer = LayerFactory.createLayer(neuronsCount, neuronProperties);

        /* Creates full connectivity in layer */
        ConnectionFactory.fullConnect(layer, 0.1);

        /* Adds layer to network */
        this.addLayer(layer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets Hopfield learning rule for this network */
        this.setLearningRule(new BinaryHebbianLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }

}
