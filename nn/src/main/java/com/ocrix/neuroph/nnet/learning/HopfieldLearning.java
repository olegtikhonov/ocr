/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import java.util.logging.Level;
import java.util.logging.Logger;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Learning algorithm for the Hopfield neural network.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class HopfieldLearning extends LearningRule {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;
    /* Local log */
    private final static Logger log = Logger.getLogger(HopfieldLearning.class
            .getName());

    /**
     * Creates new HopfieldLearning
     */
    public HopfieldLearning() {
        super();
    }

    /**
     * Calculates weights for the hopfield net to learn the specified training
     * set.
     * 
     * @param trainingSet
     *            - the training set to learn.
     */
    @Override
    public void learn(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        /* Validates a neural network not to be NULL */
        Validator.validateNeuralNetwork(getNeuralNetwork());
        int M = trainingSet.size();
        int N = neuralNetwork.getLayerAt(0).getNeuronsCount();
        Layer hopfieldLayer = neuralNetwork.getLayerAt(0);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (j == i)
                    continue;
                Neuron ni = hopfieldLayer.getNeuronAt(i);
                Neuron nj = hopfieldLayer.getNeuronAt(j);
                Connection cij = nj.getConnectionFrom(ni);
                Connection cji = ni.getConnectionFrom(nj);
                double w = 0;
                for (int k = 0; k < M; k++) {
                    TrainingElement trainingElement = trainingSet.elementAt(k);
                    /* Prevents OOBE */
                    if ((i < trainingElement.getInput().length)
                            && (j < trainingElement.getInput().length)) {
                        double pki = trainingElement.getInput()[i];
                        double pkj = trainingElement.getInput()[j];
                        w = w + pki * pkj;
                    }

                } // k
                // NPE prevention
                if (cij != null && cji != null) {
                    cij.getWeight().setValue(w);
                    cji.getWeight().setValue(w);
                } else {
                    log.warning("The neuron [" + ni.getLabel()
                            + "] seems not to be connected to the ["
                            + nj.getLabel());
                }
            } // j
        } // i
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }

    /**
     * Turns off the logger, sets Log level to {@link Level} = OFF.
     */
    public void logOff() {
        log.setLevel(Level.OFF);
    }

    /**
     * Turns on the logger, sets Log level to {@link Level} = ALL.
     */
    public void logOn() {
        log.setLevel(Level.ALL);
    }
}
