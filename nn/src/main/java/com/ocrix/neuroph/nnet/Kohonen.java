/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.Difference;
import com.ocrix.neuroph.core.input.Intensity;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nnet.learning.KohonenLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * <a href="http://goo.gl/TVqN1">Kohonen neural network</a>.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Kohonen extends NeuralNetwork {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new Kohonen network with specified number of neurons in input and
     * map layer.
     * 
     * @param inputNeuronsCount
     *            number of neurons in input layer.
     * @param outputNeuronsCount
     *            number of neurons in output layer.
     */
    public Kohonen(int inputNeuronsCount, int outputNeuronsCount) {
        this.createNetwork(inputNeuronsCount, outputNeuronsCount);
    }

    /**
     * Creates Kohonen network architecture with specified number of neurons in
     * input and map layer.
     * 
     * @param inputNeuronsCount
     *            number of neurons in input layer.
     * @param outputNeuronsCount
     *            number of neurons in output layer.
     */
    private void createNetwork(int inputNeuronsCount, int outputNeuronsCount) {
        /* Specifies the input neuron properties (use default: weighted sum input with linear transfer) */
        NeuronProperties inputNeuronProperties = new NeuronProperties();

        /* Specifies a map neuron properties */
        NeuronProperties outputNeuronProperties = new NeuronProperties(
                Neuron.class, // neuron type
                Difference.class, // weights function
                Intensity.class, // summing function
                Linear.class // transfer function
        );
        /* Sets network type */
        this.setNetworkType(NeuralNetworkType.KOHONEN);

        /* Creates the Layer input layer */
        Layer inLayer = LayerFactory.createLayer(inputNeuronsCount, inputNeuronProperties);
        this.addLayer(inLayer);

        /* Creates a map layer */
        Layer mapLayer = LayerFactory.createLayer(outputNeuronsCount, outputNeuronProperties);
        this.addLayer(mapLayer);

        /* Creates full connectivity between input and output layer */
        ConnectionFactory.fullConnect(inLayer, mapLayer);

        /* Sets network input and output cells */
        NeuralNetworkFactory.setDefaultIO(this);

        this.setLearningRule(new KohonenLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Kohonen.class.getName());
        builder.append(" [");
        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }
        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }
        builder.append("]");
        return builder.toString();
    }
}
