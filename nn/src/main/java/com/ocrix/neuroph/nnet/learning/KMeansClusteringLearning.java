package com.ocrix.neuroph.nnet.learning;

import java.util.logging.Logger;

import com.ocrix.neuroph.core.learning.UnsupervisedLearning;

/**
 * 
 * <a href=
 * "http://www.puremango.co.uk/2010/01/k-means-clustering-machine-learning/"
 * >KMeansClusteringLearning</a>
 * 
 * Euclidean k-medians k-means algorithm K-means is a special case of EM
 * clustering Research: Fuzzy c-means (Sum of the weights is 1) Harmonic K-means
 * (Use harmonic mean instead of standard mean)
 * <ul>
 * Step 1: Place ranomly initial group centroids into the 2d space.
 * Step 2: Assign each object to the group that has the closest centroid.
 * Step 3: Recalculate the positions of the centroids.
 * Step 4: If the positions of the centroids didn't change go to the next step, else go to Step 2.
 * Step 5: End.
 * </ul>
 * 
 * 
 */
public class KMeansClusteringLearning extends UnsupervisedLearning {
    /*  */
    private static final long serialVersionUID = 6709488464373594723L;
    /* Local log */
    private final static Logger log = Logger.getLogger(KMeansClusteringLearning.class.getName());


    @Override
    protected void updateNetworkWeights() {
        //TODO: implement it.
        log.severe("The working functionality IS NOT implemented yet.");
    }
}
