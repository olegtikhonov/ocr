/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.flat;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_NOT_SUPPORTED;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_UPDATE_NETWORK_WEIGHTS;

import java.io.Serializable;
import java.util.logging.Logger;

import org.encog.engine.EncogEngineError;
import org.encog.engine.data.EngineIndexableSet;
import org.encog.engine.network.flat.FlatNetwork;
import org.encog.engine.network.train.TrainFlatNetwork;
import org.encog.engine.network.train.prop.TrainFlatNetworkBackPropagation;
import org.encog.engine.network.train.prop.TrainFlatNetworkManhattan;
import org.encog.engine.network.train.prop.TrainFlatNetworkResilient;

import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.SupervisedLearning;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Learning rule for flat networks. The learning type is specified by the the
 * learningType property. Currently the flat networks can be trained in the
 * following three ways.
 * 
 * Classic momentum based propagation. The learning rate and momentum are
 * specified by using the setLearningRate and setMomentum on the
 * FlatNetworkLearning class.
 * 
 * Resilient Propagation. No parameters are needed for this learning method.
 * RPROP is the best general purpose training method.
 * 
 * Manhattan update rule. Not at all a good general purpose learning method,
 * only useful in some situations. A learning rate must be specified by using
 * the setLearningRate method on the FlatNetworkLearning class.
 * 
 * 
 * @author Jeff Heaton (http://www.jeffheaton.com)
 * 
 * @see FlatNetworkPlugin
 */
public class FlatNetworkLearning extends SupervisedLearning implements
Serializable {

    /* The object serial id. */
    private static final long serialVersionUID = 1L;
    /* The flat network to train. */
    private FlatNetwork flat;
    /*
     * The last training set used. This property is used to tell if the training
     * set has changed since the last iteration.
     */
    private transient EngineIndexableSet lastTrainingSet;
    /*
     * The last type of learning used. This property is used to tell if the
     * training set has changed since the last iteration.
     */
    private FlatLearningType lastLearningType;
    /* The flat trainer that is in use. */
    private transient TrainFlatNetwork training;
    /* The learning rate. This value is used for back propagation and Manhattan. */
    private double learningRate;
    /* The momentum. This value is used for back propagation training. */
    private double momentum;
    /*
     * The number of threads to use. The default, zero, specifies to use the
     * processor count to determine an optimal number of threads.
     */
    private int numThreads;
    /* The type of flat learning to be used. */
    private FlatLearningType learningType;

    /* Class' constants */
    private static final double LEARNING_RATE = 0.7d;
    private static final double MOMENTUM = 0.3d;
    /* Logger */
    private static final Logger log = Logger
    .getLogger(FlatNetworkLearning.class.getName());

    /**
     * Constructor, creates a flat network learning rule from the specified flat
     * network.
     * 
     * @param flat
     *            - the flat network.
     */
    public FlatNetworkLearning(FlatNetwork flat) {
        Validator.validateFlatNetwork(flat);
        init(flat);
    }

    /**
     * Constructor, create a flat network learning rule from a Neuroph network.
     * This neuroph network must have already had the flat network plugin
     * installed.
     * 
     * @param network
     *            - the neuroph network to use.
     */
    public FlatNetworkLearning(NeuralNetwork network) {
        if (network != null) {
            FlatNetworkPlugin plugin = (FlatNetworkPlugin) network
            .getPlugin(FlatNetworkPlugin.class);
            FlatNetwork flat = plugin.getFlatNetwork();
            this.flat = flat;
            // if (this.flat == null){
            // log.severe(FLAT_NET_PLUG_SHOULD_BE_ATTACHED.to());
            // throw new
            // EncogEngineError(FLAT_NET_PLUG_SHOULD_BE_ATTACHED.to());
            // }

            init(flat);
        }
    }

    /**
     * Internal method used to setup the learning rule.
     * 
     * @param flat
     *            - the network that will be trained.
     */
    private void init(FlatNetwork flat) {
        this.flat = flat;
        this.learningType = FlatLearningType.ResilientPropagation;
        this.learningRate = LEARNING_RATE;
        this.momentum = MOMENTUM;
    }

    /**
     * This method is not used.
     * 
     * @param patternError
     *            Not used.
     */
    @Override
    protected void updateNetworkWeights(double[] patternError) {
        log.severe(ERROR_UPDATE_NETWORK_WEIGHTS.to());
        throw new EncogEngineError(ERROR_UPDATE_NETWORK_WEIGHTS.to());

    }

    /**
     * Perform one learning epoch.
     * 
     * @param trainingSet
     *            - the training set to use.
     */
    @Override
    public void doLearningEpoch(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        this.previousEpochError = this.totalNetworkError;

        /*
         * have we changed learning types, or training sets? If so, create a
         * whole new trainer.
         */
        if (this.lastLearningType != this.learningType
                || this.lastTrainingSet.equals(trainingSet)) {// this.lastTrainingSet
            // != trainingSet

            this.lastTrainingSet = trainingSet;

            switch (this.learningType) {
            case ResilientPropagation:
                this.training = new TrainFlatNetworkResilient(this.flat,
                        this.lastTrainingSet);
                break;

            case ManhattanUpdateRule:
                this.training = new TrainFlatNetworkManhattan(this.flat,
                        this.lastTrainingSet, this.learningRate);
                break;

            case BackPropagation:
                this.training = new TrainFlatNetworkBackPropagation(this.flat,
                        this.lastTrainingSet, this.getLearningRate(),
                        this.getMomentum());
                break;
            }

            this.training.setNumThreads(this.numThreads);

            /*
             * remember the last state of the learning type and training set so
             * that we can tell if this changes in the next iteration.
             */
            this.lastLearningType = this.learningType;
            this.lastTrainingSet = trainingSet;
        }

        try {
            //TODO: take of NLP !!!
            /* performs a training iteration */
            this.training.iteration();
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append(e.getMessage());
            sb.append(" in the ");
            sb.append(e.getCause().getStackTrace()[0].getClassName());
            sb.append(".");
            sb.append(e.getCause().getStackTrace()[0].getMethodName());
            log.severe(sb.toString());
        } finally {
            this.totalNetworkError = this.training.getError();
            /* should Neuroph training stop */
            if (hasReachedStopCondition()) {
                stopLearning();
            }
        }
    }

    /**
     * @return The learning rate. This value is used for Backprop and Manhattan
     *         training.
     */
    @Override
    public double getLearningRate() {
        return learningRate;
    }

    /**
     * Set the learning rate. This value is used for Backprop and Manhattan
     * training.
     */
    @Override
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    /**
     * @return The momentum, this is is used for Backprop.
     */
    public double getMomentum() {
        return momentum;
    }

    /**
     * Set the momentum, this is used for Backprop.
     * 
     * @param momentum
     */
    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    /**
     * @return The number of threads to use. Zero requests that the learning
     *         rule choose a number of processors.
     */
    public int getNumThreads() {
        return numThreads;
    }

    /**
     * Set the number of threads to use.
     * 
     * @param numThreads
     */
    public void setNumThreads(int numThreads) {
        this.numThreads = numThreads;
    }

    /**
     * @return The type of learning to use.
     */
    public FlatLearningType getLearningType() {
        return learningType;
    }

    /**
     * Set the type of learning to use.
     * 
     * @param learningType
     *            The type of learning to use.
     */
    public void setLearningType(FlatLearningType learningType) {
        this.learningType = learningType;
    }

    @Override
    protected void addToSquaredErrorSum(double[] outputError) {
        throw new EncogEngineError(ERROR_NOT_SUPPORTED.to());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        // if (flat != null) {
        builder.append("flat=");
        builder.append(flat);
        builder.append(", ");
        // }
        if (lastLearningType != null) {
            builder.append("lastLearningType=");
            builder.append(lastLearningType);
            builder.append(", ");
        }
        builder.append("learningRate=");
        builder.append(learningRate);
        builder.append(", momentum=");
        builder.append(momentum);
        builder.append(", numThreads=");
        builder.append(numThreads);
        builder.append(", ");
        if (learningType != null) {
            builder.append("learningType=");
            builder.append(learningType);
        }
        builder.append("]");
        return builder.toString();
    }

    protected void setLastLearningType(FlatLearningType type) {
        this.lastLearningType = type;
    }

    protected void setLastTrainingSet(TrainingSet<?> ts) {
        this.lastTrainingSet = ts;
    }
}
