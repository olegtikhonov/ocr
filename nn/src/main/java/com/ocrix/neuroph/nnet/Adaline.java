/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.X_HIGH;
import static com.ocrix.neuroph.common.NetProperty.X_LOW;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.learning.LMS;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Adaline neural network architecture with LMS learning rule. Uses bias input,
 * bipolar inputs [-1, 1] and ramp transfer function It can be also created
 * using binary inputs and linear transfer function, but that doesn't work for
 * some problems.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Adaline extends NeuralNetwork {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new Adaline network with specified number of neurons in input
     * layer
     * 
     * @param inputNeuronsCount
     *            number of neurons in input layer
     */
    public Adaline(int inputNeuronsCount) {
        this.createNetwork(inputNeuronsCount);
    }

    /**
     * Creates adaline network architecture with specified number of input
     * neurons.
     * 
     * @param inputNeuronsCount
     *            number of neurons in input layer.
     */
    private void createNetwork(int inputNeuronsCount) {
        /* Sets network type code */
        this.setNetworkType(NeuralNetworkType.ADALINE);

        /* Creates input layer neuron settings for this network */
        NeuronProperties inNeuronProperties = new NeuronProperties();
        inNeuronProperties.setProperty(TRANSFER_FUNCTION.to(),
                TransferFunctionType.LINEAR);

        /* Creates an input layer with specified number of neurons */
        Layer inputLayer = LayerFactory.createLayer(inputNeuronsCount,
                inNeuronProperties);
        inputLayer.addNeuron(new BiasNeuron());
        /*
         * Adds bias neuron (always 1, and it will act as bias input for output
         * neuron)
         */
        this.addLayer(inputLayer);

        /* Creates output layer neuron settings for this network */
        NeuronProperties outNeuronProperties = new NeuronProperties();
        outNeuronProperties.setProperty(TRANSFER_FUNCTION.to(),
                TransferFunctionType.RAMP);
        outNeuronProperties.setProperty(SLOPE.to(), new Double(1));
        outNeuronProperties.setProperty(Y_HIGH.to(), new Double(1));
        outNeuronProperties.setProperty(X_HIGH.to(), new Double(1));
        outNeuronProperties.setProperty(Y_LOW.to(), new Double(-1));
        outNeuronProperties.setProperty(X_LOW.to(), new Double(-1));

        /* Creates an output layer (only one neuron) */
        Layer outputLayer = LayerFactory.createLayer(1, outNeuronProperties);
        this.addLayer(outputLayer);

        /* Creates a full connectivity between input and output layer */
        ConnectionFactory.fullConnect(inputLayer, outputLayer);

        /* Sets an input and output cells for network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets LMS learning rule for this network */
        this.setLearningRule(new LMS());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }
        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }
        builder.append("]");
        return builder.toString();
    }
}