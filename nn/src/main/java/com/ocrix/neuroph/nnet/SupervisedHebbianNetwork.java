/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.CommonConsts.NEG_ONE;
import static com.ocrix.neuroph.common.CommonConsts.ONE;
import static com.ocrix.neuroph.common.NetProperty.SLOPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.X_HIGH;
import static com.ocrix.neuroph.common.NetProperty.X_LOW;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.util.TransferFunctionType.RAMP;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.learning.SupervisedHebbianLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * <a href="http://goo.gl/6mjo9">Hebbian neural network</a> with supervised Hebbian learning algorithm. In order
 * to work this network needs aditional bias neuron in input layer which is
 * allways 1 in training set!
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class SupervisedHebbianNetwork extends NeuralNetwork {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Creates an instance of Supervised Hebbian Network net with specified
     * number neurons in input and output layer.
     * 
     * @param inputNeuronsNum - number of neurons in input layer.
     * @param outputNeuronsNum - number of neurons in output layer.
     */
    public SupervisedHebbianNetwork(int inputNeuronsNum, int outputNeuronsNum) {
        this.createNetwork(inputNeuronsNum, outputNeuronsNum, RAMP);
    }

    /**
     * Creates an instance of Supervised Hebbian Network with specified number
     * of neurons in input layer and output layer, and transfer function.
     * 
     * @param inputNeuronsNum - number of neurons in input layer.
     * @param outputNeuronsNum - number of neurons in output layer.
     * @param transferFunctionType - transfer function type id.
     */
    public SupervisedHebbianNetwork(int inputNeuronsNum, int outputNeuronsNum, TransferFunctionType transferFunctionType) {
        this.createNetwork(inputNeuronsNum, outputNeuronsNum, transferFunctionType);
    }

    /**
     * Creates an instance of Supervised Hebbian Network with specified number
     * of neurons in input layer, output layer and transfer function.
     * 
     * @param inputNeuronsNum - number of neurons in input layer.
     * @param outputNeuronsNum - number of neurons in output layer.
     * @param transferFunctionType - transfer function type.
     */
    private void createNetwork(int inputNeuronsNum, int outputNeuronsNum, TransferFunctionType transferFunctionType) {

        /* Initializes neuron properties */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), transferFunctionType);
        neuronProperties.setProperty(SLOPE.to(), new Double(ONE.value()));
        neuronProperties.setProperty(Y_HIGH.to(), new Double(ONE.value()));
        neuronProperties.setProperty(X_HIGH.to(), new Double(ONE.value()));
        neuronProperties.setProperty(Y_LOW.to(), new Double(NEG_ONE.value()));
        neuronProperties.setProperty(X_LOW.to(), new Double(NEG_ONE.value()));

        /* Sets network type code */
        this.setNetworkType(NeuralNetworkType.SUPERVISED_HEBBIAN_NET);

        /* Creates the input layer */
        Layer inputLayer = LayerFactory.createLayer(inputNeuronsNum, neuronProperties);
        this.addLayer(inputLayer);

        /* Creates the output layer */
        Layer outputLayer = LayerFactory.createLayer(outputNeuronsNum, neuronProperties);
        this.addLayer(outputLayer);

        /* Creates the full connectivity between input and output layers */
        ConnectionFactory.fullConnect(inputLayer, outputLayer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets appropriate learning rule for this network */
        this.setLearningRule(new SupervisedHebbianLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");

        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }

        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }

        builder.append("]");

        return builder.toString();
    }
}
