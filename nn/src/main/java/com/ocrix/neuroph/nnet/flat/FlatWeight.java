/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.flat;

import java.util.Arrays;
import java.util.logging.Logger;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Weight;

/**
 * A subclass of the Weight class. Allows weights to be stored as elements in a
 * double type array. This allows weights to very quickly be modified, affecting
 * the neural network.
 * 
 * @author Jeff Heaton (http://www.jeffheaton.com)
 * @see FlatNetworkPlugin
 */
public class FlatWeight extends Weight {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class
     */
    private static final long serialVersionUID = 1L;
    /*
     * Previous weight value (used by some learning rules like momentum for
     * backpropagation)
     */
    private transient double previousValue;

    private double[] flatWeights;

    private int weightIndex;
    /* Local Logger */
    private final static Logger log = Logger.getLogger(FlatWeight.class
            .getName());

    // -------------------------------

    /**
     * Creates an instance of connection weight with random weight value in
     * range [0..1]
     */
    public FlatWeight(double[] flatWeights, int weightIndex) {
        Validator.validateDouble(flatWeights);
        this.flatWeights = flatWeights;
        if (weightIndex > 0 && weightIndex < this.flatWeights.length) {
            this.weightIndex = weightIndex;
        } else {
            log.warning("index value is " + weightIndex + " but should be [0:"
                    + (flatWeights.length - 1) + "], will be set to 0");
            this.weightIndex = 0;
        }

        this.setValue(Math.random() - 0.5d);
        this.previousValue = this.getValue();
    }

    /**
     * Creates an instance of connection weight with the specified weight value.
     * 
     * @param value
     *            - the weight value
     */
    public FlatWeight(double value) {
        this.setValue(value);
        this.previousValue = this.getValue();
    }

    /**
     * Increases the weight for the specified amount.
     * 
     * @param amount
     *            - the amount to add to current weight value
     */
    @Override
    public void inc(double amount) {
        this.flatWeights[this.weightIndex] += amount;
    }

    /**
     * Decreases the weight for specified amount.
     * 
     * @param amount
     *            - the amount to subtract from the current weight value.
     */
    @Override
    public void dec(double amount) {
        this.flatWeights[this.weightIndex] -= amount;
    }

    /**
     * Sets the weight value.
     * 
     * @param value
     *            - the weight value to set.
     */
    @Override
    public void setValue(double value) {
        // Validator.doubleInRange(value, 0, 1);
        initFlatWeights();
        this.flatWeights[this.weightIndex] = value;
    }

    /**
     * Returns weight value.
     * 
     * @return value of this weight.
     */
    @Override
    public double getValue() {
        return this.flatWeights[this.weightIndex];
    }

    /**
     * Sets the previous weight value.
     * 
     * @param previousValue
     *            - the weight value to set.
     */
    public void setPreviousValue(double previousValue) {
        this.previousValue = previousValue;
    }

    /**
     * Returns previous weight value.
     * 
     * @return value of this weight
     */
    public double getPreviousValue() {
        return this.previousValue;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [previousValue=");
        builder.append(previousValue);
        builder.append(", ");
        builder.append("flatWeights=");
        builder.append(Arrays.toString(flatWeights));
        builder.append(", ");
        builder.append("weightIndex=");
        builder.append(weightIndex);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Sets random weight value.
     */
    @Override
    public void randomize() {
        this.flatWeights[this.weightIndex] = Math.random() - 0.5d;
        this.previousValue = this.getValue();
    }

    /**
     * Sets random weight value within specified interval
     */
    @Override
    public void randomize(double min, double max) {
        this.flatWeights[this.weightIndex] = min + Math.random() * (max - min);
        this.previousValue = this.getValue();
    }

    private void initFlatWeights() {
        if (flatWeights == null)
            flatWeights = new double[1];
    }

}
