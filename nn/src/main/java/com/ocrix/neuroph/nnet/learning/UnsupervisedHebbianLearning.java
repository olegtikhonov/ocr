/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.CommonMessages.WARN_ANN_IS_NULL;
import static com.ocrix.neuroph.common.CommonMessages.WARN_NEURON_NULL;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.core.learning.UnsupervisedLearning;

/**
 * Unsupervised hebbian learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class UnsupervisedHebbianLearning extends UnsupervisedLearning {

    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /* Local logger */
    private static final Logger log = Logger.getLogger(UnsupervisedHebbianLearning.class.getName());

    /**
     * Creates new instance of UnsupervisedHebbianLearning algorithm
     */
    public UnsupervisedHebbianLearning() {
        super();
        this.setLearningRate(0.1d);
    }

    /**
     * This method does one learning epoch for the unsupervised learning rules.
     * It iterates through the training set and trains network weights for each
     * element. Stops learning after one epoch.
     * 
     * @param trainingSet
     *            training set for training network
     */
    @Override
    public void doLearningEpoch(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        super.doLearningEpoch(trainingSet);
        stopLearning(); // stop learning after one learning epoch -- why ? -
        // because we dont have any other stopping criteria for
        // this - must limit the iterations
    }

    /**
     * Adjusts weights for the output neurons.
     */
    @Override
    protected void updateNetworkWeights() {
        if (neuralNetwork != null) {
            for (Neuron neuron : neuralNetwork.getOutputNeurons()) {
                this.updateNeuronWeights(neuron);
            }
        }else {
            log.severe(WARN_ANN_IS_NULL.to());
        }
    }

    /**
     * This method implements weights update procedure for the single neuron.
     * 
     * @param neuron
     *            neuron to update weights.
     */
    protected void updateNeuronWeights(Neuron neuron) {
        if (neuron != null) {
            double output = neuron.getOutput();

            for (Connection connection : neuron.getInputConnections()) {
                double input = connection.getInput();
                double deltaWeight = input * output * this.learningRate;
                connection.getWeight().inc(deltaWeight);
            }
        }else {
            log.warning(WARN_NEURON_NULL.to());
        }
    }

    public void logOn(){
        log.setLevel(Level.ALL);
    }

    public void logOff(){
        log.setLevel(Level.OFF);
    }
}
