/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.CommonConsts.ONE;
import static com.ocrix.neuroph.common.CommonConsts.ZERO;
import static com.ocrix.neuroph.common.NetProperty.BIAS;
import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.Y_HIGH;
import static com.ocrix.neuroph.common.NetProperty.Y_LOW;
import static com.ocrix.neuroph.util.NeuralNetworkType.BAM;
import static com.ocrix.neuroph.util.TransferFunctionType.STEP;


import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.learning.BinaryHebbianLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuronProperties;

/**
 * <a href="http://goo.gl/n06Fr">Bidirectional Associative Memory.<a> The Bidirectional associative memory is
 * heteroassociative, content-addressable memory. A BAM consists of neurons
 * arranged in two layers say A and B. The neurons are bipolar binary. The
 * neurons in one layer are fully interconnected to the neurons in the second
 * layer. There is no interconnection among neurons in the same layer. The
 * weight from layer A to layer B is same as the weights from layer B to layer
 * A. dynamics involves two layers of interaction.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class BAM extends NeuralNetwork {
    /* The finger print of this class. */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of BAM network with specified number of neurons in
     * input and output layers.
     * 
     * @param inputNeuronsCount - number of neurons in input layer.
     * @param outputNeuronsCount - number of neurons in output layer.
     */
    public BAM(int inputNeuronsCount, int outputNeuronsCount) {

        /* Inits neuron settings for BAM network */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(NEURON_TYPE.to(), InputOutputNeuron.class);
        neuronProperties.setProperty(BIAS.to(), new Double(ZERO.value()));
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), STEP);
        neuronProperties.setProperty(Y_HIGH.to(), new Double(ONE.value()));
        neuronProperties.setProperty(Y_LOW.to(), new Double(ZERO.value()));

        this.createNetwork(inputNeuronsCount, outputNeuronsCount, neuronProperties);
    }

    /**
     * Creates BAM network architecture.
     * 
     * @param inputNeuronsCount - number of neurons in input layer.
     * @param outputNeuronsCount - number of neurons in output layer.
     * @param neuronProperties - neuron properties.
     */
    private void createNetwork(int inputNeuronsCount, int outputNeuronsCount, NeuronProperties neuronProperties) {

        /* Sets network type */
        this.setNetworkType(BAM);

        /* Creates the input layer */
        Layer inputLayer = LayerFactory.createLayer(inputNeuronsCount, neuronProperties);
        /* Adds the input layer to network */
        this.addLayer(inputLayer);

        /* Creates output layer */
        Layer outputLayer = LayerFactory.createLayer(outputNeuronsCount, neuronProperties);
        /* Adds output layer to network */
        this.addLayer(outputLayer);

        /* Creates full connectivity from in to out layer */
        ConnectionFactory.fullConnect(inputLayer, outputLayer);
        /* Creates full connectivity from out to in layer */
        ConnectionFactory.fullConnect(outputLayer, inputLayer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets Hebbian learning rule for this network */
        this.setLearningRule(new BinaryHebbianLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getNetworkType() != null) {
            builder.append("network type=");
            builder.append(getNetworkType());
            builder.append(", ");
        }
        if (getLearningRule() != null) {
            builder.append("learning rule=");
            builder.append(getLearningRule());
        }
        builder.append("]");
        return builder.toString();
    }
}
