/**
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.learning;

import java.io.Serializable;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.core.learning.SupervisedLearning;

/**
 * LMS learning rule for neural networks.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class LMS extends SupervisedLearning implements Serializable {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Creates a new LMS learning rule This learning rule is used to train
     * Adaline neural network, and this class is base for all LMS based learning
     * rules like PerceptronLearning, DeltaRule, SigmoidDeltaRule,
     * Backpropagation etc.
     */
    public LMS() {

    }

    /**
     * This method implements the weights update procedure for the whole network
     * for the given output error vector.
     * 
     * @param outputError
     *            output error vector for some network input- the difference
     *            between desired and actual output.
     * 
     * @see SupervisedLearning#calculateOutputError(double[], double[])
     * @see SupervisedLearning#learnPattern(com.ocrix.neuroph.core.learning.SupervisedTrainingElement)
     *      learnPattern
     */
    @Override
    protected void updateNetworkWeights(double[] outputError) {
        Validator.validateNeuralNetwork(neuralNetwork);
        int i = 0;
        /* For each neuron in output layer */
        for (Neuron neuron : neuralNetwork.getOutputNeurons()) {
            neuron.setError(outputError[i]); // set the neuron error, as
            /*
             * Calculates the difference between desired and actual output and
             * updates neuron weights
             */
            this.updateNeuronWeights(neuron);
            i++;
        }
    }

    /**
     * This method implements weights update procedure for the single neuron It
     * iterates through all neuron's input connections, and calculates/set
     * weight change for each weight using formula deltaWeight = learningRate *
     * neuronError * input
     * 
     * where neuronError is difference between desired and actual output for
     * specific neuron neuronError = desiredOutput[i] - actualOutput[i] (see
     * method SuprevisedLearning.calculateOutputError) <br>
     * <b style="color:RED">Note:</b> If you have some problem with neuron
     * weights, check the neuron error, if it's <code>0.0d</code> then 'no
     * change'.
     * 
     * @param neuron
     *            neuron to update weights.
     * 
     * @see LMS#updateNetworkWeights(double[])
     */
    protected void updateNeuronWeights(Neuron neuron) {
        /*
         * Gets the error(delta) for specified neuron, tanh can be used to
         * minimise the impact of big error values, which can cause network
         * instability suggested at
         * https://sourceforge.net/tracker/?func=detail&
         * atid=1107579&aid=3130561&group_id=238532 double neuronError =
         * Math.tanh(neuron.getError()); iterates through all neuron's input
         * connections
         */
        if (neuron != null) {
            double neuronError = neuron.getError();
            for (Connection connection : neuron.getInputConnections()) {
                /* Gets the input from current connection */
                double input = connection.getInput();
                /* Calculates the weight change */
                double weightChange = this.learningRate * neuronError * input;

                /* Gets the connection weight */
                Weight weight = connection.getWeight();
                /*
                 * if the learning is in online mode (not batch) applies the
                 * weight change immediately
                 */
                if (this.isInBatchMode() == false) {
                    weight.weightChange = weightChange;
                    weight.value += weightChange;
                } else {
                    /*
                     * Otherwise its in batch mode, so sums the weight changes
                     * and applies them later, after the current epoch (see
                     * SupervisedLearning.doLearningEpoch method)
                     */
                    weight.weightChange += weightChange;
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [learningRate=");
        builder.append(learningRate);
        builder.append("]");
        return builder.toString();
    }
}