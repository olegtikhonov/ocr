/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import java.util.logging.Logger;

import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Backpropagation learning rule with dynamic learning rate and momentum. <br>
 * The defaults are:
 * <ul>
 * <li>maxLearningRate = <code>0.9d</code></li>
 * <li>minLearningRate = <code>0.1d</code></li>
 * <li>learningRateChange = <code>0.99926d</code></li>
 * <li>useDynamicLearningRate = <code>true</code></li>
 * <li>maxMomentum = <code>0.9d</code></li>
 * <li>minMomentum = <code>0.1d</code></li>
 * <li>momentumChange = <code>0.99926d</code></li>
 * <li>useDynamicMomentum = <code>true</code></li>
 * </ul>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class DynamicBackPropagation extends MomentumBackpropagation {
    private static final long serialVersionUID = 1L;
    /* Class member declarations */
    private double maxLearningRate = 0.9d;
    private double minLearningRate = 0.1d;
    private double learningRateChange = 0.99926d;
    private boolean useDynamicLearningRate = true;
    private double maxMomentum = 0.9d;
    private double minMomentum = 0.1d;
    private double momentumChange = 0.99926d;
    private boolean useDynamicMomentum = true;
    /* Local logger */
    private static final Logger log = Logger
            .getLogger(DynamicBackPropagation.class.getName());

    /* End of class member declarations */

    /**
     * Constructs a {@link DynamicBackPropagation} with its defaults.
     */
    public DynamicBackPropagation() {
        super();
    }

    // Adjusting learning rate dynamically
    /*
     * If network error of current epoch is higher than the network error of the
     * previous epoch the learning rate is adjusted by minus 1 per cent of
     * current learning rate. Otherwise the learning rate is adjusted by plus 1
     * per cent of current learning rate. So, learning rate increases faster
     * than decreasing does. But if learning rate reaches 0.9 it switches back
     * to 0.5 to avoid endless training. The lowest learning rate is 0.5 also to
     * avoid endless training.
     */
    protected void adjustLearningRate() {

        // 1. First approach - probably the best
        // bigger error -> smaller learning rate; minimize the error growth
        // smaller error -> bigger learning rate; converege faster
        // the amount of earning rate change is proportional to error change -
        // by using errorChange

        double errorChange = this.previousEpochError - this.totalNetworkError;
        this.learningRate = this.learningRate
                + (errorChange * learningRateChange);

        if (this.learningRate > this.maxLearningRate) {
            this.learningRate = this.maxLearningRate;
        }

        if (this.learningRate < this.minLearningRate) {
            this.learningRate = this.minLearningRate;
        }

        // System.out.println("Learning rate: "+this.learningRate);

        // 2. Second approach
        // doing this lineary for each epoch considering network error behaviour
        // probbaly the worst one
        /*
         * if (this.totalNetworkError >= this.totalNetworkErrorInPreviousEpoch)
         * { this.learningRate = this.learningRate * this.learningRateChange; if
         * (this.learningRate < this.minLearningRate) this.learningRate =
         * this.minLearningRate; } else { this.learningRate = this.learningRate
         * * (1 + (1 - this.learningRateChange)); // *1.01 if (this.learningRate
         * > this.maxLearningRate) this.learningRate = this.maxLearningRate; }
         */
        // third approach used by sharky nn
        // By default It starts with ni = 0,9, and after each epoch ni is
        // changed by: 0,99977 ^ N
        // where N is number of points, and ^ is power.
        // ni = ni * 0,99977 ^ N
        // this one drops the learning rate too fast
        // this.learningRate = this.learningRate * Math.pow(learningRateChange,
        // this.getTrainingSet().size());
        // if (this.learningRate > this.maxLearningRate)
        // this.learningRate = this.maxLearningRate;
        //
        // if (this.learningRate < this.minLearningRate)
        // this.learningRate = this.minLearningRate;

        // System.out.println("Iteration: "+currentIteration +
        // " Learning rate: "+ this.learningRate);

        // one more approach suggested at
        // https://sourceforge.net/tracker/?func=detail&atid=1107579&aid=3130561&group_id=238532
        // if (this.totalNetworkError >= this.previousEpochError) {
        // // If going wrong way, drop to minimum learning and work our way back
        // up.
        // // This way we accelerate as we improve.
        // learningRate=minLearningRate;
        // } else {
        // this.learningRate = this.learningRate * (1 + (1 -
        // this.learningRateChange)); // *1.01
        //
        // if (this.learningRate > this.maxLearningRate)
        // this.learningRate = this.maxLearningRate;
        //
        // }

    }

    protected void adjustMomentum() {
        double errorChange = this.previousEpochError - this.totalNetworkError;
        this.momentum = this.momentum + (errorChange * momentumChange);

        if (this.momentum > this.maxMomentum) {
            this.momentum = this.maxMomentum;
        }

        if (this.momentum < this.minMomentum) {
            this.momentum = this.minMomentum;
        }

        log.info("momentum=" + momentum);

        // one more approach suggested at
        // https://sourceforge.net/tracker/?func=detail&atid=1107579&aid=3130561&group_id=238532
        // Probably want to drop momentum to minimum value.
        // if (this.totalNetworkError >= this.previousEpochError) {
        // momentum = momentum * momentumChange;
        // if (momentum < minMomentum) momentum = minMomentum;
        // } else {
        // momentum = momentum * (1 + (1 - momentumChange)); // *1.01
        // if (momentum > maxMomentum) momentum = maxMomentum;
        // }

    }

    @Override
    public void doLearningEpoch(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        super.doLearningEpoch(trainingSet);

        if (currentIteration > 0) {
            if (useDynamicLearningRate) {
                adjustLearningRate();
            }
            if (useDynamicMomentum) {
                adjustMomentum();
            }
        }

    }

    /**
     * Gets a Learning Rate Change.
     * 
     * @return learning rate change.
     * @see #setLearningRateChange(double)
     */
    public double getLearningRateChange() {
        return learningRateChange;
    }

    /**
     * Sets a learning rate change.
     * 
     * @param learningRateChange
     * @see #getLearningRateChange()
     */
    public void setLearningRateChange(double learningRateChange) {
        this.learningRateChange = learningRateChange;
    }

    /**
     * Gets a maximum learning rate.
     * 
     * @return - the maximum learning rate.
     * @see #setMaxLearningRate(double)
     */
    public double getMaxLearningRate() {
        return maxLearningRate;
    }

    /**
     * Sets a maximum learning rate.
     * 
     * @param maxLearningRate
     *            - the maximum learning rate.
     * @see #getMaxLearningRate()
     */
    public void setMaxLearningRate(double maxLearningRate) {
        this.maxLearningRate = maxLearningRate;
    }

    /**
     * Gets a maximum momentum.
     * 
     * @return - the max momentum.
     * @see #setMaxMomentum(double)
     */
    public double getMaxMomentum() {
        return maxMomentum;
    }

    /**
     * Sets a maximum momentum.
     * 
     * @param maxMomentum
     *            - the maximum momentum.
     * @see #getMaxMomentum()
     */
    public void setMaxMomentum(double maxMomentum) {
        this.maxMomentum = maxMomentum;
    }

    /**
     * Gets a minimum learning rate.
     * 
     * @return - the minimum learning rate.
     * @see #setMinLearningRate(double)
     */
    public double getMinLearningRate() {
        return minLearningRate;
    }

    /**
     * Sets a minimum learning rate.
     * 
     * @param minLearningRate
     *            - the min learning rate.
     * @see #getMinLearningRate()
     */
    public void setMinLearningRate(double minLearningRate) {
        this.minLearningRate = minLearningRate;
    }

    /**
     * Gets a minimum momentum.
     * 
     * @return - the min momentum.
     * @see #setMinMomentum(double)
     */
    public double getMinMomentum() {
        return minMomentum;
    }

    /**
     * Sets a minimum momentum.
     * 
     * @param minMomentum
     *            - the minimum momentum.
     * @see #getMinMomentum()
     */
    public void setMinMomentum(double minMomentum) {
        this.minMomentum = minMomentum;
    }

    /**
     * Gets momentum change.
     * 
     * @return - the momentum change.
     * @see #setMomentumChange(double)
     */
    public double getMomentumChange() {
        return momentumChange;
    }

    /**
     * Sets a momentum change.
     * 
     * @param momentumChange
     *            - the momentum change.
     * @see #getMomentumChange()
     */
    public void setMomentumChange(double momentumChange) {
        this.momentumChange = momentumChange;
    }

    /**
     * Gets a flag indicating use dynamic learning rate or not.
     * 
     * @return - the flag meaning use of dynamic learning rate or not.
     * @see #setUseDynamicLearningRate(boolean)
     */
    public boolean getUseDynamicLearningRate() {
        return useDynamicLearningRate;
    }

    /**
     * Sets a flag indicating use of dynamic learning rate or not.
     * 
     * @param useDynamicLearningRate
     *            - the flag, meaning use of dynamic learning rate.
     * @see #getUseDynamicLearningRate()
     */
    public void setUseDynamicLearningRate(boolean useDynamicLearningRate) {
        this.useDynamicLearningRate = useDynamicLearningRate;
    }

    /**
     * Gets a flag indicating use of dynamic momentum.
     * 
     * @return - the flag, meaning the use of dynamic momentum.
     * @see #setUseDynamicMomentum(boolean)
     */
    public boolean getUseDynamicMomentum() {
        return useDynamicMomentum;
    }

    /**
     * Sets a flag indicating use of dynamic momentum.
     * 
     * @param useDynamicMomentum
     *            - a flag,meaning the use of dynamic momentum
     * @see #getUseDynamicMomentum()
     */
    public void setUseDynamicMomentum(boolean useDynamicMomentum) {
        this.useDynamicMomentum = useDynamicMomentum;
    }
}
