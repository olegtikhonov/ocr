/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet.learning;

import java.util.List;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.transfer.TransferFunction;

/**
 * <a href="http://goo.gl/3wCnY">Back Propagation learning</a> rule for Multi
 * Layer Perceptron neural networks.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 * 
 */
public class BackPropagation extends SigmoidDeltaRule {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new instance of BackPropagation learning
     */
    public BackPropagation() {
        super();
    }

    /**
     * This method implements weight update procedure for the whole network for
     * the specified output error vector.
     * 
     * @param outputError
     *            - the output error vector.
     */
    @Override
    protected void updateNetworkWeights(double[] outputError) {
        Validator.validateDouble(outputError);
        /* inherited from SigmoidDeltaRule */
        this.calculateErrorAndUpdateOutputNeurons(outputError);
        /* implemented in this class */
        this.calculateErrorAndUpdateHiddenNeurons();

    }

    /**
     * This method implements weights adjustment for the hidden layers.
     */
    protected void calculateErrorAndUpdateHiddenNeurons() {
        int layerCount = this.neuralNetwork.getLayersCount();

        for (int i = layerCount - 2; i > 0; i--) {
            Layer layer = neuralNetwork.getLayerAt(i);

            for (Neuron neuron : layer.getNeurons()) {
                /* calculates the neuron's error (delta) */
                double neuronError = this.calculateHiddenNeuronError(neuron);
                neuron.setError(neuronError);
                this.updateNeuronWeights(neuron);
            } /* for closing */
        } /* for closing */
    }

    /**
     * Calculates and returns the neuron's error (neuron's delta) for the given
     * neuron param.
     * 
     * @param neuron
     *            - the neuron to calculate error for.
     * @return neuron error (delta) for the specified neuron.
     */
    protected double calculateHiddenNeuronError(Neuron neuron) {
        List<Connection> connectedTo = (neuron).getOutConnections();

        double deltaSum = 0d;
        for (Connection connection : connectedTo) {
            double delta = connection.getToNeuron().getError()
            * connection.getWeight().value;
            /* weighted delta sum from the next layer */
            deltaSum += delta;
        } /* for closing */

        TransferFunction transferFunction = neuron.getTransferFunction();
        /* should we use input of this or other neuron? */
        double netInput = neuron.getNetInput();
        double f1 = transferFunction.getDerivative(netInput);
        double neuronError = f1 * deltaSum;
        return neuronError;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }
}
