/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.nnet.learning;

import static com.ocrix.neuroph.common.CommonConsts.CYCLES;
import static com.ocrix.neuroph.common.CommonConsts.START_TEMP;
import static com.ocrix.neuroph.common.CommonConsts.TWO;

import java.util.Iterator;
import java.util.logging.Logger;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.learning.SupervisedLearning;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.util.NeuralNetworkCODEC;

/**
 * This class implements a simulated annealing learning rule for supervised
 * neural networks. It is based on the generic SimulatedAnnealing class. It is
 * used in the same manner as any other training class that implements the
 * SupervisedLearning interface.
 * 
 * <a href="http://goo.gl/zkpyA">Simulated annealing</a> is a common training
 * method. It is often used in conjunction with a propagation training method.
 * Simulated annealing can be very good when propagation training has reached a
 * local minimum.
 * 
 * The name and inspiration come from annealing in metallurgy, a technique
 * involving heating and controlled cooling of a material to increase the size
 * of its crystals and reduce their defects. The heat causes the atoms to become
 * unstuck from their initial positions (a local minimum of the internal energy)
 * and wander randomly through states of higher energy; the slow cooling gives
 * them more chances of finding configurations with lower internal energy than
 * the initial one.
 * 
 * @author Jeff Heaton (http://www.jeffheaton.com)
 */
public class SimulatedAnnealingLearning extends SupervisedLearning {
    //---------------------------------------------------------------------
    /* The serial id. */
    private static final long serialVersionUID = 1L;

    /* The neural network that is to be trained. */
    protected NeuralNetwork network;

    /* The starting temperature. */
    private final double startTemperature;

    /* The ending temperature. */
    private final double stopTemperature;

    /* The number of cycles that will be used. */
    private final int cycles;

    /* The current temperature. */
    protected double temperature;

    /* Current weights from the neural network. */
    private final double[] weights;

    /* Best weights so far. */
    private final double[] bestWeights;

    /* Local logger */
    private final static Logger log = Logger.getLogger(SimulatedAnnealingLearning.class.getName());
    //---------------------------------------------------------------------



    @Override
    protected void addToSquaredErrorSum(double[] patternError) {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    /**
     * Constructs a simulated annleaing trainer for a feed-forward neural network.
     * 
     * @param network
     *            The neural network to be trained.
     * @param startTemp
     *            The starting temperature.
     * @param stopTemp
     *            The ending temperature.
     * @param cycles
     *            The number of cycles in a training iteration.
     */
    public SimulatedAnnealingLearning(final NeuralNetwork network,
            final double startTemp, final double stopTemp, final int cycles) {
        this.network = network;
        this.temperature = startTemp;
        this.startTemperature = startTemp;
        this.stopTemperature = stopTemp;
        this.cycles = cycles;

        this.weights = new double[NeuralNetworkCODEC.determineArraySize(network)];
        this.bestWeights = new double[NeuralNetworkCODEC.determineArraySize(network)];

        NeuralNetworkCODEC.network2array(network, this.weights);
        NeuralNetworkCODEC.network2array(network, this.bestWeights);
    }

    /**
     * Constructs {@link SimulatedAnnealingLearning}.
     * 
     * @param network to be used in the created network.
     */
    public SimulatedAnnealingLearning(final NeuralNetwork network) {
        this(network, START_TEMP.value(), TWO.value(), CYCLES.value());
    }


    /**
     * Gets the best network from the training.
     * 
     * @return The best network.
     */
    public NeuralNetwork getNetwork() {
        return this.network;
    }

    /**
     * Randomize the weights and thresholds. This function does most of the work
     * of the class. Each call to this class will randomize the data according
     * to the current temperature. The higher the temperature the more
     * randomness.
     */
    public void randomize() {
        for (int i = 0; i < this.weights.length; i++) {
            double add = 0.5 - (Math.random());
            add /= this.startTemperature;
            add *= this.temperature;
            this.weights[i] = this.weights[i] + add;
        }

        NeuralNetworkCODEC.array2network(this.weights, this.network);
    }

    /**
     * Used internally to calculate the error for a training set.
     * 
     * @param trainingSet
     *            The training set to calculate for.
     * @return The error value.
     */
    private double determineError(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        Validator.validateNeuralNetwork(getNetwork());
        double result = 0d;

        @SuppressWarnings("unchecked")
        Iterator<TrainingElement> iterator = trainingSet.iterator();
        while (iterator.hasNext() && !isStopped()) {
            SupervisedTrainingElement supervisedTrainingElement = (SupervisedTrainingElement) iterator
            .next();
            double[] input = supervisedTrainingElement.getInput();

            getNetwork().setInput(input);
            getNetwork().calculate();

            /*this.neuralNetwork.setInput(input);
            this.neuralNetwork.calculate();
            double[] output = this.neuralNetwork.getOutput(); */

            double[] output = getNetwork().getOutput();

            double[] desiredOutput = supervisedTrainingElement.getDesiredOutput();
            double[] patternError = this.calculateOutputError(desiredOutput,
                    output);
            this.updateTotalNetworkError(patternError);

            double sqrErrorSum = 0;
            for (double error : patternError) {
                sqrErrorSum += (error * error);
            }
            result += sqrErrorSum / (2 * patternError.length);

        }

        return result;
    }

    /**
     * Performs one simulated annealing epoch.
     */
    @Override
    public void doLearningEpoch(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        System.arraycopy(this.weights, 0, this.bestWeights, 0, this.weights.length);
        double bestError = determineError(trainingSet);
        this.temperature = this.startTemperature;
        for (int i = 0; i < this.cycles; i++) {
            randomize();
            double currentError = determineError(trainingSet);

            if (currentError < bestError) {
                System.arraycopy(this.weights, 0, this.bestWeights, 0, this.weights.length);
                bestError = currentError;
            } else{
                System.arraycopy(this.bestWeights, 0, this.weights, 0, this.weights.length);
            }

            NeuralNetworkCODEC.array2network(this.bestWeights, network);

            final double ratio = Math.exp(Math.log(this.stopTemperature / this.startTemperature) / (this.cycles - 1));
            this.temperature *= ratio;
        }

        this.previousEpochError = this.totalNetworkError;
        this.totalNetworkError = bestError;
        // moved stopping condition to separate method hasReachedStopCondition()
        // so it can be overriden / customized in subclasses
        if (hasReachedStopCondition()) {
            stopLearning();
        }
    }

    /**
     * Updates the total error.
     */
    protected void updateTotalNetworkError(double[] patternError) {
        Validator.validateDouble(patternError);
        double sqrErrorSum = 0;
        for (double error : patternError) {
            sqrErrorSum += (error * error);
        }
        this.totalNetworkError += sqrErrorSum / (2 * patternError.length);
    }

    /**
     * Not used.
     */
    @Override
    protected void updateNetworkWeights(double[] patternError) {
        log.warning("updateNetworkWeights(double[] patternError) method is not in use :-(");
    }



    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());

        builder.append(" [ start temperature=");// +
        builder.append(this.startTemperature);
        builder.append(", stop temperature=");
        builder.append(this.stopTemperature);
        builder.append(", cycles=");
        builder.append(cycles);
        return builder.toString();
    }
}