/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import java.util.List;
import java.util.Vector;

import static com.ocrix.neuroph.common.NetProperty.INPUT_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.USE_BIAS;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.WeightedSum;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.InputNeuron;
import com.ocrix.neuroph.nnet.flat.FlatNetworkPlugin;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;
import com.ocrix.neuroph.util.random.NguyenWidrowRandomizer;

/**
 * Multi Layer Perceptron neural network with Back propagation learning
 * algorithm.
 * 
 * @see com.ocrix.neuroph.nnet.learning.BackPropagation
 * @see com.ocrix.neuroph.nnet.learning.MomentumBackpropagation
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class MultiLayerPerceptron extends NeuralNetwork {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;
    private static final double MIN = -0.7d;
    private static final double MAX = 0.7d;
    

    
    /**
     * Creates new MultiLayerPerceptron with specified number of neurons in
     * layers.
     * 
     * @param neuronsInLayers
     *            collection of neuron number in layers.
     */
    public MultiLayerPerceptron(List<Integer> neuronsInLayers) {
        // init neuron settings
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(USE_BIAS.to(), true);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.SIGMOID);

        this.createNetwork(neuronsInLayers, neuronProperties);
    }

    public MultiLayerPerceptron(int... neuronsInLayers) {
        /* Inits neuron settings */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(USE_BIAS.to(), true);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.SIGMOID);
        neuronProperties.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);

        Vector<Integer> neuronsInLayersVector = new Vector<Integer>();

        for (int i = 0; i < neuronsInLayers.length; i++) {
            neuronsInLayersVector.add(new Integer(neuronsInLayers[i]));
        }

        this.createNetwork(neuronsInLayersVector, neuronProperties);
    }

    /**
     * Creates a {@link MultiLayerPerceptron}.
     * 
     * @param transferFunctionType a {@link TransferFunctionType}.
     * @param neuronsInLayers
     */
    public MultiLayerPerceptron(TransferFunctionType transferFunctionType, int... neuronsInLayers) {
        Validator.validateTransferFunctionType(transferFunctionType);
        /* Inits neuron settings */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(USE_BIAS.to(), true);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), transferFunctionType);
        neuronProperties.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);

        Vector<Integer> neuronsInLayersVector = new Vector<Integer>();
        
        for (int i = 0; i < neuronsInLayers.length; i++){
            neuronsInLayersVector.add(new Integer(neuronsInLayers[i]));
        }

        this.createNetwork(neuronsInLayersVector, neuronProperties);
    }

    /**
     * Constructs {@link MultiLayerPerceptron} taking list of layers and transfer function.
     * 
     * @param neuronsInLayers a list of layers.
     * @param transferFunctionType {@link TransferFunctionType}.
     * 
     * @throws IllegalArgumentException if any parameter is null.
     */
    public MultiLayerPerceptron(List<Integer> neuronsInLayers, TransferFunctionType transferFunctionType) {
        Validator.validateList(neuronsInLayers);
        Validator.validateTransferFunctionType(transferFunctionType);
        /* Inits neuron settings */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(USE_BIAS.to(), true);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), transferFunctionType);

        this.createNetwork(neuronsInLayers, neuronProperties);
    }

    /**
     * Creates new MultiLayerPerceptron net with specified number neurons in
     * getLayersIterator.
     * 
     * @param neuronsInLayers
     *            collection of neuron numbers in layers.
     * @param neuronProperties
     *            neuron properties.
     * @throws IllegalArgumentException
     *             if any parameter is null;
     */
    public MultiLayerPerceptron(List<Integer> neuronsInLayers, NeuronProperties neuronProperties) {
        Validator.validateList(neuronsInLayers);
        Validator.validateProperties(neuronProperties);
        this.createNetwork(neuronsInLayers, neuronProperties);
    }

    /**
     * Creates MultiLayerPerceptron Network architecture - fully connected feed
     * forward with specified number of neurons in each layer
     * 
     * @param neuronsInLayers
     *            collection of neuron numbers in getLayersIterator
     * @param neuronProperties
     *            neuron properties
     */
    private void createNetwork(List<Integer> neuronsInLayers, NeuronProperties neuronProperties) {
        /* Uses bias neurons by default */
        boolean useBias = true;
        
        /* Sets network type */
        this.setNetworkType(NeuralNetworkType.MULTI_LAYER_PERCEPTRON);

        /* Creates input layer */
        NeuronProperties inputNeuronProperties = new NeuronProperties(InputNeuron.class, Linear.class);
        Layer layer = LayerFactory.createLayer(neuronsInLayers.get(0), inputNeuronProperties);

        
        if (neuronProperties.hasProperty(USE_BIAS.to())) {
            useBias = (Boolean) neuronProperties.getProperty(USE_BIAS.to());
        }

        if (useBias) {
            layer.addNeuron(new BiasNeuron());
        }

        this.addLayer(layer);

        /* creates layers */
        Layer prevLayer = layer;

        for (int layerIdx = 1; layerIdx < neuronsInLayers.size(); layerIdx++) {
            Integer neuronsNum = neuronsInLayers.get(layerIdx);
            /* Creates layer */
            layer = LayerFactory.createLayer(neuronsNum, neuronProperties);

            if (useBias && (layerIdx < (neuronsInLayers.size() - 1))) {
                layer.addNeuron(new BiasNeuron());
            }

            /* Adds created layer to network */
            this.addLayer(layer);
            /* Creates full connectivity between previous and this layer */
            if (prevLayer != null) {
                ConnectionFactory.fullConnect(prevLayer, layer);
            }

            prevLayer = layer;
        }

        /* Sets input and output cells for network */
        NeuralNetworkFactory.setDefaultIO(this);

        /* Sets learnng rule */
        this.setLearningRule(new MomentumBackpropagation());
        NguyenWidrowRandomizer rand = NguyenWidrowRandomizer.getInstance();
        rand.setNguyenWidrowRandomizer(MIN, MAX);
        this.randomizeWeights(rand);

        /* sets flatten the network, if desired */
        if (Neuroph.getInstance().shouldFlattenNetworks()) {
            FlatNetworkPlugin.flattenNeuralNetworkNetwork(this);
        }
    }

    /**
     * Fully connects first and last layer.
     */
    public void connectInputsToOutputs() {
        /* Connects first and last layer */
        ConnectionFactory.fullConnect(getLayers().get(0), getLayers().get(getLayers().size() - 1), false);
    }
}