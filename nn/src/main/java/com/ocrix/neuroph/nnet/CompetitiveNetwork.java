/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.nnet;

import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.SUMMING_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.WEIGHTS_FUNCTION;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.input.Sum;
import com.ocrix.neuroph.core.input.WeightedInput;
import com.ocrix.neuroph.nnet.comp.CompetitiveLayer;
import com.ocrix.neuroph.nnet.comp.CompetitiveNeuron;
import com.ocrix.neuroph.nnet.learning.CompetitiveLearning;
import com.ocrix.neuroph.util.ConnectionFactory;
import com.ocrix.neuroph.util.LayerFactory;
import com.ocrix.neuroph.util.NeuralNetworkFactory;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.NeuronProperties;
import com.ocrix.neuroph.util.TransferFunctionType;

/**
 * Two layer neural network with competitive learning rule.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class CompetitiveNetwork extends NeuralNetwork {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates new competitive network with specified neuron number.
     * 
     * @param inputNeuronsCount
     *            number of input neurons.
     * @param outputNeuronsCount
     *            number of output neurons.
     */
    public CompetitiveNetwork(int inputNeuronsCount, int outputNeuronsCount) {
        this.createNetwork(inputNeuronsCount, outputNeuronsCount);
    }

    /**
     * Creates Competitive network architecture.
     * 
     * @param inputNeuronsCount
     *            input neurons number.
     * @param outputNeuronsCount
     *            output neurons number.
     * @param neuronProperties
     *            neuron properties.
     */
    private void createNetwork(int inputNeuronsCount, int outputNeuronsCount) {
        Validator.validateIntInRange(inputNeuronsCount, 1, Integer.MAX_VALUE);
        Validator.validateIntInRange(outputNeuronsCount, 1, Integer.MAX_VALUE);
        
        /* Sets a network type */
        this.setNetworkType(NeuralNetworkType.COMPETITIVE);

        /* Creates the input layer */
        Layer inputLayer = LayerFactory.createLayer(inputNeuronsCount, new NeuronProperties());
        this.addLayer(inputLayer);

        /* Creates properties for neurons in output layer */
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty(NEURON_TYPE.to(), CompetitiveNeuron.class);
        neuronProperties.setProperty(WEIGHTS_FUNCTION.to(), WeightedInput.class);
        neuronProperties.setProperty(SUMMING_FUNCTION.to(), Sum.class);
        neuronProperties.setProperty(TRANSFER_FUNCTION.to(), TransferFunctionType.RAMP);

        /* Creates full connectivity in competitive layer */
        CompetitiveLayer competitiveLayer = new CompetitiveLayer(outputNeuronsCount, neuronProperties);
        /* Adds competitive layer to network */
        this.addLayer(competitiveLayer);

        //TODO: check for division by zero
        double competitiveWeight = -(1 / (double) outputNeuronsCount);
        /* Creates full connectivity within competitive layer */
        ConnectionFactory.fullConnect(competitiveLayer, competitiveWeight, 1);

        /* Creates full connectivity from input to competitive layer */
        ConnectionFactory.fullConnect(inputLayer, competitiveLayer);

        /* Sets input and output cells for this network */
        NeuralNetworkFactory.setDefaultIO(this);

        this.setLearningRule(new CompetitiveLearning());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getInputNeurons() != null) {
            builder.append("input neurons=");
            builder.append(getInputNeurons().size());
        }

        if (getOutputNeurons() != null) {
            builder.append(", output neurons=");
            builder.append(getOutputNeurons().size());
        }

        builder.append("]");
        return builder.toString();
    }
}
