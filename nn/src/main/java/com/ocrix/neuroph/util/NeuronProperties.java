/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.NetProperty.INPUT_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.NEURON_TYPE;
import static com.ocrix.neuroph.common.NetProperty.SUMMING_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;
import static com.ocrix.neuroph.common.NetProperty.USE_BIAS;
import static com.ocrix.neuroph.common.NetProperty.WEIGHTS_FUNCTION;

import java.util.Enumeration;

import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.input.SummingFunction;
import com.ocrix.neuroph.core.input.WeightedSum;
import com.ocrix.neuroph.core.input.WeightsFunction;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.core.transfer.TransferFunction;

/**
 * Represents properties of a neuron.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class NeuronProperties extends Properties {
    /* Finger print of the class */
    private static final long serialVersionUID = 2L;

    /**
     * Constructs the {@link NeuronProperties}.
     */
    public NeuronProperties() {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
        this.setProperty(TRANSFER_FUNCTION.to(), Linear.class);
        this.setProperty(NEURON_TYPE.to(), Neuron.class);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param neuronClass
     *            to be set as a neuron type.
     */
    public NeuronProperties(Class<? extends Neuron> neuronClass) {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
        this.setProperty(TRANSFER_FUNCTION.to(), Linear.class);
        this.setProperty(NEURON_TYPE.to(), neuronClass);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param neuronClass
     *            to be set as a neuron type.
     * @param transferFunctionClass
     *            to be set as a {@link TransferFunctionType}.
     */
    public NeuronProperties(Class<? extends Neuron> neuronClass,
            Class<? extends TransferFunction> transferFunctionClass) {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
        this.setProperty(TRANSFER_FUNCTION.to(), transferFunctionClass);
        this.setProperty(NEURON_TYPE.to(), neuronClass);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param neuronClass
     *            to be set as a neuron type.
     * @param inputFunctionClass
     *            to be set as a {@link InputFunction}.
     * @param transferFunctionClass
     *            to be set as a {@link TransferFunctionType}.
     */
    public NeuronProperties(Class<? extends Neuron> neuronClass,
            Class<? extends InputFunction> inputFunctionClass,
            Class<? extends TransferFunction> transferFunctionClass) {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), inputFunctionClass);
        this.setProperty(TRANSFER_FUNCTION.to(), transferFunctionClass);
        this.setProperty(NEURON_TYPE.to(), neuronClass);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param neuronClass
     *            to be set as a neuron type.
     * @param weightsFunctionClass
     * @param summingFunctionClass
     * @param transferFunctionClass
     */
    public NeuronProperties(Class<? extends Neuron> neuronClass,
            Class<? extends WeightsFunction> weightsFunctionClass,
            Class<? extends SummingFunction> summingFunctionClass,
            Class<? extends TransferFunction> transferFunctionClass) {
        initKeys();
        this.setProperty(NEURON_TYPE.to(), neuronClass);
        this.setProperty(WEIGHTS_FUNCTION.to(), weightsFunctionClass);
        this.setProperty(SUMMING_FUNCTION.to(), summingFunctionClass);
        this.setProperty(TRANSFER_FUNCTION.to(), transferFunctionClass);
    }

    // @Deprecated
    // public NeuronProperties(TransferFunctionType transferFunctionType) {
    // initKeys();
    // // this.setProperty(WEIGHTS_FUNCTION.to(), WeightedInput.class);
    // // this.setProperty(SUMMING_FUNCTION.to(), Sum.class);
    // this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
    // this.setProperty(TRANSFER_FUNCTION.to(),
    // transferFunctionType.getTypeClass());
    // this.setProperty(NEURON_TYPE.to(), Neuron.class);
    // }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param neuronClass
     *            to be set as a neuron type.
     * @param transferFunctionClass
     *            to be set as a {@link TransferFunctionType}.
     */
    public NeuronProperties(Class<? extends Neuron> neuronClass,
            TransferFunctionType transferFunctionType) {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
        this.setProperty(TRANSFER_FUNCTION.to(),
                transferFunctionType.getTypeClass());
        this.setProperty(NEURON_TYPE.to(), neuronClass);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param transferFunctionType
     *            to be set as a {@link TransferFunctionType}.
     * @param useBias
     *            indicates if use a bias or not.
     */
    public NeuronProperties(TransferFunctionType transferFunctionType,
            boolean useBias) {
        initKeys();
        this.setProperty(INPUT_FUNCTION.to(), WeightedSum.class);
        this.setProperty(TRANSFER_FUNCTION.to(),
                transferFunctionType.getTypeClass());
        this.setProperty(USE_BIAS.to(), useBias);
        this.setProperty(NEURON_TYPE.to(), Neuron.class);
    }

    /**
     * Constructs the {@link NeuronProperties}.
     * 
     * @param weightsFunctionType
     *            to be set as a {@link WeightsFunctionType}.
     * @param summingFunctionType
     *            to be set as a {@link SummingFunctionType}.
     * @param transferFunctionType
     *            to be set as a {@link TransferFunctionType}.
     */
    public NeuronProperties(WeightsFunctionType weightsFunctionType,
            SummingFunctionType summingFunctionType,
            TransferFunctionType transferFunctionType) {
        initKeys();
        this.setProperty(WEIGHTS_FUNCTION.to(),
                weightsFunctionType.getTypeClass());
        this.setProperty(SUMMING_FUNCTION.to(),
                summingFunctionType.getTypeClass());
        this.setProperty(TRANSFER_FUNCTION.to(),
                transferFunctionType.getTypeClass());
        this.setProperty(NEURON_TYPE.to(), Neuron.class);
    }

    /*
     * <TRANSLATED> TODO: override setProperty to the enum type which takes the
     * appropriate class
     */
    private void initKeys() {
        createKeys(WEIGHTS_FUNCTION.to(), SUMMING_FUNCTION.to(),
                INPUT_FUNCTION.to(), TRANSFER_FUNCTION.to(), NEURON_TYPE.to(),
                USE_BIAS.to());
        // <TRANSLATED> switch to the Neural Network Properties
    }

    /**
     * Gets a weighted function.
     * 
     * @return
     */
    public Class<?> getWeightsFunction() {
        return (Class<?>) this.get(WEIGHTS_FUNCTION.to());
    }

    /**
     * Gets a summing function.
     * 
     * @return
     */
    public Class<?> getSummingFunction() {
        return (Class<?>) this.get(SUMMING_FUNCTION.to());
    }

    /**
     * Gets the input function.
     * 
     * @return
     */
    public Class<?> getInputFunction() {
        Object val = this.get(INPUT_FUNCTION.to());
        if (!val.equals(""))
            return (Class<?>) val;
        return null;
    }

    /**
     * Gets transfer function.
     * 
     * @return
     */
    public Class<?> getTransferFunction() {
        return (Class<?>) this.get(TRANSFER_FUNCTION.to());
    }

    /**
     * Gets a neuron type.
     * 
     * @return
     */
    public Class<?> getNeuronType() {
        return (Class<?>) this.get(NEURON_TYPE.to());
    }

    /**
     * Gets transfer function properties.
     * 
     * @return
     */
    public Properties getTransferFunctionProperties() {
        Properties tfProperties = new Properties();
        Enumeration<?> en = this.keys();
        while (en.hasMoreElements()) {
            String name = en.nextElement().toString();
            if (name.contains(TRANSFER_FUNCTION.to())) {
                tfProperties.setProperty(name, this.get(name));
            }
        }
        return tfProperties;
    }

    @Override
    public final void setProperty(String key, Object value) {
        // TODO: Remove instanceOf
        if (value instanceof TransferFunctionType)
            value = ((TransferFunctionType) value).getTypeClass();
        if (value instanceof WeightsFunctionType)
            value = ((WeightsFunctionType) value).getTypeClass();
        if (value instanceof SummingFunctionType)
            value = ((SummingFunctionType) value).getTypeClass();
        this.put(key, value);
    }
}