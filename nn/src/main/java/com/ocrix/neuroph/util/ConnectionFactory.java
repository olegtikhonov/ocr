/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.Weight;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.comp.DelayedConnection;

/**
 * Provides methods to connect neurons by creating Connection objects.
 */
public class ConnectionFactory {

    /**
     * Private constructor. All functors are public static.
     */
    private ConnectionFactory() {
    }

    /**
     * Creates connection between two specified neurons.
     * 
     * @param fromNeuron
     *            - the output neuron.
     * @param toNeuron
     *            - the input neuron.
     */
    public static void createConnection(Neuron fromNeuron, Neuron toNeuron) {
        // TODO: add validators
        Connection connection = new Connection(fromNeuron, toNeuron);
        toNeuron.addInputConnection(connection);
    }

    /**
     * Creates connection between two specified neurons.
     * 
     * @param fromNeuron
     *            - the neuron to connect (connection source).
     * @param toNeuron
     *            - the neuron to connect to (connection target).
     * @param weightVal
     *            - the connection weight value.
     */
    public static void createConnection(Neuron fromNeuron, Neuron toNeuron,
            double weightVal) {
        // TODO: add validators
        Connection connection = new Connection(fromNeuron, toNeuron, weightVal);
        toNeuron.addInputConnection(connection);
    }

    /**
     * Creates {@link Connection}.
     * 
     * @param fromNeuron
     *            - what a neuron to begin with.
     * @param toNeuron
     *            - what a neuron to connect to.
     * @param weightVal
     *            - a weight value for the connection.
     * @param delay
     *            - a time for the delayed connection.
     */
    public static void createConnection(Neuron fromNeuron, Neuron toNeuron,
            double weightVal, int delay) {
        // TODO: add validators
        DelayedConnection connection = new DelayedConnection(fromNeuron,
                toNeuron, weightVal, delay);
        toNeuron.addInputConnection(connection);
    }

    /**
     * Creates connection between two specified neurons.
     * 
     * @param fromNeuron
     *            - the neuron to connect (connection source).
     * @param toNeuron
     *            - the neuron to connect to (connection target).
     * @param weight
     *            - the connection weight.
     */
    public static void createConnection(Neuron fromNeuron, Neuron toNeuron,
            Weight weight) {
        // TODO: add validators
        Connection connection = new Connection(fromNeuron, toNeuron, weight);
        toNeuron.addInputConnection(connection);
    }

    /**
     * Creates full connectivity between the two specified layers.
     * 
     * @param fromLayer
     *            - the layer to connect.
     * @param toLayer
     *            - the layer to connect to.
     */
    public static void fullConnect(Layer fromLayer, Layer toLayer) {
        Validator.validateLayer(fromLayer, toLayer);
        for (Neuron fromNeuron : fromLayer.getNeurons()) {
            for (Neuron toNeuron : toLayer.getNeurons()) {
                createConnection(fromNeuron, toNeuron);
            }
        }
    }

    /**
     * Creates full connectivity between the two specified layers.
     * 
     * @param fromLayer
     *            - the layer to connect.
     * @param toLayer
     *            - the layer to connect to.
     * 
     * @param connectBiasNeuron
     *            - use either a bias neuron or not.
     * 
     * <br>
     *            <b style="color:RED">Note:</b> there is no any special action
     *            for this flag.
     */
    public static void fullConnect(Layer fromLayer, Layer toLayer,
            boolean connectBiasNeuron) {
        if (connectBiasNeuron) {
            for (Neuron fromNeuron : fromLayer.getNeurons()) {
                if (fromNeuron.getClass().getName()
                        .contains(BiasNeuron.class.getName())) {
                    for (Neuron toNeuron : toLayer.getNeurons()) {
                        createConnection(fromNeuron, toNeuron);
                    }
                }
            }
        }
    }

    /**
     * Creates full connectivity between two specified layers with specified
     * weight for all connections.
     * 
     * @param fromLayer
     *            - the output layer.
     * @param toLayer
     *            - the input layer.
     * @param weightVal
     *            - the connection weight value.
     */
    public static void fullConnect(Layer fromLayer, Layer toLayer,
            double weightVal) {
        for (Neuron fromNeuron : fromLayer.getNeurons()) {
            for (Neuron toNeuron : toLayer.getNeurons()) {
                createConnection(fromNeuron, toNeuron, weightVal);
            }
        }
    }

    /**
     * Creates full connectivity within layer - each neuron with all other
     * within the same layer.
     * 
     * @throws IllegalArgumentException
     *             if layer is null.
     */
    public static void fullConnect(Layer layer) {
        Validator.validateLayer(layer);
        int neuronNum = layer.getNeuronsCount();
        for (int i = 0; i < neuronNum; i++) {
            for (int j = 0; j < neuronNum; j++) {
                if (j == i)
                    continue;
                Neuron from = layer.getNeuronAt(i);
                Neuron to = layer.getNeuronAt(j);
                createConnection(from, to);
            } // j
        } // i
    }

    /**
     * Creates full connectivity within layer - each neuron with all other
     * within the same layer with the specified weight values for all
     * connections.
     * 
     * @throws IllegalArgumentException
     *             if layer is null.
     */
    public static void fullConnect(Layer layer, double weightVal) {
        Validator.validateLayer(layer);
        int neuronNum = layer.getNeuronsCount();
        for (int i = 0; i < neuronNum; i++) {
            for (int j = 0; j < neuronNum; j++) {
                if (j == i)
                    continue;
                Neuron from = layer.getNeuronAt(i);
                Neuron to = layer.getNeuronAt(j);
                createConnection(from, to, weightVal);
            } // j
        } // i
    }

    /**
     * Creates full connectivity within layer - each neuron with all other
     * within the same layer with the specified weight and delay values for all
     * connections.
     * 
     * @throws IllegalArgumentException
     *             if layer is null.
     */
    public static void fullConnect(Layer layer, double weightVal, int delay) {
        Validator.validateLayer(layer);
        int neuronNum = layer.getNeuronsCount();
        for (int i = 0; i < neuronNum; i++) {
            for (int j = 0; j < neuronNum; j++) {
                if (j == i) {
                    continue;
                }
                Neuron from = layer.getNeuronAt(i);
                Neuron to = layer.getNeuronAt(j);
                createConnection(from, to, weightVal, delay);
            } // j
        } // i
    }

    /**
     * Creates forward connectivity pattern between the specified layers.
     * 
     * @param fromLayer
     *            - the layer to connect.
     * @param toLayer
     *            - the layer to connect to.
     * @throws IllegalArgumentException if one of the {@link Layer}s is NULL.
     */
    public static void forwardConnect(Layer fromLayer, Layer toLayer, double weightVal) {
        Validator.validateLayer(fromLayer, toLayer);
        for (int i = 0; i < fromLayer.getNeuronsCount(); i++) {
            Neuron fromNeuron = fromLayer.getNeuronAt(i);
            Neuron toNeuron = toLayer.getNeuronAt(i);
            createConnection(fromNeuron, toNeuron, weightVal);
        }
    }

    /**
     * Creates forward connection pattern between specified layers. Connects all
     * neurons from first layer to the all neurons of the second layer, as input
     * connection, i.e. <br>
     * <code>toLayer.getNeuron(i).addInputConnection(fromLayer.getNeuron(i))</code>
     * .
     * 
     * @param fromLayer
     *            - the layer to connect.
     * @param toLayer
     *            - the layer to connect to.
     * @throws IllegalArgumentException
     *             if one of the {@link Layer}s is NULL.
     */
    public static void forwardConnect(Layer fromLayer, Layer toLayer) {
        Validator.validateLayer(fromLayer, toLayer);
        for (int i = 0; i < fromLayer.getNeuronsCount(); i++) {
            Neuron fromNeuron = fromLayer.getNeuronAt(i);
            Neuron toNeuron = toLayer.getNeuronAt(i);
            createConnection(fromNeuron, toNeuron, 1);
        }
    }
}