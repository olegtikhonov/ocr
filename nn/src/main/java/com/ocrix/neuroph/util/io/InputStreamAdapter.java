/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_CLOSE_STREAM;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_READ_INPUT;
import static com.ocrix.neuroph.common.CommonMessages.WARN_BUFFERED_READER_IS_NULL;
import static com.ocrix.neuroph.common.CommonMessages.WARN_CANNOT_PARSE;
import static com.ocrix.neuroph.common.CommonMessages.WARN_INPUT_STREAM_IS_NULL;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.util.VectorParser;

/**
 * Implementation of {@link InputAdapter}.
 */
public class InputStreamAdapter implements InputAdapter {
    /* Class member declarations */
    protected BufferedReader bufferedReader;
    private File file = null;
    private Scanner scanner = null;
    /* Local logger */
    private final static Logger log = Logger.getLogger(InputStreamAdapter.class
            .getName());

    /* End of class member declarations */

    /**
     * Constructs an input stream adapter.
     * 
     * @param an
     *            input stream
     */
    public InputStreamAdapter(InputStream inputStream) {
        if (inputStream != null) {
            bufferedReader = new BufferedReader(new InputStreamReader(
                    inputStream));
        } else {
            log.warning(WARN_INPUT_STREAM_IS_NULL.to());
        }
    }

    /**
     * Constructs an input stream adapter.
     * 
     * @param bufferedReader
     */
    public InputStreamAdapter(BufferedReader bufferedReader) {
        if (bufferedReader != null) {
            this.bufferedReader = bufferedReader;
        } else {
            log.warning(WARN_BUFFERED_READER_IS_NULL.to());
        }
    }

    /**
     * This method unlike others uses {@link Scanner} instead of
     * {@link BufferedReader}.
     * 
     * @param file
     * @throws IOException
     */
    public InputStreamAdapter(File file) throws IOException {
        Validator.validateFile(file);
        this.setFile(file);
        scanner = new Scanner(file);
    }

    /**
     * Reads an input stream.
     * 
     * @return double array.
     */
    public double[] readInput() {
        try {
            double[] inputBuffer = null;

            if (scanner != null) {
                inputBuffer = useScanner();
            } else
                inputBuffer = useBufferedReader();

            return inputBuffer;
        } catch (IOException ex) {
            log.severe(ERROR_READ_INPUT.to());
            throw new NeurophInputException(ERROR_READ_INPUT.to(), ex);
        }
    }

    /**
     * Closes the {@link BufferedReader}.
     */
    public void close() {
        try {
            if (scanner != null) {
                scanner.close();
            }

            if (bufferedReader != null) {
                bufferedReader.close();
            }

        } catch (IOException ex) {
            log.severe(ERROR_CLOSE_STREAM.to());
            throw new NeurophInputException(ERROR_CLOSE_STREAM.to(), ex);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }

    public File getFile() {
        return file;
    }

    /**
     * Sets a file which contains a double[].
     * 
     * @param file
     *            to be set.
     * 
     * @throws IllegalArgumentException
     *             if file is null.
     */
    public void setFile(File file) {
        Validator.validateFile(file);
        this.file = file;
    }

    /**
     * Uses scanner for reading input stream.
     * 
     * @return parsed double array.
     */
    private double[] useScanner() {
        double[] inputBuffer = null;
        if (scanner.hasNext()) {
            String inputLine = scanner.nextLine();

            inputBuffer = VectorParser.parseDoubleArray(inputLine);

            if (inputLine != null) {
                inputBuffer = VectorParser.parseDoubleArray(inputLine);
                return inputBuffer;
            }
        }
        return inputBuffer;
    }

    /**
     * Uses {@link BufferedReader} for stream reading.
     * 
     * @return a parsed double array.
     * 
     * @throws IOException
     */
    private double[] useBufferedReader() throws IOException {
        double[] inputBuffer = null;

        if (bufferedReader != null) {
            String inputLine = bufferedReader.readLine();

            if (inputLine != null) {
                inputBuffer = VectorParser.parseDoubleArray(inputLine);
            }
        } else {
            log.warning(WARN_CANNOT_PARSE.to());
        }

        return inputBuffer;
    }
}