package com.ocrix.neuroph.util.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;


public class URLOutputAdapter extends OutputStreamAdapter {
    
    public URLOutputAdapter(URL url) throws IOException {
        super(new BufferedWriter(new OutputStreamWriter(url.openConnection().getOutputStream())));     
    }  
    
    public URLOutputAdapter(String url) throws MalformedURLException, IOException {
        this(new URL(url));
    }
}