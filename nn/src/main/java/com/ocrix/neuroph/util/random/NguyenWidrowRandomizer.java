/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.random;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;

/**
 * Based on NguyenWidrowRandomizer from Encog. The main idea of the
 * Nguyen-Widrow algorithm is as follows. We pick small random values as initial
 * weights of the neural network. The weights are then modified in such a manner
 * that the region of interest is divided into small intervals. It is then
 * reasonable to consider speeding up the training process by setting the
 * initial weights of the first layer so that each node is assigned its own
 * interval at the start of training. As the network is trained, each hidden
 * node still has the freedom to adjust its interval size and location during
 * training. However, most of these adjustments will probably be small since the
 * majority of the weight movements were eliminated by Nguyen-Widrow method of
 * setting their initial values.
 * 
 * @author zoran
 */
public class NguyenWidrowRandomizer extends RangeRandomizer {

    private static NguyenWidrowRandomizer instance = null;

    private NguyenWidrowRandomizer() {
        super();
    }

    /**
     * Return {@link NguyenWidrowRandomizer}.
     * 
     * @return
     */
    public static NguyenWidrowRandomizer getInstance() {
        if (instance == null)
            instance = new NguyenWidrowRandomizer();
        return instance;
    }

    /**
     * Constructs Nguyen Widrow Randomizer.
     * 
     * @param min
     *            - a minimum
     * @param max
     *            - a maximum
     */
    public void setNguyenWidrowRandomizer(double min, double max) {
        RangeRandomizer.getInstance().setRangeRandomizer(min, max);
    }

    @Override
    public void randomize(NeuralNetwork neuralNetwork) {
        super.randomize(neuralNetwork);

        int inputNeuronsCount = neuralNetwork.getInputNeurons().size();
        int hiddenNeuronsCount = 0;

        for (int i = 1; i < neuralNetwork.getLayersCount() - 1; i++) {
            hiddenNeuronsCount += neuralNetwork.getLayerAt(i).getNeuronsCount();
        }

        /*
         * Should we use the total number of hidden neurons or different norm
         * for each layer
         */
        double beta = 0.7 * Math.pow(hiddenNeuronsCount,
                1.0 / inputNeuronsCount);

        for (Layer layer : neuralNetwork.getLayers()) {
            /*
             * Calculates the Euclidean Norm for the weights: norm += value *
             * value - sum of weighted squares in the layer
             */
            double norm = 0.0;
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                    double weight = connection.getWeight().getValue();
                    norm += weight * weight;
                }
            }
            norm = Math.sqrt(norm);

            /* Rescales the weights using beta and the norm: beta * value / norm */
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                    double weight = connection.getWeight().getValue();
                    weight = beta * weight / norm;
                    connection.getWeight().setValue(weight);
                }
            }
        }
    }
}