/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ocrix.neuroph.util.io;

import com.ocrix.neuroph.core.exceptions.NeurophException;


/**
 * Defines neuroph input exception.
 * 
 * @author zoran
 */
public class NeurophInputException extends NeurophException {
    
    private static final long serialVersionUID = 1L;


    /**
     * Constructs an NeurophInputException with no detail message.
     */
    public NeurophInputException() {
        super();
    }

    
    /**
     * Constructs an NeurophInputException with the specified detail message.
     * @param message the detail message.
     */
    public NeurophInputException(String message) {
        super(message);
    }

    
    /**
     * Constructs a NeurophInputException with the specified detail message and specified cause.
     * @param message the detail message.
     * @param cause the cause for exception
     */
    public NeurophInputException(String message, Throwable cause) {
        super(message, cause);
    }

    
    /**
     * Constructs a new runtime exception with the specified cause
     * @param cause the cause for exception
     */
    public NeurophInputException(Throwable cause) {
        super(cause);
    }    
}
