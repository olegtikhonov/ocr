package com.ocrix.neuroph.util.random;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;

/**
 * 
 * @author zoran
 */
public class DistortRandomizer extends WeightsRandomizer {
	/* Class member declarations */
	private double distortionFactor;
	/* End of class member declarations */

	/**
	 * Constructs distort randomizer.
	 * 
	 * @param distortionFactor - a distortion factor.
	 */
	public DistortRandomizer(double distortionFactor) {
		this.distortionFactor = distortionFactor;
	}

	@Override
	public void randomize(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		for (Layer layer : neuralNetwork.getLayers()) {
			for (Neuron neuron : layer.getNeurons()) {
				for (Connection connection : neuron.getInputConnections()) {
					double weight = connection.getWeight().getValue();
					connection.getWeight().setValue(distort(weight));
				}
			}
		}

	}

	private double distort(double weight) {
		return weight
				+ (this.distortionFactor - (randomGenerator.nextDouble()
						* this.distortionFactor * 2));
	}
}
