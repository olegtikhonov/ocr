/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import static com.ocrix.neuroph.common.CommonMessages.CLASS_NAME;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_CANT_FIND_CITOR;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_INIT_CRTNG_WF;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_INIT_NEURON;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_NEURON_NO_PERRMISSIONS;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_SF_INIT;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_TF_CREATE;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_TF_NO_PERMISSION;
import static com.ocrix.neuroph.common.CommonMessages.LINE_NUMBER;
import static com.ocrix.neuroph.common.CommonMessages.METHOD_NAME;
import static com.ocrix.neuroph.common.CommonMessages.PART_MSG_CRTNG_TF;
import static com.ocrix.neuroph.common.CommonMessages.PART_MSG_METHOD_THREW;
import static com.ocrix.neuroph.common.CommonMessages.PART_MSG_WHILE_CRTNG_NRN;
import static com.ocrix.neuroph.common.NetProperty.BIAS;
import static com.ocrix.neuroph.common.NetProperty.THRESHOLD;
import static com.ocrix.neuroph.common.NetProperty.TRANSFER_FUNCTION;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.input.SummingFunction;
import com.ocrix.neuroph.core.input.WeightsFunction;
import com.ocrix.neuroph.core.transfer.TransferFunction;
import com.ocrix.neuroph.nnet.comp.InputOutputNeuron;
import com.ocrix.neuroph.nnet.comp.ThresholdNeuron;

/**
 * Provides methods to create customized instances of Neurons.
 * 
 * <br>
 * TODO: <b style="color:RED">redesign this class</b>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class NeuronFactory {
    /* Private logger. */
    private static final Logger logger = Logger.getLogger(NeuronFactory.class
            .getName());

    private static final int DEFAULT_STACK_DEPTH = 2;

    private NeuronFactory() {
        // TODO: throw some notification/exception
    }

    /**
     * Creates and returns neuron instance according to the given specification
     * in neuronProperties.
     * 
     * @param neuronProperties
     *            - the specification of neuron properties.
     * 
     * @throws IllegalArgumentException
     *             if properties are NULL.
     * 
     * @return returns instance of neuron with specified properties.
     */
    public static Neuron createNeuron(NeuronProperties neuronProperties) {
        Validator.validateProperties(neuronProperties);

        InputFunction inputFunction = null;
        Class<?> inputFunctionClass = neuronProperties.getInputFunction();

        if (inputFunctionClass != null) {
            inputFunction = createInputFunction(inputFunctionClass);
        } else {
            WeightsFunction weightsFunction = createWeightsFunction(neuronProperties
                    .getWeightsFunction());
            SummingFunction summingFunction = createSummingFunction(neuronProperties
                    .getSummingFunction());

            inputFunction = new InputFunction(weightsFunction, summingFunction);
        }

        TransferFunction transferFunction = createTransferFunction(neuronProperties
                .getTransferFunctionProperties());

        Neuron neuron = null;
        Class<?> neuronClass = neuronProperties.getNeuronType();

        /* Uses two param constructor to create neuron */
        try {
            @SuppressWarnings("rawtypes")
            Class[] paramTypes = { InputFunction.class, TransferFunction.class };
            Constructor<?> con = neuronClass.getConstructor(paramTypes);

            Object paramList[] = new Object[2];
            paramList[0] = inputFunction;
            paramList[1] = transferFunction;

            neuron = (Neuron) con.newInstance(paramList);

        } catch (NoSuchMethodException e) {
            // logger.severe(ERROR_CANT_FIND_CITOR.to());
            // logger.severe(stackToString(e));
        } catch (InstantiationException e) {
            logger.severe(ERROR_INIT_NEURON.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (IllegalAccessException e) {
            logger.severe(ERROR_NEURON_NO_PERRMISSIONS.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (InvocationTargetException e) {
            logger.severe(PART_MSG_METHOD_THREW.to() + e.getTargetException()
                    + PART_MSG_WHILE_CRTNG_NRN.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (neuron == null) {
            /* Uses constructor without params to create neuron */
            try {
                neuron = (Neuron) neuronClass.newInstance();
            } catch (IllegalAccessException e) {
                logger.severe(ERROR_INIT_NEURON.to());
                logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
            } catch (InstantiationException e) {
                logger.severe(ERROR_INIT_NEURON.to());
                logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
            }
        }

        if (neuronProperties.hasProperty(THRESHOLD.to())) {
            ((ThresholdNeuron) neuron).setThresh((Double) neuronProperties
                    .getProperty(THRESHOLD.to()));
        } else if (neuronProperties.hasProperty(BIAS.to())) {
            ((InputOutputNeuron) neuron).setBias((Double) neuronProperties
                    .getProperty(BIAS.to()));
        }

        return neuron;

    }

    /**
     * Creates the input function.
     * 
     * @param inputFunctionClass
     *            to be created.
     * 
     * @return the {@link InputFunction}.
     */
    private static InputFunction createInputFunction(Class<?> inputFunctionClass) {
        InputFunction inputFunction = null;

        try {
            inputFunction = (InputFunction) inputFunctionClass.newInstance();
        } catch (InstantiationException e) {
            logger.severe(ERROR_INIT_NEURON.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (IllegalAccessException e) {
            logger.severe(ERROR_NEURON_NO_PERRMISSIONS.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (Exception e) {
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        }

        return inputFunction;
    }

    /**
     * Creates and returns instance of transfer function. Note: Uses reflection
     * inside.
     * 
     * @param tfProperties
     *            - the transfer function properties.
     * 
     * @return returns transfer function.
     */
    private static TransferFunction createTransferFunction(
            Properties tfProperties) {
        TransferFunction transferFunction = null;

        Class<?> tfClass = (Class<?>) tfProperties
                .getProperty(TRANSFER_FUNCTION.to());

        try {
            @SuppressWarnings("rawtypes")
            Class[] paramTypes = null;

            @SuppressWarnings("rawtypes")
            Constructor[] cons = tfClass.getConstructors();
            for (int i = 0; i < cons.length; i++) {
                paramTypes = cons[i].getParameterTypes();
                /* Uses constructor with one parameter of Properties type */
                if ((paramTypes.length == 1)
                        && (paramTypes[0] == Properties.class)) {
                    Class<?> argTypes[] = new Class[1];
                    argTypes[0] = Properties.class;
                    Constructor<?> ct = tfClass.getConstructor(argTypes);

                    Object argList[] = new Object[1];
                    argList[0] = tfProperties;
                    transferFunction = (TransferFunction) ct
                            .newInstance(argList);
                    break;

                }
                /* Uses constructor without params */
                else if (paramTypes.length == 0) {
                    transferFunction = (TransferFunction) tfClass.newInstance();
                    break;
                }
            }

            return transferFunction;

        } catch (NoSuchMethodException e) {
            logger.severe(ERROR_CANT_FIND_CITOR.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (InstantiationException e) {
            logger.severe(ERROR_TF_CREATE.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (IllegalAccessException e) {
            logger.severe(ERROR_TF_NO_PERMISSION.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (InvocationTargetException e) {
            logger.severe(PART_MSG_METHOD_THREW.to() + e.getTargetException()
                    + PART_MSG_CRTNG_TF.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
            e.printStackTrace();
        }

        return transferFunction;

    }

    /**
     * Creates and returns instance of specified weights function.
     * 
     * @param weightsFunctionClass
     *            - the weights function class.
     * 
     * @return returns instance of weights function.
     */
    private static WeightsFunction createWeightsFunction(
            Class<?> weightsFunctionClass) {
        WeightsFunction weightsFunction = null;

        try {
            weightsFunction = (WeightsFunction) weightsFunctionClass
                    .newInstance();
        } catch (InstantiationException e) {
            logger.severe(ERROR_INIT_CRTNG_WF.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (IllegalAccessException e) {
            logger.severe(ERROR_TF_NO_PERMISSION.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        }

        return weightsFunction;
    }

    /**
     * Creates and returns instance of specified summing function.
     * 
     * @param summingFunctionClass
     *            - the summing function class
     * 
     * @return returns instance of summing function
     */
    private static SummingFunction createSummingFunction(
            Class<?> summingFunctionClass) {
        SummingFunction summingFunction = null;

        try {
            summingFunction = (SummingFunction) summingFunctionClass
                    .newInstance();
        } catch (InstantiationException e) {
            logger.severe(ERROR_SF_INIT.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        } catch (IllegalAccessException e) {
            logger.severe(ERROR_TF_NO_PERMISSION.to());
            logger.severe(stackToString(e, DEFAULT_STACK_DEPTH));
        }

        return summingFunction;
    }

    /**
     * Transforms stack trace elements of the exception to textual form.
     * 
     * @param e
     *            to be transformed.
     * 
     * @return a textual form of the exception.
     */
    private static String stackToString(Exception e, int depth) {
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] list = e.getStackTrace();
        sb.append(e.getMessage());
        sb.append(" ");
        for (int i = 0; i < depth; i++) {
            sb.append(CLASS_NAME.to());
            sb.append(list[i].getClassName());
            sb.append(" ");
            sb.append(METHOD_NAME.to());
            sb.append(list[i].getMethodName());
            sb.append(" ");
            sb.append(LINE_NUMBER.to());
            sb.append(list[i].getLineNumber());
            sb.append(" ");
        }

        return sb.toString();
    }
}