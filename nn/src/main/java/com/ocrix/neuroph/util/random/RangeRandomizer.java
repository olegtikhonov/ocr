/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.random;


/**
 * Defines a range randomizer.
 * 
 * @author zoran
 */
public class RangeRandomizer extends WeightsRandomizer {
    /* Class member declarations */
    protected double min;
    protected double max;
    private static RangeRandomizer instance = null;
    /* End of class member declarations */


    /**
     * Constructs a {@link RangeRandomizer}.
     */
    protected RangeRandomizer() {
        super();
    }

    /**
     * Gets the {@link RangeRandomizer} instance.
     * 
     * @return a Random generator that generates number by given range.
     */
    public static RangeRandomizer getInstance(){
        if(instance == null)
            instance = new RangeRandomizer();
        return instance;
    }


    /**
     * Constructs range randomizer.
     * 
     * @param min - a minimum
     * @param max - a maximum
     */
    public void setRangeRandomizer(double min, double max) {
        this.max = max;
        this.min = min;
    }

    @Override
    protected double nextRandomWeight() {
        return min + randomGenerator.nextDouble() * (max - min);
    }
}
