/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import java.util.Hashtable;
import java.util.Set;

/**
 * Represents a general set of properties for neuroph objects. Extends
 * {@link Hashtable}.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Properties extends Hashtable<Object, Object> {
    /* Finger print of this class */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an empty hash map where keys are not NULLs.
     * 
     * @param keys
     *            to be put as keys.
     */
    protected void createKeys(String... keys) {
        for (String key : keys)
            this.put(key, "");
    }

    /**
     * Sets the property up.
     * 
     * @param key
     *            as key in the map.
     * @param value
     *            as value in the map.
     */
    public void setProperty(String key, Object value) {
        this.put(key, value);
    }

    /**
     * Gets a property.
     * 
     * @param key
     *            used as search key.
     * 
     * @return
     */
    public Object getProperty(String key) {
        return this.get(key);
    }

    /**
     * Answers if the maps has given property or not.
     * 
     * @param key
     *            to be searched.
     * 
     * @return either true or false.
     */
    public boolean hasProperty(String key) {
        return this.containsKey(key);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (keys() != null) {
            Set<Object> set = keySet();
            builder.append("key=");

            for (Object key : set) {
                builder.append(key);
                builder.append(", ");
            }
        }
        if (values() != null) {
            builder.append("values=");
            builder.append(values());
        }
        builder.append("]");
        return builder.toString();
    }
}
