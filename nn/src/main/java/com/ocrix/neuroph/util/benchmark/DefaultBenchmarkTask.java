/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;


import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nnet.MultiLayerPerceptron;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;


/**
 * Runs the test constructing MultiLayerPerceptron network, generating
 * TrainingSet<SupervisedTrainingElement> using defaults [trainingSetSize = 100,
 * inputSize = 10, outputSize = 5, MaxIterations = 2000], values are pseudo
 * generated Math.random().
 * 
 * After creating the instance call <code>prepareTest()</code> for
 * initialization.
 */
public class DefaultBenchmarkTask extends BenchmarkTask {
    private MultiLayerPerceptron network;
    private TrainingSet<SupervisedTrainingElement> trainingSet;

    /**
     * Constructs a {@link DefaultBenchmarkTask}.
     * 
     * @param name given for the task.
     */
    public DefaultBenchmarkTask(String name) {
        super(name);
    }

    @Override
    public void prepareTest() {
        int trainingSetSize = 100;
        int inputSize = 10;
        int outputSize = 5;

        this.trainingSet = new TrainingSet<SupervisedTrainingElement>(inputSize, outputSize);

        for (int i = 0; i < trainingSetSize; i++) {
            double input[] = new double[inputSize];
            double output[] = new double[outputSize];
            /* Initializes the input vector */
            for (int j = 0; j < inputSize; j++){
                input[j] = Math.random();
            }
            /* Initializes the output vector */
            for (int j = 0; j < outputSize; j++){
                output[j] = Math.random();
            }
            /* Creates a new supervised training element. */
            SupervisedTrainingElement element = new SupervisedTrainingElement(input, output);
            trainingSet.addElement(element);
        }
        /* Constructs multi-layer perceptron */
        network = new MultiLayerPerceptron(inputSize, 8, 7, outputSize);
        ((MomentumBackpropagation) network.getLearningRule()).setMaxIterations(2000);

    }

    @Override
    public void runTest() {
        network.learnInNewThread(trainingSet);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [network=");
        builder.append(network);
        builder.append(", trainingSet=");
        builder.append(trainingSet);
        builder.append("]");
        return builder.toString();
    }
}