package com.ocrix.neuroph.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 
 * @author zoran
 */
public class URLInputAdapter extends InputStreamAdapter {

    public URLInputAdapter(URL url) throws IOException {
        super(new BufferedReader(new InputStreamReader(url.openStream())));
    }

    public URLInputAdapter(String url) throws MalformedURLException,
            IOException {
        this(new URL(url));
    }

}
