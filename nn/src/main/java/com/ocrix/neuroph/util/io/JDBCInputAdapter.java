/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_CLOSING_DATABASE;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_READING_INPUT;
import static com.ocrix.neuroph.common.CommonMessages.HINT_JDBC_ERROR;
import static com.ocrix.neuroph.common.CommonMessages.JDBC_ERROR;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * JDBC input adaptor implements an {@link InputAdapter}.
 */
public class JDBCInputAdapter implements InputAdapter {
    /* Class member declarations */
    private Connection connection;
    private ResultSet resultSet;
    private int inputSize;
    private static final Logger log = Logger.getLogger(JDBCInputAdapter.class
            .getName());

    /* End of class member declarations */

    /**
     * Creates a {@link JDBCInputAdapter}.
     * 
     * @param connection
     *            - a connection to the DB.
     * 
     * @param sql
     *            - a sql query.
     */
    public JDBCInputAdapter(Connection connection, String sql) {
        this.setConnection(connection);
        try {
            /* Gets a statement from the connection */
            Statement stmt = connection.createStatement();
            /* Executes the query */
            resultSet = stmt.executeQuery(sql);
            ResultSetMetaData rsmd = resultSet.getMetaData();
            inputSize = rsmd.getColumnCount();
        } catch (SQLException ex) {
            log.severe(JDBC_ERROR.to() + " " + ex.getMessage()
                    + HINT_JDBC_ERROR.to());
            throw new NeurophInputException(JDBC_ERROR.toString(), ex);
        }
    }

    /**
     * Creates a {@link JDBCInputAdapter}.
     * 
     * @param connection
     *            to be connected to the given DB.
     */
    public JDBCInputAdapter(Connection connection) {
        this.connection = connection;
    }

    /**
     * Reads input and returns the double array.
     * 
     * @return double[].
     */
    public double[] readInput() {
        List<Double> list = new ArrayList<Double>();
        try {
            /* Goes row */
            while (resultSet.next()) {
                int columnCount = resultSet.getMetaData().getColumnCount();
                /* Iterates over { column 0 : N } */
                for (int i = 0; i < columnCount; i++) {
                    list.add(Double.valueOf(resultSet.getString(i + 1)));
                }
            }

        } catch (SQLException ex) {
            log.severe(ERROR_READING_INPUT.to());
            throw new NeurophInputException(ERROR_READING_INPUT.to(), ex);
        }

        return convertDoubleListTo2DDoubleArray(list);
    }

    /**
     * Closes the {@link ResultSet}.
     * 
     * @throws NeurophInputException
     *             if closing connection to DB is failed.
     */
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException ex) {
            log.severe(ERROR_CLOSING_DATABASE.to());
            throw new NeurophInputException(ERROR_CLOSING_DATABASE.to(), ex);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [connection=");
        builder.append(connection);
        builder.append(", resultSet=");
        builder.append(parseResultSet(resultSet));
        builder.append(", inputSize=");
        builder.append(inputSize);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Gets a jdbc connection.
     * 
     * @return a {@link Connection}.
     */
    protected Connection getConnection() {
        return connection;
    }

    /**
     * Sets a {@link Connection}.
     * 
     * @param connection
     *            to be set.
     */
    private void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Parses result set.
     * 
     * @param resultSet
     *            to be parsed.
     * 
     * @return a textual representation of the result set.
     */
    private String parseResultSet(ResultSet resultSet) {
        StringBuilder builder = new StringBuilder();

        try {
            if (resultSet != null) {
                int columnCount = resultSet.getMetaData().getColumnCount();
                while (resultSet.next()) {
                    for (int i = 0; i < columnCount;) {
                        builder.append(resultSet.getString(i + 1));
                        if (++i < columnCount)
                            builder.append(",");
                    }
                    builder.append(" ");
                }
            }
        } catch (Exception e) {
        }

        return builder.toString();
    }

    /**
     * Converts List to Array of doubles.
     * 
     * @param list
     *            to be converted to double[].
     * 
     * @return a double[].
     */
    private double[] convertDoubleListTo2DDoubleArray(List<Double> list) {
        double[] array = null;
        if (list != null) {
            array = new double[list.size()];
            for (int i = 0; i < list.size(); i++) {
                array[i] = list.get(i);
            }
        }
        return array;
    }
}