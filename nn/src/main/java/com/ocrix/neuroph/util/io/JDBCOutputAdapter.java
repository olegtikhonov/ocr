/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.util.io;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.Validator;

/**
 * Defines a JDBC output adapter.
 */
public class JDBCOutputAdapter implements OutputAdapter {
    /* Class member declarations */
    private Connection connection;
    private String tableName;
    private String columnName;
    private static final Logger log = Logger.getLogger(JDBCOutputAdapter.class.getName());

    /* End of class member declarations */

    /**
     * Constructs {@link JDBCOutputAdapter}.
     * 
     * @param connection
     *            a {@link com.ocrix.neuroph.core.Connection}
     * @param tableName
     */
    public JDBCOutputAdapter(Connection connection, String tableName) {
        setConnection(connection);
        setTableName(tableName);
    }

    /**
     * Closes connection.
     */
    public void close() {
        try {
            if(getConnection() != null){
                getConnection().close();
            }
        } catch (SQLException e) {
            log.severe(e.getMessage());
        }
    }

    /**
     * Inserts the output into DB.
     */
    public void writeOutput(double[] output) {
        Validator.validateDouble(output);
        Statement sta = null;
        
        try {
            sta = getConnection().createStatement();
            
            for(int i = 0; i < output.length; i++){
                sta.executeUpdate("INSERT INTO " + getTableName() + " (ID, " + getColumnName() + " )" + " VALUES(" + i + " , " + output[i] + " )");
            }
        } catch (SQLException ex) {
            log.log(Level.SEVERE, ex.getMessage());
        } finally {
            if(sta != null){
                try {
                    sta.close();
                } catch (SQLException e) {
                }
            }
        }
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [connection=");
        builder.append(connection);
        builder.append(", tableName=");
        builder.append(tableName);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Sets a table name.
     * 
     * @param tableName
     *            - a table name the output to be set.
     * @see #getTableName()
     */
    private void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Returns a {@link com.ocrix.neuroph.core.Connection}.
     * 
     * @return a connection to the DB.
     * @see #setConnection(Connection)
     */
    private Connection getConnection() {
        return connection;
    }

    /**
     * Returns a connection to the DB.
     * 
     * @param connection
     *            a {@link com.ocrix.neuroph.core.Connection}
     * @see #getConnection()
     */
    private void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Return a table name.
     * 
     * @return a table name to what we want data to be written.
     */
    private String getTableName() {
        return tableName;
    }

    /**
     * Gets a column name to be written a data.
     * 
     * @return a column name.
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets string of column name and type.
     * For example,
     * <code>" (ID, " + columnName + " )"</code>
     * 
     * @param columnNameAndType
     * @throws IllegalArgumentException if a parameter is NULL.
     */
    public void setColumnName(String columnNameAndType) {
        Validator.validateString(columnNameAndType);
        this.columnName = columnNameAndType;
    }
}
