package com.ocrix.neuroph.util.io;

/**
 *
 * @author zoran
 */
public interface OutputAdapter {
    
    public void writeOutput(double[] output);
    public void close();
    
}
