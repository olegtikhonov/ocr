package com.ocrix.neuroph.util;

public final class NNUtils {
    private NNUtils() {}


    public static boolean inBounds(int index, int arrayLen) {
        return (index >= 0) && (index < arrayLen);
    }

    public static boolean isNumeric(String toBeChecked) {
        return toBeChecked.chars().allMatch(Character::isDigit);
    }
}
