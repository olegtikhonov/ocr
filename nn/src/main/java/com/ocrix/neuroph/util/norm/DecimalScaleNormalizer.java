/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.norm;

import com.ocrix.neuroph.core.learning.DataSet;
import com.ocrix.neuroph.core.learning.DataSetRow;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Defines decimal scale normalizer.
 * 
 * @author zoran
 */
public class DecimalScaleNormalizer implements Normalizer {
    /* Class member declarations */
    double[] maxVector; // contains max values for all columns
    double[] scaleVector;

    // double[] max;

    /* End of class member declarations */

    public void normalize(TrainingSet<? extends TrainingElement> trainingSet) {
        findMaxVector(trainingSet);
        findScaleVector();

        for (TrainingElement trainingElement : trainingSet.elements()) {
            double[] input = trainingElement.getInput();
            double[] normalizedInput = normalizeScale(input);
            trainingElement.setInput(normalizedInput);
        }
    }

    private void findMaxVector(
            TrainingSet<? extends TrainingElement> trainingSet) {
        int inputSize = trainingSet.getInputSize();
        maxVector = new double[inputSize];

        for (TrainingElement te : trainingSet.elements()) {
            double[] input = te.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > maxVector[i]) {
                    maxVector[i] = Math.abs(input[i]);
                }
            }
        }
    }

    public void findScaleVector() {
        scaleVector = new double[maxVector.length];
        for (int i = 0; i < scaleVector.length; i++) {
            scaleVector[i] = 1;
        }

        for (int i = 0; i < maxVector.length; i++) {
            while (maxVector[i] > 1) {
                maxVector[i] = maxVector[i] / 10.0;
                scaleVector[i] = scaleVector[i] * 10;
            }
        }
    }

    public double[] normalizeScale(double[] vector) {
        double[] normalizedVector = new double[vector.length];
        for (int i = 0; i < vector.length; i++) {
            normalizedVector[i] = vector[i] / scaleVector[i];
        }
        return normalizedVector;
    }

    public void normalize(DataSet dataSet) {
        findMaxVector(dataSet);
        findScaleVector();

        for (DataSetRow dataSetRow : dataSet.getRows()) {
            double[] input = dataSetRow.getInput();
            double[] normalizedInput = normalizeScale(input);
            dataSetRow.setInput(normalizedInput);
        }
    }

    private void findMaxVector(DataSet dataSet) {
        int inputSize = dataSet.getInputSize();
        maxVector = new double[inputSize];

        for (DataSetRow row : dataSet.getRows()) {
            double[] input = row.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > maxVector[i]) {
                    maxVector[i] = Math.abs(input[i]);
                }
            }
        }
    }
}
