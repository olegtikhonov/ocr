/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import java.io.BufferedWriter;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_CLOSE_OUTPUT_STREAM;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_WRITE_OUTPUT_STREAM;
import static com.ocrix.neuroph.common.CommonMessages.WARN_DOUBLE_ARR_NULL_EMPTY;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.Validator;

/**
 * 
 * @author zoran
 */
public class OutputStreamAdapter implements OutputAdapter {
    /* Local logger */
    private static final Logger log = Logger.getLogger(OutputStreamAdapter.class.getName());
    protected BufferedWriter bufferedWriter;
    
    
    /**
     * Constructs {@link OutputStreamAdapter}.
     * 
     * @param outputStream 
     */
    public OutputStreamAdapter(OutputStream outputStream) {
        Validator.validateOutputStream(outputStream);
        bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
    }

    /**
     * Creates {@link OutputStreamAdapter}.
     * 
     * @param bufferedWriter be created {@link OutputStreamAdapter}.
     * 
     * @throws IllegalArgumentException if argument is null.
     */
    public OutputStreamAdapter(BufferedWriter bufferedWriter) {
        Validator.validateBufferedWroter(bufferedWriter);
        this.bufferedWriter = bufferedWriter;
    }

    /**
     * Writes data to stream.
     * 
     * @param output - an array of doubles to be written.
     * 
     * @throws IOException if writing operation failed i.e If an I/O error occurs.
     */
    public void writeOutput(double[] output) {
        
        if(output != null){
            try {
                
                String outputLine = "";
                
                for (int i = 0; i < output.length; i++) {
                    
                    if(i != (output.length - 1)){
                        outputLine += output[i] + " ";
                    }else {
                        outputLine += output[i];
                    }
                }
                
                outputLine += "\r\n";
                bufferedWriter.write(outputLine);
                
            } catch (IOException ex) {
                
                log.severe(ERROR_WRITE_OUTPUT_STREAM.to());
                
            } finally {
                
                if(bufferedWriter != null){
                    try {
                        bufferedWriter.close();
                    } catch (IOException e) {
                        log.severe(ERROR_WRITE_OUTPUT_STREAM.to());
                    }
                }
                
            }
        }else {
            log.warning(WARN_DOUBLE_ARR_NULL_EMPTY.to());
        }
    }

    /**
     * Closes the {@link BufferedWriter}.
     */
    public void close() {
        try {
            if(bufferedWriter != null){
                bufferedWriter.close();
            }
        } catch (IOException ex) {
            log.severe(ERROR_CLOSE_OUTPUT_STREAM.to());
            throw new NeurophOutputException(ERROR_CLOSE_OUTPUT_STREAM.to(), ex);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [bufferedWriter=");
        builder.append(bufferedWriter);
        builder.append("]");
        return builder.toString();
    }
}
