/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

/**
 * Defines a base behavior of the Task.
 * The defaults are:
 * <ul>
 * <li>warm iterations = 1</li>
 * <li>test iterations = 1</li>
 * </ul>
 */
abstract public class BenchmarkTask {
    /* Class members */
    /* Task name */
    private String name;
    /* A number of war iterations */
    private int warmupIterations = 1;
    /* A number of the test iterations */
    private int testIterations = 1;

    
    /**
     * Constructs default {@link BenchmarkTask}.
     * @param name
     */
    public BenchmarkTask(String name) {
        this.name = name;
    }

    /**
     * Gets a task name.
     * @return a task name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a task name.
     * 
     * @param name to be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets a test iterations.
     * 
     * @return a number of the test iterations.
     */
    public int getTestIterations() {
        return testIterations;
    }

    /**
     * Sets a number of test iterations.
     * 
     * @param testIterations a number to be sat as test iterations.
     */
    public void setTestIterations(int testIterations) {
        this.testIterations = testIterations;
    }

    /**
     * Gets a number of the warmup iterations.
     * 
     * @return a number of warmup iterations.
     */
    public int getWarmupIterations() {
        return warmupIterations;
    }

    /**
     * Sets a number of warmup iterations.
     * 
     * @param warmupIterations to be set as number of warmup iterations.
     */
    public void setWarmupIterations(int warmupIterations) {
        this.warmupIterations = warmupIterations;
    }

    /**
     * To be used as init function.
     */
    abstract public void prepareTest();

    /**
     * To be used as run function for training process.
     */
    abstract public void runTest();
}