/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import java.util.List;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.core.transfer.TransferFunction;

/**
 * Provides methods to create instance of a Layer with specified number of
 * neurons and neuron's properties.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class LayerFactory {

    /* Do not create an instance of static class */
    private LayerFactory() {
    }

    /**
     * Creates a layer.
     * 
     * @param neuronsCount
     *            - how many neurons to be used.
     * @param neuronProperties
     *            - NeuralNetworkType
     * @see NeuralNetworkType
     * @return a {@link Layer}
     */
    public static Layer createLayer(int neuronsCount, NeuronProperties neuronProperties) {
//        Validator.validateIntInRange(neuronsCount, 1, Integer.MAX_VALUE);
        Layer layer = new Layer(neuronsCount, neuronProperties);
        return layer;
    }

    /**
     * Creates a {@link Layer} of the {@link TransferFunctionType}
     * 
     * @param neuronsCount
     *            - how many neurons to be used.
     * @param transferFunctionType
     *            - a transfer function type
     * 
     * @return a {@link Layer}
     */
    public static Layer createLayer(int neuronsCount,
            TransferFunctionType transferFunctionType) {
        Validator.validateIntInRange(neuronsCount, 1, Integer.MAX_VALUE);
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty("transferFunction", transferFunctionType);
        Layer layer = createLayer(neuronsCount, neuronProperties);
        return layer;
    }

    /**
     * Creates a {@link Layer} of the {@link TransferFunction}.
     * 
     * @param neuronsCount
     *            - how many neurons to be used.
     * @param transferFunctionClass
     *            a {@link TransferFunction}
     * 
     * @return a {@link Layer}
     */
    public static Layer createLayer(int neuronsCount,
            Class<? extends TransferFunction> transferFunctionClass) {
        Validator.validateIntInRange(neuronsCount, 1, Integer.MAX_VALUE);
        NeuronProperties neuronProperties = new NeuronProperties();
        neuronProperties.setProperty("transferFunction", transferFunctionClass);
        Layer layer = createLayer(neuronsCount, neuronProperties);
        return layer;
    }

    /**
     * Creates a {@link Layer} from {@link NeuronProperties}.
     * 
     * @param neuronPropertiesVector
     *            a List<NeuronProperties>.
     * 
     * @return a {@link Layer}
     */
    public static Layer createLayer(
            List<NeuronProperties> neuronPropertiesVector) {
        Validator.validateList(neuronPropertiesVector);
        Layer layer = new Layer();

        for (NeuronProperties neuronProperties : neuronPropertiesVector) {
            Neuron neuron = NeuronFactory.createNeuron(neuronProperties);
            layer.addNeuron(neuron);
        }

        return layer;
    }
}