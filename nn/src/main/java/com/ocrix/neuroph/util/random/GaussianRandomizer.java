package com.ocrix.neuroph.util.random;

/**
 * Based on GaussianRandomizer from Encog.
 * 
 * @author zoran
 */
public class GaussianRandomizer extends WeightsRandomizer {
	/* Class member declarations */
    double mean; /* mean */
    double standardDeviation; /* standard deviation */
    /* The y2 value. */
    private double y2;
    /* Should we use the last value. */
    private boolean useLast = false;
    /* End of class member declarations */
    
    /**
     * Constructs a gaussian randomizer.
     * 
     * @param mean - the <a href="http://en.wikipedia.org/wiki/Mean">mean</a> 
     * @param standardDeviation - a <a href="http://en.wikipedia.org/wiki/Standard_deviation">standard deviation</a>.
     */
    public GaussianRandomizer(double mean, double standardDeviation) {
        this.mean = mean;
        this.standardDeviation = standardDeviation;
    }

    
    /**
     * Computes a Gaussian random number.
     * 
     * @param m - The mean.
     * @param s - The standard deviation.
     * @return The random number.
     */
    public double boxMuller(double m, double s) {
        double x1, x2, w, y1;

        /* Use value from previous call */
        if (this.useLast) {
            y1 = this.y2;
            this.useLast = false;
        } else {
            do {
                x1 = 2.0 * randomGenerator.nextDouble() - 1.0;
                x2 = 2.0 * randomGenerator.nextDouble() - 1.0;
                w = x1 * x1 + x2 * x2;
            } while (w >= 1.0);

            w = Math.sqrt((-2.0 * Math.log(w)) / w);
            y1 = x1 * w;
            this.y2 = x2 * w;
            this.useLast = true;
        }

        return (m + y1 * s);
    }

    @Override
    protected double nextRandomWeight() {
        return boxMuller(mean, standardDeviation);
    }
}
