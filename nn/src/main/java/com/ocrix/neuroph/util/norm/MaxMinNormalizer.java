/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.norm;

import com.ocrix.neuroph.core.learning.DataSet;
import com.ocrix.neuroph.core.learning.DataSetRow;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Defines max min normalizer.
 * 
 * @author zoran
 */
public class MaxMinNormalizer implements Normalizer {
    /* Class member declarations */
    /* Contains max values for all columns */
    double[] maxVector;
    /* Contains min values for all columns */
    double[] minVector;
    double[] max; // contains max values for all columns
    double[] min; // contains min values for all columns

    /* End of class member declarations */

    public void normalize(TrainingSet<? extends TrainingElement> trainingSet) {
        /* Finds min i max vectors */
        findMaxinAndMinVectors(trainingSet);

        /* Executes normalization/partition */
        for (TrainingElement trainingElement : trainingSet.elements()) {
            double[] input = trainingElement.getInput();
            double[] normalizedInput = normalizeMaxMin(input);

            trainingElement.setInput(normalizedInput);
        }

    }

    private void findMaxinAndMinVectors(
            TrainingSet<? extends TrainingElement> trainingSet) {
        int inputSize = trainingSet.getInputSize();
        maxVector = new double[inputSize];
        minVector = new double[inputSize];

        for (TrainingElement te : trainingSet.elements()) {
            double[] input = te.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > maxVector[i]) {
                    maxVector[i] = Math.abs(input[i]);
                }
                if (Math.abs(input[i]) < minVector[i]) {
                    minVector[i] = Math.abs(input[i]);
                }
            }
        }
    }

    private double[] normalizeMaxMin(double[] vector) {
        double[] normalizedVector = new double[vector.length];

        for (int i = 0; i < vector.length; i++) {
            normalizedVector[i] = (vector[i] - minVector[i])
                    / (maxVector[i] - minVector[i]);
        }

        return normalizedVector;
    }

    public void normalize(DataSet dataSet) {
        // find min i max vectors
        findMaxinAndMinVectors(dataSet);

        // izvrsi normalizciju / deljenje
        for (DataSetRow dataSetRow : dataSet.getRows()) {
            double[] input = dataSetRow.getInput();
            double[] normalizedInput = normalizeMaxMin(input);

            dataSetRow.setInput(normalizedInput);
        }
    }

    private void findMaxinAndMinVectors(DataSet dataSet) {
        int inputSize = dataSet.getInputSize();
        max = new double[inputSize];
        min = new double[inputSize];

        for (DataSetRow dataSetRow : dataSet.getRows()) {
            double[] input = dataSetRow.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > max[i]) {
                    max[i] = Math.abs(input[i]);
                }
                if (Math.abs(input[i]) < min[i]) {
                    min[i] = Math.abs(input[i]);
                }
            }
        }
    }
}
