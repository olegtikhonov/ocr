/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.core.learning.SupervisedTrainingElement;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Handles training set imports.
 * 
 * @author Zoran Sevarac
 * @author Ivan Nedeljkovic
 * @author Kokanovic Rados
 */

// TODO: importFromDatabase(sql, ...) and importFromUrl(url, ...)

public final class TrainingSetImport {
    private static final Logger log = Logger.getLogger(TrainingSetImport.class
            .getName());

    private TrainingSetImport() {
    }

    /**
     * Creates Training set importing from the file. <b>Note:</b> if output
     * vector is greater than zero, the SupervisedTrainingElement will be put
     * in, otherwise TrainingElement.
     * 
     * <br>
     * <u>code example</u><br>
     * 
     * <code><pre>
     * TrainingSet<TrainingElement> ts = new TrainingSet<TrainingElement>(DECIMAL, DECIMAL);
     * for (int i = 0; i < DECIMAL; i++) {
     *     TestUtil.addOneElement(ts);
     * }
     * String filePath = "target/" + RandomStringUtils.randomAlphabetic(DECIMAL);
     * ts.setFilePath(filePath);
     * ts.saveAsTxt(ts.getFilePath(), "~");
     * try {
     *   TrainingSet<?> set = TrainingSetImport.importFromFile(ts.getFilePath(), DECIMAL, DECIMAL, "~");
     *   System.out.println(set);
     *   } catch (NumberFormatException e) {
     *          log.severe(e);
     *   } catch (FileNotFoundException e) {
     *          log.severe(e);
     *   } catch (IOException e) {
     *          log.severe(e);
     *   }</pre>
     * </code>
     * 
     * @param filePath
     *            - where to take previously saved training set.
     * @param inputsCount
     *            - the size of the input array.
     * @param outputsCount
     *            - the size of the output array.
     * @param separator
     *            - the separator distinguishing between training elements.
     * @return the training set.
     * 
     * @throws IOException
     *             if an I/O error occurs.
     * @throws FileNotFoundException
     *             if the files does not exist.
     * @throws NumberFormatException
     *             if the parsed string does not contain parsable number.
     */
    @SuppressWarnings("unchecked")
    public static TrainingSet<?> importFromFile(String filePath,
            int inputsCount, int outputsCount, String separator)
            throws IOException, FileNotFoundException, NumberFormatException {

        FileReader fileReader = null;

        try {
            @SuppressWarnings("rawtypes")
            TrainingSet trainingSet = new TrainingSet();
            trainingSet.setInputSize(inputsCount);
            trainingSet.setOutputSize(outputsCount);
            fileReader = new FileReader(new File(filePath));
            BufferedReader reader = new BufferedReader(fileReader);

            String line = "";

            while ((line = reader.readLine()) != null) {
                double[] inputs = new double[inputsCount];
                double[] outputs = new double[outputsCount];

                String[] values = line.split(separator);

                if (values[0].equals("") || values[0].equals(separator)) {
                    continue; // skips if line was empty
                }

                /* Fills the input vector with the given values */
                for (int i = 0; i < inputsCount; i++) {
                    if (!values[i].equals(separator)) {
                        inputs[i] = Double.parseDouble(values[i]);
                    }
                }

                /* Fills the output vector with the given values */
                for (int i = 0; i < outputsCount; i++) {
                    // Prevents OOB exception && Number Format Exception
                    if (((inputsCount + i) < values.length)
                            && !values[inputsCount + i].equals(separator)) {
                        outputs[i] = Double
                                .parseDouble(values[inputsCount + i]);
                    }
                }

                if (outputsCount > 0) {
                    trainingSet.addElement(new SupervisedTrainingElement(
                            inputs, outputs));
                } else {
                    trainingSet.addElement(new TrainingElement(inputs));
                }
            }

            return trainingSet;

        } catch (FileNotFoundException ex) {
            log.severe(ANNUtils.stackToString(ex, 2));
            throw ex;
        } catch (IOException ex) {
            log.severe(ANNUtils.stackToString(ex, 2));
            throw ex;
        } catch (NumberFormatException ex) {
            fileReader.close();
            log.severe(ANNUtils.stackToString(ex, 2));
            throw ex;
        }
    }
}