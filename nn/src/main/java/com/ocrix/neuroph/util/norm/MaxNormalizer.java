/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.norm;

import com.ocrix.neuroph.core.learning.DataSet;
import com.ocrix.neuroph.core.learning.DataSetRow;
import com.ocrix.neuroph.core.learning.TrainingElement;
import com.ocrix.neuroph.core.learning.TrainingSet;

/**
 * Defines max normalizer.
 * 
 * @author zoran
 */
public class MaxNormalizer implements Normalizer {
    /* Class memebr declarations */
    /* Contains max values for all columns */
    double[] maxVector;
    double[] max; // contains max values for all columns

    public void normalize(TrainingSet<? extends TrainingElement> trainingSet) {
        findMaxVector(trainingSet);

        for (TrainingElement trainingElement : trainingSet.elements()) {
            double[] input = trainingElement.getInput();
            double[] normalizedInput = normalizeMax(input);
            trainingElement.setInput(normalizedInput);
        }

    }

    private void findMaxVector(
            TrainingSet<? extends TrainingElement> trainingSet) {
        int inputSize = trainingSet.getInputSize();
        maxVector = new double[inputSize];

        for (TrainingElement te : trainingSet.elements()) {
            double[] input = te.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > maxVector[i]) {
                    maxVector[i] = Math.abs(input[i]);
                }
            }
        }
    }

    /**
     * Normalizes the vector, i.e.
     * <code>normalizedVector[i] = vector[i] / maxVector[i]</code>
     * 
     * @param vector
     * @return
     */
    public double[] normalizeMax(double[] vector) {
        double[] normalizedVector = new double[vector.length];

        for (int i = 0; i < vector.length; i++) {
            if (maxVector != null && maxVector[i] > 1) {
                normalizedVector[i] = vector[i] / maxVector[i];
            }
        }

        return normalizedVector;
    }

    public void normalize(DataSet dataSet) {
        findMaxVector(dataSet);

        for (DataSetRow dataSetRow : dataSet.getRows()) {
            double[] input = dataSetRow.getInput();
            double[] normalizedInput = normalizeMax(input);
            dataSetRow.setInput(normalizedInput);
        }
    }

    private void findMaxVector(DataSet dataSet) {
        int inputSize = dataSet.getInputSize();
        maxVector = new double[inputSize];

        for (DataSetRow dataSetRow : dataSet.getRows()) {
            double[] input = dataSetRow.getInput();
            for (int i = 0; i < inputSize; i++) {
                if (Math.abs(input[i]) > maxVector[i]) {
                    maxVector[i] = Math.abs(input[i]);
                }
            }
        }
    }
}
