/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.util.random;

import java.util.Random;

import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;

/**
 * Defines a weights randomizer.
 * 
 * @author zoran
 */
public class WeightsRandomizer {
    private static WeightsRandomizer instance = null;

    /* Class member declarations */
    protected NeuralNetwork neuralNetwork;
    protected Random randomGenerator;

    /* End of class member declarations */

    /**
     * Constructs weights randomizer.
     */
    protected WeightsRandomizer() {
        this.randomGenerator = new Random();
    }

    public static WeightsRandomizer getInstance() {
        if (instance == null)
            instance = new WeightsRandomizer();
        return instance;
    }

    public void setCustomRandomizer(Random randGen) {
        this.randomGenerator = randGen;
    }

    /**
     * Gets a generator.
     * 
     * @return
     */
    public Random getRandomGenerator() {
        return randomGenerator;
    }


    /**
     * Randomizes connection weight of the neuron.
     * <br>The complexity is: <b>O(n^3)</b>.
     * 
     * @param neuralNetwork where to set the random values.
     */
    public void randomize(NeuralNetwork neuralNetwork) {
        this.neuralNetwork = neuralNetwork;
        /* O(n^3) ??? TODO find better approach if exists */
        for (Layer layer : neuralNetwork.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getInputConnections()) {
                    connection.getWeight().setValue(nextRandomWeight());
                }
            }
        }
    }

    /**
     * Gets next random weight.
     * 
     * @return
     */
    protected double nextRandomWeight() {
        return randomGenerator.nextDouble();
    }
}
