/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.util;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Connection;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;

/**
 * A CODEC encodes and decodes neural networks, much like the more standard
 * definition of a CODEC encodes and decodes audio/video.
 * 
 * This CODEC can encode a neural network to an array of doubles. It can also
 * decode this array of doubles back into a neural network. This is very useful
 * for both simulated annealing and genetic algorithms.
 * 
 * @author Jeff Heaton (http://www.heatonresearch.com)
 */
public class NeuralNetworkCODEC {

    /* Private constructor. */
    private NeuralNetworkCODEC() {

    }

    /**
     * Encode a network to an array.
     * 
     * @param network
     *            - the network to encode.
     */
    public static void network2array(NeuralNetwork network, double[] array) {
        Validator.validateNeuralNetwork(network);
        Validator.validateDouble(array);

        int index = 0;

        for (Layer layer : network.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getOutConnections()) {
                    array[index++] = connection.getWeight().getValue();
                }
            }
        }
    }

    /**
     * Decodes a network from an array.
     * 
     * @param array
     *            - the array used to decode.
     * @param network
     *            - the network to decode into.
     */
    public static void array2network(double[] array, NeuralNetwork network) {
        Validator.validateNeuralNetwork(network);
        Validator.validateDouble(array);

        int index = 0;

        for (Layer layer : network.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                for (Connection connection : neuron.getOutConnections()) {
                    connection.getWeight().setValue(array[index]);
                }
            }
        }
    }

    /**
     * Determines the array size for the given neural network.
     * 
     * @param network
     *            - the neural network to determine for.
     * @return The size of the array necessary to hold that network.
     */
    public static int determineArraySize(NeuralNetwork network) {
        Validator.validateNeuralNetwork(network);

        int result = 0;

        for (Layer layer : network.getLayers()) {
            for (Neuron neuron : layer.getNeurons()) {
                result += neuron.getOutConnections().size();
            }
        }
        return result;
    }
}
