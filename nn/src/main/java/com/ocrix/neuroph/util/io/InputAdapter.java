package com.ocrix.neuroph.util.io;

/**
 * Defines an input adapter.
 */
public interface InputAdapter {
	/**
	 * Reads input.
	 * 
	 * @return - double array
	 */
	public double[] readInput();

	/**
	 * Closes the input.
	 */
	public void close();
}
