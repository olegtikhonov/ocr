/**
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import com.ocrix.neuroph.core.input.Difference;
import com.ocrix.neuroph.core.input.WeightedInput;

/**
 * Contains weights functions types and labels. <br>
 * Available type are:
 * <ul>
 * <li>WEIGHTED_INPUT</li>
 * <li>DIFFERENCE</li>
 * </ul>
 */
public enum WeightsFunctionType {
    /**
     * 
     */
    WEIGHTED_INPUT("WeightedInput"),
    /**
     * 
     */
    DIFFERENCE("Difference");

    /* Value local holder */
    private String typeLabel;

    /**
     * Constructs the {@link WeightsFunctionType}.
     * 
     * @param typeLabel to be set as a value.
     */
    private WeightsFunctionType(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    /**
     * Gets the textual representation of the value.
     * 
     * @return type of the weights function.
     */
    public String getTypeLabel() {
        return typeLabel;
    }

    /**
     * Gets a weights class by its type.
     * 
     * @return either {@link WeightedInput} or {@link Difference} class.
     */
    public Class<?> getTypeClass() {
        switch (this) {
            case WEIGHTED_INPUT:
                return WeightedInput.class;
            case DIFFERENCE:
                return Difference.class;

            default:
                return WeightedInput.class;
        }
    }
}
