/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Benchmark {
    /* Task holder */
    private ArrayList<BenchmarkTask> tasks;

    /* Local logger */
    private static final Logger log = Logger.getLogger(Benchmark.class.getName());

    /**
     * Constructs a {@link Benchmark}.
     */
    public Benchmark() {
        tasks = new ArrayList<BenchmarkTask>();
    }

    /**
     * Adds a task to the task holder.
     * 
     * @param task
     *            to be ran.
     */
    public void addTask(BenchmarkTask task) {
        tasks.add(task);
    }

    public static void runTask(BenchmarkTask task) {
        log.info("Preparing task " + task.getName());
        task.prepareTest();

        log.info("Warming up " + task.getName());
        for (int i = 0; i < task.getWarmupIterations(); i++) {
            task.runTest();
        }

        log.info("Runing " + task.getName());
        // task.prepare(); // here might call a reset or init method that
        // randomize to a network, the primary class that so averse to anything
        // that could
        // to redefine and not

        Stopwatch timer = new Stopwatch();
        BenchmarkTaskResults results = new BenchmarkTaskResults(task.getTestIterations());

        for (int i = 0; i < task.getTestIterations(); i++) {
            timer.reset();

            timer.start();
            task.runTest();
            timer.stop();

            results.addElapsedTime(timer.getElapsedTime());
            // TODO: remove gc from here.
            // System.gc();
        }

        results.calculateStatistics();
        log.info(task.getName() + " results");
        log.info(results.toString()); // could be sent to file
    }

    public void run() {
        for (int i = 0; i < tasks.size(); i++)
            runTask(tasks.get(i));
    }

    /**
     * Gets a list of the benchmark tasks.
     * 
     * @return a list of the tasks.
     */
    public ArrayList<BenchmarkTask> getTasks() {
        return tasks;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [tasks=");
        builder.append(tasks);
        builder.append("]");
        return builder.toString();
    }
    
    /**
     * Sets log level to ALL.
     */
    public void logOn(){
        log.setLevel(Level.ALL);
    }
    
    /**
     * Sets log level to OFF.
     */
    public void logOff(){
        log.setLevel(Level.OFF);
    }
}
