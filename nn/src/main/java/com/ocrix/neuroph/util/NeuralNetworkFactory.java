/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import java.util.ArrayList;
import java.util.List;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.Layer;
import com.ocrix.neuroph.core.NeuralNetwork;
import com.ocrix.neuroph.core.Neuron;
import com.ocrix.neuroph.nnet.Adaline;
import com.ocrix.neuroph.nnet.BAM;
import com.ocrix.neuroph.nnet.CompetitiveNetwork;
import com.ocrix.neuroph.nnet.Hopfield;
import com.ocrix.neuroph.nnet.Instar;
import com.ocrix.neuroph.nnet.Kohonen;
import com.ocrix.neuroph.nnet.MaxNet;
import com.ocrix.neuroph.nnet.MultiLayerPerceptron;
import com.ocrix.neuroph.nnet.Outstar;
import com.ocrix.neuroph.nnet.Perceptron;
import com.ocrix.neuroph.nnet.RbfNetwork;
import com.ocrix.neuroph.nnet.SupervisedHebbianNetwork;
import com.ocrix.neuroph.nnet.UnsupervisedHebbianNetwork;
import com.ocrix.neuroph.nnet.comp.BiasNeuron;
import com.ocrix.neuroph.nnet.learning.BackPropagation;
import com.ocrix.neuroph.nnet.learning.BinaryDeltaRule;
import com.ocrix.neuroph.nnet.learning.DynamicBackPropagation;
import com.ocrix.neuroph.nnet.learning.MomentumBackpropagation;
import com.ocrix.neuroph.nnet.learning.PerceptronLearning;

/**
 * Provides methods to create various neural networks.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public final class NeuralNetworkFactory {
    
    
    private NeuralNetworkFactory(){}
    

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/CxMHH">Adaline network.</a>
     * 
     * @param inputsCount
     *            - number of inputs of Adaline network.
     * @return instance of Adaline network
     */
    public static Adaline createAdaline(int inputsCount) {
        Adaline nnet = new Adaline(inputsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/sIhhl">Perceptron network</a>
     * 
     * @param inputNeuronsCount
     *            - number of neurons in input layer.
     * @param outputNeuronsCount
     *            - number of neurons in output layer.
     * @param transferFunctionType
     *            - type of transfer function to use
     * @return instance of Perceptron network.
     */
    public static Perceptron createPerceptron(int inputNeuronsCount,
            int outputNeuronsCount, TransferFunctionType transferFunctionType) {
        Perceptron nnet = new Perceptron(inputNeuronsCount, outputNeuronsCount,
                transferFunctionType);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Perceptron network.
     * 
     * @param inputNeuronsCount
     *            - the number of neurons in input layer.
     * @param outputNeuronsCount
     *            - the number of neurons in output layer.
     * @param transferFunctionType
     *            - the type of transfer function to use.
     * @param learningRule
     *            - the learning rule class.
     * 
     * @return instance of Perceptron network.
     */
    public static Perceptron createPerceptron(int inputNeuronsCount,
            int outputNeuronsCount, TransferFunctionType transferFunctionType,
            Class<?> learningRule) {
        Perceptron nnet = new Perceptron(inputNeuronsCount, outputNeuronsCount,
                transferFunctionType);

        if (learningRule.getName().equals(PerceptronLearning.class.getName())) {
            nnet.setLearningRule(new PerceptronLearning());
        } else if (learningRule.getName().equals(
                BinaryDeltaRule.class.getName())) {
            nnet.setLearningRule(new BinaryDeltaRule());
        }

        return nnet;
    }

    /**
     * Creates and returns a new instance of Multi Layer Perceptron.
     * 
     * @param layersStr
     *            - the space separated number of neurons in layers.
     * @param transferFunctionType
     *            - the transfer function type for neurons.
     * 
     * @return instance of Multi Layer Perceptron.
     */
    public static MultiLayerPerceptron createMLPerceptron(String layersStr,
            TransferFunctionType transferFunctionType) {
        ArrayList<Integer> layerSizes = VectorParser.parseInteger(layersStr);
        MultiLayerPerceptron nnet = new MultiLayerPerceptron(layerSizes,
                transferFunctionType);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Multi Layer Perceptron.
     * 
     * @param layersStr
     *            - the space separated number of neurons in layers.
     * @param transferFunctionType
     *            - the transfer function type for neurons.
     * 
     * @throws IllegalArgumentException
     *             if one of the parameters are null.
     * 
     * @return instance of Multi Layer Perceptron.
     */
    public static MultiLayerPerceptron createMLPerceptron(String layersStr,
            TransferFunctionType transferFunctionType, Class<?> learningRule,
            boolean useBias, boolean connectIO) {

        /* Validations */
        Validator.validateString(layersStr);
        Validator.validateTransferFunctionType(transferFunctionType);
        Validator.validateObject(learningRule);

        ArrayList<Integer> layerSizes = VectorParser.parseInteger(layersStr);
        /* Creates a neuron properties indicating to use bias or not */
        NeuronProperties neuronProperties = new NeuronProperties(
                transferFunctionType, useBias);

        MultiLayerPerceptron nnet = new MultiLayerPerceptron(layerSizes,
                neuronProperties);

        /* Sets learning rule */
        if (learningRule.getName().equals(BackPropagation.class.getName())) {
            nnet.setLearningRule(new BackPropagation());
        } else if (learningRule.getName().equals(
                MomentumBackpropagation.class.getName())) {
            nnet.setLearningRule(new MomentumBackpropagation());
        } else if (learningRule.getName().equals(
                DynamicBackPropagation.class.getName())) {
            nnet.setLearningRule(new DynamicBackPropagation());
        }

        // connect io
        if (connectIO) {
            nnet.connectInputsToOutputs();
        }

        return nnet;
    }

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/GgKef">Hopfield network</a>
     * 
     * @param neuronsCount
     *            - the number of neurons in Hopfield network.
     * 
     * @return instance of Hopfield network.
     */
    public static Hopfield createHopfield(int neuronsCount) {
        Hopfield nnet = new Hopfield(neuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a href="http://goo.gl/nnlYu">BAM
     * network</a>
     * 
     * @param inputNeuronsCount
     *            - the number of input neurons.
     * @param outputNeuronsCount
     *            - the number of output neurons.
     * 
     * @return instance of BAM network.
     */
    public static BAM createBam(int inputNeuronsCount, int outputNeuronsCount) {
        BAM nnet = new BAM(inputNeuronsCount, outputNeuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/4Rqcm">Kohonen network</a>
     * 
     * @param inputNeuronsCount
     *            - the number of input neurons.
     * @param outputNeuronsCount
     *            - the number of output neurons.
     * 
     * @return instance of Kohonen network.
     */
    public static Kohonen createKohonen(int inputNeuronsCount,
            int outputNeuronsCount) {
        Kohonen nnet = new Kohonen(new Integer(inputNeuronsCount), new Integer(
                outputNeuronsCount));
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/hX1BB">Hebbian network</a>
     * 
     * @param inputNeuronsCount
     *            number of neurons in input layer.
     * @param outputNeuronsCount
     *            number of neurons in output layer.
     * @param transferFunctionType
     *            neuron's transfer function type.
     * 
     * @throws IllegalArgumentException
     *             if {@link TransferFunctionType} is NULL.
     * 
     * @return instance of Hebbian network.
     */
    public static SupervisedHebbianNetwork createSupervisedHebbian(
            int inputNeuronsCount, int outputNeuronsCount,
            TransferFunctionType transferFunctionType) {
        Validator.validateTransferFunctionType(transferFunctionType);
        SupervisedHebbianNetwork nnet = new SupervisedHebbianNetwork(
                inputNeuronsCount, outputNeuronsCount, transferFunctionType);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Unsupervised Hebbian Network.
     * 
     * @param inputNeuronsCount
     *            - the number of neurons in input layer.
     * @param outputNeuronsCount
     *            - the number of neurons in output layer.
     * @param transferFunctionType
     *            - the neuron's transfer function type.
     * 
     * @throws IllegalArgumentException
     *             if parameter is NULL;
     * 
     * @return instance of Unsupervised Hebbian Network.
     */
    public static UnsupervisedHebbianNetwork createUnsupervisedHebbian(
            int inputNeuronsCount, int outputNeuronsCount,
            TransferFunctionType transferFunctionType) {
        Validator.validateTransferFunctionType(transferFunctionType);
        UnsupervisedHebbianNetwork nnet = new UnsupervisedHebbianNetwork(
                inputNeuronsCount, outputNeuronsCount, transferFunctionType);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Max Net network. Max net is an
     * implementation of a maximum-finding function. With each iteration, the
     * neurons activations will decrease until only one neuron remains active.
     * The 'winner' is neuron that had the greatest output.
     * 
     * @param neuronsCount
     *            - number of neurons (same num in input and output layer)
     * 
     * @return instance of Max Net network
     */
    public static MaxNet createMaxNet(int neuronsCount) {
        MaxNet nnet = new MaxNet(neuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Instar network.
     * 
     * @param inputNeuronsCount
     *            - the umber of input neurons.
     * 
     * @return instance of Instar network.
     */
    public static Instar createInstar(int inputNeuronsCount) {
        Instar nnet = new Instar(inputNeuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of Outstar network.
     * 
     * @param outputNeuronsCount
     *            - number of output neurons.
     * @return instance of Outstar network.
     */
    public static Outstar createOutstar(int outputNeuronsCount) {
        Outstar nnet = new Outstar(outputNeuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a
     * href="http://goo.gl/YyrB4">competitive network</a>
     * 
     * @param inputNeuronsCount
     *            - the number of neurons in input layer.
     * @param outputNeuronsCount
     *            - the number of neurons in output layer.
     * 
     * @return instance of CompetitiveNetwork.
     */
    public static CompetitiveNetwork createCompetitiveNetwork(
            int inputNeuronsCount, int outputNeuronsCount) {
        CompetitiveNetwork nnet = new CompetitiveNetwork(inputNeuronsCount,
                outputNeuronsCount);
        return nnet;
    }

    /**
     * Creates and returns a new instance of <a href="http://goo.gl/N94tO">RBF
     * network</a>
     * 
     * @param inputNeuronsCount
     *            - the number of neurons in input layer.
     * @param rbfNeuronsCount
     *            - the number of neurons in RBF layer.
     * @param outputNeuronsCount
     *            - the number of neurons in output layer.
     * @return instance of RBF network.
     */
    public static RbfNetwork createRbfNetwork(int inputNeuronsCount,
            int rbfNeuronsCount, int outputNeuronsCount) {
        RbfNetwork nnet = new RbfNetwork(inputNeuronsCount, rbfNeuronsCount,
                outputNeuronsCount);
        return nnet;
    }

    /**
     * Sets default input and output neurons for network (first layer as input,
     * last as output).
     * 
     * @throws IllegalArgumentException if {@link NeuralNetwork} is NULL.
     */
    public static void setDefaultIO(NeuralNetwork nnet) {
        Validator.validateNeuralNetwork(nnet);
        
        ArrayList<Neuron> inputNeurons = new ArrayList<Neuron>();
        Layer firstLayer = nnet.getLayers().get(0);
        for (Neuron neuron : firstLayer.getNeurons()) {
            // TODO: change instanceof to String comparison
            if (!(neuron instanceof BiasNeuron)) { /*
                                                    * don't set input to bias
                                                    * neurons
                                                    */
                inputNeurons.add(neuron);
            }
        }

        List<Neuron> outputNeurons = (nnet.getLayers().get(nnet.getLayers()
                .size() - 1)).getNeurons();

        nnet.setInputNeurons(inputNeurons);
        nnet.setOutputNeurons(outputNeurons);
    }
}
