/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import com.ocrix.neuroph.core.transfer.Gaussian;
import com.ocrix.neuroph.core.transfer.Linear;
import com.ocrix.neuroph.core.transfer.Log;
import com.ocrix.neuroph.core.transfer.Ramp;
import com.ocrix.neuroph.core.transfer.Sgn;
import com.ocrix.neuroph.core.transfer.Sigmoid;
import com.ocrix.neuroph.core.transfer.Sin;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.core.transfer.Tanh;
import com.ocrix.neuroph.core.transfer.Trapezoid;

/**
 * Contains <a href="http://goo.gl/Rgwp7">transfer functions</a> types and
 * labels. <br>
 * <u>Available types are:</u>
 * <ul>
 * <li>LINEAR</li>
 * <li>RAMP</li>
 * <li>STEP</li>
 * <li>SIGMOID</li>
 * <li>TANH</li>
 * <li>GAUSSIAN</li>
 * <li>TRAPEZOID</li>
 * <li>SGN</li>
 * <li>SIN</li>
 * <li>LOG</li>
 * </ul>
 */
public enum TransferFunctionType {
    /**
     * <a href="http://goo.gl/TF7j8">Linear transfer function</a>
     */
    LINEAR("Linear"),
    /**
     * <a href="http://goo.gl/YTDDz">RAMP function</a>
     */
    RAMP("Ramp"),
    /**
     * <a href="http://goo.gl/CO0TT">Step function</a>
     */
    STEP("Step"),
    /**
     * <a href="http://goo.gl/gYK5n">Sigmoid function</a>
     */
    SIGMOID("Sigmoid"),
    /**
     * <a href="http://goo.gl/gpHjT">Hyperbolic tan function</a>
     */
    TANH("Tanh"),
    /**
     * <a href="http://goo.gl/jQ9H5">Gaussian function</a>
     */
    GAUSSIAN("Gaussian"),
    /**
     * <a href="http://goo.gl/Wv5wb">Trapezoid rule.</a>
     */
    TRAPEZOID("Trapezoid"),
    /**
     * <a href="http://en.wikipedia.org/wiki/Sign_function">Sign function.</a>
     */
    SGN("Sgn"),
    /**
     * <a href="http://goo.gl/RXIyM">Sine function.</a>
     */
    SIN("Sin"),
    /**
     * <a href="http://goo.gl/GLZNH">Log function</a>
     */
    LOG("Log");

    /* Local value holder */
    private String typeLabel;

    /**
     * Constructs a transfer function type.
     * 
     * @param typeLabel
     *            - associates key to value.
     */
    private TransferFunctionType(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    /**
     * Returns textual representation of the label.
     * 
     * @return
     */
    public String getTypeLabel() {
        return typeLabel;
    }

    /**
     * By given type, returns appropriate class.
     * 
     * @return specific class representing transfer function.
     */
    public Class<?> getTypeClass() {
        switch (this) {
            case LINEAR:
                return Linear.class;
            case STEP:
                return Step.class;
            case RAMP:
                return Ramp.class;
            case SIGMOID:
                return Sigmoid.class;
            case TANH:
                return Tanh.class;
            case TRAPEZOID:
                return Trapezoid.class;
            case GAUSSIAN:
                return Gaussian.class;
            case SGN:
                return Sgn.class;
            case SIN:
                return Sin.class;
            case LOG:
                return Log.class;
            default:
                return Linear.class;
        } // switch
    }
}
