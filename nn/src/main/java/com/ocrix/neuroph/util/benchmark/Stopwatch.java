/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ocrix.neuroph.util.benchmark;

/**
 * 
 * Based on <a href=
 * "http://java.sun.com/docs/books/performance/1st_edition/html/JPMeasurement.fm.html#17818"
 * > JPMeasurement</a>
 * 
 * A class to help benchmark code It simulates a real stop watch.
 */
public class Stopwatch {
    /* Start time */
    private long startTime = -1;
    /* Stop time */
    private long stopTime = -1;
    /* Indicates a status */
    private boolean running = false;

    /**
     * Constructs a {@link Stopwatch}
     */
    public Stopwatch() {
    }

    /**
     * Initializes the {@link Stopwatch}.
     */
    public void start() {
        startTime = System.currentTimeMillis();
        running = true;
    }

    /**
     * Stops the {@link Stopwatch}.
     */
    public void stop() {
        stopTime = System.currentTimeMillis();
        running = false;
    }

    /**
     * Returns elapsed time in milliseconds if the watch has never been started
     * then return zero.
     */
    public long getElapsedTime() {
        if (startTime == -1) {
            return 0;
        }
        if (running) {
            return System.currentTimeMillis() - startTime;
        } else {
            return stopTime - startTime;
        }
    }

    /**
     * Resets the start/stop times and status.
     */
    public void reset() {
        startTime = -1;
        stopTime = -1;
        running = false;
    }
}