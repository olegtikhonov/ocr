/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import static com.ocrix.neuroph.common.CommonConsts.DEFAULT_STACK_DEPTH;

import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.common.Validator;

/**
 * Provides methods to parse strings as Integer or Double vectors.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
// rename to ArrayUtils
public class VectorParser {
    /* Local logger */
    private final static Logger log = Logger.getLogger(VectorParser.class.getName());
    
    /* Does not allow instant creation */
    private VectorParser() {
    }

    /**
     * This method parses input String and returns Integer vector
     * <br>
     * Note: Catches a {@link NumberFormatException}.
     * 
     * @param str - the input String
     * 
     * @return Integer vector
     */
    static public ArrayList<Integer> parseInteger(String str) {
        StringTokenizer tok = new StringTokenizer(str);
        ArrayList<Integer> ret = new ArrayList<Integer>();
        while (tok.hasMoreTokens()) {
            try {
                Integer d = new Integer(tok.nextToken());
                ret.add(d);
            } catch (NumberFormatException e) {
                log.severe(ANNUtils.stackToString(e, DEFAULT_STACK_DEPTH.value()));
            }
            
        }
        return ret;
    }

    /**
     * This method parses input String and returns double array. Uses 'space' as
     * separator between values for splitting.
     * 
     * Note: Catches a {@link NumberFormatException}.
     * 
     * @param inputStr
     *            - the input String
     * @return double array
     */
    public static double[] parseDoubleArray(String inputStr) {
        Validator.validateString(inputStr);
        String[] inputsArrStr = inputStr.split(" ");

        double[] ret = new double[inputsArrStr.length];
        for (int i = 0; i < inputsArrStr.length; i++) {
            try {
                ret[i] = Double.parseDouble(inputsArrStr[i].replace("[", "") .replace("]", "").replace(",", ""));
            } catch (NumberFormatException e) {
                log.severe(ANNUtils.stackToString(e, DEFAULT_STACK_DEPTH.value()));
            }
        }

        return ret;
    }

    /**
     * Converts <code style="color=#990099">double[]</code> to double array
     * list.
     * 
     * @param list
     * @return
     */
    public static double[] toDoubleArray(List<Double> list) {
        Validator.validateList(list);
        double[] ret = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ret[i] = list.get(i).doubleValue();
        }
        return ret;
    }

    /**
     * Converts <code style="color=#990099">double[]</code> to {@link ArrayList}
     * 
     * @param array
     *            - to be converted.
     * 
     * @return a {@link ArrayList}.
     */
    public static ArrayList<Double> convertToVector(double[] array) {
        Validator.validateDouble(array);
        ArrayList<Double> vector = new ArrayList<Double>(array.length);

        for (double val : array) {
            vector.add(val);
        }

        return vector;
    }

    /**
     * Converts <code style="color=#990099">List<Double></code> to
     * <code style="color=#990099">double[]</code>.
     * 
     * @param list
     *            - a {@link List} to be converted.
     * 
     * @return a <code style="color=#990099">double[]</code>
     */
    public static double[] convertToArray(List<Double> list) {
        Validator.validateList(list);
        double[] array = new double[list.size()];

        int i = 0;
        for (double d : list) {
            array[i++] = d;
        }

        return array;
    }
}