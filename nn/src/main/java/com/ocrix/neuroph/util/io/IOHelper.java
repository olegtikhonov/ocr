/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ocrix.neuroph.util.io;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.NeuralNetwork;

/**
 * Class uses as helper for IO operations.
 * 
 * @author zoran
 */
public class IOHelper {

    /* Prevents initialization */
    private IOHelper() {
    }

    public static void process(NeuralNetwork neuralNet, InputAdapter in, OutputAdapter out) {
        /* Validation testing params are not null */
        Validator.validateNeuralNetwork(neuralNet);
        Validator.validateInputAdapter(in);
        Validator.validateOutputAdapter(out);

        double[] input;
        while ((input = in.readInput()) != null) {
            neuralNet.setInput(input);
            neuralNet.calculate();
            double[] output = neuralNet.getOutput();
            out.writeOutput(output);
        }

        in.close();
        out.close();
    }
}
