/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.benchmark;

import com.ocrix.neuroph.common.Validator;

/**
 * Serves as a benchmark task result container.
 */
public class BenchmarkTaskResults {
    /* A number of test iterations. */
    private int testIterations;
    /* Elapsed times */
    private long[] elapsedTimes;
    /* A time counter. */
    private int timesCounter;
    /* Average test time to be taken. */
    private double averageTestTime;
    /* A standard deviation. */
    private double standardDeviation;
    /* Minimum test time. */
    private double minTestTime;
    /* Maximum test time. */
    private double maxTestTime;

    /**
     * Constructs a {@link BenchmarkTaskResults}.
     * 
     * @param testIterations
     *            a number of test iterations.
     */
    public BenchmarkTaskResults(int testIterations) {
        Validator.validateIntInRange(testIterations, 0, Integer.MAX_VALUE);
        this.testIterations = testIterations;
        this.elapsedTimes = new long[testIterations];
        this.timesCounter = 0;
    }

    /**
     * Gets a average test time.
     * 
     * @return a average test time.
     */
    public double getAverageTestTime() {
        return averageTestTime;
    }

    /**
     * Gets a elapsed times.
     * 
     * @return the elapsed times.
     */
    public long[] getElapsedTimes() {
        return elapsedTimes;
    }

    /**
     * Gets a maximum test time.
     * 
     * @return a maximum test time.
     */
    public double getMaxTestTime() {
        return maxTestTime;
    }

    /**
     * Gets a minimum test time.
     * 
     * @return a number of test time.
     */
    public double getMinTestTime() {
        return minTestTime;
    }

    /**
     * Gets a standard deviation.
     * 
     * @return a calculated standard deviation.
     */
    public double getStandardDeviation() {
        return standardDeviation;
    }

    /**
     * Gets a number of test iterations.
     * 
     * @return a number of test iterations.
     */
    public int getTestIterations() {
        return testIterations;
    }

    /**
     * Adds elapsed time.
     * 
     * @param time
     *            to be added to elapsed times.
     */
    public void addElapsedTime(long time) {
        Validator.validateLongInRange(time, 0, Long.MAX_VALUE);
        this.elapsedTimes[timesCounter++] = time;
    }

    /**
     * Calculates statistics of the test result.
     */
    public void calculateStatistics() {

        this.minTestTime = elapsedTimes[0];
        this.maxTestTime = elapsedTimes[0];
        long sum = 0;

        for (int i = 0; i < timesCounter; i++) {
            sum += elapsedTimes[i];
            if (elapsedTimes[i] < minTestTime) {
                minTestTime = elapsedTimes[i];
            }
            if (elapsedTimes[i] > maxTestTime) {
                maxTestTime = elapsedTimes[i];
            }
        }

        if (timesCounter != 0) {
            this.averageTestTime = sum / (double) timesCounter;
        } else {
            this.averageTestTime = 0;
        }

        // std. deviation
        long sqrSum = 0;

        for (int i = 0; i < timesCounter; i++) {
            sqrSum += (elapsedTimes[i] - averageTestTime)
                    * (elapsedTimes[i] - averageTestTime);
        }

        this.standardDeviation = Math.sqrt(sqrSum / (double) timesCounter);
    }

    @Override
    public String toString() {
        String results = "Test iterations: " + testIterations + "\n"
                + "Min time: " + minTestTime + "\n" + "Max time: "
                + maxTestTime + "\n" + "Average time: " + averageTestTime
                + "\n" + "Std. deviation: " + standardDeviation + "\n";

        results += "Test times:\n";

        for (int i = 0; i < timesCounter; i++)
            results += i + ". iteration: " + elapsedTimes[i] + "\n";

        return results;
    }
}
