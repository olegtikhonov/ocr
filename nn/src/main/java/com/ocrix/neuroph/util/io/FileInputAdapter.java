/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.ocrix.neuroph.common.Validator;

/**
 * {@link InputStreamAdapter} extension.
 * 
 * @author zoran
 */
public class FileInputAdapter extends InputStreamAdapter {

    /**
     * Constructs {@link FileInputAdapter}.
     * 
     * @param file to be constructed from. 
     * @throws IOException 
     */
    public FileInputAdapter(File file) throws IOException {
        super(file);
    }
    
    /**
     * Constructs a {@link FileInputAdapter}.
     * 
     * @param fileName to be constructed from.
     * @throws IOException 
     */
    public FileInputAdapter(String fileName) throws IOException {
        this(createFile(fileName));
    }

    public FileInputAdapter(BufferedReader br){
        super(br);
    }
    
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        return builder.toString();
    }
    
    /**
     * Creates a {@link FileReader}.
     * 
     * @param file to be created from.
     * 
     * @return a {@link FileReader}.
     * 
     * @throws FileNotFoundException
     */
    @SuppressWarnings("unused")
    private static FileReader createFileReader(File file) throws FileNotFoundException {
        Validator.validateFile(file);
        return new FileReader(file);
    }
    
    /**
     * Creates a file from proveded path.
     * 
     * @param pathToFile to be created from.
     * 
     * @return a file.
     */
    private static File createFile(String pathToFile){
        Validator.validateString(pathToFile);
        return new File(pathToFile);
    }
}