/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.util;

import com.ocrix.neuroph.core.input.And;
import com.ocrix.neuroph.core.input.Intensity;
import com.ocrix.neuroph.core.input.Max;
import com.ocrix.neuroph.core.input.Min;
import com.ocrix.neuroph.core.input.Or;
import com.ocrix.neuroph.core.input.Product;
import com.ocrix.neuroph.core.input.Sum;
import com.ocrix.neuroph.core.input.SumSqr;

/**
 * Contains summing functions types and labels.
 * <br>The following labels are available:
 * <ul>
 * <li>SUM</li>
 * <li>INTENSITY</li>
 * <li>AND</li>
 * <li>OR</li>
 * <li>SUMSQR</li>
 * <li>MIN</li>
 * <li>MAX</li>
 * <li>PRODUCT</li>
 * </ul>
 */
public enum SummingFunctionType {
    /**
     * Defines 'Sum'
     */
    SUM("Sum"), 
    
    /**
     * Defines 'Intensity'
     */
    INTENSITY("Intensity"), 
    
    /**
     * Defines 'And'
     */
    AND("And"), 
    
    /**
     * Defines 'Or'
     */
    OR("Or"), 
    
    /**
     * Defines 'SumSqr'
     */
    SUMSQR("SumSqr"), 
    
    /**
     * Defines 'Min'
     */
    MIN("Min"), 
    
    /**
     * Defines 'Max'
     */
    MAX("Max"), 
    
    /**
     * Defines 'Product'
     */
    PRODUCT("Product");

    /* Label of the type */
    private String typeLabel;

    private SummingFunctionType(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    /**
     * Gets a type of the class.
     * 
     * @return a class type. 
     */
    public Class<?> getTypeClass() {
        switch (this) {
            case SUM:
                return Sum.class;
            case INTENSITY:
                return Intensity.class;
            case AND:
                return And.class;
            case OR:
                return Or.class;
            case SUMSQR:
                return SumSqr.class;
            case MIN:
                return Min.class;
            case MAX:
                return Max.class;
            case PRODUCT:
                return Product.class;
            default:
                return Sum.class;
        }
    }
}
