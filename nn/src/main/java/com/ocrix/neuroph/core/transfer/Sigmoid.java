/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_INVALID_TRANSFER_FUNCTION_PROPS;
import static com.ocrix.neuroph.common.NetProperty.SLOPE;

import java.io.Serializable;
import java.util.logging.Logger;

import com.ocrix.neuroph.util.Properties;

/**
 * <pre>
 * Sigmoid neuron transfer function.
 * 
 * output = 1 / (1 + e ^ (-slope * input))
 * </pre>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Sigmoid extends TransferFunction implements Serializable {
    /* A logger */
    private static final Logger log = Logger.getLogger(Sigmoid.class.getName());
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;
    /* The slope parameter of the sigmoid function */
    private double slope = 1d;

    // --------------------------------------

    /**
     * Creates an instance of Sigmoid neuron transfer function with default
     * <code>slope = 1</code>.
     */
    public Sigmoid() {
    }

    /**
     * Creates an instance of Sigmoid neuron transfer function with specified
     * value for slope parameter.
     * 
     * @param slope
     *            the slope parameter for the sigmoid function.
     */
    public Sigmoid(double slope) {
        this.slope = slope;
    }

    /**
     * Creates an instance of Sigmoid neuron transfer function with the
     * specified properties.
     * 
     * @param properties
     *            properties of the sigmoid function
     */
    public Sigmoid(Properties properties) {
        try {
            //            this.slope = (Double) properties.getProperty(SLOPE.to());
            this.slope = Double.parseDouble(properties.getProperty(SLOPE.to()).toString());
        } catch (NullPointerException e) {
            // if properties are not set just leave default values
        } catch (NumberFormatException e) {
            log.severe(ERROR_INVALID_TRANSFER_FUNCTION_PROPS.to());
        }
    }

    /**
     * Returns the slope parameter of this function.
     * 
     * @return slope parameter of this function.
     */
    public double getSlope() {
        return this.slope;
    }

    /**
     * Sets the slope parameter for this function.
     * 
     * @param slope
     *            value for the slope parameter.
     */
    public void setSlope(double slope) {
        this.slope = slope;
    }

    @Override
    public double getOutput(double net) {
        /* conditional logic helps to avoid NaN */
        if (net > 100) {
            return 1.0;
        } else if (net < -100) {
            return 0.0;
        }

        double den = 1d + Math.exp(-this.slope * net);
        return (1d / den);
    }

    @Override
    public double getDerivative(double net) {
        double out = getOutput(net);
        /*
         * +0.1 is fix for flat spot see
         * http://www.heatonresearch.com/wiki/Flat_Spot
         */
        double derivative = this.slope * out * (1d - out) + 0.1;
        return derivative;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [slope=");
        builder.append(slope);
        builder.append("]");
        return builder.toString();
    }
}
