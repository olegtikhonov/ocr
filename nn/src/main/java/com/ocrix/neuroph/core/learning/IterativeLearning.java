/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static com.ocrix.neuroph.common.CommonMessages.WARN_TRAINNING_SET_NULL;
import static com.ocrix.neuroph.common.CommonMessages.WARN_TS_IS_NOT_SUPERVISED;

import java.io.Serializable;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.Validator;

/**
 * Base class for all iterative learning algorithms. It provides the iterative
 * learning procedure for all of its subclasses.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
abstract public class IterativeLearning extends LearningRule implements
        Serializable {

    /**
     * The class finger-print that is set to indicate serialization
     * compatibility with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Learning rate parametar
     */
    protected double learningRate = 0.1d;

    /**
     * Current iteration counter
     */
    protected int currentIteration = 0;

    /**
     * Max training iterations (when to stopLearning training) TODO: this field
     * should be private, to force use of setMaxIterations from derived classes,
     * so iterationsLimited flag is also set at the same time. Will that break
     * backward compatibility with serialized networks?
     */
    protected int maxIterations = Integer.MAX_VALUE;

    /**
     * Flag for indicating if the training iteration number is limited
     */
    protected boolean iterationsLimited = false;

    /**
     * Flag for indicating if learning thread is paused.
     */
    private transient volatile boolean pausedLearning = false;

    /* Local logger */
    private static final Logger log = Logger.getLogger(IterativeLearning.class
            .getName());

    // -------------------------------

    /**
     * Creates new instance of IterativeLearning learning algorithm
     */
    public IterativeLearning() {
        super();
    }

    /**
     * Returns learning rate for this algorithm.
     * 
     * @return learning rate for this algorithm.
     */
    public double getLearningRate() {
        return this.learningRate;
    }

    /**
     * Sets learning rate for this algorithm.
     * 
     * @param learningRate
     *            - the learning rate for this algorithm.
     */
    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    /**
     * Sets iteration limit for this learning algorithm.
     * 
     * @param maxIterations
     *            - the iteration limit for this learning algorithm.
     */
    public void setMaxIterations(int maxIterations) {
        this.maxIterations = maxIterations;
        this.iterationsLimited = true;
    }

    /**
     * Returns current iteration of this learning algorithm.
     * 
     * @return current iteration of this learning algorithm.
     */
    public Integer getCurrentIteration() {
        return Integer.valueOf(this.currentIteration);
    }

    /**
     * Returns true if learning thread is paused, false otherwise
     * 
     * @return true if learning thread is paused, false otherwise
     */
    public boolean isPausedLearning() {
        return pausedLearning;
    }

    /**
     * Pauses the learning.
     */
    public void pause() {
        this.pausedLearning = true;
    }

    /**
     * Resumes the paused learning
     */
    public void resume() {
        this.pausedLearning = false;
        synchronized (this) {
            this.notify();
        }
    }

    /**
     * This method is executed when learning starts, before the first epoch.
     * Used for initialization.
     */
    protected void onStart() {
        this.currentIteration = 0;
    }

    protected void beforeEpochStart() {

    }

    protected void afterEpochEnd() {

    }

    @Override
    public void learn(@SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        if (trainingSet.elementAt(0).getClass().getName()
                .contains(SupervisedTrainingElement.class.getSimpleName())) {
            onStart();
            while (!isStopped()) {
                beforeEpochStart();
                doLearningEpoch(trainingSet);
                this.currentIteration++;
                afterEpochEnd();

                // todo: abstract stop condition - create abstract class or
                // interface StopCondition
                if (iterationsLimited && (currentIteration == maxIterations)) {
                    stopLearning();
                } else if (!iterationsLimited
                        && (currentIteration == Integer.MAX_VALUE)) {
                    // restart iteration counter since it has reached max value
                    // and
                    // iteration numer is not limited
                    this.currentIteration = 1;
                }

                this.notifyChange(); // notify observers

                // Thread safe pause
                if (this.pausedLearning) {
                    synchronized (this) {
                        while (this.pausedLearning) {
                            try {
                                this.wait();
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
        } else {
            log.warning(WARN_TS_IS_NOT_SUPERVISED.to());
        }
    }

    /**
     * Trains network for the specified training set and number of iterations.
     * 
     * @param trainingSet
     *            - the training set to learn (Should be supervised training
     *            set).
     * @param maxIterations
     *            - maximum number of iterations to learn
     * 
     */
    public void learn(@SuppressWarnings("rawtypes") TrainingSet trainingSet,
            int maxIterations) {
        Validator.validateTraining(trainingSet);
        Validator.validateIntInRange(maxIterations, 0, Integer.MAX_VALUE);
        this.setMaxIterations(maxIterations);
        this.learn(trainingSet);
    }

    /**
     * Runs one learning iteration for the specified training set and notifies
     * observers. This method does the the doLearningEpoch() and in addition
     * notifies observers when iteration is done.
     * 
     * @param trainingSet
     *            - the training set to learn.
     */
    public void doOneLearningIteration(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        if (trainingSet != null) {
            if (trainingSet.elementAt(0).getClass().getName()
                    .contains(SupervisedTrainingElement.class.getSimpleName())) {
                beforeEpochStart();
                this.doLearningEpoch(trainingSet);
                this.notifyChange(); // notify observers
            } else {
                log.warning(WARN_TS_IS_NOT_SUPERVISED.to());
            }
        } else {
            log.warning(WARN_TRAINNING_SET_NULL.to());
        }
    }

    /**
     * Override this method to implement specific learning epoch - one learning
     * iteration, one pass through whole training set.
     * 
     * @param trainingSet
     *            - the training set.
     */
    abstract public void doLearningEpoch(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet);

}