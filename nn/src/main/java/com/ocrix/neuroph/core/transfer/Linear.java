/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_INVALID_TRANSFER_FUNCTION_PROPS;
import static com.ocrix.neuroph.common.NetProperty.SLOPE;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ocrix.neuroph.util.Properties;

/**
 * Defines a Linear neuron transfer function.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Linear extends TransferFunction implements Serializable {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /* The slope parameter of the linear function */
    private double slope = 1d;

    /* Local logger */
    private static final Logger log = Logger.getLogger(Linear.class.getName());

    // ---------------------------------

    /**
     * Creates an instance of Linear transfer function
     */
    public Linear() {
    }

    /**
     * Creates an instance of Linear transfer function with specified value for
     * {@link #getSlope()} parameter.
     */
    public Linear(double slope) {
        this.slope = slope;
    }

    /**
     * Creates an instance of Linear transfer function with specified
     * properties.
     */
    public Linear(Properties properties) {
        try {
            // this.slope = (Double) properties.getProperty(SLOPE.to());
            this.slope = Double.parseDouble(properties.getProperty(SLOPE.to())
                    .toString());
        } catch (NullPointerException e) {
            // if properties are not set just leave default values
        } catch (NumberFormatException e) {
            log.setLevel(Level.SEVERE);
            log.severe(ERROR_INVALID_TRANSFER_FUNCTION_PROPS.to());
        }
    }

    /**
     * Returns the slope parameter of this function.
     * 
     * @return slope parameter of this function.
     */
    public double getSlope() {
        return this.slope;
    }

    /**
     * Sets the slope parameter for this function.
     * 
     * @param slope
     *            - the value for the slope parameter.
     */
    public void setSlope(double slope) {
        this.slope = slope;
    }

    @Override
    public double getOutput(double net) {
        return slope * net;
    }

    @Override
    public double getDerivative(double net) {
        return this.slope;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [slope=");
        builder.append(slope);
        builder.append("]");
        return builder.toString();
    }
}
