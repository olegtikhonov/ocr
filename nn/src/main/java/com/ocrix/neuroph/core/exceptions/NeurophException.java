package com.ocrix.neuroph.core.exceptions;

/**
 * Base exception type for Neuroph.
 * 
 * @author jheaton
 */
public class NeurophException extends RuntimeException {
    /**
     * The version ID.
     */
    private static final long serialVersionUID = 0L;

    /**
     * Default constructor.
     */
    public NeurophException() {

    }

    /**
     * Constructs a message exception.
     * 
     * @param msg
     *            - the exception message.
     */
    public NeurophException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an exception that holds another exception.
     * 
     * @param t
     *            - the other exception.
     */
    public NeurophException(final Throwable t) {
        super(t);
    }

    /**
     * Constructs an exception that holds another exception.
     * 
     * @param msg
     *            - the message.
     * @param t
     *            - the other exception.
     */
    public NeurophException(final String msg, final Throwable t) {
        super(msg, t);
    }
}
