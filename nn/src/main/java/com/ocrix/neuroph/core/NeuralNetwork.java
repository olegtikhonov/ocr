/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.CommonConsts.DEFAULT_LIST_SIZE;
import static com.ocrix.neuroph.common.CommonMessages.ERROR_NEURON_LIST_NULL;
import static com.ocrix.neuroph.common.CommonMessages.FILE_NOT_FOUND;
import static com.ocrix.neuroph.common.CommonMessages.LOG_LEVEL_PROPERTY;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;

import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.exceptions.VectorSizeMismatchException;
import com.ocrix.neuroph.core.learning.IterativeLearning;
import com.ocrix.neuroph.core.learning.LearningRule;
import com.ocrix.neuroph.core.learning.TrainingSet;
import com.ocrix.neuroph.nnet.learning.HopfieldLearning;
import com.ocrix.neuroph.util.NeuralNetworkType;
import com.ocrix.neuroph.util.plugins.PluginBase;
import com.ocrix.neuroph.util.random.RangeRandomizer;
import com.ocrix.neuroph.util.random.WeightsRandomizer;

/**
 * <pre>
 * Base class for artificial neural networks. It provides generic structure and functionality
 * for the neural networks. Neural network contains a collection of neuron layers and learning rule.
 * Custom neural networks are created by deriving from this class, creating layers of interconnected network specific neurons,
 * and setting network specific learning rule.
 * </pre>
 * 
 * @see Layer
 * @see LearningRule
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class NeuralNetwork extends Observable implements Serializable {
    private static final Logger logger = Logger.getLogger(NeuralNetwork.class
            .getName());

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 4L;

    /**
     * Network type id (see neuroph.util.NeuralNetworkType).
     */
    private NeuralNetworkType type;

    /**
     * Neural network layers.
     */
    private final List<Layer> layers;

    /**
     * Reference to network input neurons.
     */
    private List<Neuron> inputNeurons;

    /**
     * Reference to network output neurons.
     */
    private List<Neuron> outputNeurons;

    /**
     * Learning rule for this network.
     */
    private LearningRule learningRule; // learning algorithm

    /**
     * Separate thread for learning rule.
     */
    private transient Thread learningThread; // thread for learning rule

    /**
     * Plug-ins collection
     */
    @SuppressWarnings("rawtypes")
    private final Map<Class, PluginBase> plugins;

    /**
     * Label for this network
     */
    private String label = "";

    // --------------------------------------

    /**
     * Creates an instance of empty neural network.
     */
    @SuppressWarnings("rawtypes")
    public NeuralNetwork() {
        this.layers = new ArrayList<Layer>();
        this.plugins = new HashMap<Class, PluginBase>();
        // this.addPlugin(new LabelsPlugin());
        this.addPlugin(new PluginBase());
        this.label = RandomStringUtils.randomAlphabetic(DEFAULT_LIST_SIZE
                .value());
        this.outputNeurons = new ArrayList<Neuron>(DEFAULT_LIST_SIZE.value());
        this.inputNeurons = new ArrayList<Neuron>(DEFAULT_LIST_SIZE.value());
    }

    /**
     * Adds a layer to neural network.
     * 
     * @param layer
     *            - the layer to add.
     */
    public void addLayer(Layer layer) {
        layer.setParentNetwork(this);
        this.layers.add(layer);
    }

    /**
     * Adds layer to specified index position in network.
     * 
     * @param idx
     *            - an index position to add layer.
     * @param layer
     *            - the layer to add.
     */
    public void addLayer(int idx, Layer layer) {
        layer.setParentNetwork(this);
        this.layers.add(idx, layer);
    }

    /**
     * Removes specified layer from network.
     * 
     * @param layer
     *            - the layer to remove.
     */
    public void removeLayer(Layer layer) {
        this.layers.remove(layer);
    }

    /**
     * Removes layer at specified index position from net.
     * 
     * @param idx
     *            - int value represents index position of layer which should be
     *            removed.
     */
    public void removeLayerAt(int idx) {
        this.layers.remove(idx);
    }

    /**
     * Returns interface for iterating layers.
     * 
     * @return iterator interface for network getLayersIterator.
     */
    public Iterator<Layer> getLayersIterator() {
        return this.layers.iterator();
    }

    /**
     * Returns layers collection.
     * 
     * @return layers collection.
     */
    public List<Layer> getLayers() {
        return this.layers;
    }

    /**
     * Returns layer at specified index.
     * 
     * @param idx
     *            - layer index position.
     * @return layer at specified index position.
     */
    public Layer getLayerAt(int idx) {
        if (layers != null && layers.size() > 0) {
            return this.layers.get(idx);
        } else {
            logWarnMessage("The layer seems to be NULL, return new Layer() instead. It's done because of preventing NPE.");
            return new Layer();
        }

    }

    /**
     * Returns index position of the specified layer.
     * 
     * @param layer
     *            - requested Layer object.
     * @return layer position index.
     */
    public int indexOf(Layer layer) {
        return this.layers.indexOf(layer);
    }

    /**
     * Returns number of layers in network.
     * 
     * @return number of layers in net.
     */
    public int getLayersCount() {
        return this.layers.size();
    }

    /**
     * Sets network input. Input is an array of double values. If a list of
     * input neurons is null, it reinitializes it, putting default neurons in
     * it.
     * 
     * @param inputVector
     *            - network input as double array.
     */
    public void setInput(double... inputVector)
            throws VectorSizeMismatchException {

        // if (!inputNeurons.isEmpty()
        // && inputVector.length != inputNeurons.size()) {
        // throw new VectorSizeMismatchException(ERROR_INCORRECT_DIM.to());
        // }

        // if (!inputNeurons.isEmpty()) {
        // throw new VectorSizeMismatchException(ERROR_INCORRECT_DIM.to());
        // }
        //
        // /* Not sure if we have to take care of ... */
        // else
        if (inputVector != null && inputVector.length != inputNeurons.size()) {
            logWarnMessage(ERROR_NEURON_LIST_NULL.to());

            int maxLenght = Math.max(inputVector.length, inputNeurons.size());

            inputNeurons = new ArrayList<Neuron>(maxLenght);
            int countDown = maxLenght;// inputVector.length;
            while (countDown-- > 0) {
                inputNeurons.add(new Neuron());
            }
        }

        int i = 0;
        for (Neuron neuron : this.inputNeurons) {
            /* Sets input value to the corresponding neuron */
            if (inputVector != null && i < inputVector.length) {
                neuron.setInput(inputVector[i]);
                i++;
            }
        }

    }

    /**
     * Returns network output Vector. Output Vector is a collection of Double
     * values.
     * 
     * @return network output Vector.
     */
    public double[] getOutput() {
        Validator.validateList(outputNeurons);
        double[] outputVector = new double[outputNeurons.size()];

        int i = 0;
        for (Neuron neuron : this.outputNeurons) {
            outputVector[i] = neuron.getOutput();
            i++;
        }

        return outputVector;
    }

    /**
     * Performs calculation on whole network.
     */
    public void calculate() {
        for (Layer layer : this.layers) {
            layer.calculate();
        }
    }

    /**
     * Resets the activation levels of the neurons for entire network. I.e. set
     * input/output of each neuron to zero.
     */
    public void reset() {
        for (Layer layer : this.layers) {
            layer.reset();
        }
    }

    /**
     * Starts learning in a new thread to learn the specified training set, and
     * immediately returns from method to the current thread execution. If the
     * learning rule is null, than default (Hopfield) rule will be set.
     * 
     * @param trainingSetToLearn
     *            set of training elements to learn.
     */
    public void learnInNewThread(TrainingSet<?> trainingSetToLearn) {
        if (learningRule == null) {
            logWarnMessage("The learning rule is null. "
                    + HopfieldLearning.class.getName()
                    + " is set as default learning rule.");
            learningRule = new HopfieldLearning();
            learningRule.setNeuralNetwork(this);
        }

        learningRule.setTrainingSet(trainingSetToLearn);
        learningThread = new Thread(learningRule);
        learningRule.setStarted();
        learningThread.start();

    }

    /**
     * Starts learning with specified learning rule in new thread to learn the
     * specified training set, and immediately returns from method to the
     * current thread execution.
     * 
     * @param trainingSetToLearn
     *            - the set of training elements to learn.
     * @param learningRule
     *            - learning algorithm.
     */
    public void learnInNewThread(TrainingSet<?> trainingSetToLearn,
            LearningRule learningRule) {
        setLearningRule(learningRule);
        learningRule.setTrainingSet(trainingSetToLearn);
        learningThread = new Thread(learningRule);
        learningRule.setStarted();
        learningThread.start();
    }

    // /**
    // * Starts the learning in the current running thread to learn the
    // specified
    // * training set, and returns from method when network is done learning.
    // This
    // * method is @deprecated, see learn method which does the same
    // *
    // * @param trainingSetToLearn
    // * - set of training elements to learn //
    // */
    // @Deprecated
    // public void learnInSameThread(TrainingSet<?> trainingSetToLearn) {
    // learningRule.setTrainingSet(trainingSetToLearn);
    // learningRule.setStarted();
    // learningRule.run();
    // }

    /**
     * Learns the specified training set.
     * 
     * @param trainingSet
     *            - the set of training elements to learn.
     */
    public void learn(TrainingSet<?> trainingSet) {
        if (learningRule == null) {
            logWarnMessage("The learning rule is null. "
                    + HopfieldLearning.class.getName()
                    + " is set as default learning rule.");
            learningRule = new HopfieldLearning();
            learningRule.setNeuralNetwork(this);
        }
        learningRule.setTrainingSet(trainingSet);
        learningRule.setStarted();
        learningRule.run();
    }

    /**
     * Learns the specified training set, using specified learning rule.
     * 
     * @param trainingSetToLearn
     *            - a set of training elements to learn.
     * @param learningRule
     *            - an instance of learning rule to use for learning.
     */
    public void learn(TrainingSet<?> trainingSetToLearn,
            LearningRule learningRule) {
        Validator.validateLearningRule(learningRule);
        Validator.validateTraining(trainingSetToLearn);

        setLearningRule(learningRule);
        learningRule.setTrainingSet(trainingSetToLearn);
        learningRule.setStarted();
        learningRule.run();
    }

    // /**
    // * Starts the learning with specified learning rule in the current running
    // * thread to learn the specified training set, and returns from method
    // when
    // * network is done learning. This method is @deprecated, see learn method
    // * which does the same.
    // *
    // * @param trainingSetToLearn
    // * set of training elements to learn.
    // * @param learningRule
    // * learning algorithm.
    // */
    // @Deprecated
    // public void learnInSameThread(TrainingSet<?> trainingSetToLearn,
    // LearningRule learningRule) {
    // setLearningRule(learningRule);
    // learningRule.setTrainingSet(trainingSetToLearn);
    // learningRule.setStarted();
    // learningRule.run();
    // }

    /**
     * Stops learning.
     */
    public void stopLearning() {
        if (learningRule != null) {
            learningRule.stopLearning();
            if (learningThread != null) {
                logWarnMessage("Thread status "
                        + learningThread.getState().name());
            }
        }
        // TODO: remove logging
        logWarnMessage("Stops learning.");
    }

    /**
     * Pauses the learning - puts learning thread in wait state. Makes sense
     * only when learning is done in new thread with learnInNewThread() method.
     * The learning rule should be {@link IterativeLearning} instance.
     */
    public void pauseLearning() {
        /* TODO: remove instanceOf */
        if (learningRule instanceof IterativeLearning) {
            ((IterativeLearning) learningRule).pause();
        }
    }

    /**
     * Resumes paused learning - notifies the learning rule to continue. IFF
     * learning rule is instance of {@link IterativeLearning}.
     */
    public void resumeLearning() {
        /* TODO: remove instanceOf */
        if (learningRule instanceof IterativeLearning) {
            ((IterativeLearning) learningRule).resume();
        }
    }

    /**
     * Randomizes connection weights for the whole network.
     */
    public void randomizeWeights() {
        /* TODO: remove new */
        // randomizeWeights(new WeightsRandomizer());
        randomizeWeights(WeightsRandomizer.getInstance());
    }

    /**
     * Randomizes connection weights for the whole network within specified
     * value range.
     */
    public void randomizeWeights(double minWeight, double maxWeight) {
        RangeRandomizer.getInstance().setRangeRandomizer(minWeight, maxWeight);
        randomizeWeights(RangeRandomizer.getInstance());
    }

    /**
     * Randomizes connection weights for the whole network using specified
     * random generator.
     */
    public void randomizeWeights(Random random) {
        // randomizeWeights(new WeightsRandomizer(random));
        WeightsRandomizer.getInstance().setCustomRandomizer(random);
        randomizeWeights(WeightsRandomizer.getInstance());
    }

    /**
     * Randomizes connection weights for the whole network using specified
     * randomizer.
     * 
     * @param randomizer
     *            - the random weight generator to use.
     */
    public void randomizeWeights(WeightsRandomizer randomizer) {
        randomizer.randomize(this);
    }

    /**
     * Initializes connection weights for the whole network to a value.
     * 
     * @param value
     *            - the weight value.
     */
    public void initializeWeights(double value) {
        for (Layer layer : this.layers) {
            layer.initializeWeights(value);
        }
    }

    /**
     * Initializes connection weights for the whole network using a random
     * number generator call the randomizeWeights... - they are same... or use
     * randomizeWeights(Random random) if you need to specify random generator.
     * 
     * @param generator
     *            - the random number generator. // * @deprecated
     */
    // @Deprecated
    public void initializeWeights(Random generator) {
        for (Layer layer : this.layers) {
            layer.initializeWeights(generator);
        }
    }

    /**
     * Uses randomizeWeights(double minWeight, double maxWeight) instead.
     * 
     * @param min
     * @param max
     *            // * @deprecated
     */
    // @Deprecated
    public void initializeWeights(double min, double max) {
        randomizeWeights(min, max);
    }

    /**
     * Returns type of this network
     * 
     * @return network type
     */
    public NeuralNetworkType getNetworkType() {
        return type;
    }

    /**
     * Sets type for this network.
     * 
     * @param type
     *            - the network type.
     * @see #NeuralNetworkType
     */
    public void setNetworkType(NeuralNetworkType type) {
        this.type = type;
    }

    /**
     * Gets reference to input neurons Vector.
     * 
     * @return input neurons Vector.
     */
    public List<Neuron> getInputNeurons() {
        return this.inputNeurons;
    }

    /**
     * Sets reference to input neurons Vector.
     * 
     * @param inputNeurons
     *            - the input neurons collection.
     */
    public void setInputNeurons(List<Neuron> inputNeurons) {
        this.inputNeurons = inputNeurons;
    }

    /**
     * Returns reference to output neurons Vector.
     * 
     * @return output neurons Vector.
     */
    public List<Neuron> getOutputNeurons() {
        return this.outputNeurons;
    }

    /**
     * Sets reference to output neurons Vector.
     * 
     * @param outputNeurons
     *            - the output neurons collection.
     */
    public void setOutputNeurons(List<Neuron> outputNeurons) {
        this.outputNeurons = outputNeurons;
    }

    /**
     * Returns the learning algorithm of this network.
     * 
     * @return algorithm for network training.
     */
    public LearningRule getLearningRule() {
        return this.learningRule;
    }

    /**
     * Sets learning algorithm for this network.
     * 
     * @param learningRule
     *            - the learning algorithm for this network.
     */
    public void setLearningRule(LearningRule learningRule) {
        learningRule.setNeuralNetwork(this);
        this.learningRule = learningRule;
    }

    /**
     * Returns the current learning thread (if it is learning in the new thread.
     * Checks what happens if it learns in the same thread).
     */
    public Thread getLearningThread() {
        return learningThread;
    }

    /**
     * Notifies observers about some change.
     */
    public void notifyChange() {
        setChanged();
        notifyObservers();
        clearChanged();
    }

    /**
     * Creates connection with specified weight value between specified neurons.
     * 
     * @param fromNeuron
     *            - the neuron to connect.
     * @param toNeuron
     *            - the neuron to connect to.
     * @param weightVal
     *            - the connection weight value.
     */
    public void createConnection(Neuron fromNeuron, Neuron toNeuron,
            double weightVal) {
        /* The validation is done by Connection */
        Connection connection = new Connection(fromNeuron, toNeuron, weightVal);
        toNeuron.addInputConnection(connection);
    }

    // @Override
    // public String toString() {
    // StringBuilder builder = new StringBuilder();
    // builder.append(this.getClass().getName());
    // builder.append(" [");
    // if (type != null) {
    // builder.append("type=");
    // builder.append(type);
    // builder.append(", ");
    // }
    // if (layers != null && !layers.isEmpty()) {
    // builder.append("layers=");
    // builder.append(layers);
    // builder.append(", ");
    // }
    // if (inputNeurons != null) {
    // builder.append("inputNeurons=");
    // builder.append(inputNeurons);
    // builder.append(", ");
    // }
    // if (outputNeurons != null) {
    // builder.append("outputNeurons=");
    // builder.append(outputNeurons);
    // builder.append(", ");
    // }
    // if (learningRule != null) {
    // builder.append("learningRule=");
    // builder.append(learningRule);
    // builder.append(", ");
    // }
    // if (plugins != null) {
    // builder.append("plugins=");
    // builder.append(plugins);
    // builder.append(", ");
    // }
    // if (label != null) {
    // builder.append("label=");
    // builder.append(label);
    // }
    // builder.append("]");
    // return builder.toString();
    // }

    /**
     * Saves neural network into the specified file.
     * 
     * @param filePath
     *            - the file path to save network into.
     */
    public void save(String filePath) {
        ObjectOutputStream out = null;
        try {
            File file = new File(filePath);
            boolean isDeleted = false;
            if (file.exists()) {
                isDeleted = file.delete();
                logger.finest(filePath + " is deleted? " + isDeleted);
            }

            out = new ObjectOutputStream(new BufferedOutputStream(
                    new FileOutputStream(file)));
            out.writeObject(this);
            out.flush();
        } catch (IOException ioe) {
            logger.severe(ANNUtils.stackToString(ioe));
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [type=");
        builder.append(type);
        builder.append(", label=");
        builder.append(label);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Loads neural network from the specified file.
     * 
     * @param filePath
     *            - a file path to load network from.
     * @return loaded neural network as NeuralNetwork object.
     */
    public static NeuralNetwork load(String filePath) {
        ObjectInputStream oistream = null;

        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException(FILE_NOT_FOUND.to() + filePath);
            }

            oistream = new ObjectInputStream(new BufferedInputStream(
                    new FileInputStream(filePath)));
            NeuralNetwork nnet = (NeuralNetwork) oistream.readObject();

            return nnet;

        } catch (IOException ioe) {
            logger.severe(ANNUtils.stackToString(ioe));
        } catch (ClassNotFoundException cnfe) {
            logger.severe(ANNUtils.stackToString(cnfe));
        } finally {
            if (oistream != null) {
                try {
                    oistream.close();
                } catch (IOException ioe) {
                }
            }
        }

        return null;
    }

    /**
     * Loads neural network from the specified InputStream.
     * 
     * @param inputStream
     *            input stream to load network from
     * @return loaded neural network as NeuralNetwork object
     */
    public static NeuralNetwork load(InputStream inputStream) {
        ObjectInputStream oistream = null;

        try {
            oistream = new ObjectInputStream(new BufferedInputStream(
                    inputStream));
            NeuralNetwork nnet = (NeuralNetwork) oistream.readObject();

            return nnet;

        } catch (IOException ioe) {
            logger.severe(ANNUtils.stackToString(ioe));
        } catch (ClassNotFoundException cnfe) {
            logger.severe(ANNUtils.stackToString(cnfe));
        } finally {
            if (oistream != null) {
                try {
                    oistream.close();
                } catch (IOException ioe) {
                }
            }
        }

        return null;
    }

    /**
     * Adds plugin to neural network.
     * 
     * @param plugin
     *            - a neural network plugin to be added.
     */
    public void addPlugin(PluginBase plugin) {
        plugin.setParentNetwork(this);
        this.plugins.put(plugin.getClass(), plugin);
    }

    /**
     * Returns the requested plugin.
     * 
     * @param pluginClass
     *            - a class of the plugin to be gotten.
     * @return instance of specified plugin class.
     */
    public PluginBase getPlugin(Class<?> pluginClass) {
        return this.plugins.get(pluginClass);
    }

    /**
     * Removes the plugin with specified name.
     * 
     * @param pluginClass
     *            - a class of the plugin to remove.
     */
    public void removePlugin(Class<?> pluginClass) {
        this.plugins.remove(pluginClass);
    }

    /**
     * Gets network label.
     * 
     * @return network label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets a network label.
     * 
     * @param label
     *            - the network label to set.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    private void logWarnMessage(String message) {
        if (System.getProperty(LOG_LEVEL_PROPERTY.to()) != null
                && Level.parse(System.getProperty(LOG_LEVEL_PROPERTY.to())) == Level.WARNING) {
            logger.warning(message);
        }
    }

}
