/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_INVALID_TRANSFER_FUNCTION_PROPS;
import static com.ocrix.neuroph.common.NetProperty.LEFT_HIGH;
import static com.ocrix.neuroph.common.NetProperty.LEFT_LOW;
import static com.ocrix.neuroph.common.NetProperty.RIGHT_HIGH;
import static com.ocrix.neuroph.common.NetProperty.RIGHT_LOW;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ocrix.neuroph.util.Properties;

/**
 * Fuzzy trapezoid neuron transfer function.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Trapezoid extends TransferFunction implements Serializable {
    /* Local logger. */
    private static final Logger log = Logger.getLogger(Trapezoid.class
            .getName());

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /* these are the points of trapezoid function */
    double leftLow, leftHigh, rightLow, rightHigh;

    /**
     * Creates an instance of Trapezoid transfer function. Defaults are:
     * <ul>
     * <li>left low = <code>0d</code></li>
     * <li>left high = <code>1d</code></li>
     * <li>right low = <code>2d</code></li>
     * <li>right high = <code>3d</code></li>
     * </ul>
     */
    public Trapezoid() {
        this.leftLow = 0d;
        this.leftHigh = 1d;
        this.rightLow = 2d;
        this.rightHigh = 3d;
    }

    /**
     * Creates an instance of Trapezoid transfer function with the specified
     * setting.
     */
    public Trapezoid(double leftLow, double leftHigh, double rightLow,
            double rightHigh) {
        this.leftLow = leftLow;
        this.leftHigh = leftHigh;
        this.rightLow = rightLow;
        this.rightHigh = rightHigh;
    }

    /**
     * Creates an instance of Trapezoid transfer function with the specified
     * properties.
     */
    public Trapezoid(Properties properties) {
        try {
            this.leftLow = Double.parseDouble(properties.getProperty(
                    LEFT_LOW.to()).toString());
            this.leftHigh = Double.parseDouble(properties.getProperty(
                    LEFT_HIGH.to()).toString());
            this.rightLow = Double.parseDouble(properties.getProperty(
                    RIGHT_LOW.to()).toString());
            this.rightHigh = Double.parseDouble(properties.getProperty(
                    RIGHT_HIGH.to()).toString());

        } catch (NullPointerException e) {
            /* if properties are not set just leave default values */
            if (log.getLevel() == Level.ALL)
                log.severe(Properties.class.getName() + " is " + e.getMessage());

        } catch (NumberFormatException e) {
            log.severe(ERROR_INVALID_TRANSFER_FUNCTION_PROPS.to());
        }
    }

    @Override
    public double getOutput(double net) {
        if ((net >= leftHigh) && (net <= rightHigh)) {
            return 1d;
        } else if ((net > leftLow) && (net < leftHigh)) {
            return (net - leftLow) / (leftHigh - leftLow);
        } else if ((net > rightHigh) && (net < rightLow)) {
            return (rightLow - net) / (rightLow - rightHigh);
        }

        return 0d;
    }

    /**
     * Sets left low point of trapezoid function
     * 
     * @param leftLow
     *            - the left low point of trapezoid function
     */
    public void setLeftLow(double leftLow) {
        this.leftLow = leftLow;
    }

    /**
     * Sets left high point of trapezoid function
     * 
     * @param leftHigh
     *            - the left high point of trapezoid function
     */
    public void setLeftHigh(double leftHigh) {
        this.leftHigh = leftHigh;
    }

    /**
     * Sets right low point of trapezoid function.
     * 
     * @param rightLow
     *            right low point of trapezoid function
     */
    public void setRightLow(double rightLow) {
        this.rightLow = rightLow;
    }

    /**
     * Sets right high point of trapezoid function.
     * 
     * @param rightHigh
     *            - the right high point of trapezoid function.
     */
    public void setRightHigh(double rightHigh) {
        this.rightHigh = rightHigh;
    }

    /**
     * Returns left low point of trapezoid function.
     * 
     * @return left low point of trapezoid function
     */
    public double getLeftLow() {
        return leftLow;
    }

    /**
     * Returns left high point of trapezoid function.
     * 
     * @return left high point of trapezoid function.
     */
    public double getLeftHigh() {
        return leftHigh;
    }

    /**
     * Returns right low point of trapezoid function.
     * 
     * @return right low point of trapezoid function.
     */
    public double getRightLow() {
        return rightLow;
    }

    /**
     * Returns right high point of trapezoid function.
     * 
     * @return right high point of trapezoid function.
     */
    public double getRightHigh() {
        return rightHigh;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [leftLow=");
        builder.append(leftLow);
        builder.append(", leftHigh=");
        builder.append(leftHigh);
        builder.append(", rightLow=");
        builder.append(rightLow);
        builder.append(", rightHigh=");
        builder.append(rightHigh);
        builder.append("]");
        return builder.toString();
    }
}
