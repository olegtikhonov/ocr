/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_INVALID_TRANSFER_FUNCTION_PROPS;
import static com.ocrix.neuroph.common.NetProperty.SIGMA;

import java.io.Serializable;
import java.util.logging.Logger;

import com.ocrix.neuroph.util.Properties;

/**
 * <pre>
 * Defines a Gaussian neuron transfer function.
 * f(x) =  e -(x^2) / (2 * sigma ^ 2)
 * </pre>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Gaussian extends TransferFunction implements Serializable {
    /* Local logger */
    private static final Logger log = Logger
    .getLogger(Gaussian.class.getName());
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 1L;

    /* The sigma parametetar of the gaussian function */
    private double sigma = 0.5d;

    /**
     * Creates an instance of Gaussian neuron transfer.
     */
    public Gaussian() {
    }

    /**
     * Creates an instance of Gaussian neuron transfer function with the
     * specified properties.
     * 
     * @param properties
     *            - the properties of the Gaussian function
     */
    public Gaussian(Properties properties) {
        try {
            this.sigma = Double.parseDouble(properties.getProperty(SIGMA.to()).toString());
        } catch (NullPointerException e) {
            //the defaults will be set. nothing to deal with.
        } catch (NumberFormatException e) {
            log.warning(ERROR_INVALID_TRANSFER_FUNCTION_PROPS.to());
        }
    }

    @Override
    public double getOutput(double net) {
        return Math.exp(-0.5d * Math.pow((net / this.sigma), 2d));
    }

    @Override
    public double getDerivative(double net) {
        // TODO: check if this is correct
        double out = getOutput(net);
        double derivative = out * (-net / (sigma * sigma));
        return derivative;
    }

    /**
     * Returns the sigma parameter of this function.
     * 
     * @return sigma parametar of this function
     */
    public double getSigma() {
        return this.sigma;
    }

    /**
     * Sets the sigma parameter for this function.
     * 
     * @param sigma
     *            - value for the slope parameter
     */
    public void setSigma(double sigma) {
        this.sigma = sigma;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [sigma=");
        builder.append(sigma);
        builder.append("]");
        return builder.toString();
    }
}
