/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import java.io.Serializable;
import java.util.List;


import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.util.VectorParser;

/**
 * Represents training element for supervised learning algorithms. Each
 * supervised training element contains network input and desired network
 * output.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class SupervisedTrainingElement extends TrainingElement implements
Serializable {

    /*
     * The class finger-print that is set to indicate serialization
     * compatibility with a previous version of the class
     */
    private static final long serialVersionUID = 1L;

    /* Desired output for this training element */
    private double[] desiredOutput;

    // ------------------------------------

    /**
     * Creates new training element with specified input and desired output
     * vectors.
     * 
     * @param input
     *            - the input vector.
     * @param desiredOutput
     *            - the desired output vector.
     */
    // public SupervisedTrainingElement(ArrayList<Double> input,
    // ArrayList<Double> desiredOutput) {
    public SupervisedTrainingElement(List<Double> input,
            List<Double> desiredOutput) {
        super(input);
        Validator.validateList(desiredOutput);
        this.desiredOutput = VectorParser.toDoubleArray(desiredOutput);
    }

    /**
     * Creates new training element with specified input and desired output
     * vectors specified as strings.
     * 
     * @param input
     *            - the input vector as space separated string.
     * @param desiredOutput
     *            - the desired output vector as space separated string.
     */
    public SupervisedTrainingElement(String input, String desiredOutput) {
        super(input);
        Validator.validateString(desiredOutput);
        this.desiredOutput = VectorParser.parseDoubleArray(desiredOutput);
    }

    /**
     * Creates new training element with specified input and desired output
     * vectors.
     * 
     * @param input
     *            - the input array.
     * @param desiredOutput
     *            - the desired output array.
     */
    public SupervisedTrainingElement(double[] input, double[] desiredOutput) {
        super(input);
        this.desiredOutput = desiredOutput;
    }

    /**
     * Returns desired output for this training element.
     * 
     * @return desired/ideal output vector.
     */
    public double[] getDesiredOutput() {
        return this.desiredOutput;
    }

    /**
     * Sets desired output vector for this training element.
     * 
     * @param desiredOutput
     *            - the desired output vector.
     */
    public void setDesiredOutput(double[] desiredOutput) {
        this.desiredOutput = desiredOutput;
    }

    /**
     * This method will return the idea, or expected, output (same as
     * getDesiredOutput). Method added for Encog-Engine compatibility.
     * 
     * @return The ideal, or expected output.
     */
    @Override
    public double[] getIdealArray() {
        return getDesiredOutput();
    }

    /**
     * This method sets the ideal, or expected output data (same as
     * setDesiredOutput). Method added for Encog-Engine compatibility.
     * 
     * @param data
     *            - the ideal data.
     */
    @Override
    public void setIdealArray(double[] data) {
        this.desiredOutput = data;
    }

    /**
     * Method added for Encog-Engine compatibility.
     * 
     * @return <code>true</code> if this is a supervised training element. It
     *         will always return true, as this class is always used for
     *         supervised training.
     */
    @Override
    public boolean isSupervised() {
        return true;
    }

}