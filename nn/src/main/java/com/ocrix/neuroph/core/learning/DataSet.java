/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.ocrix.neuroph.core.learning;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import static com.ocrix.neuroph.common.CommonConsts.DEFAULT_LIST_SIZE;
import org.apache.commons.lang.RandomStringUtils;
import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.exceptions.NeurophException;
import com.ocrix.neuroph.core.exceptions.VectorSizeMismatchException;
import com.ocrix.neuroph.util.norm.MaxNormalizer;
import com.ocrix.neuroph.util.norm.Normalizer;

/**
 * A set of training elements for training neural network.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com> extends AbstractCollection or
 *         Implements Collection
 *         http://openforecast.sourceforge.net/docs/net/sourceforge
 *         /openforecast/DataSet.html
 */
public class DataSet implements Serializable {

    private static final Logger LOG = Logger.getLogger(DataSet.class.getName());

    /*
     * The class finger print that is set to indicate serialization
     * compatibility with a previous version of the class
     */
    private static final long serialVersionUID = 2L;
    /* Collection of data rows */
    private final List<DataSetRow> rows;
    private int inputSize = 0;
    private int outputSize = 0;
    private String[] columnNames;
    private boolean isSupervised = false;
    /* Label for this training set */
    private String label;
    /* Full file path including file name */
    private transient String filePath;

    /**
     * Creates an instance of new empty training set.
     * 
     * @param inputSize
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public DataSet(int inputSize) {
        this.rows = new ArrayList();
        this.inputSize = inputSize;
        this.isSupervised = false;
        this.label = RandomStringUtils.randomAlphabetic(DEFAULT_LIST_SIZE
                .value());
    }

    /**
     * Creates an instance of new empty training set.
     * 
     * @param inputSize
     *            Length of the input vector.
     * @param outputSize
     *            Length of the output vector.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public DataSet(int inputSize, int outputSize) {
        this.rows = new ArrayList();
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        this.isSupervised = true;
        this.label = RandomStringUtils.randomAlphabetic(DEFAULT_LIST_SIZE
                .value());
    }

    /**
     * Adds new row element to this data set.
     * 
     * @param row
     *            data set row to add.
     */
    public void addRow(DataSetRow row) throws VectorSizeMismatchException {

        if (row == null) {
            LOG.severe("Training dta row cannot be null!");
            throw new NeurophException("Training dta row cannot be null!");
        }

        // checks input vector size if it is predefined
        if ((this.inputSize != 0) && (row.getInput().length != this.inputSize)) {
            LOG.severe("Input vector size does not match data set input size!");
            throw new VectorSizeMismatchException(
                    "Input vector size does not match data set input size!");
        }

        if ((this.outputSize != 0)
                && (row.getDesiredOutput().length != this.outputSize)) {

            LOG.severe("Output vector size does not match data set output size!");
            throw new VectorSizeMismatchException(
                    "Output vector size does not match data set output size!");
        }

        // if everything went ok adds training element
        this.inputSize = row.getInput().length;
        //sometimes output is null
        if(row.getDesiredOutput() != null){
            this.outputSize = row.getDesiredOutput().length;
        }else {
            this.outputSize = this.inputSize;
        }

        this.rows.add(row);
    }

    /**
     * Adds a {@link DataSetRow}.
     * 
     * @param input
     *            to be added.
     */
    public void addRow(double[] input) {
        Validator.validateDouble(input);
        
        this.inputSize = input.length;
        this.addRow(new DataSetRow(input));
    }

    /**
     * Adds a {@link DataSetRow}.
     * 
     * @param input
     * @param output
     */
    public void addRow(double[] input, double[] output) {
        Validator.validateDouble(input);
        Validator.validateDouble(output);
        
        this.inputSize = input.length;
        this.outputSize = output.length;

        this.addRow(new DataSetRow(input, output));
    }

    /**
     * Removes training element at specified index position.
     * 
     * @param idx
     *            position of element to remove.
     */
    public void removeRowAt(int idx) {
        this.rows.remove(idx);
    }

    /**
     * Returns Iterator for iterating training elements collection.
     * 
     * @return Iterator for iterating training elements collection.
     */
    @SuppressWarnings("rawtypes")
    public Iterator iterator() {
        return this.rows.iterator();
    }

    /**
     * Returns elements of this training set.
     * 
     * @return training elements.
     */
    public List<DataSetRow> getRows() {
        return this.rows;
    }

    /**
     * Returns training element at specified index position.
     * 
     * @param idx
     *            index position of training element to return.
     * 
     * @return training element at specified index position.
     */
    public DataSetRow getRowAt(int idx) {
        return this.rows.get(idx);
    }

    /**
     * Removes all elements from training set.
     */
    public void clear() {
        this.rows.clear();
    }

    /**
     * Returns true if training set is empty, false otherwise.
     * 
     * @return true if training set is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.rows.isEmpty();
    }

    /**
     * Returns true if data set is supervised, false otherwise.
     * 
     * @return an answer if a data set used for supervised learning or not.
     */
    public boolean isSupervised() {
        return this.isSupervised;
    }

    /**
     * Returns number of training elements in this training set set.
     * 
     * @return number of training elements in this training set set.
     */
    public int size() {
        return this.rows.size();
    }

    /**
     * Returns label for this training set.
     * 
     * @return label for this training set.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label for this training set.
     * 
     * @param label
     *            label for this training set.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }

    /**
     * Sets full file path for this training set.
     * 
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Returns full file path for this training set
     * 
     * @return full file path for this training set
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Returns label of this training set
     * 
     * @return label of this training set
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName());
        sb.append(" [lable=");
        sb.append(this.label);
        sb.append(", rows=");
        sb.append(this.rows);
        sb.append(", column names=");
        sb.append(this.columnNames);
        sb.append("]");
        
        return sb.toString();
    }

    /**
     * Saves this training set to the specified file
     * 
     * @param filePath
     */
    public void save(String filePath) {
        this.filePath = filePath;
        this.save();
    }

    /**
     * Saves this training set to file specified in its filePath field
     */
    public void save() {
        ObjectOutputStream out = null;

        try {
            File file = new File(this.filePath);
            out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(this);
            out.flush();

        } catch (Exception e) {
            LOG.severe(ANNUtils.stackToString(e));
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                    LOG.severe(ANNUtils.stackToString(ioe));
                }
            }
        }
    }

    /**
     * Saves rows in file as Strings. Default delimiter is ' '.
     * 
     * @param filePath
     *            where to save.
     * @param delimiter
     *            between "nodes".
     */
    public void saveAsTxt(String filePath, String delimiter) {
        
        int errorOccured = 0;
        
        if ((delimiter == null) || delimiter.equals("")) {
            delimiter = " ";
        }

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(new File(filePath)));

            for (DataSetRow element : this.rows) {
                double[] input = element.getInput();
                for (int i = 0; i < input.length; i++) {
                    out.print(input[i] + delimiter);
                }

                if (element instanceof DataSetRow) {
                    if ((element).getDesiredOutput() != null) {

                        double[] output = (element).getDesiredOutput();
                        for (int j = 0; j < output.length; j++) {
                            if(j != (output.length - 1)){
                                out.print(output[j] + delimiter);
                            }else {
                                out.print(output[j]);
                            }
                        }
                    }else {
                        if(errorOccured++ == 0) {
                            LOG.warning("Desired output is null, please set it up");
                        }
                    }
                }
                out.println();
            }

            out.flush();

        } catch (Exception e) {
            LOG.severe(ANNUtils.stackToString(e));
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * Loads training set from the specified file.
     * 
     * @param filePath
     *            training set file.
     * 
     * @return loaded training set.
     */
    public static DataSet load(String filePath) {
        ObjectInputStream oistream = null;

        try {
            File file = new File(filePath);
            if (!file.exists()) {
                LOG.severe("Cannot find file: " + filePath);
                throw new FileNotFoundException("Cannot find file: " + filePath);
            }

            oistream = new ObjectInputStream(new FileInputStream(filePath));
            DataSet tSet = (DataSet) oistream.readObject();

            return tSet;

        } catch (IOException ioe) {
            LOG.severe(ANNUtils.stackToString(ioe));
        } catch (ClassNotFoundException cnfe) {
            LOG.severe(ANNUtils.stackToString(cnfe));
        } finally {
            if (oistream != null) {
                try {
                    oistream.close();
                } catch (IOException ioe) {
                    LOG.severe(ANNUtils.stackToString(ioe));
                }
            }
        }

        return null;
    }

    /**
     * Creates a data set from file.
     * 
     * @param filePath
     *            a location of the file.
     * @param inputsCount
     *            an amount of input neurons.
     * @param outputsCount
     *            an amount if output neurons.
     * @param delimiter
     *            between values.
     * 
     * @return created {@link DataSet}.
     */
    public static DataSet createFromFile(String filePath, int inputsCount,
            int outputsCount, String delimiter) {
        FileReader fileReader = null;

        try {
            DataSet trainingSet = new DataSet(inputsCount, outputsCount);
            fileReader = new FileReader(new File(filePath));
            @SuppressWarnings("resource")
            BufferedReader reader = new BufferedReader(fileReader);
            
            String line = "";

            while ((line = reader.readLine()) != null) {
                double[] inputs = new double[inputsCount];
                double[] outputs = new double[outputsCount];
                String[] values = line.split(delimiter);

                if (values[0].equals("") || !isNumeric(values[0])) {
                    continue; // skip if line was empty
                }
                for (int i = 0; i < inputsCount; i++) {
                    inputs[i] = Double.parseDouble(values[i]);
                }

                for (int i = 0; i < outputsCount; i++) {
                    outputs[i] = Double.parseDouble(values[inputsCount + i]);
                }

                if (outputsCount > 0) {
                    trainingSet.addRow(new DataSetRow(inputs, outputs));
                } else {
                    trainingSet.addRow(new DataSetRow(inputs));
                }
            }

            return trainingSet;

        } catch (FileNotFoundException ex) {
            LOG.severe(ANNUtils.stackToString(ex));
        } catch (IOException ex) {
            LOG.severe(ANNUtils.stackToString(ex));
        } catch (NumberFormatException ex) {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException ex1) {
                    LOG.severe(ANNUtils.stackToString(ex1));
                }
            }
            throw ex;
        }

        return null;
    }
    
    private static boolean isNumeric(String str)
    {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }
    
    

    /**
     * Normalizes a data set using {@link MaxNormalizer}.
     */
    public void normalize() {
        this.normalize(new MaxNormalizer());
    }

    /**
     * Normalizes a data set using provided {@link Normalizer}.
     * 
     * @param normalizer
     *            to be used for normalizing data set.
     */
    public void normalize(Normalizer normalizer) {
        normalizer.normalize(this);
    }

    /**
     * Creates training and tests subsets.
     * 
     * @param trainSetPercent
     * @param testSetPercent
     * 
     * @return trained data set.
     */
    public DataSet[] createTrainingAndTestSubsets(int trainSetPercent,
            int testSetPercent) {
        DataSet[] trainAndTestSet = new DataSet[2];

        ArrayList<Integer> randoms = new ArrayList<Integer>();

        for (int i = 0; i < this.size(); i++) {
            randoms.add(i);
        }

        Collections.shuffle(rows);

        // creates training set
        trainAndTestSet[0] = new DataSet(inputSize, outputSize);
        int trainingElementsCount = this.size() * trainSetPercent / 100;

        for (int i = 0; i < trainingElementsCount; i++) {
            int idx = randoms.get(i);
            trainAndTestSet[0].addRow(this.rows.get(idx));
        }

        // creates test set
        trainAndTestSet[1] = new DataSet(inputSize, outputSize);
        int testElementsCount = this.size() - trainingElementsCount;

        for (int i = 0; i < testElementsCount; i++) {
            int idx = randoms.get(trainingElementsCount + i);
            trainAndTestSet[1].addRow(this.rows.get(idx));
        }

        return trainAndTestSet;
    }

    /**
     * Returns output vector size of training elements in this training set.
     */
    public int getOutputSize() {
        return this.outputSize;
    }

    /**
     * Returns input vector size of training elements in this training set This
     * method is implementation of EngineIndexableSet interface, and it is added
     * to provide compatibility with Encog data sets and FlatNetwork
     */
    public int getInputSize() {
        return this.inputSize;
    }

    /**
     * Shuffles a {@link Collections}.
     */
    public void shuffle() {
        Collections.shuffle(rows);
    }
}