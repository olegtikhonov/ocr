/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.transfer;

import static com.ocrix.neuroph.common.CommonMessages.ERROR_INVALID_TRANSFER_FUNCTION_PROPS;
import static com.ocrix.neuroph.common.NetProperty.SLOPE;

import java.io.Serializable;
import java.util.logging.Logger;

import com.ocrix.neuroph.util.Properties;

/**
 * <pre>
 * Defines a Tanh neuron transfer function.
 * 
 * output = ( e^(2*input)-1) / ( e^(2*input)+1 )
 * </pre>
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class Tanh extends TransferFunction implements Serializable {

    /* Logger */
    private static final Logger log = Logger.getLogger(Tanh.class.getName());
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 2L;

    /* The slope parameter of the Tanh function */
    private double slope = 2d;

    /**
     * Creates an instance of Tanh neuron transfer function with default
     * <code>slope=1</code>.
     */
    public Tanh() {
    }

    /**
     * Creates an instance of Tanh neuron transfer function with specified value
     * for slope parametar.
     * 
     * @param slope
     *            the slope parametar for the Tanh function.
     */
    public Tanh(double slope) {
        this.slope = slope;
    }

    /**
     * Creates an instance of Tanh neuron transfer function with the specified
     * properties.
     * 
     * @param properties
     *            properties of the Tanh function.
     */
    public Tanh(Properties properties) {
        try {
            this.slope = Double.parseDouble(properties.get(SLOPE.to()).toString());
        } catch (NullPointerException e) {
            // TODO: handle exception
        } catch (NumberFormatException e) {
            log.warning(ERROR_INVALID_TRANSFER_FUNCTION_PROPS.to());
        }
    }

    @Override
    final public double getOutput(double net) {
        /* conditional logic helps to avoid NaN */
        if (net > 100) {
            return 1.0;
        } else if (net < -100) {
            return -1.0;
        }

        double E_x = Math.exp(this.slope * net);
        return (E_x - 1d) / (E_x + 1d);
    }

    @Override
    final public double getDerivative(double net) {
        double out = getOutput(net);
        return (1d - out * out);
    }

    /**
     * Returns the slope parameter of this function.
     * 
     * @return slope parameter of this function.
     */
    public double getSlope() {
        return this.slope;
    }

    /**
     * Sets the slope parameter for this function.
     * 
     * @param slope
     *            value for the slope parameter.
     */
    public void setSlope(double slope) {
        this.slope = slope;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [slope=");
        builder.append(slope);
        builder.append("]");
        return builder.toString();
    }
}
