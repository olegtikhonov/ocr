/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core.learning;

import static com.ocrix.neuroph.common.CommonMessages.WARN_ANN_IS_NULL;

import java.io.Serializable;
import java.util.Iterator;
import java.util.logging.Logger;

import com.ocrix.neuroph.common.Validator;

/**
 * Base class for all unsupervised learning algorithms.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
abstract public class UnsupervisedLearning extends IterativeLearning implements
        Serializable {

    /*
     * The class finger-print that is set to indicate serialization
     * compatibility with a previous version of the class
     */
    private static final long serialVersionUID = 1L;

    /* Local logger */
    private static final Logger log = Logger
            .getLogger(UnsupervisedLearning.class.getName());

    // ------------------------------------

    /**
     * Creates new unsupervised learning rule.
     */
    public UnsupervisedLearning() {
        super();
    }

    /**
     * This method does one learning epoch for the unsupervised learning rules.
     * It iterates through the training set and trains network weights for each
     * element
     * 
     * @param trainingSet
     *            - training set for training network
     */
    @Override
    public void doLearningEpoch(
            @SuppressWarnings("rawtypes") TrainingSet trainingSet) {
        Validator.validateTraining(trainingSet);
        @SuppressWarnings("unchecked")
        Iterator<TrainingElement> iterator = trainingSet.iterator();
        while (iterator.hasNext() && !isStopped()) {
            TrainingElement trainingElement = iterator.next();
            learnPattern(trainingElement);
        }
    }

    /**
     * Trains network with the pattern from the specified training element.
     * 
     * @param trainingElement
     *            - the unsupervised training element which contains network
     *            input.
     */
    protected void learnPattern(TrainingElement trainingElement) {
        double[] input = trainingElement.getInput();
        /* Prevents NPE */
        if (this.neuralNetwork != null) {
            this.neuralNetwork.setInput(input);
            this.neuralNetwork.calculate();
            this.updateNetworkWeights();
        } else {
            log.severe(WARN_ANN_IS_NULL.to());
        }
    }

    /**
     * This method implements the weight adjustment
     */
    abstract protected void updateNetworkWeights();
}