/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ocrix.neuroph.core;

import static com.ocrix.neuroph.common.CommonConsts.DEFAULT_LIST_SIZE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;

import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.input.InputFunction;
import com.ocrix.neuroph.core.transfer.Step;
import com.ocrix.neuroph.core.transfer.TransferFunction;

/**
 * <pre>
 * Basic general neuron model according to McCulloch-Pitts neuron model.
 * Different neuron models can be created by using different input and transfer functions for instances of this class,
 * or by deriving from this class. The neuron is basic processing element of neural network.
 * This class implements the following behavior:
 * 
 * output = transferFunction( inputFunction(inputConnections) )
 * </pre>
 * 
 * @see InputFunction
 * @see TransferFunction
 * @author Zoran Sevarac <sevarac@gmail.com>
 */

public class Neuron implements Serializable {
    /**
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class.
     */
    private static final long serialVersionUID = 3L;

    /**
     * Parent layer for this neuron.
     */
    protected Layer parentLayer;

    /**
     * Collection of neuron's input connections (connections to this neuron).
     */
    protected List<Connection> inputConnections = new ArrayList<Connection>();

    /**
     * Collection of neuron's output connections (connections from this to other
     * neurons).
     */
    protected List<Connection> outConnections = new ArrayList<Connection>();

    /**
     * Total net input for this neuron. Represents total input for this neuron
     * received from input function.
     */
    protected transient double netInput = 0;

    /**
     * Neuron output
     */
    protected transient double output = 0;

    /**
     * Local error for this neuron
     */
    protected transient double error = 0;

    /**
     * Input function for this neuron
     */
    protected InputFunction inputFunction;

    /**
     * Transfer function for this neuron
     */
    protected TransferFunction transferFunction;

    /**
     * Neuron's label
     */
    private String label;

    // ----------------------------------------

    /**
     * Creates an instance of Neuron with the weighted sum, input function and
     * Step transfer function. This is the original <a
     * href="http://www.mind.ilstu.edu/curriculum/modOverview.php?modGUI=212"
     * >McCulloch-Pitts neuron model</a>.
     */
    public Neuron() {
        this.inputFunction = new InputFunction();
        this.transferFunction = new Step();
        /* Generates a default label */
        this.label = RandomStringUtils.randomAlphabetic(DEFAULT_LIST_SIZE
                .value());
    }

    /**
     * Creates an instance of Neuron with the specified input and transfer
     * functions.
     * 
     * @param inputFunction
     *            - an input function for this neuron.
     * @param transferFunction
     *            - a transfer function for this neuron.
     */
    public Neuron(InputFunction inputFunction, TransferFunction transferFunction) {
        this.inputFunction = inputFunction;
        this.transferFunction = transferFunction;
        /* Generates a default label */
        this.label = RandomStringUtils.randomAlphabetic(DEFAULT_LIST_SIZE
                .value());
    }

    /**
     * Calculates neuron's output.
     */
    public void calculate() {
        // why do we need this? for input neurons!!!
        if (this.hasInputConnections()) {
            this.netInput = this.inputFunction.getOutput(this.inputConnections);
        }
        this.output = this.transferFunction.getOutput(this.netInput);
    }

    /**
     * Sets input and output activation levels to zero.
     */
    public void reset() {
        this.setInput(0d);
        this.setOutput(0d);
    }

    /**
     * Sets neuron's input.
     * 
     * @param input
     *            - an input value to set.
     */
    public void setInput(double input) {
        this.netInput = input;
    }

    /**
     * Returns total net input.
     * 
     * @return total net input.
     */
    public double getNetInput() {
        return this.netInput;
    }

    /**
     * Returns neuron's output.
     * 
     * @return neuron output.
     */
    public double getOutput() {
        return this.output;
    }

    /**
     * Returns true if there are input connections for this neuron, false
     * otherwise.
     * 
     * @return <code>true</code> if there is input connection,
     *         <code>false</code> otherwise.
     */
    public boolean hasInputConnections() {
        return (!this.inputConnections.isEmpty());
    }

    /**
     * Returns Iterator interface for accessing input connections.
     * 
     * @return iterator interface for accessing input connections.
     */
    public Iterator<Connection> getInputsIterator() {
        return this.inputConnections.iterator();
    }

    /**
     * Adds the specified input connection.
     * 
     * @param connection
     *            - the input connection to add.
     */
    public void addInputConnection(Connection connection) {
        this.inputConnections.add(connection);
        Neuron fromNeuron = connection.getFromNeuron();
        fromNeuron.addOutputConnection(connection);
    }

    /**
     * Adds input connection from specified neuron.
     * 
     * @param fromNeuron
     *            - the neuron to connect from.
     */
    public void addInputConnection(Neuron fromNeuron) {
        Connection connection = new Connection(fromNeuron, this);
        this.addInputConnection(connection);
    }

    /**
     * Adds input connection with the given weight, from given neuron.
     * 
     * @param fromNeuron
     *            - the neuron to connect from.
     * @param weightVal
     *            - the connection weight value.
     * 
     */
    public void addInputConnection(Neuron fromNeuron, double weightVal) {
        Connection connection = new Connection(fromNeuron, this, weightVal);
        this.addInputConnection(connection);
    }

    /**
     * Adds the specified output connection.
     * 
     * @param connection
     *            - the output connection to add.
     */
    public void addOutputConnection(Connection connection) {
        this.outConnections.add(connection);
    }

    /**
     * Returns input connections for this neuron as Vector collection.
     * 
     * @return input connections of this neuron.
     */
    public List<Connection> getInputConnections() {
        return inputConnections;
    }

    /**
     * Returns output connections from this neuron.
     * 
     * @return output connections from this neuron.
     */
    public List<Connection> getOutConnections() {
        return outConnections;
    }

    /**
     * Removes input connection which is connected to specified neuron.
     * 
     * @param fromNeuron
     *            - the neuron which is connected as input.
     */
    public void removeInputConnectionFrom(Neuron fromNeuron) {
        for (Connection connection : this.inputConnections) {
            if (connection.getFromNeuron() == fromNeuron) {
                this.inputConnections.remove(connection);
                return;
            }
        }
    }

    /**
     * Gets input connection from the specified neuron neuron connected to this
     * neuron as input.
     * 
     * @param fromNeuron
     * 
     */
    public Connection getConnectionFrom(Neuron fromNeuron) {
        Connection toBeReturned = null;
        for (Connection connection : this.inputConnections) {
            if (connection.getToNeuron() == fromNeuron){
                //            if (connection.getFromNeuron() == fromNeuron){
                toBeReturned = connection;
                break;
            }
        }
        return toBeReturned;
    }

    /**
     * Sets input function.
     * 
     * @param inputFunction
     *            - the input function for this neuron.
     */
    public void setInputFunction(InputFunction inputFunction) {
        this.inputFunction = inputFunction;
    }

    /**
     * Sets transfer function.
     * 
     * @param transferFunction
     *            - the transfer function for this neuron.
     */
    public void setTransferFunction(TransferFunction transferFunction) {
        Validator.validateTransferFunction(transferFunction);
        this.transferFunction = transferFunction;
    }

    /**
     * Returns an input function.
     * 
     * @return input function.
     */
    public InputFunction getInputFunction() {
        return this.inputFunction;
    }

    /**
     * Returns transfer function.
     * 
     * @return transfer function.
     */
    public TransferFunction getTransferFunction() {
        return this.transferFunction;
    }

    /**
     * Sets reference to parent layer for this neuron (layer in which the neuron
     * is located).
     * 
     * @param parent
     *            reference on layer in which the cell is located.
     */
    public void setParentLayer(Layer parent) {
        this.parentLayer = parent;
    }

    /**
     * Returns reference to parent layer for this neuron.
     * 
     * @return parent layer for this neuron.
     */
    public Layer getParentLayer() {
        return this.parentLayer;
    }

    /**
     * Returns weights vector of input connections.
     * 
     * @return weights vector of input connections.
     */
    public List<Weight> getWeightsVector() {
        List<Weight> weights = new ArrayList<Weight>();
        for (Connection connection : this.inputConnections) {
            Weight weight = connection.getWeight();
            weights.add(weight);
        }
        return weights;
    }

    /**
     * Returns an error for this neuron. This is used by supervised learning
     * rules.
     * 
     * @return error for this neuron which is set by learning rule.
     */
    public double getError() {
        return error;
    }

    /**
     * Sets an error for this neuron. This is used by supervised learning rules.
     * 
     * @param error
     *            - the neuron error.
     */
    public void setError(double error) {
        this.error = error;
    }

    /**
     * Sets this neuron output.
     * 
     * @param output
     *            - the value to set.
     */
    public void setOutput(double output) {
        this.output = output;
    }

    /**
     * Randomizes all input weights.
     */
    public void randomizeInputWeights() {
        for (Connection connection : this.inputConnections) {
            connection.getWeight().randomize();
        }
    }

    /**
     * Randomizes all input weights within specified value range.
     */
    public void randomizeInputWeights(double minWeight, double maxWeight) {
        for (Connection connection : this.inputConnections) {
            connection.getWeight().randomize(minWeight, maxWeight);
        }
    }

    /**
     * Initializes weights for all input connections to specified value.
     * 
     * @param value
     *            - the weight value.
     */
    public void initializeWeights(double value) {
        for (Connection connection : this.inputConnections) {
            connection.getWeight().setValue(value);
        }
    }

    /**
     * Initializes weights for all input connections to using random number
     * generator.
     * 
     * @param generator
     *            - the random number generator.
     */
    public void initializeWeights(Random generator) {
        for (Connection connection : this.inputConnections) {
            connection.getWeight().setValue(generator.nextDouble());
        }
    }

    /**
     * Initializes weights for all input connections with random value within
     * specified interval.
     * 
     * @param min
     * @param max
     */
    public void initializeWeights(double min, double max) {
        for (Connection connection : this.inputConnections) {
            connection.getWeight().randomize(min, max);
        }
    }

    /**
     * Returns label for this neuron.
     * 
     * @return label for this neuron.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the label for this neuron.
     * 
     * @param label
     *            - the neuron label to set.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (getTransferFunction() != null) {
            builder.append("transfer function=");
            builder.append(getTransferFunction());
            builder.append(", ");
        }
        if (getLabel() != null) {
            builder.append("label=");
            builder.append(getLabel());
        }

        builder.append(", input=" + getNetInput());

        builder.append("]");
        return builder.toString();
    }
}
