/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.ocrix.neuroph.core.learning;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import com.ocrix.neuroph.util.VectorParser;

/**
 * Represents training element for supervised learning algorithms. Each
 * supervised training element contains network input and desired network
 * output.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class DataSetRow implements Serializable {
    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class
     */
    private static final long serialVersionUID = 1L;
    /* Input vector for this training element */
    protected double[] input;
    /* Desired output for this training element. */
    private double[] desiredOutput;
    /* Label for this training element */
    protected String label;

    /**
     * Creates new training element with specified input and desired output
     * vectors specified as strings.
     * 
     * @param input
     *            input vector as space separated string.
     * @param desiredOutput
     *            desired output vector as space separated string.
     */
    public DataSetRow(String input, String desiredOutput) {
        this.input = VectorParser.parseDoubleArray(input);
        this.desiredOutput = VectorParser.parseDoubleArray(desiredOutput);
    }

    /**
     * Creates new training element with specified input and desired output
     * vectors.
     * 
     * @param input
     *            input array.
     * @param desiredOutput
     *            desired output array.
     */
    public DataSetRow(double[] input, double[] desiredOutput) {
        this.input = input;
        this.desiredOutput = desiredOutput;
    }

    /**
     * Creates new training element with input array.
     * 
     * @param input
     *            input array.
     */
    public DataSetRow(double... input) {
        this.input = input;
    }

    /**
     * Creates new training element with specified input and desired output
     * vectors.
     * 
     * @param input
     *            input vector.
     * @param desiredOutput
     *            desired output vector.
     */
    public DataSetRow(ArrayList<Double> input, ArrayList<Double> desiredOutput) {
        this.input = VectorParser.toDoubleArray(input);
        this.desiredOutput = VectorParser.toDoubleArray(desiredOutput);
    }

    /**
     * Creates {@link DataSetRow} from {@link ArrayList}.
     * 
     * @param input
     *            from it to be created data set row.
     */
    public DataSetRow(ArrayList<Double> input) {
        this.input = VectorParser.toDoubleArray(input);
    }

    /**
     * Returns input vector.
     * 
     * @return input vector.
     */
    public double[] getInput() {
        return this.input;
    }

    /**
     * Sets input vector.
     * 
     * @param input
     *            input vector.
     */
    public void setInput(double[] input) {
        this.input = input;
    }

    /**
     * Gets the desired output.
     * 
     * @return
     */
    public double[] getDesiredOutput() {
        return desiredOutput;
    }

    public void setDesiredOutput(double[] desiredOutput) {
        this.desiredOutput = desiredOutput;
    }

    /**
     * Gets training element label.
     * 
     * @return training element label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets training element label.
     * 
     * @param label
     *            label for this training element.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Indicates if it's used by supervised learning.
     * 
     * @return <code>true</code> is used in supervised learning.
     */
    public boolean isSupervised() {
        return (desiredOutput == null ? false : true);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [input=");
        builder.append(Arrays.toString(input));
        builder.append(", desiredOutput=");
        builder.append(Arrays.toString(desiredOutput));
        builder.append(", label=");
        builder.append(label);
        builder.append("]");
        return builder.toString();
    }
    
    
}