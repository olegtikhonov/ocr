/*
 * Copyright 2010 Neuroph Project http://neuroph.sourceforge.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocrix.neuroph.core.learning;

import com.ocrix.neuroph.common.ANNUtils;
import com.ocrix.neuroph.common.Validator;
import com.ocrix.neuroph.core.exceptions.VectorSizeMismatchException;
import com.ocrix.neuroph.util.NNUtils;
import com.ocrix.neuroph.util.norm.MaxNormalizer;
import com.ocrix.neuroph.util.norm.Normalizer;
import org.apache.commons.lang.RandomStringUtils;
import org.encog.engine.data.EngineData;
import org.encog.engine.data.EngineIndexableSet;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import static com.ocrix.neuroph.common.CommonConsts.DEFAULT_LIST_SIZE;

/**
 * Defines a set of training elements for training neural network.
 * 
 * @author Zoran Sevarac <sevarac@gmail.com>
 */
public class TrainingSet<E extends TrainingElement> implements Serializable,
        EngineIndexableSet {

    /*
     * The class fingerprint that is set to indicate serialization compatibility
     * with a previous version of the class
     */
    private static final long serialVersionUID = 2L;
    /* Collection of training elements */
    private final List<E> elements;
    private int inputVectorSize = 0;
    private int outputVectorSize = 0;
    /* Label for this training set */
    private String label;
    /* Full file path including file name */
    private transient String filePath;
    /* Logger log */
    private static final Logger log = Logger.getLogger(TrainingSet.class
            .getName());

    // ------------------------------------------------

    /**
     * Creates an instance of new empty training set with randomly generated
     * label.
     */
    public TrainingSet() {
        this.elements = new ArrayList<E>();
        this.label = RandomStringUtils.randomAlphanumeric(DEFAULT_LIST_SIZE
                .value());
    }

    /**
     * Creates an instance of new empty training set with given label.
     * 
     * @param label
     *            training set label.
     */
    public TrainingSet(String label) {
        this.label = label;
        this.elements = new ArrayList<E>();
    }

    /**
     * Creates an instance of new empty training set.
     * 
     * @param inputVectorSize
     *            - the size of the input vector.
     */
    public TrainingSet(int inputVectorSize) {
        this.elements = new ArrayList<E>();
        this.inputVectorSize = inputVectorSize;
    }

    /**
     * Creates an instance of new empty training set.
     * 
     * @param inputVectorSize
     *            - the size of the input vector.
     * @param outputVectorSize
     *            - the size of the output vector.
     */
    public TrainingSet(int inputVectorSize, int outputVectorSize) {
        this.elements = new ArrayList<E>();
        this.inputVectorSize = inputVectorSize;
        this.outputVectorSize = outputVectorSize;
        /* If not set */
        this.filePath = ".";
    }

    /**
     * Adds new training element to this training set.
     * 
     * @param el
     *            - the training element to add.
     */
    public void addElement(E el) throws VectorSizeMismatchException {
        /* Checks input vector size if it is predefined */
        if ((this.inputVectorSize != 0)
                && (el.getInput().length != this.inputVectorSize)) {
            throw new VectorSizeMismatchException(
                    "Input vector size does not match training set!");
        }
        /* check output vector size if it is predefined */
        if (el instanceof SupervisedTrainingElement) {
            SupervisedTrainingElement sel = (SupervisedTrainingElement) el;
            if ((this.outputVectorSize != 0)
                    && (sel.getDesiredOutput().length != this.outputVectorSize)) {
                throw new VectorSizeMismatchException(
                        "Output vector size does not match training set!");
            }
        }
        /* if everything went ok, adds training element */
        this.elements.add(el);
    }

    /**
     * Removes training element at specified index position.
     * 
     * @param idx
     *            - the position of element to remove.
     */
    public void removeElementAt(int idx) {
        Validator.validateIntInRange(idx, 0, this.elements.size());
        this.elements.remove(idx);
    }

    /**
     * Returns Iterator for iterating training elements collection.
     * 
     * @return Iterator for iterating training elements collection.
     */
    public Iterator<E> iterator() {
        return this.elements.iterator();
    }

    // /**
    // * Returns training elements collection
    // *
    // * @return training elements collection
    // */
    // @Deprecated
    // public List<E> trainingElements() {
    // return this.elements;
    // }

    /**
     * Returns elements of this training set
     * 
     * @return training elements
     */
    public List<E> elements() {
        return this.elements;
    }

    /**
     * Returns training element at specified index position.
     * 
     * @param idx
     *            - the index position of training element to return.
     * @return training element at specified index position.
     */
    public TrainingElement elementAt(int idx) {
        Validator.validateIntInRange(idx, 0, this.elements.size());
        return this.elements.get(idx);
    }

    /**
     * Removes all elements from training set.
     */
    public void clear() {
        this.elements.clear();
    }

    /**
     * Returns <code>true</code> if training set is empty, <code>false</code>
     * otherwise
     * 
     * @return <code>true</code> if training set is empty, <code>false</code>
     *         otherwise.
     */
    public boolean isEmpty() {
        return this.elements.isEmpty();
    }

    /**
     * Returns number of training elements in this training set set.
     * 
     * @return number of training elements in this training set set.
     */
    public int size() {
        return this.elements.size();
    }

    /**
     * Returns label for this training set.
     * 
     * @return label for this training set.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label for this training set.
     * 
     * @param label
     *            - label for this training set.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Sets full file path for this training set.
     * 
     * @param filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Returns full file path for this training set.
     * 
     * @return full file path for this training set.
     */
    public String getFilePath() {
        return filePath;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getClass().getName());
        builder.append(" [");
        if (elements() != null) {
            builder.append("number of elements=");
            builder.append(elements().size());
            builder.append(", ");
        }
        if (getLabel() != null) {
            builder.append("label=");
            builder.append(getLabel());
            builder.append(", ");
        }
        if (getFilePath() != null) {
            builder.append("file path=");
            builder.append(getFilePath());
            builder.append(", ");
        }
        builder.append("output size=");
        builder.append(getOutputSize());
        builder.append(", input size=");
        builder.append(getInputSize());
        builder.append("]");
        return builder.toString();
    }

    /**
     * Saves this training set to the specified file.
     * 
     * @param filePath
     */
    public void save(String filePath) {
        this.filePath = filePath;
        this.save();
    }

    /**
     * Saves this training set to file specified in its filePath field.
     */
    public void save() {
        ObjectOutputStream out = null;

        try {
            if (filePath != null) {
                File file = new File(this.filePath);
                out = new ObjectOutputStream(new FileOutputStream(file));
                out.writeObject(this);
                out.flush();
            }

        } catch (Exception e) {
            log.severe(ANNUtils.stackToString(e, 2));
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioe) {
                }
            }
        }
    }

    /**
     * Save as text.
     * 
     * @param filePath
     * @param delimiter
     */
    public void saveAsTxt(String filePath, String delimiter) {
        if ((delimiter == null) || delimiter.equals("")) {
            delimiter = " ";
        }

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(new File(filePath)));

            for (E element : this.elements) {
                double[] input = element.getInput();
                for (int i = 0; i < input.length; i++) {
                    out.print(input[i] + delimiter);
                }

                if (element instanceof SupervisedTrainingElement) {
                    double[] output = ((SupervisedTrainingElement) element)
                            .getDesiredOutput();
                    for (int j = 0; j < output.length; j++) {
                        out.print(output[j] + delimiter);
                    }
                }
                out.println();
            }
            out.flush();
        } catch (Exception e) {
            log.severe(e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * Loads training set from the specified file.
     * 
     * @param filePath
     *            - training set file
     * @return loaded training set
     */
    @SuppressWarnings("rawtypes")
    public static TrainingSet load(String filePath) {
        ObjectInputStream oistream = null;
        if (filePath != null) {
            try {

                File file = new File(filePath);
                if (!file.exists()) {
                    throw new FileNotFoundException("Cannot find file: "
                            + filePath);
                }

                oistream = new ObjectInputStream(new FileInputStream(filePath));
                TrainingSet tSet = (TrainingSet) oistream.readObject();

                return tSet;

            } catch (ClassNotFoundException cnfe) {
                log.severe(cnfe.getMessage());
            } catch (IOException ioe) {//
                log.severe(ioe.getMessage());
            } finally {
                if (oistream != null) {
                    try {
                        oistream.close();
                    } catch (IOException ioe) {
                    }
                }
            }
        } else {
            log.warning("Provided path to the file is NULL");
        }

        return null;
    }

    /**
     * Creates training set from file.
     * 
     * @param filePath
     *            - a path to the file.
     * @param inputsCount
     *            - an input count
     * @param outputsCount
     *            - an output count
     * @param delimiter
     *            - a delimiter
     * 
     * @return {@link TrainingSet}
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static TrainingSet createFromFile(String filePath, int inputsCount,
            int outputsCount, String delimiter) {

        if(filePath == null) {
            return new TrainingSet();
        }

        Validator.validateString(filePath);
        FileReader fileReader = null;

        try {
            TrainingSet trainingSet = new TrainingSet();
            trainingSet.setInputSize(inputsCount);
            trainingSet.setOutputSize(outputsCount);
            fileReader = new FileReader(new File(filePath));
            @SuppressWarnings("resource")
            BufferedReader reader = new BufferedReader(fileReader);

            String line = "";

            while ((line = reader.readLine()) != null) {
                double[] inputs = new double[inputsCount];
                double[] outputs = new double[outputsCount];
                String[] values = line.split(delimiter);

                if (values[0].equals("")) {
                    continue; // skips if line was empty
                }
                for (int i = 0; i < inputsCount; i++) {
                    if(NNUtils.inBounds(i, values.length) && NNUtils.isNumeric(values[i])) {
                        inputs[i] = Double.parseDouble(values[i]);
                    }
                }

                for (int i = 0; i < outputsCount; i++) {
                    if(NNUtils.inBounds((inputsCount + i), values.length) && NNUtils.isNumeric(values[inputsCount + i])) {
                        outputs[i] = Double.parseDouble(values[inputsCount + i]);
                    }
                }

                if (outputsCount > 0) {
                    trainingSet.addElement(new SupervisedTrainingElement(inputs, outputs));
                } else {
                    trainingSet.addElement(new TrainingElement(inputs));
                }
            }

            return trainingSet;

        } catch (FileNotFoundException ex) {
            log.severe(ANNUtils.stackToString(ex, 2));
        } catch (IOException ex) {
            log.severe(ANNUtils.stackToString(ex));
        } catch (NumberFormatException ex) {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException ex1) {
                }
            }
            log.severe(ANNUtils.stackToString(ex));
            throw ex;
        }

        return new TrainingSet();
    }

    public void normalize() {
        this.normalize(new MaxNormalizer());
    }

    public void normalize(Normalizer normalizer) {
        normalizer.normalize(this);
    }

    /**
     * Returns output vector size of training elements in this training set This
     * method is implementation of EngineIndexableSet interface, and it is added
     * to provide compatibility with Encog data sets and FlatNetwork
     */
    // @Override
    public int getIdealSize() {
        return this.outputVectorSize;
    }

    /**
     * Sets an input vector size.
     * 
     * @param inputVectorSize
     */
    public void setInputSize(int inputVectorSize) {
        Validator.validateIntInRange(inputVectorSize, 1, Integer.MAX_VALUE);
        this.inputVectorSize = inputVectorSize;
    }

    /**
     * Sets an output vector size.
     * 
     * @param outputVectorSize
     */
    public void setOutputSize(int outputVectorSize) {
        // Validator.validateIntInRange(outputVectorSize, 1, Integer.MAX_VALUE);
        this.outputVectorSize = outputVectorSize;
    }

    /**
     * Creates training and test subsets.
     * 
     * <a href="http://java.about.com/od/javautil/a/uniquerandomnum.htm">unique
     * random number</a>
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public TrainingSet[] createTrainingAndTestSubsets(int trainSetPercent,
            int testSetPercent) {
        Validator.validateIntInRange(trainSetPercent, 1, 100);
        TrainingSet[] trainAndTestSet = new TrainingSet[2];

        ArrayList<Integer> randoms = new ArrayList<Integer>();
        for (int i = 0; i < this.size(); i++) {
            randoms.add(i);
        }

        Collections.shuffle(elements);

        /* Creates training set */
        trainAndTestSet[0] = new TrainingSet();
        int trainingElementsCount = this.size() * trainSetPercent / 100;
        for (int i = 0; i < trainingElementsCount; i++) {
            int idx = randoms.get(i);
            trainAndTestSet[0].addElement(this.elements.get(idx));
        }

        /* Creates test set */
        trainAndTestSet[1] = new TrainingSet();
        int testElementsCount = this.size() - trainingElementsCount;
        for (int i = 0; i < testElementsCount; i++) {
            int idx = randoms.get(trainingElementsCount + i);
            trainAndTestSet[1].addElement(this.elements.get(idx));
        }

        return trainAndTestSet;
    }

    /**
     * Returns output vector size of training elements in this training set.
     */
    public int getOutputSize() {
        return this.outputVectorSize;
    }

    /**
     * Returns input vector size of training elements in this training set This
     * method is implementation of EngineIndexableSet interface, and it is added
     * to provide compatibility with Encog data sets and FlatNetwork
     */
    // @Override
    public int getInputSize() {
        return this.inputVectorSize;
    }

    /**
     * Randomly permutes the specified list using a default source of
     * randomness.
     */
    public void shuffle() {
        Collections.shuffle(elements);
    }

    /**
     * Returns true if training set contains supervised training elements This
     * method is implementation of EngineIndexableSet interface, and it is added
     * to provide compatibility with Encog data sets and FlatNetwork
     */
    // @Override
    public boolean isSupervised() {
        return this.outputVectorSize > 0;
    }

    /**
     * Gets training data/record at specified index position. This method is
     * implementation of EngineIndexableSet interface. It is added for
     * Encog-Engine compatibility.
     */
    // @Override
    public void getRecord(long index, EngineData pair) {
        EngineData item = this.elements.get((int) index);
        pair.setInputArray(item.getInputArray());
        pair.setIdealArray(item.getIdealArray());
    }

    /**
     * Returns training elements/records count This method is implementation of
     * EngineIndexableSet interface. It is added for Encog-Engine compatibility.
     */
    // @Override
    public long getRecordCount() {
        return this.elements.size();
    }

    /**
     * This method is implementation of EngineIndexableSet interface, and it is
     * added to provide compatibility with Encog data sets and FlatNetwork.
     * 
     * Some datasets are not memory based, they may make use of a SQL connection
     * or a binary flat file. Because of this these datasets need to be cloned
     * for multi-threaded training or performance will greatly suffer. Because
     * this is a memory-based dataset, no cloning takes place and the "this"
     * object is returned.
     */
    // @Override
    public EngineIndexableSet openAdditional() {
        return this;
    }

}