## This is a short guide how to use this project.
Intention - to build comprehensive Java optical character recognition framework.
What is a current state:
it has two models:
filters - just for image manipulation. For example, noise reduction.
nn - neural network, taken to be maintainable from one place.

## ROADMAP
To add support of real OCR.

## Other
In addition there is a google doc where explained how to use filters. Also code snippets
included.
  
## How to contribute
There are a lot of ways to do so.
1. clone and use it
2. open bugs and request new features
3. distribute the knowledge if this framework, i.e write blogs, articles.  
4. contribute some money

Any other suggestions?
ping me: olegtikhonov [at] gmail [dot] com

 